import {connect} from 'react-redux';
import {filterByDate} from '../../../../redux/Home/HomeActions';
import {ByDateFilter} from './ByDateFilter';

function mapStateToProps(state, ownProps) {
  const startDate = state.filters['start[gte]'] || undefined;
  const endDate = state.filters['start[lte]'] || undefined;
  return {startDate, endDate};
}

export default connect(
  mapStateToProps,
  {filterByDate}
)(ByDateFilter);