import {connect} from 'react-redux';
import {filterByEngagementState} from '../../../../redux/Home/HomeActions';
import {ByEngagementStateFilter} from './ByEngagementStateFilter';

function mapStateToProps(state, ownProps) {
  const selected = state.filters['feat.eng_state'] ? state.filters['feat.eng_state'] !== 0 : false;
  return {selected};
}

export default connect(
  mapStateToProps,
  {filterByEngagementState}
)(ByEngagementStateFilter)