"""Handler class for aggregation table in reporting page"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging
from typing import List, Tuple
from handlers.db_data_handler import DBDataHandler
import numpy as np

from handlers.reporting_utils import *

logger = logging.getLogger(__name__)


class AggTable:
    """ Aggregation table class"""

    def __init__(self,
                 caption: str,
                 row_caption: str,
                 col_caption: str,
                 col_labels: List[str],
                 row_labels: List[str],
                 value_field: str,
                 precision: int = 2):
        self.caption = caption
        self.row_caption = row_caption
        self.col_caption = col_caption
        self.col_labels = col_labels
        self.row_labels = row_labels
        self.value_field = value_field
        self.precision = precision

    def set_values(self, values: list):
        self.values = values

    def set_totals(self, row_total, col_total, total: float):
        self.row_total = row_total
        self.col_total = col_total
        self.total = total

    def _round(self, x: float) -> float:
        return round(x, self.precision)  # ('%.2f' % x).rstrip('0').rstrip('.')

    def to_dict(self):
        return {
            "caption": self.caption,
            "row_caption": self.row_caption,
            "col_caption": self.col_caption,
            "col_labels": self.col_labels,
            "row_labels": self.row_labels,
            "values": [[self._round(i) for i in x] for x in self.values],
            "col_total": [self._round(i) for i in self.col_total],
            "row_total": [self._round(i) for i in self.row_total],
            "total": self._round(self.total),
        }


class ReportingTripsTableAggHandler:
    """
    Class for fetching report data (/reporting/trips/tables/agg)
    """

    def __init__(self):
        self.db_handler = DBDataHandler()

    def get_table_agg(self, start: int, end: int, location: str,
                      report_type: str, ign_dur: bool, ign_dist: bool,
                      ign_no_info: bool, ign_priv: bool) -> list:
        """
           Get a list of tables
            :param start: start timestamp in ns
            :param end: end timestamp in ns
            :param location: country string ["DE", "US"]
            :param report_type: Type of the report ["DMV"]
            :param ign_dur: Ignore trips with short duration
            :param ign_dist: Ignore trips with short distance
            :param ign_no_info:  Ignore trips with no diseng info
            :param ign_priv: Ignore trips on private roads
            :return: list of tables
        """
        # TODO: Create aggregation pipelines in a flexible, reusable way instead of hard-wiring everything
        if location not in REPORTING_LOCATIONS:
            raise ValueError(
                f"'location' has to be one of {REPORTING_LOCATIONS.keys()}")
        else:
            loc = REPORTING_LOCATIONS[location]

            public_only = True
            if report_type == REPORT_TYPE_DMV:
                public_only = ign_priv

            # fetch public data from db
            res_pub = self.db_handler.aggregate_extractor_requests(
                self._pipeline(start, end, loc, ign_dur, ign_dist, ign_no_info,
                               public_only))

            if len(res_pub) > 0:  # check if any results were returned
                if report_type == REPORT_TYPE_DMV:
                    # create DMV tables
                    return [t.to_dict() for t in self._get_dmv_tables(res_pub)]
                elif report_type == REPORT_TYPE_RP:
                    # fetch total data from db
                    res_tot = self.db_handler.aggregate_extractor_requests(
                        self._pipeline(start, end, loc, ign_dur, ign_dist,
                                       ign_no_info, False))
                    if len(res_tot) > 0:  # check if any results were returned
                        # create RP tables
                        return [
                            t.to_dict()
                            for t in self._get_rp_tables(res_pub, res_tot)
                        ]
        return []

    def _get_dmv_tables(self, query_result) -> List[AggTable]:
        """ Get DMV Report tables"""
        dist_table = self._create_sum_table(
            query_result, "Engaged Distance per Month and VIN", "VIN", "MONTH",
            "engaged_distance_km", MILES_IN_KM)
        diseng_table = self._create_sum_table(
            query_result,
            "Unplanned Disengagements(manual + error) per Month and VIN",
            "VIN", "MONTH", "total_unplanned_disengagements")

        # calculate miles per diseng by dividing the first two tables
        vins, months = self._get_vins_and_months(query_result)
        miles_per_diseng_table = AggTable(
            "Miles per Unplanned Disengagement per Month and VIN", "VIN",
            "MONTH", months, vins, "miles_per_disengagement")
        self._divide_tables(dist_table, diseng_table, miles_per_diseng_table)
        return [dist_table, diseng_table, miles_per_diseng_table]

    def _get_rp_tables(self, res_pub, res_tot) -> List[AggTable]:
        """ Get Regierungspräsidium Report tables"""
        pub_table = self._create_sum_table(
            res_pub, "Public Engaged Distance per Month and VIN", "VIN",
            "MONTH", "engaged_distance_km")
        tot_table = self._create_sum_table(
            res_tot, "Total Engaged Distance per Month and VIN", "VIN",
            "MONTH", "engaged_distance_km")

        return [pub_table, tot_table]

    def _create_sum_table(self,
                          query_res,
                          caption: str,
                          row_caption: str,
                          col_caption: str,
                          value_field: str,
                          factor: float = 1) -> AggTable:
        """
        Create a table from the given query res with totals sums
        :param query_res: Mongo Query Result
        :param caption: Table caption
        :param row_caption: Row caption
        :param col_caption: Col caption
        :param value_field: Field in the query result holding the values
        :param factor: Each value will be multiplied by this
        :return: 
        """
        vins, months = self._get_vins_and_months(query_res)
        table = AggTable(caption, row_caption, col_caption, months, vins,
                         value_field)

        values = []
        # initialize table values with zeros
        for _ in vins:
            values.append([0] * len(months))

        for result in query_res:
            # prepare date column name
            col = str(result["_id"]["month"].month) + "-" + str(
                result["_id"]["month"].year)

            # prepare vin row name
            if "vin" in result["_id"]:
                if result["_id"]["vin"] is not None:
                    row = result["_id"]["vin"]
                else:
                    row = "Unknown"
            else:
                row = "Unknown"

            values[vins.index(row)][months.index(
                col)] = result[table.value_field] * factor

        table.set_values(values)

        np_values = np.array(table.values)
        col_total_dist = np.sum(np_values, axis=0).tolist()
        row_total_dist = np.sum(np_values, axis=1).tolist()
        total_dist = sum(col_total_dist)
        table.set_totals(row_total_dist, col_total_dist, total_dist)
        return table

    def _divide_tables(self, table1: AggTable, table2: AggTable,
                       result_table: AggTable):
        """ Divides all values and totals of table1 by table2 and stores the result in result_table"""
        values_1 = np.array(table1.values)
        values_2 = np.array(table2.values)
        result_table.set_values(
            np.divide(values_1, values_2, out=values_1,
                      where=values_2 != 0).tolist())
        row_total = [
            float(x) / max(float(y), 1)
            for x, y in zip(table1.row_total, table2.row_total)
        ]
        col_total = [
            float(x) / max(float(y), 1)
            for x, y in zip(table1.col_total, table2.col_total)
        ]
        total = float(table1.total) / max(float(table2.total), 1)
        result_table.set_totals(row_total, col_total, total)

    def _pipeline(self, start: int, end: int, location: str, ign_dur: bool,
                  ign_dist: bool, ign_no_info: bool,
                  public_only: bool) -> list:
        """ Mongo Aggregation Pipeline for tables"""
        return [
            self._agg_match_stage(start, end, location, ign_dur, ign_dist,
                                  ign_no_info, public_only),
            self._agg_add_fields_stage_1(),
            self._agg_group_stage(public_only),
            self._agg_add_fields_stage_2()
        ]

    @staticmethod
    def _get_vins_and_months(query_res) -> Tuple[List[str], List[str]]:
        """Get vins and months from mongo query result"""
        vins = set()
        months = set()
        for result in query_res:
            if "vin" in result["_id"]:
                if result["_id"]["vin"] is not None:
                    vins.add(result["_id"]["vin"])
                else:
                    vins.add("Unknown")
            else:
                vins.add("Unknown")

            months.add(result["_id"]["month"])
        months = list(months)
        months.sort()
        months = [
            str(date_obj.month) + "-" + str(date_obj.year)
            for date_obj in months
        ]
        vins = list(vins)
        vins.sort()
        return vins, months

    @staticmethod
    def _agg_match_stage(start: int, end: int, location: str, ign_dur: bool,
                         ign_dist: bool, ign_no_info: bool,
                         public_only: bool) -> dict:
        stage = {
            "start": {
                "$gte": start
            },
            "end": {
                "$lte": end
            },
            "extractor_id": "feature_extraction_diseng_details"
        }
        if location in ["DE", "US"]:
            stage["metadata.country"] = location
        elif location != "Any":
            stage["metadata.geofence"] = location

        if ign_no_info:
            stage["metadata.end_details"] = {"$ne": None}
        if ign_dist:
            stage["metadata.distance"] = {"$gte": MIN_DRIVE_DISTANCE}
        if ign_dur:
            stage["metadata.duration"] = {"$gte": MIN_DRIVE_DURATION}
        if public_only:
            stage["$or"] = [{
                "metadata.end_public_road": {
                    "$eq": True
                }
            }, {
                "metadata.distance_public": {
                    "$ne": 0
                }
            }]
        return {"$match": stage}

    @staticmethod
    def _agg_add_fields_stage_1() -> dict:
        return {
            "$addFields": {
                "manual_unplanned_diseng": {
                    "$cond": [{
                        "$and": [{
                            "$eq": ["$metadata.manual_disengagement", True]
                        }, {
                            "$ne": ["$metadata.end_planned", True]
                        }]
                    }, 1, 0]
                },
                "error_diseng": {
                    "$cond": [{
                        "$and": [{
                            "$ne": ["$metadata.manual_disengagement", True]
                        }, {
                            "$ne": ["$metadata.end_planned", True]
                        }]
                    }, 1, 0]
                },
                "date": {
                    "$dateFromString": {
                        "dateString": {
                            "$substr": ["$metadata.end_ts", 0, 7]
                        }
                    }
                }
            }
        }

    @staticmethod
    def _agg_group_stage(public_only: bool) -> dict:
        if public_only:
            dist = "$metadata.distance_public"
        else:
            dist = "$metadata.distance"
        return {
            "$group": {
                "_id": {
                    "vin": "$metadata.vin",
                    "month": "$date"
                },
                "total_unplanned_disengagements": {
                    "$sum": {
                        "$add": ["$error_diseng", "$manual_unplanned_diseng"]
                    }
                },
                'engaged_distance_m': {
                    "$sum": dist
                }
            }
        }

    @staticmethod
    def _agg_add_fields_stage_2() -> dict:
        return {
            "$addFields": {
                "engaged_distance_km": {
                    "$divide": ["$engaged_distance_m", 1000]
                }
            }
        }
