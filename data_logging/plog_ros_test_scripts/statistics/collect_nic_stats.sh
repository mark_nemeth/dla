#!/bin/sh
# This script collects the stats from different sources and analyses the
# stats
INTERFACE=$2
FILENAME_ETHTOOL="nic_stat_ethtool_${INTERFACE}_$1.log"
FilENAME_IFCONFIG="nic_stat_ifconfig_${INTERFACE}_$1.log"
FILENAME_OUTPUT="nic_statistics_${INTERFACE}_$1.log"
#load and analyse the ethtool nic_statistics
declare -a ethtool_errors=('rx_bytes:' 'rx_packets:' 'rx_wqe_err' 'rx_buff_alloc_err:'
  'rx_crc_errors_phy:' 'rx_in_range_len_errors_phy' 'rx_symbol_err_phy'
   'rx0_wqe_err'  'rx0_buff_alloc_err'  'rx1_wqe_err'  'rx1_buff_alloc_err'
   'rx2_wqe_err'  'rx2_buff_alloc_err' 'rx3_wqe_err'  'rx3_buff_alloc_err'
   'rx4_wqe_err'  'rx4_buff_alloc_err' 'rx5_wqe_err'  'rx5_buff_alloc_err'
    'rx6_wqe_err'  'rx6_buff_alloc_err' 'rx7_wqe_err'  'rx7_buff_alloc_err'
    'rx_xdp_drop' 'rx0_xdp_drop' 'rx1_xdp_drop' 'rx2_xdp_drop' 'rx3_xdp_drop'
     'rx4_xdp_drop' 'rx5_xdp_drop' 'rx6_xdp_drop' 'rx7_xdp_drop')

declare -a ifconfig_errors=('rx_bytes' 'rx_packages' 'rx_errors' 'rx_dropped'
 'rx_overruns' 'collisions')
echo "-----------------------------------------------------------"  >> $FILENAME_OUTPUT
echo "ethtool nic statistics" >> $FILENAME_OUTPUT
echo "-----------------------------------------------------------"  >> $FILENAME_OUTPUT
NUMBEROFPARAMS=${#ethtool_errors[@]}
declare -A ethtool_before
declare -A ethtool_after
declare -A ethtool_differences
for (( i=1; i<${NUMBEROFPARAMS}+1; i++ ));
do
    ethtool_before["${ethtool_errors[$i-1]}"]=$(more $FILENAME_ETHTOOL | grep "${ethtool_errors[$i-1]}" -m 1 | cut -d":" -f2 | sed 's/ //g')
    ethtool_after["${ethtool_errors[$i-1]}"]=$(more $FILENAME_ETHTOOL | grep "${ethtool_errors[$i-1]}" -m 2 | tail -n1 |cut -d":" -f2  | sed 's/ //g')
    ethtool_differences["${ethtool_errors[$i-1]}"]=$(expr ${ethtool_after["${ethtool_errors[$i-1]}"]} - ${ethtool_before["${ethtool_errors[$i-1]}"]})
#activate the if condition if only the differences should be plotted
#     if [ ${ethtool_differences["${ethtool_errors[$i-1]}"]} != 0 ]; then
       echo "${ethtool_errors[$i-1]} difference:  ${ethtool_differences["${ethtool_errors[$i-1]}"]}" >>  $FILENAME_OUTPUT
#     fi
done
#load and analyse the ifconfig nic_statistics
echo "-----------------------------------------------------------"  >> $FILENAME_OUTPUT
echo "ifconfig nic statistics" >> $FILENAME_OUTPUT
echo "-----------------------------------------------------------"  >> $FILENAME_OUTPUT
NUMBEROFPARAMS=${#ifconfig_errors[@]}
declare -A ifconfig_before
declare -A ifconfig_after
declare -A ifconfig_differences
#check for rx bytes:
ifconfig_before['rx_bytes']=$(more $FilENAME_IFCONFIG | grep 'RX bytes:' -m1 |sed -e 's/^[ \t]*//' |cut -d " " -f2 |cut -d ":" -f2)
ifconfig_after['rx_bytes']=$(more $FilENAME_IFCONFIG  | grep 'RX bytes:' -m 2 | tail -n1 | sed -e 's/^[ \t]*//' |cut -d " " -f2 |cut -d ":" -f2)
ifconfig_differences['rx_bytes']=$(expr ${ifconfig_after['rx_bytes']} - ${ifconfig_before['rx_bytes']})
#activate the if condition if only the differences should be plotted
# if [ ${ifconfig_differences['rx_bytes']} != 0 ]; then
   echo "rx_bytes difference:  ${ifconfig_differences['rx_bytes']}" >>  $FILENAME_OUTPUT
# fi
#check for the errors
for (( i=2; i<${NUMBEROFPARAMS}; i++ ));
do
    x=$i
    ifconfig_before["${ifconfig_errors[$i-1]}"]=$(more $FilENAME_IFCONFIG | grep 'RX packets' -m 1 | sed -e 's/^[ \t]*//' |cut -d " " -f$x |cut -d ":" -f2)
    ifconfig_after["${ifconfig_errors[$i-1]}"]=$(more $FilENAME_IFCONFIG | grep 'RX packets' -m 2 | tail -n1 | sed -e 's/^[ \t]*//' |cut -d " " -f$x |cut -d ":" -f2)
    ifconfig_differences["${ifconfig_errors[$i-1]}"]=$(expr ${ifconfig_after["${ifconfig_errors[$i-1]}"]} - ${ifconfig_before["${ifconfig_errors[$i-1]}"]})
#      if [ ${ifconfig_differences["${ifconfig_errors[$i-1]}"]} != 0 ]; then
       echo "${ifconfig_errors[$i-1]} difference:  ${ifconfig_differences["${ifconfig_errors[$i-1]}"]}" >>  $FILENAME_OUTPUT
#     fi
done
#check for collisions:
ifconfig_before['collisions']=$(more $FilENAME_IFCONFIG | grep 'collisions' -m 1| sed -e 's/^[ \t]*//' |cut -d " " -f1 |cut -d ":" -f2)
ifconfig_after['collisions']=$(more $FilENAME_IFCONFIG  | grep 'collisions' -m 2 | tail -n1 | sed -e 's/^[ \t]*//' |cut -d " " -f1 |cut -d ":" -f2)
ifconfig_differences['collisions']=$(expr ${ifconfig_after["${ifconfig_errors[$i-1]}"]} - ${ifconfig_before["${ifconfig_errors[$i-1]}"]})
#activate the if condition if only the differences should be plotted
# if [ ${ifconfig_differences['collisions']} != 0 ]; then
   echo "collisions difference:  ${ifconfig_differences['collisions']}" >>  $FILENAME_OUTPUT
# fi
