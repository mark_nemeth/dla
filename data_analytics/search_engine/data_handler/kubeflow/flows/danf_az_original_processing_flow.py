__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from pathlib import Path, PurePath
from typing import Optional

from DANFUtils.danf_environment import Platform, danf_env
from kfp import dsl
from kubeflow.flows.common import BaseFlow, get_mount_id
from kubeflow.steps.atbeval_extractor import AtbevalExtractorOp
from kubeflow.steps.atpdml_extractor import AtpdmlExtractorOp
from kubeflow.steps.atpdml_poi_extractor import AtpdmlPoiExtractorOp
from kubeflow.steps.atpftst_extractor import AtpftstExtractorOp
from kubeflow.steps.cleanup import CleanupOp
from kubeflow.steps.danf_extractor import DanfExtractorInitOp, DanfExtractorOp
from kubeflow.steps.rca_extractor import RcaExtractorOp
from kubeflow.steps.splitter import AzureSplitterOp
from kubeflow.steps.stripper import StripperOp
from kubernetes.client import V1LocalObjectReference
from model.paths import BlobStoragePath
from proxies.blob_storage import BlobStorageHandler

logger = logging.getLogger(__name__)


class DanfAzOriginalProcessingFlow(BaseFlow):

    NAME = "danf-az-original-processing-flow"
    VERSION = "1.0"
    DESCRIPTION = "Azure flow for processing ORIGINAL bagfiles"
    IMAGE_PULL_SECRET = "dla-docker-pull-secret"
    OUTPUT_CONTAINER_NAME = "output"

    @classmethod
    def create(cls,
               bagfile: dict,
               flow_run_guid: str,
               input_location: BlobStoragePath,
               test_mode: bool):
        """Creates a new instance of this flow. The flow can run on a single
        bagfile or on a whole container.

        :param bagfile:
        :param flow_run_guid:
        :param input_location: path to the bagfile
        :param test_mode:
        :return:
        """
        BlobStorageHandler()._ensure_container_exists(storage_account=input_location.storage_account,
                                                      container_name=cls.OUTPUT_CONTAINER_NAME)
        return cls._build_pipeline(
            bagfile['guid'],
            flow_run_guid,
            bagfile['link'],
            input_location,
            test_mode)

    @classmethod
    def _build_pipeline(cls,
                        bagfile_guid,
                        flow_run_guid,
                        file_path,
                        input_location,
                        test_mode):
        @dsl.pipeline(
            name=cls.NAME,
            description=cls.DESCRIPTION
        )
        def processing_pipeline():
            # initialize param
            mount_id = get_mount_id()
            logger.info(f"Processing DanfAzOriginalProcessingFlow for bagfile_guid {bagfile_guid}")

            cleanup_step = CleanupOp.create_for_azure(
                tasks=['set-sync-flag', 'update-flow-run'],
                bagfile_guid=bagfile_guid,
                flow_run_guid=flow_run_guid,
                bagfile_name=input_location.file,
                test_mode=test_mode
            )

            with dsl.ExitHandler(cleanup_step):
                # create metadata for each bagfile in the DB
                init_step = DanfExtractorInitOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=input_location,
                    output_container=cls.OUTPUT_CONTAINER_NAME,
                    mount_id=mount_id,
                    test_mode=test_mode
                )

                # strip the bagfiles by topic into childbagfiles
                stripper_step = StripperOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=input_location,
                    output_container=cls.OUTPUT_CONTAINER_NAME,
                    mount_id=mount_id,
                    test_mode=test_mode
                ).after(init_step)

                # extract events for DANF
                extractor_step = DanfExtractorOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=input_location,
                    output_container=cls.OUTPUT_CONTAINER_NAME,
                    mount_id=mount_id,
                    test_mode=test_mode
                ).after(init_step)

                # split the bagfile into childbagfile by time
                splitter_step = AzureSplitterOp.create_azure_splitter(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=input_location,
                    output_container=cls.OUTPUT_CONTAINER_NAME,
                    mount_id=mount_id,
                    test_mode=test_mode
                ).after(extractor_step)

        return DanfAzOriginalProcessingFlow(processing_pipeline)
