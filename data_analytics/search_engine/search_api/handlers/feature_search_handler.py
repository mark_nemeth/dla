"""Handler class for feature search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging
from typing import List

from handlers.db_data_handler import DBDataHandler
import re
logger = logging.getLogger(__name__)


class FeatureSearchHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def create_search_kwargs(self, query_args: dict) -> dict:
        """
        Build search arguments from query arguments 
        """
        search_kwargs = {
            'query': None,
            'limit': 50,
            'offset': 0,
            'sortby': 'guid'
        }
        query = {}
        range_query = []
        elem_match_query = {}
        for key, value in query_args.items():
            # Query filters
            if key in ['bagfile_guid', 'guid']:
                query[key] = value[0]
            elif key == "start[gte]":
                try:
                    range_query.append({'start': {"$gte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid start[gte] query parameter in feature search - integer expected'
                    )
            elif key == "start[lte]":
                try:
                    range_query.append({'start': {"$lte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid start[lte] query parameter in feature search - integer expected'
                    )
            elif key == "end[gte]":
                try:
                    range_query.append({'end': {"$gte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid end[gte] query parameter in feature search - integer expected'
                    )
            elif key == "end[lte]":
                try:
                    range_query.append({'end': {"$lte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid end[lte] query parameter in feature search - integer expected'
                    )
            elif key == "start_index[gte]":
                try:
                    range_query.append(
                        {'start_index': {
                            "$gte": abs(int(value[0]))
                        }})
                except ValueError:
                    logger.warning(
                        'Invalid start_index[gte] query parameter in feature search - integer expected'
                    )
            elif key == "start_index[lte]":
                try:
                    range_query.append(
                        {'start_index': {
                            "$lte": abs(int(value[0]))
                        }})
                except ValueError:
                    logger.warning(
                        'Invalid start_index[lte] query parameter in feature search - integer expected'
                    )
            elif key == "end_index[gte]":
                try:
                    range_query.append(
                        {'end_index': {
                            "$gte": abs(int(value[0]))
                        }})
                except ValueError:
                    logger.warning(
                        'Invalid end_index[gte] query parameter in feature search - integer expected'
                    )
            elif key == "end_index[lte]":
                try:
                    range_query.append(
                        {'end_index': {
                            "$lte": abs(int(value[0]))
                        }})
                except ValueError:
                    logger.warning(
                        'Invalid end_index[lte] query parameter in feature search - integer expected'
                    )
            elif "feat." in key:
                # handle queries for feature values
                feat_regex = r"^feat.([\w|_]*)(\[gte\]|\[lte\]|\[\]|)$"
                match = re.search(feat_regex, key, re.IGNORECASE)
                if match:
                    feat_name = match.group(1)
                    operator = match.group(2).replace("[", "").replace("]", "")
                    if operator == "":  # matches equals condition
                        elem_match_query[feat_name] = float(value[0])
                    else:
                        operator = f"${operator}"
                        # handle multiple conditions for one feature
                        if feat_name in elem_match_query:
                            elem_match_query[feat_name][operator] = float(
                                value[0])
                        else:
                            elem_match_query[feat_name] = {
                                operator: float(value[0])
                            }

            # Pagination
            elif key == 'limit':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning(
                        'Invalid limit query parameter in  feature search - integer expected'
                    )
            elif key == 'offset':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning(
                        'Invalid offset query parameter in  feature search - integer expected'
                    )
            # Sort
            elif key == 'sortby':
                # Ascending order
                if value[0] in [
                        'guid', 'bagfile_guid', 'start', 'start_index'
                ]:
                    search_kwargs[key] = value[0]
                elif value[0] in [
                        '+guid', '+bagfile_guid', '+start', '+start_index'
                ]:
                    search_kwargs[key] = value[0][1:]
                # Descending order
                elif value[0] in [
                        '-guid', '-bagfile_guid', '-start', '-start_index'
                ]:
                    search_kwargs[key] = [(value[0][1:], -1)]

        if len(range_query) >= 1:
            query['$and'] = range_query

        if len(elem_match_query) >= 1:
            query['features'] = {"$elemMatch": elem_match_query}
        search_kwargs['query'] = query
        return search_kwargs

    def perform_search(self, **search_kwargs: dict) -> (list, int):
        """
        For the given search arguments return all matching feature buckets
        """
        return self.db_data_handler.search_feature_buckets(**search_kwargs)

    def perform_search_with_aggregation(self, feature_query: dict,
                                        bagfile_guids: List[str], offset: int,
                                        limit: int):
        """
        For the given search arguments return all matching feature buckets
        """
        res = []
        total = 0
        query_offset = 0
        bagfile_guid_limit = max(500, int(len(bagfile_guids) / 20))
        # query the feature bucket collection with a limited number of bagfiles at a time
        # the query will be repeated if not enough matching files where found
        while len(res) < offset + limit:
            pipeline = [{
                "$match": {
                    "features": feature_query,
                    "bagfile_guid": {
                        "$in":
                            bagfile_guids[query_offset:query_offset +
                                                       bagfile_guid_limit]
                    }
                }
            }, {
                '$project': {
                    'bagfile_guid': 1,
                    'start': 1
                }
            }, {
                '$group': {
                    '_id': '$bagfile_guid',
                    'min_start': {
                        '$min': '$start'
                    }
                }
            }, {
                '$sort': {
                    'start': 1
                }
            }, {
                '$project': {
                    'bagfile_guid': 1
                }
            }]
            logger.info(
                f"Executing bagfile search query (on features collection ): {pipeline}"
            )
            res.extend(
                self.db_data_handler.aggregate_feature_buckets(pipeline))

            query_offset += bagfile_guid_limit
            if query_offset >= len(bagfile_guids):
                break

        if total == 0 and query_offset != 0:
            # estimate number of total results, since the exact number is not available
            total = len(bagfile_guids) / query_offset * len(res)
        elif query_offset >= len(bagfile_guids) or len(res) < offset + limit:
            total = len(res)
        res_guids = [f["_id"] for f in res]
        return res_guids[offset:min(offset +
                                    limit, len(res_guids))], int(total)
