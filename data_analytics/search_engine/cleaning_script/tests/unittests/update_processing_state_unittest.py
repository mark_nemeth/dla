__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from test.support import EnvironmentVarGuard
from unittest.mock import Mock

from DANFUtils.exceptions import RESTException

from cleanup.tasks.update_processing_state import UpdateProcessingStateTask


class TestUpdateProcessingStateTask(unittest.TestCase):

    BAGFILE_GUID = '00000000-0000-0000-0000-000000000000'

    def setUp(self):
        self.env = EnvironmentVarGuard()
        self.env.set('BAGFILE_GUID', self.BAGFILE_GUID)

        # prevent that initial unmocked container tries to obtain oauth token
        self.env.set('AUTH', 'none')

        self.mock_container_client = Mock()
        self.task = UpdateProcessingStateTask()
        self.task.client = self.mock_container_client

    def test_execute_when_all_steps_are_successful_should_result_in_successful(self):
        self._mock_bagfile([
            ('extractor', 'SUCCESS'),
            ('splitter', 'SUCCESS')
        ])
        self.task.execute()
        self._assert_processing_state('SUCCESSFUL')

    def test_execute_when_steps_is_successfully_retried_should_result_in_successful(self):
        self._mock_bagfile([
            ('extractor', 'FAILED'),
            ('extractor', 'SUCCESS')
        ])
        self.task.execute()
        self._assert_processing_state('SUCCESSFUL')

    def test_execute_when_one_step_failed_should_result_in_failed(self):
        self._mock_bagfile([
            ('extractor', 'FAILED'),
            ('extractor', 'FAILED'),
            ('extractor', 'FAILED'),
            ('extractor', 'FAILED'),
            ('splitter', 'SUCCESS')
        ])
        self.task.execute()
        self._assert_processing_state('FAILED')

    def test_execute_when_steps_are_successful_and_skipped_should_result_in_successful(self):
        self._mock_bagfile([
            ('extractor', 'SUCCESS'),
            ('splitter', 'SKIPPED')
        ])
        self.task.execute()
        self._assert_processing_state('SUCCESSFUL')

    def test_execute_when_guid_not_present_should_raise_exception(self):
        self.env.pop('BAGFILE_GUID')
        self._mock_bagfile([('extractor', 'SUCCESS')])
        self.assertRaises(ValueError, self.task.execute)

    def test_execute_when_get_bagfile_fails_should_raise_exception(self):
        exp = RESTException('Test', 'GET', '', 500, 'error')
        self.mock_container_client.get_bagfile.side_effect = exp
        self.assertRaises(Exception, self.task.execute)

    def test_execute_when_submitting_state_fails_should_raise_exception(self):
        self._mock_bagfile([('extractor', 'SUCCESS')])
        exp = RESTException('Test', 'PATCH', '', 500, 'error')
        self.mock_container_client.update_bagfile.side_effect = exp
        self.assertRaises(RuntimeError, self.task.execute)

    def _mock_bagfile(self, states):
        bag = {'guid': self.BAGFILE_GUID, 'state': []}
        for state in states:
            bag['state'].append({'name': state[0], 'status': state[1]})
        self.mock_container_client.get_bagfile.return_value = bag

    def _assert_processing_state(self, expected_state):
        self.mock_container_client.update_bagfile.assert_called_with(
            self.BAGFILE_GUID,
            {'processing_state': expected_state}
        )


if __name__ == '__main__':
    unittest.main()
