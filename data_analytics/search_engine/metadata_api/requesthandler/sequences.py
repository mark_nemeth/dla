"""Search Engine and Metadata REST API sequence route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.constants import constants
from DANFUtils.exceptions import GUIDNotFoundException, DANFBaseException
from DANFUtils.tracing import tracing
from DANFUtils.utils import validate_json

from handlers.auth import auth
from handlers.bagfiles import BagFilesHandler
from handlers.sequences import SequenceHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


logger = logging.getLogger(__name__)


class SequencesRequestHandler(BaseRequestHandler):
    """
    Sequence Route RequestHandler
    """

    def initialize(self):
        """
        Hook for Sequence Route RequestHandler initialization
        :return: None
        """
        self.schema_guid = super().load_json_schema("schema_guid")
        self.schema_post = super().load_json_schema(
            "sequence_metadata_schema_post")
        self.schema_patch = super().load_json_schema(
            "sequence_metadata_schema_patch")

        self.sequence_handler = SequenceHandler()
        self.bagfile_handler = BagFilesHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET, POST, PATCH, DELETE')

    @auth.requires_entitlements(["openid"])
    def post(self):
        """
        Hook for Sequence POST method
        :return: None
        """

        #################################
        ##### Request Interpretation ####
        #################################
        tracing.start_task_in_context('sequences/post',
                                      context_headers=self.request.headers)
        try:
            data = validate_json(self.request.body.decode('utf-8'),
                                 self.schema_post)
        except DANFBaseException as e:
            error_message = 'Bad request: ' + str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_BAD_REQ"])
            return

        #################################
        ####### Process Request #########
        #################################
        try:
            guid = self.sequence_handler.add_sequence_to_db(data)
            res = 'Sequence(s) succesfully added to db.'
        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_SERVER_ERROR"])
            return

        ################################
        ########## RESPONSE ############
        ################################
        logger.info("request served successfully")
        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res,
            status_code=constants["REQ_STATUS_CODE_RESOURCE_CREATED"],
            location="/sequence/" + guid)
        return

    @auth.requires_entitlements(["openid"])
    def get(self, guid):
        """
        Hook for Sequence GET method
        :param guid: sequence guid
        :return: None
        """

        #################################
        ##### Request Interpretation ####
        #################################
        tracing.start_task_in_context('sequences/get',
                                      context_headers=self.request.headers)
        try:
            validate_json('{"guid":"' + str(guid) + '"}', self.schema_guid)
        except DANFBaseException as e:
            error_message = 'Bad request: ' + str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_BAD_REQ"])
            return

        #################################
        ####### Process Request #########
        #################################
        try:
            res = self.sequence_handler.get_sequence_from_db(guid)
        except GUIDNotFoundException as e:
            error_message = str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_NOT_FOUND"])
            return
        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_SERVER_ERROR"])
            return

        ################################
        ########## RESPONSE ############
        ################################
        logger.info("request served successfully")
        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
        return

    @auth.requires_entitlements(["openid"])
    def patch(self, guid):
        """
            Hook for Sequence Patch method
            :param guid: sequence guid
            :return: None
        """

        #################################
        ##### Request Interpretation ####
        #################################
        tracing.start_task_in_context('sequences/patch',
                                      context_headers=self.request.headers)
        try:
            validate_json('{"guid":"' + str(guid) + '"}', self.schema_guid)
            data = validate_json(self.request.body.decode('utf-8'),
                                 self.schema_patch)
        except DANFBaseException as e:
            error_message = 'Bad request: ' + str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_BAD_REQ"])
            return

        #################################
        ####### Process Request #########
        #################################
        try:
            self.sequence_handler.update_sequence_in_db(guid, data)
            res = 'Sequence(s) succesfully updated in db.'
        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_SERVER_ERROR"])
            return

        ################################
        ########## RESPONSE ############
        ################################
        logger.info("request served successfully")
        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
        return

    @auth.requires_entitlements(["openid"])
    def delete(self, guid):
        """
        Hook for Sequence DELETE method
        :param guid: sequence guid
        :return: None

        """

        #################################
        ##### Request Interpretation ####
        #################################
        tracing.start_task_in_context('sequences/delete',
                                      context_headers=self.request.headers)
        try:
            validate_json('{"guid":"' + str(guid) + '"}', self.schema_guid)
        except DANFBaseException as e:
            error_message = 'Bad request: ' + str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_BAD_REQ"])
            return

        #################################
        ####### Process Request #########
        #################################
        try:
            res = self.sequence_handler.delete_sequence_from_db(guid)

        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            self._handle_error(error_message,
                               constants["REQ_STATUS_CODE_SERVER_ERROR"])
            return

        ################################
        ########## RESPONSE ############
        ################################
        logger.info("request served successfully")
        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
        return

    def _handle_error(self, error_msg, status_code):
        logger.exception(error_msg)
        tracing.end_task(success=False)
        utils.build_response(self, utils.build_error_from_message(error_msg),
                             None, status_code)
