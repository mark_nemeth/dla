"""GPS Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config.constants import *
from extractors.utils import timestamp2s, timestamp2ms
from collections import Counter
from extractors.base_interpreter import Interpreter

BVA_STEREO_CNN = 0


class StereoCnnInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.last_ts = None
        self.current_ts = None
        self.bounding_box_for_current_ts = {}
        self.labels_for_current_ts = {}
        self.bva = BVA_STEREO_CNN
        self.event_type = "metadata_enrichment"

    @staticmethod
    def get_bounding_box_label_dict(msg):
        bb_label_list = list(bb.label.id for bb in msg.bounding_boxes)
        bb_label_dict = Counter(bb_label_list)
        return dict(bb_label_dict)

    @staticmethod
    def get_label_set_label_dict(msg):
        label_set_list = list(l.id for l in msg.label_set.labels)
        return label_set_list

    def feed_new_msg(self, topic, msg, t):
        self.last_ts = self.current_ts
        self.current_ts = timestamp2s(t)

        # Logic: save one cnn entry for each sequence (1 sec), so in each second we only consider the first message
        # First message in the bagfile: append to list
        if self.last_ts is None:
            self.bounding_box_for_current_ts = self.get_bounding_box_label_dict(msg)
            self.labels_for_current_ts = self.get_label_set_label_dict(msg)

        else:
            # Still in the same second: do not consider the message
            if self.last_ts == self.current_ts:
                pass
            # Entering the next second: append to list and update
            else:
                stereo_cnn_obj = {}
                stereo_cnn_obj["ts"] = self.last_ts
                stereo_cnn_obj["bounding_box"] = self.bounding_box_for_current_ts
                stereo_cnn_obj["label_set"] = self.labels_for_current_ts

                stereo_cnn_obj = self.calc_start_end_ts_snippet(stereo_cnn_obj)

                # Append the message from last second to list
                self.event_list += [stereo_cnn_obj]

                # Save current message
                self.bounding_box_for_current_ts = self.get_bounding_box_label_dict(msg)
                self.labels_for_current_ts = self.get_label_set_label_dict(msg)

        return self.event_list

    @staticmethod
    def calc_start_end_ts_snippet(event) -> dict:
        event["start_ts"] = int(event["ts"] * 1000)
        event["end_ts"] = int(((event["ts"] + 1) * 1000) - 1)
        event["ts"] = int(event["ts"] * 1000)
        return event