from bagfile_provider import BagFileProvider

# Test data
bag_1 = {
    "version": "1.0",
    "guid": "39a758db-7f24-4350-ae7a-d16259ecbe95",
    "link": "testdata",
    "file_name": "testdata1.bag",
    "size": 21231,
    "num_messages": 4444,
    "start": 1563262908000,
    "end": 1563262911001,
    "duration": 3001,
    "vehicle_id_num": "V-123-234",
    "topics": ["topic2"],
    "drive_types": ["CID"],
    "is_test_data": 1
}

child_1 = {
    "version": "1.0",
    "guid": "39a758db-7f24-4350-ae7a-d16259ecbe01",
    "parent_guid": "39a758db-7f24-4350-ae7a-d16259ecbe95",
    "link": "testdata",
    "file_name":
    "20200130_152349_cid_guest_drive_part2_0_EXTRACTEDBY_LogbookIssueInterface_at_20200130_152926.bag",
    "start": 1558962320000,
    "end": 1558962372000,
    "parent_index_start": 169,
    "parent_index_end": 220,
    "size": 32768,
    "vehicle_id_num": "V-123-234",
    "metadata": {
        "logbook": ""
    },
    "bva": 7,
    "is_test_data": 1
}

child_2 = {
    "version": "1.0",
    "guid": "39a758db-7f24-4350-ae7a-d16259ecbe02",
    "parent_guid": "39a758db-7f24-4350-ae7a-d16259ecbe95",
    "link": "testdata",
    "file_name": "testdata_child2.bag",
    "start": 1558962402000,
    "end": 1558962458000,
    "parent_index_start": 251,
    "parent_index_end": 306,
    "size": 65536,
    "vehicle_id_num": "V-123-234",
    "extractor_id": "ext1",
    "bva": 8,
    "is_test_data": 1
}

# use "00112233-4455-6677-8899-aabbccddeeff" while testing any Athena-bagfile
# the start-end range covers any bagfile between 9 Sep 2001 and 24 Jan 2065
bag_2 = {
    "version": "1.0",
    "guid": "00112233-4455-6677-8899-aabbccddeeff",
    "link": "testdata",
    "file_name": "testdata1.bag",
    "size": 21231,
    "num_messages": 4444,
    "start": 1000000000000,
    "end": 3000000000000,
    "duration": 3001,
    "vehicle_id_num": "V-123-234",
    "topics": ["topic2"],
    "drive_types": ["CID"],
    "is_test_data": 1
}


class SimpleBagFileProvider(BagFileProvider):

    def get_bagfiles(self):
        return [bag_1, bag_2]

    def get_child_bagfiles(self):
        return [child_1, child_2]
