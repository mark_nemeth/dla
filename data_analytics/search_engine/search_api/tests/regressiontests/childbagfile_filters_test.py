__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
import requests

BASE_URL = 'http://localhost:8889'
FILTER_TOPICS_URL = BASE_URL + '/childbagfile_filters'


class TestSearchFilters(unittest.TestCase):
    def test_get_filters(self):
        res = requests.get(url=FILTER_TOPICS_URL)
        self.assertEqual(200, res.status_code)
        res_json = res.json()['results']

        self.assertIsInstance(res_json, dict)
        self.assertIsInstance(res_json.get('duration_min_max', None), list)
        self.assertIsInstance(res_json.get('locations', None), list)
        self.assertIsInstance(res_json.get('size_min_max', None), list)
        self.assertIsInstance(res_json.get('topics', None), list)
        self.assertIsInstance(res_json.get('vehicle_ids', None), list)

