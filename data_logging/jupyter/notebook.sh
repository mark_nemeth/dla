#export RDA_JAR_PATH=/mapr/738.mbc.de/var/mdm/robodrive/libraries/rda1.1.0/jar
#export RDA_VERSION=1.1.0.0224 
#export RDA_JAR_PATH=/lhome/lucasc/development/rda1.1.0/jar
#export RDA_VERSION=1.1.0.0224 
export RDA_JAR_PATH=/mapr/738.mbc.de/var/mdm/robodrive/libraries/rda1.2.0/jar
export RDA_VERSION=1.2.0.0306
if [ -d "robodrive" ]; then
  export RDA_JAR_PATH=${PWD}/robodrive/libraries/rda1.2.0/jar
fi


cd jupyter

export SPARK_HOME=${PWD}/lib/python3.6/site-packages/pyspark
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
export PYTHONPATH=$SPARK_HOME/python/:$PYTHONPATH

source bin/activate

export PYSPARK_DRIVER_PYTHON='jupyter'
export PYSPARK_DRIVER_PYTHON_OPTS="notebook"

cd ../notebooks

pyspark --conf spark.sql.catalogImplementation=in-memory --driver-memory 25g --conf spark.sql.execution.pandas.respectSessionTimeZone=False --jars=\
${RDA_JAR_PATH}/adtf-analyzer-${RDA_VERSION}.jar,\
${RDA_JAR_PATH}/commons-${RDA_VERSION}.jar,\
${RDA_JAR_PATH}/datafusion-${RDA_VERSION}.jar,\
${RDA_JAR_PATH}/mdf4-analyzer-${RDA_VERSION}.jar,\
${RDA_JAR_PATH}/metadata-discovery-${RDA_VERSION}.jar,\
${RDA_JAR_PATH}/ros-analyzer-${RDA_VERSION}.jar

