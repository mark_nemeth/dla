import os
import sys

sys.path.insert(0,
                os.path.abspath(
                    os.path.join(os.path.dirname(__file__), '../../')))
import json

from KafkaClient.kafka_client import KafkaClient, KafkaClientError, KafkaClientException
from DANFUtils.logging import logger

if __name__ == '__main__':

    config_localhost = {
        "group.id": "test_group",
        "bootstrap.servers": "localhost:9092",
        "enable.auto.commit": False,
        "auto.offset.reset": "earliest"
    }
    # as for how to get the config_event_hub, pls contact Xiaoli or you can build the config accord to the readme.md
    with open("./config_event_hub.json") as f:
        config_event_hub = json.load(f)

    client = KafkaClient(**config_localhost)
    client.subscribe(["car", "topic2"])
        # message
    for x in range(10):
        msg = client.poll_msg(timeout=100)
        if msg is None:
            continue
        elif msg.error():
            # usually it is just a warning like: "Broker: No more messages"
            if msg.error().code() == KafkaClientError._PARTITION_EOF:
                # End of partition event
                logger.warning(msg.error())
            else:
                # Error
                logger.error(msg.error())
                raise KafkaClientException(msg.error().code())
        else:
            print(msg.value())
