import React from 'react';
import {defaultEmptyValue} from '../../Table';
import {meterToMileConverter} from '../../../utils/valueConverter';
import {isNumberValid} from '../../../utils/helperFunctions';

export default function MileDistanceEntry(props) {

  if (!isNumberValid(props.distance_in_meter)) return <span>{defaultEmptyValue}</span>;

  const formattedDistance = meterToMileConverter(props.distance_in_meter);

  return (
    <span>{formattedDistance}</span>
  )
}