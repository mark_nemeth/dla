__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""
from handlers.db_data_handler import DBDataHandler


class SyncHandler:

    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def get_bagfile_guids_to_sync_from_db(self):
        return self.db_data_handler.get_all_bagfileguids_to_sync()

    def get_bagfile_guids_to_delete_from_db(self):
        return self.db_data_handler.get_all_bagfileguids_to_delete()