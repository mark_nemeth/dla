__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from kfp import dsl

from kubeflow.flows.common import get_mount_id, \
    configure_blobstorage_output_locations, BaseFlow
from kubeflow.steps.cleanup import CleanupOp
from kubeflow.steps.common import (MOUNT_DATA_BASE_PATH,
                                   MOUNT_OUTPUT_DATA_BASE_PATH)
from kubeflow.steps.cv1_exporter import Cv1ExporterOp
from kubeflow.steps.danf_extractor import ExtractorOp
from kubeflow.steps.lidar_exporter import LidarExporterOp
from kubeflow.steps.splitter import SplitterOp
from model.paths import BlobStoragePath


class DanfAzureProcessingFlow(BaseFlow):

    NAME = "danf-processing-flow-v1"
    VERSION = "1.0"
    DESCRIPTION = "three steps processing extraction, splitting and exporting"

    @classmethod
    def create(cls,
               bagfile: dict,
               flow_run_guid: str,
               test_mode: bool):
        file_path = BlobStoragePath(bagfile['link'])
        output_storage_account, output_containers = \
            configure_blobstorage_output_locations(bagfile)
        return cls._build_pipeline(
            bagfile['guid'],
            flow_run_guid,
            file_path.storage_account,
            file_path.container,
            file_path.file,
            output_storage_account,
            output_containers,
            test_mode
        )

    @classmethod
    def _build_pipeline(cls,
                        bagfile_guid,
                        flow_run_guid,
                        input_storage_account,
                        input_container_name,
                        file_name,
                        output_storage_account,
                        output_containers,
                        test_mode):
        @dsl.pipeline(
            name=cls.NAME,
            description=cls.DESCRIPTION
        )
        def processing_pipeline():
            # initialize params
            mount_id = get_mount_id()
            input_file_path = f"{MOUNT_DATA_BASE_PATH}/{file_name}"

            cleanup_step = CleanupOp.create_for_azure(
                tasks=['update-processing-state'],
                bagfile_guid=bagfile_guid,
                flow_run_guid=flow_run_guid,
                bagfile_name=file_name,
                test_mode=test_mode
            )

            with dsl.ExitHandler(cleanup_step):

                extractor_step = ExtractorOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    mount_id=mount_id,
                    input_storage_account=input_storage_account,
                    input_container=input_container_name,
                    input_file_path=input_file_path,
                    output_storage_account=output_storage_account,
                    output_containers=output_containers,
                    test_mode=test_mode
                )

                with dsl.Condition(extractor_step.outputs['significant_metadata_exists'] == 'True'):
                    splitter_step = SplitterOp.create_azure_splitter(
                        bagfile_guid=bagfile_guid,
                        flow_run_guid=flow_run_guid,
                        mount_id=mount_id,
                        input_storage_account=input_storage_account,
                        input_container=input_container_name,
                        input_file_path=input_file_path,
                        output_storage_account=output_storage_account,
                        output_container=output_containers.get("filtered_files_container"),
                        childbagfiles_output_dir=MOUNT_OUTPUT_DATA_BASE_PATH,
                        test_mode=test_mode
                    ).after(extractor_step)

                with dsl.Condition(extractor_step.outputs['significant_metadata_exists'] == 'True'):
                    with dsl.Condition(extractor_step.outputs['bagfile_recording_date'] >= Cv1ExporterOp.CUTOFF_DATE_TS):
                        cv1_exporter_step = Cv1ExporterOp.create(
                            bagfile_guid=bagfile_guid,
                            flow_run_guid=flow_run_guid,
                            bagfile_name=file_name,
                            mount_id=mount_id,
                            storage_account=output_storage_account,
                            input_container=output_containers.get("filtered_files_container"),
                            output_container=output_containers.get("exported_files_container"),
                            test_mode=test_mode
                        ).after(splitter_step)

                with dsl.Condition(extractor_step.outputs['significant_metadata_exists'] == 'True'):
                    lidar_exporter_step = LidarExporterOp.create(
                        bagfile_guid=bagfile_guid,
                        flow_run_guid=flow_run_guid,
                        bagfile_name=file_name,
                        mount_id=mount_id,
                        storage_account=output_storage_account,
                        input_container=output_containers.get("filtered_files_container"),
                        output_container=output_containers.get("exported_files_container"),
                        test_mode=test_mode
                    ).after(splitter_step)

        return DanfAzureProcessingFlow(processing_pipeline)
