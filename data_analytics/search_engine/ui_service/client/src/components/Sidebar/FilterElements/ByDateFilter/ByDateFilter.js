import React from 'react';
import Form from 'react-bootstrap/Form';
import DatePicker from 'react-datepicker';
import {Portal} from 'react-overlays';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import {
  dateToTimestampConverter,
  timestampToDateConverter,
  timestampToShortDateStringConverter
} from '../../../../utils/valueConverter';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import './ByDateFilter.scss';

// Defined limits are From Jan, 2018 to current date
export const LIMITS = {
  'min': new Date(2018, 0, 1),
  'max': new Date()
};

export function ByDateFilter(props) {

  function onStartDateSelection(date) {
    props.filterByDate({
      startdate: dateToTimestampConverter(date),
      enddate: props.endDate
    })
  }

  function onEndDateSelection(date) {
    props.filterByDate({
      startdate: props.startDate,
      enddate: dateToTimestampConverter(date, true)
    })
  }

  function resetDateValues() {
    props.filterByDate({
      startdate: undefined,
      enddate: undefined
    })
  }

  const CalendarContainer = ({children}) => {
    return (
      <Portal container={document.getElementById('calendar-portal')}>
        {children}
      </Portal>
    )
  };

  const subLabel = "selected";
  const active = props.startDate !== undefined || props.endDate !== undefined;

  return (
    <CollapseContainer label={active ? "Date: " + subLabel : "Date"}
                       active={active}
                       resetCallback={resetDateValues}
    >
      <Form className="date-wrapper">
        <div className="from-date-wrapper">
          <DatePicker
            placeholderText="Select start date"
            selected={timestampToDateConverter(props.startDate)}
            onChange={onStartDateSelection}
            minDate={LIMITS.min}
            maxDate={props.endDate ? props.endDate : LIMITS.max}
            dateFormat="dd/MM/yyyy"
            popperContainer={CalendarContainer}
          />
        </div>
        <span className="date-divider">-</span>
        <div className="to-date-wrapper">
          <DatePicker
            placeholderText="Select end date"
            selected={timestampToDateConverter(props.endDate)}
            onChange={onEndDateSelection}
            minDate={props.startDate ? props.startDate : LIMITS.min}
            maxDate={LIMITS.max}
            dateFormat="dd/MM/yyyy"
            popperContainer={CalendarContainer}
          />
        </div>
      </Form>
    </CollapseContainer>
  );
}