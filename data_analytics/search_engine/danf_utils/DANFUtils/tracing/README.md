# DANFUtils - tracing

The DANF tracing client sends trace data to Azure Monitor using the OpenCensus Python SDK. Trace data enable tracking of the bagfile processing through the whole flow, starting from the data handler and also including individual steps in the processing flow and associated APIs such as metadata and search API.

The DANF tracing client represents a vendor-independent interface/SDK to collect tracing data, backends other than Azure Monitor (more specifically Application Insights) are possible, but not supported right now.

## Usage

Instrumenting code with tracing data is relatively straight-forward. A single trace (called span) always has a start and an end. Traces can be associated with user-defined attributes, which can carry meta-data (e.g. bagfile guid). It is also possible to nest spans, which establishes are hierarchical relationship between two spans. This should be used if a tasks consists of multiple subtasks.

### Example

If we want to instrumenting a longer running operation called 'process', which consists of the sub-tasks 'init', 'load' and 'export', we can do the following:

    from DANFTracingClient.tracing import start_task, end_task

    start_task('process', attributes={'id': id})

    start_task('init')
    ...
    end_task()

    start_task('load')
    ...
    end_task()

    start_task('export')
    ...
    end_task()

    end_task()

This will create four spans/traces in the output:

1) A span 'process' that is the parent span of all following spans and covers the whole process.
2) One span for reach of init, load and export.

The tracing client keeps track of which span is currently active, which is why `end_task` can be called without arguments. 

If a single method should be instrumented, it is also possible to use the decorator `trace_task`:

    @trace_task('init'):
    def init():
        ...

Both APIs can be used together, e.g. it possible to use `start_task`/`end_task` within a method decorated with `trace_task`.

### Application Insights instrumentation key

The instrumentation key uniquely identify the Application Insights resource the trace data is written to. It is set using the environment variable `APPINSIGHTS_INSTRUMENTATIONKEY`.

### Trace propagation

Tracing a single operation across multiple requests and processing steps requires propagation of the tracing information among theses processes. For example, if a given service A calls another service B, this hierarchical relationship should also be captured in the tracing data. This requires that service A passes its tracing information to service B, such that service B can set the span of service A as its parent.

The tracing information consists of three parameters:

* Trace ID is the ID of the whole operation (called operation ID in Application Insights). The trace ID should be the same for a single process flow across multiple components.
* Span ID is the ID of the current span. It is used to identify which parent span a new span belongs to.
* Trace options: TODO

Trace information can be propagated in two way: using environment variables or as HTTP headers.

#### Environment variables

Environment variables can used when starting subprocess or containers.

The method `get_propagation_env_vars` method can be used to get a dictionary with the three environment variables:

* DANF_TRACING_TRACE_ID
* DANF_TRACING_SPAN_ID
* DANF_TRACING_TRACE_OPTIONS

These variables should then be passed to the subprocess/container.

If these variables are present in the process environment, the tracing client will load them directly during initialization. Not code is necessary to do this.

#### HTTP headers

TODO not implemented yet