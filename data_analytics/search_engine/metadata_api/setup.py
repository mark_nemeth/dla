"""Setup file"""
from setuptools import setup, find_packages
from config.constants import VERSION

setup(name='metadata_api',
      version=VERSION,
      packages=find_packages(),
      test_suite="tests",
      install_requires=[
          'tornado>=6.0.1', 'requests>=2.21.0', 'Authlib>=0.11',
          'DANFUtils>=0.2.29', 'DBConnector>=0.4.75', 'coverage>=4.5.4',
          'pydantic>=1.2,<2.0', 'packaging>=20.1'
      ])
