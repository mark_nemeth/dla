"""Search Engine REST API caching topics route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from handlers.auth import auth
from handlers.utils import utils
from handlers.bagfiles import BagFilesHandler
from requesthandler.base_request_handler import BaseRequestHandler
from model.custom_attributes import UpdateCustomAttributes


class BagfileCustomAttributesRequestHandler(BaseRequestHandler):

    def initialize(self):
        self.schema_guid = super().load_json_schema("schema_guid")
        self.bagfile_handler = BagFilesHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'PATCH')

    @auth.requires_entitlements(["openid"])
    def patch(self, bagfile_guid):
        tracing.start_task_in_context('bagfiles/custom_attributes/patch',
                                      context_headers=self.request.headers)
        self.validate_json('{"guid":"' + str(bagfile_guid) + '"}', self.schema_guid)
        data = self.deserialize('{"custom_attributes":' + self.request.body.decode('utf-8') + '}',
                                UpdateCustomAttributes)

        # extractor name is hardcoded will be replaced in future with fetching the extractor name
        # from the request headers
        self.bagfile_handler.add_custom_attributes(bagfile_guid, 'common_values', data)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results="bagfile custom attributes is updated",
                             status_code=constants["REQ_STATUS_CODE_OK"])
