"""Handler class for feature collection"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List

from DANFUtils.utils import generate_guid
from handlers.db_data_handler import DB_Data_Handler


class FeatureBucketsHandler:
    def __init__(self):
        self.db_data_handler = DB_Data_Handler()

    def add_feature_bucket_to_db(self, feature_bucket: dict):
        """Creates a new feature bucket.

        :param feature_bucket: data of feature bucket to create
        """
        unique_key = f"{feature_bucket['bagfile_guid']}_{feature_bucket['start']}_{feature_bucket['end']}"
        guid = generate_guid(unique_url=unique_key)

        self.db_data_handler.upsert_single_feature_bucket(
            guid=guid, feature_bucket_data=feature_bucket)

        return guid

    def get_feature_bucket_from_db(self, guid: str) -> dict:
        """Retrieves a single feature bucket by guid.

        :param guid: guid identifying feature_buckets to retrieve
        :return: feature bucket metadata as dict
        """
        return self.db_data_handler.get_feature_buckets(guids=[guid])

    def update_feature_bucket_in_db(self, guid: str, data: dict):
        """Updates the feature bucket identified by the given guid.

        :param guid: guid of the feature bucket to update
        :param data: update to apply
        """
        self.db_data_handler.update_feature_buckets(guids=[guid],
                                                    feature_bucket_data=[data])

    def delete_feature_bucket_from_db(self, guid: str):
        """Deletes the feature bucket identified by the given guid.

        :param guid: guid of the feature_bucket to delete
        """
        self.db_data_handler.delete_feature_buckets(guids=[guid])

    def get_features_by_feature_bucket(self, guid: str) -> List[str]:
        """Retrieves the features of a feature bucket

        :param guid: identifies the feature bucket whose features should be retrieved
        :return: list of features
        """
        feature_bucket = self.db_data_handler.get_feature_buckets(guids=[guid])
        return feature_bucket[0].get('features', [])
