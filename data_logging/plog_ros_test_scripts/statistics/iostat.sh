#!/bin/sh

TT=`date +%j%H%M%S`
echo $TT

iostat -d 1 -m -c -x /dev/md127 /dev/nvme0n1 /dev/nvme1n1 2>&1 > iostat_$TT.log
