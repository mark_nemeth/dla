import {insertString} from './stringHelper';

describe('StringHelper function', () => {

  it('should return default string when no placeholder was provided', () => {
    const string = 'Hello World!';
    expect(insertString(string)).toBe(string);
  });

  it('should return string when with on placeholder inserted', () => {
    const string = 'Hello $1$!';
    const placeholder1 = 'Developer';
    const outString = 'Hello Developer!';
    expect(insertString(string, placeholder1)).toBe(outString);
  });

  it('should return default string when no placeholder was provided', () => {
    const string = '$2$ $1$!';
    const placeholder1 = 'Developer';
    const placeholder2 = 'Whats up';
    const outString = 'Whats up Developer!';
    expect(insertString(string, placeholder1, placeholder2)).toBe(outString);
  });

});