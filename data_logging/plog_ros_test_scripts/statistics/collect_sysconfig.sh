#!/bin/bash


tune2fs -l /dev/md127 > tune2fs.log

hdm generate-report -p /dev/nvme0 > nvme0_hdm_report.log
hdm generate-report -p /dev/nvme1 > nvme1_hdm_report.log

hdm capture-diagnostics --name nvme0diags --path /dev/nvme0
hdm capture-diagnostics --name nvme1diags --path /dev/nvme1

mdadm --detail /dev/md127 > mdadm_detail.log

cp -r /etc/sysctl.conf /etc/fstab /etc/mdadm/mdadm.conf .

cat /etc/os-release > os-release.log
uname -a > uname.log

ip a | grep inet6 > inet6.log
