import random
import json
import os

OUTPUT_FILENAME = "flowruns.json"
VERSION = "0.1"

guid = "9de06cbd-a2b8-4d14-1337-c8a8da45cfe4"
bagfile_guid = "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4"
flow_guid = "9de06cbd-a2b8-4d14-2378-c8a8da45cfe4"

flow_runs = {
    "version": "2.0",
    "guid": guid,
    "kubeflow_workflow_name": "workflow1",
    "kubeflow_workflow_id": "1337",
    "started_at": random.randint(1483228800000, 1563454684000),
    "completed_at": random.randint(1563454684000, 1563462684000),
    "status": "SUCCESSFUL",
    "reference": {"type": "bagfile_guid", "value": bagfile_guid},
    "steps": [
        {
            "name": "extractor",
            "image": "extractor_v1",
            "tag": "my_tag",
            "max_retries": "3",
            "status": "SUCCESSFUL",
            "is_essential": True,
            "runs": [
                {
                    "started_at": random.randint(1483228800000, 1563454684000),
                    "completed_at": random.randint(1563454684000, 1563462684000),
                    "status": "FAILED",
                    "report": {
                        "message": "Permission denied opening file xxx"
                    }
                },
                {
                    "started_at": random.randint(1483228800000, 1563454684000),
                    "completed_at": random.randint(1563454684000, 1563462684000),
                    "status": "SUCCESSFUL",
                }
            ]
        },
        {
            "name": "stripper",
            "image": "stripper_v1",
            "tag": "my_tag",
            "max_retries": "3",
            "status": "SUCCESSFUL",
            "is_essential": True,
            "runs": [
                {
                    "started_at": random.randint(1483228800000, 1563454684000),
                    "completed_at": random.randint(1563454684000, 1563462684000),
                    "status": "SUCCESSFUL",
                }
            ]
        }
    ]
}

result = {"results": flow_runs}

outpath = os.path.join("public/mockdata")
if not os.path.exists(outpath):
    os.makedirs(outpath)

outfilePath = os.path.join(outpath, OUTPUT_FILENAME)
with open(outfilePath, 'w') as outfile:
    json.dump(result, outfile, indent=4)

print("Mock json file creates: " + outfilePath)
