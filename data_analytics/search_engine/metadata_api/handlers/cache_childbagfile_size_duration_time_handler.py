"""Handler class for cache size duation and time """
__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DB_Data_Handler
from handlers.utils import Utils

logger = logging.getLogger(__name__)


class CacheChildbagfileSizeDurationTimeHandler:

    def __init__(self):
        self.db_data_handler = DB_Data_Handler()
        self.utils = Utils()

    def cache_childbagfile_size_duration_time(self):
        logger.info('caching the size duration and time for childbagfiles')
        guids = []
        documents = []
        min_size = self.utils.get_value_or_default(self.db_data_handler.get_min_field_value('childbagfiles', 'size'), 0)
        max_size = self.utils.get_value_or_default(self.db_data_handler.get_max_field_value('childbagfiles', 'size'),
                                                   10000000000000)
        min_duration = self.utils.get_value_or_default(
            self.db_data_handler.get_min_field_value('childbagfiles', 'duration'),
            0)
        max_duration = self.utils.get_value_or_default(
            self.db_data_handler.get_max_field_value('childbagfiles', 'duration'),
            28800000)
        min_time = self.utils.get_value_or_default(self.db_data_handler.get_min_field_value('childbagfiles', 'time'),
                                                   1514764800)
        max_time = self.utils.get_value_or_default(self.db_data_handler.get_max_field_value('childbagfiles', 'time'),
                                                   1735689600)

        logger.info(f'min & max size is {min_size} - {max_size}')
        logger.info(f'min & max duration is {min_duration} - {max_duration}')
        logger.info(f'min & max time is {min_time} - {max_time}')

        for i in ['size', 'duration', 'time']:
            cache_type = f'childbagfiles_{i}_cache'
            search_kwargs = {'query': {'type': cache_type}}
            cached_docs = self.db_data_handler.search_documents(**search_kwargs)
            if len(cached_docs) > 0:
                guid = cached_docs[0]['guid']
            else:
                guid = generate_guid()
            guids.append(guid)
            documents.append({'type': cache_type, 'min': eval(f'min_{i}'), 'max': eval(f'max_{i}')})

        self.db_data_handler.update_documents(guids, documents, True)

        return guids
