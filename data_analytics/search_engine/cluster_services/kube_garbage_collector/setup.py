"""Setup file"""
from setuptools import setup
from config.constants import VERSION

setup(
    name='kubegc',
    version=VERSION,
    install_requires=[
        'kubernetes>=10.0.0',
        'requests>=2.22.0',
        'DANFUtils>=0.2.29'
    ]
)
