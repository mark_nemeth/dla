__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

logger = logging.getLogger(__name__)


class MongoDBHandler:

    def create_field_query(self, value):
        if isinstance(value, list):
            value = {"$in": value}
        return value

    def parse_to_query(self, query_args, kwrds_func, query_func, range_func, any_query_func, sortby_func):
        # try, except parsing
        def do_parsing(k, v, funcs):
            parsed = None
            try:
                parsed = funcs[k](v)
            except ValueError:
                logger.warning(
                    f'Invalid {k} query parameter in bagfile search - integer expected'
                )
            except:
                logger.error(
                    f'unexpected error in (key,value pair): ({k}, {v})'
                )
            return parsed

        # Actual Parsing
        # correct the schema before parsing
        if 'file_path' in query_args:
            query_args['link'] = query_args.pop('file_path')

        #  Parsing functionality keywords with default values for
        default_kwargs = {
            'limit': 50,
            'offset': 0,
            'sortby': 'guid'
        }

        # apply parsing function to 'query' keywords
        query = {k: do_parsing(k, v, query_func) for k, v in query_args.items() if k in query_func}
        query = {k: v for k, v in query.items() if v}

        # apply parsing function to 'default' keywords
        keywrds = {k: do_parsing(k, v, kwrds_func) for k, v in query_args.items() if k in kwrds_func}
        keywrds = {k: v for k, v in keywrds.items() if v}
        residual_keywrds = {k: v for k, v in default_kwargs.items() if k not in keywrds}
        keywrds = {**keywrds, **residual_keywrds}

        # apply parsing function to '$and' keywords
        search_range = [do_parsing(k, v, range_func) for k, v in query_args.items() if k in range_func]
        search_range = [v for v in search_range if v]
        search_range = {'$and': search_range} if len(search_range) > 0 else {}

        # apply parsing function to '$or' keywords
        search_any = [do_parsing(k, v, any_query_func) for k, v in query_args.items() if k in any_query_func]
        search_any = [v for sublist in search_any for v in sublist if v]
        search_any = {'$or': search_any} if len(search_any) > 0 else {}

        # apply parsing function to 'sortby' keywords
        sortby_kwrd = {k: do_parsing(k, v, sortby_func) for k, v in query_args.items() if k in sortby_func}
        sortby_kwrd = {k: v for k, v in sortby_kwrd.items() if v}
        sort_default = {k: v for k, v in sortby_kwrd.items() if k not in sortby_kwrd}
        sortby_kwrd = {**sortby_kwrd, **sort_default}

        return {
            'query': {
                **query,       # keywords with value
                **search_range, # keywords with range
                **search_any # keywords with 'any'
            },
            **keywrds,     # keywords with default
            **sortby_kwrd # keywords ascending or descending
        }

    def sortby_key_value(self, keys, whitelist):
        whitelistp = list(map(lambda x: '+' + str(x), whitelist))
        whitelistn = list(map(lambda x: '-' + str(x), whitelist))

        # Ascending order
        if keys[0] in whitelist:
            key_value = keys[0]
        elif keys[0] in whitelistp:
            key_value = keys[0][1:]
        # Descending order
        elif keys[0] in whitelistn:
            key_value = [(keys[0][1:], -1)]
        else:
            key_value = None

        return key_value
