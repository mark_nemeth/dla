import { createAction } from '@reduxjs/toolkit';
import axios from 'axios';
import configHandler from '../../config/configHandler';
import { FileTypes } from '../../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import strings from '../../assets/string/default';
import { insertString } from '../../assets/string/stringHelper';
import { isObjectValid } from '../../utils/helperFunctions';

export const fetchFileByBagPathBegin = createAction('overview/fetchFileByBagPathBegin');
export const fetchFileByBagPathSuccess = createAction('overview/fetchFileByBagPathSuccess');
export const fetchFileByBagPathError = createAction('overview/fetchFileByBagPathError');

export function fetchFileByBagPath(bagPath) {

  return (dispatch, getState) => {

    if (getState().httpRequests.fetchFileByBagPathRequest.ongoing === true) {
      return;
    }

    dispatch(fetchFileByBagPathBegin());

    axios.request(configHandler.getLocationAPI({ bagPath: bagPath }))
      .then(res => {
        let file = res.data;
        dispatch(fetchFileByBagPathSuccess(file))
      })
      .catch(error => {
        dispatch(fetchFileByBagPathError(error))
      });
  }
}

//region Fetch file action
export const fetchFileByGuidSuccess = createAction('overview/fetchFileByGuidSuccess');
export const fetchFileByGuidError = createAction('overview/fetchFileByGuidError');
export const clearOverviewBagfile = createAction('overview/clearOverviewBagfile');
export const loadMap = createAction('overview/loadMap');
export const loadMapError = createAction('overview/loadMapError');
export const cleanMapComponent = createAction('overview/cleanMapComponent')

export function translateData(feat_bucket_doc) {
  let tsstruct = []
  for (let j = 0, tslen = feat_bucket_doc.ts.length; j < tslen; j++) {
    tsstruct.push({ "ts": feat_bucket_doc.ts[j], "loc": feat_bucket_doc.loc[j] })
  }
  tsstruct.sort(function (f, s) {
    return f['ts'] - s['ts']
  })
  let filteredcoords = []
  tsstruct.map(elem => {
    if (elem.loc !== null) {
      let temp = [elem.loc.coordinates[1],elem.loc.coordinates[0]]
      filteredcoords.push(temp)
    }
  })
  if (filteredcoords.length === 0) {
    filteredcoords.push('Null')
  }
  return filteredcoords
}
export function getGPSdata(bagfile_guid) {
  return (dispatch, getState) => {
    let coords = []
    axios.request(configHandler.getFilterFeaturesAPI({ "bagfile_guid": bagfile_guid, features: ["loc"] }))
      .then(res => {
        coords = translateData(res.data.results.features)
        dispatch(loadMap(coords))
      }
      )
      .catch(error => {
        dispatch(loadMapError(['Error']))
      });
  }
}

export function cleanMapComponentData(){
  return (dispatch, getState) => {
    dispatch(cleanMapComponent())
  }
}

export function fetchFileByGuid(guid, fileType) {
  return (dispatch, getState) => {
    dispatch(clearOverviewBagfile());
    dispatch(clearSelectedDriveItem());
    if (fileType === FileTypes.ORIGINALFILE || fileType === FileTypes.ALWAYSONFILE) {
      return fetchBagfileByGuid(guid, getState).then(bagfile =>
        fetchAllChildBagfilesForParentBagfileFromApi(guid).then(childbagfiles =>
          dispatch(fetchFileByGuidSuccess({ bagfile: bagfile, childbagfiles: childbagfiles }))
        ).catch(error => dispatch(fetchFileByGuidError(error)))
      ).catch(error => dispatch(fetchFileByGuidError(error)));
    } else if (fileType === FileTypes.DRIVE) {
      return fetchDriveByGuid(guid, getState).then(drive =>
        dispatch(fetchFileByGuidSuccess({ bagfile: drive, childbagfiles: [] }))
      ).catch(error => dispatch(fetchFileByGuidError(error)));
    } else {
      return fetchChildbagfileByGuid(guid, getState).then(childbagfile =>
        fetchBagfileByGuid(childbagfile.parent_guid, getState).then(bagfile =>
          dispatch(fetchFileByGuidSuccess({ bagfile: bagfile, childbagfiles: [childbagfile] }))
        ).catch(error => dispatch(fetchFileByGuidError(error)))
      ).catch(error => dispatch(fetchFileByGuidError(error)));
    }
  }
}

//endregion


//region Fetch single bagfile actions
function fetchBagfileByGuid(guidOfBagfile, getState) {
  // Check if bagfiles are already fetched
  if (!isObjectValid(getState().currentFiles)) return loadBagfileFromApi(guidOfBagfile);
  // Check if store contains bagfile with guid
  const bagfileWithGuid = getState().currentFiles.find(bag => bag.guid === guidOfBagfile);
  if (!isObjectValid(bagfileWithGuid)) return loadBagfileFromApi(guidOfBagfile);
  // Load bagfile from local store
  else return Promise.resolve(bagfileWithGuid);

  function loadBagfileFromApi(guidOfBagfile) {
    return axios.request(configHandler.getBagfileAPI({ guid: guidOfBagfile }))
      .then(res => {
        if (res.data.results.bagfiles.length === 1) {
          return Promise.resolve(res.data.results.bagfiles[0]);
        } else {
          return Promise.reject(insertString(strings.no_bagfile_found, guidOfBagfile));
        }
      })
      .catch(err => {
        console.log(err);
        return Promise.reject(insertString(strings.no_bagfile_error, guidOfBagfile));
      });
  }
}

//endregion


//region Fetch single drives actions
export function fetchDriveByGuid(guidOfDrive, getState) {
  // Check if drive file is already fetched
  if (!isObjectValid(getState().drives)) return loadDriveFromApi(guidOfDrive);
  // Check if store contains drive file with guid
  const driveWithGuid = getState().drives.find(drive => drive.guid === guidOfDrive);
  if (!isObjectValid(driveWithGuid)) return loadDriveFromApi(guidOfDrive);
  // Load drive file from local store
  else return Promise.resolve(driveWithGuid);

  function loadDriveFromApi(guidOfDrive) {
    return axios.request(configHandler.getDriveAPI({ guid: guidOfDrive }))
      .catch(err => {
        console.log(err);
        return Promise.reject(insertString(strings.no_drives_error, guidOfDrive));
      })
      .then(res => {
        if (res.data.results.drives.length === 1) {
          return Promise.resolve(res.data.results.drives[0]);
        } else {
          return Promise.reject(insertString(strings.no_drive_found, guidOfDrive));
        }
      })
  }
}

//endregion

//region Fetch single childbagfiles actions
export function fetchChildbagfileByGuid(guidOfChildbagfile, getState) {
  // Check if childbagfile is already fetched
  if (!isObjectValid(getState().childBagfiles)) return loadChildbagfileFromApi(guidOfChildbagfile);
  // Check if store contains childbagfile with guid
  const childbagfileWithGuid = getState().childBagfiles.find(childbag => childbag.guid === guidOfChildbagfile);
  if (!isObjectValid(childbagfileWithGuid)) return loadChildbagfileFromApi(guidOfChildbagfile);
  // Load childbagfile from local store
  else return Promise.resolve(childbagfileWithGuid);

  function loadChildbagfileFromApi(guidOfChildbagfile) {
    return axios.request(configHandler.getChildBagfileAPI({ guid: guidOfChildbagfile }))
      .catch(err => {
        console.log(err);
        return Promise.reject(insertString(strings.no_childbagfile_error, guidOfChildbagfile));
      })
      .then(res => {
        if (res.data.results.childBagfiles.length === 1) {
          return Promise.resolve(res.data.results.childBagfiles[0]);
        } else {
          return Promise.reject(insertString(strings.no_childbagfile_found, guidOfChildbagfile));
        }
      })
  }
}

//endregion

//region Fetch all childbagfiles for bagfile actions
export function fetchAllChildBagfilesForParentBagfileFromApi(guidOfParentBagfile) {
  return axios.request(configHandler.getChildBagfileAPI({ parent_guid: guidOfParentBagfile }))
    .catch(err => {
      console.log(err);
      return Promise.reject(insertString(strings.no_childbagfiles_error, guidOfParentBagfile));
    })
    .then(res => {
      if (res.data.results.childBagfiles) {
        return Promise.resolve(res.data.results.childBagfiles);
      } else {
        return Promise.reject(insertString(strings.no_childbagfiles_found, guidOfParentBagfile));
      }
    })
}

//endregion


//region Load flow runs actions
export const fetchFlowRunsSuccess = createAction('overview/fetchFlowRunsSuccess');
export const fetchFlowRunsError = createAction('overview/fetchFlowRunsError');

export function loadFlowRunsFromApi(guidOfBagfile) {
  return (dispatch) => {
    return axios.request(configHandler.getFlowRunsAPI({ bagfile_guid: guidOfBagfile }))
      .then(res => {
        if (res.data.results) {
          dispatch(fetchFlowRunsSuccess(res.data.results));
        } else {
          dispatch(fetchFlowRunsError());
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(fetchFlowRunsError());
      });
  }
}

//endregion

//region selection hovering childbagfile
export const highlightSelectedChildBagfile = createAction('overview/setSelectedChildbagfile');
export const highlightHoveringChildBagfile = createAction('overview/setHoveringChildbagfile');
//endregion

//region selection hovering event
export const highlightSelectedEvent = createAction('overview/setSelectedEvent');
export const highlightHoveringEvent = createAction('overview/setHoveringEvent');
//endregion

//region selection hovering event
export const showSelectedDriveItem = createAction('overview/setSelectedDriveItem');
export const clearSelectedDriveItem = createAction('overview/clearSelectedDriveItem');
//endregion