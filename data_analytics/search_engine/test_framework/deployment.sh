if [ $# -eq 0 ]
   then
     echo "version argument missing!"
   else
     docker_image_version=$2
     docker_app_name=$1

     # build docker image
     sudo docker build -t $docker_app_name:$docker_image_version . --no-cache --network host

     # push docker image to registry
     sudo docker push $docker_app_name:$docker_image_version

     sed "s/{docker_image_version}/$docker_image_version/g;s/{docker_app_name}/$docker_app_name/g" kubernetes/test_frame.yaml |sudo kubectl  apply -f -
fi
