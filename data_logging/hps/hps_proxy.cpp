#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/uio.h>
#include "common.h"
#include "aiostream.h"
#include "hps.h"

hps_statistics statistics = { 0 };

hpStreamT *hps_open(const char *uri)
{
	char type[256];
	strcpy(type, uri);
	char* path = strchr(type, ':');
	if (path)
		*(path++) = 0;

	if (strcasecmp(type, "NULL") == 0)
	{
		return (hpStreamT*)hps_null_open();
	}
	else if (strcasecmp(type, "TCP") == 0)
	{
		char* port = strchr(path, ':');
		*(port++) = 0;
		return (hpStreamT*)hps_tcp_open(path, atoi(port));
	}
	else if (strcasecmp(type, "UDP") == 0)
	{
		char* port = strchr(path, ':');
		*(port++) = 0;
		return (hpStreamT*)hps_udp_open(path, atoi(port));
	}
	else if (strcasecmp(type, "RDMA") == 0)
	{
		char* port = strchr(path, ':');
		*(port++) = 0;
		return (hpStreamT*)hps_rdma_open(path, atoi(port));
	}
	else if (strcasecmp(type, "FILE") == 0)
	{
		return (hpStreamT*)hps_aio_open(path, -1);
	}
	else
	{
		printf("Error: unsupported protocol type %s\n", type);
		return NULL;
	}
}

int hps_write_begin(hpStreamT *s, void **buffer, unsigned size)
{
	if (s == NULL)
		return -1;

	switch (s->type)
	{
	case HPS_NULL:
		return hps_null_write_begin((hpNullStreamT*)s, buffer, size);
		break;
	case HPS_AIO:
		return hps_aio_write_begin((aioStreamT*)s, buffer, size);
		break;
	case HPS_RDMA:
		return hps_rdma_write_begin((hpRdmaStreamT*)s, buffer, size);
		break;
	default:
		printf("Error: %s not implemented for HPS protocol 0x%X\n", __FUNCTION__, s->type);
		return -1;
		break;
	}
}

int hps_write_end(hpStreamT *s, unsigned size)
{
	if (s == NULL)
		return -1;

	switch (s->type)
	{
	case HPS_NULL:
		return hps_null_write_end((hpNullStreamT*)s, size);
		break;
	case HPS_AIO:
		return hps_aio_write_end((aioStreamT*)s, size);
		break;
	case HPS_RDMA:
		return hps_rdma_write_end((hpRdmaStreamT*)s, size);
		break;
	default:
		printf("Error: %s not implemented for HPS protocol 0x%X\n", __FUNCTION__, s->type);
		return -1;
		break;
	}
}

int hps_writev(hpStreamT *s, const struct iovec *iov, int count)
{
	if (s == NULL)
		return -1;

	switch (s->type)
	{
	case HPS_NULL:
		return hps_null_writev((hpNullStreamT*)s, iov, count);
		break;
	case HPS_TCP:
		return hps_tcp_writev((hpTcpStreamT*)s, iov, count);
		break;
	case HPS_UDP:
		return hps_udp_writev((hpUdpStreamT*)s, iov, count);
		break;
	case HPS_RDMA:
		return hps_rdma_writev((hpRdmaStreamT*)s, iov, count);
		break;
	case HPS_AIO:
		return hps_aio_writev((aioStreamT*)s, iov, count);
		break;
	default:
		printf("Error: %s not implemented for HPS protocol 0x%X\n", __FUNCTION__, s->type);
		return -1;
		break;
	}
}

int hps_write(hpStreamT *s, void *data, unsigned size)
{
	struct iovec iov;
	iov.iov_base = data;
	iov.iov_len = size;
	return hps_writev(s, &iov, 1);
}

int hps_close(hpStreamT *s)
{
	if (s == NULL)
		return -1;

	switch (s->type)
	{
	case HPS_NULL:
		return hps_null_close((hpNullStreamT*)s);
		break;
	case HPS_TCP:
		return hps_tcp_close((hpTcpStreamT*)s);
		break;
	case HPS_UDP:
		return hps_udp_close((hpUdpStreamT*)s);
		break;
	case HPS_RDMA:
		return hps_rdma_close((hpRdmaStreamT*)s);
		break;
	case HPS_AIO:
		return hps_aio_close((aioStreamT*)s);
		break;
	default:
		printf("Error: %s not implemented for HPS protocol 0x%X\n", __FUNCTION__, s->type);
		return -1;
		break;
	}
}

hpServerT* hps_start_server(const char *uri, const char *out)
{
	char tmp[256];
	strcpy(tmp, uri);
	char* type = tmp;
	char* name = strchr(type, ':');
	if (name)
		*(name++) = 0;

	if (strcasecmp(type, "TCP") == 0)
	{
		int port = DEFAULT_TCP_PORT;
		char* ip = name;
		if (ip)
		{
			char* sPort = strchr(ip, ':');
			if (sPort)
			{
				*(sPort++) = 0;
				port = atoi(sPort);
			}
		}
		return (hpServerT*)hps_tcp_start_server(ip, port, out);
	}
	else if (strcasecmp(type, "UDP") == 0)
	{
		int port = DEFAULT_UDP_PORT;
		char* ip = name;
		if (ip)
		{
			char* sPort = strchr(ip, ':');
			if (sPort)
			{
				*(sPort++) = 0;
				port = atoi(sPort);
			}
		}
		return (hpServerT*)hps_udp_start_server(ip, port, out);
	}
	else if (strcasecmp(type, "FILE") == 0)
	{
		char* file = name;
		return (hpServerT*)hps_write_start_server(file);
	}
	else if (strcasecmp(type, "RAW") == 0)
	{
		char* dev = name;
		return (hpServerT*)hps_raw_start_server(dev, out);
	}
	else if (strcasecmp(type, "RDMA") == 0)
	{
		int port = DEFAULT_RDMA_PORT;
		char* ip = name;
		if (ip)
		{
			char* sPort = strchr(ip, ':');
			if (sPort)
			{
				*(sPort++) = 0;
				port = atoi(sPort);
			}
		}
		return (hpServerT*)hps_rdma_start_server(ip, port, out);
	}
	else
	{
		printf("Error: unsupported protocol type %s\n", type);
		return NULL;
	}
}

int hps_close_server(hpServerT *s)
{
	if (s == NULL)
		return -1;

	switch (s->type)
	{
	case HPS_TCP:
		return hps_tcp_close_server((hpTcpServerT*)s);
		break;
	case HPS_UDP:
		return hps_udp_close_server((hpUdpServerT*)s);
		break;
	case HPS_RAW:
		return hps_raw_close_server((hpRawServerT*)s);
		break;
	case HPS_RDMA:
		return hps_rdma_close_server((hpRdmaServerT*)s);
		break;
	default:
		printf("Error: %s not implemented for HPS protocol 0x%X\n", __FUNCTION__, s->type);
		return -1;
		break;
	}
}

void hps_get_statistics(hps_statistics *stat)
{
	if (stat == NULL)
		return;
	statistics.t = getTimestampNs();
	memcpy(stat, &statistics, sizeof(statistics));
}
