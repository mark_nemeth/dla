#include <chrono>
#include <map>
#include <getopt.h>

#include "ros/ros.h"
#include "dla/TestMsg.h"

using namespace std;
using namespace std::chrono;

struct header
{
	uint64_t id;
};

// Settings
long long runTimeMs = 10000;
static bool dump = false;
int topicStartIdx = 0;
int topicCount = 250;

const int headerSize = (4 + 8 + 4) + sizeof(dla::TestMsg::_id_type) + 4;

struct RxContext
{
	uint64_t count = 0;
	uint32_t lastSequenceCounter = -1;
	ros::Time t;
};

static high_resolution_clock::time_point start;
static long long rxMessageCount = 0;
static long long rxBytes = 0;
static long long rxErrors = 0;
map<uint16_t, RxContext> RxContexts;

void arrayCallback(const dla::TestMsg::ConstPtr& msg)
{
	RxContext &ctx = RxContexts[msg->id];
	size_t msgSize = headerSize + msg->data.size();

	if (ctx.count == 0)
	{
		if (dump)
		{
			printf("Msg size = %lu, ID = %u, SC = %u, connections = %lu\n",
					msgSize, msg->id, msg->header.seq, RxContexts.size());
		}
	}
	else
	{
		uint32_t seq = (uint32_t)(ctx.lastSequenceCounter + 1);
		if (seq != msg->header.seq)
		{
			printf("Error: expected SC %u, received SC %u\n", seq, msg->header.seq);
			rxErrors++;
		}
	}

	ctx.count++;
	ctx.lastSequenceCounter = msg->header.seq;
	ctx.t = msg->header.stamp;

	rxMessageCount++;
	rxBytes += msgSize;

	// Statistics
	if (rxMessageCount % 32 == 0)
	{
		high_resolution_clock::time_point t = high_resolution_clock::now();
		long long dt = duration_cast<milliseconds>(t - start).count();
		if (1000 < dt)
		{
			double MPS = rxMessageCount * 1000 / dt;
			double MBPS = rxBytes * 1000 / dt / 1024 / 1024;
			printf("%.2f messages/s %.2f MB/s, %llu errors\n", MPS, MBPS, rxErrors);
			start = t;
			rxMessageCount = 0;
			rxBytes = 0;
		}
	}
}

void help()
{
	printf("rx [options]\n");
	printf("Options:\n");
	printf(" -s,--start <start index>\n");
	printf(" -c,--count <count>\n");
	printf(" -v\n");
}

int main(int argc, char **argv)
{
	// Parse options
    int idx = 1;
    while (idx < argc)
    {
        if (strcmp(argv[idx], "-t") == 0 || strcmp(argv[idx], "--time") == 0)
        {
        	runTimeMs = atoll(argv[++idx]);
        }
        else if (strcmp(argv[idx], "-s") == 0 || strcmp(argv[idx], "--start") == 0)
        {
        	topicStartIdx = atoi(argv[++idx]);
        }
        else if (strcmp(argv[idx], "-c") == 0 || strcmp(argv[idx], "--count") == 0)
        {
        	topicCount = atoi(argv[++idx]);
        }
        else if (strcmp(argv[idx], "-v") == 0)
        {
        	dump = true;
        }
        else
        {
        	help();
        	return 0;
        }

        idx++;
    }

	ros::init(argc, argv, "rx", ros::init_options::AnonymousName);
	ros::NodeHandle n;

	start = high_resolution_clock::now();

	vector<ros::Subscriber> subscribers;
	for (int i = topicStartIdx; i < topicStartIdx + topicCount; i++)
	{
		char topicName[256];
		sprintf(topicName, "test_%03u", i);

		printf("Subscribe topic: %s\n", topicName);
		ros::Subscriber sub = n.subscribe(string(topicName), 1000, arrayCallback);
		subscribers.push_back(sub);
	}

	while (ros::ok())
	{
		// Check run time timeout
		high_resolution_clock::time_point t = high_resolution_clock::now();
		long long dt = duration_cast<milliseconds>(t - start).count();
		if (runTimeMs < dt)
			break;

		ros::spinOnce();
	}

	if (ros::isStarted())
	{
		printf("Shutdown\n");
		ros::shutdown();
		ros::waitForShutdown();
	}

	return 0;
}
