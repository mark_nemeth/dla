import React, {useEffect, useState} from 'react';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import './CollapseContainer.scss'

function CollapseContainer(props) {

  const [extend, setExtend] = useState(props.preexpanded === undefined ? false : props.preexpanded);

  useEffect(() => {
    setExtend(props.preexpanded === undefined ? false : props.preexpanded)
  }, [props.preexpanded])

  function toggleCollapseView() {
    setExtend(!extend);
  }

  const headerClass = `${extend ? "collapse-header-extend" : ""} ${props.useBodyStyle ? "collapse-header-body" : props.active ? "collapse-header-set" : "collapse-header-unset"} collapse-header`;

  return (
      <Accordion className="collapse-accordion-container" defaultActiveKey={extend ? "0" : ""}>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="0" onClick={toggleCollapseView} className={headerClass}>
            <div className="collapse-header-title">{props.label}</div>
            <div className="collapse-header-icon">
              {props.active && <div className="icon-bin-1" onClick={props.resetCallback}/>}
              <div className={extend ? "icon-arrow-left-1" : "icon-arrow-down-1"}/>
            </div>
          </Accordion.Toggle>
          <Accordion.Collapse
              eventKey="0"
              className={"collapse-body " + (props.variant === undefined ? "collapse-body-normal" : "collapse-body-" + props.variant)}>
            {props.children}
          </Accordion.Collapse>
        </Card>
      </Accordion>
  );
}

export default CollapseContainer;