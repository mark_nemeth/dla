import random
import uuid
import json
import os
from generate_mock_filters import *

NUMBER_OF_METADATA = 100
OUTPUT_FILENAME = "bagfiles.json"
VERSION = "0.1"

extractor_id_list = ["ext1", "ext2", "ext3"]

data = []
for i in range(0, NUMBER_OF_METADATA):
    start_time = random.randint(1483228800000, 1563454684000)
    duration_time = random.randint(600000, 8000000)
    end_time = start_time + duration_time
    file_name = "/" + str(random.randint(10000000, 999999999)) + "_mock_bagfile_test.bag"

    data.append({
        "_id": uuid.uuid4().hex,
        "file_name": file_name,
        "link": "/input/plog/WDD2221591A" + str(random.randint(0, 999999)) + "/2019/07/" + str(random.randint(0, 30)) + file_name,
        "size": random.randint(10000000, 999999999),
        "num_messages": random.randint(10000, 9999999),
        "start": start_time,
        "end": end_time,
        "duration": duration_time,
        "vehicle_id_num": random.choice(filters_info['vehicle_ids']),
        "extractor": random.choice(extractor_id_list),
        "topics": random.choices(filters_info['topics'], k=random.randint(1, 20)),
        "version": VERSION,
        "guid": str(uuid.uuid4())
    })

result = {"results": data, "total": len(data)}

outpath = os.path.join("public/mockdata")
if not os.path.exists(outpath):
    os.makedirs(outpath)

outfilePath = os.path.join(outpath, OUTPUT_FILENAME)
with open(outfilePath, 'w') as outfile:
    json.dump(result, outfile, indent=4)

print("Mock json file created: " + outfilePath)
