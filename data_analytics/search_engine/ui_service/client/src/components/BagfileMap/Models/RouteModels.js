// Note: The distance calculations here assume that only small distances are covered, i.e. it's errror grows for large differences in longitude/ latitude of the points
// Note: inherently it is assumed that the the triangle created by the coordinates lives in a euclidean geometry
// Note: If the childBagfiles are not directly connected, the total distance can be greater than the sum of the individual childBagfiles

// earth's average radius in km 
const EARTH_RADIUS_KM = 6378.14;

export class RouteData extends Array {
    constructor(obj) {
        // convert data to class hierarchy
        let list = [];
        
        // Fix for no metadata entry for file_name in childbagfiles 
        const fileNames = obj.map((item) => item.file_name || null)
        if (fileNames.indexOf(null) === -1) { 
          obj.forEach(el => {
              list.push(new ChildBagfile(el))
          });
        }
        // sort by start time
        list = list.sort((a, b) => { return a.start - b.start });
        super(...list);
    }

    isValid() {
        return (this.length > 0);
    }

    /**
     * TODO: Adjust all methods to actual data model
     */
    getEventsPerChildBagfile() {
        if (!this.isValid()) { return []; }
        let events = [];
        for (let childBagfile of this) {
            events.push(childBagfile.metadata);
        }
        return events;
    }

    getAllEventsFlat() {
        if (!this.isValid()) { return []; }
        let allEvents = [];
        for (let childBagfile of this.childBagfiles) {
            allEvents = allEvents.concat(childBagfile.events);
        }
        return allEvents;
    }

    getChildBagfileLengthsInKm() {
        if (!this.isValid()) { return []; }
        let lengths = [];
        this.forEach(childBagfile => {
            lengths.push(childBagfile.getLengthInKm());
        });
        return lengths;
    }

    getRouteLengthInKm() {
        if (!this.isValid()) { return 0; }
        let length = 0;
        this.forEach(childBagfile => {
            length += childBagfile.getLengthInKm();
        });
        return length;
    }

    getRouteTimeInterval() {
        let allMinMax = []
        for (let childBagfile of this) {
            let interval = childBagfile.getCoveredTimeInterval();
            allMinMax.push(interval.start);
            allMinMax.push(interval.end);
        }
        allMinMax.sort((a, b) => { return a - b });
        return new TimeInterval(allMinMax[0], allMinMax[allMinMax.length - 1]);
    }

    getRouteDurationInMS() {
        if (!this.isValid()) { return 0; }
        let duration = this[this.length - 1].end - this[0].start;
        return duration;
    }

    getDurationCoveredByChildBagfiles() {
        let coveredDuration = 0;
        let childBagfileDurationsInMS = this.getChildBagfileDurationsInMS();
        for (let duration of childBagfileDurationsInMS) {
            coveredDuration += duration;
        }
        return coveredDuration;
    }

    getChildBagfileDurationsInMS() {
        let childBagfileTimeIntervals = []
        for (let childbagFile of this) {
            childBagfileTimeIntervals.push(childbagFile.getDurationlInMS());
        }
        return childBagfileTimeIntervals;
    }

    getEventTimes() {
        if (!this.isValid()) { return []; }

        let eventTimes = [];

        for (let childBagfile of this) {
            for (let event of childBagfile.events) {
                eventTimes.push(event.timestamp);
            }
        }

        eventTimes.sort((a, b) => { return a - b });
        return eventTimes;
    }

    getDurationsFromRouteStartToEventInMS() {
        if (!this.isValid()) { return []; }
        let eventDurationsInMS = [];
        let startTimestamp = this.getRouteTimeInterval().start;

        for (let childBagfile of this) {
            let events = childBagfile.events;
            for (let event of events) {
                let eventDurationInMS = event.timestamp - startTimestamp;
                eventDurationsInMS.push(eventDurationInMS);
            }
        }
        return eventDurationsInMS;
    }

    getDurationsFromChildBagfileStartToEventInMs() {
        if (!this.isValid()) { return []; }

        let childBagfileDurations = [];
        let childBagfileDurationOffsetInMS = 0;
        let useOffsetIdx = -1;
        let childBagfileDurationsInMS = this.getChildBagfileDurationsInMS();
        for (let childBagfile of this) {
            let eventDurations = [];
            for (let event of childBagfile.events) {
                let eventDuration;
                if (useOffsetIdx < 0) {
                    eventDuration = event.timestamp - childBagfile.getCoveredTimeInterval().start;
                    eventDurations.push(eventDuration);
                } else {
                    let delta = event.timestamp - childBagfile.getCoveredTimeInterval().start;
                    eventDuration = childBagfileDurationOffsetInMS + delta;
                    eventDurations.push(eventDuration);
                }
            }
            useOffsetIdx++;
            childBagfileDurationOffsetInMS += childBagfileDurationsInMS[useOffsetIdx];
            childBagfileDurations.push(eventDurations);
        }
        return childBagfileDurations;
    }

    // TODO:  This function was created for the time displayed in the timeline, but I don't know what time exactly to display there
    getDurationsFromChildBagfileStart() {
        let childBagfiles = [];
        // TODO: adjust to new data model
        // for (let childBagFile of this.childBagfiles) {
        //     let passedMSSinceChildBagfileStart = [];
        //     let childBagfileStartTimestamp = childBagFile.getCoveredTimeInterval().start;
        //     for (let event of childBagFile.events) {
        //         let passedMS = childBagfileStartTimestamp - event.timestamp;
        //         passedMSSinceChildBagfileStart.push(passedMS);
        //     }
        //     childBagfiles.push(passedMSSinceChildBagfileStart);
        // }
        return childBagfiles;
    }

    // TODO: finish, fix offset ...
    getAllMinutesSecondsString() {
        if (!this.isValid()) { return []; }
        let allStrings = [];
        for (let childBagfile of this) {
          let startDate = new Date(childBagfile.start);
          let endDate = new Date(childBagfile.end);
          let startHours = ("0" + startDate.getUTCHours()).slice(-2);
          let startMin = ("0" + startDate.getUTCMinutes()).slice(-2);
          let startString = `${startHours}:${startMin}`;
          let endHours = ("0" + endDate.getUTCHours()).slice(-2);
          let endMin = ("0" + endDate.getUTCMinutes()).slice(-2);
          let endString = `${endHours}:${endMin}`;
          let currentStrings = [startString, endString];
          allStrings.push(currentStrings);
        }
        return allStrings;
    }
}

export class ChildBagfile {
    constructor(obj) {
        Object.assign(this, obj)
        let events = [];
        let entries = Object.entries(obj.metadata);
        entries.forEach(entry => {
            events.push(new ChildBagfileEvent(entry[0], Object.keys(entry[1])[0], Object.values(entry[1])[0][0].ts, obj.file_name));
        });
        this.events = events;
    }

    isValid() {
        return (!(this.gps_track === undefined || this.gps_track.coordinates.length === 0));
    }

    // TODO: There might be an error with the length when using the correct data due to another data model
    getLengthInKm() {
        if (!this.isValid()) { console.log('childBagfile invalid, no coordinates', this.coordinates); return 0; }
        let lastCoordinate;
        let totalDist = 0;

        for (let coordinate of this.gps_track.coordinates) {
            if (lastCoordinate) {
                let dPhi = Math.abs(Math.abs(lastCoordinate[1]) - Math.abs(coordinate[1]));
                let dLambda = Math.abs(Math.abs(lastCoordinate[0]) - Math.abs(coordinate[0]));

                let dx = 2 * Math.PI * dPhi / 360 * EARTH_RADIUS_KM;
                let dy = 2 * Math.PI * dLambda / 360 * EARTH_RADIUS_KM;
                let dk = Math.sqrt(dx * dx + dy * dy);

                totalDist += dk;
            }
            lastCoordinate = coordinate;
        }
        return totalDist;
    }

    getCoveredTimeInterval() {
        // if (!this.isValid()) { return []; }
        return new TimeInterval(this.start, this.end);
    }

    getDurationlInMS() {
        return this.end - this.start;
    }
}

export class ChildBagfileEvent {
    constructor(extractor, feature, timestamp, name) {
        this.coordinate = null;
        this.color = ColorScheme.colorEventDefault;
        this.symbol = ScaleTheme.sizeXS;
        this.extractor = extractor;
        this.feature = feature;
        this.timestamp = timestamp;
        this.childBagfileName = name
    }

    // TODO: finish function
    interpolateGeoPositionFromTimestamp(childBagfileStartTime, childBagfileEndTime, childBagfileStartCoordinate, childBagfileEndCoordinate) {
        let dPhi = Math.abs(Math.abs(childBagfileStartCoordinate[1]) - Math.abs(childBagfileEndCoordinate[1]));
        let dTheta = Math.abs(Math.abs(childBagfileStartCoordinate[0]) - Math.abs(childBagfileEndCoordinate[0]));
        let dx = 2 * Math.PI * dPhi / 360 * EARTH_RADIUS_KM;
        let dy = 2 * Math.PI * dTheta / 360 * EARTH_RADIUS_KM;
        let distance = Math.sqrt(dx * dx + dy * dy);

        let duration = childBagfileEndTime - childBagfileStartTime;
        let time = this.timestamp - childBagfileStartTime;
        let factor = duration / time;

        let eventDistance = factor * distance;
        // further from here...
    }
}

export class TimeInterval {
    constructor(start, end) {
        this.start = start;
        this.end = end;
    }
}

export class Coordinate {
    constructor(lat, lng, timestamp = Date.now()) {
        this.lat = lat;
        this.lng = lng;
    }

    getDistanceToCoordinateInKm(coordinate) {
        let dPhi = Math.abs(Math.abs(coordinate.lng) - Math.abs(this.lng));
        let dTheta = Math.abs(Math.abs(coordinate.lat) - Math.abs(this.lat));
        let dx = 2 * Math.PI * dPhi / 360 * EARTH_RADIUS_KM;
        let dy = 2 * Math.PI * dTheta / 360 * EARTH_RADIUS_KM;
        let dk = Math.sqrt(dx * dx + dy * dy);
        return dk;
    }
}

export const ScaleTheme = {
    sizeXL: 48,
    sizeL: 32,
    sizeM: 24,
    sizeS: 16,
    sizeXS: 8,
    sizeXXS: 4,
    sizeXXXS: 0.188 * 8
}

export const ColorScheme = {
    colorChildBagfileEmpty: '#e6e6e6', //  gray-500
    colorChildBagfileDefault: '#e6e6e6', // gray-500
    colorChildBagfileHover: '#79aebf', // $cyan
    colorChildBagfileSelected: '#00677f', // $petrol
    colorEventDefault: '#707070', // $gray-800
    colorEventBackground: 'c8c8c8', // $gray-600
    colorEventHover: '#79aebf', // $cyan
    colorEventSelected: '#00677f', // $petrol
    colorEventChildBagfileSelected: '#fff', // $white
    colorRed: 'ff0000',
    colorGreen: '6ea046',
    colorOrange: 'e69123'
}
