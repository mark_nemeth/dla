/*
  Here you can define helper functions that can be used across all classes
 */

export function isStringValid(str) {
  return str !== undefined && str !== null && str.length > 0;
}

export function isNumberValid(num) {
  return num !== undefined && num !== null && !Number.isNaN(num);
}

export function isObjectValid(obj) {
  if (obj === undefined) return false;
  if (obj == null) return false;
  if (Object.keys(obj).length <= 0) return false;
  if (obj.length > 0) return true;
  if (obj.length === 0) return false;
  if (typeof obj !== "object") return false;
  for (let key in obj) {
    if (hasOwnProperty.call(obj, key)) return true;
  }
  return false;
}