"""Handler class for cache duration and time """
__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DB_Data_Handler
from handlers.utils import Utils

logger = logging.getLogger(__name__)


class CacheDriveDurationTimeHandler:

    def __init__(self):
        self.db_data_handler = DB_Data_Handler()
        self.utils = Utils()

    def cache_drive_duration_time(self):
        logger.info('caching the duration and time for drives')
        guids = []
        documents = []

        min_duration = self.utils.get_value_or_default(self.db_data_handler.get_min_field_value('drives', 'duration'),
                                                       0)
        max_duration = self.utils.get_value_or_default(self.db_data_handler.get_max_field_value('drives', 'duration'),
                                                       28800000)
        min_time = self.utils.get_value_or_default(self.db_data_handler.get_min_field_value('drives', 'start'),
                                                   1514764800)
        max_time = self.utils.get_value_or_default(self.db_data_handler.get_max_field_value('drives', 'start'),
                                                   1735689600)

        logger.info(f'min & max duration is {min_duration} - {max_duration}')
        logger.info(f'min & max time is {min_time} - {max_time}')

        for type_key in ['duration', 'time']:
            cache_type = f'drives_{type_key}_cache'
            search_kwargs = {'query': {'type': cache_type}}
            cached_docs = self.db_data_handler.search_documents(**search_kwargs)
            if len(cached_docs) > 0:
                guid = cached_docs[0]['guid']
            else:
                guid = generate_guid()
            guids.append(guid)
            documents.append({'type': cache_type, 'min': eval(f'min_{type_key}'), 'max': eval(f'max_{type_key}')})

        self.db_data_handler.update_documents(guids, documents, True)

        return guids
