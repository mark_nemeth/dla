__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.logging import logger
from ContainerClient import ContainerClient
from DANFUtils.exceptions import RESTException


class RefreshCachesTask:

    def __init__(self):
        self.client = ContainerClient()

    def __call__(self):
        try:
            self.client.cache_origins()
        except RESTException as e:
            logger.exception(f"Failed to cache post request for "
                             f"caching origins. Reason: {e}")
    
        try:
            self.client.cache_bagfile_size_duration_time()
        except RESTException as e:
            logger.exception(f"Failed to cache post request for "
                             f"caching bagfile size_duration_time. Reason: {e}")

        try:
            self.client.cache_childbagfile_size_duration_time()
        except RESTException as e:
            logger.exception(f"Failed to cache post request for "
                             f"caching childbagfile  size_duration_time. Reason: {e}")
    
        try:
            self.client.cache_topics()
        except RESTException as e:
            logger.exception("Failed to cache post request for "
                             "caching topics. Reason: {e}")
    
        try:
            self.client.cache_vehicleids()
        except RESTException as e:
            logger.exception(f"Failed to cache post request for "
                             f"caching vehicleids. Reason: {e}")

        try:
            self.client.cache_tags()
        except RESTException as e:
            logger.exception(f"Failed to cache post request for "
                             f"caching bagfile tags. Reason: {e}")
