"""Abstract handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import logging
import json
import jsonschema
import subprocess
import math
from abc import ABC, abstractmethod


class AbstractHandler(ABC):

    def __init__(self, config, config_schema_path, tmt_path):
        self.tmt_path = tmt_path
        self.config = config
        self.config_schema_path = os.path.abspath(config_schema_path)
        self.__validate_config_json()

        # validate paths
        for f in self.config['input_file_paths']:
            assert os.path.isfile(f), "Input file {} not found!".format(f)
        out_folder = '/'.join(self.config['output_file_path'].split('/')[:-1])
        assert os.path.isdir(out_folder), "Output folder {} not found!".format(out_folder)

        self.validate_config_fields()

    def __validate_config_json(self):
        # load json schema
        try:
            schema = open(self.config_schema_path).read()
        except (IOError, OSError):
            logging.error('Schema file {} could not be found'.format(self.config_schema_path))
            raise
        try:
            self.config_schema = json.loads(schema)
        except ValueError:
            logging.error('Bad schema: unable to parse schema file {}'.format(self.config_schema_path))
            raise

        # validation
        try:
            jsonschema.validate(self.config, self.config_schema)
        except (jsonschema.ValidationError, jsonschema.SchemaError):
            logging.error('Bad json config: json config file does not match the schema {}'.format(self.config))
            raise

    def run(self):
        logging.info("Preparing TMT execution...")
        cmd_list = self.build_cmd_list()
        for cmd in cmd_list:
            logging.info("Executing command \"{}\"...".format(' '.join(cmd)))
            output = subprocess.check_output(cmd).decode("utf-8").split('\n')
            sub_proc = subprocess.Popen(cmd)
            sub_proc.communicate()
            sub_proc.wait()
            logging.debug(output)
        logging.info("TMT execution successful!")
        return 0

    def add_file_name_appendix(self, file_name, appendix):
        split_fn = file_name.split('.')
        return "{}_{}.{}".format('.'.join(split_fn[:-1]), appendix, split_fn[-1])

    def count_digits(self, value):
        return int(math.log10(value) + 1)

    @abstractmethod
    def validate_config_fields(self):
        pass

    @abstractmethod
    def build_cmd_list(self):
        pass
