#!/usr/bin/env python
# This script analyses iostat output.



import re
import os
import sys
import getopt
import matplotlib.pyplot as plt
import pickle

list_d = []
time =[]
tstart=0;
inputfile = ''
crop_end= False
crop_start= False
dimension = 'MB/s'
device="md127"
value= "wMB/s"
export = False
try:
	    options, remainder = getopt.getopt(
		sys.argv[1:],
		'i:s:e:mMgGd:v:p',
		['input=',
		 'starttime=',
		 'endtime='
		 ])
except getopt.GetoptError as err:
	    print('ERROR:', err)
	    sys.exit(1)



for opt, arg in options:
	    if opt in ('-i', '--input'):
		inputfile = arg
	    elif opt in ('-s', '--starttime'):
		tstart = int(arg)
		crop_start = True
	    elif opt in ('-e', '--endtime'):
		tend = int(arg)
		crop_end= True
	    elif opt == "-m":
            	dimension = 'Mb/s'
	    elif opt == "-M":
            	dimension = 'MB/s'
	    elif opt == "-g":
            	dimension = 'Gb/s'
	    elif opt == "-G":
            	dimension = 'GB/s'
	    elif opt == "-d":
		device = arg
		print device
	    elif opt == "-v":
		value=arg
		print value
	    elif opt == "-p":
 		export= True


if inputfile != '':
	print "Analysing iostat output"
	with open(inputfile, 'r') as f:
		#read the data from file
		t=0;

		bandwith_column=-1
		for line in f:
			#split the line at whitespaces
			columns = line.split()

			#if the line is empty go to the next line
			if not columns:
			   continue

			#find the column for the value
			if columns[0]== "Device" and bandwith_column==-1:
				i=0;
			   	for column in columns:
					if column==value:
						bandwith_column = i
				   		break;
					i=i+1;
			#find the device like md127
			elif columns[0]==device and bandwith_column != -1 :
			   list_d.append(float(columns[bandwith_column].replace(",", "."))) #appends column of the value
			   time.append(t)
			   t=t+1;

		#scale the data to the dimension
		if  (dimension == 'Mb/s'):
		   list_scaled =[ i*8 for i in list_d ]
		elif(dimension == 'GB/s'):
		   list_scaled =[ i/1024 for i in list_d]
		elif(dimension == 'Gb/s'):
	           list_scaled =[ i*8/1024 for i in list_d]
		else:
		   list_scaled = list_d
		list_path=os.path.dirname(inputfile)+"/"+"iostat"+".list"
		iostat=list_scaled
		with open(list_path,'w') as f:
    			pickle.dump(iostat,f)

		time_path=os.path.dirname(inputfile)+"/"+"time"+".list"
		with open(time_path,'w') as ft:
    			pickle.dump(time,ft)

		if (crop_start==False and crop_end==False):
			#plot the output
			fig = plt.figure()
			ax = fig.add_subplot(111)
			line, = ax.plot(time,list_scaled)
			fig.suptitle('iostat output', fontsize=14, fontweight='bold')
			plt.ylabel('writes to RAID in ' + dimension)
			plt.xlabel('time in s')
			plt.xlim(0,time[-1])
			if export== False:
				plt.show()

		elif (crop_start == True and crop_end==False):
			#crop the list
			list_time=list_scaled[tstart:]
			time_crop=time[tstart:]
			#plot the output
			fig = plt.figure()
			ax = fig.add_subplot(111)
			line, = ax.plot(time_crop,list_time)
			fig.suptitle('iostat output', fontsize=14, fontweight='bold')
			plt.ylabel('writes to RAID in ' + dimension)
			plt.xlabel('time in s')
			plt.xlim(tstart,time_crop[-1])
			if export== False:
				plt.show()

		elif (crop_end == True and crop_start == False):
			#crop the list
			list_time_cut=list_scaled[:(tend-tstart)]
			time_cut=time[:(tend-tstart)]
			#plot the output
			fig = plt.figure()
			ax = fig.add_subplot(111)
			line, = ax.plot(time_cut,list_time_cut)
			fig.suptitle('iostat output', fontsize=14, fontweight='bold')
			plt.ylabel('writes to RAID in '+ dimension)
			plt.xlabel('time in s')
			plt.xlim(time_cut[0],time_cut[-1])
			if export== False:
				plt.show()



		elif (crop_end == True and crop_start==True):

			#crop and do the math
			list_time=list_scaled[tstart:]
			time_crop=time[tstart:]
			list_time_cut=list_time[:(tend-tstart)]
			time_cut=time_crop[:(tend-tstart)]
			max_value = max(list_time_cut)
			min_value = min(list_time_cut)
			avg_value = sum(list_time_cut)/len(list_time_cut)


			#plot the stuff
			fig = plt.figure()
			fig.suptitle('iostat output', fontsize=20, fontweight='bold')
			ax = fig.add_subplot(111)
			fig.subplots_adjust(top=0.89)
			ax.set_title('min=' + '{0:.2f}'.format(min_value) + ' max=' + '{0:.2f}'.format(max_value) + ' avg=' + '{0:.2f}'.format(avg_value) + ' (' +dimension+')' ,fontsize=12)
			line, = ax.plot(time_cut,list_time_cut)
			plt.ylabel('writes to RAID in '+ dimension)
			plt.xlabel('time in s')
			plt.xlim(time_cut[0],time_cut[-1])
			plt.ylim(0)
			if export== False:
				plt.show()
		if export == True:
			fig.savefig(os.path.dirname(inputfile)+"/iostat.png", bbox_inches='tight')

else:
	print("Usage: ./analyse_iostat.py -i iostat_3401220.log")
	print("If you want to crop the plot you should use:")
	print("Usage: ./analyse_iostat.py -i iostat_3401220.log -s startime -e endtime")
	print("If you want to scale the data to MB/s Mb/s GB/s Gb/s")
	print("Add one of the parameters: -M -m -G -g")
	print("If your device has not the name 'md127'	 use -d deviceName ")
	print("If you would change the value from 'wMB/s' use -v value ")
	print("If you want to print the plot direct to a png file add the parameter -p")
