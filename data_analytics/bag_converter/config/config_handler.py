__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

__doc__ = '''
This implementation is following the singloton design pattern
https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html
Handler class for reading settings_files files
'''

import json
import os


class ConfigHandler:
    __configuration = None

    def __init__(self):
        """ Virtually private constructor. """
        if ConfigHandler.__configuration is None:
            raise Exception("This class is a singleton!, please call getConfiguration")

    @classmethod
    def getHandler(self):
        """ Static access method. """
        if ConfigHandler.__configuration is None:
            mode = os.getenv("APP_ENV", "local")
            if mode not in ["local", "prod"]:
                mode = "local"
            try:
                path = os.path.abspath(os.path.dirname(__file__))
                conf_file = f"{path}/settings_files/app_env/config_{mode}.json"
                print(f'loading {conf_file}')
                with open(file=conf_file, mode="r", encoding='utf-8', errors='ignore') as f:
                    ConfigHandler.__configuration = json.load(f)
            except Exception as e:
                print(f"{conf_file} is missing or incorrect! Did you provide one?")
                print(e)
                raise ValueError('Configs_json are missing! Crash app!')
        return self

    @staticmethod
    def set_value(key, value):
        ConfigHandler.__configuration[key] = value

    @staticmethod
    def get_value(key, default=None):
        return_value = default
        try:
            return_value = ConfigHandler.__configuration[key]
        except:
            ConfigHandler.__configuration[key] = return_value
        return return_value

    @staticmethod
    def get_all_configurations():
        return ConfigHandler.__configuration


def get_value(key, default=None):
    ch = ConfigHandler.getHandler()
    return ch.get_value(key=key, default=default)


def set_value(key, value):
    ch = ConfigHandler.getHandler()
    ch.set_value(key=key, value=value)
