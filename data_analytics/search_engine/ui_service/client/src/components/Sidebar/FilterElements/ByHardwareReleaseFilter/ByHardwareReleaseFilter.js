import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import './ByHardwareReleaseFilter.scss';

export function ByHardwareReleaseFilter(props) {

  function onHardwareReleaseSelect(hardwareRelease) {
    if (props.selectedHardwareReleaseList.includes(hardwareRelease)) props.filterByHardwareReleases(props.selectedHardwareReleaseList.filter(t => t !== hardwareRelease));
    else props.filterByHardwareReleases(props.selectedHardwareReleaseList.concat(hardwareRelease));
  }

  function resetHardwareReleaseList() {
    props.filterByHardwareReleases([]);
  }

  const showLoading = props.hardwareReleaseList === undefined || props.hardwareReleaseList.length <= 0;
  const subLabel = props.selectedHardwareReleaseList.length + ' selected';
  const active = props.selectedHardwareReleaseList.length > 0;

  const listItemComponent = (item) => (
      <label className="hardware-release-list-item">
        <input type="checkbox" value={item}
               onChange={event => onHardwareReleaseSelect(event.target.value)}
               checked={props.selectedHardwareReleaseList.includes(item)}/>
        &nbsp; {item}
      </label>
  );

  return (
      <CollapseContainer label={active > 0 ? "Hardware Release: " + subLabel : "Hardware Release"}
                         active={active}
                         variant="list"
                         resetCallback={resetHardwareReleaseList}
      >
        <ScrollList
            showLoading={showLoading}
            itemList={props.hardwareReleaseList}
            listItemComponent={listItemComponent}
        />
      </CollapseContainer>
  );
}