"""Entry point of the application."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from fastapi import FastAPI

from . import __version__
from .api.v2 import router as api_router
from .core.config import API_VERSION
from .core.errors import (
    RHSColonOperatorError,
    RHSColonValueError,
    rhs_colon_operator_error_handler,
    rhs_colon_value_error_handler,
)
from .db.mongodb_utils import close_mongo_connection, connect_to_mongo

# Consider forbidding unknown parameters if this feature will be available
# https://github.com/tiangolo/fastapi/pull/1297
app = FastAPI(
    title="DANF API",
    description="The metadata API for the data-driven age.",
    version=__version__,
)

app.add_event_handler("startup", connect_to_mongo)
app.add_event_handler("shutdown", close_mongo_connection)

app.add_exception_handler(RHSColonOperatorError, rhs_colon_operator_error_handler)
app.add_exception_handler(RHSColonValueError, rhs_colon_value_error_handler)

app.include_router(api_router, prefix=f"/api/v{API_VERSION}")
