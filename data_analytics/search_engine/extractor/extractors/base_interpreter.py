"""Extractor"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from typing import List

from pydantic import ValidationError

from config.config_handler import ConfigHandler
from extractors.utils import timestamp2ms
from model.extracted_event import ExtractedEvent

logger = logging.getLogger(__name__)


class Interpreter:
    def __init__(self, name):
        """Initializes an Interpreter with default values for bva (0) and event_type ("filter").
        In the subclasses
        - bva: should be set to a value between 0 and 10
        - event_type: should be set to "metadata_enrichment" if applicable".
        - downsample_factor: factor for downsampling the extracted event list.
                             example: if it is set to 5, only every 5th extracted event will be submitted
        """
        self.name = name
        self.extracted_events = []
        self.event_list = []
        self.bva = 0
        self.event_type = "filter"
        self.downsample_factor = None

    def get_topics_and_handler(self, bagfile_start_ts, topics_list) -> list:
        """Get list of relevant topics for the extractor based on the recording date of the bagfile
        For each extractor, the configuration should be added to config/version.json in the existing format.
        Optionally this function can be overridden in the subclass.

        :param bagfile_start_ts: start recording time of the bagfile
        :param topics_list: list of available topics in the bagfile
        :return: topic relevant for the extractor
        """
        config = ConfigHandler()
        return config.get_relevant_topic_for_interpreter(self.name, bagfile_start_ts, topics_list)

    def feed_new_msg(self, topic, message, t) -> list:
        """Logic to process each message in the bagfile.
        The main logic of how to extract events from the bagfile should be implemented here.
        This function MUST be overridden in the sub-classes.

        :param topic: topic of the message
        :param message: raw message from the bagfile
        :param t: recording timestamp of the message (either this timestamp or the message.header timestamp can be used)
        :return: List of events. Mandatory attributes are:
                - "ts": timestamp of the event in ms (int)
                - "start_ts": start timestamp of the snippet around the event
                - "end_ts": end timestamp of the snippet
                Any additional key/value pairs can be added
        """
        return self.event_list

    def get_extracted_events(self, bagfile_guid, bagfile_start_ts, bagfile_end_ts) -> List[ExtractedEvent]:
        """Generates a list of extracted events. For each event in self.event_list, an ExtractedEvent
        object is generated according to the defined model.
        In most of the cases this function should NOT be overridden in the subclasses.

        :param bagfile_start_ts: start timestamp of the bagfile
        :param bagfile_end_ts: end timestamp of the bagfile
        :return: a list of ExtractedEvent
        """
        for event in self.event_list:
            # Make sure that the start/end time of the sequence is not over the boundary of the bagfile start/end time
            validated_start = min(max(event["start_ts"], timestamp2ms(bagfile_start_ts)), timestamp2ms(bagfile_end_ts))
            validated_end = max(min(event["end_ts"], timestamp2ms(bagfile_end_ts)), timestamp2ms(bagfile_start_ts))
            try:
                extracted_event = ExtractedEvent(bagfile_guid=bagfile_guid,
                                                 extractor_id=self.name,
                                                 ts=event["ts"],
                                                 start=validated_start,
                                                 end=validated_end,
                                                 metadata={x: event[x] for x in event
                                                           if x not in ["start_ts", "end_ts", "ts"]},
                                                 bva=self.bva,
                                                 event_type=self.event_type)
            except ValidationError as e:
                raise RuntimeError("Invalid extraction request metadata!") from e

            self.extracted_events += [extracted_event]
        logger.info(f"[{self.name}] Number of extracted events: {len(self.extracted_events)}")

        if self.downsample_factor:
            self._downsample_extracted_event_list()
        logger.info(f"[{self.name}] Number of events after downsampling: {len(self.extracted_events)}")
        return self.extracted_events

    def _downsample_extracted_event_list(self):
        """If self.downsample_factor is set to n, every n-th extracted event will be kept and submitted.
        """
        original_list = self.extracted_events
        factor = int(self.downsample_factor)
        self.extracted_events = original_list[0::factor]

