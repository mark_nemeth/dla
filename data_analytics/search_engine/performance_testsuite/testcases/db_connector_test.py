__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config.config_handler import config_dict
from locust import TaskSet, task

from utils.dbconnector_locust import DbConnectorLocust


class SearchBagfileBehavior(TaskSet):
    @task
    def query_bagfiles_unsorted(self):
        with self.client as client:
            client.search_bagfiles(query={})

    @task
    def query_bagfiles_sort_by_guid(self):
        with self.client as client:
            client.search_bagfiles(query={}, sortby='guid')

    @task
    def query_bagfiles_sort_by_start(self):
        with self.client as client:
            client.search_bagfiles(query={}, sortby='start')

    @task
    def query_bagfiles_sort_by_start(self):
        with self.client as client:
            client.search_bagfiles(query={'duration': {'$gte': 1000}}, limit=10)

    @task
    def query_bagfiles_sort_by_start(self):
        with self.client as client:
            client.search_bagfiles(
                query={"$and": [
                    {'duration': {'$gte': 1000}},
                    {'size': {'$lte': 100000}}
                ]},
                limit=10
            )


class SearchChildBagfileBehavior(TaskSet):
    @task
    def query_childbagfiles_by_non_existing_parent_guid(self):
        with self.client as client:
            client.search_bagfiles(query={'parent_guid': 'xxxxxx-xxxx-xxxx-xxxx-xxxxxxxx'})

    @task
    def query_childbagfiles_by_bva(self):
        with self.client as client:
            client.search_bagfiles(query={'bva': {'$gte': 5}})

    @task
    def query_childbagfiles_by_bva_sorted_by_guid(self):
        with self.client as client:
            client.search_bagfiles(query={'bva': {'$gte': 5}},
                                   sortby='guid')

    @task
    def query_childbagfiles_unsorted(self):
        with self.client as client:
            client.search_bagfiles()

    @task
    def query_childbagfiles_sort_by_guid(self):
        with self.client as client:
            client.search_bagfiles(sortby='guid')

    @task
    def query_childbagfiles_sort_by_start(self):
        with self.client as client:
            client.search_bagfiles(sortby='start')


class User(DbConnectorLocust):
    host = config_dict.get_value('DB_HOST')
    port = config_dict.get_value('DB_PORT')
    user = config_dict.get_value('DB_USER')
    key = config_dict.get_value('DB_KEY')

    task_set = SearchBagfileBehavior
