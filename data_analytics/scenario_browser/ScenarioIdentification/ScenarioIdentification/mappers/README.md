# Generate files required for Similarity Search
`python pivot_trajectory.py`  
`python pivot_dynobj.py`  
Files would be generated in *../Data/geom_features_all_files_timetraj_pivot.csv* and *../Data/dyn_features_pivot.csv* respectively.