# Kafka Client
# Current version : 0.0.2
## Prerequisites
* Install librdkafka
```console
sudo apt-get install librdkafka
```
or manually install, please refer to the `setup.sh`

* Install confluent-kafka
```console
pip install confluent-kafka
```

* setup tunnel when you run with EventHub locally
```console
ssh -N -L 9093:dla-event-test.servicebus.windows.net:9093  -o "ProxyCommand=nc -X connect -x localhost:3128 %h %p" -p 443 dlaadmin@52.233.189.117 -i /path/to/certs/key_rsa
```
also config in /etc/hosts
add `dla-event-test.servicebus.windows.net` to `127.0.0.1`

## Create .whl file manually (optional)
```python
python3 setup.py sdist bdist_wheel
```

## Installation
```python
pip install --upgrade dist/KafkaClient-0.0.2-py3-none-any.whl 
```


## Usage
### Run with Native broker:
set valid port and topics
* test-mode
1. launch zookeeper
> $KAFKA_PATH/bin/zookeeper-server-start.sh $KAFKA_PATH/config/zookeeper.properties
2. kafka server
> $KAFKA_PATH/bin/kafka-server-start.sh  $KAFKA_PATH/config/server.properties
3. producer
> $KAFKA_PATH/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic topic1

### Run with EventHub
In order to run the KafkaClient connecting EventHub, you may need the following part
0. open tunnel
```console
ssh -N -L 9093:dla-event-test.servicebus.windows.net:9093  -o "ProxyCommand=nc -X connect -x localhost:3128 %h %p" -p 443 dlaadmin@52.233.189.117 -i /path/to/certs/key_rsa
```
1. the config file to connect
```json
{
        "bootstrap.servers": "localhost:9093",
        "security.protocol": "SASL_SSL",
        "ssl.ca.location": "/path/to/cacert.pem", # you need to replace
        "sasl.mechanism": "PLAIN",
        "sasl.username": "$ConnectionString",
        "sasl.password": "/to/get/ConnectionStringKey", # you need to replace
        "group.id": "test_group", # needed
        "client.id": "consumer1",
        "request.timeout.ms": 9000,
        "session.timeout.ms": 9000,
        "default.topic.config": {"auto.offset.reset": "smallest"}
}
```
you need to make sure the `namespace`, `topics` and other configs exist in the Event Hub.
concerning how to config the connction part, take a look at : https://github.com/Azure/azure-event-hubs-for-kafka/tree/master/quickstart/python

2. CA Certificates
to download cacert
```commandline
 wget https://curl.haxx.se/ca/cacert.pem
```

3. sasl.password
you can find in the Microsoft Azure Portal

# reference
https://curl.haxx.se/docs/caextract.html
