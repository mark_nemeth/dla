import json
import subprocess
import requests

'''
Used for reprocess failed bagfiles.
prerequisites:
1. Use the right kubernetes context"
2. port forward is needed:
   2.2. kubectl port-forward search-api-xxxx 8445:8445
   2.3  kubectl port-forward DH-api-xxxx 8447:8447
'''


class ReprocessHandler:

    def __init__(self):
        self.reprocess_url = "https://localhost:8447/files/reprocess"
        self.search_url = "https://localhost:8445/search/bagfile"

    def get_all_failed_pods(self):
        cmd = "kubectl get pods --field-selector=status.phase=Failed  -n kubeflow | awk '{print $1}'"
        pods = subprocess.check_output(cmd, shell=True).decode("utf-8").split()[1:]
        return pods

    def check_status(self, file_name):
        query_data = dict()
        query_data['file_name'] = file_name
        res = requests.get(url=self.search_url, json=query_data, verify=False)
        res_json = res.json()['results']
        return(res_json)

    def get_pod_info(self, pod_name):
        cmd = f"kubectl get po {pod_name} -o json -n kubeflow"
        body = subprocess.check_output(cmd, shell=True).decode("utf-8")
        body = json.loads(body)
        return body

    def _get_filename_from_pod(self, po_body):
        bagfiles_name = po_body["metadata"]["labels"]["danf_filename"]
        return bagfiles_name

    def execute_(self, list_pods):
        for po in list_pods:
            body = self.get_pod_info(po)
            file_name = self._get_filename_from_pod(body)
            print(file_name)
            res = self.check_status(file_name=file_name)
            if "FAILED" in str(res):
                file_path = res[0].get("link")
                print(file_path)
                self.execute_reprocess([file_path])
            else:
                print("okay! delete the legacy pods")
                self.delete_po(po)

    def delete_po(self, po_name):
        cmd = f"kubectl delete po {po_name} -n kubeflow"
        body = subprocess.check_output(cmd, shell=True).decode("utf-8")
        print(body)

    def execute_reprocess(self, file_paths):
        print("reprocess!")
        query_data = dict()
        query_data['file_paths'] = file_paths
        res = requests.post(url=self.reprocess_url, json=query_data, verify=False)
        res_json = res.json()['results']
        print(res_json)

    def get_context(self):
        cmd = f"kubectl config current-context"
        body = subprocess.check_output(cmd, shell=True).decode("utf-8")
        return body


if __name__ == '__main__':
    RH = ReprocessHandler()
    context = RH.get_context()
    arg = input(f"Is the current context {context} Input: Y/N ")
    if arg in ["y", "Y"]:
        print("Start the reprocess!")
        list_po = RH.get_all_failed_pods()
        RH.execute_(list_po)
    else:
        print("Pls switch to the right context and start again")
