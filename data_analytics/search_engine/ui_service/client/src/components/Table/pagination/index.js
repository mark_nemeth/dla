import {connect} from 'react-redux';
import {TablePagination} from './TablePagination';
import {setPaginationOffset, filterWithPagination} from '../../../redux/Home/HomeActions';

function mapStateToProps(state, ownProps) {
  const maxVisiblePages = 9;
  const total = state.total || 50;
  const limit = state.appliedFilters.limit || 50;
  const offset = state.appliedFilters.offset || 0;
  return {maxVisiblePages, total, limit, offset};
}

export default connect(
  mapStateToProps,
  {setPaginationOffset, filterWithPagination}
)(TablePagination);