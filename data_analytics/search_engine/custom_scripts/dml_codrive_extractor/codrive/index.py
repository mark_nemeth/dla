'''Extractor'''

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import rospy
import rosbag
import math
import yaml

from ContainerClient import ContainerClient
from DANFUtils.logging import logger


SNIPPET_LENGTH_SECS = 3


class ATPDMLCodriveExtractor:

    CO_DRIVER_EVENT = os.environ.get('CO_DRIVER_EVENT', default="/events")
    EXTRACTOR_VERSION = os.environ['EXTRACTOR_VERSION']
    EXTRACTOR_NAME = os.environ['EXTRACTOR_NAME']
    BAGFILE_RESPONSIBLE = os.environ['BAGFILE_RESPONSIBLE']
    BAGFILE_SKIP_RESPONSIBLE = "BAGFILE_SKIP_RESPONSIBLE"
    events_batch = 100

    def __init__(self, client, snippet_length_secs, path):
        self.client = client
        self.snippet_length = snippet_length_secs
        self.bag = self._get_bag_object(path)
        self.vin = "unknown"

        # get needed metadata (start time, end time, list of topics)
        try:
            self.start_ts, self.end_ts, self.messages, self.duration, self.size, self.topics_list = self._get_metadata()
        except Exception as exp:
            logger.error(f"Cannot get metadata of the bag file! file: {path}")
            raise exp

    def _get_bag_object(self, path):
        return rosbag.Bag(path, 'r')

    def _generate_bagfile_data(self):
        return {
            "size": self.size,
            "num_messages": self.messages,
            "start": timestamp2ms(self.start_ts),
            "end": timestamp2ms(self.end_ts),
            "duration": int(self.duration * 1000),
            "vehicle_id_num": self.vin,
            "extractor": ATPDMLCodriveExtractor.EXTRACTOR_NAME,
            "topics": list(set(self.topics_list)),
            # hard-code for now
            "origin": "DE"
        }

    def update_bagfile_metadata(self, guid):
        bagfile_data = self._generate_bagfile_data()
        try:
            self.client.update_bagfile(guid, bagfile_data)
        except Exception as e:
            logger.exception(f"Error updating metadata of bagfile {guid}")
            raise e
        else:
            logger.info(f"Successfully updated metadata of bagfile {guid}")

    def _get_metadata(self):
        try:
            info_dict = yaml.safe_load(self.bag._get_yaml_info())
            topics = []
            if "topics" in info_dict:
                topics = [t["topic"] for t in info_dict["topics"]]
            else:
                logger.warning("'topics' is missing in info_dict! Will return an empty list [].")

            returned_values = {}
            for key in ["start", "end", "messages", "duration", "size"]:
                returned_values[key] = 0
                if key in info_dict:
                    returned_values[key] = info_dict[key]
                else:
                    logger.warning(f"{key} is missing in info_dict! Will return default value 0.")

            return returned_values["start"], returned_values["end"], returned_values["messages"], \
                   returned_values["duration"], returned_values["size"], topics
        except Exception:
            logger.warning("Bagfile seems to be corrupt, does not contain info dict!")
            # return a dummy metadata list and proceed with execution
            return [0, 0, 0, 0, 0, []]

    # Just an example how to parse data from the bagfile
    def parse_bagfile(self, path, guid):
        bag = self.bag
        min_start = rospy.Time.from_sec(bag.get_start_time())
        max_end = rospy.Time.from_sec(bag.get_end_time())

        # check if bagfile contains valid DML events
        topic_info = bag.get_type_and_topic_info(ATPDMLCodriveExtractor.CO_DRIVER_EVENT)[1]
        topic_tuple = topic_info[ATPDMLCodriveExtractor.CO_DRIVER_EVENT] if ATPDMLCodriveExtractor.CO_DRIVER_EVENT in topic_info else None
        r = ATPDMLCodriveExtractor.BAGFILE_RESPONSIBLE
        if self.skip_bagfile(bag):
            logger.info('Skipped bagfile : "{}". Responsible-Header set to: {}.'.format(path, r))
            return
        if topic_tuple is None:
            logger.info('Skipped bagfile: "{}". No events found.'.format(path))
            return
        elif topic_tuple.msg_type != 'std_msgs/Header':
            logger.error('Cannot parse ' + ATPDMLCodriveExtractor.CO_DRIVER_EVENT +
                         ' in bagfile. Non-DML event message type: {}:'.format(topic_tuple.msg_type))
            return

        # main loop over all filtered messages
        events = []
        offset = rospy.Duration.from_sec(self.snippet_length/2)
        for topic, message, t in bag.read_messages(topics=[ATPDMLCodriveExtractor.CO_DRIVER_EVENT]):
            events.append({
                'start': math.floor(cast_sec_to_msec(max(min_start, t-offset).to_sec())),
                'end': math.ceil(cast_sec_to_msec(min(max_end, t+offset).to_sec())),
                'type': ATPDMLCodriveExtractor.EXTRACTOR_NAME,
                'metadata': {
                    'ts': cast_sec_to_msec(t.to_sec()),
                    'frame_id': message.frame_id,
                    'namespace': ATPDMLCodriveExtractor.EXTRACTOR_NAME,
                    'version': ATPDMLCodriveExtractor.EXTRACTOR_VERSION},
                'bva': 10
            })
            # send extraction events every n messages
            if len(events) >= ATPDMLCodriveExtractor.events_batch:
                self._send_extraction_request(guid, events)
                events.clear()
                del events[:]
        if len(events) > 0:
            # sent last few extraction requests
            self._send_extraction_request(guid, events)

    def skip_bagfile(self, bag):
        env_responsible_is_set = ATPDMLCodriveExtractor.BAGFILE_RESPONSIBLE is not None
        skip_responsible_set = ATPDMLCodriveExtractor.BAGFILE_RESPONSIBLE == ATPDMLCodriveExtractor.BAGFILE_SKIP_RESPONSIBLE
        bag_responsible_set = bag._responsible is not None

        # Only skip a bag file if a responsible is set in the bag header
        # AND it is not equal to the given BAGFILE_RESPONSIBLE
        return env_responsible_is_set \
            and not skip_responsible_set \
            and bag_responsible_set \
            and bag._responsible != ATPDMLCodriveExtractor.BAGFILE_RESPONSIBLE

    def _send_extraction_request(self, guid, events):
        extraction_request = {
            'version': '1.0',
            'bagfile_guid': guid,
            'extractor_id': ATPDMLCodriveExtractor.EXTRACTOR_NAME + ':' + ATPDMLCodriveExtractor.EXTRACTOR_VERSION,
            'metadata_list': events
        }
        logger.info('Sending extraction request: {}:'.format(extraction_request))
        extraction_guid = self.client.submit_metadata(extraction_request)
        logger.info('Extraction request is inserted into the DB with guid:{}'.format(extraction_guid))
        return extraction_guid


def cast_sec_to_msec(sec):
    return int(round(sec * 1000))


def timestamp2ms(timestamp):
    """Converts timestamp to milliseconds (int)"""
    return int(str(timestamp).replace(".", "")[0:13])


def main(path, guid):
    # Extract metadata
    client = ContainerClient()
    extractor = ATPDMLCodriveExtractor(client, SNIPPET_LENGTH_SECS, path)
    extractor.update_bagfile_metadata(guid)
    extractor.parse_bagfile(path, guid)


if __name__ == '__main__':
    logger.set_logging_folder(
        os.path.abspath(os.path.dirname(__file__)) + '/logs')
    logger.set_log_level('info')
    main(os.environ['BAGFILE_PATH'], os.environ['BAGFILE_GUID'])
