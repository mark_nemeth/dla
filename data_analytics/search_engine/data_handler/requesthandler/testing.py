__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from datetime import datetime
from pathlib import PurePath

from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.utils import validate_json

from handlers.auth import auth
from handlers.bagfile_event_processor import get_cloud_event_processor
from handlers.utils import utils
from kubeflow.flow_creation import FlowManager
from model.models import NewOnPremBagfileEvent, NewBagfileEvent
from model.paths import BlobStoragePath
from proxies.metadata_api import MetadataApiProxy
from proxies.search_api import SearchApiProxy
from requesthandler.base import BaseRequestHandler


class NewFileRequestHandler(BaseRequestHandler):

    def initialize(self):
        self.processor = get_cloud_event_processor()
        self.metadata_api = MetadataApiProxy()
        self.search_api = SearchApiProxy()
        self.schema = self.load_json_schema('simulate_new_file_post.schema')

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'POST')

    @auth.requires_scope(["openid"])
    def post(self):
        data = validate_json(self.request.body.decode('utf-8'), self.schema)

        guid, job = (self.trigger_on_prem_flow(data)
                     if danf_env().platform == Platform.ON_PREM
                     else self.trigger_azure_flow(data))

        utils.build_response(response=self,
                             results={'bagfile_guid': guid,
                                      'job_id': str(job.id) if job else ""})

    def trigger_on_prem_flow(self, data):
        path = PurePath(data['path'])
        date = f"{datetime.now():%Y-%m-%d_%H-%M-%S}"
        output_dir = data.get('output_directory',
                              f"tmp/danf/testing/{path.stem}/{date}")

        event = NewOnPremBagfileEvent(str(path.name), str(path))
        guid = self.processor.process_new_file_event(event)
        self.metadata_api.mark_bagfile_for_testing(guid)
        bag = self.metadata_api.get_bagfile(guid)
        job = self.processor.process_new_bagfile_metadata_event(
           bag, output_dir, True)
        return guid, job

    def trigger_azure_flow(self, data):
        flow_name = data.get('flow_name')

        if flow_name == 'danf-az-snippet-flow':
            return self.trigger_azure_snippet_flow(data)
        else:
            return self.trigger_azure_full_flow(data)

    def trigger_azure_snippet_flow(self, data):
        path = BlobStoragePath(data['path'])

        if path.is_container():
            # case 1: input is a container, directly invoke flow mnger
            guid = self.metadata_api.upsert_bagfile(
                "c52adb2f-test-guid-test-d3573fd77b14", str(path) + '/fake.bag', 'fake.bag',
                'SNIPPET', [], [], 'PROCESSING_DEACTIVATED', 1
            )
            mnger = FlowManager()
            fake_bag = {'guid': guid, 'link': str(path)}
            job = mnger.launch_danf_az_snippet_flow(fake_bag, path, True)
            return fake_bag['guid'], job

        # case 2: input is a file, trigger normal functions
        event = NewBagfileEvent(
            file_name=str(path.file),
            file_path=str(path),
            container_name=str(path.container),
            storage_account=str(path.storage_account),
            msg_id=None,
            msg_pop_receipt=None
        )
        guid = self.processor.process_new_file_event(event)
        bag = self.search_api.get_childbagfile(guid)
        job = self.processor.process_new_bagfile_metadata_event(bag, True)
        return guid, job


    def trigger_azure_full_flow(self, data):
        path = BlobStoragePath(data['path'])
        event = NewBagfileEvent(
            file_name=str(path.file),
            file_path=str(path),
            container_name=str(path.container),
            storage_account=str(path.storage_account),
            msg_id=None,
            msg_pop_receipt=None
        )
        guid = self.processor.process_new_file_event(event)
        bag = self.metadata_api.get_bagfile(guid)
        job = self.processor.process_new_bagfile_metadata_event(bag, True)
        return guid, job

