import dotenv from 'dotenv';
import configLocal from '../config/config-local';
import configDev from '../config/config-dev';
import configProd from '../config/config-prod';
import configAzDev from '../config/config-az_dev';
import configAzInt from '../config/config-az_int';
import configAzPrd from '../config/config-az_prd';
import configSvTest from '../config/config-sv_test';
import configSvInt from '../config/config-sv_int';
import configSvProd from '../config/config-sv_prod';
import configVaiProd from '../config/config-vai_prod';
import configVaiInt from '../config/config-vai_int';
import configDevOnPrem from '../config/config-dev-onprem';
import packageJson from '../package.json'
import proxy from '../config/proxy';
import danf_version from '../config/config-env_danf'
import {isNumberValid, isStringValid} from './utils/helperFunctions';
import getLogger from './utils/logging';

const logger = getLogger(__filename)

dotenv.config();

const NODE_ENV_DEVELOPMENT = 'development';
const NODE_ENV_PRODUCTION = 'production';
const NODE_ENV_LOCAL = 'development-local';
const NODE_ENV_DEVELOPMENT_ONPREM = 'development-onprem';

class ConfigHandler {

  constructor() {

    this.ui_version = packageJson.version;
    this.danf_version = null

    if (process.env.DANF_ENV) {
      switch (process.env.DANF_ENV) {
        case 'dev':
          this.config = configDev;
          break;
        case 'prod':
          this.config = configProd;
          break;
        case 'az_dev':
          this.config = configAzDev;
          break;
        case 'az_int':
          this.config = configAzInt;
          break;
        case 'az_prd':
          this.config = configAzPrd;
          break;
        case 'sv_test':
          this.config = configSvTest;
          break;
        case 'sv_int':
          this.config = configSvInt;
          break;
        case 'sv_prod':
          this.config = configSvProd;
          break;
        case 'vai_prod':
          this.config = configVaiProd;
          break;
        case 'vai_int':
          this.config = configVaiInt;
          break;
      }
      this.danf_version = danf_version.danfVersion;
      this.setProxy();
    } else {
      switch (process.env.NODE_ENV) {
        case NODE_ENV_DEVELOPMENT_ONPREM:
          this.config = configDevOnPrem;
          break;
        case NODE_ENV_LOCAL:
          this.config = configLocal;
          break;
        default:
          this.config = configDev;
      }
      this.setProxy();
      this.config.host = 'https://localhost:3001'; // for development use localhost url
      this.danf_version = '1.0.0-dummy-danf-version'
    }
    logger.info('DANF_ENV: ' + process.env.DANF_ENV)
    logger.info('NODE_ENV: ' + process.env.NODE_ENV);
    logger.info('PROXY: ' + JSON.stringify(this.proxy));

    if (process.env.UI_DEV_MODE === undefined || process.env.UI_DEV_MODE.toLowerCase() === 'false') {
      this.uiDevModeIsActive = false;
    } else if (process.env.UI_DEV_MODE.toLowerCase() === 'true') {
      logger.info('UI is in dev mode. Authentication is not active!');
      this.uiDevModeIsActive = true;
    } else {
      throw new Error(`Invalid value (${process.env.UI_DEV_MODE}) for environment Variable UI_DEV_MODE. Allowed: [false, true, not defined].`)
    }

    if (process.env.REACT_APP_NODE_DRIVE_VIEW_ENABLED && process.env.REACT_APP_NODE_DRIVE_VIEW_ENABLED.toLowerCase() === 'true') {
      logger.info('UI for drive centric view is active!');
      this.isDriveViewActive = true;
    } else {
      this.isDriveViewActive = false;
    }

    if (process.env.REACT_APP_NODE_INGEST_DASHBOARD_ENABLED && process.env.REACT_APP_NODE_INGEST_DASHBOARD_ENABLED.toLowerCase() === 'true') {
      logger.info('UI for ingest dashboard is active!');
      this.isIngestDashboardActive = true;
    } else {
      this.isIngestDashboardActive = false;
    }
  }

  setProxy() {
    if (process.env.UI_PROXY_HOST !== undefined &&
      process.env.UI_PROXY_PORT !== undefined) {
      this.proxy = {
        host: process.env.UI_PROXY_HOST,
        port: process.env.UI_PROXY_PORT
      }
    } else {
      this.proxy = (isStringValid(proxy.host) && isNumberValid(proxy.port)) ? proxy : undefined;
    }
  }

  getNodeEnv() {
    return process.env.NODE_ENV || NODE_ENV_DEVELOPMENT;
  }

  isNodeEnvInProduction() {
    return this.getNodeEnv() === NODE_ENV_PRODUCTION;
  }

  isNodeEnvLocal() {
    return this.getNodeEnv() === NODE_ENV_LOCAL;
  }

  getDanfEnv() {
    return process.env.DANF_ENV || null;
  }

  isDanfInSVCluster() {
    return process.env.DANF_ENV === 'sv_prod' || process.env.DANF_ENV === 'sv_test'
      || process.env.DANF_ENV === 'sv_int';
  }

  isDanfInVaiCluster() {
    return process.env.DANF_ENV === 'vai_prod' || process.env.DANF_ENV === 'vai_test'
      || process.env.DANF_ENV === 'vai_int';
  }

  isDanfInAzureCluster() {
    return process.env.DANF_ENV === 'az_dev' || process.env.DANF_ENV === 'az_int' || process.env.DANF_ENV === 'az_prd';
  }

  useDaimlerLogin() {
    return this.isDanfInSVCluster() || this.isDanfInAzureCluster() || this.isDanfInVaiCluster();
  }

  getDefaultRequestConfig() {
    return {
      qsStringifyOptions: {arrayFormat: 'repeat'}
    }
  }

  getFileCatalogApiConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.FILE_CATALOG_HOST || this.config.fileCatalogHost) + (process.env.FILE_CATALOG_FILE_API || this.config.fileCatalogFileApi),
    });
  }

  getDataTransferSearchApiConfig(){
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.DATA_TRANSFER_HOST || this.config.dataTransferHost) + (process.env.DATA_TRANSFER_SEARCH_API || this.config.dataTransferSearchApi),
    });
  }

  getDataTransferLatestIngestApiConfig(){
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.DATA_TRANSFER_HOST || this.config.dataTransferHost) + (process.env.DATA_TRANSFER_LATEST_INGEST_API || this.config.dataTransferLatestIngestApi),
    });
  }

  getDataTransferSearchIngestApiConfig(){
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.DATA_TRANSFER_HOST || this.config.dataTransferHost) + (process.env.DATA_TRANSFER_SEARCH_INGEST_API || this.config.dataTransferSearchIngestApi),
    });
  }

  getDataTransferApiConfig(){
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.DATA_TRANSFER_HOST || this.config.dataTransferHost) + (process.env.DATA_TRANSFER_API || this.config.dataTransferApi),
    });
  }

  getDataTransferApiHealthConfig(){
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.DATA_TRANSFER_HOST || this.config.dataTransferHost) + (process.env.DATA_TRANSFER_HEALTH || this.config.dataTransferHealth),
    });
  }

  getBagfileAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.BAGFILE_API || this.config.bagfileApi),
    });
  }

  getDriveAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.DRIVE_FILE_API || this.config.driveApi),
    });
  }

  getChildBagfileAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.CHILD_BAGFILE_API || this.config.childBagfileApi),
    });
  }

  getReportingKPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.REPORTING_KPI_API || this.config.reportingKpiApi),
    });
  }

  getReportingTablesSimpleConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.REPORTING_TABLES_SIMPLE_API || this.config.reportingTablesSimpleApi),
    });
  }

  getReportingTablesAggregationConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.REPORTING_TABLES_AGGREGATION_API || this.config.reportingTablesAggregationApi),
    });
  }

  getFiltersAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.FILTERS_API_PATH || this.config.filtersApi),
    });
  }

  getFilterFeaturesAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.FILTER_FEATURES_API_PATH || this.config.filterFeaturesApi),
    });
  }

  getChildBagfileFiltersAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.CHILD_BAGFILE_FILTERS_API_PATH || this.config.childBagfileFiltersApi),
    });
  }

  getDriveFiltersAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.DRIVE_FILE_FILTERS_API_PATH || this.config.driveFiltersApi),
    });
  }

  getFlowrunAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.SEARCH_HOST || this.config.searchApiHost) + (process.env.FLOW_RUN_API_PATH || this.config.flowRunApi),
    });
  }

  getReprocessingAPIConfig() {
    return Object.assign(this.getDefaultRequestConfig(), {
      url: (process.env.DATAHANDLER_HOST || this.config.dataHandlerApiHost) + (process.env.REPROCESSING_API || this.config.reprocessingApi),
    });
  }

  hasProxy() {
    return this.proxy !== undefined && this.proxy.host !== undefined && this.proxy.port !== undefined
  }

  getProxyUrl() {
    return this.hasProxy() ? 'http://' + this.proxy.host + ':' + this.proxy.port : undefined;
  }

  getAuthDomain() {
    return this.useDaimlerLogin()
      ? (process.env.AUTH_GAS_DOMAIN || configHandlerSingleton.config.authGASDomain)
      : (process.env.AUTH0_DOMAIN || configHandlerSingleton.config.auth0Domain)
  }

  getAuthCallbackURL() {
    return process.env.AUTH_CALLBACK_URL || this.config.host + this.config.authCallbackUrl
  }

  getAuthLogoutURL() {
    return process.env.AUTH_GAS_LOGOUT_URL || this.getAuthDomain() + this.config.authGASLogoutUrl
  }

  getAuthAuthEndpoint() {
    return process.env.AUTH_GAS_AUTH_ENDPOINT || configHandlerSingleton.config.authGASAuthEndpoint
  }

  getAuthTokenEndpoint() {
    return process.env.AUTH_GAS_TOKEN_ENDPOINT || configHandlerSingleton.config.authGASTokenEndpoint
  }

  getAuthUserEndpoint() {
    return process.env.AUTH_GAS_USER_ENDPOINT || configHandlerSingleton.config.authGASUserEndpoint
  }

  getAuthJWKSEndpoint() {
    return process.env.AUTH_GAS_JWKS_ENDPOINT || configHandlerSingleton.config.authGASJWKSEndpoint
  }

  getAuthClientID() {
    return this.useDaimlerLogin()
      ? (process.env.AUTH_GAS_CLIENT_ID || configHandlerSingleton.config.authClientID)
      : (process.env.AUTH0_CLIENT_ID || configHandlerSingleton.config.authClientID)
  }

  getAuthClientSecret() {
    return this.useDaimlerLogin()
      ? (process.env.AUTH_GAS_CLIENT_SECRET || configHandlerSingleton.config.authClientSecret)
      : (process.env.AUTH0_CLIENT_SECRET || configHandlerSingleton.config.authClientSecret)
  }

  getSessionClientSecret() {
    return process.env.SESSION_CLIENT_SECRET || configHandlerSingleton.config.sessionClientSecret
  }

  getRedisConfig() {
    const redisConfig = {};
    if (this.getDanfEnv()) {
      redisConfig.host = process.env.REDIS_HOST || configHandlerSingleton.config.redisHost;
      redisConfig.port = process.env.REDIS_PORT || configHandlerSingleton.config.redisPort;
      redisConfig.auth_pass = process.env.REDIS_CACHE_KEY || configHandlerSingleton.config.redisKey;
      if (!this.isDanfInSVCluster() && !this.isDanfInVaiCluster()) redisConfig.tls = {servername: process.env.REDIS_HOST || configHandlerSingleton.config.redisHost};
    } else {
      // use local redis instance for development
      redisConfig.host = 'localhost';
      redisConfig.port = '6379';
    }
    return redisConfig
  }

  getRedisEncryptionKey() {
    return process.env.REDIS_ENCRYPTION_KEY || this.config.redisEncryptionKey;
  }

  uiIsInDevMode() {
    return this.uiDevModeIsActive;
  }

  getVersionInformation() {
    return {
      uiVersion: this.ui_version,
      danfVersion: this.danf_version
    }
  }

  getFeatureToggles() {
    return {
      isDriveViewActive: this.isDriveViewActive,
      isIngestDashboardActive: this.isIngestDashboardActive
    }
  }
}

const configHandlerSingleton = new ConfigHandler();
export default configHandlerSingleton;