"""Unittest for bagfiles handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from pathlib import PurePath
from unittest.mock import Mock

from kubeflow.flows.danf_sv_processing_flow import DanfSvProcessingFlow


class TestDanfSvProcessingFlow(unittest.TestCase):

    DEFAULT_BAGFILE_GUID = "509ede01-1071-415a-a63f-31669fb8b984"
    DEFAULT_FILE_NAME = "file"
    DEFAULT_INPUT_PATH = PurePath("data/input/byName/v222-5253/2019/12/24")
    DEFAULT_VEHICLE = "v222-5253"

    def setUp(self):
        self.config_mock = Mock()
        DanfSvProcessingFlow.get_config = self.config_mock

    def test_get_output_path_when_vehicle_whitelisted_should_return_input_dir(self):
        self.config_mock.return_value = {'whitelisted_vehicles': ["x", "v222-5253"]}
        p = self.get_output_path_wrapper()
        self.assertEqual(p, str(self.DEFAULT_INPUT_PATH))

    def test_get_output_path_when_vehicle_not_whitelisted_should_return_tmp_dir(self):
        self.config_mock.return_value = {'whitelisted_vehicles': ["x"]}
        p = self.get_output_path_wrapper()
        self.assertTrue(p.startswith('tmp'))
        self.assertEqual(p, f"tmp/danf/{self.DEFAULT_VEHICLE}/2019/12/24")

    def test_get_output_path_when_whitelisted_not_defined_should_return_tmp_dir(self):
        self.config_mock.return_value = {}
        p = self.get_output_path_wrapper()
        self.assertTrue(p.startswith('tmp'))

    def test_get_output_path_when_vin_input_path_should_return_tmp_dir(self):
        self.config_mock.return_value = {'whitelisted_vehicles': ["x", "v222-5253"]}
        p = self.get_output_path_wrapper(
            path_to_bagfile_dir_in_mapr=PurePath("data/input/WDD222xxxxxxxxxxx/2019"))
        self.assertTrue(p.startswith('tmp'))

    def test_get_output_path_when_byVehicleId_input_path_should_return_tmp_dir(self):
        self.config_mock.return_value = {'whitelisted_vehicles': ["x", "v222-5253"]}
        p = self.get_output_path_wrapper(
            path_to_bagfile_dir_in_mapr=PurePath(f"data/input/byVehicleId/{self.DEFAULT_VEHICLE}/2019"))
        self.assertTrue(p.startswith('tmp'))

    def test_get_output_path_when_override_present_should_return_override(self):
        override_path = "tmp/testing"
        p = self.get_output_path_wrapper(output_path_override=override_path)
        self.assertEqual(p, "tmp/testing")

    # helpers
    def get_output_path_wrapper(self,
                                output_path_override=None,
                                path_to_bagfile_dir_in_mapr=DEFAULT_INPUT_PATH,
                                file_name=DEFAULT_FILE_NAME,
                                bagfile_guid=DEFAULT_BAGFILE_GUID):
        return DanfSvProcessingFlow.get_output_path(
            output_path_override,
            path_to_bagfile_dir_in_mapr,
            file_name,
            bagfile_guid)


if __name__ == '__main__':
    unittest.main()
