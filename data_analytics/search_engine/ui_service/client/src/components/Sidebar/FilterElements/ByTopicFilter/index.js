import {connect} from 'react-redux';
import {filterByTopics, setCurrentTopicSearchQuery} from '../../../../redux/Home/HomeActions';
import {ByTopicFilter} from './ByTopicFilter';
import {FileTypes} from "../ByFileFilter/ByFileFilter";

function mapStateToProps(state, ownProps) {
  let topicList;
  switch (state.fileType) {
    case FileTypes.ORIGINALFILE:
      topicList = state.filterOptions.originalFileTopicList || [];
      break;
    case FileTypes.ALWAYSONFILE:
      topicList = state.filterOptions.alwaysonFileTopicList || [];
      break;
    case FileTypes.CHILDBAGFILE:
      topicList = state.filterOptions.childBagfileTopicList || [];
      break;
  }
  const selectedTopicList = state.filters.topics || [];
  const topicSearchQuery = state.currentTopicSearchQuery || '';
  return {selectedTopicList, topicList, topicSearchQuery};
}

export default connect(
  mapStateToProps,
  {filterByTopics, setCurrentTopicSearchQuery}
)(ByTopicFilter);