"""
This script highlights how you can use the DBConnector to add test data to a local mongo db instance
"""
from datetime import datetime, timedelta
import json
import numpy as np
import math

from DBConnector import DBConnector

# DB Connection Settings
HOST = "localhost"
PORT = 27017
DB_NAME = "metadata"
USER = "user"
KEY = "password"
PROTOCOL = "mongodb"
SSL = False

bag = {
    "guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "link": "/test/test/test/test/test/test/test",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "version": "1.0",
    "is_test_data": 1
}

feat_bucket_doc = {
    "version":
    "1.0",
    "guid":
    "22221111-aaaa-bbbb-cccc-999999999999",
    "bagfile_guid":
    bag["guid"],
    "start":
    1563262907000,
    "end":
    1563262911001,
    "start_index":
    60,
    "end_index":
    119,
    "tags": ["", ""],
    "features": [{
        "ts": 1563262907000,
        "vel": 0.4,
        "acc": 0.2,
        "eng_state": 5,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 5.1
    }, {
        "ts": 1563262908000,
        "vel": 0.5,
        "acc": 0.3,
        "eng_state": 11,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 5.1
    }],
    "is_test_data":
    1
}


def main():
    connector = DBConnector(HOST, PORT, DB_NAME, USER, KEY, PROTOCOL, SSL)
    with connector as con:
        con.add_bagfiles([bag])
        con.add_feature_buckets([feat_bucket_doc])


if __name__ == '__main__':
    main()
