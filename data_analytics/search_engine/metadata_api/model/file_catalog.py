__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List, Optional, Set, Dict

from model.types import Guid, UnixTimestampMilliseconds
from pydantic import BaseModel, validator


###############################################################################
# request/response models
###############################################################################
class FileUrlInCreate(BaseModel):
    accessor_url: str
    creation_date: UnixTimestampMilliseconds
    entry_created_by: str
    url_status: str
    last_changed: Optional[UnixTimestampMilliseconds] = None
    expiration_date: Optional[UnixTimestampMilliseconds]

    @validator('last_changed', pre=True, always=True)
    def default_ts_changed(cls, v, *, values, **kwargs):
        return v or values['creation_date']


class FileUrl(FileUrlInCreate):
    url_id: int


class FileInCreate(BaseModel):
    parent_guid: Optional[Guid]
    file_type: str = "data"
    file_size: int
    urls: List[FileUrlInCreate] = []
    tags: List[Dict[str, str]] = []


class File(FileInCreate):
    file_guid: Guid


class FileFilterParams(BaseModel):
    file_type: str
    url: str
    tags: Set[str]
    url_status: str


class FileInResponse(BaseModel):
    file: File


class ManyFilesInResponse(BaseModel):
    files: List[File]
    count: int


class FileCatalogUpdateRequest(BaseModel):
    file_guid: Guid
    file_url: FileUrlInCreate


class FileCatalogUrlStatusUpdateRequestBody(BaseModel):
    url_status: str


class FileCatalogUrlStatusUpdateRequest(FileCatalogUrlStatusUpdateRequestBody):
    file_guid: Guid
    url_id: int
    last_changed: UnixTimestampMilliseconds


class FileCatalogGetByStatus(BaseModel):
    file_guid: Optional[Guid]
    url_status: Optional[str]
