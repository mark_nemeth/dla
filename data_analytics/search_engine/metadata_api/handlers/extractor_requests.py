"""Handler class for extractor requests"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import warnings
from copy import deepcopy
from typing import List

from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DB_Data_Handler
from handlers.utils import utils
from model.exceptions import TimestampOutOfRangeException, DANFApiError
from model.extractor_event import ExtractedEvent

logger = logging.getLogger(__name__)


class ExtractorRequestsHandler:
    def __init__(self):
        self.db_data_handler = DB_Data_Handler()

    def new_extractor_request_legacy(self, extractor_request):
        try:
            # Computation of sequences (temporarily) deactivated, because the new
            # extractor is not supported
            # self._compute_and_store_sequences(extractor_request)
            warnings.warn("Deprecated", DeprecationWarning)
            return self.add_extractor_requests_to_db_legacy(extractor_request)
        except TimestampOutOfRangeException as e:
            raise DANFApiError(400, f"Timestamp Error: {e}") from e

    def new_extractor_request(self, extracted_events: List[ExtractedEvent]
                              ) -> List[str]:
        """Handles new extracted events

        :param extracted_events: list of extracted events
        :return event_guids: list of event guids
        """
        event_guids = self.add_extracted_events_to_db(extracted_events)
        logger.debug(f"Saved event guids: {event_guids}")
        return event_guids

    def _compute_and_store_sequences(self, bagfile: dict,
                                     event: ExtractedEvent):
        """Computes and stores sequences for extracted event

        :param bagfile: bagfile dict
        :param event: extracted event
        """
        bagfile_start = int(bagfile.get('start', 0))
        bagfile_end = int(bagfile.get('end', 0))
        # validate timestamps
        if not bagfile_start <= event.start <= event.end <= bagfile_end:
            raise DANFApiError(
                400,
                f"Extractor event could not be processed: timestamp violates the time range. "
                f"Failed check: {bagfile_start} <= {event.start} <= {event.end} <= {bagfile_end}"
            )

        # create indices relative to bagfile start/end
        index_list = list(
            range(
                utils.timestamp_to_sequence_index(event.start, bagfile_start),
                utils.timestamp_to_sequence_index(event.end, bagfile_start) +
                1))

        # create sequences
        sequences = []
        bagfile_guid_index_tuples = []
        for index in index_list:
            sequence = {
                "start":
                utils.sequence_index_to_timestamp(index, bagfile_start),
                "end":
                utils.sequence_index_to_timestamp(index + 1, bagfile_start) -
                1,
                "bva": [{
                    "extractor_id": event.extractor_id,
                    "event_guid": event.
                    guid,  # TODO replace by vote_guid once voting is implemented
                    "value": event.bva
                }],
                "metadata": [{
                    "extractor_id": event.extractor_id,
                    "event_guid": event.guid,
                    "tags": event.tags
                }]
            }
            sequences.append(sequence)
            bagfile_guid_index_tuples.append((event.bagfile_guid, index))

        # update/insert sequences/ext_request to db
        self.db_data_handler.update_sequences_by_index(
            bagfile_guid_index_tuples, sequences)

    def _compute_and_store_sequences_legacy(self, extractor_request: dict):
        """Generates sequences and stores them in the DB.

        :param extractor_request: Extractor Request as dict
        """
        warnings.warn("Deprecated", DeprecationWarning)

        version = extractor_request['version']
        bagfile_guid = extractor_request['bagfile_guid']
        bagfile = self.db_data_handler.get_bagfiles([bagfile_guid])[0]
        extractor_id = extractor_request['extractor_id']
        bagfile_start = int(bagfile['start'])
        bagfile_end = int(bagfile['end'])
        # prepare sequences/ext_request for insertion to db
        sequence_dict = {}
        for metadata in extractor_request['metadata_list']:
            # validate timestamps
            start = int(metadata['start'])
            if not bagfile_start <= start <= bagfile_end:
                error_message = 'Extractor request start out of range error'
                logger.error(error_message)
                raise TimestampOutOfRangeException(
                    'Extractor Request could not be processed: Extractor request start timestamp violates the time range of the provided bagfile',
                    start, bagfile_start, bagfile_end)
            end = int(metadata['end'])
            if not bagfile_start <= end <= bagfile_end:
                error_message = 'Extractor request end out of range error'
                logger.error(error_message)
                raise TimestampOutOfRangeException(
                    'Extractor Request could not be processed: Extractor request end timestamp violates the time range of the provided bagfile',
                    end, bagfile_start, bagfile_end)

            # create indices relative to bagfile start/end
            index_list = list(
                range(
                    utils.timestamp_to_sequence_index(start, bagfile_start),
                    utils.timestamp_to_sequence_index(end, bagfile_start) + 1))

            metadata_dict = metadata.get('metadata', {}).copy()
            # ensure that metadata values are stored as list
            if metadata_dict != {}:
                for k, v in metadata_dict.items():
                    if not isinstance(v, list):
                        metadata_dict[k] = [v]
            bva = metadata.get('bva', 0)

            # store all sequences in sequence_dict (+aggregate overlapping sequences)
            for i in index_list:
                if i in sequence_dict.keys():
                    # update sequence thats already part of the dict
                    sequence_dict[i]['extractors.' + extractor_id +
                                     '.bva'] = max(
                                         sequence_dict[i]['extractors.' +
                                                          extractor_id +
                                                          '.bva'], bva)
                    if metadata_dict != {}:
                        for k, v in metadata_dict.items():
                            # check if metadata fields exist and append new data if necessary
                            if k in sequence_dict[i]['extractors.' +
                                                     extractor_id +
                                                     '.metadata'].keys():
                                sequence_dict[i]['extractors.' + extractor_id +
                                                 '.metadata'][k].extend(
                                                     deepcopy(v))
                            else:
                                sequence_dict[i]['extractors.' + extractor_id +
                                                 '.metadata'][k] = deepcopy(v)
                else:
                    # add new sequence
                    metadata_update = {}
                    metadata_update['version'] = version
                    metadata_update['extractors.' + extractor_id +
                                    '.metadata'] = deepcopy(metadata_dict)
                    metadata_update['extractors.' + extractor_id +
                                    '.bva'] = bva
                    metadata_update[
                        'start'] = utils.sequence_index_to_timestamp(
                            i, bagfile_start)
                    metadata_update['end'] = utils.sequence_index_to_timestamp(
                        i + 1, bagfile_start)
                    sequence_dict[i] = deepcopy(metadata_update)
        # reformat data for insertion to db
        bagfile_guid_index_tuples = []
        sequence_list = []
        for k, v in sequence_dict.items():
            bagfile_guid_index_tuples.append((bagfile_guid, k))
            sequence_list.append(v)
        # update/insert sequences/ext_request to db
        self.db_data_handler.update_sequences_by_index(
            bagfile_guid_index_tuples, sequence_list)

    def add_extractor_requests_to_db_legacy(self, extractor_request):
        """Add extractor requests (version 1) to DB by iterating over the metadata list

        :param extractor_request: object
        :
        :return: guid of last extractor request added to DB
        """
        warnings.warn("Deprecated", DeprecationWarning)

        extractor_guids = []
        for metadata in extractor_request.get('metadata_list', []):
            ext_metadata = {
                'guid': generate_guid(),
                'version': extractor_request.get('version', ''),
                'bagfile_guid': extractor_request.get('bagfile_guid', ''),
                'extractor_id': extractor_request.get('extractor_id', ''),
                'start': metadata.get('start', 0),
                'end': metadata.get('end', 0),
                'bva': metadata.get('bva', 0),
                'metadata': metadata.get('metadata', {})
            }
            extractor_guids.append(ext_metadata['guid'])
            self.db_data_handler.add_extractor_requests_legacy([ext_metadata])

        return extractor_guids

    def add_extracted_events_to_db(self,
                                   extracted_events: List[ExtractedEvent]):
        """Add extracted events to DB

        :param extracted_events: List of extracted events
        :return: guids of extracted events added to DB
        """
        guids = []
        for event in extracted_events:
            if isinstance(event, ExtractedEvent):
                unique_url = f"https://dla.bosch.com/extractor-request/" \
                             f"{event.bagfile_guid}/{event.extractor_id}/{event.ts}/{event.start}/{event.end}"
                guid = generate_guid(unique_url)
                guids.append(guid)
            else:
                raise DANFApiError(400, f"Invalid extractor request query")

        self.db_data_handler.upsert_extractor_requests(guids, extracted_events)
        return guids

    def delete_extractor_request_from_db(self, guid):
        self.db_data_handler.delete_extractor_requests([guid])
        return
