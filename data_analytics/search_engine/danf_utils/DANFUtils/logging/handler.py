"""DANF Logger"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from datetime import datetime

from DANFUtils.logging.config import configure_root_logger

LOG_LEVELS = {'error': logging.ERROR,
              'warning': logging.WARNING,
              'info': logging.INFO,
              'debug': logging.DEBUG}

LOGGER_NAME = 'danf_logger'
LOG_FILE_PREFIX = 'log'
ERR_FILE_PREFIX = 'err'


class LoggingHandler:
    """
    DANF Logger
    """

    def __init__(self, log_level='info'):
        """
        Initialize the logger
        :param log_level: Determines which messages will be logged
            'error': Only error messages
            'warning': Error and warning messages
            'info': Error, warning and info messages (default)
            'debug': All messages
        """
        # perform initialization of root logger lazily, to avoid automatic
        # config of root logger when DANFUtils.logging is imported
        # somewhere. Only perform root logger config if LoggingHandler is
        # actually used
        self._root_logger_initialized = False

        self.logger = logging.getLogger(LOGGER_NAME)
        self.set_log_level(log_level)

    def set_logging_folder(self, folder_path):
        """
        Set the path for storing the logfiles
        :param folder_path: Path to the logging folder
        """
        date = datetime.now().strftime("%y_%m_%d")
        file_path = f"{folder_path}/logs_{date}.log"
        self._init_root_logger(file_path=file_path)

    def set_log_level(self, log_level):
        """
        Update the log level of the logger
        :param log_level: Determines which messages will be logged
            'error': Only error messages
            'warning': Error and warning messages
            'info': Error, warning and info messages
            'debug': All messages
        """
        assert log_level in LOG_LEVELS, 'log_level has to take one of the values [' + ','.join(LOG_LEVELS) + ']'
        self.logger.setLevel(LOG_LEVELS[log_level])

    def error(self, msg):
        """
        Log a message on level 'error'
        :param msg: The message str that shall be logged
        """
        if not self._root_logger_initialized:
            self._init_root_logger()
        self.logger.error(msg)

    def exception(self, msg):
        """
        Log a exception message on level 'error'. The stacktrace will
        be automatically included in the log message.
        :param msg: The message str that shall be logged
        """
        if not self._root_logger_initialized:
            self._init_root_logger()
        self.logger.exception(msg)

    def warning(self, msg):
        """
        Log a message on level 'warning'
        :param msg: The message str that shall be logged
        """
        if not self._root_logger_initialized:
            self._init_root_logger()
        self.logger.warning(msg)

    def info(self, msg):
        """
        Log a message on level 'info'
        :param msg: The message str that shall be logged
        """
        if not self._root_logger_initialized:
            self._init_root_logger()
        self.logger.info(msg)

    def debug(self, msg):
        """
        Log a message on level 'debug'
        :param msg: The message str that shall be logged
        """
        if not self._root_logger_initialized:
            self._init_root_logger()
        self.logger.debug(msg)

    def _init_root_logger(self, file_path=None):
        configure_root_logger(file_path=file_path)
        self._root_logger_initialized = True


logger = LoggingHandler()
