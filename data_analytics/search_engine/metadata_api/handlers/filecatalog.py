__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from handlers.db_data_handler import DB_Data_Handler
from model.file_catalog import FileCatalogUpdateRequest, File, \
    FileCatalogUrlStatusUpdateRequest, FileCatalogGetByStatus


class FileCatalogHandler:
    """Implements the Flow API business logic with model.flows.Flow objects
    at its core. A Flow represents a conceptual data processing pipeline (i.e.
    kubeflow pipeline). Flows are designed to be immutable."""

    def __init__(self):
        self.db_handler = DB_Data_Handler()

    def create_catalog(self, req: File, update=False):
        """Creates and persists a new file catalog.

        :param req: TransferFileRequest
        :return: The created flow instance
        """
        self.db_handler.add_catalog([req.dict()])
        return req.dict()

    def update_catalog(self, req: FileCatalogUpdateRequest, update=True):
        """Updates an existing file catalog.

        :param req: CatalogFileUpdateRequest
        """
        fileUpdateCatalog = FileCatalogUpdateRequest(
            **req.dict()
        )
        self.db_handler.update_catalog(fileUpdateCatalog)
        return None

    def update_catalog_url_status(self, req: FileCatalogUrlStatusUpdateRequest, update=True):
        """Updates an existing url status in file catalog.

        :param req: FileCatalogUrlStatusUpdateRequest 
        """
        fileUpdateCatalog = FileCatalogUrlStatusUpdateRequest(
            **req.dict()
        )
        self.db_handler.update_catalog_url_status(fileUpdateCatalog)
        return None

    def get_catalog_all_urls(self, file_guid: str) -> File:
        """Retrieves and returns the catalog identified by the given guid

        :param file_guid: guid of the file catalog to retrieve
        :return: catalog
        """
        catalogs = self.db_handler.get_catalog_all([file_guid])
        if len(catalogs) == 1:
            return catalogs[0]
        return None

    def get_catalog(self, file: FileCatalogGetByStatus) -> File:
        """Retrieves and returns the catalog identified by the given guid and status

        :param file: FileCatalogGetByStatus
        :return: catalog
        """
        # catalogs = self.db_handler.get_catalog(file.file_guid, file.url_status)
        catalogs = self.db_handler.get_catalog_all([file.file_guid])
        if len(catalogs) == 1:
            catalog_entry = catalogs[0]
            catalog_entry.urls = [url for url in catalog_entry.urls if url.url_status == file.url_status]
            return catalog_entry
        return None

    def delete_catalog(self, guid: str) -> int:
        return self.db_handler.delete_catalog(guid)
