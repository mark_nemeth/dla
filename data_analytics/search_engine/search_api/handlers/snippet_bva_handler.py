"""Handler class for bagfile search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

import numpy as np
from DANFUtils.exceptions import GUIDNotFoundException
from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DBDataHandler

logger = logging.getLogger(__name__)


class SnippetBVAHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def get_snippets_bva(self,
                         bagfile_guid,
                         bva_limit,
                         gps_subsampling_ratio=5,
                         extractors=None,
                         generate_child_bagfiles=True):
        """
        Generates snippets for a bagfile based on minimum BVA value

        :param bagfile_guid: guid of bagfile to be processed
        :param bva_limit: int minimum BVA value to filter sequences
        :param gps_subsampling_ratio: int subsampling ratio for gps_track aggregation
        :param extractors: if a list of extractors is provided then process only the metadata of these
        :param generate_child_bagfiles: bool to generate child bagfiles or not to generate it
        :return: list of child bagfile JSON metadata
        """
        try:
            # retrieve all sequences for bagfile from db
            sequences = self.db_data_handler.get_sequences_by_bagfile_guid(
                bagfile_guid)
        except GUIDNotFoundException:
            # check if bagfile actually exists in db -> throws error if not
            self.db_data_handler.get_bagfiles([bagfile_guid])

            # warn that bagfile has no metadata and return empty list
            logger.warning(
                'Bagfile ' + str(bagfile_guid) +
                ' has no sequences(metadata/bva). BVA Snippet calculation will return an empty list.'
            )
            return []

        # extract sequence/bva value matrix and indices
        indices = []
        index_lookup = {}
        bva_mat = []
        for i, s in enumerate(sequences):
            indices.append(s['index'])
            index_lookup[s['index']] = i
            bva_mat.append([
                max(s['extractors'][k]['bva']
                    if extractors is None or k in extractors else 0
                    for k, v in s['extractors'].items())
            ])

        if indices == []:
            bva_diff = []

        else:
            # compute row wise average
            bva_max = np.max(np.array(bva_mat), axis=1)

            # create sequences array including empty sequences (bva = -1) and fill existing bva values
            bva_all_sequences = np.subtract(np.zeros((np.max(indices) + 1)), 1)
            bva_all_sequences[indices] = bva_max

            # find sequences with bva>=bva_limit
            relevant = bva_all_sequences >= bva_limit

            # insert dummy sequences at start/end for diff operation
            relevant = np.insert(relevant, 0, 0) * 1
            relevant = np.append(relevant, 0)

            # compute first order difference (change of relevant/not relevant)
            bva_diff = np.diff(relevant, n=1)

        # compute start/end indices of bva snippets
        start_index = []
        end_index = []
        for seq_list_index, x in enumerate(bva_diff):
            if x == 1:
                start_index.append(seq_list_index)
            elif x == -1:
                end_index.append(seq_list_index)

        # create child bagfile dicts
        child_bagfiles = []
        for start, end in zip(start_index, end_index):
            orig_start = index_lookup[start]
            orig_end = index_lookup[end - 1]

            # aggregate metadata + bva for the childbagfile
            aggregated_metadata, aggregated_bva = self._aggregate_metadata(
                sequences[orig_start:orig_end + 1], extractors=extractors)

            gps_track = self._aggregate_gps(
                sequences[orig_start:orig_end + 1],
                gps_subsampling_ratio=gps_subsampling_ratio)

            child_bag = {
                'version': '1.0',
                'guid': generate_guid(),
                'parent_guid': bagfile_guid,
                'start': sequences[orig_start]['start'],
                'end': sequences[orig_end]['end'],
                'parent_index_start': start,
                'parent_index_end': end - 1,
                'bva_max': aggregated_bva,
                'metadata': aggregated_metadata,
                'gps_track': gps_track
            }
            child_bagfiles.append(child_bag)

        if generate_child_bagfiles:
            # add child bagfiles to db if non-empty
            if child_bagfiles:
                self.db_data_handler.add_child_bagfiles(child_bagfiles)
            else:
                logger.info(
                    f"No child bagfiles found for bagfile {bagfile_guid} with bva limit {bva_limit}."
                )

        return child_bagfiles

    @staticmethod
    def _aggregate_metadata(sequences, extractors=None):
        # TODO: Refactor this when the new sequence data model has been released
        metadata_dict = {}
        max_bva = []
        # aggregate metadata/bva over all sequences/extractors while preserving the extractor/metadata keys
        for seq in sequences:
            for extractor_key, extractor_value in seq['extractors'].items():
                # skip redundant ext1_3 GPS metadata as it is aggregated separately into gps_track GeoJSON
                if extractor_key == 'ext1_3':
                    continue
                # aggregate only relevant metadata if defined by extractors list
                if extractors is not None and extractor_key not in extractors:
                    continue
                max_bva.append([seq['extractors'][extractor_key]['bva']])
                if extractor_key not in metadata_dict:
                    metadata_dict[extractor_key] = {}
                if 'metadata' in seq['extractors'][extractor_key]:
                    for metadata_key, metadata in seq['extractors'][
                            extractor_key]['metadata'].items():
                        if metadata_key not in metadata_dict[extractor_key]:
                            metadata_dict[extractor_key][
                                metadata_key] = metadata
                        else:
                            for metadata_item in metadata:
                                exists = False
                                # TODO: refactor this quick workaround for bug ATTDATA-1268
                                try:
                                    for added_item in metadata_dict[
                                            extractor_key][metadata_key]:
                                        if 'ts' in metadata_item and 'ts' in added_item:
                                            if metadata_item[
                                                    'ts'] == added_item['ts']:
                                                exists = True
                                except TypeError:
                                    logger.debug(
                                        "Warning: metadata key is not iterable"
                                    )
                                if not exists:
                                    metadata_dict[extractor_key][
                                        metadata_key].append(metadata_item)
        return metadata_dict, int(np.max(max_bva))

    @staticmethod
    def _aggregate_gps(sequences, gps_subsampling_ratio=5):
        """Aggregates extractor ext1_3 GPS metadata to sub-sampled GeoJSON LineString

        :param sequences: list of sequences dict
        :param gps_sub-sampling_ratio: int sub-sampling ratio
        """
        subsampled_sequences = sequences[::gps_subsampling_ratio]
        gps_track = {}
        coordinates = []
        for seq in subsampled_sequences:
            gps_data = seq.get('extractors',
                               {}).get('ext1_3', {}).get('metadata',
                                                         {}).get('gps', None)
            if gps_data and isinstance(gps_data, list):
                latitude = gps_data[0].get('latitude', 0.0)
                longitude = gps_data[0].get('longitude', 0.0)
                coordinates.append([longitude, latitude])
        if len(coordinates) >= 2:
            gps_track = {"type": "LineString", "coordinates": coordinates}
            logger.debug(
                f"GPS data has been aggregated into gps_track: {gps_track}")
        else:
            logger.debug("no GPS track data found in sequences")
        return gps_track
