__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from datetime import datetime

from kubernetes import client, config
from kubernetes.client import CoreV1Api, V1DeleteOptions

from DANFUtils.logging import logger


class PodRemover:
    """PodRemover"""

    POD_SELECTOR = 'status.phase=Succeeded'

    def __init__(self,
                 namespace: str,
                 threshold: int,
                 timeout: int = 60) -> None:
        """Initialize PodRemover

        :param namespace: from which pods should be removed
        :param threshold: delete pods once the threshold is reached
        :param timeout: optional request timeout for kube API
        """
        self.v1 = self._init_kube_api()
        self.namespace = namespace
        self.timeout = timeout
        self.threshold = threshold

    def remove_pods(self) -> None:
        """Removes all succeeded pods from the cluster if the defined threshold is reached.
        """

        try:
            count = self._get_amount_of_successfully_terminated_pods()
            if count >= self.threshold:
                logger.info(
                    f"Succeeded pod count: {count} over the threshold of {self.threshold}")
                self._delete_successfully_terminated_pods()
                logger.info("Successfully deleted all succeeded pods")
        except Exception as exp:
            logger.error(f"Error occurred while removing pods. Exp: {exp}")
            raise exp

    def _init_kube_api(self) -> CoreV1Api:
        config.load_incluster_config()
        return client.CoreV1Api()

    def _get_amount_of_successfully_terminated_pods(self) -> int:
        ret = self.v1.list_namespaced_pod(namespace=self.namespace,
                                          pretty=True,
                                          timeout_seconds=self.timeout,
                                          field_selector=self.POD_SELECTOR)
        return len(ret.items)

    def _delete_successfully_terminated_pods(self) -> None:
        self.v1.delete_collection_namespaced_pod(namespace=self.namespace,
                                                 pretty=True,
                                                 timeout_seconds=self.timeout,
                                                 field_selector=self.POD_SELECTOR)


class WorkflowRemover:
    """WorkflowRemover"""

    API_GROUP = "argoproj.io"
    API_VERSION = "v1alpha1"
    CRD_PLURAL = "workflows"

    def __init__(self,
                 namespace: str,
                 success_threshold: int,
                 age_in_days_threshold: int = 7,
                 timeout: int = 60):
        """
        Initialize WorkflowRemover

        :param namespace: from which workflows should be removed
        :param success_threshold: delete succeded workflows once the threshold is reached
        :param age_in_days_threshold: diff of days after which failed workflows should be deleted
        :param timeout: optional request timeout for kube API
        """
        self.custom_objects_api = self._init_kube_extensions_api()
        self.success_threshold = success_threshold
        self.age_in_days_threshold = age_in_days_threshold
        self.namespace = namespace
        self.timeout = timeout

    def remove_workflows(self) -> None:
        """Removes all succeeded workflows from the cluster if the defined threshold is reached.
        Removes all failed workflows older than one week from the cluster.
        """
        try:
            ret = self.custom_objects_api.list_namespaced_custom_object(group=self.API_GROUP,
                                                                        version=self.API_VERSION,
                                                                        namespace=self.namespace,
                                                                        timeout_seconds=self.timeout,
                                                                        plural=self.CRD_PLURAL)
            workflows = ret['items']
            succeeded_workflows = [wf for wf in workflows if
                                   self._is_suceeded(wf)]
            success_count = len(succeeded_workflows)

            if success_count >= self.success_threshold:
                logger.info(
                    f"Succeeded workflow count: {success_count} over the threshold of {self.success_threshold}")
                self._delete_terminated_workflows(succeeded_workflows)
                logger.info("Successfully deleted all succeeded workflows")

            error_workflows_older_than_one_week = [wf for wf in workflows if self._is_failed(wf) and self._is_older_than(wf, self.age_in_days_threshold)]
            self._delete_terminated_workflows(error_workflows_older_than_one_week)
            logger.info("Successfully deleted all failed workflows older than one week")

        except Exception as exp:
            logger.error(f"Error occurred while removing workflows. Exp: {exp}")
            raise exp

    def _init_kube_extensions_api(self):
        config.load_incluster_config()
        return client.CustomObjectsApi()

    def _delete_terminated_workflows(self, workflow_list: list) -> None:
        for workflow in workflow_list:
            workflow_name = workflow['metadata']['name']
            self.custom_objects_api.delete_namespaced_custom_object(group=self.API_GROUP,
                                                                    version=self.API_VERSION,
                                                                    namespace=self.namespace,
                                                                    plural=self.CRD_PLURAL,
                                                                    name=workflow_name,
                                                                    body=V1DeleteOptions())

    def _is_suceeded(self, workflow) -> bool:
        status = workflow.get('status')
        if not status:
            return False
        return status.get('phase') == 'Succeeded'

    def _is_failed(self, workflow) -> bool:
        status = workflow.get('status')
        if not status:
            return False
        return status.get('phase') == 'Error'

    def _get_finished_at(self, workflow):
        status = workflow.get('status')
        if not status:
            return None
        return datetime.strptime(status.get('finishedAt'), "%Y-%m-%dT%H:%M:%SZ")

    def _is_older_than(self, workflow, days_diff: int) -> bool:
        finished_time = self._get_finished_at(workflow)

        if not finished_time:
            return False

        time_diff = datetime.now() - finished_time
        return time_diff.days > days_diff
