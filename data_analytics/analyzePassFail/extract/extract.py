import rosbag

bag = rosbag.Bag('20171208_113350_.bag')

topic = '/CalService/instance_0/GPSInfoInterface'

filename = 'Out'+topic.replace("/","_")+'.csv'


with open(filename, 'w') as f:
  for msg in bag.read_messages(topics=[topic]):
    print(msg.message)
    #print >> f, msg.message.longitude
    f.write(str(msg.message.timestamp.s)+","
           +str(msg.message.timestamp.ns)+","
           +str(msg.message.latitude)+","
           +str(msg.message.longitude)+","
           +str(msg.message.altitude)+","
           +str(msg.message.heading)+","
           +str(msg.message.speed)+","
           +str(msg.message.climb)
           +"\n")


f.close()
bag.close()    
