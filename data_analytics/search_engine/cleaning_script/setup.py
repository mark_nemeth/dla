"""Setup file"""
from setuptools import setup, find_packages
from config.constants import VERSION

setup(
    name='cleaning_script',
    version=VERSION,
    packages=find_packages(),
    test_suite="tests",
    install_requires=[
        'requests>=2.21.0',
        'DANFUtils>=0.2.29',
        'ContainerClient>=0.6.102',
        'coverage>=4.5.4'
    ]
)
