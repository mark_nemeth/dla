#ifndef RDMA_COMMON_H
#define RDMA_COMMON_H

#include <exception>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <rdma/rdma_cma.h>
#include "hps.h"

#define CACHE_LINE_SIZE				(64)

#define RDMA_SEND_BLOCK_SIZE		(1024llu * 1024llu)
#define RDMA_BLOCK_SIZE				(1024llu * 1024llu)
#define RDMA_BUFFER_SIZE			(1024 * 1024llu * 1024llu)
#define RDMA_BLOCK_COUNT			(RDMA_BUFFER_SIZE / RDMA_BLOCK_SIZE)

#define RDMA_CLIENT_SR_THRESHOLD	(16 * RDMA_SEND_BLOCK_SIZE)
#define RDMA_CLIENT_GET_C_TO_MS		(10)
#define RDMA_CLIENT_MAX_SEND_WR		(128)
#define RDMA_CLIENT_MAX_RECV_WR		(4)
#define RDMA_SERVER_MAX_SEND_WR		(4)
#define RDMA_SERVER_MAX_RECV_WR		(4)
#define RDMA_MIN_CQE				(64)
#define RDMA_QCB_SIZE				(4096)
#define RDMA_POLL_CQ_BATCH_SIZE		(16)
#define RDMA_TIMEOUT_MS				(500)
#define DEBUG						(0)

#define TEST_NZ(x) do { if ((x)) { perror("Error: " #x " failed" ); throw exception(); } } while (0)
#define TEST_Z(x)  do { if (!(x)) { perror("Error: " #x " failed"); throw exception(); } } while (0)
#define TEST_E(x)  do { int e = (x); if (e) { printf("Error: " #x " failed (%s)\n", strerror(x)); throw exception(); } } while (0)

struct queueControlBlock
{
	// Complete offset (incremented in BLOCK_SIZE granularity)
	volatile long long __attribute__((aligned(CACHE_LINE_SIZE))) c;
	// Read offset
	volatile long long __attribute__((aligned(CACHE_LINE_SIZE))) r;
	// Write offset
	volatile long long __attribute__((aligned(CACHE_LINE_SIZE))) w;
};

enum message_id
{
	MSG_INVALID = 0, MSG_MR, MSG_UPDATE_C, MSG_DONE
};

struct remoteBufferInfo
{
	uint64_t addr;
	uint64_t size;
	uint32_t rkey;
};

struct message
{
	int id;

	union
	{
		remoteBufferInfo mr;
		struct
		{
			uint64_t c;
		} update;
	} data;
};

#endif
