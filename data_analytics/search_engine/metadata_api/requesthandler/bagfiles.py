"""Search Engine and Metadata REST API bagfiles route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from handlers.auth import auth
from handlers.bagfiles import BagFilesHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


class BagfilesRequestHandler(BaseRequestHandler):
    """Request handler for bagfile route: /bagfile"""

    _schema_post = BaseRequestHandler.load_json_schema(
        'bagfile_metadata_post.schema')

    bagfile_handler = BagFilesHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('bagfiles/post',
                                      context_headers=self.request.headers)

        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_post)
        guid = self.bagfile_handler.add_bagfile_to_db(data)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results='Bagfile(s) succesfully added to db.',
            status_code=constants["REQ_STATUS_CODE_RESOURCE_CREATED"],
            location="/bagfile/" + guid)


class BagfileRequestHandler(BaseRequestHandler):
    """Request handler for bagfile route: /bagfile/{guid}"""

    _schema_guid = BaseRequestHandler.load_json_schema(
        'guid.schema')
    _schema_patch = BaseRequestHandler.load_json_schema(
        'bagfile_metadata_patch.schema')

    bagfile_handler = BagFilesHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET, PATCH, DELETE')

    @auth.requires_entitlements(["openid"])
    def get(self, guid):
        tracing.start_task_in_context('bagfiles/get',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}',
                           self._schema_guid)
        res = self.bagfile_handler.get_bagfile_from_db(
            guid,
            whitelist=self.get_query_arguments('whitelist'))

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def patch(self, guid):
        tracing.start_task_in_context('bagfiles/patch', context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_patch)
        self.bagfile_handler.update_bagfile_in_db(guid, data)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results="Bagfile updated",
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def delete(self, guid):
        tracing.start_task_in_context('bagfiles/delete', context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        cascade_to_list = self.get_query_arguments("cascade_to")
        hard_delete = self.get_query_argument("hard_delete", default="False") == 'True'
        res = self.bagfile_handler.delete_bagfile_from_db(guid, cascade_to_list, hard_delete)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
