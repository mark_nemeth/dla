__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

from model.paths import BlobStoragePath, MapRPath


class TestBlobStoragePath(unittest.TestCase):

    STORAGE = "storage"
    CONTAINER = "container"
    FILE = "file.bag"

    def test_is_file_should_return_true_if_path_is_file(self):
        p = BlobStoragePath(f"/{self.STORAGE}/{self.CONTAINER}/{self.FILE}")
        self.assertTrue(p.is_file())

    def test_is_file_should_return_false_if_path_is_not_file(self):
        p = BlobStoragePath(f"/{self.STORAGE}/{self.CONTAINER}")
        self.assertFalse(p.is_file())

    def test_is_container_should_return_true_if_path_is_container(self):
        p = BlobStoragePath(f"/{self.STORAGE}/{self.CONTAINER}")
        self.assertTrue(p.is_container())

    def test_is_container_should_return_false_if_path_is_not_container(self):
        p = BlobStoragePath(f"/{self.STORAGE}/{self.CONTAINER}/{self.FILE}")
        self.assertFalse(p.is_container())

    def test_is_storage_account_should_return_true_if_path_is_storage_account(self):
        p = BlobStoragePath(f"/{self.STORAGE}")
        self.assertTrue(p.is_storage_account())

    def test_is_storage_account_should_return_false_if_path_is_not_storage_account(self):
        p = BlobStoragePath(f"/{self.STORAGE}/{self.CONTAINER}/{self.FILE}")
        self.assertFalse(p.is_storage_account())

    def test_parsing_should_parse_file_path(self):
        p = BlobStoragePath(f"/{self.STORAGE}/{self.CONTAINER}/{self.FILE}")
        self.assertEqual(p.storage_account, self.STORAGE)
        self.assertEqual(p.container, self.CONTAINER)
        self.assertEqual(p.file, self.FILE)

    def test_parsing_should_parse_container_path(self):
        p = BlobStoragePath(f"/{self.STORAGE}/{self.CONTAINER}")
        self.assertEqual(p.storage_account, self.STORAGE)
        self.assertEqual(p.container, self.CONTAINER)
        self.assertIsNone(p.file)

    def test_parsing_should_parse_storage_account_path(self):
        p = BlobStoragePath(f"/{self.STORAGE}")
        self.assertEqual(p.storage_account, self.STORAGE)
        self.assertIsNone(p.container, self.CONTAINER)
        self.assertIsNone(p.file, self.FILE)

    def test_parsing_should_ignore_trailing_slash(self):
        p = BlobStoragePath(f"/{self.STORAGE}/")
        self.assertEqual(p.storage_account, self.STORAGE)
        self.assertIsNone(p.container, self.CONTAINER)
        self.assertIsNone(p.file, self.FILE)

    def test_parsing_when_no_trailing_slash_should_parse(self):
        p = BlobStoragePath(self.STORAGE)
        self.assertEqual(p.storage_account, self.STORAGE)
        self.assertIsNone(p.container, self.CONTAINER)
        self.assertIsNone(p.file, self.FILE)

    def test_parsing_should_raise_exception_on_too_many_segments(self):
        self.assertRaises(ValueError, BlobStoragePath, "/a/b/c/d")


class TestMapRPathPath(unittest.TestCase):

    CLUSTER = '/mapr/624.mbc.us'
    FILE = 'file_ORIGINAL.bag'

    def test_parsing_should_parse_tmp_zone_path_to_folder(self):
        p = MapRPath(f"{self.CLUSTER}/tmp/x/y/z")
        self.assertEqual(self.CLUSTER, p.cluster)
        self.assertEqual('tmp', p.zone)
        self.assertIsNone(p.vin)
        self.assertIsNone(p.year)
        self.assertIsNone(p.month)
        self.assertIsNone(p.day)
        self.assertIsNone(p.file)

    def test_parsing_should_parse_tmp_zone_path_to_folder(self):
        p = MapRPath(f"{self.CLUSTER}/tmp/x/y/z/{self.FILE}")
        self.assertEqual(self.CLUSTER, p.cluster)
        self.assertEqual('tmp', p.zone)
        self.assertIsNone(p.vin)
        self.assertIsNone(p.year)
        self.assertIsNone(p.month)
        self.assertIsNone(p.day)
        self.assertEqual(self.FILE, p.file)

    def test_parsing_should_parse_buffer_zone_path_to_file(self):
        p = MapRPath(f"{self.CLUSTER}/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A383190/2020/02/20/{self.FILE}")
        self.assertEqual(self.CLUSTER, p.cluster)
        self.assertEqual('buffer', p.zone)
        self.assertEqual('WDD2221591A383190', p.vin)
        self.assertEqual('2020', p.year)
        self.assertEqual('02', p.month)
        self.assertEqual('20', p.day)
        self.assertEqual(self.FILE, p.file)

    def test_parsing_should_parse_input_zone_path_to_file(self):
        p = MapRPath(f"{self.CLUSTER}/data/input/rd/athena/08_robotaxi/plog/WDD2221591A383190/2020/02/20/{self.FILE}")
        self.assertEqual(self.CLUSTER, p.cluster)
        self.assertEqual('input', p.zone)
        self.assertEqual('WDD2221591A383190', p.vin)
        self.assertEqual('2020', p.year)
        self.assertEqual('02', p.month)
        self.assertEqual('20', p.day)
        self.assertEqual(self.FILE, p.file)

    def test_parsing_should_parse_buffer_zone_path_to_day_level_folder(self):
        p = MapRPath(f"{self.CLUSTER}/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A383190/2020/02/20")
        self.assertEqual(self.CLUSTER, p.cluster)
        self.assertEqual('buffer', p.zone)
        self.assertEqual('WDD2221591A383190', p.vin)
        self.assertEqual('2020', p.year)
        self.assertEqual('02', p.month)
        self.assertEqual('20', p.day)
        self.assertIsNone(p.file)

    def test_parsing_should_parse_buffer_zone_path_to_subfolder_in_day_level_folder(self):
        p = MapRPath(f"{self.CLUSTER}/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A383190/2020/02/20/somedir")
        self.assertEqual(self.CLUSTER, p.cluster)
        self.assertEqual('buffer', p.zone)
        self.assertEqual('WDD2221591A383190', p.vin)
        self.assertEqual('2020', p.year)
        self.assertEqual('02', p.month)
        self.assertEqual('20', p.day)
        self.assertIsNone(p.file)

    def test_parsing_should_parse_buffer_zone_path_to_file_in_subfolder_in_day_level_folder(self):
        p = MapRPath(f"{self.CLUSTER}/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A383190/2020/02/20/somedir/{self.FILE}")
        self.assertEqual(self.CLUSTER, p.cluster)
        self.assertEqual('buffer', p.zone)
        self.assertEqual('WDD2221591A383190', p.vin)
        self.assertEqual('2020', p.year)
        self.assertEqual('02', p.month)
        self.assertEqual('20', p.day)
        self.assertEqual(self.FILE, p.file)

    def test_parsing_should_not_parse_buffer_zone_path_to_month_level_folder(self):
        self.assertRaises(ValueError, MapRPath, f"{self.CLUSTER}/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A383190/2020/02")

    def test_parsing_should_not_parse_buffer_zone_path_sym_link_path(self):
        self.assertRaises(ValueError, MapRPath, f"{self.CLUSTER}/data/buffer/rd/athena/08_robotaxi/plog/byName/v222-5415/2020/02/20/{self.FILE}")

    def test_parsing_should_not_parse_buffer_zone_non_plog_path(self):
        self.assertRaises(ValueError, MapRPath, f"{self.CLUSTER}/data/buffer/rd/athena/08_robotaxi/lidar/WDD2221591A383190/2020/02/20/{self.FILE}")

    def test_parsing_should_not_parse_non_buffer_input_tmp_path(self):
        self.assertRaises(ValueError, MapRPath, f"{self.CLUSTER}/data/xxx/rd/athena/08_robotaxi/plog/WDD2221591A383190/2020/02/20/{self.FILE}")

    def test_parsing_should_not_parse_path_without_cluster(self):
        self.assertRaises(ValueError, MapRPath, f"/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A383190/2020/02/20/{self.FILE}")


if __name__ == '__main__':
    unittest.main()
