"""Handler class for flow search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

from handlers.db_data_handler import DBDataHandler

logger = logging.getLogger(__name__)


class FlowSearchHandler:

    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def create_search_kwargs(self, query_args):
        search_kwargs = {
            'query': None,
            'limit': 50,
            'offset': 0,
            'sortby': 'guid'
        }
        query = {}
        for key, value in query_args.items():
            # Query filters
            if key in ('guid', 'name', 'version', 'flow_version', 'kubeflow_experiment_id'):
                query[key] = value[0]

            # Pagination
            elif key == 'limit':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning('Invalid limit query parameter in flow search - integer expected')
            elif key == 'offset':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning('Invalid offset query parameter in flow search - integer expected')

            # Sort
            elif key == 'sortby':
                # Ascending order
                if value[0] in ['guid', 'version', 'name', 'flow_version', 'kubeflow_experiment_id']:
                    search_kwargs[key] = value[0]
                elif value[0] in ['+guid', '+version', '+name', '+flow_version', '+kubeflow_experiment_id']:
                    search_kwargs[key] = value[0][1:]
                # Descending order
                elif value[0] in ['-guid', '-version', '-name', '-flow_version', '-kubeflow_experiment_id']:
                    search_kwargs[key] = [(value[0][1:], -1)]

            # no match
            else:
                logger.warning(f"unexpected parameter in flow search: {key}, value: {value}")

        search_kwargs['query'] = query
        return search_kwargs

    def perform_search(self, **search_kwargs):
        return self.db_data_handler.search_flows(**search_kwargs)
