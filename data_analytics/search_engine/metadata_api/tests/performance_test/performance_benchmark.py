import requests
import json

"""Unittests for metadata-api"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
# connection config for tests
# can change the localhost into external ip:
bagfile_url = 'http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/bagfile/'
sequence_url = 'http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/sequence/'
feature_bucket_url = 'http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/featurebucket/'
extractorrequest_url = 'http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/extractorrequest/'

raw_data_bagfile = {
    "version": "1.0",
    "guid": "",
    "link": "XXXXXX",
    "size": 21231,
    "num_messages": 4444,
    "start": 1554800000000,
    "end": 1557848250000,
    "duration": 3048250000,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1"
}

raw_data_sequence = {
    "version": "1.0",
    "guid": "",
    "index": 0,
    "bagfile_guid": raw_data_bagfile['guid'],
    "start": 1555847224000,
    "end": 1557847225000,
    "vehicle_id_num": "V-123-234",
    "type": "basic",
    "extractor": "ext1"
}


raw_data_extraction = {
    "version": "1.0",
    "bagfile_guid": raw_data_bagfile['guid'],
    "extractor_id": "ext1",
    "metadata_list": [
        {
            "start": 1555847224000,
            "end": 1557847260000,
            "metadata": {"key1": "sfdsfsdjf2387hdjfsd",
                         "about": "Voluptate elit velit in enim adipisicing ea laboris enim aute velit nulla culpa Lorem. Eiusmod exercitation amet consequat voluptate id dolore est dolor sunt officia nulla. Dolore consectetur consequat do laborum in qui dolor excepteur aute aute eiusmod. Irure velit esse nostrud Lorem quis sunt do. Magna minim nostrud veniam ullamco labore occaecat id exercitation esse ea mollit occaecat ex eiusmod. Sit nostrud laboris amet est deserunt sint exercitation.\r\n",
                         "registered": "2014-05-09T04:56:33 -02:00",
                         "latitude": -78.65933,
                         "longitude": -27.883847,
                         "tags": [
                             "non",
                             "reprehenderit",
                             "ut",
                             "irure",
                             "commodo",
                             "Lorem",
                             "veniam"
                         ]
            }
        }
    ]
}


feature_bucket = {
    "version":
    "1.0",
    "guid":
    "22221111-aaaa-bbbb-cccc-999999999999",
    "bagfile_guid":
    raw_data_bagfile["guid"],
    "start":
    1563262908000,
    "end":
    1563262911001,
    "start_index":
    60,
    "end_index":
    119,
    "tags": ["", ""],
    "features": [{
        "ts": 1563262908000,
        "vel": 0.5,
        "acc": 0.3,
        "eng_state": 11
    }, {
        "ts": 1563262908000,
        "vel": 0.6,
        "acc": 0.2,
        "eng_state": 5
    }],
    "is_test_data":
    1
}


# generate a list of time windows
def get_time_windows(n, size):
    tws = []
    start = 1555847224000
    for i in range(1, n+1):
        end = start+size*i
        tws.append([start, end])
        start = end+1
    return tws


class Preparation():
    # prepare to insert bagfile, feature_buckets and sequences into the db
    def test_post_bagfile(self):
        res = requests.post(url=bagfile_url, json=raw_data_bagfile)
        uuid = res.headers['Location'].split('/')[2]

        raw_data_sequence["bagfile_guid"] = uuid
        feature_bucket["bagfile_guid"] = uuid
        raw_data_bagfile["guid"] = uuid
        raw_data_extraction["bagfile_guid"] = uuid

        print("bagfile post:{}".format(res.content))

    def test_post_sequence(self):
        res = requests.post(url=sequence_url, json=raw_data_sequence)
        uuid = res.headers['Location'].split('/')[2]
        raw_data_sequence["guid"] = uuid

        print("sequenece post:{}".format(res.content))

    def test_post_feature_bucket(self):
        res = requests.post(url=feature_bucket_url, json=feature_bucket)
        uuid = res.headers['Location'].split('/')[2]
        feature_bucket["guid"] = uuid

        print("feature bucket post:{}".format(res.content))

class TestPostSizeofTimeWindow():
    def test_post_extraction_request(self, n=150, step_size=1000):
        """
        used to test the metadata-api with different size of time window
        :param n int: the number of test cased
        :param step_size int: the basic time window(min 1000)
        """
        if step_size < 1000:
            print("the step_size must >= 1000")
            return
        tws = get_time_windows(n, step_size)
        times = []
        results = []
        for pair in tws:
            raw_data_extraction["metadata_list"][0]["start"] = pair[0]
            raw_data_extraction["metadata_list"][0]["end"] = pair[1]
            res = requests.post(url=extractorrequest_url, json=raw_data_extraction)
            times.append(res.elapsed.total_seconds())
            results.append(res.status_code)

        data = dict()
        data["times"] = times
        data["res_code"] = results
        with open('test_time_windows_size_{}.json'.format(step_size), 'w') as outfile:
            json.dump(data, outfile)

    def delet_all_test(self):
        uuid_bagfile = raw_data_bagfile['guid']
        requests.delete(bagfile_url + uuid_bagfile)

        uuid_seq = raw_data_sequence['guid']
        requests.delete(sequence_url + uuid_seq)

        uuid_feat = feature_bucket['guid']
        requests.delete(feature_bucket_url + uuid_feat)
# test with in


class TestPostNum():
    """
    used to test metadata api with different length of post requests
    :param n int: the test cases
    """
    def test_post_extraction_request(self, n=50):
        data = {
            "start": 1555847224000,
            "end": 1555847225000,
            "bva": 7
        }

        raw_data_extraction["metadata_list"] = []
        times = []
        results = []
        for i in range(1, n+1):
            raw_data_extraction["metadata_list"].append(data)
            res = requests.post(url=extractorrequest_url, json=raw_data_extraction)
            times.append(res.elapsed.total_seconds())
            results.append(res.status_code)

        data = {}
        data["times"] = times
        data["res_code"] = results
        with open('test_req_list_size_{}.json'.format(n), 'w') as outfile:
            json.dump(data, outfile)


if __name__ == '__main__':
    pre = Preparation()
    pre.test_post_bagfile()
    pre.test_post_sequence()
    pre.test_post_feature_bucket()

    exp=TestPostSizeofTimeWindow()
    exp.test_post_extraction_request(150, 1000)

    post_num = TestPostNum()
    post_num.test_post_extraction_request(50)

    exp.delet_all_test()