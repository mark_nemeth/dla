import React, {useEffect, useState, useRef} from 'react';
import LabelContainer from '../../../Utils/LabelContainer/LabelContainer';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import FormControl from 'react-bootstrap/FormControl';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Overlay from 'react-bootstrap/Overlay';
import Popover from 'react-bootstrap/Popover';
import Tooltip from 'react-bootstrap/Tooltip';
import {isStringValid} from '../../../../utils/helperFunctions';
import './ByTopicFilter.scss'

export function ByTopicFilter(props) {

  const searchBox = useRef(null);
  const resultList = useRef(null);
  const [suggestionsOpen, setSuggestionsOpen] = useState(false);
  const [suggestionList, setSuggestionList] = useState(props.topicList);

  useEffect(() => {
    if (suggestionsOpen) document.addEventListener("mousedown", handleClickOutside);
    else document.removeEventListener("mousedown", handleClickOutside);
    return () => document.removeEventListener("mousedown", handleClickOutside);
  }, [suggestionsOpen]);

  function handleClickOutside(event) {
    if (resultList != null && resultList.current != null && resultList.current.contains(event.target)) return;
    setSuggestionsOpen(false);
  }

  function onSearchTopicInputChange(event) {
    props.setCurrentTopicSearchQuery(event.target.value);
    if (!isStringValid(event.target.value)) setSuggestionList(props.topicList);
    else setSuggestionList(props.topicList.filter(topic => topic.toLowerCase().includes(event.target.value.toLowerCase())));
    setSuggestionsOpen(true);
  }

  function onTopicSelect(topic) {
    if (props.selectedTopicList.includes(topic)) props.filterByTopics(props.selectedTopicList.filter(t => t !== topic));
    else props.filterByTopics(props.selectedTopicList.concat(topic));
  }

  function resetTopicSelection() {
    props.setCurrentTopicSearchQuery();
    props.filterByTopics();
  }

  const showLoading = props.topicList === undefined || props.topicList.length <= 0;
  const subLabel = props.selectedTopicList.length + ' selected';
  const active = props.selectedTopicList.length > 0;

  const selectedTopicItemComponent = (item) => (
    <OverlayTrigger placement="right" delay={{ show: 400, hide: 0 }} outOfBoundaries overlay={
      <Tooltip id={item} className="select-topic-tooltip">{item}</Tooltip>}>
      <div className="select-topic-item">
        <div className="icon-subtract" onClick={() => onTopicSelect(item)}/>
        <div className="select-topic-label">{item}</div>
      </div>
    </OverlayTrigger>
  );

  const suggestionItemComponent = (item) => (
    <label className={props.selectedTopicList.includes(item) ? 'suggestion-item-active' : 'suggestion-item'}>
      <input type="checkbox" value={item} onChange={() => onTopicSelect(item)}
             checked={props.selectedTopicList.includes(item)}/>
      &nbsp; {item}
    </label>
  );

  return (
    <CollapseContainer label={active > 0 ? "Topic: " + subLabel : "Topic"}
                       active={active}
                       variant="list"
                       resetCallback={resetTopicSelection}
    >
      <>
        <div className="suggestion-box">
          <FormControl ref={searchBox}
                       placeholder="Search by topic ..."
                       aria-label="Search query"
                       aria-describedby="basic-addon2"
                       value={props.topicSearchQuery}
                       onChange={onSearchTopicInputChange}
                       onClick={onSearchTopicInputChange}
          />

          {suggestionList.length > 0 &&
          <Overlay target={searchBox.current} show={suggestionsOpen} placement="top">
            <Popover id="suggestion-list-overflow-container">
              <div ref={resultList} className="suggestion-item-list">
                <ScrollList striped={false} listHeight='250px'
                            showLoading={showLoading}
                            itemList={suggestionList}
                            listItemComponent={suggestionItemComponent}
                />
              </div>
            </Popover>
          </Overlay>
          }
        </div>

        {props.selectedTopicList.length > 0 &&
        <LabelContainer variant="card-outline label-inner-container" label="Selected topics">
          <ScrollList showLoading={false}
                      itemList={props.selectedTopicList}
                      listItemComponent={selectedTopicItemComponent}
          />
        </LabelContainer>
        }
      </>
    </CollapseContainer>
  );
}