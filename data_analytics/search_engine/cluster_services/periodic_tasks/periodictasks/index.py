__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse

from DANFUtils.logging import logger

from periodictasks.tasks import registry


def _get_commandline_args():
    parser = argparse.ArgumentParser(description='Executes a set of tasks')
    parser.add_argument('--tasks',
                        action='store',
                        nargs='*',
                        required=True,
                        help=f"Specify the list of tasks to execute. "
                             f"Currently supported tasks: "
                             f"{', '.join(registry.keys())}")
    return parser.parse_args()


def main(args):
    tasks = args.tasks
    logger.info(f"Executing tasks: {tasks}")

    for task_name in tasks:
        task = registry.get(task_name)
        if not task:
            logger.warning(f"skipping unknown task: {task_name}")
            continue
        try:
            task()
        except Exception as e:
            logger.exception(f"Failed to execute task {task} due to: {e}")


if __name__ == '__main__':
    logger.info("################################")
    logger.info("######## Periodic tasks ########")
    logger.info("################################")
    cli_args = _get_commandline_args()
    main(cli_args)
