import React from 'react';
import Support from './Support';
import {shallow} from 'enzyme/build';

describe('Support component', () => {

  it('should renders without crashing', () => {
    const wrapper = shallow(<Support />);
    expect(wrapper.exists()).toBe(true);
  });

});