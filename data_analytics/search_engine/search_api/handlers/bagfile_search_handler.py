"""Handler class for bagfile search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

from handlers.db_data_handler import DBDataHandler

logger = logging.getLogger(__name__)


class BagfileSearchHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def create_search_kwargs(self, query_args):
        # try, except parsing
        def do_parsing(k, v, funcs):
            parsed = None
            try:
                parsed = funcs[k](v)
            except ValueError:
                logger.warning(
                    f'Invalid {k} query parameter in bagfile search - integer expected'
                )
            except:
                logger.error(
                    f'unexpected error in (key,value pair): ({k}, {v})'
                )
            return parsed

        #  Parsing functionality keywords with default values for
        default_kwargs = {
            'limit': 50,
            'offset': 0,
            'sortby': 'guid'
        }
        _kwrds_func = {
            'limit': lambda value: abs(int(value[0])),
            'offset': lambda value: abs(int(value[0]))
        }

        # Parsing functionality keywords with value to be appended in the 'query' dictionary
        _query_func = {
            'guid': lambda value: value[0],
            'is_deleted': lambda value: self.create_is_deleted_query(value[0]),
            'file_type': lambda value: self.create_vin_query(value),
            'processing_state': lambda value: self.create_processing_state_query(value),
            'vehicle_id_num': lambda value: self.create_vin_query(value),
            'topics': lambda value: self.create_topic_query(value),
            'file_name': lambda value: {'$regex': f'{value[0]}', '$options': 'i'},
            'link': lambda value: {'$regex': f'{value[0]}'.replace('\\', '\\\\'), '$options': 'i'},
            'current_link': lambda value: {'$regex': f'{value[0]}'.replace('\\', '\\\\'), '$options': 'i'},
            'any_link': lambda value: [{"link": {"$regex": f'{value[0]}'.replace('\\', '\\\\'), '$options': 'i'}},
                                       {"current_link": {""'$regex': f'{value[0]}'.replace('\\', '\\\\'), '$options': 'i'}}]
        }

        # Parsing functionality keywords with range to be appended in the '$and' dictionary
        _range_func = {
            'size[gte]': lambda value: {'size': {"$gte": abs(int(value[0]))}},
            'size[lte]': lambda value: {'size': {"$lte": abs(int(value[0]))}},
            'start[gte]': lambda value: {'start': {"$gte": abs(int(value[0]))}},
            'start[lte]': lambda value: {'start': {"$lte": abs(int(value[0]))}},
            'duration[gte]': lambda value: {'duration': {"$gte": abs(int(value[0]))}},
            'duration[lte]': lambda value: {'duration': {"$lte": abs(int(value[0]))}}
        }

        # Parsing functionality keywords with range to be appended in the '$or' dictionary
        _any_query_func = {
            'drive_types': lambda value: list(map(lambda v: {'drive_types': str(v)}, value))
        }

        # Parsing functionality keywords ascending or descending to be appended in the 'sortby' dictionary
        def _sortby_key_value(value):
            whitelist = ['guid', 'start', 'duration', 'vehicle_id_num', 'size', 'processing_priority',
                         'processing_state']
            whitelistp = list(map(lambda x: '+'+str(x), whitelist))
            whitelistn = list(map(lambda x: '-' + str(x), whitelist))

            # Ascending order
            if value[0] in whitelist:
                key_value = value[0]
            elif value[0] in whitelistp:
                key_value = value[0][1:]
            # Descending order
            elif value[0] in whitelistn:
                key_value = [(value[0][1:], -1)]
            else:
                key_value = None

            return key_value

        _sortby_func = {
            'sortby': lambda value: _sortby_key_value(value)
        }

        # Actual Parsing
        # correct the schema before parsing
        if 'file_path' in query_args:
            query_args['link'] = query_args.pop('file_path')

        # By default we should return not deleted documents
        if 'is_deleted' not in query_args:
            query_args['is_deleted'] = ['false']

        # apply parsing function to 'query' keywords
        query = {k: do_parsing(k, v, _query_func) for k, v in query_args.items() if k in _query_func}
        query = {k: v for k, v in query.items() if v}
        if "any_link" in query:
            query["$or"] = query.pop("any_link")


        # apply parsing function to 'default' keywords
        keywrds = {k: do_parsing(k, v, _kwrds_func) for k, v in query_args.items() if k in _kwrds_func}
        keywrds = {k: v for k, v in keywrds.items() if v}
        residual_keywrds = {k: v for k, v in default_kwargs.items() if k not in keywrds}
        keywrds = {**keywrds, **residual_keywrds}

        # apply parsing function to '$and' keywords
        search_range = [do_parsing(k, v, _range_func) for k, v in query_args.items() if k in _range_func]
        search_range = [v for v in search_range if v]
        search_range = {'$and': search_range} if len(search_range) > 0 else {}

        # apply parsing function to '$or' keywords
        search_any = [do_parsing(k, v, _any_query_func) for k, v in query_args.items() if k in _any_query_func]
        search_any = [v for sublist in search_any for v in sublist if v]
        search_any = {'$or': search_any} if len(search_any) > 0 else {}

        # apply parsing function to 'sortby' keywords
        sortby_kwrd = {k: do_parsing(k, v, _sortby_func) for k, v in query_args.items() if k in _sortby_func}
        sortby_kwrd = {k: v for k, v in sortby_kwrd.items() if v}
        sort_default = {k: v for k, v in sortby_kwrd.items() if k not in sortby_kwrd}
        sortby_kwrd = {**sortby_kwrd, **sort_default}

        return {
            'query': {
                **query,       # keywords with value
                **search_range, # keywords with range
                **search_any # keywords with 'ANY'
            },
            **keywrds,     # keywords with default
            **sortby_kwrd # keywords ascending or descending
        }


    def create_search_query(self, query_fields):
        field_list = []
        for key, value in query_fields.items():
            if key == 'file_path':
                field_list.append({"link": {'$regex': f'{value}', '$options': 'i'}})
            elif key in ['file_name', 'current_link']:
                field_list.append({key: {'$regex': f'{value}', '$options': 'i'}})
            elif key == "any_link":
                field_list.append({"$or": [{"link": {"$regex": f'{value}', '$options': 'i'}},
                                           {"current_link":{""'$regex': f'{value}', '$options': 'i'}}]})
            elif key == 'topics':
                field_list.append({key: self.create_topic_query(value)})
            elif key == 'processing_state':
                field_list.append({key: self.create_processing_state_query(value)})
            elif key == 'vehicle_id_num':
                field_list.append({key: self.create_vin_query(value)})
            else:
                field_list.append({key: value})
        if len(field_list) > 1:
            return {"$and": field_list}
        elif len(field_list) == 1:
            return field_list[0]
        return {}

    @staticmethod
    def create_processing_state_query(processing_state_field):
        """
        to search for processing_state, query must be changed including an "$in"
        (true, if any state from query are in a database entry)
        :param processing_state_field: dict element containing the key == processing_state
        :return:
        """
        if isinstance(processing_state_field, list):
            processing_state_field = {"$in": processing_state_field}
        return processing_state_field

    @staticmethod
    def create_vin_query(vins_field):
        """
        to search for a list of VINs, query must be changed including an "$in"
        (true, if any VINs from query are in a database entry)
        :param vins_field: dict element containing the key == vin
        :return:
        """
        if isinstance(vins_field, list):
            vins_field = {"$in": vins_field}
        return vins_field

    @staticmethod
    def create_topic_query(topics_field):
        """
        to search for a list of topics, query must be changed including a "$all"
        (true, if all topics from query are in a database entry)
        :param topics_field: dict element containing the key == topics
        :return:
        """
        topics_field = {"$all": topics_field}
        return topics_field

    @staticmethod
    def create_is_deleted_query(is_deleted_field: str):
        """
        To either search for bagfiles that are not soft deleted (default)
        or to specifically search for the deleted ones.
        :param is_deleted_field: if 'false', then "$ne: true" returns bagfiles with is_deleted = false OR missing field
        :return:
        """
        if str.lower(is_deleted_field) == 'false':
            is_deleted_field = {"$ne": True}
        else:
            is_deleted_field = {"$eq": True}
        return is_deleted_field

    def perform_search(self, **search_kwargs):
        return self.db_data_handler.search_bagfiles(**search_kwargs)