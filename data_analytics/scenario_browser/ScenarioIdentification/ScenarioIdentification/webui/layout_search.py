import dash
import flask
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_table
import pandas as pd
import numpy as np

from data import pivot_file
filenames = pivot_file['Filename'].unique().tolist()
ego_feats = ['Accel', 'Steer_Angle', 'Turn_Ind',
       'Vel', 'Yaw']
dyn_feats = ['Bicycle', 'Human', 'LargeVehicle', 'Obstacle',
       'PassengerCar', 'Pedestrian', 'RidableVehicle', 'Rider', 'Truck',
       'TwoWheeler', 'Vehicle']


def create_search_layout(app, query):
    if 'fname' in query:
        fname = query['fname'][0]
    else:
        fname = None
    if 'ts' in query:
        ts = float(query['ts'][0])
    else:
        ts = None

    layout = html.Div(
        children=[
            html.Div(
                className="twelve columns",
                children=[
                    html.Div(
                        children=[
                            html.H1("Athena Scenario Search",
                                           style={
                                               'textAlign': 'center',
                                    }),
                            html.Div(id='hidden',children=[],style={'display': 'none'}),
                            html.Hr(),
                        ],
                    ),
                    html.Div(
                        children=[
                            html.Div(
                                className="eight columns",
                                children=[
                                    dcc.Dropdown(
                                        id='file-selector',
                                        options=[{'label': _, 'value': _} for _ in filenames],
                                        multi=False,
                                        value=fname,
                                        placeholder='Select a drive',
                                        style={
                                            'width':'100%',
                                            'padding-left':'3%'
                                        }
                                    ),
                                ]),
                            html.Div(
                                className="three columns",
                                children=[
                                    dcc.Input(id="timestamp-input",
                                              type='number', debounce=True,
                                              value=ts,
                                              placeholder='Timestamp',
                                             style={
                                                 'width':'50%',
                                                 'padding-left': '5%',
                                                 'padding-right':'5%'
                                             }),
                                    html.Button(id='search-button', n_clicks=0, children='Search',
                                               style={
                                                   'padding-left':'5%',
                                                   'width':'50%'
                                               })
                                ]
                            ),
                        ]
                    ),
                    html.Div(className='twelve columns',
                        children=[
                            html.Div(className="three columns"),
                            html.Div(
                                className="two columns",
                                children=[
                                    dcc.Dropdown(id='ego-feature-selector',
                                                 options=[{'label':_, 'value':_} for _ in ego_feats],
                                                 multi=True,
                                                 placeholder='Optional ego feature',
    #                                              style={
    #                                                  'width':'100%',
    # #                                                  'padding-left':'5%'
    #                                              }
                                                ),
                                ]
                            ),
                            html.Div(className="two columns",
                                children=[
                                    dcc.Dropdown(id='map-feature-selector',
                                                 options=[{'label':'Crosswalks', 'value':'CROSSWALK'},
                                                          {'label':'ParkingSpace', 'value':'PARKINGSPACE'}],
                                                 multi=True,
                                                 placeholder='Optional map feature',
    #                                              style={
    #                                                  'width':'100%',
    # #                                                  'padding-left':'5%'
    #                                              }
                                                ),
                                ]),
                            html.Div(className="two columns",
                                children=[
                                    dcc.Dropdown(id='obj-feature-selector',
                                                 options=[{'label':_, 'value':_} for _ in dyn_feats],
                                                 multi=True,
                                                 placeholder='Optional object feature',
    #                                              style={
    #                                                  'width':'100%',
    # #                                                  'padding-left':'5%'
    #                                              }
                                                ),
                                ]),
                            html.Div(className="three columns"),
                        ]
                    ),
                ],
            ),
            html.Div(
                className="twelve columns",
                children=[
                    html.Hr(),
                    html.H1("Search Results",
                           style={
                               'textAlign':'center'
                           }),
                    html.Div(id='search-results')
#                     dash_table.DataTable(id='search-results',
#                             style_as_list_view=False,
#                             style_cell_conditional=[
#                                 {'if': {'column_id': 'FileName'},
#                                  'width': '40%'},
#                                 {'if': {'column_id': 'TimeStamp'},
#                                  'width': '10%'},
#                             ],
#                             style_header={
#                                 'backgroundColor': 'rgb(30, 30, 30)',
#                                 'fontWeight': 'bold'
#                                          },
#                             style_cell={
#                                 'backgroundColor': 'rgb(50, 50, 50)',
#                                 'color': 'white',
#                                 'padding': '5px'
#                                 } 
#                             )
#                     generate_table(search_res)
                ]
            )
        ]
    )
    return layout