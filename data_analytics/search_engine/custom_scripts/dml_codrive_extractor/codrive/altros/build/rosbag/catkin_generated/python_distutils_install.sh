#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/app/altros/src/rosbag"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/app/altros/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/app/altros/install/lib/python2.7/dist-packages:/app/altros/build/rosbag/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/app/altros/build/rosbag" \
    "/usr/bin/python2" \
    "/app/altros/src/rosbag/setup.py" \
    build --build-base "/app/altros/build/rosbag" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/app/altros/install" --install-scripts="/app/altros/install/bin"
