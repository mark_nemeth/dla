"""Handler class for drives"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List

from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DB_Data_Handler


class DriveHandler:

    def __init__(self):
        self.db_data_handler = DB_Data_Handler()

    def add_drive_to_db(self, drive):
        drive['state'] = []
        self.db_data_handler.add_drives([drive])
        return drive['guid']

    def get_drive_from_db(self, guid):
        return self.db_data_handler.get_drives([guid])[0]

    def update_drive_in_db(self, guid, data):
        self.db_data_handler.update_drives([guid], [data])

    def delete_drive_from_db(self, guid, cascade_to_list, hard_delete):
        self.db_data_handler.delete_drives([guid], cascade_to_list, hard_delete)

    def update_field(self, drive_guids, values, field, append=False):
        self.db_data_handler.update_field(drive_guids, values, field, append)

    def add_state_event(self, guid, event):
        """Appends the given state event to the state event list of the
        metadata of the drive with the given guid.

        :param guid: drive to update
        :param event: to append to the drive metadata
        :return: None
        """
        drive = self.db_data_handler.get_drive([guid])[0]

        if 'state' not in drive:
            drive['state'] = []
        drive['state'].append(event)

        # remove not updateable fields
        drive.pop('guid', None)

        self.update_drive_in_db(guid, drive)
