import {connect} from 'react-redux';
import {filterByLaneChange} from '../../../../redux/Home/HomeActions';
import {ByLaneChangeFilter} from './ByLaneChangeFilter';

function mapStateToProps(state, ownProps) {
  const selected = state.filters['feat.number_of_lanechanges[gte]'] ? state.filters['feat.number_of_lanechanges[gte]'] !== 0 : false;
  return {selected};
}

export default connect(
  mapStateToProps,
  {filterByLaneChange}
)(ByLaneChangeFilter)