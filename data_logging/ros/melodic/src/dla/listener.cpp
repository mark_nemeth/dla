#include <ros/ros.h>
#include <std_msgs/String.h>
#include <getopt.h>
#include <stdio.h>
#include <string>
#include <sstream>


void data_callback(const std_msgs::String::ConstPtr& msg)
{
    // Nothing to be done here when just listening
}

int main(int argc, char *argv[])
{
    std::string topic_suffix = "";
    std::string topic = "SyntheticData";

    int opt;
    while ((opt = getopt(argc, argv, "t:")) != -1)
    {
        switch (opt)
        {
        case 't':
            topic_suffix = std::string(optarg);
            break;

        default:
            printf("listener -t <suffix>");
            abort ();
        }
    }

    ros::init(argc, argv, "listener", ros::init_options::AnonymousName);
    ros::NodeHandle n;

    ros::Subscriber sub = n.subscribe(topic.append(topic_suffix), 10, data_callback);

    ros::spin();

    return 0;
}

