#include <string.h>
#include <time.h>
#include "hps.h"

static int fileId = 0;

hps_type hps_get_protocol(const char *uri)
{
	char tmp[128];
	strncpy(tmp, uri, sizeof(tmp));
	char *p = strchr(tmp, ':');
	if (p != NULL)
		*p = 0;
	if (strcasecmp(tmp, "TCP") == 0)
		return HPS_TCP;
	else if (strcasecmp(tmp, "UDP") == 0)
		return HPS_UDP;
	else if (strcasecmp(tmp, "RAW") == 0)
		return HPS_RAW;
	else if (strcasecmp(tmp, "RDMA") == 0)
		return HPS_RDMA;
	else if (strcasecmp(tmp, "FILE") == 0)
		return HPS_AIO;
	else if (strcasecmp(tmp, "NULL") == 0)
		return HPS_NULL;
	else
		return HPS_NULL;
}

long long getTimestampNs()
{
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return (long long)t.tv_sec * 1000000000llu + (long long)t.tv_nsec;
}

char* getFileName(const char* out)
{
    char name[1024];
    char *s = name;
    memset(name, 0, sizeof(name));

    time_t t = time(NULL);
    struct tm *localTime = localtime(&t);

    for (int i = 0; out[i] != 0; i++)
    {
    	if (out[i] == '%')
    	{
    		i++;
    		if (out[i] == 'n')
    			s += sprintf(s, "%d", fileId++);
    		else if (out[i] == 'Y')
    			s += sprintf(s, "%4d", 1900 + localTime->tm_year);
    		else if (out[i] == 'm')
    			s += sprintf(s, "%02d", localTime->tm_mon);
    		else if (out[i] == 'd')
    			s += sprintf(s, "%02d", localTime->tm_mday);
    		else if (out[i] == 'h')
    			s += sprintf(s, "%02d", localTime->tm_hour);
    		else if (out[i] == 'i')
    			s += sprintf(s, "%02d", localTime->tm_min);
    		else if (out[i] == 's')
    			s += sprintf(s, "%02d", localTime->tm_sec);
    	}
    	else
    	{
    		*s++ = out[i];
    	}
    }
    return strdup(name);
}
