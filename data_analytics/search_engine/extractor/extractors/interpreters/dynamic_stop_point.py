"""Fallback Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019-2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from extractors.utils import rospytime2int, ros_timestamp2ms
from extractors.base_interpreter import Interpreter

BVA_DYNAMIC_STOP_POINT = 7
SECONDS_BEFORE_DYNAMIC_STOP_POINT = 5
SECONDS_AFTER_DYNAMIC_STOP_POINT = 5


class DynamicStopPointInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.is_dynamic_stop_point = False
        self.was_dynamic_stop_point = False
        self.is_engaged = False
        self.bva = BVA_DYNAMIC_STOP_POINT

    def feed_new_msg(self, topic, msg, t):
        dynamic_stop_point = {}

        if hasattr(msg, "nodes"):
            for single_node in msg.nodes:
                try:
                    enum = single_node.node.action.behavior_target.objective.stop_point_type
                    if enum.value == enum.kDynamic:
                        self.is_dynamic_stop_point = True
                except AttributeError:
                    pass

        if hasattr(msg, "eSystemStatus"):
            self.is_engaged = all([msg.eSystemStatus.eSystemState == 2
                                  and msg.eSystemStatus.eDegradationLevel == 2
                                  and msg.eSystemStatus.eOperationMode == 4])

        if self.is_dynamic_stop_point and not self.was_dynamic_stop_point \
                and self.is_engaged:
            dynamic_stop_point["ts"] = ros_timestamp2ms(msg.header.stamp)
            dynamic_stop_point = self.calc_start_end_ts_snippet(
                    dynamic_stop_point)
            self.event_list += [dynamic_stop_point]

        self.was_dynamic_stop_point = self.is_dynamic_stop_point

    @staticmethod
    def calc_start_end_ts_snippet(event):
        event["start_ts"] = int(event["ts"] - (SECONDS_BEFORE_DYNAMIC_STOP_POINT * 1e3))
        event["end_ts"] = int(event["ts"] + (SECONDS_AFTER_DYNAMIC_STOP_POINT * 1e3))
        event["ts"] = rospytime2int(event["ts"])
        return event
