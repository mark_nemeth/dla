"""Setup file"""
from setuptools import setup, find_packages
from config.constants import VERSION, EXTRACTOR_NAME

setup(name=EXTRACTOR_NAME,
      version=VERSION,
      packages=find_packages(),
      test_suite="tests",
      install_requires=[
          'pandas==1.0.5',
          'numpy==1.18.5',
          'pyproj==2.4',
          'gnupg',
          'shapely',
          'pyyaml',
          'scipy',
          'oauthlib',
          'requests-oauthlib',
          'requests',
          'ContainerClient>=0.6.102',
          'DANFUtils>=0.2.29',
          'coverage>=4.5.4',
          "pycrypto",
          "pycryptodomex",
          "pycryptodome"
      ])
