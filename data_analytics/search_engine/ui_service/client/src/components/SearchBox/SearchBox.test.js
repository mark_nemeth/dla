import React from 'react';
import {shallow} from 'enzyme/build';
import {SearchBox} from './SearchBox';

describe('SearchBox component', () => {

  it('should renders without crashing', () => {
    const wrapper = shallow(<SearchBox />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should change searchQuery when enter text', () => {
    const reduxAction = jest.fn();
    const props = {
      searchQuery: 'Abc',
      setCurrentSearchQuery: reduxAction
    };
    const wrapper = shallow(<SearchBox {...props} />);
    const searchInput = wrapper.find('FormControl').first();
    expect(searchInput.props().value).toBe('Abc');
    searchInput.simulate('change', { target: { value: '123' } });
    expect(reduxAction).toHaveBeenCalled();
  });

  it('should call action button was pressed', () => {
    const reduxAction1 = jest.fn();
    const props = {
      searchQuery: 'Test',
      filterBySearchQuery: reduxAction1
    };
    const wrapper = shallow(<SearchBox {...props} />);
    const searchInput = wrapper.find('FormControl').first();
    searchInput.props().onKeyDown({key: 'Enter'});
    expect(reduxAction1).toHaveBeenCalled();
  });

});