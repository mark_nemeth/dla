#!/bin/bash
az devops login --organization https://dev.azure.com/halfdome/

az artifacts universal download \
  --organization "https://dev.azure.com/halfdome/" \
  --project "f5b64c59-5484-4b7d-b955-8bcffbc5cd9a" \
  --scope project \
  --feed "DLA" \
  --name "dla_converter" \
  --version "1.0.43" \
  --path .