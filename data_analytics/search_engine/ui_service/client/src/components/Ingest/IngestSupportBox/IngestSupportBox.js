import React from 'react';
import {faCarCrash} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import './IngestSupportBox.scss';

export default function IngestSupportBox() {

  return (
      <div className="ingest-support-card">
        <FontAwesomeIcon icon={faCarCrash} className="main-item-icon"/>
        {"Problems? See our "}
        <a href="https://athena.daimler.com/confluence/display/ATIIT4AD/HowTo%3A+Ingest+car+data+into+the+cluster"
           target="_blank"
           rel="noreferrer">
          confluence page
        </a>.
      </div>
  );
}
