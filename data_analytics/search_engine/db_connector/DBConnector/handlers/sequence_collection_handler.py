"""Sequence Collection Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import pymongo
from pymongo import UpdateOne
from pymongo.errors import BulkWriteError
from DANFUtils.logging import logger
from DANFUtils.utils import generate_guid
from DBConnector.exceptions import IndexOverwriteException, BulkOperationException
from DBConnector.utils import calculate_request_size, MAXIMUM_REQUEST_BATCH_SIZE, delete_all_documents_from_collection


class SequenceCollectionHandler:
    """
    SequenceCollectionHandler
    """

    # Sequence index for guids
    SEQUENCES_COLLECTION_NAME = 'sequences'
    SEQUENCES_INDEX_NAME = 'sequences_guid_index'
    SEQUENCES_INDEX_FIELD_GUID = 'guid'

    # Compound Sequence index for bagfile guid + index fields
    SEQUENCES_INDEX_2_NAME = 'sequences_order_index'
    SEQUENCES_INDEX_2_FIELD_BAG = 'bagfile_guid'
    SEQUENCES_INDEX_2_FIELD_INDEX = 'index'

    def __init__(self, db):
        self.db = db

        col = self.db[self.SEQUENCES_COLLECTION_NAME]
        col.create_index(self.SEQUENCES_INDEX_FIELD_GUID,
                         unique=True,
                         name=self.SEQUENCES_INDEX_NAME)
        col.create_index(
            [(self.SEQUENCES_INDEX_2_FIELD_BAG, pymongo.ASCENDING),
             (self.SEQUENCES_INDEX_2_FIELD_INDEX, pymongo.ASCENDING)],
            unique=True,
            name=self.SEQUENCES_INDEX_2_NAME)

    def add(self, sequences):
        assert isinstance(
            sequences, list), 'Data inserted into db has to be a list of dicts'
        assert all(
            isinstance(x, dict) for x in
            sequences), 'Data inserted into db has to be a list of dicts'
        col = self.db[self.SEQUENCES_COLLECTION_NAME]

        num_docs = len(sequences)
        request_size = calculate_request_size(sequences)
        logger.debug("Adding " + str(num_docs) +
                     " documents to collection \"" +
                     self.SEQUENCES_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")

        try:
            col.insert_many(sequences)

        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

        for x in sequences:
            del x['_id']

    def get(self, guids):
        col = self.db[self.SEQUENCES_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        request = {self.SEQUENCES_INDEX_FIELD_GUID: {"$in": guids}}

        request_size = calculate_request_size(request)
        logger.debug("Retrieving " + str(len(guids)) +
                     " documents from collection \"" +
                     self.SEQUENCES_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {'_id': 0})

    def get_by_index(self, bagfile_guid_index_tuples):
        col = self.db[self.SEQUENCES_COLLECTION_NAME]
        assert isinstance(bagfile_guid_index_tuples,
                          list), 'Expected a list of guid_index tuples.'
        assert all(isinstance(x, tuple) for x in bagfile_guid_index_tuples
                   ), 'Expected a list of bagfile_guid_index_tuples'
        request = {
            '$or': [{
                self.SEQUENCES_INDEX_2_FIELD_BAG: b,
                self.SEQUENCES_INDEX_2_FIELD_INDEX: i
            } for b, i in bagfile_guid_index_tuples]
        }

        request_size = calculate_request_size(request)
        logger.debug("Retrieving " + str(len(bagfile_guid_index_tuples)) +
                     " documents from collection \"" +
                     self.SEQUENCES_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {'_id': 0})

    def get_by_bagfile_guid(self, bagfile_guids):
        col = self.db[self.SEQUENCES_COLLECTION_NAME]
        assert isinstance(bagfile_guids, list), 'Expected a list of guids.'
        assert all(isinstance(x, str)
                   for x in bagfile_guids), 'Expected a list of bagfile_guids'
        request = {self.SEQUENCES_INDEX_2_FIELD_BAG: {"$in": bagfile_guids}}

        request_size = calculate_request_size(request)
        logger.debug("Retrieving sequences for " + str(len(bagfile_guids)) +
                     " documents from collection \"" +
                     self.SEQUENCES_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {
            '_id': 0
        }).sort([("index", pymongo.ASCENDING)])

    def update(self, guids, sequence_data, create_if_missing=False):
        col = self.db[self.SEQUENCES_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        assert isinstance(sequence_data,
                          list), 'Expected a list of sequence dicts.'
        assert all(
            isinstance(x, dict)
            for x in sequence_data), 'Expected a list of sequence dicts.'
        assert isinstance(create_if_missing,
                          bool), 'create_if_missing has to be of type bool'
        assert len(guids) == len(
            sequence_data
        ), 'List guids and list sequence_data should be of same length'

        prepared_sequence_data = []
        for d in sequence_data:
            if self.SEQUENCES_INDEX_FIELD_GUID in d.keys() \
                    or self.SEQUENCES_INDEX_2_FIELD_BAG in d.keys() \
                    or self.SEQUENCES_INDEX_2_FIELD_INDEX in d.keys():
                msg = 'Overwriting index fields is not permitted!'
                logger.error(msg)
                raise IndexOverwriteException(msg)

            # Resolve full path of dict keys (dot notation) to prevent overwrite of existing
            # structures recursively and separate array fields
            prepared_sequence_data.append(
                preprocess_update_query_dict(d, '', '', [], [], [], []))

        requests = []
        for g, (d, p) in zip(guids, prepared_sequence_data):
            # Add GUID to dict and create update request
            if d:
                requests.append(
                    UpdateOne({self.SEQUENCES_INDEX_FIELD_GUID: g},
                              {"$set": {
                                  **d
                              }},
                              upsert=create_if_missing))

            # Update array fields separately (require push of new elements)
            if p:
                requests.append(
                    UpdateOne({self.SEQUENCES_INDEX_FIELD_GUID: g},
                              {"$push": {
                                  **p
                              }},
                              upsert=create_if_missing))
        try:
            # TODO: Compute request size
            logger.debug("Updating  " + str(len(guids)) +
                         " documents in collection \"" +
                         self.SEQUENCES_COLLECTION_NAME + "\"")
            col.bulk_write(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

    def update_by_index(self,
                        bagfile_guid_index_tuples,
                        sequence_data,
                        upsert=False):
        col = self.db[self.SEQUENCES_COLLECTION_NAME]
        assert isinstance(bagfile_guid_index_tuples,
                          list), 'Expected a list of guid_index tuples.'
        assert all(isinstance(x, tuple) for x in bagfile_guid_index_tuples
                   ), 'Expected a list of bagfile_guid_index_tuples.'
        assert isinstance(sequence_data,
                          list), 'Expected a list of sequence dicts.'
        assert all(
            isinstance(x, dict)
            for x in sequence_data), 'Expected a list of sequence dicts.'
        assert isinstance(upsert,
                          bool), 'create_if_missing has to be of type bool'
        assert len(bagfile_guid_index_tuples) == len(
            sequence_data
        ), 'List bagfile_guid_index_tuples and list sequence_data should be of same length'

        prepared_sequence_data = []
        for d in sequence_data:
            if self.SEQUENCES_INDEX_FIELD_GUID in d.keys() \
                    or self.SEQUENCES_INDEX_2_FIELD_BAG in d.keys() \
                    or self.SEQUENCES_INDEX_2_FIELD_INDEX in d.keys():
                msg = 'Overwriting index fields is not permitted!'
                logger.error(msg)
                raise IndexOverwriteException(msg)

            # Resolve full path of dict keys (dot notation) to prevent overwrite of existing
            # structures recursively and separate array fields
            prepared_sequence_data.append(
                preprocess_update_query_dict(d, '', '', [], [], [], []))
        requests = []
        for g, (d, p) in zip(bagfile_guid_index_tuples,
                             prepared_sequence_data):
            # Add GUID to dict and create update request
            # Set generated guids for upserted sequences by setOnInsert
            new_guid = generate_guid()
            if d:
                requests.append(
                    UpdateOne(
                        {
                            self.SEQUENCES_INDEX_2_FIELD_BAG: g[0],
                            self.SEQUENCES_INDEX_2_FIELD_INDEX: g[1]
                        }, {
                            "$set": {
                                **d
                            },
                            "$setOnInsert": {
                                self.SEQUENCES_INDEX_FIELD_GUID:
                                new_guid,
                                self.SEQUENCES_INDEX_2_FIELD_BAG: g[0],
                                self.SEQUENCES_INDEX_2_FIELD_INDEX: g[1]
                            }
                        },
                        upsert=upsert))

            # Update array fields separately (require push of new elements)
            if p:
                requests.append(
                    UpdateOne(
                        {
                            self.SEQUENCES_INDEX_2_FIELD_BAG: g[0],
                            self.SEQUENCES_INDEX_2_FIELD_INDEX: g[1]
                        }, {
                            "$push": {
                                **p
                            },
                            "$setOnInsert": {
                                self.SEQUENCES_INDEX_FIELD_GUID:
                                new_guid,
                                self.SEQUENCES_INDEX_2_FIELD_BAG: g[0],
                                self.SEQUENCES_INDEX_2_FIELD_INDEX: g[1]
                            }
                        },
                        upsert=upsert))

        # TODO: implement consistent exception handling
        try:

            # TODO: Compute request size
            logger.debug("Updating  " + str(len(bagfile_guid_index_tuples)) +
                         " documents in collection \"" +
                         self.SEQUENCES_COLLECTION_NAME + "\"")
            col.bulk_write(requests, ordered=False)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

    def delete(self, guids):
        col = self.db[self.SEQUENCES_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'

        num_docs = len(guids)
        if num_docs > 0:
            request = {'guid': {"$in": guids}}
            request_size = calculate_request_size(request)
            logger.debug("Deleting  " + str(num_docs) +
                         " documents from collection \"" +
                         self.SEQUENCES_COLLECTION_NAME +
                         "\" (Request size: {:.2f}".format(request_size) +
                         " kb)")
            if num_docs <= MAXIMUM_REQUEST_BATCH_SIZE:
                col.delete_many(request)
            else:
                # batch delete
                logger.debug(
                    "Info: Number of delete operations exceeds maximum for single request."
                    + "Performing document deletion in batches of size " +
                    str(MAXIMUM_REQUEST_BATCH_SIZE))
                i = 0
                while i < num_docs:
                    start_id = i
                    i += MAXIMUM_REQUEST_BATCH_SIZE
                    end_id = i
                    if end_id >= num_docs:
                        end_id = num_docs
                    logger.debug("Deleting batch objects " + str(end_id) +
                                 " of " + str(num_docs))
                    request = {'guid': {"$in": guids[start_id:end_id]}}
                    col.delete_many(request)

    def delete_all(self):
        delete_all_documents_from_collection(self.SEQUENCES_COLLECTION_NAME,
                                             self.db)


def preprocess_update_query_dict(d, key, value, dotted_keys, dotted_values,
                                 array_field_keys, array_field_values):
    """
    Iterates recursively over update query dict and extracts dict keys as full path (dot notation).
    This is needed to prevent field overwrite on update. Array field keys and values are returned separately since 
    they can not be updated directly, but have to be pushed to the existing array fields.
    
    :param d: Query dict
    :param key: The current key 
    :param value: The current value
    :param dotted_keys: A list containing the collected keys in dot notation (except array fields)
    :param dotted_values: A list containing the collected values (except array fields)
    :param array_field_keys: A list containing the collected array field keys in dot notation
    :param array_field_values: A list containing the collected array field values
    :return: A tuple of two dictionaries: 
    (Dict holding the dotted keys + corresponding values(except array fields), 
    Dict holding the dotted keys + corresponding values for array fields)
    """
    if isinstance(d, dict):
        for k, v in d.items():
            preprocess_update_query_dict(d[k], key + '.' + k if key else k, v,
                                         dotted_keys, dotted_values,
                                         array_field_keys, array_field_values)
    else:
        if isinstance(d, list):
            array_field_keys.append(key)
            array_field_values.append(value)
        else:
            dotted_keys.append(key)
            dotted_values.append(value)

    return (dict(zip(dotted_keys, dotted_values)),
            dict(
                zip(array_field_keys, [{
                    "$each": x
                } for x in array_field_values])))
