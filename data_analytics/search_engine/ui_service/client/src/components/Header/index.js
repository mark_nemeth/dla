import {connect} from 'react-redux';
import {Header} from './Header';
import {fetchFeatureToggles} from '../../redux/App/AppActions';
import './Header.scss';

function mapStateToProps(state, ownProps) {
  const isIngestDashboardActive = state.featureToggles.isIngestDashboardActive;

  return {isIngestDashboardActive};
}

export default connect(
    mapStateToProps,
    {fetchFeatureToggles}
)(Header);