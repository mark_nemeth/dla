node("SMTCA01551") {
properties([disableConcurrentBuilds()])
    stage('Build wheels...') {
        node = "SMTCA01551"
        build job: '../wheels/container_client_wheel', parameters:
        [[$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/container_client"],
        [$class: 'StringParameterValue', name: 'node', value: node],
        [$class: 'StringParameterValue', name: 'registry_target', value: "ATTDATADANF_pypi/containerclient/"],
        [$class: 'StringParameterValue', name: 'component', value: "ContainerClient"]]
        build job: '../wheels/DANFUtils_wheel', parameters:
        [[$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/danf_utils"],
        [$class: 'StringParameterValue', name: 'node', value: node],
        [$class: 'StringParameterValue', name: 'registry_target', value: "ATTDATADANF_pypi/danfutils/"],
        [$class: 'StringParameterValue', name: 'component', value: "DANFUtils"]]
        build job: '../wheels/DBConnector_wheel', parameters:
        [[$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/db_connector"],
        [$class: 'StringParameterValue', name: 'node', value: node],
        [$class: 'StringParameterValue', name: 'registry_target', value: "ATTDATADANF_pypi/dbconnector/"],
        [$class: 'StringParameterValue', name: 'component', value: "DBConnector"]]
    }
    stage('Build docker images...') {
        sleep time: 60000, unit: 'MILLISECONDS'
        build job: '../docker/cleaning-script-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "cleaning_script"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cleaning_script"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/data-handler-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-handler-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/data_handler"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/extractor-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "extractor"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/extractor"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/ros:melodic-robot-bionic"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.6.8"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/kube-garbage-collector-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "kubegc"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cluster_services/kube_garbage_collector"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/kube-refresh-cache-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "cache-refresher"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cluster_services/kube_refresh_cache"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/metadata-api-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "metadata-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/metadata_api"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/search-api-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "search-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/search_api"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/splitter-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "splitter"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/splitter"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/ros:melodic-robot-bionic"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.6.8"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/tracing-sidecar-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "tracing-sidecar"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/tracing_sidecar"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/ui-service-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "ui-service"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/ui_service"],
        [$class: 'StringParameterValue', name: 'image', value: "athena.daimler.com/dla_docker/ui-base-image:latest"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/data-crawler-scanner-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-crawler-scanner"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cluster_services/data_crawler/scanner"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../docker/data-crawler-collector-docker', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-crawler-collector"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cluster_services/data_crawler/collector"],
        [$class: 'StringParameterValue', name: 'base_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'downloader_docker_image', value: "athena.daimler.com/dla_docker/python:3.7"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
    }
    stage('Deploy configmaps...') {
    build job: '../vai_deploy/configmap-data-handler', parameters:
        [[$class: 'StringParameterValue', name: 'configmap_filepath', value: "data_analytics/search_engine/data_handler/kubernetes/configmap_vai_int.yaml"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'loc', value: "vai"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'filename', value: "configmap_vai_int.yaml"],
        [$class: 'StringParameterValue', name: 'configmap_name', value: "data-handler-configmap"],
        [$class: 'StringParameterValue', name: 'replace_placeholders', value: "true"],
        [$class: 'StringParameterValue', name: 'namespace', value: "dla-danf"]]
    build job: '../sv_deploy/configmap-data-handler', parameters:
        [[$class: 'StringParameterValue', name: 'configmap_filepath', value: "data_analytics/search_engine/data_handler/kubernetes/configmap_sv_int.yaml"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'loc', value: "sv"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'filename', value: "configmap_sv_int.yaml"],
        [$class: 'StringParameterValue', name: 'configmap_name', value: "data-handler-configmap"],
        [$class: 'StringParameterValue', name: 'replace_placeholders', value: "true"],
        [$class: 'StringParameterValue', name: 'namespace', value: "dla-danf"]]
    }
    stage('Deploy services vai int...') {
        build job: '../vai_deploy/search-api', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "search-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/search_api/kubernetes/search_api_vai_int.yaml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/search_api/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "search-api"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "vai"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../vai_deploy/metadata-api', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "metadata-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/metadata_api/kubernetes/metadata_api_vai_int.yaml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/metadata_api/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "metadata-api"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "vai"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../vai_deploy/data-handler-api', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-handler-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/data_handler/kubernetes/data_handler_vai_int.yaml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/data_handler/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "data-handler-api"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "vai"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../vai_deploy/ui-service', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "ui-service"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/ui_service/server/kubernetes/ui_service_vai_int.yaml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/ui_service/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "microservice-web-ui"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "vai"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../vai_deploy/data-crawler-collector', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-crawler-collector"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/cluster_services/data_crawler/collector/kubernetes/cronjobs/int/vai/crawler-collector-cronjob.yml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cluster_services/data_crawler/collector/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "data-crawler-collector"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "vai"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../vai_deploy/data-crawler-scanner-vai', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-crawler-scanner"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/cluster_services/data_crawler/scanner/kubernetes/cronjobs/int/vai/crawler-scanner-cronjob-vai.yml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cluster_services/data_crawler/scanner/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "data-crawler-scanner-vai"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "vai"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../vai_deploy/data-crawler-scanner-hks', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-crawler-scanner"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/cluster_services/data_crawler/scanner/kubernetes/cronjobs/int/vai/crawler-scanner-cronjob-hks.yml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cluster_services/data_crawler/scanner/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "data-crawler-scanner-hks"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "vai"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
    }
    stage('Deploy services sv int...') {
        build job: '../sv_deploy/search-api', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "search-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/search_api/kubernetes/search_api_sv_int.yaml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/search_api/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "search-api"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "sv"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../sv_deploy/metadata-api', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "metadata-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/metadata_api/kubernetes/metadata_api_sv_int.yaml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/metadata_api/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "metadata-api"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "sv"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../sv_deploy/data-handler-api', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-handler-api"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/data_handler/kubernetes/data_handler_sv_int.yaml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/data_handler/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "data-handler-api"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "sv"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
        build job: '../sv_deploy/data-crawler-scanner-sv', parameters:
        [[$class: 'StringParameterValue', name: 'image_name', value: "data-crawler-scanner"],
        [$class: 'StringParameterValue', name: 'BRANCH_NAME', value: BRANCH_NAME],
        [$class: 'StringParameterValue', name: 'path_to_yaml', value: "data_analytics/search_engine/cluster_services/data_crawler/scanner/kubernetes/cronjobs/int/sv/crawler-scanner-cronjob-sv.yml"],
        [$class: 'StringParameterValue', name: 'path_to_component', value: "data_analytics/search_engine/cluster_services/data_crawler/scanner/"],
        [$class: 'StringParameterValue', name: 'version', value: "latest"],
        [$class: 'StringParameterValue', name: 'deployment_label', value: "data-crawler-scanner-sv"],
        [$class: 'StringParameterValue', name: 'stage', value: "int"],
        [$class: 'StringParameterValue', name: 'loc', value: "sv"],
        [$class: 'StringParameterValue', name: 'node', value: node]]
    }
}
