"""Search Engine REST API feature search route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing
from tornado.escape import recursive_unicode

from handlers.auth import auth
from handlers.feature_filter_handler import FeatureFilterHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler

logger = logging.getLogger(__name__)


class FeatureFilterRequestHandler(BaseRequestHandler):
    def initialize(self):
        self.filter_handler = FeatureFilterHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, GET')

    @auth.requires_entitlements(["openid"])
    def get(self):
        tracing.start_task_in_context('featurefilter/get',
                                      context_headers=self.request.headers)

        # Interpret query arguments
        bagfile_guid, features = self.filter_handler.get_filter_arguments(
            recursive_unicode(self.request.query_arguments))

        logger.info(
            f"Retrieving features {features} for bagfile {bagfile_guid}")
        res = self.filter_handler.perform_filtering(bagfile_guid, features)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"],
                             total=1)
