import {connect} from 'react-redux';
import {DriveDetail} from "./DriveDetail";
import './DriveDetail.scss'
import {showError} from "../../redux/App/AppActions";
import {showSelectedDriveItem} from "../../redux/Overview/OverviewActions";

function mapStateToProps(state, ownProps) {
  const drive = state.overview.bagfile;
  const selectedDriveItem = state.overview.selectedDriveItem;
  const selectedDriveItemType = state.overview.selectedDriveItemType;

  return {drive, selectedDriveItem, selectedDriveItemType};
}

export default connect(
    mapStateToProps, {showError, showSelectedDriveItem}
)(DriveDetail)