import {connect} from 'react-redux';
import BagfileMap from './BagfileMap';
import {
  highlightSelectedChildBagfile,
  highlightHoveringChildBagfile,
  highlightSelectedEvent,
  highlightHoveringEvent
} from '../../redux/Overview/OverviewActions';

function mapStateToProps(state, ownProps) {
  const selectedChildbagfileGuid = state.overview.selectedChildbagfileGuid || null;
  const hoveringChildBagfile = state.overview.hoveringChildbagfile || null;
  const selectedEvent = state.overview.selectedEvent || null;
  const hoveringEvent = state.overview.hoveringEvent || null;
  return {selectedChildbagfileGuid, hoveringChildBagfile, selectedEvent, hoveringEvent};
}

export default connect(
  mapStateToProps,
  {highlightSelectedChildBagfile, highlightHoveringChildBagfile, highlightSelectedEvent, highlightHoveringEvent}
)(BagfileMap);