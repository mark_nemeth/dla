import React from 'react';
import {DriveItemTypes} from "../DriveDetail";
import DriveMetadataView from "./DriveMetadataView";
import MissionMetadataView from "./MissionMetadataView";
import EngagementMetadataView from "./EngagementMetadataView";
import SystemRunMetadataView from "./SystemRunMetadataView";
import AlwaysOnRecordingMetadataView from "./AlwaysOnRecordingMetadataView";
import OnDemandRecordingMetadataView from "./OnDemandRecordingMetadataView";
import './DriveItemView.scss';
import AnnotationMetadataView from "./AnnotationMetadataView";

export function DriveItemView(props) {

  function getChild() {
    if (!props.itemType) {
      return '';
    }
    switch (props.itemType) {
      case DriveItemTypes.DRIVE:
        return <DriveMetadataView drive={props.item}/>;
      case DriveItemTypes.MISSION:
        return <MissionMetadataView mission={props.item}/>;
      case DriveItemTypes.ENGAGEMENT:
        return <EngagementMetadataView engagement={props.item}/>;
      case DriveItemTypes.ALWAYS_ON_RECORDING:
        return <AlwaysOnRecordingMetadataView recording={props.item}/>;
      case DriveItemTypes.ON_DEMAND_RECORDING:
        return <OnDemandRecordingMetadataView recording={props.item}/>;
      case DriveItemTypes.SYSTEM_RUN:
        return <SystemRunMetadataView system_run={props.item}/>;
      case DriveItemTypes.ANNOTATION:
        return <AnnotationMetadataView annotation={props.item}/>;
      default :
        return "";
    }
  }

  return (
      <div className="drive-item-information-section p-5">
        {getChild()}
      </div>
  )
}