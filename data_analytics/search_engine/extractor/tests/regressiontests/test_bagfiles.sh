#!/bin/sh

export BAGFILE_GUID=1234-1234-1234-1234

FILES="
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/02/07/20180207_132217alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/01/31/20180131_105858__.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDB9634031L568466/2018/01/29/20180129_094709alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDB9634031L568466/2018/02/05/20180205_142531_immendingen_poles_01.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDB9634031L568466/2018/03/13/20180313_183859_rc_74_1_cal_loc_map-pln_wrong_tfs.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDB9634031L568466/2018/04/19/20180419_143943__ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/05/02/20180502_112259_loop_vaihingen.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/05/24/20180524_095503alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/06/05/20180605_104916_SDATS_08_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/07/06/20180706_101546_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/08/07/20180807_112307_CAL_CheckDriverBracking_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/08/14/20180814_104409_CAL_safetycheck_braking_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/08/14/20180814_111858_TC3-01-01-01_03_EXTRACTEDBY_LogbookIssueInterface_at_20180814_112020.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/08/28/20180828_104426_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/09/27/20180927_153606_creep_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/10/02/20181002_100813_HEREmaps_test_demo_1_EXTRACTEDBY_LogbookIssueInterface_at_20181002_101256.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/10/02/20181002_110408_HEREmaps_test_demo_5_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/10/04/20181004_105123_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/11/06/20181106_160108_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/11/26/20181126_145520_alwayson.bag*
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/12/07/20181207_114247_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/12/19/20181219_115226_tc_3-01-01-01_01_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/01/15/20190115_104516_vehicle_startup07_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/01/15/20190115_141451_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/01/15/20190115_154839_mission_64_bug_EXTRACTEDBY_LogbookIssueInterface_at_20190115_154904.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/01/17/20190117_142739_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/02/26/20190226_153144_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/03/15/20190315_094319_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/04/01/20190401_152933_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121754/2018/08/30/20180830_100210_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121754/2018/10/02/20181002_105155_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121754/2018/12/11/20181211_131152_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121754/2019/01/21/20190121_093816_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121789/2018/07/10/20180710_100007_cid_drive3_lidar_EXTRACTEDBY_LogbookIssueInterface_at_20180710_100231.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121789/2018/07/10/20180710_151031_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121789/2018/09/20/20180920_125824_creeping_tests_m45_02_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121789/2018/05/15/20180515_145748_pi1_presentation.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A396347/2019/02/07/20190207_095140_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A396347/2019/01/16/20190116_143215_sclass_svs_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121789/2019/04/08/20190408_145104_alwayson.bag
"

rm log.txt
echo "Filename Filesize Exectime(s) Returncode" > log.txt

for file in $FILES
do
    echo "Processing $file"
    start=`date +%s`

    export BAGFILE_PATH=$file

    python ../index.py

    # get retrun code from the python file
    ret=$?

    end=`date +%s`
    runtime=$((end-start))

    filesize=$(stat -c%s "$file")

    echo "$file $filesize $ret $runtime" >> log.txt
done