import React from 'react';
import './ErrorBanner.scss'

/**
 * ErrorBanner
 * @return null when there is no error so that no element will be rendered, when error happens return a banner element
 */
export function ErrorBanner(props) {

  if (!props.error) return null;

  // Remove banner automatically after 10 sec
  setTimeout(() => {
    hideErrorBanner();
  }, props.disappearMilSeconds);

  function hideErrorBanner() {
    props.hideError();
  }

  return (
    <div className={"error-banner" + " " + "error-banner-" + props.variant}>
      <div className="error-banner-message">{props.errorMessage}</div>
      <div className="error-banner-close-icon icon-close" onClick={hideErrorBanner}/>
    </div>
  );
}