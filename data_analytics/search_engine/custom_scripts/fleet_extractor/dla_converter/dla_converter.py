from subprocess import check_call
import rosbag
import yaml

from ContainerClient import ContainerClient
from DANFUtils.logging import logger

BAGFILE_DATA_VERSION = "0.1"

class DlaConverter:

    def __init__(self, input, output):
        self.input_path = input
        self.output_path = output
        self.bag=None
        self._client = ContainerClient()

    def execute_dla_converter(self, dbc_file):
        check_call([
            "dla_converter/dla_converter",
            "--in", self.input_path,
            "--dbc", "6", dbc_file,
            "--out", self.output_path
        ])
    def create_metadata(self, file_type, file_tag, processing_state):
        file_name = self.output_path.split("/")[-1]
        # get needed metadata (start time, end time, list of topics)
        self.bag = self._get_bag_object()
        try:
            start_ts, end_ts, messages, duration, size, topics_list = self._get_metadata(
            )
        except Exception as exp:
            logger.exception(f"Cannot get metadata of the bag file! file: {self.input_path}")
            raise exp

        bagfile_data = {
            "link": self.output_path,
            "version": BAGFILE_DATA_VERSION,
            "file_name": file_name,
            "file_type": file_type,
            "tags": file_tag,
            "processing_state": processing_state,
            "size": size,
            "num_messages": messages,
            "start": int(start_ts * 1000),
            "end": int(end_ts * 1000),
            "duration": int(duration * 1000),
            # "vehicle_id_num": self.vin,
            "extractor": "ext1",
            "topics": list(set(topics_list)),
            "origin": "DE" # hard code the origin to "DE" for now
        }
        logger.info(str(bagfile_data))
        bagfile_guid = self._client.create_bagfile(bagfile_data)
        logger.info(f"Created metadata for bagfile {bagfile_guid} "
                    f"at {self.output_path}")
        return bagfile_guid

    def _get_bag_object(self):
        return rosbag.Bag(self.output_path, 'r')

    def _get_metadata(self):
        info_dict = yaml.safe_load(self.bag._get_yaml_info())

        topics = []
        for t in info_dict['topics']:
            topic = t['topic']
            topics.append(topic)
        return info_dict['start'], info_dict['end'], info_dict[
            'messages'], info_dict['duration'], info_dict['size'], topics



