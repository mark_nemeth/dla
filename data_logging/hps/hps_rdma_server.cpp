#include <thread>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <libgen.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <libaio.h>
#include <rdma/rdma_verbs.h>
#include "common.h"
#include "hps_rdma_common.h"
#include "hps.h"

using namespace std;

#define KB						(1024)
#define MB						(1024 * 1024)
#define GB						(1024llu * 1024llu * 1024llu)
#define CACHE_LINE_SIZE			(64)

// Configuration
#define BLOCK_SIZE				(1 * MB)
#define BUFFER_SIZE				(1 * GB)
#define MAX_QUEUE_DEPTH			(128)
#define FILE_TRUNCATE_TRESHOLD	(BUFFER_SIZE)
#define FILE_TRUNCATE_SIZE		(BUFFER_SIZE)
#define AIO_MAX_EVENTS			(MAX_QUEUE_DEPTH)

#define CTX_COUNT				(BUFFER_SIZE / BLOCK_SIZE)

typedef enum
{
	AIOS_UNUSED = 0,
	AIOS_SHUTDOWN_REQUEST
} aioStreamStateT;

struct hpRdmaConnection
{
	hps_type type = HPS_RDMA;

	char *out = NULL;

	struct rdma_cm_id *id = NULL;

	struct ibv_pd *pd = NULL;
	struct ibv_comp_channel *channel = NULL;
	struct ibv_cq *cq = NULL;

	thread cqPollThread;
	thread queueDispatcherThread;

	size_t bufferSize = 0;
	char *buffer = NULL;
	char *dataBuffer = NULL;
	struct queueControlBlock *LQCB = NULL;
	struct ibv_mr *buffer_mr = NULL;

	struct message *msg = NULL;
	struct ibv_mr *msg_mr = NULL;

	// AIO contexts
	const char *fileName = NULL;
	int fd = -1;
	long long fileSize = 0;
	int queueDepth = 0;
	io_context_t ctx = NULL;
	struct iocb IOCBs[CTX_COUNT];
	volatile aioStreamStateT state;

	void initFile(const char *out)
	{
		int e;

		fileName = getFileName(out);
		remove(fileName);

		// Open file
		fd = open(fileName, O_CREAT | O_RDWR | O_NOATIME | O_EXCL | O_TRUNC | O_DIRECT, S_IRUSR | S_IWUSR);
		if (fd < 0)
		{
			throw exception();
		}

		e = posix_fadvise(fd, 0, 0, POSIX_FADV_DONTNEED);
		if (e != 0)
			printf("Warning: posix_fadvise failed to set POSIX_FADV_DONTNEED\n");
		e = posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
		if (e != 0)
			printf("Warning: posix_fadvise failed to set POSIX_FADV_DONTNEED\n");

		// Create AIO context
		if (io_setup(AIO_MAX_EVENTS, &ctx) != 0)
		{
			throw exception();
		}

		// Prepare IOCBs
		for (long i = 0; i < CTX_COUNT; i++)
		{
			void *block = (void*)((uintptr_t)buffer + i * BLOCK_SIZE);
			io_prep_pwrite(&IOCBs[i], fd, block, BLOCK_SIZE, 0);
			IOCBs[i].data = &IOCBs[i];
			IOCBs[i].u.c.offset = 0xffffffffffffffff;
		}
	}

	hpRdmaConnection(struct rdma_cm_id *id, const char *out)
	{
		// Associate
		this->id = id;
		id->context = this;

		ibv_device_attr dev_attr;
		TEST_E(ibv_query_device(id->verbs, &dev_attr));

		// Create CQ
		TEST_Z(pd = ibv_alloc_pd(id->verbs));
		TEST_Z(channel = ibv_create_comp_channel(id->verbs));
		TEST_Z(cq = ibv_create_cq(id->verbs, 10, NULL, channel, 0));
		TEST_NZ(ibv_req_notify_cq(cq, 0));

		// Create QP
		struct ibv_qp_init_attr qp_attr;
		memset(&qp_attr, 0, sizeof(qp_attr));
		qp_attr.send_cq = cq;
		qp_attr.recv_cq = cq;
		qp_attr.qp_type = IBV_QPT_RC;
		qp_attr.cap.max_send_wr = MIN(RDMA_SERVER_MAX_SEND_WR, dev_attr.max_qp_wr);
		qp_attr.cap.max_recv_wr = MIN(RDMA_SERVER_MAX_RECV_WR, dev_attr.max_qp_wr);
		qp_attr.cap.max_send_sge = MIN(1, dev_attr.max_sge);
		qp_attr.cap.max_recv_sge = MIN(1, dev_attr.max_sge);
		TEST_NZ(rdma_create_qp(id, pd, &qp_attr));

		// Allocate receive buffer
		bufferSize = RDMA_QCB_SIZE + RDMA_BUFFER_SIZE;
		TEST_NZ(posix_memalign((void **)&buffer, sysconf(_SC_PAGESIZE), bufferSize));
		memset(buffer, 0, bufferSize);
		TEST_Z(buffer_mr = ibv_reg_mr(pd, buffer, bufferSize,
				IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_REMOTE_READ));
		LQCB = (struct queueControlBlock *)buffer;
		dataBuffer = buffer + RDMA_QCB_SIZE;

		// Strip protocol
		this->out = strdup(out);
		char *protocol = this->out;
		char *file = strchr(this->out, ':');
		if (file)
		{
			*file++ = 0;
			if (strcasecmp(protocol, "FILE") == 0)
			{
				initFile(file);
			}
		}

		// Allocate message buffer
		TEST_NZ(posix_memalign((void **)&msg, sysconf(_SC_PAGESIZE), sizeof(*msg)));
		TEST_Z(msg_mr = ibv_reg_mr(pd, msg, sizeof(*msg), IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE));

		// Post some receives
		post_recv();

		// Start CQ poll thread
		cqPollThread = thread(&hpRdmaConnection::poll_cq, this);
		if (strcasecmp(protocol, "FILE") == 0)
			queueDispatcherThread = thread(&hpRdmaConnection::queueDispatcher, this);
		else
			queueDispatcherThread = thread(&hpRdmaConnection::nullDispatcher, this);
	}

	~hpRdmaConnection()
	{
		if (fd != -1)
			close(fd);

		if (id)
			rdma_destroy_qp(id);

		if (buffer_mr)
			ibv_dereg_mr(buffer_mr);
		if (msg_mr)
			ibv_dereg_mr(msg_mr);

		if (id)
			rdma_destroy_id(id);

		if (buffer)
			free(buffer);
		if (msg)
			free(msg);
	}

	int post_recv()
	{
		struct ibv_recv_wr wr, *bad_wr = NULL;
		struct ibv_sge sge;

		wr.wr_id = (uintptr_t)id;
		wr.next = NULL;
		wr.sg_list = NULL;
		wr.num_sge = 0;

		return ibv_post_recv(id->qp, &wr, &bad_wr);
	}

	void post_message(bool completion = true)
	{
		struct ibv_send_wr wr, *bad_wr = NULL;
		struct ibv_sge sge;

		memset(&wr, 0, sizeof(wr));
		wr.wr_id = msg->id;
		wr.opcode = IBV_WR_SEND;
		wr.sg_list = &sge;
		wr.num_sge = 1;
		if (completion)
			wr.send_flags = IBV_SEND_SIGNALED;

		sge.addr = (uintptr_t)msg;
		sge.length = sizeof(*msg);
		sge.lkey = msg_mr->lkey;

		TEST_E(ibv_post_send(id->qp, &wr, &bad_wr));
	}

	void on_connection()
	{
		msg->id = MSG_MR;
		msg->data.mr.addr = (uintptr_t)buffer_mr->addr;
		msg->data.mr.size = bufferSize;
		msg->data.mr.rkey = buffer_mr->rkey;

		post_message();
	}

	void nullDispatcher()
	{
		while (1)
		{
			long long c = LQCB->c;
			long long w = LQCB->w;
			if (c != w)
			{
				long long n = w - c;
				statistics.rxBytes += n;
				LQCB->r = w;
				LQCB->c = w;
			}
			else
			{
				usleep(1000);
			}
		}
	}

	void queueDispatcher()
	{
		struct timespec timeout = { .tv_sec = 0, .tv_nsec = 0 };
		struct io_event events[AIO_MAX_EVENTS];
		iocb *pIOCBs[MAX_QUEUE_DEPTH];

		// The I/O handler shall not process SIGTERM signals.
		sigset_t mask;
		sigemptyset(&mask);
		sigaddset(&mask, SIGTERM);
		sigprocmask(SIG_BLOCK, &mask, NULL);

		while (1)
		{
			long long w = LQCB->w;
			long long r = LQCB->r;

			// Advance file size
			if ((state != AIOS_SHUTDOWN_REQUEST) && (fileSize <= w + FILE_TRUNCATE_TRESHOLD))
			{
				if (fallocate64(fd, 0, fileSize, FILE_TRUNCATE_SIZE) == 0)
					fileSize += FILE_TRUNCATE_SIZE;
				else
				{
					state = AIOS_SHUTDOWN_REQUEST;
					perror("ftruncate() failed");
				}
			}

			// Process completion events
			if (0 < queueDepth)
			{
				int n = io_getevents(ctx, 1, AIO_MAX_EVENTS, events, &timeout);
				if (0 < n)
				{
					for (int i = 0; i < n; i++)
					{
						assert(events[i].res == BLOCK_SIZE);
						assert(events[i].res2 == 0);
						// Clear offset to indicate that this block is available again
						((struct iocb*)events[i].data)->u.c.offset = 0xffffffffffffffff;
					}
					queueDepth -= n;

					statistics.rxBytes += n * BLOCK_SIZE;

					long long c = LQCB->c;
					while (c < r)
					{
						int idx = (c % BUFFER_SIZE) / BLOCK_SIZE;
						if (IOCBs[idx].u.c.offset != 0xffffffffffffffff)
							break;
						c += BLOCK_SIZE;
					}
					LQCB->c = c;
				}
				assert(0 <= n);
			}

			// Enqueue new blocks writes
			int writeBlockCount = 0;
			long long rMax = w & ~(BLOCK_SIZE - 1);
			if (state == AIOS_SHUTDOWN_REQUEST)
				rMax = w;
			while (r < rMax && queueDepth + writeBlockCount < MAX_QUEUE_DEPTH)
			{
				// Prepare write
				int idx = (r % BUFFER_SIZE) / BLOCK_SIZE;
				IOCBs[idx].u.c.offset = r;
				pIOCBs[writeBlockCount] = &IOCBs[idx];
				r += BLOCK_SIZE;
				writeBlockCount++;
			}
			// Submit block writes
			if (0 < writeBlockCount)
			{
				int n = io_submit(ctx, writeBlockCount, pIOCBs);
				if (0 < n)
				{
					LQCB->r += n * BLOCK_SIZE;
					queueDepth += n;
				}
				else if (n < 0)
				{
					if (n == -(EAGAIN))
					{
						// Insufficient resources are available to queue any iocbs
					}
					else
					{
						printf("io_submit() failed (%d)\n", n);
					}
				}
			}

			usleep(10000);
		}
	}

	void on_completion(struct ibv_wc *wc)
	{
		struct rdma_cm_id *id = (struct rdma_cm_id*)(uintptr_t)wc->wr_id;

		if (wc->opcode == IBV_WC_RECV_RDMA_WITH_IMM)
		{
			uint32_t size = wc->imm_data;

			if (0 < size)
			{
				post_recv();
			}
			else
			{
				printf("Received EOF\n");
				msg->id = MSG_DONE;
				post_message();
				// connection will be closed --> post_recv() not required
			}
		}
		else if (wc->opcode == IBV_WC_SEND)
		{
			if (wc->status == IBV_WC_SUCCESS)
			{
			}
		}
		else
		{
			printf("Unexpected completion: opcode 0x%08X, status: 0x%08Xn", wc->opcode, wc->status);
		}
	}

	void poll_cq()
	{
		using namespace std;

		struct ibv_cq *ev_cq;
		void *ev_ctx;
		struct ibv_wc wc[RDMA_POLL_CQ_BATCH_SIZE];

		try
		{
			while (1)
			{
				TEST_NZ(ibv_get_cq_event(channel, &ev_cq, &ev_ctx));
				assert(ev_cq == cq);
				ibv_ack_cq_events(ev_cq, 1);
				TEST_NZ(ibv_req_notify_cq(ev_cq, 0));

				int n = ibv_poll_cq(ev_cq, RDMA_POLL_CQ_BATCH_SIZE, wc);
				if (n < 0)
				{
					printf("ibv_poll_cq failed (%d)\n", n);
					break;
				}
				for (int i = 0; i < n; i++)
				{
					on_completion(&wc[i]);
				}
			}
		}
		catch (exception&)
		{
		}
	}
};

struct hpRdmaServerT
{
	hps_type type = HPS_TCP;
	const char *ip;
	int port;
	const char *out = NULL;
	struct rdma_cm_id *listener = NULL;
	struct rdma_event_channel *ec = NULL;
	thread serverThread;

	hpRdmaServerT(const char *ip, int port, const char *out)
	{
		this->ip = ip ? strdup(ip) : NULL;
		this->port = port;
		this->out = strdup(out);
		// Start server event handler
		serverThread = thread(&hpRdmaServerT::run, this);
	}

	~hpRdmaServerT()
	{
		if (listener)
			rdma_destroy_id(listener);
		if (ec)
			rdma_destroy_event_channel(ec);
		serverThread.join();
	}

	void run()
	{
		struct sockaddr_in addr;

		memset(&addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		if (strcmp(ip, "*") == 0)
			addr.sin_addr.s_addr = INADDR_ANY;
		else
			inet_aton(ip, &addr.sin_addr);

		TEST_Z(ec = rdma_create_event_channel());
		TEST_NZ(rdma_create_id(ec, &listener, NULL, RDMA_PS_TCP));
		TEST_NZ(rdma_bind_addr(listener, (struct sockaddr * )&addr));
		printf("RDMA server listening on port %d\n", port);
		TEST_NZ(rdma_listen(listener, 10));

		struct rdma_conn_param cm_params;
		memset(&cm_params, 0, sizeof(cm_params));
		cm_params.initiator_depth = cm_params.responder_resources = 1;
		cm_params.retry_count = 7;
		cm_params.rnr_retry_count = 7; /* infinite retry */

		// Server event loop
		struct rdma_cm_event *event = NULL;
		while (rdma_get_cm_event(ec, &event) == 0)
		{
			struct rdma_cm_event e;
			memcpy(&e, event, sizeof(*event));
			rdma_ack_cm_event(event);

			if (e.event == RDMA_CM_EVENT_CONNECT_REQUEST)
			{
				hpRdmaConnection *s = new hpRdmaConnection(e.id, out);
				TEST_NZ(rdma_accept(e.id, &cm_params));
			}
			else if (e.event == RDMA_CM_EVENT_ESTABLISHED)
			{
				hpRdmaConnection* s = (hpRdmaConnection*)e.id->context;
				s->on_connection();
			}
			else if (e.event == RDMA_CM_EVENT_DISCONNECTED)
			{
				printf("Disconnect\n");
				hpRdmaConnection* s = (hpRdmaConnection*)e.id->context;
				delete s;
				// Exit on disconnect
				break;
			}
			else
			{
				printf("Unknown event 0x%08X\n", e.event);
				break;
			}
		}
	}
};

/******************************************************************************
 * Public API
 *****************************************************************************/

hpRdmaServerT* hps_rdma_start_server(const char *ip, int port, const char *out)
{
	try
	{
		return new hpRdmaServerT(ip, port, out);
	}
	catch (exception&)
	{
		return NULL;
	}
}

int hps_rdma_close_server(hpRdmaServerT *server)
{
	try
	{
		delete server;
		return 0;
	}
	catch (exception&)
	{
		return -1;
	}
}

