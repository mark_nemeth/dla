import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import './ByDriverFilter.scss'

export function ByDriverFilter(props) {

  function onDriverSelect(driver) {
    if (props.selectedDriverList.includes(driver)) props.filterByDrivers(props.selectedDriverList.filter(t => t !== driver));
    else props.filterByDrivers(props.selectedDriverList.concat(driver));
  }

  function resetDriverList() {
    props.filterByDrivers([]);
  }

  const showLoading = props.driverList === undefined || props.driverList.length <= 0;
  const subLabel = props.selectedDriverList.length + ' selected';
  const active = props.selectedDriverList.length > 0;

  const listItemComponent = (item) => (
      <label className="driver-list-item">
        <input type="checkbox" value={item}
               onChange={event => onDriverSelect(event.target.value)}
               checked={props.selectedDriverList.includes(item)}/>
        &nbsp; {item}
      </label>
  );

  return (
      <CollapseContainer label={active > 0 ? "Driver: " + subLabel : "Driver"}
                         active={active}
                         variant="list"
                         resetCallback={resetDriverList}
      >
        <ScrollList
            showLoading={showLoading}
            itemList={props.driverList}
            listItemComponent={listItemComponent}
        />
      </CollapseContainer>
  );
}