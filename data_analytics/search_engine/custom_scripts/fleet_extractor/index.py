"""Extractor"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import sys
import yaml

import rosbag
import rospy

from ContainerClient import ContainerClient
from DANFUtils.logging import logger
sys.path.insert(0,
                os.path.abspath(
                    os.path.join(os.path.dirname(__file__), '../../')))
from dla_converter.dla_converter import DlaConverter


class Extractor:
    def __init__(self, path, guid):
        logger.info("Find the max of power consumption of AD-Kit by adding the measured power of the power boxes")
        self.path = path
        self.guid = guid
        # open bag file for reading
        try:
            self.bag = self._get_bag_object()
        except Exception as exp:
            logger.exception(f"Error opening bag file! file: {path}")
            raise exp

        # get needed metadata (start time, end time, list of topics)
        try:
            self.start_ts, self.end_ts, self.messages, self.duration, self.size, self.topics_list = self._get_metadata(
            )
        except Exception as exp:
            logger.exception(f"Cannot get metadata of the bag file! file: {path}")
            raise exp

    def _get_bag_object(self):
        return rosbag.Bag(self.path, 'r')

    def _get_metadata(self):
        info_dict = yaml.safe_load(self.bag._get_yaml_info())

        topics = []
        for t in info_dict['topics']:
            topic = t['topic']
            topics.append(topic)
        return info_dict['start'], info_dict['end'], info_dict[
            'messages'], info_dict['duration'], info_dict['size'], topics

    def _generate_bagfile_data(self):
        return {
            "size": self.size,
            "num_messages": self.messages,
            "start": int(self.start_ts* 1000),
            "end": int(self.end_ts* 1000),
            "duration": int(self.duration * 1000),
            #"vehicle_id_num": self.vin,
            "extractor": "ext1",
            "topics": list(set(self.topics_list)),
            # hard code the origin to "DE" for now
            "origin": "DE"
        }

    # Just an example how to parse data from the bagfile
    def parse_bagfile(self):

        bag = rosbag.Bag(self.path, 'r')

        # include all topics to be extracted
        # bag_file_white_list = ["/rosout_agg"]
        bag_file_white_list = ["/CAN/Infrastructure/PBX_Stat48", "/CAN/Infrastructure/PBX1_Stat",
                               "/CAN/Infrastructure/PBX2_Stat"]
        last_ts = None
        msg_pbx_stat48 = 0
        msg_pbx1_stat = 0
        msg_pbx2_stat = 0
        Pges = 0
        tmax = 0 # assign before you use
        PgesMax = 0

        # main loop over all filtered messages
        msg_count = 0
        Psum = 0
        for topic, message, t in bag.read_messages(
                topics=bag_file_white_list):

            # check that the last timestamp is current
            # relevant after busses go to sleep
            if (last_ts is not None and (t - last_ts) > rospy.Duration.from_sec(5)):
                msg_pbx_stat48 = 0
                msg_pbx1_stat = 0
                msg_pbx2_stat = 0
                # print("Time diff reset with {}".format(t - last_ts))
            last_ts = t

            # cache/find the messages, populate message varaiables
            if ('/CAN/Infrastructure/PBX_Stat48' == topic):
                msg_pbx_stat48 = message
            elif ('/CAN/Infrastructure/PBX1_Stat' == topic):
                msg_pbx1_stat = message
            elif ('/CAN/Infrastructure/PBX2_Stat' == topic):
                msg_pbx2_stat = message

            if (msg_pbx_stat48 != 0 and msg_pbx1_stat != 0 and msg_pbx2_stat != 0):
                # 48V Box 1:
                p48_pxb1 = msg_pbx_stat48.Cur48_PBX1 * msg_pbx_stat48.Volt48_PBX1
                # 48V Box2:
                p48_pxb2 = msg_pbx_stat48.Cur48_PBX2 * msg_pbx_stat48.Volt48_PBX2
                # 12V Box1:
                p12_pbx1 = msg_pbx1_stat.Cur_PBX1 * msg_pbx1_stat.Volt_PBX1
                # 12V Box2
                p12_pbx2 = msg_pbx2_stat.Cur_PBX2 * msg_pbx2_stat.Volt_PBX2
                # Sum
                Pges = p48_pxb1 + p48_pxb2 + p12_pbx1 + p12_pbx2
                msg_count += 1
                Psum += Pges


                if (Pges > PgesMax):
                    PgesMax = Pges
                    tmax = t

        #logger.info("Found max Power {0:5.1f} W @t={1}".format(PgesMax, tmax))  there is a bug
        #logger.info("Average Power is {0:5.1f} W".format(Psum / msg_count))  division by zero
        try:
            speed = Psum / msg_count
        except Exception as e:
            logger.exception(str(e))
            speed = 0

        return (tmax, PgesMax, speed)
        # print first 3 messages
        # while msg_count < 3:
        # logger.info("topic: {}".format(topic))
        # logger.info("message: {}".format(message))
        # logger.info("timestamp: {}".format(t))
        # msg_count += 1

    def generate_extraction_request(self, t, p, p_av):
        t_form = int(str(t)[0:13])
        logger.info(f"the time point is: {t_form}")
        extraction_request = {
            "version": "1.0",
            "bagfile_guid": self.guid,  # only for test, no real id!!!
            "extractor_id": "ext1",
            "metadata_list": [{"start": t_form, "end": t_form, "type": "basic",
                               "metadata": {"PowerConsumptionADKit": {"ts": t_form, "Max_PowerconsumptionADKit": p}},
                               "bva": 10}]
        }
        return extraction_request


class MetadataManager(object):
    def __init__(self):
        self.client = ContainerClient()

    def update_bagfile_metadata(self, guid, bagfile_data):
        logger.info(f"Updating metadata for bagfile {guid} "
                    f"with data {bagfile_data}")
        try:
            self.client.update_bagfile(guid, bagfile_data)
        except Exception as e:
            logger.exception(f"Error updating metadata of bagfile {guid}")
            raise e
        else:
            logger.info(f"Successfully updated metadata of bagfile {guid}")



def main(input_path, guid):
    # check whether it is needed to use dla converter
    dla_converter = os.environ.get("DLA_CONVERTER", "False") == "True"
    logger.info(f"Using dla_converter: {dla_converter}")
    dbc_file =  os.environ.get("DBC_FILE")  #e.g."Infrastructure_CAN_222_190227.dbc"
    if dla_converter:
        output_path = os.environ.get("OUTPUT_PATH", "tmp_convertered_file.bag")
        if input_path is None:
            logger.exception("Invalid input path")
            sys.exit(1)
        if dbc_file is None:
            logger.exception("The dbc file is missing, terminating the program")
            sys.exit(1)
        converter = DlaConverter(input_path, output_path)
        converter.execute_dla_converter(dbc_file)
        guid = converter.create_metadata(file_tag=["dla_converter"], file_type="ORIGINAL", processing_state="BEING_PROCESSED")
        input_path = output_path #take the output file as the input of the extractor

    # Extract metadata
    #metadata_manager = MetadataManager()
    extractor = Extractor(input_path, guid)
    #bagfile_data = extractor._generate_bagfile_data()
    #metadata_manager.update_bagfile_metadata(guid, bagfile_data)  #complete the metadat of the bagfile in db
    (t, p, p_av) = extractor.parse_bagfile()

    # Transform output
    extraction_request = extractor.generate_extraction_request(t, p, p_av)
    logger.info("Sending extraction request: {}:".format(extraction_request))

    # Send request
    client = ContainerClient()
    extraction_guid = client.submit_metadata(extraction_request)
    logger.info("Extraction request is inserted into the DB with guid:{}".format(extraction_guid))


if __name__ == '__main__':
    logger.set_logging_folder(
        os.path.abspath(os.path.dirname(__file__)) + '/logs')
    logger.set_log_level('info')

    main(os.environ["INPUT_PATH"], os.environ["BAGFILE_GUID"])
