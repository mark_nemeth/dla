__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import (ENV_SPAN_ID, ENV_TRACE_ID,
                                       ENV_TRACE_OPTIONS)
from kfp import dsl
from kubernetes.client import V1VolumeMount
from kubernetes.client.models import V1EnvVar

from config.config_handler import config_dict
from kubeflow.steps.common import (VM_LABEL_ATTRIBUTE, RETRY_NUM,
                                   PIPELINE_POD_LABEL,
                                   BAGFILE_POD_LABEL, FILE_NAME_POD_LABEL,
                                   sanitize_label, METADATA_API_BASE_URL,
                                   APPINSIGHTS_KEY,
                                   DANF_ENV, AUTH, AUTH_TOKEN_URL,
                                   CPU_RESOURCE_TITLE,
                                   MEMORY_RESOURCE_TITLE, TESTING_POD_LABEL,
                                   extract_file_name_from_path,
                                   AUTH_METADATA_AUDIENCE,
                                   AUTH_METADATA_CLIENT_CRED_ID,
                                   AUTH_METADATA_CLIENT_CRED_SECRET,
                                   CUSTOM_NODE_RESOURCE_TITLE,
                                   CUSTOM_STORAGE_VM_TITLE,
                                   MOUNT_DATA_BASE_PATH,
                                   attach_blobfuse_volume,
                                   get_pvc_volume,
                                   NO_PROXY,
                                   HTTP_PROXY,
                                   HTTPS_PROXY,
                                   AUTH_DATAHANDLER_AUDIENCE,
                                   AUTH_DATAHANDLER_CLIENT_CRED_ID,
                                   AUTH_DATAHANDLER_CLIENT_CRED_SECRET,
                                   AUTH_SEARCH_AUDIENCE,
                                   AUTH_SEARCH_CLIENT_CRED_ID,
                                   AUTH_SEARCH_CLIENT_CRED_SECRET, BaseStep)

class AtpdmlExtractorOp(dsl.ContainerOp):

    NAME = "atpdml-extractor"
    VERSION = "1.0"
    DOCKER_IMG = config_dict.get_value(NAME)
    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "2Gi"
    MEMORY_LIMIT = "6Gi"
    BAGFILE_RESPONSIBLE = "BAGFILE_SKIP_RESPONSIBLE"
    CO_DRIVER_EVENT = "/events"
    INPUT_MOUNT_BASE_PATH = "/data"
    OUTPUT_MOUNT_BASE_PATH = "/outputs"

    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               input_file_path,
               output_folder,
               pvc_name,
               vin,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_file_path: path to the bagfile in the pod
        :param output_folder: path in pod to which output files
            should be written
        :param pvc_name: name of the pvc to mount
        :param vin: of the vehicle from which the processed bagfile originates
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the AtpdmlExtractor operation
        """
        step = AtpdmlExtractorOp().set_retry(RETRY_NUM) \
            .add_pod_label(name=PIPELINE_POD_LABEL, value=sanitize_label(cls.NAME)) \
            .add_pod_label(name=BAGFILE_POD_LABEL, value=sanitize_label(bagfile_guid)) \
            .add_pod_label(name=FILE_NAME_POD_LABEL, value=sanitize_label(extract_file_name_from_path(input_file_path)))

        if test_mode:
            step.enable_test_mode()

        step.add_env_variable(V1EnvVar(name="BAGFILE_PATH", value=input_file_path)) \
            .add_env_variable(V1EnvVar(name="BAGFILE_GUID", value=bagfile_guid)) \
            .add_env_variable(V1EnvVar(name="METADATA_API_BASE_URL", value=METADATA_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="FLOW_RUN_GUID", value=flow_run_guid)) \
            .add_env_variable(V1EnvVar(name="APPINSIGHTS_INSTRUMENTATIONKEY", value=APPINSIGHTS_KEY)) \
            .add_env_variable(V1EnvVar(name="DANF_ENV", value=DANF_ENV)) \
            .add_env_variable(V1EnvVar(name="EXTRACTOR_NAME", value=cls.NAME)) \
            .add_env_variable(V1EnvVar(name="EXTRACTOR_VERSION", value=cls.VERSION)) \
            .add_env_variable(V1EnvVar(name="BAGFILE_RESPONSIBLE", value=cls.BAGFILE_RESPONSIBLE)) \
            .add_env_variable(V1EnvVar(name="CO_DRIVER_EVENT", value=cls.CO_DRIVER_EVENT)) \
            .add_env_variable(V1EnvVar(name="no_proxy", value=NO_PROXY)) \
            .add_env_variable(V1EnvVar(name="HTTP_PROXY", value=HTTP_PROXY)) \
            .add_env_variable(V1EnvVar(name="HTTPS_PROXY", value=HTTPS_PROXY)) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_ID, value=tracing.get_propagation_vars().get(ENV_TRACE_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_SPAN_ID, value=tracing.get_propagation_vars().get(ENV_SPAN_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_OPTIONS, value=tracing.get_propagation_vars().get(ENV_TRACE_OPTIONS))) \
            .add_env_variable(V1EnvVar(name="AUTH", value=AUTH)) \
            .add_env_variable(V1EnvVar(name="AUTH_TOKEN_URL", value=AUTH_TOKEN_URL)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_AUDIENCE", value=AUTH_DATAHANDLER_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_ID", value=AUTH_DATAHANDLER_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_SECRET", value=AUTH_DATAHANDLER_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_AUDIENCE", value=AUTH_METADATA_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_ID", value=AUTH_METADATA_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_SECRET", value=AUTH_METADATA_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_AUDIENCE", value=AUTH_SEARCH_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_ID", value=AUTH_SEARCH_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_SECRET", value=AUTH_SEARCH_CLIENT_CRED_SECRET))

        volume_name = "mapr-mount"
        step.add_volume(get_pvc_volume(volume_name, pvc_name)) \
            .add_volume_mount(V1VolumeMount(
                mount_path=cls.INPUT_MOUNT_BASE_PATH,
                name=volume_name)) \
            .add_volume_mount(V1VolumeMount(
                mount_path=cls.OUTPUT_MOUNT_BASE_PATH,
                name=volume_name))
        step.add_resource_request(CPU_RESOURCE_TITLE, cls.CPU_REQUEST) \
            .add_resource_request(MEMORY_RESOURCE_TITLE, cls.MEMORY_REQUEST) \
            .add_resource_limit(CPU_RESOURCE_TITLE, cls.CPU_LIMIT) \
            .add_resource_limit(MEMORY_RESOURCE_TITLE, cls.MEMORY_LIMIT)

        return step
      
    def __init__(self):
        super().__init__(
            name=self.NAME,
            image=self.DOCKER_IMG,
        )
