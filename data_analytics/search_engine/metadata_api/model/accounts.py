__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import Optional, List

from pydantic import BaseModel

from model.types import Guid


###############################################################################
# domain models
###############################################################################

class Account(BaseModel):
    version: str = "1.0"
    guid: Guid
    name: str
    key_hash: str
    available_quota: int
    entitlements: List[str]

###############################################################################
# request/response models
###############################################################################

class CreateAccountRequest(BaseModel):
    name: str
    key: str
    available_quota: int
    entitlements: List[str] = []


class UpdateAccountRequest(BaseModel):
    name: Optional[str]
    key: Optional[str]
    available_quota: Optional[int]
    entitlements: Optional[List[str]]
