"""ContainerClient"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List, Union
from DANFUtils.logging import logger
from ContainerClient.handlers.metadata_handler import MetadataHandler
from ContainerClient.handlers.search_handler import SearchHandler
from ContainerClient.handlers.data_handler import DataHandler


class ContainerClient:
    """
    ContainerClient class
    """

    def __init__(self):
        self.metadata_handler = MetadataHandler()
        self.search_handler = SearchHandler()
        self.data_handler = DataHandler()
        self._last_response = None

    def __enter__(self):
        """"""
        return self

    def __exit__(self, exc_type, exc_value, tb):
        pass

    def get_bagfile(self, bagfile_guid: str, whitelist: List[str] = None):
        """
        Retrieves a single bagfile.

        :param bagfile_guid: GUID identifying the bagfile
        :param whitelist: (default: None) list of additional properties to
            include in response
        :return: the bagfile
        """
        bagfile, res = self.metadata_handler.get_bagfile(
            bagfile_guid, whitelist)
        self._last_response = res
        return bagfile

    def create_bagfile(self, bagfile_data_dict: dict):
        """
        Add a single bagfile to the metadata DB
        :param bagfile_data_dict: Bagfile data dictionary
            Example:
            {
                "link": "/path/to/bagfile.bag",
                "version": '0.1'
            }
        :return: GUID of the added bagfile

        """
        guid, res = self.metadata_handler.create_bagfile(bagfile_data_dict)
        self._last_response = res
        return guid

    def update_bagfile(self, guid: str, bagfile_data_dict: dict):
        """
        Update a single bagfile in the metadata DB
        :param guid: Bagfile guid
        :param bagfile_data_dict: Bagfile data dictionary
            Example:
            {
                "size": 21231,
                "num_messages": 4444,
                "start": 1557847224000,
                "end": 1557847287000,
                "duration": 752728,
                "vehicle_id_num": "V-123-234",
                "extractor": "ext1",
                "topics": ["vin",...],
            }
        """
        self._last_response = self.metadata_handler.update_bagfile(
            guid, bagfile_data_dict)

    def upsert_bagfile(self, bagfile_data_dict: dict):
        """
        Either update and reactivate a *soft deleted* bagfile or create a new one in metadata DB.

        After the general processing flow, 'link' shows the original place of the bagfile, while
        'current_link' shows the updated path, where the bagfile is moved after processing.

        At reprocessing we use the file on the updated path (current_link).

        :param bagfile_data_dict: Bagfile data dictionary
        :return: GUID of the added/updated bagfile
        """
        soft_deleted_bagfiles = self.search_bagfiles(
            {'link': bagfile_data_dict['link'],
             'is_deleted': True})
        if soft_deleted_bagfiles:
            update_dict = dict(bagfile_data_dict)
            update_dict.pop('link')
            update_dict['is_deleted'] = False
            guid = soft_deleted_bagfiles[0]['guid']
            self.update_bagfile(guid, update_dict)
            return guid

        guid = self.create_bagfile(bagfile_data_dict)
        return guid

    def update_bagfile_processing_state(self, guid: str, processing_state: str, cascade_to: List = None):
        """
        Update a single bagfile in the metadata DB
        :param guid: Bagfile guid
        :param processing_state: State to be updated to
        :param cascade_to: List of strings to mark if childbagfiles or extractor_requests should also be updated
        """
        cascade_to = cascade_to or []
        self._last_response = self.metadata_handler.update_bagfile_processing_state(
            guid, processing_state, cascade_to)

    def create_drivefile(self, drive_file_dict: dict):
        """
        Add a single drive file to the metadata DB
        :param drive_file_dict:  data dictionary
            Example:
            {
                "link": "/path/to/drive.json",
                "version": '0.1'
            }
        :return: GUID of the added drive.json

        """
        guid, res = self.metadata_handler.create_drivefile(drive_file_dict)
        self._last_response = res
        return guid

    def update_drive(self, guid: str, data: dict):
        """
        Update a drive in the drive collection of DB
        :param guid: guid of the drive
        :param data: fields to update
        """
        self._last_response = self.metadata_handler.update_drive(guid, data)

    def upsert_drive(self, drive_data_dict: dict):
        """
        Either update and reactivate a *soft deleted* drive or create a new one in metadata DB.
        :param drive_data_dict: Drive data dictionary
        :return: GUID of the added/updated drive
        """
        soft_deleted_drives = self.search_drivefiles(
            {'guid': drive_data_dict['guid'],
             'is_deleted': True})
        if soft_deleted_drives:
            update_dict = dict(drive_data_dict)
            update_dict.pop('guid')
            update_dict['is_deleted'] = False
            guid = soft_deleted_drives[0]['guid']
            self.update_drive(guid, update_dict)
            return guid

        guid = self.create_drivefile(drive_data_dict)
        return guid

    def delete_bagfile(self, guid: str, cascade_to_list: List[str] = None, hard_delete: bool = False):
        """
        Delete a single bagfile from the metadata DB
        :param cascade_to_list: List if childbagfile or extractor_requests should be also deleted
        :param guid: Bagfile GUID
        :param hard_delete: If False, then only a flag is used to mark the document
        """
        self._last_response = self.metadata_handler.delete_bagfile(
            guid, cascade_to_list, hard_delete)

    def delete_drivefile(self, guid: str, cascade_to_list: List[str] = None, hard_delete: bool = False):
        """
        Delete a single drivefile from the metadata DB
        :param cascade_to_list: List if childbagfile or bagfiles should be also deleted
        :param guid: drive GUID
        :param hard_delete: If False, then only a flag is used to mark the document
        """
        self._last_response = self.metadata_handler.delete_drivefile(guid, cascade_to_list, hard_delete)

    def add_bagfile_custom_attributes(self, bagfile_guid: str,
                                      extractor_id: str,
                                      custom_attributes: dict):
        """
        Adds custom attributes to the bagfile of given bagfile guid.
        :param bagfile_guid:
        :param custom_attributes: dictionary container custom attributes
            Example:
            "custom_attributes": {
                "number_of_disenagement": 5,
                "total_engaged_distance": 12000
            }
        :return: None
        """
        self._last_response = self.metadata_handler.add_bagfile_custom_attributes(
            bagfile_guid, extractor_id, custom_attributes)

    def add_bagfile_state_event(self, bagfile_guid: str, state_event: dict):
        """
        Appends the specified state_event to the state attribute of the
        bagfile identified by the given bagfile_guid.
        :param bagfile_guid:
        :param state_event: Bagfile state event dictionary
            Example:
            {
                "name": "extractor",
                "version": "v1",
                "time": 1475272800000,
                "status": "SUCCESS",
                "report": {
                    "message": "All good"
                }
            }
        :return: None
        """
        self._last_response = self.metadata_handler.add_bagfile_state_event(
            bagfile_guid, state_event)

    def create_childbagfile(self, childbagfile_data_dict: dict):
        """
        Add a single childbagfile to the metadata DB
        :param childbagfile_data_dict: Childbagfile data dictionary
            Example:
            {
                "version": '0.1',
                "parent_guid": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
                "start": 123456789,
                "end": 123456789,
                "parent_index_start": 1,
                "parent_index_end": 10,
                "metadata": {...}
            }
        :return: GUID of the added childbagfile

        """
        guid, res = self.metadata_handler.create_childbagfile(
            childbagfile_data_dict)
        self._last_response = res
        return guid

    def update_childbagfile(self, childbagfile_guid: str,
                            child_bagfile_data_dict: dict):
        """
        Update a single childbagfile from the metadata DB
        :param childbagfile_guid: Childbagfile GUID
        :param child_bagfile_data_dict: Childbagfile data dictionary
        :return:
        """
        self._last_response = self.metadata_handler.update_childbagfile(
            childbagfile_guid, child_bagfile_data_dict)

    def upsert_childbagfile(self, childbagfile_data_dict: dict):
        """
        Either update and reactivate a *soft deleted* childbagfile or create a new one in metadata DB.

        For childbagfiles we only use 'link' (there is no 'current_link'), this field can be used to search for
        an already soft deleted instance of the same childbagfile.

        :param childbagfile_data_dict: Childbagfile data dictionary
        :return: GUID of the added/updated childbagfile
        """
        soft_deleted_childbagfiles = self.search_childbagfiles(
            {'link': childbagfile_data_dict['link'],
             'is_deleted': True})
        logger.debug(f"upserting the metadata {childbagfile_data_dict} for {childbagfile_data_dict['link']}")
        logger.debug(f"Soft deleted child bag files are {soft_deleted_childbagfiles}")
        if soft_deleted_childbagfiles:
            update_dict = dict(childbagfile_data_dict)
            update_dict['is_deleted'] = False
            guid = soft_deleted_childbagfiles[0]['guid']
            self.update_childbagfile(guid, update_dict)
            return guid

        guid = self.create_childbagfile(childbagfile_data_dict)
        return guid

    def add_childbagfile_custom_attributes(self, childbagfile_guid: str,
                                           extractor_id: str,
                                           custom_attributes: dict):
        """
        Adds custom attributes to the childbagfile of given childbagfile guid.
        :param childbagfile_guid:
        :param custom_attributes: dictionary container custom attributes
            Example:
            "custom_attributes": {
                "number_of_disenagement": 5
            }
        :return: None
        """
        self._last_response = self.metadata_handler.add_childbagfile_custom_attributes(
            childbagfile_guid, extractor_id, custom_attributes)

    def delete_childbagfile(self, guid: str, hard_delete: bool = False):
        """Delete a single bagfile from the metadata DB

        :param guid: Childbagfile GUID
        :param hard_delete: If False, then only a flag is used to mark the document
        """
        self._last_response = self.metadata_handler.delete_childbagfile(guid, hard_delete)

    def submit_metadata(self, metadata_wrapper_dict: Union[list, dict], file_name: str = None):
        """
        Add extracted events the metadata DB.

        :param metadata_wrapper_dict: List of dicts containing extracted
            events. Alternatively, a dict can also be passed (legacy format).

            Example:
            [
                {
                    "version": "1.0",
                    "bagfile_guid": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
                    "extractor_id": "fallback",
                    "extractor_version": "5.0",
                    "start": 123456789,
                    "end": 123456789,
                    "metadata": {...},
                    "bva": 10,
                    "event_type": "filter"
                }
            ]

            Example (legacy format):
            {
            "bagfile_guid": 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'
            "extractor_id": 'ext1',
            "version": '0.1',
            "metadata_list":
                [
                    {
                        "start": 1557847224000,
                        "end": 1557847263000,
                        "metadata": {
                            'rain': 1,
                            'fingerprint': 'ABC123456'
                        },
                        "bva": 5
                    }
                ]
            }

        :return: list of GUIDs of the submitted extracted events
        """
        guids, res = self.metadata_handler.submit_metadata(
            metadata_wrapper_dict, file_name)

        self._last_response = res

        return guids

    def delete_extractor(self, guid: str):
        """
        Delete a single extractor entry from the metadata DB
        :param guid: extractor GUID
        """
        self._last_response = self.metadata_handler.delete_extractorrequest(
            guid)

    def create_feature_bucket(self, feature_bucket_dict: dict) -> str:
        """
        Add a single feature_bucket to the metadata DB
        :param feature_bucket_dict: feature bucket data dictionary
            Example:
            {
                "version": "1.0",
                "guid": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXX1",
                "bagfile_guid": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXX2",
                "start": 1563262908000,
                "end": 1563262911001,
                "start_index": 60,
                "end_index": 119,
                "features": [{
                                "ts": 1563262908000,
                                "vel": 0.5,
                                "acc": 0.3,
                                "eng_state": 11
                            }, {
                                "ts": 1563262908000,
                                "vel": 0.6,
                                "acc": 0.2,
                                "eng_state": 5
                            }
                ],
                "is_test_data": 1
            }

        :return: GUID of the added feature_bucket
        """
        guid, res = self.metadata_handler.create_feature_bucket(
            feature_bucket_dict)
        self._last_response = res
        return guid

    def update_feature_bucket(self, feature_bucket_guid: str,
                              feature_bucket_dict: dict) -> None:
        """
        Update a single feature_bucket from the metadata DB
        :param feature_bucket_guid: feature_bucket GUID
        :param feature_bucket_dict: feature_bucket data dictionary
        :return:
        """
        self._last_response = self.metadata_handler.update_feature_bucket(
            feature_bucket_guid, feature_bucket_dict)

    def get_snippets_bva(self,
                         bagfile_guid: str,
                         bva_limit: int,
                         gps_subsampling_ratio: int = 5,
                         extractors: List[str] = None,
                         generate_child_bagfiles: bool = True):
        """
        Get list of child_bagfile snippet dicts for bagfile sequences with an
        max bva value above the provided bva_limit

        :param bagfile_guid: Bagfile GUID
        :param bva_limit: The minimum threshold for the business value
        :param gps_subsampling_ratio: Subsampling ratio of gps_track GeoJSON
            aggregation
        :param extractors: (optional) list of extractors to consider for
            childbagfile creation
        :param generate_child_bagfiles: Generate child bagfiles or not
        :return: A list of child_bagfile snippet dicts
        """
        child_bagfiles, res = self.search_handler.get_snippets_bva(
            bagfile_guid,
            bva_limit,
            gps_subsampling_ratio=gps_subsampling_ratio,
            extractors=extractors,
            generate_child_bagfiles=generate_child_bagfiles)
        self._last_response = res
        if not child_bagfiles:
            logger.warning(
                'BVA Snippet calculation returned an empty list. ' +
                'Bagfile ' + str(bagfile_guid) +
                ' seems to have no sequences(metadata/bva) with maximum_bva>bva_limit('
                + str(bva_limit) + ').')
        return child_bagfiles

    def delete_event(self, event_id: str, pop_receipt: str):
        """
        Delete the event message from the storage queue

        :param event_id:
        :param pop_receipt:
        :return:
        """
        return self.data_handler.delete_event_record(event_id, pop_receipt)

    def get_all_bagfile_links(self):
        """
        Get paths to all bagfiles

        :return: List of all bagfile links
        """
        bagfile_links = self.search_handler.get_bagfile_links()
        return bagfile_links

    def get_all_drive_links(self):
        """
        Get all drive files's path. each drive.json contains a key "file_path"

        :return: List of all drive links/paths
        """
        drive_links, res = self.search_handler.get_drive_links()
        self._last_response = res
        return drive_links

    def get_snippets_extractor(self,
                               bagfile_guid: str,
                               bva_limit: int = 0,
                               extractors: List[str] = None,
                               generate_child_bagfiles: bool = True):
        """
        Get list of child_bagfile snippet dicts for bagfile sequences based
        on extractor metadata

        :param bagfile_guid: Bagfile GUID
        :param bva_limit: The minimum threshold for the business value
        :param extractors: (optional) list of extractors to consider for
            childbagfile creation
        :param generate_child_bagfiles: Generate child bagfiles or not
        :return: A list of child_bagfile snippet dicts
        """
        child_bagfiles, res = self.search_handler.get_snippets_extractor(
            bagfile_guid, bva_limit, extractors, generate_child_bagfiles)
        self._last_response = res
        if not child_bagfiles:
            logger.warning(f"Extractor Snippet calculation returned an empty "
                           f"list for Bagfile: {bagfile_guid}")
        return child_bagfiles

    def search_bagfiles(self,
                        query: dict,
                        limit: int = None,
                        offset: int = None,
                        sortby: str = None):
        """
        Search bagfiles in the database based on the json query

        :param query: The search key value pairs to include in the query params
        :param limit:
        :param offset:
        :param sortby:
        :return: A list of bagfiles based on the search query
        """
        bagfiles = self.search_handler.search_bagfiles(query, limit, offset,
                                                       sortby)
        return bagfiles

    def search_drivefiles(self,
                          query: dict,
                          limit: int = None,
                          offset: int = None,
                          sortby: str = None):
        """
        Search drivefiles in the database based on the json query

        :param query: The search key value pairs to include in the query params
        :param limit:
        :param offset:
        :param sortby:
        :return: A list of drivefiles based on the search query
        """
        drivefiles = self.search_handler.search_drivefiles(
            query, limit, offset, sortby)
        return drivefiles

    def add_sync_flag(self, bagfile_guid: str):
        """
        Adds the guid of the bagfile to the sync document
        to indicate which files need to be synced
        :param bagfile_guid: String
        """
        self.metadata_handler.add_flag(flag_type="sync",
                                       bagfile_guid=bagfile_guid)

    def remove_sync_flag(self):
        """
        Removes the first guid from the sync flags document
        """
        self.metadata_handler.delete_flag(flag_type="sync")

    def add_delete_flag(self, bagfile_guid: str):
        """
        Adds the guid of the bagfile to the delete document
        to indicate which files need to be deleted
        :param bagfile_guid: String
        """
        self.metadata_handler.add_flag(flag_type="delete",
                                       bagfile_guid=bagfile_guid)

    def remove_delete_flag(self):
        """
        Removes the first guid from the delete flags document
        """
        self.metadata_handler.delete_flag(flag_type="delete")

    def get_filters(self):
        """
        Get all the cache values for the duration min max,
        locations, size min max , topics and vehicle id
        """
        return self.search_handler.get_filters()

    def get_unsynced_bagfile_guids(self) -> List[str]:
        """
        Search for bagfile guids which are not synced to Azure
        :return: list of bagfile guids
        """
        bagfiles = self.search_handler.get_unsynced_bagfile_guids()
        return bagfiles

    def get_bagfile_guids_to_delete(self) -> List[str]:
        """
        Search for bagfile guids which are not deleted in
        Azure but already deleted on prem
        :return: list of bagfile guids
        """
        bagfiles = self.search_handler.get_bagfile_guids_to_delete()
        return bagfiles

    def search_childbagfiles(self,
                             query: dict,
                             limit: int = None,
                             offset: int = None,
                             sortby: str = None):
        """
        Search childbagfiles in the database based on the json query

        :param query: The search key value pairs to include in the query params
        :param limit:
        :param offset:
        :param sortby:
        :return: A list of childbagfiles based on the search query
        """
        childbagfiles = self.search_handler.search_childbagfiles(
            query, limit, offset, sortby)
        return childbagfiles

    def import_metadata(self, sync_metadata):
        """
        Export/Import metadata (bagfiles, childbagfiles, sequences, exrtactor_requests).
        Aim: Export metadata from On Prem DB and import to CosmosDB

        :param sync_metadata: dict of metadata
        :return:
        """
        self.metadata_handler.import_metadata(sync_metadata)

    def search_sequences(self, query: dict):
        """
        Search sequences in the database based on the json query

        :param query: The search key value pairs to include in the query params
        :return: A list of sequences
        """
        sequences = self.search_handler.search_sequences(query)
        return sequences

    def search_extractor_requests(self, query):
        """
        Search extractor requests in the database based on the json query

        :param query: The search key value pairs to include in the query params
        :return: A list of extractor_requests
        """
        extractor_requests = self.search_handler.search_extractor_requests(
            query)
        return extractor_requests

    def create_flow(self, flow_data):
        """Create a new flow.

        :param flow_data: Flow data dictionary
        :return: The newly created flow as dict
        """
        flow, res = self.metadata_handler.create_flow(flow_data)
        self._last_response = res
        return flow

    def get_flow(self, guid):
        """Retrieves a single flow.

        :param guid: GUID identifying the flow
        :return: the flow as dict
        """
        flow_run, res = self.metadata_handler.get_flow(guid)
        self._last_response = res
        return flow_run

    def create_flow_run(self, flow_run_data):
        """Create a new flow run.

        :param flow_run_data: Flow run data dictionary
        :return: The newly created flow run as dict
        """
        flow_run, res = self.metadata_handler.create_flow_run(flow_run_data)
        self._last_response = res
        return flow_run

    def get_flow_run(self, guid):
        """Retrieves a single flow run.

        :param guid: GUID identifying the flow run
        :return: the flow run as dict
        """
        flow_run, res = self.metadata_handler.get_flow_run(guid)
        self._last_response = res
        return flow_run

    def update_flow_run(self, guid, flow_run_update):
        """Updates a single flow run.

        :param guid: GUID identifying the flow run
        :param flow_run_update: dict containing the updates to perform
        """
        res = self.metadata_handler.update_flow_run(guid, flow_run_update)
        self._last_response = res

    def add_flow_run_step_run(self, guid, step_name, flow_run_step_run_data):
        """Adds a new step run to a flow run.

        :param guid: GUID identifying the flow run
        :param step_name: name of the step to which run should be added
        :param flow_run_step_run_data: data of the step run as dict
        """
        res = self.metadata_handler.add_flow_run_step_run(
            guid, step_name, flow_run_step_run_data)
        self._last_response = res

    def simulate_new_file(self, path, output_dir):
        """

        :param path: path of the file to start the simulation
        :param output_dir: output directory for the output files
        :return:
        """
        data, res = self.data_handler.simulate_new_file(path, output_dir)
        self._last_response = res
        return data

    def file_check(self, paths, delete_files=False):
        """

        :param paths: paths of the files to be checked
        :param delete_files: if True delete the checked files otherwise don't delete the files
        :return:
        """
        data, res = self.data_handler.file_check(paths, delete_files)
        self._last_response = res
        return data

    def cache_vehicleids(self):
        """
        Caches vehicleIDs in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_vehicleids()
        return res

    def cache_topics(self):
        """
        Caches topics in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_topics()
        return res

    def cache_origins(self):
        """
        Caches origins in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_origins()
        return res

    def cache_drivers(self):
        """
        Caches drivers in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_drivers()
        return res

    def cache_operators(self):
        """
        Caches operators in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_operators()
        return res

    def cache_hardware_releases(self):
        """
        Caches hardware releases in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_hardware_releases()
        return res

    def cache_driven_by_values(self):
        """
        Caches driven by values in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_driven_by_values()
        return res

    def cache_bagfile_size_duration_time(self):
        """
        Caches bagfile size_duration_time in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_bagfile_size_duration_time()
        return res

    def cache_childbagfile_size_duration_time(self):
        """
        Caches childbagfile size_duration_time in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_childbagfile_size_duration_time()
        return res

    def cache_drive_duration_time(self):
        """
        Caches drive duration and time in the database
        :return: guid of the document
        """
        res = self.metadata_handler.cache_drive_duration_time()
        return res

    def search_flows(self,
                     query: dict,
                     limit: int = None,
                     offset: int = None,
                     sortby: str = None):
        """Search flows based on the json query

        :param query: The search key value pairs to include in the query params
        :param limit: Maximum number of results to retrieve
        :param offset: offset to use for pagination
        :param sortby: Attribute to sort by
        :return: A list of bagfiles based on the search query
        """
        flows = self.search_handler.search_flows(query, limit, offset, sortby)
        return flows

    def search_flow_runs(self,
                         query: dict,
                         limit: int = None,
                         offset: int = None,
                         sortby: str = None):
        """Search flow runs based on the json query

        :param query: The search key value pairs to include in the query params
        :param limit: Maximum number of results to retrieve
        :param offset: offset to use for pagination
        :param sortby: Attribute to sort by
        :return: A list of bagfiles based on the search query
        """
        flows = self.search_handler.search_flow_runs(query, limit, offset,
                                                     sortby)
        return flows

    def search_catalog(self,
                       query: dict,
                       limit: int = None,
                       offset: int = None,
                       sortby: str = None):
        """Search catalogs based on the json query

        :param query: The search key value pairs to include in the query params
        :param limit: Maximum number of results to retrieve
        :param offset: offset to use for pagination
        :param sortby: Attribute to sort by
        :return: A list of catalogs based on the search query
        """
        catalogs = self.search_handler.search_catalog(query, limit, offset,
                                                      sortby)
        return catalogs

    def update_url_status_in_file_catalog(self, file_guid, url_index, url_status):
        """Update url status of an existing url in the file catalog"""
        self.metadata_handler.update_url_status_in_file_catalog(file_guid, url_index, url_status)

    def refill_quota(self):
        """Trigger endpoint to refill the quota"""
        self._last_response = self.metadata_handler.refill_quota()
