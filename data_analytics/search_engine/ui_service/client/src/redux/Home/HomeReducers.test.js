import {HomeReducer} from './HomeReducers';
import {
  fetchBagfilesError,
  fetchBagfilesSuccess,
  fetchOriginalFilesError,
  fetchOriginalFilesSuccess,
  fetchAlwaysonFilesError,
  fetchAlwaysonFilesSuccess,
  fetchBagfileFiltersSuccess,
  fetchBagfileFiltersError,
  fetchOriginalFileFiltersSuccess,
  fetchOriginalFileFiltersError,
  fetchAlwaysonFileFiltersSuccess,
  fetchAlwaysonFileFiltersError,
  fetchChildBagfileFiltersSuccess,
  fetchChildBagfileFiltersError,
  setCurrentSearchQuery,
  setFilterBySearchQuery,
  reprocessingBagfileSuccess,
  reprocessingBagfileError,
} from './HomeActions';
import {
  getTestAlwaysonFileList,
  getTestOriginalFileList,
  getTestFiltersList,
  getTestBagfileList
} from '../../setupTests';

describe('Home reducers', () => {

  it('should return the initial state', () => {
    expect(HomeReducer(undefined, {})).toEqual({});
  });

  describe('fetch bagfiles', () => {
    it('should handle fetchBagfilesError', () => {
      expect(HomeReducer({}, {
        type: fetchBagfilesError.type,
      })).toEqual({
        error: true,
        errorMessage: undefined,
        errorMessageType: 'danger',
        bagfiles: [],
        currentFiles: [],
        total: 0,
        showLoading: false
      });
    });
    it('should handle fetchOriginalFilesSuccess', () => {
      const bagfiles = getTestBagfileList(true);
      const total = bagfiles.length;
      expect(HomeReducer({}, {
        type: fetchBagfilesSuccess.type,
        payload: {bagfiles, total}
      })).toEqual({
        error: false,
        bagfiles: bagfiles,
        currentFiles: bagfiles,
        total: total,
        showTableMessage: false,
        showLoading: false
      });
    });
  });

  describe('fetch bagfile filters', () => {
    it('should handle fetchBagfileFiltersError', () => {
      expect(HomeReducer({filterOptions: {}}, {
        type: fetchBagfileFiltersError.type,
      })).toEqual({
        error: true,
        filterOptions: {
          bagfileVehicleIdList: [],
          bagfileSizeLimits: [],
          bagfileDurationLimits: [],
          bagfileTopicList: []
        },
      });
    });
    it('should handle fetchBagfileFiltersSuccess', () => {
      const filtersPayload = getTestFiltersList().results;
      expect(HomeReducer({filterOptions: {}}, {
        type: fetchBagfileFiltersSuccess.type,
        payload: filtersPayload
      })).toEqual({
        error: false,
        filterOptions: {
          bagfileVehicleIdList: filtersPayload.vehicle_ids,
          bagfileSizeLimits: filtersPayload.size_min_max,
          bagfileDurationLimits: filtersPayload.duration_min_max,
          bagfileTopicList: filtersPayload.topics
        },
      });
    });
  });

  describe('fetch original files', () => {
    it('should handle fetchOriginalFilesError', () => {
      expect(HomeReducer({}, {
        type: fetchOriginalFilesError.type,
      })).toEqual({
        error: true,
        errorMessage: undefined,
        errorMessageType: 'danger',
        originalFiles: [],
        currentFiles: [],
        total: 0,
        showLoading: false
      });
    });
    it('should handle fetchOriginalFilesSuccess', () => {
      const bagfiles = getTestOriginalFileList(true);
      const total = bagfiles.length;
      expect(HomeReducer({}, {
        type: fetchOriginalFilesSuccess.type,
        payload: {bagfiles, total}
      })).toEqual({
        error: false,
        originalFiles: bagfiles,
        currentFiles: bagfiles,
        total: total,
        showTableMessage: false,
        showLoading: false
      });
    });
  });

  describe('fetch original files filters', () => {
    it('should handle fetchOriginalFileFiltersError', () => {
      expect(HomeReducer({filterOptions: {}}, {
        type: fetchOriginalFileFiltersError.type,
      })).toEqual({
        error: true,
        filterOptions: {
          originalFileVehicleIdList: [],
          originalFileSizeLimits: [],
          originalFileDurationLimits: [],
          originalFileTopicList: []
        },
      });
    });
    it('should handle fetchOriginalFileFiltersSuccess', () => {
      const filtersPayload = getTestFiltersList().results;
      expect(HomeReducer({filterOptions: {}}, {
        type: fetchOriginalFileFiltersSuccess.type,
        payload: filtersPayload
      })).toEqual({
        error: false,
        filterOptions: {
          originalFileVehicleIdList: filtersPayload.vehicle_ids,
          originalFileSizeLimits: filtersPayload.size_min_max,
          originalFileDurationLimits: filtersPayload.duration_min_max,
          originalFileTopicList: filtersPayload.topics
        },
      });
    });
  });

  describe('fetch alwayson files', () => {
    it('should handle fetchAlwaysonFilesError', () => {
      expect(HomeReducer({}, {
        type: fetchAlwaysonFilesError.type,
      })).toEqual({
        error: true,
        errorMessage: undefined,
        errorMessageType: 'danger',
        alwaysonFiles: [],
        currentFiles: [],
        total: 0,
        showLoading: false
      });
    });
    it('should handle fetchAlwaysonFilesSuccess', () => {
      const bagfiles = getTestAlwaysonFileList(true);
      const total = bagfiles.length;
      expect(HomeReducer({}, {
        type: fetchAlwaysonFilesSuccess.type,
        payload: {bagfiles: bagfiles, total}
      })).toEqual({
        error: false,
        alwaysonFiles: bagfiles,
        currentFiles: bagfiles,
        total: total,
        showTableMessage: false,
        showLoading: false
      });
    });
  });

  describe('fetch alwayson files filters', () => {
    it('should handle fetchAlwaysonFileFiltersError', () => {
      expect(HomeReducer({filterOptions: {}}, {
        type: fetchAlwaysonFileFiltersError.type,
      })).toEqual({
        error: true,
        filterOptions: {
          alwaysonFileVehicleIdList: [],
          alwaysonFileSizeLimits: [],
          alwaysonFileDurationLimits: [],
          alwaysonFileTopicList: []
        },
      });
    });
    it('should handle fetchAlwaysonFileFiltersSuccess', () => {
      const filtersPayload = getTestFiltersList().results;
      expect(HomeReducer({filterOptions: {}}, {
        type: fetchAlwaysonFileFiltersSuccess.type,
        payload: filtersPayload
      })).toEqual({
        error: false,
        filterOptions: {
          alwaysonFileVehicleIdList: filtersPayload.vehicle_ids,
          alwaysonFileSizeLimits: filtersPayload.size_min_max,
          alwaysonFileDurationLimits: filtersPayload.duration_min_max,
          alwaysonFileTopicList: filtersPayload.topics
        },
      });
    });
  });

  describe('fetch childBagfile filters', () => {
    it('should handle fetchChildBagfileFiltersError', () => {
      expect(HomeReducer({filterOptions: {}}, {
        type: fetchChildBagfileFiltersError.type,
      })).toEqual({
        error: true,
        filterOptions: {
          childBagfileVehicleIdList: [],
          childBagfileSizeLimits: [],
          childBagfileDurationLimits: [],
          childBagfileTopicList: []
        },
      });
    });
    it('should handle fetchChildBagfileFiltersSuccess', () => {
      const filtersPayload = getTestFiltersList().results;
      expect(HomeReducer({filterOptions: {}}, {
        type: fetchChildBagfileFiltersSuccess.type,
        payload: filtersPayload
      })).toEqual({
        error: false,
        filterOptions: {
          childBagfileVehicleIdList: filtersPayload.vehicle_ids,
          childBagfileSizeLimits: filtersPayload.size_min_max,
          childBagfileDurationLimits: filtersPayload.duration_min_max,
          childBagfileTopicList: filtersPayload.topics
        },
      });
    });
  });


  describe('search filters', () => {
    it('should handle setCurrentSearchQuery', () => {
      const searchQueryPayload = 'search_query';
      expect(HomeReducer({filters: {}}, {
        type: setCurrentSearchQuery.type,
        payload: searchQueryPayload
      })).toEqual({
        filters: {
          file_name: searchQueryPayload
        }
      });
    });
    it('should handle setFilterBySearchQuery', () => {
      const searchQueryPayload = 'search_query';
      expect(HomeReducer({filters: {}, appliedFilters: {}}, {
        type: setFilterBySearchQuery.type,
        payload: searchQueryPayload
      })).toEqual({
        filters: {
          offset: 0,
          file_name: searchQueryPayload
        },
        appliedFilters: {
          offset: 0,
          file_name: searchQueryPayload
        }
      });
    });
  });

  describe('call reprocessing', () => {
    it('should handle reprocessingBagfileSuccess', () => {
      const payload = 'Successful';
      expect(HomeReducer({}, {
        type: reprocessingBagfileSuccess.type,
        payload: payload
      })).toEqual({
        error: true,
        errorMessage: payload,
        errorMessageType: 'success'
      });
    });
    it('should handle reprocessingBagfileError', () => {
      const payload = 'Failed';
      expect(HomeReducer({}, {
        type: reprocessingBagfileError.type,
        payload: payload
      })).toEqual({
        error: true,
        errorMessage: payload,
        errorMessageType: 'danger'
      });
    });
  });
});