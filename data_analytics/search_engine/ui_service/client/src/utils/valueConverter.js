import {isNumberValid, isStringValid} from './helperFunctions'
import {FileTypes} from '../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter';

//region Size converter
export function sizeConverter(size) {
  if (!isNumberValid(size)) return null;
  return (size < 1000000000)
      ? (size / 1000000).toFixed(2) + ' MB'
      : (size / 1000000000).toFixed(2) + ' GB';
}

export function toGBConverter(byte) {
  if (!isNumberValid(byte)) return null;
  return {
    min: Math.floor(byte.min / 1000000000),
    max: Math.ceil(byte.max / 1000000000)
  }
}

//endregion

//region Duration converter
export function durationConverter(duration) {
  if (!isNumberValid(duration)) return null;
  let milliseconds = parseInt((duration % 1000) / 100),
      seconds = parseInt((duration / 1000) % 60),
      minutes = parseInt((duration / (1000 * 60)) % 60),
      hours = parseInt((duration / (1000 * 60 * 60)) % 24),
      days = parseInt(((duration / (1000 * 60 * 60 * 24))));
  // round seconds with milliseconds
  seconds = (milliseconds < 5) ? seconds : seconds + 1;

  const formattedDays = (days > 0) ? days + 'd ' : '';
  const formattedHours = (hours === 0) ? '' : (hours < 10) ? '0' + hours + 'h ' : hours + 'h ';
  const formattedMinutes = (minutes < 10) ? '0' + minutes + 'm ' : minutes + 'm ';
  const formattedSeconds = (seconds < 10) ? '0' + seconds + 's' : seconds + 's';

  let time = formattedDays + formattedHours + formattedMinutes;
  time += (formattedDays === '') ? formattedSeconds : '';
  return time;
}

//endregion

//region FileName converter
export function fileNameConverter(name, fileType) {
  if (!isStringValid(name)) return null;
  if (fileType === FileTypes.DRIVE)
    return (isStringValid(name) ? name.slice(0, -5) : name);
  else
    return (isStringValid(name) ? name.slice(0, -4) : name);
}

//endregion

//region Status converter
export function statusConverter(status, capitalizeFirstLetter = false) {
  if (!isStringValid(status)) return null;
  let lower = status.replace('_', ' ').toLowerCase();
  if (!capitalizeFirstLetter) return lower;
  return lower.charAt(0).toUpperCase().toString() + lower.substring(1);
}

//endregion

//region DateTime converter
export function timestampConverter(timestamp) {
  if (!isNumberValid(timestamp)) return null;
  return new Date(timestamp).toLocaleString();
}

export function toMinutesConverter(milliseconds) {
  if (!isNumberValid(milliseconds)) return null;
  return {
    min: Math.floor(milliseconds.min / 60000),
    max: Math.ceil(milliseconds.max / 60000)
  }
}

export function dateToTimestampConverter(dateString, endOfTheDay = false) {
  if (endOfTheDay) {
    const date = new Date(dateString);
    date.setDate(date.getDate() + 1);
    return date.getTime() - 1;
  }
  return new Date(dateString).getTime();
}

export function timestampToDateConverter(timestamp) {
  if (!isNumberValid(timestamp)) return;
  const date = new Date(timestamp);
  date.setHours(0, 0, 0, 0);
  return date;
}

export function timestampToShortDateStringConverter(timestamp) {
  if (!isNumberValid(timestamp)) return '';
  const date = new Date(timestamp);
  return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
}

export function timestampToTimeStringConverter(timestamp) {
  if (!isNumberValid(timestamp)) return '';
  return new Date(timestamp).toISOString().substring(11, 19) + " GMT"
}

export function timestampToShortTimeStringConverter(timestamp) {
  if (!isNumberValid(timestamp)) return '';
  return new Date(timestamp).toISOString().substring(11, 16) + " GMT"
}

//endregion

export function meterToMileConverter(distance_in_meters) {
  if (!isNumberValid(distance_in_meters)) return '';
  const miles = Number.parseFloat(distance_in_meters / 1609.35).toFixed(2);
  return miles + " Miles";
}

export function timestampToTimeConverter(timestampMS, capitalizeFirstLetter = true) {
  if ((Date.now() - timestampMS) < 86400000) return secondsToTimeConverter((Date.now() - timestampMS) / 1000, capitalizeFirstLetter) + " ago";
  return timestampToShortDateStringConverter(timestampMS);
}

export function secondsToTimeConverter(time_in_seconds, capitalizeFirstLetter = true) {
  if (!isNumberValid(time_in_seconds)) return '';

  let lowerTimeLabel;
  //bigger than 1 day as textual information
  if (time_in_seconds > 86400) lowerTimeLabel = "more than 1 day remaining";
  //bigger than 2 hours as rounded hour
  else if (time_in_seconds > 7200) lowerTimeLabel = Math.floor(time_in_seconds / 3600) + " hours remaining";
  //bigger than 30 minutes as rounded 10min steps
  else if (time_in_seconds > 1800) lowerTimeLabel = Math.floor(time_in_seconds / 600) * 10 + " minutes remaining";
  //bigger than 10 minutes as rounded 5min steps
  else if (time_in_seconds > 600) lowerTimeLabel = Math.ceil(Math.floor(time_in_seconds / 60) / 5) * 5 + " minutes remaining";
  //bigger than 60 seconds as rounded 1min steps
  else if (time_in_seconds > 60) lowerTimeLabel = Math.floor(time_in_seconds / 60) + " minute(s) remaining";
  //bigger than 0 seconds as textual information
  else if (time_in_seconds > 0) lowerTimeLabel = "less than 1 minute remaining";
  //equal to 0 seconds when transfer succeeded or finished with error as textual information
  else if (time_in_seconds === 0) lowerTimeLabel = "finished";
  //smaller than 0 seconds when transfer is not started yet as textual information
  else if (time_in_seconds < 0) lowerTimeLabel = "not started";

  if (!capitalizeFirstLetter) return lowerTimeLabel;
  return lowerTimeLabel.charAt(0).toUpperCase().toString() + lowerTimeLabel.substring(1);
}