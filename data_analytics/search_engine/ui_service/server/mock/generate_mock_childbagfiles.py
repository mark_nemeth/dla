import random
import uuid
import json
import os

NUMBER_OF_METADATA = 25
OUTPUT_FILENAME = "childbagfiles.json"
VERSION = "0.1"

parent_bagfile_guid_list = [str(uuid.uuid4()), str(uuid.uuid4()), str(uuid.uuid4())]
test_cases = [4, 6]
labels = [18, 20]
extractor_id_list = ["ext1", "ext2", "ext3"]

def create_silent_testing():
    silent_testings = []
    for i in range(0, random.randint(3, 10)):
        silent_testings.append(
            {
                "ts": random.randint(start_time, end_time),
                "objects": [
                    {
                        "testcase": random.choice(test_cases),
                        "position": [
                            random.uniform(200.5, 999.5),
                            random.uniform(200.5, 999.5),
                            random.uniform(200.5, 999.5),
                            random.uniform(200.5, 999.5),
                        ],
                        "label": random.choice(labels)
                    }
                ]
            }
        )
    return silent_testings



data = []
for i in range(0, NUMBER_OF_METADATA):
    start_time = random.randint(1483228800000, 1563454684000)
    duration_time = random.randint(600000, 8000000)
    end_time = start_time + duration_time
    file_name = "/" + str(random.randint(10000000, 999999999)) + "_mock_childbagfile_test.bag"

    data.append({
        "version": VERSION,
        "guid": str(uuid.uuid4()),
        "parent_guid": random.choice(parent_bagfile_guid_list),
        "start": start_time,
        "end": end_time,
        "file_name": file_name,
        "link": "/input/plog/WDD2221591A" + str(random.randint(0, 999999)) + "/2019/07/" + str(random.randint(0, 30)) + file_name,
        "parent_index_start": random.randint(100, 999),
        "parent_index_end": random.randint(100, 999),
        "bva_max": random.randint(0, 10),
        "metadata": {
            "ext1_2": {
                "silent_testing": create_silent_testing()
            }
        },
        "gps_track": {
            "type": "LineString",
            "coordinates": [
                [
                    random.uniform(-200.5, 399.5),
                    random.uniform(-200.5, 399.5),
                ],
                [
                    random.uniform(-200.5, 399.5),
                    random.uniform(-200.5, 399.5),
                ],
                [
                    random.uniform(-200.5, 399.5),
                    random.uniform(-200.5, 399.5),
                ],
                [
                    random.uniform(-200.5, 399.5),
                    random.uniform(-200.5, 399.5),
                ]
            ]
        }
    })
result = {"results": data, "total": len(data)}


outpath = os.path.join("public/mockdata")
if not os.path.exists(outpath):
    os.makedirs(outpath)

outfilePath = os.path.join(outpath, OUTPUT_FILENAME)
with open(outfilePath, 'w') as outfile:
    json.dump(result, outfile, indent=4)

print("Mock json file created: " + outfilePath)
