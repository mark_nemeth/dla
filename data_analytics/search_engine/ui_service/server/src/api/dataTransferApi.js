import configHandlerSingleton from '../configHandler';
import BaseApi from './BaseApi';
import getLogger from '../utils/logging';
import {isObjectValid} from '../utils/helperFunctions';

const logger = getLogger(__filename)

export default class DataTransferApi {

  static getTransfersByDataCatalogFileId(fileId) {
    let config = configHandlerSingleton.getDataTransferSearchApiConfig();
    config.qs = {
      'fileId': fileId
    };

    if (configHandlerSingleton.isNodeEnvLocal()) {

      let mockResponse = [
        {
          requestId: 1,
          targetCluster: 'SV',
          sourceCluster: 'VAI',
          status: 'STARTED',
          progress: 55.49,
          startTime: Date.now(),
          lastUpdated: Date.now(),
          errorMessage: null,
          created: Date.now(),
          queuePosition: 0,
          etaSeconds: 86469,
          transferType: 'SINGLE',
          requiresAdmin: false,
          regexUrl: "",
          retryCount: 0,
          size: 1234567890,
          files: [
            {
              fileGuid: fileId,
              size: 1234567890,
              sourceUrl: "hdfs://059-va2.mbc.de/data/input/folder1/test.bag",
              targetUrl: "hdfs://624.mbc.us/data/transfer/folder1/test.bag",
              excludedReason: ""
            }
          ]
        },
        {
          requestId: 2,
          targetCluster: 'ABT',
          sourceCluster: 'VAI',
          status: 'WAITING',
          progress: 0.0,
          startTime: null,
          lastUpdated: Date.now(),
          errorMessage: null,
          created: Date.now(),
          queuePosition: 2,
          etaSeconds: -1,
          transferType: 'SINGLE',
          requiresAdmin: false,
          regexUrl: "",
          retryCount: 0,
          size: 1234567890,
          files: [
            {
              fileGuid: fileId,
              size: 1234567890,
              sourceUrl: "hdfs://059-va2.mbc.de/data/input/folder1/test.bag",
              targetUrl: "hdfs://abt/data/input/folder1/test.bag",
              excludedReason: ""
            }
          ]
        },
        {
          requestId: 3,
          targetCluster: 'HKS',
          sourceCluster: 'VAI',
          status: 'ERROR',
          progress: 0.0,
          startTime: Date.now(),
          lastUpdated: Date.now(),
          errorMessage: "TransferId 3: Mock error message.",
          created: Date.now() + 2000,
          queuePosition: 0,
          etaSeconds: 0,
          transferType: 'SINGLE',
          requiresAdmin: false,
          regexUrl: "",
          retryCount: 0,
          size: 1234567890,
          files: [
            {
              fileGuid: fileId,
              size: 1234567890,
              sourceUrl: "hdfs://059-va2.mbc.de/data/input/folder1/test.bag",
              targetUrl: "hdfs://738.mbc.de/data/transfer/folder1/test.bag",
              excludedReason: ""
            }
          ]
        },
        {
          requestId: 4,
          targetCluster: 'HKS',
          sourceCluster: 'VAI',
          status: 'WAITING',
          progress: 0.0,
          startTime: Date.now(),
          lastUpdated: Date.now(),
          errorMessage: "",
          created: Date.now(),
          queuePosition: 1,
          etaSeconds: -1,
          transferType: 'SINGLE',
          requiresAdmin: false,
          regexUrl: "",
          retryCount: 0,
          size: 1234567890,
          files: [
            {
              fileGuid: fileId,
              size: 1234567890,
              sourceUrl: "hdfs://059-va2.mbc.de/data/input/folder1/test.bag",
              targetUrl: "hdfs://738.mbc.de/data/transfer/folder1/test.bag",
              excludedReason: ""
            }
          ]
        }
      ];

      return new Promise((resolve, reject) => {
        let transfers = mockResponse.map(mock => {
          return Transfer.createFromApiResponse(mock);
        });
        resolve(transfers);
      });
    } else {
      return BaseApi.doRequest(config).then(transfers => {

        if (!transfers || transfers.length < 1) {
          return [];
        }

        return transfers.map(transfer => {
          return Transfer.createFromApiResponse(transfer);
        })
      });
    }
  }

  static createTransfer(fileGuid, targetCluster, username) {
    logger.debug(`Attempt to create transfer for file ${fileGuid} to cluster ${targetCluster} by user ${username}`);
    let config = configHandlerSingleton.getDataTransferApiConfig();
    config.method = 'POST';
    let body = {
      destinationCluster: targetCluster,
      fileGuid: fileGuid,
      username: username
    };
    config.body = body;
    config.json = true;

    if (configHandlerSingleton.isNodeEnvLocal()) {
      return new Promise((resolve, reject) => {
        const files = [new TransferFile(fileGuid, 1234567890, "hdfs://059-va2.mbc.de/data/input/folder1/test.bag", "hdfs://738.mbc.de/data/transfer/folder1/test.bag", "")];
        let createdTransfer = new Transfer(1, targetCluster, 'VAI', 'WAITING', 0.0,
          Date.now(), null, Date.now(), "", Date.now(), 1, -1, 'SINGLE', null, 0, 1234567890, files);
        resolve(createdTransfer);
      });
    } else {
      return BaseApi.doRequest(config).then(transfer => {
        logger.debug(`Successfully created transfer for file ${fileGuid} to cluster ${targetCluster} by user ${username}`);
        return Transfer.createFromApiResponse(transfer);
      });
    }
  }

  static getApiHealth() {
    if (configHandlerSingleton.isNodeEnvLocal()) {
      return new Promise((resolve, reject) => {
        resolve("UP");
      });
    } else {
      logger.debug('Attempting to fetch Data Transfer API health status.')
      let config = configHandlerSingleton.getDataTransferApiHealthConfig();
      config.method = 'GET';
      return BaseApi.doRequest(config).then(body => {
        return body.status;
      });
    }
  }

  static getLatestIngestPerCarFromApi(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getDataTransferLatestIngestApiConfig();
      config.headers = authHeader;
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static getIngestsFromApi(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getDataTransferSearchIngestApiConfig();
      config.headers = authHeader;
      config.qs = {
        ...this.getNewestElements(req.query)
      }
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  // Sort result for newest ingests
  static getNewestElements(query) {
    if (!isObjectValid(query)) return query;
    query.sortby = '-created';
    return query;
  }
}

class Transfer {
  constructor(requestId, targetCluster, sourceCluster, status, progress, startTimestamp, endTimestamp, lastUpdatedTimestamp, errorMessage, created, queuePosition, etaSeconds, transferType, regexUrl, retryCount, size, files) {
    this.requestId = requestId;
    this.targetCluster = targetCluster;
    this.sourceCluster = sourceCluster;
    this.status = status;
    this.progress = progress;
    this.startTimestamp = startTimestamp;
    this.endTimestamp = endTimestamp;
    this.lastUpdatedTimestamp = lastUpdatedTimestamp;
    this.errorMessage = errorMessage;
    this.created = created;
    this.queuePosition = queuePosition;
    this.etaSeconds = etaSeconds;
    this.transferType = transferType;
    this.regexUrl = regexUrl;
    this.retryCount = retryCount;
    this.size = size;
    this.files = files;
  }

  static createFromApiResponse(body) {
    return new Transfer(body.requestId, body.targetCluster, body.sourceCluster, body.status, body.progress, body.startTime, body.endTime, body.lastUpdated,
      body.errorMessage, body.created, body.queuePosition, body.etaSeconds, body.transferType, body.regexUrl, body.retryCount, body.size, TransferFile.createFromApiResponse(body.files));
  }
}

class TransferFile {
  constructor(fileGuid, size, sourceUrl, targetUrl, excludedReason) {
    this.fileGuid = fileGuid;
    this.size = size;
    this.sourceUrl = sourceUrl;
    this.targetUrl = targetUrl;
    this.excludedReason = excludedReason;
  }

  static createFromApiResponse(files) {
    let fileList = [];
    for (const transferFile of files) {
      fileList.push(new TransferFile(transferFile.fileGuid, transferFile.size, transferFile.sourceUrl, transferFile.targetUrl, transferFile.excludedReason));
    }
    return fileList;
  }
}

class Ingest {
  constructor(ingestId, ingestCluster, carName, status, progress, vin, startTime, endTime, lastUpdated, created, etaSeconds, size, errorMessage, retryCount, files) {
    this.ingestId = ingestId;
    this.ingestCluster = ingestCluster;
    this.carName = carName;
    this.status = status;
    this.progress = progress;
    this.vin = vin;
    this.startTime = startTime;
    this.endTime = endTime;
    this.lastUpdated = lastUpdated;
    this.created = created;
    this.etaSeconds = etaSeconds;
    this.size = size;
    this.errorMessage = errorMessage;
    this.retryCount = retryCount;
    this.files = files;
  }

  static createFromApiResponse(body) {
    return new Ingest(body.ingestId, body.ingestCluster, body.carName, body.status, body.progress, body.vin, body.startTime, body.endTime, body.lastUpdated,
        body.created, body.etaSeconds, body.size, body.errorMessage, body.retryCount, IngestFile.createFromApiResponse(body.files));
  }
}

class IngestFile {
  constructor(url, size, target) {
    this.url = url;
    this.size = size;
    this.target = target;
  }

  static createFromApiResponse(files) {
    let fileList = [];
    for (const ingestFile of files) {
      fileList.push(new IngestFile(ingestFile.url, ingestFile.size, ingestFile.target));
    }
    return fileList;
  }
}