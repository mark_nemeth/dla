"""Search Engine REST API bagfile search route (including feature queries)"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from typing import Tuple

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing
from tornado.escape import recursive_unicode

from handlers.auth import auth

from handlers.reporting_trips_kpi_handler import ReportingTripsKPIHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler

logger = logging.getLogger(__name__)

supported_args = {
    "start": "int",
    "end": "int",
    "location": "str",
    "report_type": "str",
    "ign_dur": "bool",
    "ign_dist": "bool",
    "ign_no_info": "bool",
    "ign_priv": "bool"
}


class ReportingTripsKPIRequestHandler(BaseRequestHandler):
    def initialize(self):
        self.report_handler = ReportingTripsKPIHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, GET')

    @auth.requires_entitlements(["openid"])
    def get(self):
        # TODO: Introduce the possibility to specify in the URL which kpis should be returned
        tracing.start_task_in_context('reportkpi/get',
                                      context_headers=self.request.headers)

        res = None
        total = 0
        status_code = constants["REQ_STATUS_CODE_BAD_REQ"]

        args, err = self.process_args()
        if err is None:
            kpis = self.report_handler.get_kpis(**args)
            total = len(kpis)
            res = {"kpis": kpis}
            status_code = constants["REQ_STATUS_CODE_OK"]
        tracing.end_task()
        utils.build_response(response=self,
                             err=err,
                             results=res,
                             status_code=status_code,
                             total=total)

    def process_args(self):
        res = {}
        total_err = ""
        query_args = recursive_unicode(self.request.query_arguments)

        for arg_name, arg_type in supported_args.items():
            value, err = self.process_single_arg(query_args, arg_name,
                                                 arg_type)
            if value is not None:
                res[arg_name] = value
            else:
                total_err += err
                logger.warning(err)

        if total_err == "":
            return res, None
        return res, total_err

    @staticmethod
    def process_single_arg(query_args: dict, arg_name: str, arg_type: str):
        value = err = None
        try:
            raw_value = query_args[arg_name][0]
            if arg_type == "str":
                value = str(raw_value).lower()
            elif arg_type == "int":
                value = int(raw_value)
            elif arg_type == "bool":
                value = raw_value.lower() == "true"
        except (ValueError, KeyError):
            err = f'Invalid {arg_name} query parameter in report kpis - {arg_type} expected\n'
        return value, err
