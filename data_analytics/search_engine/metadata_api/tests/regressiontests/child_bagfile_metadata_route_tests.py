"""Regressiontests for child bagfile routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from copy import deepcopy

import requests
from DANFUtils.utils import generate_deterministic_guid

from handlers.child_bagfiles import ChildBagFilesHandler
from tests.regressiontests.testutils import get_testdb_connector

INVALID_GUID_NOT_DETECTED = 'Invalid GUID not detected'

# testdata
# can change the localhost into external ip:
base_url = 'http://localhost:8887'
child_bagfile_url = base_url + '/childbagfile/'
tags_url_template = child_bagfile_url + '{}/tags'
childbagfile_url = base_url + '/childbagfile/'
childbagfile_custom_attributes_url_template = childbagfile_url + '{}/custom_attributes'

raw_data_bagfile = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "origin": "DE"
}

raw_data_child_bagfile = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "parent_guid": raw_data_bagfile['guid'],
    "start": 1475272825000,
    "end": 1475272867000,
    "file_name": "test_filename",
    "link": "test_link/test_filename",
    "size": 123456,
    "vehicle_id_num": "v222_test",
    "num_messages": 12345,
    "topics": ["topic1", "topic2"],
    "tags": ["tag1", "tag2"],
    "drive_types": ["drive_types1", "drive_types2"],
    "parent_index_start": 25,
    "parent_index_end": 67,
    "metadata": {
        "is_test_data": 1
    }
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
        con.db['child_bagfiles'].delete_many({'metadata.is_test_data': 1})
        con.db['bagfiles'].insert_one(raw_data_bagfile)


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
        con.db['child_bagfiles'].delete_many({'metadata.is_test_data': 1})


class TestChildBagfilePOST(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_child_bagfile_POST(self):
        data = deepcopy(raw_data_child_bagfile)

        res = requests.post(url=child_bagfile_url, json=data)

        self.assertEqual(201, res.status_code, 'Child Bagfile post failed')
        self.assertRegex(
            res.headers['Location'],
            '/childbagfile/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_child_bagfile_POST_remove_unrequired_attributes(self):
        data = deepcopy(raw_data_child_bagfile)
        del data['parent_index_start']
        del data['parent_index_end']
        del data['tags']
        del data['drive_types']

        res = requests.post(url=child_bagfile_url, json=data)

        self.assertEqual(201, res.status_code, 'Child Bagfile post failed')
        self.assertRegex(
            res.headers['Location'],
            '/childbagfile/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_child_bagfile_POST_missing_attributes(self):
        data = deepcopy(raw_data_child_bagfile)
        del data['guid']
        del data['parent_guid']

        res = requests.post(url=child_bagfile_url, json=data)

        self.assertEqual(400, res.status_code,
                         'Missing attributes not detected')

    @unittest.skip("test-case is broken")
    def test_child_bagfile_POST_add_reference_to_bagfile(self):
        ch = ChildBagFilesHandler()
        data = deepcopy(raw_data_child_bagfile)
        raw_bag = deepcopy(raw_data_bagfile)
        del raw_bag['origin']
        del raw_bag['_id']
        raw_bag['guid'] = generate_deterministic_guid()
        data["parent_guid"] = raw_bag['guid']

        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_bag])

        child_guid1 = ch.add_child_bagfile_to_db(data)
        child_guid2 = ch.add_child_bagfile_to_db(data)

        with connector as con:
            bag = list(con.get_bagfiles([raw_bag["guid"]]))

        self.assertCountEqual(
            [child_guid1, child_guid2], bag[0]["childbagfiles"],
            'Not all childbagfiles (guids) added to bagfile!')
        self.assertEqual(child_guid1, bag[0]["childbagfiles"][0],
                         'Childbagfile 1 guid not equal in bagfile!')
        self.assertEqual(child_guid2, bag[0]["childbagfiles"][1],
                         'Childbagfile 2 guid not equal in bagfile!')

    def test_child_bagfile_POST_copy_special_tag_from_bagfile(self):
        tag_to_test = 'tags'
        ch = ChildBagFilesHandler()
        data = deepcopy(raw_data_child_bagfile)
        raw_bag = deepcopy(raw_data_bagfile)
        del raw_bag['_id']
        del raw_bag['origin']
        raw_bag[tag_to_test] = ['test_tag', 'test_tag2']
        raw_bag['guid'] = generate_deterministic_guid()
        data["parent_guid"] = raw_bag['guid']
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_bag])
        child_guid = ch.add_child_bagfile_to_db(data)
        ch_bag = ch.get_child_bagfile_from_db(child_guid)
        self.assertCountEqual(
            raw_bag[tag_to_test], ch_bag[0][tag_to_test],
            'Special tag from bagfile not copied to childbag!')


class TestChildBagfileGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_child_bagfile_GET(self):
        data = deepcopy(raw_data_child_bagfile)
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([data])
        res = requests.get(url=child_bagfile_url +
                           raw_data_child_bagfile['guid'])
        self.assertDictEqual(raw_data_child_bagfile,
                             res.json()['results'][0],
                             'Child Bagfile get failed')

    def test_child_bagfile_GET_invalid(self):
        res = requests.get(url=child_bagfile_url +
                           '12341234-1234-1234-1234-123412341235')
        self.assertEqual(404, res.status_code, INVALID_GUID_NOT_DETECTED)


class TestChildBagfilesPATCH(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_child_bagfile_PATCH(self):
        data = deepcopy(raw_data_child_bagfile)
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([data])
        patch_data = deepcopy(raw_data_child_bagfile)
        del patch_data['guid']
        patch_data['version'] = "2.0"
        res = requests.patch(url=child_bagfile_url +
                             raw_data_child_bagfile['guid'],
                             json=patch_data)
        self.assertEqual(200, res.status_code, 'Child Bagfile patch failed')

    def test_child_bagfile_PATCH_when_unknown_guid_should_return_404(self):
        data = deepcopy(raw_data_child_bagfile)
        del data['guid']
        data['version'] = "2.0"
        res = requests.patch(url=child_bagfile_url +
                             '12341234-1234-1234-1234-123412341235',
                             json=data)
        self.assertEqual(404, res.status_code, INVALID_GUID_NOT_DETECTED)


class TestChildBagfileDELETE(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_child_bagfile_DELETE_should_delete(self):
        data = deepcopy(raw_data_child_bagfile)
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([data])

        res = requests.delete(url=child_bagfile_url +
                              raw_data_child_bagfile['guid'])
        self.assertEqual(200, res.status_code, 'Child Bagfile delete failed')

    def test_child_bagfile_DELETE_when_unknown_guid_should_return_404(self):
        res = requests.delete(url=child_bagfile_url +
                              '12341234-1234-1234-1234-123412341235')
        self.assertEqual(404, res.status_code, '%s' % INVALID_GUID_NOT_DETECTED)


class TestChildbagfilesTagsPATCH(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_childbagfile_tags_PATCH_should_append_to_existing_tags(self):
        bag = deepcopy(raw_data_child_bagfile)
        bag['tags'] = ['old_tag']
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([bag])

        res = requests.patch(url=tags_url_template.format(bag['guid']),
                             json=['tag1', 'tag2'])
        self.assertEqual(200, res.status_code)

        res = requests.get(url=tags_url_template.format(bag['guid']))
        self.assertEqual(['old_tag', 'tag1', 'tag2'], res.json()['results'])

    def test_childbagfile_tags_PATCH_when_invalid_request_should_return_400(self):
        bag = deepcopy(raw_data_child_bagfile)
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([bag])

        res = requests.patch(url=tags_url_template.format(bag['guid']),
                             json={'foo': 'bar'})
        self.assertEqual(400, res.status_code)

    def test_childbagfile_tags_PATCH_when_unknown_guid_should_return_404(self):
        res = requests.patch(url=tags_url_template.format('00000000-0000-0000-0000-000000000000'),
                             json={'foo': 'bar'})
        self.assertEqual(400, res.status_code)


class TestChildbagfilesTagsPUT(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_childbagfile_tags_PUT_should_replace_existing_tags(self):
        bag = deepcopy(raw_data_child_bagfile)
        bag['tags'] = ['old_tag']
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([bag])

        res = requests.put(url=tags_url_template.format(bag['guid']),
                             json=['tag1', 'tag2'])
        self.assertEqual(200, res.status_code)

        res = requests.get(url=tags_url_template.format(bag['guid']))
        self.assertEqual(['tag1', 'tag2'], res.json()['results'])

    def test_childbagfile_tags_PUT_when_invalid_request_should_return_400(self):
        bag = deepcopy(raw_data_child_bagfile)
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([bag])

        res = requests.put(url=tags_url_template.format(bag['guid']),
                           json={'foo': 'bar'})
        self.assertEqual(400, res.status_code)

    def test_childbagfile_tags_PUT_when_unknown_guid_should_return_404(self):
        res = requests.put(url=tags_url_template.format('00000000-0000-0000-0000-000000000000'),
                           json={'foo': 'bar'})
        self.assertEqual(400, res.status_code)


class TestChildbagfilesTagsGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_childbagfile_tags_GET_should_retrieve_tags(self):
        bag = deepcopy(raw_data_child_bagfile)
        bag['tags'] = ['old_tag']
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([bag])

        res = requests.get(url=tags_url_template.format(bag['guid']))
        self.assertEqual(200, res.status_code)

        res = requests.get(url=tags_url_template.format(bag['guid']))
        self.assertEqual(['old_tag'], res.json()['results'])

    def test_childbagfile_tags_GET_when_unknown_guid_should_return_404(self):
        res = requests.get(url=tags_url_template.format('00000000-0000-0000-0000-000000000000'))
        self.assertEqual(404, res.status_code)


class TestChildbagfilesTagsDELETE(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_childbagfile_tags_DELETE_should_replace_existing_tags(self):
        bag = deepcopy(raw_data_child_bagfile)
        bag['tags'] = ['tag1', 'tag2', 'tag3']
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([bag])

        res = requests.delete(url=tags_url_template.format(bag['guid']),
                              params={'value': ['tag1', 'tag2']})
        self.assertEqual(200, res.status_code)

        res = requests.get(url=tags_url_template.format(bag['guid']))
        self.assertEqual(['tag3'], res.json()['results'])

    @unittest.skip("test-case is broken")
    def test_childbagfile_tags_DELETE_when_non_existing_tag_should_not_delete(self):
        bag = deepcopy(raw_data_child_bagfile)
        bag['tags'] = ['tag1', 'tag2', 'tag3']
        connector = get_testdb_connector()
        with connector as con:
            con.add_child_bagfiles([bag])

        res = requests.delete(url=tags_url_template.format(bag['guid']),
                              params={'value': ['xxx']})
        self.assertEqual(200, res.status_code)

        res = requests.get(url=tags_url_template.format(bag['guid']))
        self.assertEqual(['tag1', 'tag2', 'tag3'], res.json()['results'])

    @unittest.skip("test-case is broken")
    def test_childbagfile_tags_DELETE_when_unknown_guid_should_return_404(self):
        res = requests.delete(url=tags_url_template.format('00000000-0000-0000-0000-000000000000'),
                              json={'foo': 'bar'})
        self.assertEqual(404, res.status_code)

class TestChildBagfilesCustomAttributesPOST(unittest.TestCase):
    def setUp(self):
        prepare_db()
        connector = get_testdb_connector()
        with connector as con:
            con.add_childbagfiles([raw_data_bagfile])
        self.guid = raw_data_bagfile['guid']

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_custom_attributes_PATCH(self):
        res = requests.patch(url=childbagfilecustom_attributes_url_template.format(self.guid),
                             json=custom_attributes)
        self.assertEqual(200, res.status_code, 'ChildBagfile custom attributes post failed')
        res = requests.get(url=Childbagfile_url + self.guid)
        self.assertEqual(200, res.status_code, 'ChildBagfile GET failed')
        data = raw_data_bagfile.copy()
        del data["sequences"]
        del data["childbagfiles"]
        data['custom_attributes'] = {"common_values": custom_attributes}
        self.assertDictEqual(data,
                             res.json()['results'][0],
                             'childBagfile custom attributes post failed')



if __name__ == '__main__':
    unittest.main()
