__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from pydantic import constr, conint

Guid = constr(regex=r"^([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$")
"""Str constraint to the guid pattern"""

UnixTimestampMilliseconds = conint(gt=946684800000, lt=4133980799000)
"""Int to represent a unix timestamp in milliseconds resolution. Values are
constraint to 01. Jan 2000 - 31. Dec 2100"""
