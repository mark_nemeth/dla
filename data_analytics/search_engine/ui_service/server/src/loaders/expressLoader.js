import express from 'express';
import cookieParser from 'cookie-parser';
import requestLogger from 'morgan';
import request from 'request-promise-native';
import {isObjectValid} from "../utils/helperFunctions";
import getLogger from '../utils/logging';

const logger = getLogger(__filename)

export default (app) => {

  // Logger
  // Incoming request
  app.use(requestLogger('tiny'));
  // Outgoing requests
  require('request-debug')(request, function(type, data) {
    if (!isObjectValid(type) || !isObjectValid(data)) return;
    if (data.hasOwnProperty('body') && data.body.length > 0) data.body = data.body.length;
    if (data.hasOwnProperty('debugId')) delete data.debugId;
    logger.info(`[OUTGOING] ${type.toUpperCase()} ${JSON.stringify(data)}`);
  });

  // Encoding
  app.use(express.json());
  app.use(express.urlencoded({extended: false}));
  app.use(cookieParser());
  logger.info('EXPRESS: enabled');

  return app;
}