import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByOperatorFilter} from "./ByOperatorFilter";
import ScrollList from "../../../Utils/ScrollList/ScrollList";

describe('ByOperatorFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      operatorList: ['Operator-1', 'Operator-2', 'Operator-3'],
      selectedOperatorList: ['Operator-2']
    };
    const wrapper = shallow(<ByOperatorFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should renders operator list with selected column', () => {
    const reduxAction = jest.fn();
    const props = {
      operatorList: ['Operator-1', 'Operator-2', 'Operator-3'],
      selectedOperatorList: ['Operator-2'],
      fetchAllOperators: reduxAction,
      filterByOperators: reduxAction
    };
    const wrapper = mount(<ByOperatorFilter {...props} />);
    const operatorList = wrapper.find(ScrollList);
    const entries = operatorList.find('tr');
    expect(entries.length).toBe(3);
    expect(entries.at(0).find('input').props().checked).toBe(false);
    expect(entries.at(1).find('input').props().checked).toBe(true);
    expect(entries.at(2).find('input').props().checked).toBe(false);
  });


  it('should call action when operator is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      operatorList: ['Operator-1', 'Operator-2', 'Operator-3'],
      selectedOperatorList: ['Operator-2'],
      fetchAllOperators: jest.fn(),
      filterByOperators: reduxAction
    };
    const wrapper = mount(<ByOperatorFilter {...props} />);
    const input = wrapper.find('tr').at(1).find('input');
    input.simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

});
