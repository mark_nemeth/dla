"""Handler class for child bagfiles"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List

from DANFUtils.utils import generate_bag_guid

from handlers.bagfiles import BagFilesHandler
from handlers.db_data_handler import DB_Data_Handler


class ChildBagFilesHandler:

    def __init__(self):
        self.db_data_handler = DB_Data_Handler()
        self.bagfile_handler = BagFilesHandler()

    def add_child_bagfile_to_db(self, child_bagfile: dict):
        """Creates a new childbagfile.

        :param child_bagfile: data of childbagfile to create
        """
        child_bagfile['guid'] = generate_bag_guid(child_bagfile['link'])

        # Attention: Hacky! In azure, currently childbagfiles are created
        # based on uploaded snippets without any parent bagfiles being present
        # in the azure db. Thus, certain functionality of this endpoint must
        # be deactivated. Thse cases are detected by the parent_guid being
        # all zeros...
        has_parent = child_bagfile['parent_guid'] != '00000000-0000-0000-0000-000000000000'

        if has_parent:
            inherited_props = ['vehicle_id_num', 'origin', 'tags']
            self._inherit_bagfile_properties(child_bagfile, inherited_props)

        self.db_data_handler.add_child_bagfiles([child_bagfile])

        if has_parent:
            self.bagfile_handler.update_field([child_bagfile['parent_guid']],
                                              [child_bagfile['guid']],
                                              "childbagfiles",
                                              append=True)
        return child_bagfile['guid']

    def get_child_bagfile_from_db(self, guid: str) -> dict:
        """Retrieves a single childbagfile by guid.

        :param guid: guid identifying bagfile to retrieve
        :return: childbagfile metadata as dict
        """
        return self.db_data_handler.get_child_bagfiles([guid])

    def update_child_bagfile_in_db(self, guid: str, data: dict):
        """Updates the childbagfile identified by the given guid.

        :param guid: guid of the childbagfile to update
        :param data: update to apply
        """
        self.db_data_handler.update_child_bagfiles([guid], [data])

    def delete_child_bagfile_from_db(self, guid: str, hard_delete: bool):
        """Deletes the childbagfile identified by the given guid.

        :param guid: guid of the childbagfile to delete
        """
        self.db_data_handler.delete_child_bagfiles([guid], hard_delete)

    def _inherit_bagfile_properties(self, child_bagfile, inherited_props):
        bag_file = self.bagfile_handler.get_bagfile_from_db(
            child_bagfile["parent_guid"])

        for key in inherited_props:
            if key in bag_file[0]:
                child_bagfile.update({key: bag_file[0][key]})

    def add_custom_attributes(self, guid, account_name, data):
        """Add the custome attributes to the childbagfile from the external extractor

        :param guid: childbagfile to update
        :param account_name: name of the extractor
        :param data: custom attributes to be added to the childbagfile
        :return: None
        """
        update_query = dict()
        for key, value in data.custom_attributes.items():
            update_query['custom_attributes.' + key] = value
        self.update_childbagfile_in_db(guid, update_query)

    def update_childbagfile_in_db(self, guid, data):
        self.db_data_handler.update_childbagfiles([guid], [data])
