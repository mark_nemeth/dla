import React from 'react';
import LabelEntry from './LabelEntry';
import { shallow } from 'enzyme';
import { defaultEmptyValue } from '../../Table';

describe('Test LabelEntry', () => {

    it('should show custom default value given an undefined value', () => {
        const customDefaultValue = 0;
        const props = { defaultValue: customDefaultValue, value: undefined };
        const wrapper = shallow(<LabelEntry {...props} />);
        expect(wrapper.exists()).toBe(true);
        expect(wrapper.find('span').props().children).toBe(customDefaultValue);
    });

    it('should show predefined default value given an undefined value and an undefined custom default value', () => {
        const props = { defaultValue: undefined, value: undefined };
        const wrapper = shallow(<LabelEntry {...props} />);
        expect(wrapper.exists()).toBe(true);
        expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
    });

    it('should show specified string value', () => {
        const value = 'testValue';
        const props = { defaultValue: '-', value: value };
        const wrapper = shallow(<LabelEntry {...props} />);
        expect(wrapper.exists()).toBe(true);
        expect(wrapper.find('span').props().children).toBe(value);
    });

    it('should show specified number value', () => {
        const value = 0;
        const props = { defaultValue: '-', value: value };
        const wrapper = shallow(<LabelEntry {...props} />);
        expect(wrapper.exists()).toBe(true);
        expect(wrapper.find('span').props().children).toBe(value);
    });

    it('should show specified boolean value', () => {
        const value = true;
        const props = { defaultValue: '-', value: value };
        const wrapper = shallow(<LabelEntry {...props} />);
        expect(wrapper.exists()).toBe(true);
        expect(wrapper.find('span').props().children).toBe(value);
    });

    it('should show default given a value of type object', () => {
        const value = {};
        const props = { value: value };
        const wrapper = shallow(<LabelEntry {...props} />);
        expect(wrapper.exists()).toBe(true);
        expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
    });

    it('should show default given a value of type list', () => {
        const value = [1, 2, 3, 4];
        const props = { value: value};
        const wrapper = shallow(<LabelEntry {...props} />);
        expect(wrapper.exists()).toBe(true);
        expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
    });

});
