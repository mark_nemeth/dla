import redis from 'redis';
import session from 'express-session';
import {promisify} from 'util';
import configHandlerSingleton from '../configHandler';
import {decryptStringData, encryptStringData} from '../utils/cyptoFunctions';
import getLogger from '../utils/logging';

const logger = getLogger(__filename)

const SESSION_PREFIX_KEY = 'ui-sess:';
const TOKEN_PREFIX_KEY = 'ui-token:';
const USER_PREFIX_KEY = 'ui-user:';

class RedisStore {

  constructor() {
    const redisConfig = configHandlerSingleton.getRedisConfig();
    this.redisClient = redis.createClient(redisConfig);
    this.asyncRedisGet = promisify(this.redisClient.get).bind(this.redisClient);
    // Subscribe to log redis events
    this.redisClient.on("ready", function () {
      logger.info(`Redis is connected to ${redisConfig.host}:${redisConfig.port} and is ready to use`);
    });
    this.redisClient.on("error", function (err) {
      logger.error('Redis error:');
      logger.error(err);
    });
  }

  getSessionStore() {
    logger.info('REDIS_SESSIONS_STORAGE: enabled');
    const client = this.redisClient;
    const RedisStore = require('connect-redis')(session);
    return new RedisStore({prefix: SESSION_PREFIX_KEY, client});
  }

  async getUserTokenDataAsync(userID) {
    const key = TOKEN_PREFIX_KEY + userID;
    const redisEntry = await this.asyncRedisGet(key);
    const decrypted = this._getDataDecrypted(redisEntry);
    logger.debug(`Loaded user token data from redis with key: ${key} value: ${decrypted}.`);
    return decrypted;
  }

  saveUserTokenData(userID, tokenData) {
    const encryptedData = this._encryptData(tokenData);
    this.redisClient.set(TOKEN_PREFIX_KEY + userID, encryptedData);
  }

  deleteUserTokenData(userID) {
    this.redisClient.del(TOKEN_PREFIX_KEY + userID);
  }

  async getUserDataAsync(userID) {
    const key = USER_PREFIX_KEY + userID;
    const res = await this.asyncRedisGet(key);

    if (!res) {
      logger.error(`No authentification data found for userID ${userID}.`);
      return null;
    }

    let userData; 
    try {
      userData = JSON.parse(res);
    } catch(error) {
      logger.error(`Failed to prase user information of userID ${userID}. Response Redis: ${res}. Error: ${JSON.stringify(error)}`);
      return null;
    }
    
    logger.debug(`Loaded user data: Key: ${key}, UserData: ${res}.`);
    return userData;
  }

  saveUserData(userID, profile) {
    const key = USER_PREFIX_KEY + userID;
    logger.info(`Saving profile ${profile} for userID ${userID} with key ${key}.`);
    const encryptedData = this._encryptData(profile);
    this.redisClient.set(key, encryptedData);
  }

  deleteUserData(userID) {
    const key= USER_PREFIX_KEY + userID;
    logger.debug(`Deleting user information. UserID: ${userID}, Key: ${key}`)
    this.redisClient.del(key);
  }

  _encryptData(data) {
    logger.debug(`Encrypt data: ${data}`)
    let encryptedData = null;
    if(configHandlerSingleton.isNodeEnvInProduction()){
      encryptedData =  encryptStringData(JSON.stringify(data), configHandlerSingleton.getRedisEncryptionKey());
    } else {
      logger.info("Non productive environment detected. Data is not encrypted.");
      encryptedData =  JSON.stringify(data);
    }
    logger.debug(`Return encrypted data: ${encryptedData}`);
    return encryptedData;
  }

  _getDataDecrypted(encryptData) {
    logger.debug(`Decrypt data: ${encryptData}`);
    let decryptedData = null;

    if(configHandlerSingleton.isNodeEnvInProduction()){
      decryptedData =  decryptStringData(encryptData, configHandlerSingleton.getRedisEncryptionKey());
    } else {
      logger.debug("Non productive environment detected. Data is not decrypted.");
      decryptedData =  JSON.stringify(encryptData);
    }
    logger.debug(`Return decrypted data: ${decryptedData}`);
    return decryptedData;
  }

  _getSingleRandomTokenAsync() {
    return promisify(redisStoreSingleton.redisClient.keys).bind(redisStoreSingleton.redisClient)('*')
      .then((keys) => {
        const userTokenKeys = keys.filter(key => key.includes(TOKEN_PREFIX_KEY));
        if (userTokenKeys.length <= 0) return Promise.reject('No User Tokens found in local redis DB');
        return Promise.resolve(userTokenKeys[0].replace(TOKEN_PREFIX_KEY, ''))
      })
  }

}

const redisStoreSingleton = new RedisStore();
export default redisStoreSingleton;