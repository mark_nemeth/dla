__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import (ENV_SPAN_ID, ENV_TRACE_ID,
                                       ENV_TRACE_OPTIONS)
from kfp import dsl
from kubernetes.client import V1VolumeMount
from kubernetes.client.models import V1EnvVar

from config.config_handler import config_dict
from kubeflow.steps.common import (VM_LABEL_ATTRIBUTE, RETRY_NUM,
                                   PIPELINE_POD_LABEL,
                                   BAGFILE_POD_LABEL, FILE_NAME_POD_LABEL,
                                   sanitize_label, METADATA_API_BASE_URL,
                                   APPINSIGHTS_KEY,
                                   DANF_ENV, AUTH, AUTH_TOKEN_URL,
                                   CPU_RESOURCE_TITLE,
                                   MEMORY_RESOURCE_TITLE, TESTING_POD_LABEL,
                                   extract_file_name_from_path,
                                   AUTH_METADATA_AUDIENCE,
                                   AUTH_METADATA_CLIENT_CRED_ID,
                                   AUTH_METADATA_CLIENT_CRED_SECRET,
                                   CUSTOM_NODE_RESOURCE_TITLE,
                                   CUSTOM_STORAGE_VM_TITLE,
                                   MOUNT_DATA_BASE_PATH,
                                   attach_blobfuse_volume,
                                   get_pvc_volume,
                                   AUTH_DATAHANDLER_AUDIENCE,
                                   AUTH_DATAHANDLER_CLIENT_CRED_ID,
                                   AUTH_DATAHANDLER_CLIENT_CRED_SECRET,
                                   AUTH_SEARCH_AUDIENCE,
                                   AUTH_SEARCH_CLIENT_CRED_ID,
                                   AUTH_SEARCH_CLIENT_CRED_SECRET, BaseStep)


class FeatureExtractorOp(BaseStep):
    NAME = "feature-extraction-library"
    DOCKER_IMG = config_dict.get_value(NAME)
    CPU_REQUEST = "500m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "2Gi"
    MEMORY_LIMIT = "6Gi"

    INPUT_MOUNT_BASE_PATH = "/data"
    OUTPUT_MOUNT_BASE_PATH = "/outputs"


    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               input_location,
               output_container,
               mount_id,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_file_path: path to the bagfile in the pod
        :param output_folder: path in pod to which output files
            should be written
        :param pvc_name: name of the pvc to mount
        :param vin: of the vehicle from which the processed bagfile originates
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the DanfExtractor operation
        """
        step = FeatureExtractorOp(
            flow_run_guid,
            bagfile_guid,
            input_location.file,
            vin='undefined'
        )

        if test_mode:
            step.enable_test_mode()

        step \
            .add_kubernetes_resources_for_pod(cls.CPU_REQUEST, cls.MEMORY_REQUEST, cls.CPU_LIMIT, cls.MEMORY_LIMIT) \
            .connect_metadata_api() \
            .enable_proxy() \
            .enable_tracing_integration() \
            .add_authentication_env_variables()

        step.add_env_variable(V1EnvVar(name="BAGFILE_PATH", value=f"{cls.INPUT_MOUNT_BASE_PATH}/{input_location.file}")) \
            .add_env_variable(V1EnvVar(name="OUTPUT_FOLDER", value=cls.OUTPUT_MOUNT_BASE_PATH))

        attach_blobfuse_volume(step,
                               cls.INPUT_MOUNT_BASE_PATH,
                               f"input-container-{mount_id}",
                               input_location.storage_account,
                               input_location.container)
        attach_blobfuse_volume(step,
                               cls.OUTPUT_MOUNT_BASE_PATH,
                               f"output-container-{mount_id}",
                               input_location.storage_account,
                               output_container,
                               False,
                               0,
                               mount_sub_path=input_location.container)
        return step
