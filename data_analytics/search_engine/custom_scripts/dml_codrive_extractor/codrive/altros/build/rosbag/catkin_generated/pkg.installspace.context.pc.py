# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/app/altros/install/include".split(';') if "/app/altros/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "rosbag_storage;rosconsole;roscpp;std_srvs;topic_tools;xmlrpcpp;message_runtime;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lrosbag".split(';') if "-lrosbag" != "" else []
PROJECT_NAME = "rosbag"
PROJECT_SPACE_DIR = "/app/altros/install"
PROJECT_VERSION = "1.14.3"
