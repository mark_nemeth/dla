# Scenario Identification

This program goes through a .bag-file and classifies specific scenarios

## Build docker

to build the docker use this command:

```

docker build --network host -t <image-name> .
```

an example:
```

docker build --network host -t final .
```


### Run the docker

to run the docker use this command:

```

docker run -v /folder/with/bagfile/and/model:/input -v /folder/to/put/output:/output -e BAG_PATH=input/bagfile.bag -e NN_MODEL=input/Neural_NET_model.h5 -e TAKEOUT=<n-th datasample should be looked at> <image-name>
```

an example:
```

docker run -v ${PWD}/input/:/input -v ${PWD}/output/:/output -e BAG_PATH=input/20190407_174319_SanJose_Sunnday_Dyn_World.bag -e NN_MODEL=input/Parking_Cars.h5 -e TAKEOUT=3 final
```

### Explanation

* /folder/with/bagfile/and/model this will be mapped into the input folder within the docker. One can also use two different folders and clarify it in the BAG_PATH and NN_MODEL variables.
* /folder/to/put/output json file with the classification will be put here. 
* BAG_PATH path and name of the bag_file
* NN_MODEL path and name of the Neural Network model (included are three Neural Networks that were trained on hand-labeled data for crossings, parking cars and car following)
* The TAKEOUT variable gives the option to classify only every n-th timestep. If every timestep should be classified the variable should be 1, for once every second it is 10.

