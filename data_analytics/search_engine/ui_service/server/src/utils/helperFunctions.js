/*
  Here you can define helper functions that can be used across all classes
 */

export function isStringValid(str) {
  return str !== undefined && str !== null && str.length > 0;
}

export function isNumberValid(num) {
  return num !== undefined && num !== null && !Number.isNaN(num);
}

export function isObjectValid(obj) {
  return obj !== undefined && obj !== null;
}

export function areNestedKeysPresent(obj, level,  ...rest){
  if (obj === undefined) return false;
  if (rest.length == 0 && obj.hasOwnProperty(level)) return true;
  return areNestedKeysPresent(obj[level], ...rest);
}