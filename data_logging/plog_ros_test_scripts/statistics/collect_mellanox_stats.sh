#!/bin/sh


if [ "clear" == $1 ]
then
	echo -e ""
	echo -e "\033[35m clear counters \033[0m"
	exp="before"
	sshpass -p admin ssh admin@192.168.253.1 cli \"enable\" \"configure terminal\" \"interface ethernet clear counters\"
else
	exp="after"
	echo -e "\033[31m port counters will not be cleared \033[0m"
fi

echo -e ""
echo -e "\033[35m collect port information \033[0m"
sshpass -p admin ssh admin@192.168.253.1 cli \"enable\" \"configure terminal\" \"show interfaces ethernet status\" > mellanox_port_status_all.log

echo -e ""
echo -e "\033[35m collect port numbers \033[0m"
ports=$(sshpass -p admin ssh admin@192.168.253.1 cli \"enable\" \"configure terminal\" \"show interfaces ethernet status\" | awk '{print $1}' | sed '4,20!d')

for port in $ports
do
	echo -e ""
	echo -e "\033[35m collect port counters -  ${port} \033[0m"
	portstr=$(echo $port | sed -e 's/\/ */_/g')
	portstr+="_$exp"
	sshpass -p admin ssh admin@192.168.253.1 cli \"enable\" \"configure terminal\" \"show interface ethernet ${port:3} counters\" > mellanox_port_counters_${portstr}.log
done







 
