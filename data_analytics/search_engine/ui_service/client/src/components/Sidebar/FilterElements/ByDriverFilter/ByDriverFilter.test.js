import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByDriverFilter} from "./ByDriverFilter";
import ScrollList from "../../../Utils/ScrollList/ScrollList";

describe('ByDriverFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      driverList: ['Driver-1', 'Driver-2', 'Driver-3'],
      selectedDriverList: ['Driver-2']
    };
    const wrapper = shallow(<ByDriverFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should renders driver list with selected column', () => {
    const reduxAction = jest.fn();
    const props = {
      driverList: ['Driver-1', 'Driver-2', 'Driver-3'],
      selectedDriverList: ['Driver-2'],
      fetchAllDrivers: reduxAction,
      filterByDrivers: reduxAction
    };
    const wrapper = mount(<ByDriverFilter {...props} />);
    const driverList = wrapper.find(ScrollList);
    const entries = driverList.find('tr');
    expect(entries.length).toBe(3);
    expect(entries.at(0).find('input').props().checked).toBe(false);
    expect(entries.at(1).find('input').props().checked).toBe(true);
    expect(entries.at(2).find('input').props().checked).toBe(false);
  });


  it('should call action when driver is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      driverList: ['Driver-1', 'Driver-2', 'Driver-3'],
      selectedDriverList: ['Driver-2'],
      fetchAllDrivers: jest.fn(),
      filterByDrivers: reduxAction
    };
    const wrapper = mount(<ByDriverFilter {...props} />);
    const input = wrapper.find('tr').at(1).find('input');
    input.simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

});
