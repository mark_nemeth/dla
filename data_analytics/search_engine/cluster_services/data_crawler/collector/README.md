# Data Crawler - Collector

The purpose of the collector container is to collect the outputs of the scanner containers and add the extracted data to the file catalog.
The extracted data is saved in a .csv file on mapr and is written by the first container (Scanner), which scans the clusters.

The collector defined by the Dockerfile is meant to be run on the Athena Kubernetes cluster in Vaihingen.

## Build Docker Image Locally

Initial configuration of access to the Azure DevOps python package repository
Create and copy a `Personal Access Tokens` in Azure DevOps UI with Packaging (read) privileges.
Export the index URL including the token: `export index_url_with_access=https://<YOUR_TOKEN>@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/`

#### Linux
```
docker build --build-arg https_proxy=http://localhost:3128 --build-arg http_proxy=http://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access --network="host" -t crawler_collector:latest .
```

#### Windows
```
docker build --build-arg https_proxy=http://docker.for.win.localhost:3128 --build-arg http_proxy=http://docker.for.win.localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access -t crawler_collector:latest .
```

## Push to Artifactory
A manual push is not required. A merge to integration will trigger a build of the collector and apply the kubernetes int cronjobs on the specified clusters and merge to production will trigger a build of the collector and apply the kubernetes prod cronjobs on the specified clusters.
Remember to bump the versions or a new build will not be triggered! 