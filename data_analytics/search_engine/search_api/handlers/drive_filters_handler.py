"""Handler class for filters"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from handlers.db_data_handler import DBDataHandler


class DriveFiltersHandler:

    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def get_filters(self):
        drive_duration_min_max = self.db_data_handler.get_drive_duration_min_max()
        vehicle_ids = self.db_data_handler.get_all_vins()
        driven_by_values = self.db_data_handler.get_all_driven_by_values()
        drivers = self.db_data_handler.get_all_drivers()
        operators = self.db_data_handler.get_all_operators()
        hardware_releases = self.db_data_handler.get_all_hardware_releases()
        filters = {
            "duration_min_max": drive_duration_min_max,
            "vehicle_ids": vehicle_ids,
            'driven_by_values': driven_by_values,
            'drivers': drivers,
            'operators': operators,
            'hardware_releases': hardware_releases
        }
        return filters
