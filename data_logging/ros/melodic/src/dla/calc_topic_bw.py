#! /usr/bin/env python

import rosbag
import yaml
import json


def calc_datarate_per_topic(path, bfile):
    """Calculate the datarate for each topic in the bagfile."""
    bagfile = path + bfile
    with rosbag.Bag(bagfile) as bag:
        info = yaml.load(bag._get_yaml_info())
        duration = info['duration']
        results = dict()
        total_payload = 0
        for tp in set(tpi['topic'] for tpi in info['topics']):
            results[tp] = {'Payload Size [Byte]': 0,
                           'Num. Messages': 0,
                           'Avg. Message Size [Byte]': 0,
                           'Avg. Message Datarate [Mbit/s]': 0}
        for topic, msg, t in bag.read_messages():
            results[topic]['Payload Size [Byte]'] += msg.__sizeof__()
            results[topic]['Num. Messages'] += 1
        for tp in results:
            results[tp]['Avg. Message Size [Byte]'] = calc_avg_msg_size(results[tp]['Num. Messages'],
                                                                        results[tp]['Payload Size [Byte]'])
            results[tp]['Avg. Message Datarate [Mbit/s]'] = calc_datarate(results[tp]['Payload Size [Byte]'], duration)
            total_payload += results[tp]['Payload Size [Byte]']
        print('Total payload in Bagfile: {} MByte'.format(total_payload/1e6))
        return results


def calc_avg_msg_size(num_msgs, size):
    avg_size = size/num_msgs
    return avg_size


def calc_datarate(size, duration):
    """Calculate datarate in Mbit/s"""
    datarate = size/duration*8/1e6
    return datarate


if __name__=="__main__":

    # Choose bag file from local directory
    path = u'/lhome/abrechf/workspace/'
    bagfiles = ['20180626_104413_following2_ORIGINAL_STRIPPED.bag']  # , '20180626_104413_following2_ORIGINAL.bag']
    for bfile in bagfiles:
        print('\nProcessing {} \n'.format(bfile))
        result = calc_datarate_per_topic(path, bfile)
        print(json.dumps(result, indent=3))
