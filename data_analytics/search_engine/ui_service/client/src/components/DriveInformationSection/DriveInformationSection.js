import React from 'react';
import Card from 'react-bootstrap/Card';
import NameEntry from '../AttributeFormatter/Name/NameEntry';
import LabelContainer from '../Utils/LabelContainer/LabelContainer';
import DateTimeEntry from '../AttributeFormatter/DateTime/DateTimeEntry';
import './DriveInformationSection.scss';
import DurationEntry from "../AttributeFormatter/Duration/DurationEntry";
import LabelEntry from "../AttributeFormatter/Label/LabelEntry";

const emptyPlaceholder = '-';

export function DriveInformationSection(props) {
  return (
      <Card className="file-information-section">
      <span className="file-information-section-label">
        Drive Information
      </span>
        <LabelContainer label="Description" variant="card-style-filled">
          <LabelEntry value={props.drive.description}/>
        </LabelContainer>
        <LabelContainer label="File Path" variant="card-style-filled">
          <NameEntry guid={props.drive.guid} fileName={props.drive.file_path} withoutLink disableTooltip/>
        </LabelContainer>
        <div className="information-row">
          <LabelContainer label="Start time" variant="seventy-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.drive.start}/>
          </LabelContainer>
          <LabelContainer label="Duration" variant="thirty-percent-card card-style-filled">
            <DurationEntry duration={props.drive.duration}/>
          </LabelContainer>
        </div>
        <div className="information-row">
          <LabelContainer label="End time" variant="seventy-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.drive.end}/>
          </LabelContainer>
        </div>
        <div className="information-row">
          <LabelContainer label="VID" variant="fifty-percent-card card-style-filled">
            <span>{props.drive.vehicle_id_num || emptyPlaceholder}</span>
          </LabelContainer>
        </div>
        <div className="information-row">
          <LabelContainer label="Operator" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.drive.operator}/>
          </LabelContainer>
          <LabelContainer label="Driver" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.drive.driver}/>
          </LabelContainer>
        </div>
        <div className="information-row">
          <LabelContainer label="Blocker driver" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.drive.blocker_driver}/>
          </LabelContainer>
          <LabelContainer label="Driven by" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.drive.driven_by}/>
          </LabelContainer>
        </div>
        <LabelContainer label="Drive impression" variant="card-style-filled">
          <LabelEntry value={props.drive.drive_impression}/>
        </LabelContainer>
        <div className="information-row">
          <LabelContainer label="Sensor set version" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.drive.sensor_set_version}/>
          </LabelContainer>
          <LabelContainer label="Hardware Release" variant="card-style-filled">
            <LabelEntry value={props.drive.hardware_release}/>
          </LabelContainer>
        </div>
      </Card>
  )
}