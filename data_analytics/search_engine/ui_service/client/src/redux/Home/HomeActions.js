import {createAction} from '@reduxjs/toolkit';
import configHandler from '../../config/configHandler';
import axios from 'axios';
import {FileTypes} from '../../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import {isObjectValid} from '../../utils/helperFunctions';
import {insertString} from '../../assets/string/stringHelper';
import strings from '../../assets/string/default';

//region Load bagfiles Actions
export const fetchBagfilesSuccess = createAction('bagfiles/fetchBagfilesSuccess');
export const fetchBagfilesError = createAction('bagfiles/fetchBagfilesError');
export const fetchBagfilesEmpty = createAction('bagfiles/fetchBagfilesEmpty');
//endregion

//region Load Original files Actions
export const fetchOriginalFilesSuccess = createAction('bagfiles/fetchOriginalFilesSuccess');
export const fetchOriginalFilesError = createAction('bagfiles/fetchOriginalFilesError');
export const fetchOriginalFilesEmpty = createAction('bagfiles/fetchOriginalFilesEmpty');
//endregion

//region Load Alwayson files Actions
export const fetchAlwaysonFilesSuccess = createAction('bagfiles/fetchAlwaysonFilesSuccess');
export const fetchAlwaysonFilesError = createAction('bagfiles/fetchAlwaysonFilesError');
export const fetchAlwaysonFilesEmpty = createAction('bagfiles/fetchAlwaysonFilesEmpty');
//endregion

//region Load Child bagfiles Actions
export const fetchChildBagfilesSuccess = createAction('bagfiles/fetchChildBagfilesSuccess');
export const fetchChildBagfilesError = createAction('bagfiles/fetchChildBagfilesError');
export const fetchChildBagfilesEmpty = createAction('bagfiles/fetchChildBagfilesEmpty');
//endregion

//region Load drive files Actions
export const fetchDrivesSuccess = createAction('drives/fetchDrivesSuccess');
export const fetchDrivesError = createAction('drives/fetchDrivesError');
export const fetchDrivesEmpty = createAction('drives/fetchDrivesEmpty');
//endregion


//region Bulk Actions
export const selectTableEntry = createAction('table/selectTableEntry');
export const deleteSelectedBagfiles = createAction('action/deleteSelectedBagfiles');
export const downloadSelectedBagfiles = createAction('action/downloadSelectedBagfiles');
//endregion


//region Filter Actions
export const showLoading = createAction('filter/showLoading');
export const resetFilters = createAction('filter/resetFilters');
export const setAppliedFilters = createAction('filter/applyFilters');

export function applyFilters() {
  return dispatch => {
    dispatch(setAppliedFilters());
    dispatch(refresh());
  };
}

export function refresh() {
  return (dispatch, getState) => {
    dispatch(showLoading());
    switch (getState().fileType) {
      case FileTypes.ORIGINALFILE:
        return loadOriginalFiles(dispatch, getState().appliedFilters);
      case FileTypes.ALWAYSONFILE:
        return loadAlwaysonFiles(dispatch, getState().appliedFilters);
      case FileTypes.CHILDBAGFILE:
        return loadChildBagfiles(dispatch, getState().appliedFilters);
      case FileTypes.DRIVE:
        return loadDrives(dispatch, getState().appliedFilters);
      default:
        return loadOriginalFiles(dispatch, getState().appliedFilters);
    }
  };

  // Load Original files
  function loadOriginalFiles(dispatch, currentFilters) {
    return axios.request(configHandler.getBagfileAPI(currentFilters))
        .then(res => {
          if (res.data.results.bagfiles.length > 0 && res.data.results.total) {
            dispatch(fetchOriginalFilesSuccess({bagfiles: res.data.results.bagfiles, total: res.data.results.total}));
          } else {
            const searchQueryValue = ('file_name' in currentFilters && currentFilters.file_name !== undefined)
                ? ' "' + currentFilters.file_name + '" or' : '';
            dispatch(fetchOriginalFilesEmpty(insertString(strings.no_results_found, searchQueryValue)));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchOriginalFilesError(insertString(strings.connection_error)));
        });
  }

  // Load drive files
  function loadDrives(dispatch, currentFilters) {
    return axios.request(configHandler.getDriveAPI(currentFilters))
        .then(res => {
          if (res.data.results.drives.length > 0 && res.data.results.total) {
            dispatch(fetchDrivesSuccess({drives: res.data.results.drives, total: res.data.results.total}));
          } else {
            const searchQueryValue = ('description' in currentFilters && currentFilters.description !== undefined)
                ? ' "' + currentFilters.description + '" or' : '';
            dispatch(fetchDrivesEmpty(insertString(strings.no_results_found, searchQueryValue)));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchDrivesError(insertString(strings.connection_error)));
        });
  }

  // Load Alwayson files
  function loadAlwaysonFiles(dispatch, currentFilters) {
    return axios.request(configHandler.getBagfileAPI(currentFilters))
        .then(res => {
          if (res.data.results.bagfiles.length > 0 && res.data.results.total) {
            dispatch(fetchAlwaysonFilesSuccess({bagfiles: res.data.results.bagfiles, total: res.data.results.total}));
          } else {
            const searchQueryValue = ('file_name' in currentFilters && currentFilters.file_name !== undefined)
                ? ' "' + currentFilters.file_name + '" or' : '';
            dispatch(fetchAlwaysonFilesEmpty(insertString(strings.no_results_found, searchQueryValue)));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchAlwaysonFilesError(insertString(strings.connection_error)));
        });
  }

// Load Childbagfiles
  function loadChildBagfiles(dispatch, currentFilters) {
    return axios.request(configHandler.getChildBagfileAPI(currentFilters))
        .then(res => {
          if (res.data.results.childBagfiles.length > 0 && res.data.results.total) {
            dispatch(fetchChildBagfilesSuccess({
              childBagfiles: res.data.results.childBagfiles,
              total: res.data.results.total
            }));
          } else {
            const searchQueryValue = ('file_name' in currentFilters && currentFilters.file_name !== undefined)
                ? ' "' + currentFilters.file_name + '" or' : '';
            dispatch(fetchChildBagfilesEmpty(insertString(strings.no_results_found, searchQueryValue)));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchChildBagfilesError(insertString(strings.connection_error)));
        });
  }
}


export const exportResultsWarning = createAction('results/exportWarning');
export const exportResultsError = createAction('results/exportError');

export function exportResults() {
  const EXPORT_LIMIT = 100;

  return (dispatch, getState) => {
    const fileType = getState().fileType;
    const query = {limit: EXPORT_LIMIT, offset: 0};

    Object.assign(query, getState().appliedFilters);
    downloadResults(fileType, query)
        .then(res => {
          let total = res.data.results.total;
          let results;
          switch (fileType) {
            case FileTypes.CHILDBAGFILE:
              results = res.data.results.childBagfiles;
              break;
            case FileTypes.ALWAYSONFILE:
              results = res.data.results.bagfiles;
              break;
            case FileTypes.ORIGINALFILE:
              results = res.data.results.bagfiles;
              break;
            case FileTypes.DRIVE:
              results = res.data.results.drives;
              break;
            default:
              dispatch(exportResultsError(insertString(strings.export_for_file_type_not_supported_error)));
              return
          }
          saveResults(results);
          if (results.length < total) {
            dispatch(exportResultsWarning(insertString(strings.export_limit_reached_warning, EXPORT_LIMIT)))
          }
        })
        .catch(err => {
          console.log(err)
        });
  };

  async function downloadResults(fileType, query) {
    switch (fileType) {
      case FileTypes.ORIGINALFILE:
        return axios.request(configHandler.getBagfileAPI(query));
      case FileTypes.ALWAYSONFILE:
        return axios.request(configHandler.getBagfileAPI(query));
      case FileTypes.CHILDBAGFILE:
        return axios.request(configHandler.getChildBagfileAPI(query));
      case FileTypes.DRIVE:
        return axios.request(configHandler.getDriveAPI(query));
      default:
        return axios.request(configHandler.getBagfileAPI(query));
    }
  }

  function saveResults(results) {
    const json = JSON.stringify(results, null, 2);
    const blob = new Blob([json], {type: 'application/json'});
    const href = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = href;
    link.download = "SearchResults.json";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

}

// SortBy
export const setSortBy = createAction('filter/sortBy');

export function sortBy(query) {
  return dispatch => {
    dispatch(setSortBy(query));
    dispatch(refresh())
  }
}

// Type
export const setFilterByFileType = createAction('filter/byFileType');

export function filterByFileType(query) {
  return dispatch => {
    dispatch(setFilterByFileType(query));
    dispatch(refresh())
  }
}

// Pagination
export const setPaginationLimit = createAction('filter/setPaginationLimit');
export const setPaginationOffset = createAction('filter/setPaginationOffset');

export function filterWithPagination(offsetLimit) {
  return dispatch => {
    dispatch(refresh())
  }
}

// SearchQuery
export const setCurrentSearchQuery = createAction('filter/setCurrentSearchQuery');
export const setFilterBySearchQuery = createAction('filter/bySearchQuery');

export function filterBySearchQuery(query) {
  return dispatch => {
    dispatch(setFilterBySearchQuery(query));
    dispatch(refresh())
  }
}

// VID
export const filterByVIDs = createAction('filter/byVID');
// Date
export const filterByDate = createAction('filter/byDate');
// duration
export const setCurrentDurationRange = createAction('filter/setCurrentDurationRange');
export const filterByDuration = createAction('filter/byDuration');
// Size
export const setCurrentSizeRange = createAction('filter/setCurrentSizeRange');
export const filterBySize = createAction('filter/bySize');
// Topics
export const filterByTopics = createAction('filter/byTopics');
// Status type
export const filterByStatus = createAction('filter/byStatus');
// Events
export const filterByEvents = createAction('filter/byEvents');
// Bagfile type
export const filterByBagfileType = createAction('filter/byBagfileType');
// Drive Type
export const filterByDriveType = createAction('filter/byDrivetype');
// velocity
export const setCurrentVelocityRange = createAction('filter/setCurrentVelocityRange');
export const filterByVelocity = createAction('filter/byVelocity');
// acceleration
export const setCurrentAccelerationRange = createAction('filter/setCurrentAccelerationRange');
export const filterByAcceleration = createAction('filter/byAcceleration');
// jerk
export const setCurrentJerkRange = createAction('filter/setCurrentJerkRange');
export const filterByJerk = createAction('filter/byJerk');
// engagement state
export const filterByEngagementState = createAction('filter/byEngagementState');
// laneChange
export const filterByLaneChange = createAction('filter/byLaneChange');
// driven by values
export const filterByDrivenByValues = createAction('filter/byDrivenByValues');
// drivers
export const filterByDrivers = createAction('filter/byDrivers');
// operators
export const filterByOperators = createAction('filter/byOperators');
// hardware releases
export const filterByHardwareReleases = createAction('filter/byHardwareReleases');
//endregion


// Load values for filters vehicle, duration, size and topics
export function fetchBagfileFilters() {
  return dispatch => {
    return axios.request(configHandler.getBagfileFiltersAPI())
        .then(res => {
          if (isObjectValid(res.data.results)) {
            dispatch(fetchBagfileFiltersSuccess(res.data.results));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchBagfileFiltersError());
        });
  }
}

export function fetchOriginalFileFilters() {
  return dispatch => {
    return axios.request(configHandler.getOriginalFileFiltersAPI())
        .then(res => {
          if (isObjectValid(res.data.results)) {
            dispatch(fetchOriginalFileFiltersSuccess(res.data.results));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchOriginalFileFiltersError());
        });
  }
}

export function fetchDriveFilters() {
  return dispatch => {
    return axios.request(configHandler.getDriveFiltersAPI())
        .then(res => {
          if (isObjectValid(res.data.results)) {
            dispatch(fetchDriveFiltersSuccess(res.data.results));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchDriveFiltersError());
        });
  }
}

export function fetchAlwaysonFileFilters() {
  return dispatch => {
    return axios.request(configHandler.getAlwaysonFileFiltersAPI())
        .then(res => {
          if (isObjectValid(res.data.results)) {
            dispatch(fetchAlwaysonFileFiltersSuccess(res.data.results));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchAlwaysonFileFiltersError());
        });
  }
}

export function fetchChildBagfileFilters() {
  return dispatch => {
    return axios.request(configHandler.getChildBagfileFiltersAPI())
        .then(res => {
          if (isObjectValid(res.data.results)) {
            dispatch(fetchChildBagfileFiltersSuccess(res.data.results));
          }
        })
        .catch(err => {
          handleError(dispatch, err, fetchChildBagfileFiltersError());
        });
  }
}

export const fetchBagfileFiltersSuccess = createAction('filter/fetchBagfileFiltersSuccess');
export const fetchBagfileFiltersError = createAction('filter/fetchBagfileFiltersError');

export const fetchOriginalFileFiltersSuccess = createAction('filter/fetchOriginalFileFiltersSuccess');
export const fetchOriginalFileFiltersError = createAction('filter/fetchOriginalFileFiltersError');

export const fetchAlwaysonFileFiltersSuccess = createAction('filter/fetchAlwaysonFileFiltersSuccess');
export const fetchAlwaysonFileFiltersError = createAction('filter/fetchAlwaysonFileFiltersError');

export const fetchChildBagfileFiltersSuccess = createAction('filter/fetchChildBagfileFiltersSuccess');
export const fetchChildBagfileFiltersError = createAction('filter/fetchChildBagfileFiltersError');

export const fetchDriveFiltersSuccess = createAction('filter/fetchDriveFiltersSuccess');
export const fetchDriveFiltersError = createAction('filter/fetchDriveFiltersError');

// Load Topics list Actions
export const setCurrentTopicSearchQuery = createAction('topics/setCurrentTopicSearchQuery');

//region Login
export function showLoginPage() {
  window.location.reload();
}

//endregion

//endregion

//region DataHandler bagfile Actions
export const reprocessingBagfileSuccess = createAction('dataHandler/reprocessingBagfileSuccess');
export const reprocessingBagfileError = createAction('dataHandler/reprocessingBagfileError');

export function reprocessBagfile(bagfilePATH) {
  return dispatch => {
    return axios.request(configHandler.getReprocessAPI(bagfilePATH))
        .then(res => {
          console.log(res);
          if (res.data.results.results.length <= 0) {
            dispatch(reprocessingBagfileError(insertString(strings.reprocessing_error)));
            return;
          }
          for (let file of res.data.results.results) {
            if (file.hasOwnProperty('result') && file.result !== 'SUCCESSFUL') {
              dispatch(reprocessingBagfileError(insertString(strings.reprocessing_unsuccessful)));
              return;
            }
          }
          dispatch(reprocessingBagfileSuccess(insertString(strings.reprocessing_successful)));
        })
        .catch(err => {
          if (err.response.status === 403) {
            dispatch(reprocessingBagfileError(insertString(strings.reprocessing_denied)));
          } else {
            handleError(dispatch, err, reprocessingBagfileError(insertString(strings.connection_error)));
          }
        });
  }
}

//endregion

function handleError(dispatch, err, errorTask) {
  console.log(err);
  if (err.response && err.response.status === 401) dispatch(showLoginPage());
  else dispatch(errorTask);
}