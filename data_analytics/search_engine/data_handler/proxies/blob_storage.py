"""Interface to azure storage account blob storage"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from azure.storage.blob.baseblobservice import BaseBlobService

from handlers.utils import utils

logger = logging.getLogger(__name__)


class BlobStorageHandler:
    """The storage handler takes care of storage layer persistence
    return output container
    """

    def create_output_containers(
            self, input_storage_account, output_storage_account,
            container_name, file_name, bagfile_guid):
        logger.info(f"creating output containers for file '{file_name}' in container '{container_name}' in storage account '{input_storage_account}'")
        output_container_name = utils.build_target_container_name(input_storage_account, container_name, file_name)
        output_container_metadata = self._create_container_metadata(input_storage_account, container_name, file_name, bagfile_guid)
        filtered_files_container = "{}-filtered".format(output_container_name)
        exported_files_container = "{}-exported".format(output_container_name)
        self._ensure_container_exists(output_storage_account, filtered_files_container, output_container_metadata)
        self._ensure_container_exists(output_storage_account, exported_files_container, output_container_metadata)
        return {
            "filtered_files_container": filtered_files_container,
            "exported_files_container": exported_files_container
        }

    def _ensure_container_exists(self, storage_account, container_name, metadata=None):
        """Ensures that the container exists in the given storage account by creating it
        if it does not exist. If the container already exists, no action is taken.
        """
        output_blob_service = self._get_blob_service_for_storage_account(storage_account)
        exists = output_blob_service.exists(container_name=container_name)
        if not exists:
            logger.info(f"output container '{container_name}': does not exist, creating new container")
            output_blob_service.create_container(container_name, metadata=metadata)
        else:
            logger.info(f"output container '{container_name}': already exists")

    def is_blob_available(self, blob_path):
        storage_account, container_name, blob_name = blob_path.strip('/').split('/')
        blob_service = self._get_blob_service_for_storage_account(storage_account)
        exists = blob_service.exists(container_name=container_name, blob_name=blob_name)
        logger.info(f"Existence of blob in the path {blob_path} is {exists}")
        return exists

    @staticmethod
    def _get_blob_service_for_storage_account(storage_account):
        """Create a BlobService object for the given storage account using the access keys found in the
        configuration. If no access key is found for the storage account, an exception is raised.

        :raises Exception: if no access key is found in the configuration for the storage account
        """
        sastoken = utils.get_storage_account_access_key(storage_account)
        if not sastoken:
            raise Exception(f"no access sastoken found in configuration for storage account f{storage_account}")
        blob_service = BaseBlobService(account_name=storage_account,
                                       sas_token=sastoken)
        return blob_service

    @staticmethod
    def _create_container_metadata(storage_account, container_name, file_name, bagfile_guid):
        return {
            'source_storage_account': storage_account,
            'source_container': container_name,
            'source_file': file_name,
            'source_bagfile_guid': bagfile_guid
        }
