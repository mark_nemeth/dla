"""Regressiontests for snippet bva route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests

from .testutils import get_testdb_connector

base_url = 'http://localhost:8889'
gps_search_childbag_url = base_url + '/search/childbagfile'

child_1 = {
    "version": "1.0",
    "guid": "0b09230e-7af5-4898-ba3f-c2f55d5ffeb6",
    "parent_guid": "1807b8ab-3145-465d-8c1a-7d7bf10563b7",
    "start": 1565876127000,
    "end": 1565876136000,
    "parent_index_start": 2,
    "parent_index_end": 10,
    "bva_max": 10,
    "link": "test_data",
    "gps_track": {
        "type": "LineString",
        "coordinates": [[2, 1], [3, 2], [4, 2], [5, 4], [6, 1]]
    }
}

child_2 = {
    "version": "1.0",
    "guid": "0b09230e-7af5-4898-ba3f-c2f55d5ffeb7",
    "parent_guid": "1807b8ab-3145-465d-8c1a-7d7bf10563b7",
    "start": 1565876127000,
    "end": 1565876136000,
    "parent_index_start": 2,
    "parent_index_end": 10,
    "bva_max": 10,
    "link": "test_data",
    "gps_track": {
        "type": "LineString",
        "coordinates": [[1, 2], [2, 5], [4, 1], [5, 6]]
    }
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['child_bagfiles'].delete_many({'link': 'test_data'})
        con.add_child_bagfiles([child_1, child_2])


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['child_bagfiles'].delete_many({'link': 'test_data'})


class TestGpsSearchChildBagfilesGET(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_gps_get(self):
        query_data = {
            'file_path': 'test_data',
            'gps_polygon': '[[5,0], [5,3], [6,3], [6,0], [5,0]]'
        }
        res = requests.get(url=gps_search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search by gps failed for get')
        self.assertEqual([child_1], res_json,
                         'child bagfile search by gps failed for get')

    def test_search_by_gps_post(self):
        query_data = {
            'file_path': 'test_data',
            'gps_polygon': [[0, 0], [0, 3], [2, 3], [2, 0], [0, 0]]
        }
        res = requests.post(url=gps_search_childbag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search by gps failed for post ')
        self.assertEqual([child_2], res_json,
                         'child bagfile search by gps failed for post')
