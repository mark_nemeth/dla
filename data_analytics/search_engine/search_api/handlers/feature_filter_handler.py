"""Handler class for feature filtering"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging
from typing import List

from handlers.db_data_handler import DBDataHandler
import re
logger = logging.getLogger(__name__)


class FeatureFilterHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def get_filter_arguments(self, query_args: dict) -> (str, List[str]):
        """
        Parse bagfile_guid and feature names from query_args
        """
        bagfile_guid = ""
        features = []
        for key, value in query_args.items():
            if key == 'bagfile_guid':
                bagfile_guid = value[0]
            elif key == "features":
                features = value[0].split(',')
        return bagfile_guid, features

    def perform_filtering(self, bagfile_guid: str,
                          features: List[str]) -> dict:
        """
        For the given bagfile_guid retrieve all values of the given features and their timestamps
        """
        query = {"bagfile_guid": bagfile_guid}
        projection = {f"features.{f}": 1 for f in features}
        projection["features.ts"] = 1
        results, total = self.db_data_handler.search_feature_buckets(
            query=query, projection=projection)
        res = {}
        if len(results) > 0:
            for bucket in results:
                for sec in bucket["features"]:
                    for k, v in sec.items():
                        if k in res:
                            res[k].append(v)
                        else:
                            res[k] = []
        res["bagfile_guid"] = bagfile_guid

        return res
