import React from 'react';
import Modal from 'react-bootstrap/Modal';
import './ModalBox.scss';

export function ModalBox(props) {
  return (
    <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered scrollable>
      <div onClick={props.onHide}>
        <div className="modal-icon-close icon-close"/>
      </div>
      <Modal.Header>
        <Modal.Title className="modal-title" id="contained-modal-title-vcenter">{props.header}</Modal.Title>
      </Modal.Header>
      <Modal.Body>{props.body}</Modal.Body>
    </Modal>
  );
}

export default ModalBox;