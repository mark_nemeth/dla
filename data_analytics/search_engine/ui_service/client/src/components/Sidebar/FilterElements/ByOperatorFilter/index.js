import {connect} from 'react-redux';
import {filterByOperators} from '../../../../redux/Home/HomeActions';
import {ByOperatorFilter} from './ByOperatorFilter';

function mapStateToProps(state, ownProps) {
  let operatorList = state.filterOptions.operatorList || [];
  const selectedOperatorList = state.filters.operator || [];

  return {selectedOperatorList, operatorList};
}

export default connect(
  mapStateToProps,
  {filterByOperators}
)(ByOperatorFilter);