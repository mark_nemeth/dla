#!/bin/sh
# This script dumps the network statistics from ethtool to a file.
#The statiscics should be dumped at the end and at the beginning of
#the test. For calculation of the difference there is the script
#collect_overall_stats.py(sh)
#in this version the oubtput from ethtool and from ifconfig is saved for
#experimental reasons

INTERFACE=$2
FILENAMEETH="nic_stat_ethtool_${INTERFACE}_$1.log"
FILENAMEIFC="nic_stat_ifconfig_${INTERFACE}_$1.log"


#first write the files
ethtool -S $INTERFACE >> $FILENAMEETH
ifconfig $INTERFACE >> $FILENAMEIFC
