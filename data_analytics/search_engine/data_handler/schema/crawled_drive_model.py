import json
from typing import List, Optional

from pydantic import BaseModel as PydanticBaseModel, Field, UUID4


class BaseModel(PydanticBaseModel):
    class Config:
        @staticmethod
        def schema_extra(schema, model):
            for prop, value in schema.get('properties', {}).items():
                # retrieve right field from alias or name
                field = [x for x in model.__fields__.values() if x.alias == prop][0]
                if field.allow_none:
                    # only one type e.g. {'type': 'integer'}
                    if 'type' in value:
                        value['anyOf'] = [{'type': value.pop('type')}]
                    value['anyOf'].append({'type': 'null'})
                if 'items' in value:
                    if field.sub_fields[0].allow_none:
                        if 'type' in value['items']:
                            value['items']['anyOf'] = [{'type': value['items'].pop('type')}]
                        value['items']['anyOf'].append({'type': 'null'})


class Annotation(BaseModel):
    guid: UUID4 = Field(..., example="d1b40d34-5b78-46a2-adc7-a6c20dd3e152")
    timestamp: float = Field(..., example=1590503052209.022, ge=946684800000)
    type: Optional[str] = Field(None, example="MANUAL")
    description: Optional[str] = Field(None, example="Annotation 1")
    severity: int
    screenshots: List[str] = []
    title: Optional[str] = Field(None, example="Sudden break, ghost object")


class AutonomousDrive(BaseModel):
    guid: UUID4 = Field(..., example="81004bf6-bd66-4025-9a99-f72c82f2f7a8")
    engagement: float = Field(..., example=1590503052209.065, ge=946684800000)
    disengagement: float = Field(..., example=1590503052209.067, ge=946684800000)
    disengagement_type: Optional[str] = Field(None, example="dropout")


class Mission(BaseModel):
    guid: UUID4 = Field(..., example="ce004b34-e029-457a-93a1-11fd5b1f54b2")
    mission_id: int = Field(..., example=1)
    mission_name: Optional[str] = Field(None, example="Mission 1")
    start: float = Field(..., example=1590503052209.081, ge=946684800000)


class AlwaysOnRecording(BaseModel):
    guid: UUID4 = Field(..., example="21db7eda-5aed-4c87-85a7-d1bc82802b90")
    start: float = Field(..., example=1590503052209.05, ge=946684800000)
    end: float = Field(..., example=1590503052209.267, ge=946684800000)
    recorder_version: Optional[str] = Field(None, example="RC123.1")
    recorder_path: Optional[str] = Field(None, example="/athena/path/RC123.1")
    files: List[str] = Field([], example=["always_on.bag"])
    bagfile_guids: List[Optional[UUID4]] = Field([], example=[None, "51ec46fa-9e13-47f4-a627-d4b6d6d49cf6"])
    topics: List[str] = Field([], example=["/always_on_topic1", "/always_on_topic2", "/always_on_topic3"])


class OnDemandRecording(BaseModel):
    guid: UUID4 = Field(..., example="06a803d7-2f6d-4ebc-997b-82696b49d6fb")
    start: float = Field(..., example=1590503052209.116, ge=946684800000)
    end: float = Field(..., example=1590503052209.241, ge=946684800000)
    file_name_postfix: Optional[str] = Field(None, example="CID_rec_1")
    name: Optional[str] = Field(None, example="CID rec 1")
    configuration: Optional[str] = Field(None, example="default_whitelist")
    recorder_version: Optional[str] = Field(None, example="RC123.1")
    recorder_path: Optional[str] = Field(None, example="/athena/path/RC123.1")
    files: List[str] = Field([], example=["on_demand_0.bag", "on_demand_1.bag"])
    bagfile_guids: List[Optional[UUID4]] = Field([], example=[None, "5c853448-2ac0-4e8d-8bfb-09e00cc55ada"])
    topics: List[str] = Field([], example=["/on_demand_topic1", "/on_demand_topic2", "/on_demand_topic3"])


class Component(BaseModel):
    name: str = Field(..., example="SVS")
    version: str = Field(..., example="RC_123.4")


class SystemRun(BaseModel):
    guid: UUID4 = Field(..., example="7442afe7-2b84-4639-a90e-d51e8c8a3b4c")
    start: float = Field(..., example=1590503052209.042, ge=946684800000)
    end: float = Field(..., example=1590503052209.264, ge=946684800000)
    global_software_version: Optional[str] = Field(None, example="RC_123.4")
    test_drive_type: Optional[str] = Field(None, example="CID")
    components: List[Component] = []
    always_on_recording: AlwaysOnRecording
    autonomous_drives: List[AutonomousDrive] = []
    missions: List[Mission] = []
    on_demand_recordings: List[OnDemandRecording] = []


class Versions(BaseModel):
    rsm_version: str = Field(..., example="0.1.0")
    rsm_rest_api_version: str = Field(..., example="0.1.0")
    rsm_drive_json_version: str = Field(..., example="0.1.0")


class Drive(BaseModel):
    guid: UUID4 = Field(..., example="7b97df67-5165-476c-9259-01d63295d8a2")
    start: float = Field(..., example=1590503052208.98, ge=946684800000)
    end: float = Field(..., example=15905030522092.71, ge=946684800000)
    duration: float = Field(..., example=15905030522092.71 - 1590503052208.98)
    drive_types: List[str] = []
    hardware_release: Optional[str] = Field(None, example="hw_release_1")
    sensor_set_version: Optional[str] = Field(None, example="SSU2.8")
    driven_by: Optional[str] = Field(None, example="SIT-DE")
    description: str = Field(None, example="some description")
    driver: Optional[str] = Field(None, example="user1")
    operator: Optional[str] = Field(None, example="user2")
    blocker_driver: Optional[str] = Field(None, example="user3")
    drive_impression: Optional[str] = Field(None, example="Everything fine")
    vehicle_id_num: str = Field(..., example="v222-5252")
    vehicle_type: Optional[str] = Field(None, example="v222")
    vin: Optional[str] = Field(None, example="WDD2221591A417315")
    file_path: Optional[str] = Field(None)
    data_model_version: str = Field(..., example="1")
    bagfiles_list: List[Optional[UUID4]] = []
    versions: Versions
    annotations: List[Annotation] = []
    system_runs: List[SystemRun] = []


if __name__ == '__main__':

    def remove_examples(d):
        if not isinstance(d, (dict, list)):
            return d
        if isinstance(d, list):
            return [remove_examples(v) for v in d]
        return {k: remove_examples(v) for k, v in d.items() if k not in {'example'}}


    schema = Drive.schema()
    schema = remove_examples(schema)

    output_file = 'drive.schema'
    with open(output_file, 'w') as f:
        json.dump(schema, f, indent=2)
