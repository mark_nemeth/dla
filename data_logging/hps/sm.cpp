#include <algorithm>
#include <vector>
#include <thread>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include "common.h"
#include "hps.h"

using namespace std;

#define DEFAULT_MSG_SIZE	(64 * 1024)
#define DEFAULT_MSG_COUNT	(-1)

static volatile sig_atomic_t shutdownRequest = 0;

typedef struct
{
	int showStatistics = 0;
	int cpuAffinity = -1;
	int msgSize = DEFAULT_MSG_SIZE;
	int msgCount = DEFAULT_MSG_COUNT;
	uint64_t rate = -1;
} cfgT;

typedef struct
{
	uint32_t size;
	uint32_t sequence;
	uint64_t timestamp;
} msgHeaderT;

struct hpClient
{
	cfgT cfg;
	const char *uri = NULL;
	hps_type protocol = HPS_NULL;
	hpStreamT *s = NULL;
	bool shutdownRequest = false;
	thread handlerThread;

	hpClient(const char* uri, cfgT *config)
	{
		this->uri = uri;
		protocol = hps_get_protocol(uri);
		memcpy(&cfg, config, sizeof(cfgT));

		cfg.msgSize = MAX(cfg.msgSize, (int)sizeof(msgHeaderT));
		if (protocol == HPS_UDP)
			cfg.msgSize = MIN(cfg.msgSize, MAX_UDP_SIZE);
		printf("Using message size %u\n", cfg.msgSize);

		// Connect
		s = hps_open(uri);
		if (s == NULL)
		{
			printf("Failed to open %s\n", uri);
			throw exception();
		}

		if (s->type == HPS_UDP)
			cfg.msgSize = MIN(cfg.msgSize, MAX_UDP_SIZE);

		handlerThread = thread(&hpClient::run, this);
	}

	~hpClient()
	{
		shutdownRequest = true;
		handlerThread.join();
		if (s)
			hps_close(s);
	}

	void run()
	{
	    // Set CPU affinity
		if (0 <= cfg.cpuAffinity)
		{
		    cpu_set_t cpuset;
		    CPU_ZERO(&cpuset);
		    CPU_SET(cfg.cpuAffinity, &cpuset);
		    printf("Set CPU affinity: %d\n", cfg.cpuAffinity);
		    pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
		}

		// Generate test data pattern
		int bufferSize = cfg.msgSize - sizeof(msgHeaderT) + 256;
		uint8_t *buffer = new uint8_t[bufferSize];
		for (int i = 0; i < bufferSize; i++)
			buffer[i] = (uint8_t)i;

	    // Header
		msgHeaderT header;
		header.size = cfg.msgSize - sizeof(header);
		header.sequence = 0;
		header.timestamp = 0;

		// IOV
		struct iovec iov[2];
		iov[0].iov_base = &header;
		iov[0].iov_len = sizeof(header);
		iov[1].iov_base = buffer;
		iov[1].iov_len = cfg.msgSize - sizeof(header);

		printf("Sending data...\n");
		uint64_t tNextMessageNs = getTimestampNs();
		double MPS = (double)cfg.rate / (double)cfg.msgSize;
		uint64_t tMessageSlotNs = 0 < MPS ? (uint64_t)(1000000000. / MPS) : 0;
		while (!shutdownRequest && cfg.msgCount)
		{
			uint64_t t = getTimestampNs();
			if (cfg.rate != -1 && t < tNextMessageNs)
			{
				pthread_yield();
				continue;
			}

			// Update message
			header.timestamp = t;
			iov[1].iov_base = buffer + header.sequence % 256;

			// Send
			int n = hps_writev(s, iov, 2);
			if (n != cfg.msgSize)
			{
				if (n < 0)
				{
					if (errno == ENOBUFS)
					{
						// Retry
						printf("Failed to send packet (ENOBUFS). Retry.\n");
						pthread_yield();
						continue;
					}
					perror("hps_writev() failed");
					break;
				}
				else
				{
					printf("Could not send all data (peer possibly disconnected)\n");
					break;
				}
			}

			header.sequence++;

			if (0 < cfg.msgCount)
				cfg.msgCount--;

			if (cfg.rate != -1)
				tNextMessageNs += tMessageSlotNs;
		}

		hps_close(s);
		s = NULL;
	}
};

void term(int signum)
{
	shutdownRequest = 1;
}

void help()
{
	printf("Usage: sm [options]\n");
	printf("Options:\n");
	printf("  -s [uri] [out]   Start server on [uri] and send data to [out]\n");
	printf("                   Default server URI: %s\n", DEFAULT_SERVER_URI);
	printf("                   Default out URI: %s\n", DEFAULT_RECORD_URI);
	printf("  -c [uri]         Connect client to [uri]\n");
	printf("                   Default client URI: %s\n", DEFAULT_CLIENT_URI);
	printf("\n");
	printf("  URI :=           protocol[:path[:port]]\n");
	printf("  protocol:=       TCP|UDP|RAW|RDMA|FILE|NULL\n");
	printf("  For TCP|UDP|RAW|RDMA|FILE the path is the IP address.\n");
	printf("    Example: TCP:192.168.77.100:6000\n");
	printf("  Server URI may use asterix (*) instead of the IP.\n");
	printf("    Example: TCP:*:6000\n");
	printf("  FILE protocol must be followed by the file path.\n");
	printf("    Example: FILE:/home/user/file.dat\n");
	printf("    Following placeholders are supported in the file name:\n");
	printf("    %%n : Sequence number\n");
	printf("    %%Y : Current year\n");
	printf("    %%m : Current month\n");
	printf("    %%d : Current day\n");
	printf("    %%h : Current hour\n");
	printf("    %%i : Current minute\n");
	printf("    %%s : Current second\n");
	printf("  NULL protocol does not require any path or port.\n");
	printf("\n");
	printf("  -size [size]     Message size\n");
	printf("  -count [count]   Message count\n");
	printf("  -rate [bps]      Data rate (bytes / s)\n");
	printf("  -a [core]        Set core affinity\n");
	printf("  --recorder       Start sm in recorder mode\n");
	printf("  -v               Show statistics\n");
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	vector<hpServerT*> servers;
	vector<hpClient*> clients;
	cfgT cfg;

    // Set limits
    struct rlimit lim;
    lim.rlim_cur = RLIM_INFINITY;
    lim.rlim_max = RLIM_INFINITY;
    if (setrlimit(RLIMIT_MEMLOCK, &lim))
        perror("Failed to set RLIMIT_MEMLOCK");
    else if (getrlimit(RLIMIT_MEMLOCK, &lim))
        perror("Failed to get RLIMIT_MEMLOCK");

	// Parse options
	if (argc <= 1)
		help();
	for (int i = 1; i < argc; i++)
	{
		if (strcasecmp(argv[i], "-v") == 0)
		{
			cfg.showStatistics = 1;
		}
		else if (strcasecmp(argv[i], "-a") == 0)
		{
			cfg.cpuAffinity = atoi(argv[++i]);
		}
		else if (strcasecmp(argv[i], "-size") == 0)
		{
			cfg.msgSize = atoi(argv[++i]);
		}
		else if (strcasecmp(argv[i], "-count") == 0)
		{
			cfg.msgCount = atoi(argv[++i]);
		}
		else if (strcasecmp(argv[i], "-rate") == 0)
		{
			cfg.rate = atol(argv[++i]);
		}
		else if (strcasecmp(argv[i], "--recorder") == 0)
		{
			const char *outUri = "file:/home/pid1083/sequences/data_%Y_%m_%d_%h_%i_%s_%n.tmp";
			hpServerT *server;

			cfg.showStatistics = 1;

			server = hps_start_server("TCP:*:6000", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("TCP:*:6001", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("TCP:*:6002", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("TCP:*:6003", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("TCP:*:6004", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("UDP:*:7000", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("UDP:*:7001", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("UDP:*:7002", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("UDP:*:7003", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("UDP:*:7004", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("RDMA:*:8000", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("RDMA:*:8001", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("RDMA:*:8002", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("RDMA:*:8003", outUri);
			if (server != NULL)
				servers.push_back(server);
			server = hps_start_server("RDMA:*:8004", outUri);
			if (server != NULL)
				servers.push_back(server);
		}
		else if (strcasecmp(argv[i], "-s") == 0)
		{
			const char *uri = DEFAULT_SERVER_URI;
			const char *out = DEFAULT_RECORD_URI;

			if (i + 1 < argc)
			{
				uri = argv[++i];
			}
			if (i + 1 < argc)
			{
				out = argv[++i];
			}

			printf("Starting server %s -> %s\n", uri, out);
			hpServerT *server = hps_start_server(uri, out);
			if (server != NULL)
				servers.push_back(server);
		}
		else if (strcasecmp(argv[i], "-c") == 0)
		{
			const char *uri = DEFAULT_CLIENT_URI;

			if (i + 1 < argc)
			{
				uri = argv[++i];
			}

			printf("Starting client %s\n", uri);
			try
			{
				hpClient *client = new hpClient(uri, &cfg);
				clients.push_back(client);
			}
			catch (exception&)
			{
				printf("Failed to start client %s\n", uri);
			}
		}
		else
		{
			help();
		}
	}

	if (!(clients.size() || servers.size()))
		return 0;

	// Statistics
	if (cfg.showStatistics)
	{
		// Register SIGTERM handler
		signal(SIGTERM, term);

		// Warmup
		usleep(5000000);

		// Init statistics
		hps_statistics startStat;
		hps_statistics lastStat;
		hps_statistics stat;
		hps_get_statistics(&stat);
		memcpy(&startStat, &stat, sizeof(stat));
		memcpy(&lastStat, &stat, sizeof(stat));

		// Statistics loop
		printf("%*s %*s | ", 12, "TX [MB/s]", 12, "Avg [MB/s]");
		printf("%*s %*s | ", 12, "RX [MB/s]", 12, "Avg [MB/s]");
		printf("%*s %*s | ", 12, "WRITE [MB/s]", 12, "Avg [MB/s]");
		printf("%*s %*s | ", 10, "Packets/s", 10, "Avg. P/s");
		printf("%*s %*s %*s", 9, "Lost/s", 9, "Avg/s", 9, "Loss [%]");
		printf("\n");
	    while (!shutdownRequest)
	    {
	    	usleep(1000000);
	    	hps_get_statistics(&stat);

			double dtS = ((double)(stat.t - lastStat.t) / (double) 1e9);
			double totalS = ((double) (stat.t - startStat.t) / (double) 1e9);
			// TX
			double TxMBPS = (double)(stat.txBytes - lastStat.txBytes) / 1024 / 1024 / dtS;
			double AverageTxMBPS = (double)(stat.txBytes - startStat.txBytes) / 1024 / 1024 / totalS;
			// RX
			double RxMBPS = (double)(stat.rxBytes - lastStat.rxBytes) / 1024 / 1024 / dtS;
			double AverageRxMBPS = (double)(stat.rxBytes - startStat.rxBytes) / 1024 / 1024 / totalS;
			// WRITE
			double WriteMBPS = (double)(stat.writeBytes - lastStat.writeBytes) / 1024 / 1024 / dtS;
			double AverageWriteMBPS = (double)(stat.writeBytes - startStat.writeBytes) / 1024 / 1024 / totalS;
			// Packets/s
			double PPS = (double)(stat.rxMessages - lastStat.rxMessages) / dtS;
			double AveragePPS = (double)(stat.rxMessages - startStat.rxMessages) / totalS;
			// Loss
			double LostMPS = (double)(stat.rxLostMessages - lastStat.rxLostMessages) / dtS;
			double AverageLostMPS = (double)(stat.rxLostMessages - startStat.rxLostMessages) / totalS;
			double LossRate = AverageLostMPS / (AverageLostMPS + AveragePPS) * 100;
			if (0 < TxMBPS || 0 < RxMBPS || WriteMBPS)
			{
				printf("%12.2f %12.2f | ", TxMBPS, AverageTxMBPS);
				printf("%12.2f %12.2f | ", RxMBPS, AverageRxMBPS);
				printf("%12.2f %12.2f | ", WriteMBPS, AverageWriteMBPS);
				printf("%10.0f %10.0f | ", PPS, AveragePPS);
				printf("%9.2f %9.2f %8.4f%%", LostMPS, AverageLostMPS, LossRate);
				printf("\n");
				fflush(stdout);
			}
			memcpy(&lastStat, &stat, sizeof(stat));
	    }
	}
	else
	{
		int sig;
		sigset_t sigmask;
		sigemptyset(&sigmask);
		sigaddset(&sigmask, SIGTERM);
		pthread_sigmask(SIG_UNBLOCK, &sigmask, (sigset_t *)0);
		sigwait(&sigmask, &sig);
	}

	// Shutdown all servers
	if (servers.size())
	{
	    printf("Shutting down all servers...\n");
	    for (hpServerT* server : servers)
	    	hps_close_server(server);
	    servers.clear();
	}

	// Shutdown all clients
	if (clients.size())
	{
	    printf("Shutting down all clients...\n");
	    for (hpClient* client : clients)
	    	delete(client);
	    clients.clear();
	}

    printf("Stream manager terminated\n");

    return 0;
}
