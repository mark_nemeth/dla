import React from 'react';
import {defaultEmptyValue} from '../../Table';
import {sizeConverter} from '../../../utils/valueConverter';
import {isNumberValid} from '../../../utils/helperFunctions';

export default function SizeEntry(props) {

  if (!isNumberValid(props.size)) return <span>{defaultEmptyValue}</span>;

  const formattedSize = sizeConverter(props.size);

  return (
    <span>{formattedSize}</span>
  )
}