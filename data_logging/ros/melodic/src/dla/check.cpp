#include <stdio.h>
#include <chrono>
#include <regex>
#include <ros/ros.h>
#include <ros/duration.h>
#include <ros/message_traits.h>
#include <ros/serialization.h>
#include <ros/time.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include "rosbag/structures.h"
#include "dla/TestMsg.h"

// Also check: http://wiki.ros.org/ros_type_introspection

#define PACKED __attribute__((__packed__))

using namespace std;
using namespace chrono;
using namespace ros;
using namespace rosbag;

enum TypeClass
{
	TYPE_STRUCT,
	TYPE_BOOL,
	TYPE_INT8,
	TYPE_UINT8,
	TYPE_INT16,
	TYPE_UINT16,
	TYPE_INT32,
	TYPE_UINT32,
	TYPE_INT64,
	TYPE_UINT64,
	TYPE_FLOAT32,
	TYPE_FLOAT64,
	TYPE_STRING,
	TYPE_TIME,
	TYPE_DURATION
};

struct TypeInfo
{
	// Element name
	string name;
	// Full type name
	string fullTypeName;
	// Type prefix
	string typePrefix;
	// Type name
	string typeName;
	// Type class
	TypeClass typeClass = TYPE_STRUCT;
	// Base type
	TypeInfo *baseType = nullptr;
	// Array size (-1 = unspecified)
	int array = 0;
	// Structure members
	vector<TypeInfo*> members;

	TypeInfo(string& fullTypeName, TypeClass typeClass)
	{
		this->fullTypeName = fullTypeName;
		this->typeClass = typeClass;

		int n = fullTypeName.rfind('/');
		typeName = n != string::npos ? string(fullTypeName, n + 1) : fullTypeName;
		typePrefix = n != string::npos ? string(fullTypeName, 0, n) : "";
	}

	TypeInfo(string& name, string& fullTypeName, int array)
	{
		this->name = name;
		this->fullTypeName = fullTypeName;
		this->array = array;

		int n = fullTypeName.rfind('/');
		typeName = n != string::npos ? string(fullTypeName, n + 1) : fullTypeName;

		if (typeName == "bool")
			typeClass = TYPE_BOOL;
		else if (typeName == "int8" || typeName == "char")
			typeClass = TYPE_INT8;
		else if (typeName == "uint8" || typeName == "byte")
			typeClass = TYPE_UINT8;
		else if (typeName == "int16")
			typeClass = TYPE_INT16;
		else if (typeName == "uint16")
			typeClass = TYPE_UINT16;
		else if (typeName == "int32")
			typeClass = TYPE_INT32;
		else if (typeName == "uint32")
			typeClass = TYPE_UINT32;
		else if (typeName == "int64")
			typeClass = TYPE_INT64;
		else if (typeName == "uint64")
			typeClass = TYPE_UINT64;
		else if (typeName == "float32")
			typeClass = TYPE_FLOAT32;
		else if (typeName == "float64")
			typeClass = TYPE_FLOAT64;
		else if (typeName == "string")
			typeClass = TYPE_STRING;
		else if (typeName == "time")
			typeClass = TYPE_TIME;
		else if (typeName == "duration")
			typeClass = TYPE_TIME;
		else
			typeClass = TYPE_STRUCT;
	}

	TypeInfo(string fullTypeName, string &data)
	{
		stringstream ss(data);
		string s;
		regex rMsg("^MSG:[\\s]*([^\\s]+).*");
		regex rMember("^([^\\s\\[]+)(\\[([0-9]*)\\])?[\\s]+([^\\s=]+)([\\s]*=[\\s]*[0-9]+)?.*");

		vector<TypeInfo*> types;

		this->fullTypeName = fullTypeName;
		TypeInfo *T = this;
		types.push_back(T);

		while(getline(ss, s, '\n'))
		{
			if (s.empty() || s[0] == '#' || s[0] == '=')
				continue;
			size_t n = s.find('#');
			if (n != string::npos)
				s = string(s, 0, n);

			smatch m;
			if (regex_search(s, m, rMsg))
			{
				// New type definition
				//cout << s << endl;

				string fullTypeName = m[1];

				// Add new type definition
				T = new TypeInfo(fullTypeName, TYPE_STRUCT);
				types.push_back(T);

				continue;
			}

			if (regex_search(s, m, rMember))
			{
				// Struct member definition
				//cout << s << endl;

				string fullTypeName = m[1];
				string arrayData = m[2];
				string arrayCount = m[3];
				string name = m[4];
				string constant = m[5];

				// Constant values are not stored in the message
				if (!constant.empty())
					continue;

				int array = 0;
				if (!arrayData.empty())
				{
					if (!arrayCount.empty())
						array = stoi(arrayCount);
					else
						array = -1;
				}

				// Add member
				TypeInfo *e = new TypeInfo(name, fullTypeName, array);
				T->members.push_back(e);
			}
		}

		// Link all structure members to corresponding types
		for (auto type : types)
		{
			for (auto member: type->members)
			{
				if (member->typeClass != TYPE_STRUCT)
					continue;

				if (member->baseType != nullptr)
					continue;

				string name = member->fullTypeName;
				if (member->fullTypeName.find('/') == string::npos)
					name = type->typePrefix + "/" + member->fullTypeName;

				for (auto e : types)
				{
					if (member->fullTypeName == e->fullTypeName)
					{
						member->baseType = e;
						break;
					}
				}

				if (member->baseType != nullptr)
					continue;

				for (auto e : types)
				{
					if (member->typeName == e->typeName)
					{
						member->baseType = e;
						break;
					}
				}
			}
		}
	}

	void dump(const string &prefix)
	{
		for (auto m: members)
		{
			cout << prefix;
			printf("%-*s", (int)(60 - prefix.size()), m->name.c_str());
			cout << m->fullTypeName;
			if (0 < m->array)
				cout << "[" << m->array << "]";
			else if (-1 == m->array)
				cout << "[]";
			cout << endl;

			if (m->typeClass == TYPE_STRUCT)
			{
				if (m->baseType != nullptr)
				{
					m->baseType->dump(prefix + "  ");
				}
			}
		}
	}

	void dump()
	{
		cout << fullTypeName << endl;
		dump("  ");
	}
};

struct DataAccess
{
	TypeInfo *t;
	uintptr_t d;

	DataAccess()
	{
		t = nullptr;
		d = 0;
	}

	DataAccess(TypeInfo *typeInfo, void *data)
	{
		t = typeInfo;
		d = (uintptr_t)data;
	}

	DataAccess operator[](const string &name)
	{
		for (int i = 0; i < t->members.size(); i++)
		{
			if (t->members[i]->name == name)
			{
				return DataAccess(t->members[i], (void*)d);
			}
		}

		return DataAccess();
	}

	string getString()
	{
		if (t == nullptr)
			throw exception();
		if (t->typeClass != TYPE_STRING)
			throw exception();
		uintptr_t p = d;
		uint32_t len = *(uint32_t*)p;
		p += sizeof(uint32_t);
		return string((char*)p, 0, len);
	}
};

/*
	Header header
	================================================================================
	MSG: std_msgs/Header
	# Standard metadata for higher-level stamped data types.
	# This is generally used to communicate timestamped data
	# in a particular coordinate frame.
	#
	# sequence ID: consecutively increasing ID
	uint32 seq
	#Two-integer timestamp that is expressed as:
	# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
	# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
	# time-handling sugar is provided by the client library
	time stamp
	#Frame this data is associated with
	# 0: no frame
	# 1: global frame
	string frame_id
 */
struct PACKED ROSHeader
{
	uint32_t seq;
	struct PACKED
	{
		uint32_t sec;
		uint32_t nsec;
	} t;
	uint32_t frame_id_len;
	char frame_id[];
};

/*
	aracom_interface_common/aracom_interface_common__Header header
	================================================================================
	MSG: aracom_interface_common/aracom_interface_common__Header
	aracom_interface_common__Timestamp timestamp
	aracom_interface_common__Timestamp tx_timestamp
	uint64 seq
	uint8 frame_id
	================================================================================
	MSG: aracom_interface_common/aracom_interface_common__Timestamp
	uint64 s
	int32 ns
 */
struct PACKED AraComHeader
{
	struct PACKED
	{
		uint64_t s;
		uint32_t ns;
	} timestamp;
	struct PACKED
	{
		uint64_t s;
		uint32_t ns;
	} tx_timestamp;
	uint64_t seq;
	uint8_t frame_id;
};

struct TestMsgHeader
{
	uint64_t id;
	uint64_t seq;
	uint64_t t;
};

// Time context
struct TimeCtx
{
	uint64_t count = 0;
	uint64_t tLast = -1;
	uint64_t dtMin = -1;
	uint64_t dtMax = 0;

	void set(uint64_t t)
	{
		if (0 < count)
		{
			uint64_t dt = (t - tLast) / 1000;
			dtMin = min(dtMin, dt);
			dtMax = max(dtMax, dt);
		}
		tLast = t;
		count++;
	}
};

struct Statistics
{
	Time start = TIME_MAX;
	Time end = TIME_MIN;
	uint64_t count = 0;
	uint64_t size = 0;
	uint64_t minSize = -1;
	uint64_t maxSize = 0;
	uint64_t lost = 0;
	uint64_t duplicates = 0;
	uint64_t recessed = 0;
	uint64_t errors = 0;

	double getDuration()
	{
		return (end - start).toSec();
	}
};

struct StreamCtx
{
	string callerId;

	uint32_t lastSeq = -1;
	bool isSeq = false;

	Statistics stat;

	// Timestamps
	TimeCtx tRecord;
	TimeCtx t;
	TimeCtx tTx;
	uint64_t tPropUs = 0;

	void setSeq(uint32_t seq)
	{
		if (0 < stat.count)
		{
			if (lastSeq != seq)
				isSeq = true;

			int32_t delta = (int32_t)(uint32_t)(seq - lastSeq);
			if (1 < delta)
				stat.lost += delta - 1;
			else if (delta == 0)
				stat.duplicates++;
			else if (delta < 0)
				stat.recessed++;
		}
		lastSeq = seq;
	}

	// Does topic update the sequence counter
	bool seqUpdateOK()
	{
		return (stat.count <= 1) || isSeq;
	}
};

struct Topic
{
	string name;
	string type;
	TypeInfo *typeInfo = nullptr;
	string messageDefinition;
	bool isLatching = false;
	bool isBool = false;
	bool isString = false;
	bool isStatistics = false;
	bool isRosHeader = false;
	bool isAraComHeader = false;

	map<string, StreamCtx> streams;

	void AnalyzeHeader()
	{
		if (type == "std_msgs/String")
		{
			isString = true;
		}
		else if (type == "std_msgs/Bool")
		{
			isBool = true;
		}
		else if (type == "profiler_statistics_msgs/HostStatistics" || type == "profiler_statistics_msgs/NodeStatistics")
		{
			isStatistics = true;
		}
		else
		{
			regex rosHeader("^Header[\\s]+header.*");
			regex araComHeader("^aracom_interface_common/aracom_interface_common__Header[\\s]+header.*");
			stringstream ss(messageDefinition);
			string s;
			while (getline(ss, s, '\n'))
			{
				if (regex_match(s, rosHeader))
				{
					isRosHeader = true;
					break;
				}
				else if (regex_match(s, araComHeader))
				{
					isAraComHeader = true;
					break;
				}
			}
		}
	}
};

enum TopicRule
{
	NONE = 0,
	BLACKLIST,
	SINGLE_SHOT
};

map<string, TopicRule> topicRules;
map<string, Topic> topics;

int main(int argc, char **argv)
{
	bool verbose = false;
	bool listTopics = false;
	string bagFilePath;

	auto start = high_resolution_clock::now();

	topicRules["/tf_static"] = BLACKLIST;

	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-v") == 0)
		{
			verbose = true;
		}
		else if (strcmp(argv[i], "-l") == 0)
		{
			listTopics = true;
		}
		else
		{
			bagFilePath = string(argv[i]);
			break;
		}
	}
	if (bagFilePath.empty())
		return -1;

	cout << "Processing file " << bagFilePath << endl;

	Bag bag;
	try
	{
		bag.open(bagFilePath);
	}
	catch (exception& ex)
	{
		cout << " Error: " << ex.what() << endl;
		return -1;
	}

	View view(bag);

	// Scan all messages
	vector<uint8_t> buffer;
	for (MessageInstance const& mi: view)
	{
		uint64_t size = mi.size();
		// rosbag time
		Time t = mi.getTime();
		uint64_t tRecord = (uint64_t)t.sec * 1e9 + t.nsec;

		// Get data
		if (buffer.size() < size)
			buffer.resize(size);
		serialization::OStream stream(buffer.data(), size);
		mi.write(stream);

		// Get topic
		Topic& topic = topics[mi.getTopic()];
		if (topic.streams.empty())
		{
			// First topic
			topic.name = mi.getTopic();
			topic.type = mi.getDataType();
			topic.messageDefinition = mi.getMessageDefinition();
			topic.isLatching = mi.isLatching();
			topic.typeInfo = new TypeInfo(topic.type, topic.messageDefinition);
			topic.AnalyzeHeader();
		}

//		if (verbose)
//		{
//			if (topic.type == "std_msgs/String")
//			{
//				DataAccess da(topic.typeInfo, buffer.data());
//				string test = da["data"].getString();
//				cout << topic.name << " = " << test << endl;
//			}
//		}

		// Get stream
		StreamCtx& s = topic.streams[mi.getCallerId()];
		if (s.stat.count == 0)
		{
			// First message
			s.stat.start = t;
			s.callerId = mi.getCallerId();
		}

		s.tRecord.set(tRecord);

		if (topic.isRosHeader)
		{
			ROSHeader *header = (ROSHeader*)&buffer[0];

			s.setSeq(header->seq);

			uint64_t t = (uint64_t)header->t.sec * 1e9 + header->t.nsec;
			s.t.set(t);
		}
		else if (topic.isAraComHeader)
		{
			AraComHeader *header = (AraComHeader*)&buffer[0];

			s.setSeq((uint32_t)header->seq);

			uint64_t t = (uint64_t)header->timestamp.s * 1e9 + header->timestamp.ns;
			uint64_t tTX = (uint64_t)header->tx_timestamp.s * 1e9 + header->tx_timestamp.ns;
			s.t.set(t);
			s.tTx.set(tTX);
		}

		s.stat.count++;
		s.stat.size += size;
		s.stat.minSize = min(s.stat.minSize, size);
		s.stat.maxSize = max(s.stat.maxSize, size);
		s.stat.end = t;

		if (topic.isRosHeader || topic.isAraComHeader)
		{
			uint64_t tPropUs = (s.tRecord.tLast - s.t.tLast) / 1000;
			s.tPropUs += tPropUs;
		}
	}

	// Collect total statistics
	Statistics stat;
	size_t maxTopicSize = 0;
	size_t maxTypeSize = 0;
	size_t maxCallerSize = 0;
	for (auto &e : topics)
	{
		Topic& topic = e.second;

		maxTopicSize = max(maxTopicSize, topic.name.size());
		maxTypeSize = max(maxTypeSize, topic.type.size());

		for (auto &kvStream : topic.streams)
		{
			StreamCtx& s = kvStream.second;

			maxCallerSize = max(maxCallerSize, s.callerId.size());

			stat.start = min(stat.start, s.stat.start);
			stat.end = max(stat.end, s.stat.end);
			stat.count += s.stat.count;
			stat.size += s.stat.size;
			stat.minSize = min(stat.minSize, s.stat.minSize);
			stat.maxSize = max(stat.maxSize, s.stat.maxSize);
			stat.lost += s.stat.lost;
			stat.duplicates += s.stat.duplicates;
		}
	}

	printf("\n");

	// Dump types
	if (verbose)
	{
		printf("Types:\n");
		set<string> typeNames;

		for (auto &e : topics)
		{
			Topic &topic = e.second;
			if (typeNames.find(topic.type) == typeNames.end())
			{
				topic.typeInfo->dump();
				typeNames.insert(topic.type);
			}
		}

		cout << endl;
	}

	if (listTopics)
	{
		printf("Topics:\n");
		for (auto &e : topics)
		{
			Topic &topic = e.second;
			printf("%s (%lu)\n", topic.name.c_str(), topic.streams.size());
		}
		printf("\n");
	}

	// Dump topics 1/2
	{
		printf("%3s %-*s %-*s %-*s\n",
				"No",
				(int)maxTopicSize, "Topic",
				(int)maxTypeSize, "Type",
				(int)maxCallerSize, "Caller");

		int idx = 0;
		for (auto &e : topics)
		{
			Topic &topic = e.second;

			for (auto &kvStream : topic.streams)
			{
				StreamCtx& s = kvStream.second;

				printf("%3u %-*s %-*s %-*s\n",
						idx++,
						(int)maxTopicSize, topic.name.c_str(),
						(int)maxTypeSize, topic.type.c_str(),
						(int)maxCallerSize, s.callerId.c_str());
			}
		}
	}

	printf("\n");

	// Dump topics 2/2
	{
		printf("Legend:\n");
		printf("[H] Header: [A] = ara::com [R] = ROS\n");
		printf("[L] Latches: [X] = Latched topic [ ] = not latched topic\n");
		printf("[S] Sequence counter: [X] = sequence counter is set [ ] = sequence counter is not set\n");
		printf("ROSMinDT: min. ROS recorder interframe time [us]\n");
		printf("ROSMaxDT: max. ROS recorder interframe time [us]\n");
		printf("MinDT: min. header timestamp interframe time [us]\n");
		printf("MaxDT: max. header timestamp interframe time [us]\n");
		printf("TXMinDT: min. header tx_timestamp interframe time [us]\n");
		printf("TXMaxDT: max. header tx_timestamp interframe time [us]\n");
		printf("TProp: Average propagation time (record time - transmit time) [us]\n");
		printf("%3s %-*s [H] [L] [S] %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s\n",
				"No",
				(int)maxTopicSize, "Topic",
				"Count", "Time", "Frames/s", "MB/s",
				"ROSMinDT", "ROSMaxDT", "MinDT", "MaxDT", "TXMinDT", "TXMaxDT", "TProp");

		int idx = 0;
		for (auto &e : topics)
		{
			Topic &topic = e.second;

			for (auto &kvStream : topic.streams)
			{
				StreamCtx& s = kvStream.second;

				printf("%3u %-*s %s %s",
						idx++,
						(int)maxTopicSize, topic.name.c_str(),
						topic.isAraComHeader ? "[A]" : (topic.isRosHeader ? "[R]" : "[ ]"),
						topic.isLatching ? "[X]" : "[ ]");
				if (topic.isAraComHeader || topic.isRosHeader)
					printf(" %s", s.seqUpdateOK() ? "[X]" : "[ ]");
				else
					printf(" %s", "   ");
				printf(" %8lu", s.stat.count);
				printf(" %8.3f", s.stat.getDuration());
				printf(" %8.3f", s.stat.count / s.stat.getDuration());
				printf(" %8.3f", s.stat.size / s.stat.getDuration() / 1024 / 1024);

				{
					if (s.tRecord.count == 0)
						printf(" --------");
					else if (9999999 < s.tRecord.dtMin)
						printf(" >9999999");
					else
						printf(" %8lu", s.tRecord.dtMin);

					if (s.tRecord.count == 0)
						printf(" --------");
					else if (9999999 < s.tRecord.dtMax)
						printf(" >9999999");
					else
						printf(" %8lu", s.tRecord.dtMax);
				}

				{
					if (s.t.count == 0)
						printf(" --------");
					else if (9999999 < s.t.dtMin)
						printf(" >9999999");
					else
						printf(" %8lu", s.t.dtMin);

					if (s.t.count == 0)
						printf(" --------");
					else if (9999999 < s.t.dtMax)
						printf(" >9999999");
					else
						printf(" %8lu", s.t.dtMax);
				}

				{
					if (s.tTx.count == 0)
						printf(" --------");
					else if (9999999 < s.tTx.dtMin)
						printf(" >9999999");
					else
						printf(" %8lu", s.tTx.dtMin);

					if (s.tTx.count == 0)
						printf(" --------");
					else if (9999999 < s.tTx.dtMax)
						printf(" >9999999");
					else
						printf(" %8lu", s.tTx.dtMax);
				}

				{
					uint64_t tAvgPropUs = s.tPropUs / s.stat.count;
					if (!(topic.isAraComHeader || topic.isRosHeader))
						printf(" --------");
					else if (9999999 < tAvgPropUs)
						printf(" >9999999");
					else
						printf(" %8lu", tAvgPropUs);
				}

				printf("\n");
			}
		}
	}

	// Dump statistics
	{
		double dt = stat.getDuration();
		double MB = (double)stat.size / 1024 / 1024;
		uint64_t avgMsgSize = stat.size / stat.count;
		double MBPS = (double)MB / dt;
		double MPS = (double)stat.count / dt;
		printf("\nStatistics\n");
		printf("Topics:             %lu\n", topics.size());
		printf("Messages:           %lu\n", stat.count);
		printf("Messages lost:      %lu\n", stat.lost);
		printf("Min. message size:  %lu\n", stat.minSize);
		printf("Avg. message size:  %lu\n", avgMsgSize);
		printf("Max. message size:  %lu\n", stat.maxSize);
		printf("Message rate:       %.1f messages/s\n", MPS);
		printf("Data size:          %.1f MB\n", (double)stat.size / 1024 / 1024);
		printf("Data rate:          %.1f MB/s\n", MBPS);
		printf("File size:          %.1f MB (%.2f %% overhead)\n", (double)bag.getSize() / 1024 / 1024,
				((double)(bag.getSize()) / stat.size - 1) * 100.);
	}

	// Processing time
	{
		auto stop = high_resolution_clock::now();
		double dt = (double)chrono::duration_cast<milliseconds>(stop - start).count() / 1000;
		double MB = (double)bag.getSize() / 1024 / 1024;
		double MBPS = MB / dt;
		printf("Processing time:    %.1f s (%.1f MB/s)\n", dt, MBPS);
	}

	// Dump problems
	printf("\nProblems:\n");
	int errors = 0;
	for (auto &e : topics)
	{
		Topic &topic = e.second;

		if (topicRules[topic.name] == BLACKLIST)
			continue;

		if (!(topic.isRosHeader || topic.isAraComHeader || topic.isStatistics || topic.isString || topic.isBool))
		{
			printf("Topic %s (type %s) is missing ROS or ARACOM header\n",
					topic.name.c_str(), topic.type.c_str());
			errors++;
		}

		for (auto &kvStream : topic.streams)
		{
			StreamCtx& s = kvStream.second;

			// We need at least 2 messages for analysis
			if (s.stat.count <= 1)
				continue;

			// Check sequence counter
			if (topic.isAraComHeader || topic.isRosHeader)
			{
				// Check if topic update sequence counter
				if (s.seqUpdateOK())
				{
					if (s.stat.lost == 0 && s.stat.duplicates == 0 && s.stat.recessed == 0)
					{
						// Everything is OK
						continue;
					}

					// Check duplicates
					if (0 < s.stat.duplicates)
					{
						printf("Topic %s sequence counter encountered %lu duplicates\n",
								topic.name.c_str(), s.stat.duplicates);
						errors++;
					}

					// Check recessed
					if (0 < s.stat.recessed)
					{
						printf("Topic %s sequence counter recessed %lu times\n",
								topic.name.c_str(), s.stat.recessed);
						errors++;
					}
				}
				else
				{
					printf("Topic %s does not set sequence counter\n", topic.name.c_str());
					errors++;
				}
			}

			// Sequence based verification failed, try inter-frame time analysis

			// Check time stamps

			// Try to validate via timestamp
			if (topic.isAraComHeader || topic.isRosHeader)
			{
				if (0 < s.t.dtMin && ((double)s.t.dtMax / (double)s.t.dtMin < 1.5))
				{
					// Time sequence looks fine
					continue;
				}

				if (0 < s.tTx.dtMin && ((double)s.tTx.dtMax / (double)s.tTx.dtMin < 1.5))
				{
					// Time sequence looks fine
					continue;
				}
			}

			if (1000000 < s.tRecord.dtMin && ((double)s.tRecord.dtMax / (double)s.tRecord.dtMin < 1.5))
			{
				// Time sequence looks fine
				continue;
			}

			if (0 < s.stat.lost)
			{
				printf("Topic %s lost %lu messages\n",
						topic.name.c_str(), s.stat.lost);
				errors++;
			}
			else
			{
				printf("Failed to analyze topic %s\n",
						topic.name.c_str());
				errors++;
			}
		}
	}

	bag.close();

	if (errors)
	{
		printf("\nBag file analysis identified %d errors\n", errors);
	}
	else
	{
		printf("No errors\n");
	}

	return 0;
}

