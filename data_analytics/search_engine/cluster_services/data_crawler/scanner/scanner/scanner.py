import csv
import itertools
import logging
import os
import pathlib
import threading
import uuid
from datetime import datetime
from hashlib import md5
from multiprocessing import Pool
from multiprocessing.pool import ThreadPool
from typing import AnyStr, Generator, Iterable, List, Optional, Tuple
from config.constants import DANF_VERSION_TAG

import fire

logging.basicConfig(format='%(asctime)s:\t%(levelname)s:\tPID %(process)d:\t%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def scan_dirs(output_path: str, directories: List[str], num_processes: int, num_workers: int, chunk_size: int,
              whitelist: str = "*") -> None:
    expanded_dirs = []
    for directory in directories:
        expanded_dirs += expand_dirs(pathlib.Path(directory))

    process_pool = Pool(num_processes)

    for expanded_dir in expanded_dirs:
        process_pool.apply_async(scan, (output_path, [expanded_dir], num_workers, chunk_size, whitelist))

    process_pool.close()
    process_pool.join()


def expand_dirs(directory: pathlib.Path) -> List[pathlib.Path]:
    assert directory.is_dir()
    return [sub_directory for sub_directory in directory.iterdir() if sub_directory.is_dir()]


def scan(output_path: str, directories: List[str], num_workers: int, chunk_size: int, whitelist: str = "**/*") -> None:
    temp_name = str(uuid.uuid4())
    try:
        output_path = pathlib.Path(output_path)
        assert output_path.is_dir(), f'"{output_path}" is not a directory.'
        directories = [pathlib.Path(directory) for directory in directories]
        for directory in directories:
            assert directory.is_dir(), f'"{directory}" is not a directory.'
        num_workers = int(num_workers)
        chunk_size = int(chunk_size)
    except (ValueError, AssertionError) as e:
        print(f"Error parsing arguments:\n{str(e)}")
        exit(1)

    pool = ThreadPool(num_workers)

    files = list_files(directories, whitelist)

    def gen_jobs() -> Generator[Tuple[Iterable[pathlib.Path], int, str], None, None]:
        i = 0
        while True:
            top_chunk = list(itertools.islice(files, chunk_size))
            if not top_chunk:
                logger.info("All jobs queued.")
                return
            logger.info(f"Queueing job {i}...")
            yield top_chunk, i, temp_name
            i += 1

    results = pool.imap_unordered(collect_file_metadata, gen_jobs())

    for result in results:
        logger.info(f"Job {result} completed.")

    pool.close()
    pool.join()

    paths = pathlib.Path.cwd().glob(f"scanner-{temp_name}-*.csv")
    if not output_path.exists():
        os.mkdir(output_path)

    clusters = determine_cluster_name(directories)
    with open(output_path.joinpath(f"scanner-{clusters}-{temp_name}.csv"), "w") as csv_file:
        csv_file.write("Timestamp,FullPath,FileName,CreationTime,FileSize,MD5(lastMB)\n")
        for path in paths:
            print(f'Appending {path.name}')
            with open(path, "r") as temp:
                csv_file.write(temp.read())
                csv_file.flush()
            path.unlink()

    with open(output_path.joinpath(f"scanner-{clusters}-{temp_name}.folder"), "w") as scanned_folder:
        content = "\n".join([str(directory.absolute()) for directory in directories])
        scanned_folder.write(content)
        scanned_folder.flush()
    print(f"Scanning folder {directories} complete.")


def collect_file_metadata(arguments: Tuple[Iterable[pathlib.Path], int, str]) -> int:
    files, chunk, folder_name = arguments
    timestamp = datetime.now().timestamp()
    lines = [collect_metadata(file, timestamp) for file in files]
    lines = [line for line in lines if line]

    with open(f"scanner-{folder_name}-{threading.current_thread().ident}.csv", 'a') as fp:
        writer = csv.writer(fp, quoting=csv.QUOTE_ALL)
        writer.writerows(lines)
    return chunk


def collect_metadata(file: pathlib.Path, timestamp: float) -> Optional[List[str]]:
    try:
        if file.is_dir():
            return
        full_path = file.absolute()
        file_name = file.name
        creation_time, file_size = fetch_creation_time_and_file_size(file)
        chunk = get_last_mb(file, file_size)

        if chunk is None:
            md5_hash = "ERROR"
        else:
            md5_hash = md5(chunk).hexdigest()
        result = [str(e) for e in [timestamp, full_path, file_name, creation_time, file_size, md5_hash]]
        return result
    except OSError as e:
        print(e)
        return


def determine_cluster_name(directories: List[pathlib.Path]) -> str:
    def get_name(directory: pathlib.Path):
        cluster_name = directory.parts[2]
        if cluster_name == "hdfs":
            return "abt"
        if cluster_name == "624.mbc.us":
            return "sv"
        if cluster_name == "738.mbc.de":
            return "hks"
        if cluster_name == "059-va2.mbc.de":
            return "vai"
        return "unknown"

    names = set(get_name(directory) for directory in directories)
    return "-".join(sorted(names))


def list_files(folders: List[pathlib.Path], whitelist: str) -> Iterable[pathlib.Path]:
    return itertools.chain.from_iterable(folder.rglob(whitelist) for folder in folders)


def fetch_creation_time_and_file_size(file: pathlib.Path) -> Tuple[float, int]:
    stat = file.stat()
    creation_time = stat.st_ctime
    file_size = stat.st_size
    return creation_time, file_size


def get_last_mb(file: pathlib.Path, file_size: int) -> Optional[AnyStr]:
    try:
        with open(file, 'rb') as fp:
            fp.seek(-1 * min(file_size, 1024 * 1024), os.SEEK_END)
            chunk = fp.read()
        return chunk
    except OSError as e:
        print(f"Error accessing file {file}: {str(e)}")
        return None


if __name__ == "__main__":
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    fire.Fire(scan_dirs)
