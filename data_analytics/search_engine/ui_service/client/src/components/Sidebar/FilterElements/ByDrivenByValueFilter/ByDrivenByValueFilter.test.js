import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByDrivenByValueFilter} from "./ByDrivenByValueFilter";
import ScrollList from "../../../Utils/ScrollList/ScrollList";

describe('ByDrivenByValueFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      drivenByValueList: ['driven_by-me-1', 'driven_by-me-2', 'driven_by-me-3'],
      selectedDrivenByValueList: ['driven_by-me-2']
    };
    const wrapper = shallow(<ByDrivenByValueFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should renders driven by value list with selected column', () => {
    const reduxAction = jest.fn();
    const props = {
      drivenByValueList: ['driven_by-me-1', 'driven_by-me-2', 'driven_by-me-3'],
      selectedDrivenByValueList: ['driven_by-me-2'],
      fetchAllDrivenByValues: reduxAction,
      filterByDrivenByValues: reduxAction
    };
    const wrapper = mount(<ByDrivenByValueFilter {...props} />);
    const drivenByValueList = wrapper.find(ScrollList);
    const entries = drivenByValueList.find('tr');
    expect(entries.length).toBe(3);
    expect(entries.at(0).find('input').props().checked).toBe(false);
    expect(entries.at(1).find('input').props().checked).toBe(true);
    expect(entries.at(2).find('input').props().checked).toBe(false);
  });


  it('should call action when driven by value is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      drivenByValueList: ['driven_by-me-1', 'driven_by-me-2', 'driven_by-me-3'],
      selectedDrivenByValueList: ['driven_by-me-2'],
      fetchAllDrivenByValues: jest.fn(),
      filterByDrivenByValues: reduxAction
    };
    const wrapper = mount(<ByDrivenByValueFilter {...props} />);
    const input = wrapper.find('tr').at(1).find('input');
    input.simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

});
