__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from locust import TaskSet, task

from config.config_handler import config_dict
from utils.locust_tools import time_tracked
from utils.pymongo_locust import PyMongoLocust


class Behavior(TaskSet):

    @task
    @time_tracked(request_type='pymongo')
    def demo(self):
        col = self.client['child_bagfiles']
        cursor = col.find(
            {'bva': {'$gte': 5}},
            {'_id': 0}
        ).limit(10)
        list(cursor)  # exhaust cursor


class User(PyMongoLocust):
    host = config_dict.get_value('DB_HOST')
    port = config_dict.get_value('DB_PORT')
    user = config_dict.get_value('DB_USER')
    key = config_dict.get_value('DB_KEY')

    task_set = Behavior
