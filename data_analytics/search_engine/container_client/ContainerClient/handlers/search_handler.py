"""Search Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import requests

from ContainerClient.config.config_handler import config_dict
from ContainerClient.handlers.auth_handler import create_session
from DANFUtils.tracing import tracing
from DANFUtils.constants import *
from DANFUtils.exceptions import RESTException, MissingEnvironmentVariableException
from ContainerClient.handlers.base_request_handler import BaseRequestHandler, HttpMethod


class SearchHandler(BaseRequestHandler):
    """
    Search Handler class
    """

    def __init__(self):
        super().__init__()

        self.BASE_URL = config_dict.get_value('SEARCH_API_BASE_URL')
        if not self.BASE_URL:
            raise MissingEnvironmentVariableException('ContainerClient',
                                                      'SEARCH_API_BASE_URL')
        self.SNIPPET_BVA_URL = self.BASE_URL + "snippet/bva/"
        self.BAGFILE_LINKS_URL = self.BASE_URL + "crawler/bagfiles"
        self.SEARCH_DRIVE_URL = self.BASE_URL + "search/drive"
        self.SNIPPET_EXTRACTOR_URL = self.BASE_URL + "snippet/extractor"
        self.SEARCH_BAGFILE_URL = self.BASE_URL + "search/bagfile"
        self.FILTERS_URL = self.BASE_URL + "filters"
        self.SEARCH_CHILDBAGFILE_URL = self.BASE_URL + "search/childbagfile"
        self.SEARCH_SEQUENCES_URL = self.BASE_URL + "search/sequence"
        self.SEARCH_FLOW_URL = self.BASE_URL + "search/flow"
        self.SEARCH_FLOW_RUN_URL = self.BASE_URL + "search/flow-run"
        self.SEARCH_CATALOG_URL = self.BASE_URL + "file/"
        self.SEARCH_EXTRACTOR_REQUEST_URL = self.BASE_URL + "search/extractorrequest"
        self.UNSYNCED_BAGFILES_URL = self.BASE_URL + "bagfiles/guids/synchronizable"
        self.DELETED_BAGFILES_URL = self.BASE_URL + "bagfiles/guids/deletable"

    def _init_session(self):
        self.AUTH = config_dict.get_value('AUTH')
        if self.AUTH == "auth0":
            token_url = config_dict.get_value('AUTH_TOKEN_URL')
            client_credentials_id = config_dict.get_value('AUTH_SEARCH_CLIENT_CRED_ID')
            client_credentials_secret = config_dict.get_value('AUTH_SEARCH_CLIENT_CRED_SECRET')
            audience = config_dict.get_value('AUTH_SEARCH_AUDIENCE')
            if token_url and client_credentials_id and client_credentials_secret and audience:
                self.session = create_session(token_url=token_url,
                                              client_credentials_id=client_credentials_id,
                                              client_credentials_secret=client_credentials_secret,
                                              audience=audience)
        if 'session' not in self.__dict__:
            self.session = requests.Session()
        self.set_retries()

    @tracing.trace_task('get_snippets_bva')
    def get_snippets_bva(self,
                         bagfile_guid,
                         bva_limit,
                         gps_subsampling_ratio=5,
                         extractors=None,
                         generate_child_bagfiles=True):
        # validate input data
        assert isinstance(bagfile_guid, str), 'guid has to be of type string.'
        assert isinstance(bva_limit, int), 'bva_limit has to be of type int'
        assert 0 <= bva_limit <= 10, 'bva_limit has to be in range 0<=bva_limit<=10'
        assert isinstance(gps_subsampling_ratio,
                          int), 'gps_subsampling_ratio has to be of type int'
        assert 1 <= gps_subsampling_ratio <= 60, 'gps_subsampling_ratio has to be in range [1, 60]'
        assert isinstance(
            generate_child_bagfiles,
            bool), 'generate_child_bagfiles has to be of type bool'

        tracing.task_attribute("bagfile_guid", bagfile_guid)

        data_dict = {
            'bagfile_guid': bagfile_guid,
            'bva_limit': bva_limit,
            'gps_subsampling_ratio': gps_subsampling_ratio,
            'generate_child_bagfiles': generate_child_bagfiles
        }
        if extractors:
            data_dict['extractors'] = extractors

        res = self.send_request(HttpMethod.POST,
                                url=self.SNIPPET_BVA_URL,
                                json=data_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            snippet_dicts = json.loads(res.text)['results']
            return snippet_dicts, res
        else:
            # handle errors
            raise RESTException(self.COMPONENT,
                                HttpMethod.POST.value,
                                self.SNIPPET_BVA_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('get_bagfile_links')
    def get_bagfile_links(self):
        res = self.send_request(HttpMethod.GET,
                                url=self.BAGFILE_LINKS_URL,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.BAGFILE_LINKS_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('get_drive_links')
    def get_drive_links(self):
        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_DRIVE_URL,
                                params={'projection': 'file_path','limit': 0},
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            drives = json.loads(res.text)['results']
            return [drive.get('file_path', '') for drive in drives], res
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.SEARCH_DRIVE_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('get_snippets_extractor')
    def get_snippets_extractor(self, bagfile_guid, bva_limit, extractors,
                               generate_child_bagfiles):
        assert isinstance(bagfile_guid, str), 'guid has to be of type string.'
        assert isinstance(bva_limit, int), 'bva_limit has to be of type int'
        assert 0 <= bva_limit <= 10, 'bva_limit has to be in range 0<=bva_limit<=10'
        assert isinstance(generate_child_bagfiles, bool), 'generate_child_bagfiles has to be of type bool'

        tracing.task_attribute("bagfile_guid", bagfile_guid)

        data_dict = {
            "bagfile_guid": bagfile_guid,
            "bva_limit": bva_limit,
            "generate_child_bagfiles": generate_child_bagfiles
        }

        if extractors:
            data_dict["extractors"] = extractors

        res = self.send_request(HttpMethod.POST,
                                url=self.SNIPPET_EXTRACTOR_URL,
                                json=data_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            snippet_dicts = json.loads(res.text)['results']
            return snippet_dicts, res
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.POST.value,
                                self.SNIPPET_EXTRACTOR_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('bagfilesearch/get', tracing.Kind.DEPENDENCY)
    def search_bagfiles(self, query, limit, offset, sortby):
        assert isinstance(query, dict), "query must be of type dict"
        assert limit is None or isinstance(limit, int), "limit must be of type int"
        assert offset is None or isinstance(offset, int), "offset must be of type int"
        assert sortby is None or isinstance(sortby, str), "sortby must be of type str"

        params = dict()
        params.update(query)
        if limit is not None:
            params['limit'] = limit
        if offset is not None:
            params['offset'] = offset
        if sortby is not None:
            params['sortby'] = sortby

        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_BAGFILE_URL,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.POST.value,
                                self.SEARCH_BAGFILE_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('search_drivefiles/get', tracing.Kind.DEPENDENCY)
    def search_drivefiles(self, query, limit, offset, sortby):
        assert isinstance(query, dict), "query must be of type dict"
        assert limit is None or isinstance(limit, int), "limit must be of type int"
        assert offset is None or isinstance(offset, int), "offset must be of type int"
        assert sortby is None or isinstance(sortby, str), "sortby must be of type str"

        params = dict()
        params.update(query)
        if limit is not None:
            params['limit'] = limit
        if offset is not None:
            params['offset'] = offset
        if sortby is not None:
            params['sortby'] = sortby

        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_DRIVE_URL,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.POST.value,
                                self.SEARCH_DRIVE_URL,
                                res.status_code,
                                res.text)


    @tracing.trace_task('filters/get', tracing.Kind.DEPENDENCY)
    def get_filters(self):
        res = self.send_request(HttpMethod.GET,
                                url=self.FILTERS_URL,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.FILTERS_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('bagfiles/guids/synchronizable/get', tracing.Kind.DEPENDENCY)
    def get_unsynced_bagfile_guids(self):
        res = self.send_request(HttpMethod.GET,
                                url=self.UNSYNCED_BAGFILES_URL,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']

        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.UNSYNCED_BAGFILES_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('bagfiles/guids/deletable/get', tracing.Kind.DEPENDENCY)
    def get_bagfile_guids_to_delete(self):
        res = self.send_request(HttpMethod.GET,
                                url=self.DELETED_BAGFILES_URL,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']

        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.DELETED_BAGFILES_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('childbagfilesearch/get', tracing.Kind.DEPENDENCY)
    def search_childbagfiles(self, query, limit, offset, sortby):
        assert isinstance(query, dict), "query must be of type dict"
        assert limit is None or isinstance(limit, int), "limit must be of type int"
        assert offset is None or isinstance(offset, int), "offset must be of type int"
        assert sortby is None or isinstance(sortby, str), "sortby must be of type str"

        params = dict()
        params.update(query)
        if limit is not None:
            params['limit'] = limit
        if offset is not None:
            params['offset'] = offset
        if sortby is not None:
            params['sortby'] = sortby

        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_CHILDBAGFILE_URL,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.SEARCH_CHILDBAGFILE_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('sequencesearch/get', tracing.Kind.DEPENDENCY)
    def search_sequences(self, query):
        assert isinstance(query, dict), "query must be of type dict"

        params = dict()
        params.update(query)

        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_SEQUENCES_URL,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.SEARCH_SEQUENCES_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('extractorrequestsearch/get', tracing.Kind.DEPENDENCY)
    def search_extractor_requests(self, query):
        assert isinstance(query, dict), "query must be of type dict"

        params = dict()
        params.update(query)
        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_EXTRACTOR_REQUEST_URL,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.SEARCH_EXTRACTOR_REQUEST_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('search/flow/get', tracing.Kind.DEPENDENCY)
    def search_flows(self, query, limit, offset, sortby):
        assert isinstance(query, dict), "query must be of type dict"
        assert limit is None or isinstance(limit, int), "limit must be of type int"
        assert offset is None or isinstance(offset, int), "offset must be of type int"
        assert sortby is None or isinstance(sortby, str), "sortby must be of type str"

        params = dict()
        params.update(query)
        if limit is not None:
            params['limit'] = limit
        if offset is not None:
            params['offset'] = offset
        if sortby is not None:
            params['sortby'] = sortby

        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_FLOW_URL,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.POST.value,
                                self.SEARCH_FLOW_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('search/flow-run/get', tracing.Kind.DEPENDENCY)
    def search_flow_runs(self, query, limit, offset, sortby):
        assert isinstance(query, dict), "query must be of type dict"
        assert limit is None or isinstance(limit, int), "limit must be of type int"
        assert offset is None or isinstance(offset, int), "offset must be of type int"
        assert sortby is None or isinstance(sortby, str), "sortby must be of type str"

        params = dict()
        params.update(query)
        if limit is not None:
            params['limit'] = limit
        if offset is not None:
            params['offset'] = offset
        if sortby is not None:
            params['sortby'] = sortby

        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_FLOW_RUN_URL,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.SEARCH_FLOW_RUN_URL,
                                res.status_code,
                                res.text)

    @tracing.trace_task('search/catalog/get', tracing.Kind.DEPENDENCY)
    def search_catalog(self, query, limit, offset, sortby):
        assert isinstance(query, dict), "query must be of type dict"
        assert limit is None or isinstance(limit, int), "limit must be of type int"
        assert offset is None or isinstance(offset, int), "offset must be of type int"
        assert sortby is None or isinstance(sortby, str), "sortby must be of type str"

        params = dict()
        params.update(query)
        if limit is not None:
            params['limit'] = limit
        if offset is not None:
            params['offset'] = offset
        if sortby is not None:
            params['sortby'] = sortby

        res = self.send_request(HttpMethod.GET,
                                url=self.SEARCH_CATALOG_URL,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_OK:
            return json.loads(res.text)['results']
        else:
            raise RESTException(self.COMPONENT,
                                HttpMethod.GET.value,
                                self.SEARCH_CATALOG_URL,
                                res.status_code,
                                res.text)
