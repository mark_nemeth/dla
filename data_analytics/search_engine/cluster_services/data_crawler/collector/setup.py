"""Setup file"""
from setuptools import find_packages, setup

from config.constants import VERSION

setup(
    name='collector',
    version=VERSION,
    packages=find_packages(),
    test_suite="tests",
    install_requires=[
        'pandas~=1.0.5',
        'fire~=0.3.1',
        'requests~=2.24.0',
        'joblib~=0.16.0',
        'dataclasses_json~=0.5.2',
        'DANFUtils==0.2.29',
    ]
)
