# DANF - performance testsuite 0.1.4
## Dockerize/Deploy Metadata API - Service to cluster
**If you want to build, run, and push the docker images manually, please add the prefix `man-`**

The performance test suite is based on the locust project. It can be run in a interactive mode with a web based UI or
in batch mode.

# How to run tests locally

## Interactive mode

 ```console
# locust -f <locust file>
locust -f testcases/search_test.py
```

A local web server will be launched that servers the locust UI on http://localhost:8089

Attention: db_connector_test.py can only be run in batch mode at the moment due to a conflict of locust
with the usage of `coloredlogs` in DANFUtils logging.

## Batch mode

 ```console
# python3 testcases/runner.py --file <locust file>
python3 testcases/runner.py --file search_test.py --host http://danf-search-api-dev.westeurope.cloudapp.azure.com:8889
```

Required parameters are the name of the locust file to execute and the target host (e.g. of the api or the database,
depending on the test case). For optional arguments, see `python runner.py --help`.

# How to run tests in cluster

Right now, the performance test pod is deployed to AKS but does not perform any tests by default. Tests have to
be triggered manually by connecting to the pod with `kubectl exec -it <pod> bash` and then executing one of the
commands described in the section for running locally.

# How to write test cases

Locust supports time tracking of HTTP calls out of the box with its `HttpLocust` class that can be extended.
See `search_test.py` on how to integrate it.

For other use cases, the `time_tracked` decorator can be used, e.g.

```python
@task
@time_tracked(request_type='pymongo')
def demo(self):
    do_something()
```
This will:
- track the duration of the method execution
- use the method name as name of the tracked operation in locust
- use the provided request_type as request type in locust
- map exceptions raised by the method to failed operations. Therefore, no exception handling
  should be done in the method

For an example, please refer to `pymongo_test.py`.  

# How to deploy the testsuite

## First steps:
1. Log into athena artifactory with your credentials.
2. Search for "ATTDATADANF_pypi" and generate a password.
3. Create your EXTRA_INDEX_URL with your generated key.

```console
export index_url_with_access=https://<YOUR_KEY>@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```

1. build and push docker image:
```console
docker build -t dlaregistrydev.azurecr.io/performance-testsuite:<version> . --network=host --no-cache --build-arg https_proxy=https://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access
az acr login --name dlaregistrydev
docker push dlaregistrydev.azurecr.io/performance-testsuite:${latest_version}
```

2. deploy to cluster
```console
kubectl apply -f kubernetes/deployment_dev.yaml
```