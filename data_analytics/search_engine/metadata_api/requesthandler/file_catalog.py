"""Search Engine and Metadata REST API bagfiles route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import datetime

from DANFUtils.constants import constants, REQ_STATUS_CODE_OK, REQ_STATUS_CODE_NOT_FOUND
from DANFUtils.tracing import tracing
from handlers.filecatalog import FileCatalogHandler
from model.file_catalog import File, FileCatalogGetByStatus, FileCatalogUpdateRequest, \
    FileCatalogUrlStatusUpdateRequest, FileCatalogUrlStatusUpdateRequestBody, FileInCreate, FileUrlInCreate
from requesthandler.base_request_handler import BaseRequestHandler

from handlers.auth import auth
from handlers.utils import utils


class FileCatalogRequestHandler(BaseRequestHandler):
    def initialize(self):
        self.file_catalog_handler = FileCatalogHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, GET, PUT')

    @auth.requires_entitlements(["openid"])
    def put(self, file_guid):
        tracing.start_task_in_context('file/{file_guid}/put',
                                      context_headers=self.request.headers)
        file_catalog_req = self.deserialize(self.request.body,
                                            FileInCreate)
        file = File(file_guid=file_guid, **file_catalog_req.dict())

        self.file_catalog_handler.create_catalog(file, update=False)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results="Created",
            status_code=constants['REQ_STATUS_CODE_RESOURCE_CREATED'])

    @auth.requires_entitlements(["openid"])
    def get(self, file_guid):
        tracing.start_task_in_context('file/{file_guid}/get',
                                      context_headers=self.request.headers)

        file_catalog_req = FileCatalogGetByStatus(file_guid=file_guid,
                                                  url_status=self.get_query_argument("url_status", "ALL"))
        if file_catalog_req.url_status == "ALL":
            file_catalog = self.file_catalog_handler.get_catalog_all_urls(file_guid)
        else:
            file_catalog = self.file_catalog_handler.get_catalog(file_catalog_req)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=file_catalog.dict(),
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def delete(self, file_guid: str) -> None:
        tracing.start_task_in_context('file/{file_guid}/delete', context_headers=self.request.headers)

        deleted_count = self.file_catalog_handler.delete_catalog(file_guid)

        tracing.end_task()
        if deleted_count == 0:
            utils.build_response(
                response=self,
                err="Not Found",
                results=None,
                status_code=REQ_STATUS_CODE_NOT_FOUND
            )
            return

        utils.build_response(response=self,
                             err=None,
                             results="Deleted",
                             status_code=REQ_STATUS_CODE_OK)


class FileCatalogUpdateRequestHandler(BaseRequestHandler):
    def initialize(self):
        self.file_catalog_handler = FileCatalogHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self, file_guid):
        file_catalog_req = self.deserialize(self.request.body,
                                            FileUrlInCreate)
        tracing.start_task_in_context('file/{file_guid}/urls/post',
                                      context_headers=self.request.headers)

        file_catalog_update = FileCatalogUpdateRequest(file_guid=file_guid, file_url=file_catalog_req)
        file_catalog_resp = self.file_catalog_handler.update_catalog(file_catalog_update, update=True)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results="File catalog urls updated",
                             status_code=constants["REQ_STATUS_CODE_OK"])


class FileCatalogUrlStatusRequestHandler(BaseRequestHandler):
    def initialize(self):
        self.file_catalog_handler = FileCatalogHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, PUT')

    @auth.requires_entitlements(["openid"])
    def put(self, file_guid, url_id):
        body = self.deserialize(self.request.body,
                                FileCatalogUrlStatusUpdateRequestBody)

        tracing.start_task_in_context('file/{file_guid}/urls/{url_id}/put',
                                      context_headers=self.request.headers)

        timestamp = datetime.datetime.now(tz=datetime.timezone.utc).timestamp() * 1000
        db_req = FileCatalogUrlStatusUpdateRequest(file_guid=file_guid,
                                                   url_id=url_id,
                                                   url_status=body.url_status,
                                                   last_changed=timestamp)
        file_catalog_resp = self.file_catalog_handler.update_catalog_url_status(db_req, update=True)
        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results="File catalog url status updated",
                             status_code=constants["REQ_STATUS_CODE_OK"])
