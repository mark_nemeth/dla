__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

__doc__ = '''Setup file'''

import json
import pathlib

import pkg_resources
from setuptools import setup

from config import config_handler as configurator
from config import ExceptionHandler

try:
    with open(file=configurator.get_value('constants_file_env'), mode="r", encoding='utf-8', errors='ignore') as f:
        constants = json.load(f)
except ValueError:
    raise ExceptionHandler('invalid_json_err')

try:
    with pathlib.Path('requirements.txt').open() as requirements_txt:
        install_requires = [str(requirement) for requirement \
                            in pkg_resources.parse_requirements(requirements_txt)
                            ]
except Exception as e:
    raise ExceptionHandler(code='invalid_requirements_err', debug_msg=e)

setup(
    name=constants['name'],
    version=constants['version'],
    dependency_links=constants['dependency_links'],
    install_requires=install_requires
)
