"""Job Record"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing
from DANFUtils.utils import validate_json

from handlers.utils import utils
from handlers.auth import auth
from handlers.file_check_handler import FileCheckHandler

from requesthandler.base import BaseRequestHandler


class FileCheck(BaseRequestHandler):

    def initialize(self):
        self.file_check_handler = FileCheckHandler()
        self.schema = self.load_json_schema('filecheck_post.schema')

    @auth.requires_scope(["openid"])
    def post(self):
        tracing.start_task_in_context(
            'filecheck/post', context_headers=self.request.headers)

        data = validate_json(self.request.body.decode('utf-8'), self.schema)

        delete_files = data.get('delete_files', False)
        search_res = self.file_check_handler.check_if_file_exist(data["paths"],
                                                                 delete_files)

        tracing.end_task()
        utils.build_response(
            response=self,
            results=search_res,
            status_code=constants["REQ_STATUS_CODE_OK"])
