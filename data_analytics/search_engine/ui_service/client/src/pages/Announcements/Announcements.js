import React from 'react';
import ErrorBanner from '../../components/ErrorBanner';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card'
import announcements from './Announcements.json';
import './Announcements.scss'

announcements.sort(function (a, b) {
  return new Date(b.date).getTime() - new Date(a.date).getTime();
});

function Announcements() {

  const projectAnnouncementCard = (announcement) => {
    return <Card key={announcement} className="announcement-card">
      <Card.Body>
        <Card.Title>
          <h5 className="d-inline-block card-title">{announcement.title}</h5>
          <h6 className="d-inline-block card-date float-right">{new Date(announcement.date).toDateString()}</h6>
        </Card.Title>
        <Card.Text>{announcement.body}</Card.Text>
      </Card.Body>
    </Card>

  };

  return (
    <Container className="announcements-page">
      <Row className="justify-content-md-center first-announcement">
        <Col xs={10}>
          {announcements.length > 0 && projectAnnouncementCard(announcements[0])}
        </Col>
      </Row>

      <Row className="justify-content-md-center">
        <Col xs={10}>
          <div className="card-columns">
            {announcements.filter((announcement, i) => {
              return i !== 0
            })
              .map((announcement) => (
                projectAnnouncementCard(announcement)
              ))}
          </div>
        </Col>
      </Row>
      <ErrorBanner/>
    </Container>
  );
}

export default Announcements;
