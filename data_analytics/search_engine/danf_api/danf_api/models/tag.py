"""Tag related models."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import List, ClassVar, Type, TypeVar, Optional, Union

from pydantic import BaseModel, Field, constr, conint

from .base import OpValue, BaseQueryParams

T = TypeVar("T", bound="Tag")
TT = TypeVar("TT", bound="TagFilterParams")


class Tag(BaseModel):
    RE_ID: ClassVar[str] = "^[^/:]+::[^/:]+$"

    namespace: str = Field(default=..., regex="^[^/:]+$")
    value: str = Field(default=..., regex="^[^/:]+$")

    @property
    def id(self):
        return f"{self.namespace}::{self.value}"

    @classmethod
    def from_id(cls: Type[T], tag_id: str) -> T:
        parts = tag_id.split("::", maxsplit=1)
        assert len(parts) == 2
        return cls(value=parts.pop(), namespace=parts.pop())

    class Config:
        frozen = True  # makes Tag hashable -> for membership testing (if a in b...)


class TagFilterParams(BaseModel):
    RE_ID_FORMAT: ClassVar[str] = "^[^/:]+::.+$"

    namespace: Optional[str] = Field(default=..., regex="^[^/:]+$")
    value: Optional[OpValue]

    @classmethod
    def __get_validators__(cls):
        """https://pydantic-docs.helpmanual.io/usage/types/#classes-with-__get_validators__"""
        yield cls.from_id_format

    @classmethod
    def from_id_format(cls: Type[TT], s: Union[str, Type[TT]]) -> TT:
        if isinstance(s, cls):
            return s
        parts = s.split("::", maxsplit=1)
        assert len(parts) == 2
        return cls(value=parts.pop(), namespace=parts.pop())


class TagQueryParams(BaseQueryParams):
    filter_params: TagFilterParams


class AddTagResponse(BaseModel):
    tag_id: constr(regex=Tag.RE_ID)


class ManyTags(BaseModel):
    tags: List[Tag]
    total_count: conint(ge=0)
