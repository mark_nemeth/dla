/*
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
*/

// https://athena.daimler.com/jira/browse/ATTDATA-2258
// update the processing_state

// update all successful
db.getCollection('bagfiles').update(
    {
        $and: [
            {"processing_state": "BEING_PROCESSED", "file_type": "ORIGINAL"},
            {"state": {$elemMatch: { "name": "post_ingest_extractor", "status": "SUCCESS" }}},
            {"state": {$elemMatch: { "name": "splitter", "status": "SUCCESS" }}},
            {"state": {$elemMatch: { "name": "stripper", "status": "SUCCESS" }}}
        ]
    },
    {
        $set: {"processing_state": "SUCCESSFUL"}
    },
    {
        multi: true
    }
);

// update all skipped
db.getCollection('bagfiles').update(
    {
        $and: [
            {"processing_state": "BEING_PROCESSED", "file_type": "ORIGINAL"},
            {"state": {$elemMatch: { "name": "post_ingest_extractor", "status": "SUCCESS" }}},
            {"state": {$elemMatch: { "name": "splitter", "status": "SKIPPED" }}},
            {"state": {$elemMatch: { "name": "stripper", "status": "SUCCESS" }}}
        ]
    },
    {
        $set: {"processing_state": "SUCCESSFUL"}
    },
    {
        multi: true
    }
);

// update all failed. (Attention: this will also match flows, that are still running, have some failed steps but not exceeded their limit yet)
// so better only target older files
db.getCollection('bagfiles').update(
    {
        $or: [
            {
                $and: [
                    {"processing_state": "BEING_PROCESSED", "file_type": "ORIGINAL"},
                    {"state": {$elemMatch: { "name": "post_ingest_extractor", "status": "FAILED" }}},
                    {"state": {$not: {$elemMatch: { "name": "post_ingest_extractor", "status": "SUCCESS" }}}}
                ]
            },
            {
                $and: [
                    {"processing_state": "BEING_PROCESSED", "file_type": "ORIGINAL"},
                    {"state": {$elemMatch: { "name": "splitter", "status": "FAILED" }}},
                    {"state": {$not: {$elemMatch: { "name": "splitter", "status": "SUCCESS" }}}}
                ]
            },
            {
                $and: [
                    {"processing_state": "BEING_PROCESSED", "file_type": "ORIGINAL"},
                    {"state": {$elemMatch: { "name": "stripper", "status": "FAILED" }}},
                    {"state": {$not: {$elemMatch: { "name": "stripper", "status": "SUCCESS" }}}}
                ]
            }
        ]
    },
    {
        $set: {"processing_state": "FAILED"}
    },
    {
        multi: true
    }
);