# Swagger-UI for API documentation
## How to add new api documentation (swagger.yaml file)


1. Create folder "YOUR_API_NAME" and store your yaml file at ./swagger_files/{on_azure || on_premise}:
2. Check "Dockerfile" in ./ and add lines for copying swagger ui and yaml files
"COPY ./dist/* /usr/share/nginx/html/YOUR_API_NAME/", "COPY ./swagger_files/YOUR_API_NAME/* /usr/share/nginx/html/YOUR_API_NAME/"

3. Open ./docker/nginx.conf file and add endpoint:

    location /YOUR_API_NAME/ {
      alias            /usr/share/nginx/html/YOUR_API_NAME/;
      expires 1d;

      location ~* \.(?:json|yml|yaml)$ {
        expires -1;

          include cors.conf;
      }
      
          include cors.conf;
    }

4.Build docker container. 
>  docker build -t dlaregistryon-azue.azurecr.io/swagger-server-on-azue:latest --build-arg build="on_azure"
>  docker build -t athena.daimler.com/dla_docker/swagger-server-on-prem:latest  --build-arg build="on_premise"

5. Add entrance for YOUR_API_NAME on index page:
   e.g. edit the HTML file:  
   > on_azure/default/index.html
  
    
## Tip for development
1. use the swagger editor `https://github.com/swagger-api/swagger-editor` 

