import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../../Table';
import TopicEntry from './TopicEntry';

describe('Bagfile Table TopicEntry', () => {

  it('should format topic number correct ', () => {
    const formattedResult = '4 Topics';
    const props = {topics: ['1', '2', '3', '4']};
    const wrapper = shallow(<TopicEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should create a disabled button when path not exists', () => {
    const props = {topics: null};
    const wrapper = shallow(<TopicEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});