import React, { useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import './Support.scss'

export function Support(props) {

  useEffect(() => {
    props.fetchDanfVersionInformation()
  }, [])

  return (
    <Container className="support-page">
      <Row className="justify-content-md-center">
        <Col xs={5}>
          <Card className="support-card">
            <Card.Body>
              <Card.Title>Support</Card.Title>
              <Card.Text>
                If you have any feature request or bug to report regarding DANF, please create a feature request or report
                in Jira, or alternatively reach out to us directly via Teams or email: <a href="mailto:gyorgy.katona@hu.bosch.com">Gyorgy Katona</a>.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={5}>
          <Card className="support-card">
            <Card.Body>
              <Card.Title>DANF Search Engine in L4</Card.Title>
              <Card.Text>
                DANF is the data processing pipeline / framework responsible for data processing, filtering and management. DANF Search Engine
                makes it possible to query and find data that is the most valuable to the user. DANF UI is not only the web interface for
                searching, but you will also find the most recent DANF updates here in the future.

                If you want to know more about how to access measurement data, please see
                <a href="https://inside-docupedia.bosch.com/confluence/x/0onNbw" target="_blank">this Docupedia page</a>.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row className="justify-content-md-center pt-4">
        <Col xs={5}>
          <Card className="support-card">
            <Card.Body>
              <Card.Title>Deployed Versions</Card.Title>
              <Card.Text>
                DANF-UI: {props.uiVersion}<br />
                DANF: {props.danfVersion}
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

