__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from config.config_handler import config_dict
from kubeflow.steps.common import BaseStep, attach_blobfuse_volume
from kubernetes.client.models import V1EnvVar

logger = logging.getLogger(__name__)


class StripperOp(BaseStep):
    NAME = "danf-stripper"
    DOCKER_IMG = config_dict.get_value(NAME)
    INPUT_MOUNT_BASE_PATH = "/data"
    OUTPUT_MOUNT_BASE_PATH = "/outputs"
    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "1Gi"
    MEMORY_LIMIT = "3Gi"
    DLA_CONVERTER = "True"
    TOPIC_PROFILES_MOUNT_PATH = "/topic_profiles"

    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               input_location,
               output_container,
               mount_id,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_location: path to the bagfile in the storage account
        :param output_container: location of the blob storage container where the data should be written to
        :param mount_id: used to generate unique mount names
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the Stripper operation
        """
        # Output should be in the same storage account as the input, but in a container inside the storageaccount called "output"
        # e.g.
        # input in: baseinfraabtraw01sadev/20200220/20200220_134051_wdd2221591a470493_cid_rc124_m1_sm_open_loop_0_original_test.bag
        # output in: baseinfraabtfltr01sadev/20200220/...

        output_location = input_location
        output_location.storage_account.replace("raw", "fltr")

        logger.info(
            f"StripperOp created for bagfile_guid {bagfile_guid}, "
            f"input_location {input_location.storage_account}, "
            f"output_location {output_location.storage_account} "
            f"and output_container {output_location.container}")

        # derive a 'virtual' sub directory within output container from the input container name
        output_container_sub_path = input_location.container

        step = StripperOp(
            flow_run_guid,
            bagfile_guid,
            input_location.file,
            vin='undefined'
        )

        if test_mode:
            step.enable_test_mode()

        step.set_image_pull_policy('Always') \
            .enable_tracing_integration() \
            .add_kubernetes_resources_for_pod(cls.CPU_REQUEST,
                                              cls.MEMORY_REQUEST, cls.CPU_LIMIT,
                                              cls.MEMORY_LIMIT) \
            .connect_search_api() \
            .connect_metadata_api() \
            .enable_proxy() \
            .enable_tracing_integration() \
            .add_authentication_env_variables()

        childbagfile_link_template = f"{output_location.storage_account}/{output_location.container}"

        step.add_env_variable(V1EnvVar(name="MODE", value="stripper")) \
            .add_env_variable(V1EnvVar(name="BAGFILE_PATH",
                                       value=f"{cls.INPUT_MOUNT_BASE_PATH}/{input_location.file}")) \
            .add_env_variable(
            V1EnvVar(name="OUTPUT_FOLDER", value=cls.OUTPUT_MOUNT_BASE_PATH)) \
            .add_env_variable(V1EnvVar(name="CHILDBAGFILE_LINK_TEMPLATE",
                                       value=childbagfile_link_template)) \
            .add_env_variable(
            V1EnvVar(name="DLA_CONVERTER", value=cls.DLA_CONVERTER))

        attach_blobfuse_volume(step,
                               cls.INPUT_MOUNT_BASE_PATH,
                               f"stripping-input-container-{mount_id}",
                               input_location.storage_account,
                               input_location.container)
        attach_blobfuse_volume(step,
                               cls.OUTPUT_MOUNT_BASE_PATH,
                               f"stripping-output-container-{mount_id}",
                               output_location.storage_account,
                               output_location.container,
                               False,
                               0)
        return step
