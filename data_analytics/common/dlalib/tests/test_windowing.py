# -*- coding: utf-8 -*-
__copyright__ = '''
COPYRIGHT: (c) 2017-2018 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

"""Tests for windowing module of dlalib."""

import unittest
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
from datetime import datetime, timedelta
import pandas as pd
from collections import OrderedDict

from .context import dlalib
from dlalib.windowing import (rows_for_value_change, is_in_windows, create_windows,
                              extract_windows_from_interval, extract_intervals, label_windows_udf,
                              explode_window_labels)


class WindowsTestCase(unittest.TestCase):

    def setUp(self):
        """Create spark session and set of simple data sets."""
        global spark
        global df1, df2, df3, df4, df5

        spark = (SparkSession
            .builder
            .getOrCreate())

        # zero transitions
        l1 = [(datetime(2018, 1, 1, 00, 00, 00), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 5), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 10), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 15), 'f1', 0)]
        df1 = spark.createDataFrame(data=l1, schema=["ts", "filename", "level"])
        # one transition
        l2 = [(datetime(2018, 1, 1, 00, 00, 00), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 5), 'f1', 2), \
              (datetime(2018, 1, 1, 00, 00, 10), 'f1', 2), \
              (datetime(2018, 1, 1, 00, 00, 15), 'f1', 0)]
        df2 = spark.createDataFrame(data=l2, schema=["ts", "filename", "level"])
        # two transitions
        l3 = [(datetime(2018, 1, 1, 00, 00, 00), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 5), 'f1', 2), \
              (datetime(2018, 1, 1, 00, 00, 10), 'f1', 2), \
              (datetime(2018, 1, 1, 00, 00, 15), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 20), 'f2', 0), \
              (datetime(2018, 1, 1, 00, 00, 25), 'f2', 2), \
              (datetime(2018, 1, 1, 00, 00, 30), 'f2', 2), \
              (datetime(2018, 1, 1, 00, 00, 35), 'f2', 2)]
        df3 = spark.createDataFrame(data=l3, schema=["ts", "filename", "level"])
        # two transitions: first file also ends in engaged mode
        l4 = [(datetime(2018, 1, 1, 00, 00, 00), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 5), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 10), 'f1', 2), \
              (datetime(2018, 1, 1, 00, 00, 15), 'f1', 2), \
              (datetime(2018, 1, 1, 00, 00, 20), 'f2', 0), \
              (datetime(2018, 1, 1, 00, 00, 25), 'f2', 2), \
              (datetime(2018, 1, 1, 00, 00, 30), 'f2', 2), \
              (datetime(2018, 1, 1, 00, 00, 35), 'f2', 0)]
        df4 = spark.createDataFrame(data=l4, schema=["ts", "filename", "level"])

        # two transitions: different outside values
        l5 = [(datetime(2018, 1, 1, 00, 00, 00), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 5), 'f1', 0), \
              (datetime(2018, 1, 1, 00, 00, 10), 'f1', 2), \
              (datetime(2018, 1, 1, 00, 00, 15), 'f1', 2), \
              (datetime(2018, 1, 1, 00, 00, 20), 'f2', 3), \
              (datetime(2018, 1, 1, 00, 00, 25), 'f2', 2), \
              (datetime(2018, 1, 1, 00, 00, 30), 'f2', 2), \
              (datetime(2018, 1, 1, 00, 00, 35), 'f2', 1), \
              (datetime(2018, 1, 1, 00, 00, 40), 'f2', 0), \
              (datetime(2018, 1, 1, 00, 00, 45), 'f2', 2), \
              (datetime(2018, 1, 1, 00, 00, 50), 'f2', 2), \
              (datetime(2018, 1, 1, 00, 00, 55), 'f2', 1)]
        df5 = spark.createDataFrame(data=l5, schema=["ts", "filename", "level"])

    def tearDown(self):
        pass

    def test_rows_for_value_change_no_trans(self):
        transitions = rows_for_value_change(df1, 'level', [0], [2])
        transitions_pd = transitions.toPandas()
        self.assertTrue(transitions_pd.empty)

    def test_rows_for_value_change_one_trans(self):
        transitions = rows_for_value_change(df2, 'level', [0], [2])
        transitions_pd = transitions.toPandas()
        transitions_compare = pd.DataFrame.from_dict(
            OrderedDict([('ts', (datetime(2018, 1, 1, 00, 00, 5))),
                         ('filename', ['f1']), ('level', [2])
                         ])
        )
        self.assertTrue(transitions_compare.equals(transitions_pd))

    def _convert_sort_reindex(self, df):
        return (df.toPandas()
            .sort_values("ts", ascending=True)
            .reset_index(drop=True))

    def test_rows_for_value_change_two_trans(self):
        transitions = rows_for_value_change(df3, 'level', [0], [2])
        transitions_pd = self._convert_sort_reindex(transitions)
        transitions_compare = pd.DataFrame.from_dict(
            OrderedDict([('ts', (datetime(2018, 1, 1, 00, 00, 5), datetime(2018, 1, 1, 00, 00, 25))),
                         ('filename', ['f1', 'f2']),
                         ('level', [2, 2])
                         ])
        )
        self.assertTrue(transitions_compare.equals(transitions_pd))

    def test_is_in_windows_zero_win(self):
        windows = [('f1', datetime(2018, 1, 2, 00, 00, 5), datetime(2018, 1, 2, 00, 00, 10))]
        rows_filtered = is_in_windows(df3, windows)
        rows_filtered_pd = rows_filtered.toPandas()
        self.assertTrue(rows_filtered_pd.empty)

    def test_is_in_windows_one_win(self):
        windows = [('f1', datetime(2018, 1, 1, 00, 00, 5), datetime(2018, 1, 1, 00, 00, 10))]
        rows_filtered = is_in_windows(df3, windows)
        rows_filtered_pd = self._convert_sort_reindex(rows_filtered)
        rows_filtered_compare = pd.DataFrame.from_dict(
            OrderedDict([('ts', (datetime(2018, 1, 1, 00, 00, 5), datetime(2018, 1, 1, 00, 00, 10))),
                         ('filename', ['f1', 'f1']),
                         ('level', [2, 2])
                         ])
        )
        self.assertTrue(rows_filtered_compare.equals(rows_filtered_pd))

    def test_create_windows_symmetrical(self):
        windows = create_windows(['f1'], [datetime(2018, 1, 1, 00, 00, 20)], 1000, "symmetrical")
        windows_compare = [('f1', datetime(2018, 1, 1, 00, 00, 19, 500000), datetime(2018, 1, 1, 00, 00, 20, 500000))]
        self.assertTrue(windows_compare == windows)

    def test_create_windows_left(self):
        windows = create_windows(['f1'], [datetime(2018, 1, 1, 00, 00, 20)], 1000, "left")
        windows_compare = [('f1', datetime(2018, 1, 1, 00, 00, 19), datetime(2018, 1, 1, 00, 00, 20))]
        self.assertTrue(windows_compare == windows)

    def test_create_windows_right(self):
        windows = create_windows(['f1'], [datetime(2018, 1, 1, 00, 00, 20)], 1000, "right")
        windows_compare = [('f1', datetime(2018, 1, 1, 00, 00, 20), datetime(2018, 1, 1, 00, 00, 21))]
        self.assertTrue(windows_compare == windows)

    def test_extract_windows_from_interval_no_skip(self):
        interval = ('f1', datetime(2018, 1, 1, 00, 00, 00), datetime(2018, 1, 1, 00, 00, 6))
        windows = extract_windows_from_interval(interval, window_length_ms=3000, overlap_ms=1000)
        windows_compare = [('f1', datetime(2018, 1, 1, 00, 00, 00), datetime(2018, 1, 1, 00, 00, 3)),
                           ('f1', datetime(2018, 1, 1, 00, 00, 2), datetime(2018, 1, 1, 00, 00, 5))]
        self.assertTrue(windows_compare == windows)

    def test_extract_windows_from_interval_skip(self):
        interval = ('f1', datetime(2018, 1, 1, 00, 00, 00), datetime(2018, 1, 1, 00, 00, 6))
        windows = extract_windows_from_interval(interval, window_length_ms=3000, overlap_ms=1000, skip_last_ms=2000)
        windows_compare = [('f1', datetime(2018, 1, 1, 00, 00, 00), datetime(2018, 1, 1, 00, 00, 3))]
        self.assertTrue(windows_compare == windows)

    def test_extract_windows_from_interval_reverse(self):
        interval = ('f1', datetime(2018, 1, 1, 00, 00, 00), datetime(2018, 1, 1, 00, 00, 8))
        windows = extract_windows_from_interval(interval, window_length_ms=3000, overlap_ms=1000,
                                                reverse_extraction=True)
        windows_compare = [('f1', datetime(2018, 1, 1, 00, 00, 5), datetime(2018, 1, 1, 00, 00, 8)),
                           ('f1', datetime(2018, 1, 1, 00, 00, 3), datetime(2018, 1, 1, 00, 00, 6)),
                           ('f1', datetime(2018, 1, 1, 00, 00, 1), datetime(2018, 1, 1, 00, 00, 4))]
        self.assertTrue(windows_compare == windows)

    def test_extract_windows_from_interval_reverse_skip(self):
        interval = ('f1', datetime(2018, 1, 1, 00, 00, 00), datetime(2018, 1, 1, 00, 00, 8))
        windows = extract_windows_from_interval(interval, window_length_ms=3000, overlap_ms=1000, skip_last_ms=1000,
                                                reverse_extraction=True)
        windows_compare = [('f1', datetime(2018, 1, 1, 00, 00, 4), datetime(2018, 1, 1, 00, 00, 7)),
                           ('f1', datetime(2018, 1, 1, 00, 00, 2), datetime(2018, 1, 1, 00, 00, 5)),
                           ('f1', datetime(2018, 1, 1, 00, 00, 0), datetime(2018, 1, 1, 00, 00, 3))]
        self.assertTrue(windows_compare == windows)

    def test_extract_intervals_no(self):
        intervals = extract_intervals(df1, column_name="level", values_outside_before=[0], values_inside=[2],
                                      values_outside_after=[0])
        self.assertTrue(intervals == [])

    def test_extract_intervals_one(self):
        intervals = extract_intervals(df2, column_name="level", values_outside_before=[0], values_inside=[2],
                                      values_outside_after=[0])
        intervals_compare = [('f1', datetime(2018, 1, 1, 0, 0, 5), datetime(2018, 1, 1, 0, 0, 15))]
        self.assertTrue(intervals == intervals_compare)

    def test_extract_intervals_two(self):
        intervals = extract_intervals(df3, column_name="level", values_outside_before=[0], values_inside=[2],
                                      values_outside_after=[0])
        intervals_compare = [('f1', datetime(2018, 1, 1, 0, 0, 5), datetime(2018, 1, 1, 0, 0, 15)),
                             ('f2', datetime(2018, 1, 1, 0, 0, 25), datetime(2018, 1, 1, 0, 0, 35))]
        self.assertTrue(intervals == intervals_compare)

    def test_extract_intervals_two_first_engaged(self):
        intervals = extract_intervals(df4, column_name="level", values_outside_before=[0], values_inside=[2],
                                      values_outside_after=[0])
        intervals_compare = [('f1', datetime(2018, 1, 1, 0, 0, 10), datetime(2018, 1, 1, 0, 0, 15)),
                             ('f2', datetime(2018, 1, 1, 0, 0, 25), datetime(2018, 1, 1, 0, 0, 35))]
        self.assertTrue(intervals == intervals_compare)

    def test_extract_intervals_two_different_outside_values(self):
        intervals = extract_intervals(df5, column_name="level", values_outside_before=[0, 1, 3], values_inside=[2],
                                      values_outside_after=[1])
        intervals_compare = [('f1', datetime(2018, 1, 1, 0, 0, 10), datetime(2018, 1, 1, 0, 0, 15)),
                             ('f2', datetime(2018, 1, 1, 0, 0, 25), datetime(2018, 1, 1, 0, 0, 35)),
                             ('f2', datetime(2018, 1, 1, 0, 0, 45), datetime(2018, 1, 1, 0, 0, 55))]

        self.assertTrue(intervals == intervals_compare)

    def test_label_windows_udf_string(self):
        windows = [('f1', datetime(2018, 1, 1, 00, 00, 5), datetime(2018, 1, 1, 00, 00, 15)),
                   ('f1', datetime(2018, 1, 1, 00, 00, 10), datetime(2018, 1, 1, 00, 00, 15))]
        df2_labeled = df2.withColumn('label', label_windows_udf(windows, 'string')(col("ts"), col("filename")))
        df2_labeled_pd = self._convert_sort_reindex(df2_labeled)
        df2_labeled_compare = pd.DataFrame.from_dict(
            OrderedDict([('ts', (datetime(2018, 1, 1, 00, 00, 0), datetime(2018, 1, 1, 00, 00, 5),
                                 datetime(2018, 1, 1, 00, 00, 10), datetime(2018, 1, 1, 00, 00, 15))),
                         ('filename', ['f1', 'f1', 'f1', 'f1']),
                         ('level', [0, 2, 2, 0]),
                         ('label', ['', '0,', '0,1,', '0,1,'])
                         ])
        )
        self.assertTrue(df2_labeled_compare.equals(df2_labeled_pd))

    def test_label_windows_udf_int_list(self):
        windows = [('f1', datetime(2018, 1, 1, 00, 00, 5), datetime(2018, 1, 1, 00, 00, 15)),
                   ('f1', datetime(2018, 1, 1, 00, 00, 10), datetime(2018, 1, 1, 00, 00, 15))]
        df2_labeled = df2.withColumn('label', label_windows_udf(windows, 'list')(col("ts"), col("filename")))
        df2_labeled_pd = self._convert_sort_reindex(df2_labeled)
        df2_labeled_compare = pd.DataFrame.from_dict(
            OrderedDict([('ts', (datetime(2018, 1, 1, 00, 00, 0), datetime(2018, 1, 1, 00, 00, 5),
                                 datetime(2018, 1, 1, 00, 00, 10), datetime(2018, 1, 1, 00, 00, 15))),
                         ('filename', ['f1', 'f1', 'f1', 'f1']),
                         ('level', [0, 2, 2, 0]),
                         ('label', [[], [0], [0, 1], [0, 1]])
                         ])
        )
        self.assertTrue(df2_labeled_compare.equals(df2_labeled_pd))

    def test_explode_window_labels(self):
        windows = [('f1', datetime(2018, 1, 1, 00, 00, 5), datetime(2018, 1, 1, 00, 00, 15)),
                   ('f1', datetime(2018, 1, 1, 00, 00, 10), datetime(2018, 1, 1, 00, 00, 15))]
        df2_labeled = df2.withColumn('label', label_windows_udf(windows, 'list')(col("ts"), col("filename")))
        df2_exploded = explode_window_labels(df2_labeled, 'label')
        df2_exploded_pd = self._convert_sort_reindex(df2_exploded)
        df2_exploded_compare = pd.DataFrame.from_dict(
            OrderedDict([('ts', (datetime(2018, 1, 1, 00, 00, 5), datetime(2018, 1, 1, 00, 00, 10),
                                 datetime(2018, 1, 1, 00, 00, 10), datetime(2018, 1, 1, 00, 00, 15),
                                 datetime(2018, 1, 1, 00, 00, 15))),
                         ('filename', ['f1', 'f1', 'f1', 'f1', 'f1']),
                         ('level', [2, 2, 2, 0, 0]),
                         ('label', [0, 0, 1, 0, 1])
                         ])
        )
        self.assertTrue(df2_exploded_compare.equals(df2_exploded_pd))
