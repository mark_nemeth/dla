"""BagTag related models."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import List

from pydantic import BaseModel, conint

from .tag import Tag


class ManyBagTags(BaseModel):
    bagtags: List[Tag]
    total_count: conint(ge=0)
