import {connect} from 'react-redux';
import {IngestHistory} from './IngestHistory';
import {fetchIngests, refresh} from "../../../redux/Ingest/IngestActions";

function mapStateToProps(state, ownProps) {
  const ingests = state.ingests || [];
  const error = state.error || false;
  const errorMessage = state.errorMessage || '';
  const showLoading = state.showLoading || false;

  return {ingests, error, errorMessage, showLoading};
}

export default connect(
    mapStateToProps,
    {
      fetchIngests,
      refresh
    }
)(IngestHistory);
