"""Search Engine REST API flow search route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing
from tornado.escape import recursive_unicode

from handlers.auth import auth
from handlers.flow_search_handler import FlowSearchHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler

logger = logging.getLogger(__name__)


class FlowRequestHandler(BaseRequestHandler):

    def initialize(self):
        self.search_handler = FlowSearchHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, GET')

    @auth.requires_entitlements(["openid"])
    def get(self):
        tracing.start_task_in_context('search/flow/get',
                                      context_headers=self.request.headers)

        # Interpret query arguments
        search_kwargs = self.search_handler.create_search_kwargs(
            recursive_unicode(self.request.query_arguments))

        logger.info(f"Executing search query: {search_kwargs}")
        res = self.search_handler.perform_search(**search_kwargs)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])