__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os
from argparse import ArgumentError

from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DB_Data_Handler


logger = logging.getLogger(__name__)


class BagFilesFlagHandler:

    def __init__(self):
        self.db_data_handler = DB_Data_Handler()

    def add_flag(self, bagfile_guid, flag_type):
        if flag_type is None:
            raise ArgumentError("Flag type must be given")

        if self._is_azure():
            logger.warning(f"Received add flag request: {flag_type}, for {bagfile_guid} in the Cloud, ignoring.")
            return

        if flag_type == "sync":
            guid = self._get_guid_for_document("sync_flag")
            array_dict = {"bagfile_guids": bagfile_guid}
            self.db_data_handler.push_to_array_in_documents([guid], [array_dict], create_if_missing=True)

        if flag_type == "delete":
            guid = self._get_guid_for_document("delete_flag")
            array_dict = {"bagfile_guids": bagfile_guid}
            self.db_data_handler.push_to_array_in_documents([guid], [array_dict], create_if_missing=True)

    def delete_flag(self, flag_type):
        if flag_type is None:
            raise ArgumentError("Flag type must be given")

        if self._is_azure():
            logger.warning(f"Received delete flag request: {flag_type}, in the Cloud, ignoring.")
            return

        if flag_type == "sync":
            guid = self._get_guid_for_document("sync_flag")
            array_dict = {"bagfile_guids": -1}
            self.db_data_handler.pop_from_array_in_documents([guid], [array_dict])

        if flag_type == "delete":
            guid = self._get_guid_for_document("delete_flag")
            array_dict = {"bagfile_guids": -1}
            self.db_data_handler.pop_from_array_in_documents([guid], [array_dict])

    def _get_guid_for_document(self, doc_type):
        search_kwargs = {'query': {'type': f'{doc_type}'}}
        docs = self.db_data_handler.search_documents(**search_kwargs)
        if len(docs) > 0:
            guid = docs[0]['guid']
        else:
            guid = generate_guid()
            self.db_data_handler.update_documents([guid], [{'type': doc_type, 'bagfile_guids': []}], True)
        return guid

    def _is_azure(self):
        mode = os.getenv("DANF_ENV", "dev")
        if mode in ["dev", "prod"]:
            return True
        else:
            return False
