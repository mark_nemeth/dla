"""Metadata REST API flows route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from handlers.auth import auth
from handlers.flows import FlowHandler
from handlers.utils import utils
from model.flows import CreateFlowRequest
from requesthandler.base_request_handler import BaseRequestHandler


class FlowsRequestHandler(BaseRequestHandler):

    def initialize(self):
        self._flow_handler = FlowHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('flows/post',
                                      context_headers=self.request.headers)

        create_flow_req = self.deserialize(self.request.body,
                                           CreateFlowRequest)

        new_flow = self._flow_handler.create_flow(create_flow_req)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=[new_flow.dict()],
            status_code=constants['REQ_STATUS_CODE_RESOURCE_CREATED'],
            location=f"/flows/{new_flow.guid}")


class FlowRequestHandler(BaseRequestHandler):

    _schema_guid = BaseRequestHandler.load_json_schema("guid.schema")

    def initialize(self):
        self._flow_handler = FlowHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET')

    @auth.requires_entitlements(["openid"])
    def get(self, guid):
        tracing.start_task_in_context('flows/{guid}/get',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        flow = self._flow_handler.get_flow(guid)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=[flow.dict()],
                             status_code=constants["REQ_STATUS_CODE_OK"])
