from DANFUtils.exceptions import RESTException

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from ContainerClient import ContainerClient
from DANFUtils.logging import logger
from ..config.config_handler import config_dict
import itertools


class UpdateHandler:

    def __init__(self):
        self.on_prem_client = ContainerClient()
        self.azure_client = ContainerClient()
        # TODO should be refactored to have the possibility to have "multiple
        #  constructors" to set the value for specific URLs while initializing
        self.azure_client.metadata_handler.IMPORT_URL = config_dict.get_value(
            "AZURE_METADATA_IMPORT_URL")

    def upsert_bagfiles_to_azure(self):
        updated_bagfiles = self.on_prem_client.get_unsynced_bagfile_guids()
        logger.info(
            f"Received Bagfile list: {updated_bagfiles} to update on Azure")
        bagfile_list = list()
        sequences_list = list()
        childbagfile_list = list()
        extractor_requests_list = list()

        for guid in itertools.islice(updated_bagfiles, 0,
                                     config_dict.get_value("sync_batch_size")):
            bagfile_list.append(self.on_prem_client.get_bagfile(guid))
            childbagfile_list.append(self._get_childbagfiles_by_parent(guid))
            sequences_list.append(self._get_sequences_by_parent(guid))
            extractor_requests_list.append(
                self._get_extractor_requests_by_parent(guid))

        sync_metadata = {"bagfiles": bagfile_list,
                         "sequences": sequences_list,
                         "childbagfiles": childbagfile_list,
                         "extractor_requests": extractor_requests_list}

        self.azure_client.import_metadata(sync_metadata)

        self._remove_flag(updated_bagfiles)

        logger.info(f"Updated Bagfiles with GUID: {updated_bagfiles} on Azure")

    def _get_childbagfiles_by_parent(self, guid):
        self.on_prem_client.search_childbagfiles({"parent_guid": str(guid)})

    def _get_sequences_by_parent(self, guid):
        self.on_prem_client.search_sequences({"bagfile_guid": str(guid)})

    def _get_extractor_requests_by_parent(self, guid):
        self.on_prem_client.search_extractor_requests({"bagfile_guid": str(guid)})

    def _remove_flag(self, updated_bagfiles):
        for guid in itertools.islice(updated_bagfiles, 0,
                                     config_dict.get_value("sync_batch_size")):
            # Catching exceptions while removing sync flags does not harm the sync process since we
            # would just sync these files once more and try to remove the flag again
            try:
                self.on_prem_client.remove_sync_flag()
            except RESTException as e:
                logger.warning(
                    f"Removing sync flag for guid {guid} did not work. {e}")
