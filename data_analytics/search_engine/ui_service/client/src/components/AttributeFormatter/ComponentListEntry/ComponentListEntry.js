import React, {useState} from 'react';
import FormControl from "react-bootstrap/FormControl";
import {isObjectValid} from "../../../utils/helperFunctions";
import {defaultEmptyValue} from "../../Table";
import ScrollList from "../../Utils/ScrollList/ScrollList";

export default function ComponentListEntry(props) {

  const [filteredComponentList, setFilteredComponentList] = useState(props.components);

  if (!isObjectValid(props.components)) return <span>{defaultEmptyValue}</span>;

  const componentItem = (item) => (
      <div className='file-list-item'>
        {item.name}{item.version && ": " + item.version}
      </div>
  );

  function onSearchComponent(event) {
    const filteredComponents = (props.components.filter(component =>
        component.name.toLowerCase().includes(event.target.value.toLowerCase())
        || component.version.toLowerCase().includes(event.target.value.toLowerCase())
    ));
    if (filteredComponents.length === 0) filteredComponents.push({name: 'No results found'});
    setFilteredComponentList(filteredComponents)
  }

  return (
      <div>
        <FormControl placeholder="Search available components..."
                     aria-label="Search query"
                     aria-describedby="basic-addon2"
                     onChange={onSearchComponent}
                     onClick={onSearchComponent}
        />
        <ScrollList showLoading={false}
                    listHeight={props.components.length === 0 ? '28px' : (props.components.length < 5 ? '150px' : '200px')}
                    itemList={filteredComponentList}
                    listItemComponent={componentItem}
        /></div>
  )
}