"""Handler class for cache topics"""
__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DB_Data_Handler


logger = logging.getLogger(__name__)


class CacheTopicsHandler:
    def __init__(self):
        self.db_data_handler = DB_Data_Handler()

    def cache_topics(self):
        logger.info('fetching all the topics')
        offset = 0
        topics = set()
        while True:
            search_kwargs = {'query': {}, 'limit': 20, 'offset': offset, 'sortby': 'guid'}
            docs = self.db_data_handler.search_bagfiles(**search_kwargs)
            if len(docs) == 0:
                break
            for doc in docs:
                if 'topics' in doc and len(doc['topics']) > 0:
                    topics.update(doc['topics'])
            offset += 20

        topics = list(topics)
        logger.info(f'number of unique topics: {len(topics)}')

        search_kwargs = {'query': {'type': 'topics_cache'}}
        cached_docs = self.db_data_handler.search_documents(**search_kwargs)
        if len(cached_docs) > 0:
            guid = cached_docs[0]['guid']
        else:
            guid = generate_guid()

        documents = [{'type': 'topics_cache', 'topics': topics}]
        self.db_data_handler.update_documents([guid], documents, True)
        return guid

    def delete_cache_document(self, guid):
        logger.info('deleting the cache document')
        guids = [guid]
        return self.db_data_handler.delete_documents(guids)

    def get_cache_document(self, guid):
        logger.info('get the cache document')
        guids = [guid]
        documents = self.db_data_handler.get_documents(guids)
        return documents
