"""Fallback Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config.constants import *
from extractors.utils import rospytime2int, timestamp2ms, ros_timestamp2ms
from extractors.base_interpreter import Interpreter

BVA_FALLBACK = 10
SECONDS_BEFORE_FALLBACK = 5
SECONDS_AFTER_FALLBACK = 5
FALLBACK_DURATION_TRESHOLD_MS = 2000
TASK_FALLBACK = 3


class FallbackInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.is_fallback = False
        self.was_fallback = False
        self.is_engaged = False
        self.last_fallback_event_ts = None
        self.bva = BVA_FALLBACK

    def feed_new_msg(self, topic, msg, t):
        fallback = {}

        if hasattr(msg, "actions"):
            self.is_fallback = (msg.actions[0].behavior_target.objective.chosen_task_id == TASK_FALLBACK)

        if hasattr(msg, "eSystemStatus"):
            self.is_engaged = all([msg.eSystemStatus.eSystemState == 2
                                  and msg.eSystemStatus.eDegradationLevel == 2 and
                                  msg.eSystemStatus.eOperationMode == 4])  # Engaged mode

        if self.is_fallback and not self.was_fallback and self.is_engaged:
            fallback["ts"] = ros_timestamp2ms(msg.header.stamp)
            if self.last_fallback_event_ts is None \
                    or (fallback["ts"] - self.last_fallback_event_ts > FALLBACK_DURATION_TRESHOLD_MS):
                fallback = self.calc_start_end_ts_snippet(fallback)
                self.event_list += [fallback]

        self.was_fallback = self.is_fallback

    @staticmethod
    def calc_start_end_ts_snippet(event):
        event["start_ts"] = int(event["ts"] - (SECONDS_BEFORE_FALLBACK * 1e3))
        event["end_ts"] = int(event["ts"] + (SECONDS_AFTER_FALLBACK * 1e3))
        event["ts"] = rospytime2int(event["ts"])
        return event
