"""Data Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import ssl
from datetime import timedelta

import tornado.httpserver
import tornado.ioloop
import tornado.web
from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.logging import configure_root_logger

from config.config_handler import config_dict
from config.constants import DANF_VERSION_TAG
from handlers.flow_scheduler import FlowScheduler
from handlers.storage_queue_ingestor import StorageQueueIngestor
from requesthandler.base import DummyRequestHandler
from requesthandler.delete_record import DeleteRecord
from requesthandler.file_check import FileCheck
from requesthandler.file_reprocessing import FileReprocessingRequestHandler
from requesthandler.testing import NewFileRequestHandler

logger = logging.getLogger(__name__)


def _init_periodic_task_handler() -> (StorageQueueIngestor, FlowScheduler):
    return StorageQueueIngestor(), FlowScheduler()


def _register_function_execution(funct, periodic_delay_in_sec):
    tornado.ioloop.IOLoop.instance().add_timeout(
        timedelta(seconds=periodic_delay_in_sec), funct)


def _periodically_triggered_file_ingestion():
    frequency = int(config_dict.get_value('data_ingest_frequency', 900))
    if frequency <= 0:
        # do not perform ingestion
        return
    try:
        file_ingestor.process_new_files()
    finally:
        _register_function_execution(
            _periodically_triggered_file_ingestion,
            frequency)


def _periodically_triggered_bagfile_processing():
    frequency = int(config_dict.get_value('data_processing_frequency', 600))
    if frequency <= 0:
        # do not perform processing
        return
    try:
        scheduler.schedule_next_flows()
    finally:
        _register_function_execution(_periodically_triggered_bagfile_processing, frequency)


def _make_app():
    return tornado.web.Application([
        (r"/test/?", DummyRequestHandler),
        (r"/delete_record/?", DeleteRecord),
        (r"/files/reprocess/?", FileReprocessingRequestHandler),
        (r"/testing/simulate-new-file/?", NewFileRequestHandler),
        (r"/testing/file-check/?", FileCheck)
    ])


if __name__ == '__main__':
    log_level = config_dict.get_value('log_level', 'INFO').upper()
    configure_root_logger(
        log_level=log_level,
        enable_azure_log_aggregation=danf_env().platform != Platform.LOCAL,
    )
    logger.setLevel(log_level)

    logger.info("#############################")
    logger.info("######## Data Handler #######")
    logger.info("#############################")
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    logger.info(f"DANF ENV: {config_dict.get_value('DANF_ENV')}")
    logger.info("starting server...")

    app = _make_app()

    http_port = config_dict.get_value("app_http_port", 0)
    https_port = config_dict.get_value("app_https_port", 8447)
    ssl_cert = config_dict.get_value("ssl_cert", None)
    ssl_key = config_dict.get_value("ssl_key", None)
    if http_port:
        app.listen(http_port)
        logger.info(f"app is listening on HTTP port: {http_port} ...")
    if https_port:
        https_server = tornado.httpserver.HTTPServer(
            app,
            ssl_options={
                'certfile': ssl_cert,
                'keyfile': ssl_key,
                'ssl_version': ssl.PROTOCOL_TLSv1_2
            })
        https_server.listen(https_port)
        logger.info(f"app is listening on HTTPS port: {https_port} ...")

    file_ingestor, scheduler = _init_periodic_task_handler()
    _periodically_triggered_file_ingestion()
    _periodically_triggered_bagfile_processing()
    tornado.ioloop.IOLoop.current().start()
