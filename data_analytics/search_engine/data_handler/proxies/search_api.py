"""Interface to the DANF search API"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


from ContainerClient.container_client import ContainerClient


class SearchApiProxy(object):

    def __init__(self):
        self._client = ContainerClient()

    def get_unprocessed_bagfiles(self, limit):
        """
        Returns a list of unprocessed bagfiles.

        :param limit: Maximum number of bagfiles to return
        :return: list of bagfiles
        """
        if limit <= 0:
            # passing limit of 0 to API will retrieve the default limit (50)
            return []

        bagfiles = self._client.search_bagfiles(
            query={'processing_state': 'NOT_PROCESSED'},
            limit=limit,
            sortby='-processing_priority'
        )
        return bagfiles

    def get_unprocessed_childbagfiles(self, limit):
        """
        Returns a list of unprocessed childbagfiles.

        :param limit: Maximum number of bagfiles to return
        :return: list of bagfiles
        """
        if limit <= 0:
            # passing limit of 0 to API will retrieve the default limit (50)
            return []
        bagfiles = self._client.search_childbagfiles(
            query={'processing_state': 'NOT_PROCESSED'},
            limit=limit
        )
        return bagfiles

    def get_bagfiles_by_path(self, path):
        return self._client.search_bagfiles({'any_link': path})

    def get_bagfile(self, guid: str, is_deleted: bool = False):
        bag_list = self._client.search_bagfiles(query={'guid': guid, 'is_deleted': is_deleted},
                                                limit=1)
        if bag_list:
            return bag_list[0]
        return None

    def get_childbagfile(self, guid: str, is_deleted: bool = False):
        childbagfile_list = self._client.search_childbagfiles(query={'guid': guid, 'is_deleted': is_deleted},
                                                              limit=1)
        if childbagfile_list:
            return childbagfile_list[0]
        else:
            return None

    def get_drive(self, guid: str, is_deleted: bool = False):
        drive_list = self._client.search_drivefiles(query={'guid': guid, 'is_deleted': is_deleted},
                                                    limit=1)
        if drive_list:
            return drive_list[0]
        return None

    def get_drivefiles_by_path(self, path, is_deleted=False):
        return self._client.search_drivefiles({'file_path': path, 'is_deleted': is_deleted})
