import React from 'react';
import featureToggle from '../../config/featureToggleHelper';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory from 'react-bootstrap-table2-filter';
import TablePagination from './pagination';
import paginationFactory, {
  PaginationProvider,
  PaginationTotalStandalone,
  SizePerPageDropdownStandalone
} from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import NameEntry from '../AttributeFormatter/Name/NameEntry';
import PathEntry from '../AttributeFormatter/Path/PathEntry';
import SizeEntry from '../AttributeFormatter/Size/SizeEntry';
import TopicListEntry from '../AttributeFormatter/Topic/Topic/TopicEntry';
import DateTimeEntry from '../AttributeFormatter/DateTime/DateTimeEntry';
import DurationEntry from '../AttributeFormatter/Duration/DurationEntry';
import StatusEntry from '../AttributeFormatter/Status/StatusEntry';
import ActionEntry from '../AttributeFormatter/Action/ActionEntry';
import {FileTypes} from '../Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import './Table.scss'

export function Table(props) {

  //region Table style
  const tableRowStripe = (row, rowIndex) => {
    const style = {};
    if (rowIndex % 2 !== 0) style.backgroundColor = '#fff';
    else style.backgroundColor = 'rgba(230, 230, 230, .25)';
    return style;
  };
  //endregion

  //region Table select
  const selectRow = {
    mode: 'checkbox',
    clickToSelect: true,
    selected: props.selectedRows,
    hideSelectColumn: !featureToggle.showSelectColumn,
    onSelect: onRowSelect,
    onSelectAll: onSelectAll
  };

  function onRowSelect(row, isSelect) {
    const newSelectedRows = (isSelect)
        ? props.selectedRows.concat([row.guid])
        : props.selectedRows.filter(x => x !== row.guid);
    props.selectTableEntry(newSelectedRows);
  }

  function onSelectAll(isSelect, rows) {
    if (isSelect) {
      props.selectTableEntry(rows.map(row => row.guid));
    } else {
      props.selectTableEntry([]);
    }
  }

  //endregion

  //region custom total text
  const currentPage = (props.offset / props.limit) + 1;
  const firstEntryNumb = currentPage * props.limit - props.limit + 1;
  const lastEntryNumb = (currentPage * props.limit > props.total) ? props.total : currentPage * props.limit;
  const paginationTotalRenderer = (from, to) => (
      <span className="react-bootstrap-table-pagination-total">
      Showing {firstEntryNumb} to {lastEntryNumb} of {props.total} Results
    </span>
  );
  //endregion

  //region sizePerPage buttons
  const sizePerPageRenderer = ({options, currSizePerPage}) => (
      <div className="btn-group sizePerPage-list" role="group">
        {
          options.map((option) => {
            const isSelect = currSizePerPage === `${option.page}`;
            return (
                <button key={option.text} type="button"
                        className={isSelect ? "sizePerPage-button-selected" : "sizePerPage-button-unselected"}
                        onClick={() => {
                          props.setPaginationLimit(option.page);
                          props.setPaginationOffset(0);
                          props.filterWithPagination();
                        }}>
                  {option.text}
                </button>
            );
          })
        }
      </div>
  );

  const options = {
    custom: true,
    sizePerPage: props.limit,
    sizePerPageRenderer,
    sizePerPageList: [{
      text: '15', value: 15
    }, {
      text: '50', value: 50
    }],
    totalSize: props.total,
    paginationTotalRenderer,
    hidePageListOnlyOnePage: true,
  };
  //endregion

  const NoDataIndication = () => {
    if (!props.loadingState && props.showTableMessage) {
      return (
          <div className="error-message-container">
            <div className="icon-search"/>
            {props.tableMessage}
          </div>
      );
    } else {
      return (
          <></>
      );
    }
  };

  function sortBy(columnID, order) {
    props.sortBy(((order === 'desc') ? '+' : '-') + columnID);
  }

  const columnFormatData = [{
    dataField: 'guid',
    text: 'ID',
    sort: true,
    hidden: true
  }, {
    dataField: 'file_name',
    text: 'Name',
    classes: 'table-name-column',
    formatter: (cell, row) => {
      return <NameEntry guid={row.guid} fileName={row.file_name} fileType={props.fileType}/>
    },
    headerStyle: () => {
      return {width: '30%'}
    },
    hidden: props.fileType === FileTypes.DRIVE
  }, {
    dataField: 'description',
    text: 'Description',
    classes: 'table-name-column',
    sort: true,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    formatter: (cell, row) => {
      let description = row.description || "-";
      return <NameEntry guid={row.guid} fileName={description} fileType={props.fileType}/>
    },
    headerStyle: () => {
      return {width: '30%'}
    },
    hidden: props.fileType !== FileTypes.DRIVE
  }, {
    dataField: props.displayedLink,
    text: 'Path',
    formatter: (cell, row) => {
      return <PathEntry filePath={cell} buttonVariant/>
    },
    headerStyle: () => {
      return {width: '6%'}
    },
    hidden: !featureToggle.showPathColumn
  }, {
    dataField: 'size',
    text: 'Size',
    formatter: (cell, row) => {
      return <SizeEntry size={cell}/>
    },
    sort: true,
    hidden: props.fileType === FileTypes.DRIVE,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    headerStyle: () => {
      return {width: '6%'}
    }
  }, {
    dataField: 'topics',
    text: 'Number of Topics',
    formatter: (cell, row) => {
      return <TopicListEntry topics={cell}/>
    },
    hidden: (props.fileType !== FileTypes.ORIGINALFILE && props.fileType !== FileTypes.ALWAYSONFILE),
    headerStyle: () => {
      return {width: '8%'}
    }
  }, {
    dataField: 'start',
    text: 'Start Time',
    sort: true,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    formatter: (cell, row) => {
      return <DateTimeEntry timestamp={cell}/>
    },
    headerStyle: () => {
      return {width: '15%'}
    }
  }, {
    dataField: 'end',
    text: 'End Time',
    hidden: true,
    formatter: (cell, row) => {
      return <DateTimeEntry timestamp={cell}/>
    },
    headerStyle: () => {
      return {width: '8%'}
    }
  }, {
    dataField: 'duration',
    text: 'Duration',
    sort: true,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    formatter: (cell, row) => {
      return <DurationEntry duration={cell}/>
    },
    headerStyle: () => {
      return {width: '6%'}
    }
  }, {
    dataField: 'vehicle_id_num',
    text: 'VID',
    sort: true,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    headerStyle: () => {
      return {width: '6%'}
    }
  }, {
    dataField: 'origin',
    text: 'Origin',
    headerStyle: () => {
      return {width: '6%'}
    },
    hidden: props.fileType === FileTypes.DRIVE
  }, {
    dataField: 'driven_by',
    text: 'Driven by',
    sort: true,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    headerStyle: () => {
      return {width: '6%'}
    },
    hidden: props.fileType !== FileTypes.DRIVE
  }, {
    dataField: 'driver',
    text: 'Driver',
    hidden: props.fileType !== FileTypes.DRIVE,
    sort: true,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    headerStyle: () => {
      return {width: '6%'}
    }
  }, {
    dataField: 'operator',
    text: 'Operator',
    hidden: props.fileType !== FileTypes.DRIVE,
    sort: true,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    headerStyle: () => {
      return {width: '6%'}
    }
  }, {
    dataField: 'processing_state',
    text: 'Status',
    hidden: props.fileType === FileTypes.CHILDBAGFILE || props.fileType === FileTypes.DRIVE,
    formatter: (cell, row) => {
      return <StatusEntry status={cell}/>
    },
    classes: 'table-status-column',
    sort: true,
    sortFunc: () => {
    },
    onSort: (field, order) => sortBy(field, order),
    headerStyle: () => {
      return {width: '8%'}
    }
  }, {
    dataField: 'actions',
    text: 'Actions',
    formatter: (cell, row) => {
      return <ActionEntry
          guid={row.guid} adminUser={props.adminUser} fileType={props.fileType}
          link={row['current_link'] || row['link']}
          showBanner={props.showError} reprocessCallback={props.reprocessBagfile} fileName={row['file_name']}
      />
    },
    headerStyle: () => {
      return {width: '3%'}
    }
  }];

  return (
      <PaginationProvider
          pagination={paginationFactory(options)}>
        {({paginationProps, paginationTableProps}) => (
            <div>
              <div className="table-container">
                <BootstrapTable
                    bootstrap4
                    keyField="guid"
                    data={props.currentFiles}
                    columns={columnFormatData}
                    selectRow={selectRow}
                    noDataIndication={() => <NoDataIndication/>}
                    filter={filterFactory()}
                    hover
                    condensed
                    striped
                    headerClasses="table-header-style"
                    rowClasses="table-row-style"
                    rowStyle={tableRowStripe}
                    {...paginationTableProps}
                />
              </div>

              <div className="pagination-footer">
                {props.currentFiles.length > 0 &&
                <SizePerPageDropdownStandalone {...paginationProps}/>}
                <PaginationTotalStandalone {...paginationProps}/>
                <TablePagination/>
              </div>

            </div>
        )
        }
      </PaginationProvider>
  )
}