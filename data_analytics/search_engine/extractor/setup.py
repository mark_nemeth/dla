"""Setup file"""
from setuptools import setup, find_packages

from config.constants import VERSION, EXTRACTOR_NAME

setup(name=EXTRACTOR_NAME,
      version=VERSION,
      packages=find_packages(),
      test_suite="tests",
      install_requires=[
          'numpy==1.18.1',
          'pycrypto',
          'gnupg',
          'ContainerClient>=0.6.102',
          'DANFUtils==0.2.29',
          'pydantic>=1.4',
          'pandas>=0.25.1',
          "pycryptodomex>=3.9.7",
          'psutil==5.7.3'
      ])
