import React, {Component} from 'react';
import {ColorScheme, ScaleTheme, RouteData} from '../BagfileMap/Models/RouteModels';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import {timestampConverter} from '../../utils/valueConverter'
import Tooltip from 'react-bootstrap/Tooltip';
import './BagfileTimeline.scss';

class BagfileTimeline extends Component {
  constructor(props) {

    super(props);
    this.timeLinewidth = React.createRef();
    this.route = this.props.route;
    this.routeLengthInKm = null;
    this.routeTimeInterval = null;
    this.routeDurationInMS = null;

    this.state = {
      width: window.innerWidth - 430,
      height: window.innerHeight
    }
  }

  updateDimensions = () => {
    this.setState({width: this.timeLinewidth.current.clientWidth, height: window.innerHeight});
  };

  componentDidMount() {
    this.setState({
      width: this.timeLinewidth.current.clientWidth,
      height: window.innerHeight
    });
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  renderTimeline() {
    if (this.props.route && this.props.route instanceof RouteData) {
      this.routeLengthInKm = this.props.route.getRouteLengthInKm();
      this.routeTimeInterval = this.props.route.getRouteTimeInterval();
      this.routeDurationInMS = this.props.route.getRouteDurationInMS();
    } else {
      return (
        <svg ref={this.timeLinewidth} width='100%' height='100%'>
          <rect width='100%' height='100%' strokeWidth={ScaleTheme.sizeS} stroke={ColorScheme.colorChildBagfileDefault}
                fill="#ffffff"/>
          <line x1={0} y1={0} x2={window.innerWidth - 430} y2={0} stroke={ColorScheme.colorChildBagfileDefault}
                strokeWidth={2}/>
          <line x1={0} y1={0} x2={0} y2='100%' stroke={ColorScheme.colorChildBagfileDefault} strokeWidth={2}/>
        </svg>
      );
    }

    const boxHeight = 2 * ScaleTheme.sizeS + 3 * ScaleTheme.sizeXS + 2 * 1.5 * ScaleTheme.sizeXS;

    return (
      <svg ref={this.timeLinewidth} width='100%' height={boxHeight}>
        <rect width='100%' height={boxHeight} strokeWidth={ScaleTheme.sizeS}
              stroke={ColorScheme.colorChildBagfileDefault} fill="#ffffff"/>
        <line x1={0} y1={0} x2={window.innerWidth - 430} y2={0} stroke={ColorScheme.colorChildBagfileDefault}
              strokeWidth={2}/>
        <line x1={0} y1={0} x2={0} y2={boxHeight} stroke={ColorScheme.colorChildBagfileDefault} strokeWidth={2}/>
        {this.renderChildBagfiles()}
        {this.renderEvents()}
      </svg>
    );
  }

  renderChildBagfiles() {
    if (!(this.props.route && this.props.route instanceof RouteData)) {
      return;
    }
    let childBagfileDurationsInMS = this.props.route.getChildBagfileDurationsInMS();
    if (!childBagfileDurationsInMS) {
      return;
    }
    let accessibleWidth = this.state.width - (2 * ScaleTheme.sizeS + (childBagfileDurationsInMS.length + 1) * ScaleTheme.sizeXS);
    let lines = [];
    let routeDurationCoveredByChildBagfiles = this.props.route.getDurationCoveredByChildBagfiles();
    let allTimeDisplayStrings = this.props.route.getAllMinutesSecondsString();


    let last = ScaleTheme.sizeXS + ScaleTheme.sizeS;
    for (let idx = 0; idx < this.props.route.length; idx++) {
      let distance = this.props.route[idx].getDurationlInMS();
      let timeDisplayString = allTimeDisplayStrings[idx];

      let x1 = last;
      let x2 = last + distance / routeDurationCoveredByChildBagfiles * accessibleWidth;
      let y1 = ScaleTheme.sizeS + ScaleTheme.sizeXS;
      let y2 = ScaleTheme.sizeS + ScaleTheme.sizeXS;

      let textHtml;
      let textLength = 40;

      if (idx === 0) {
        textHtml = <text key={Math.random()} textLength={textLength} x={x1} y={2 * y2 + ScaleTheme.sizeXXS}
                         fill="black">{timeDisplayString[0]}</text>;
      } else if (idx === childBagfileDurationsInMS.length - 1) {
        textHtml =
          <text key={Math.random()} textLength={textLength} x={x1 - textLength * 0.6} y={2 * y2 + ScaleTheme.sizeXXS}
                fill="black">{timeDisplayString[0]}</text>;
        let lastTextHtml = <text key={Math.random()} textLength={textLength}
                                 x={x1 - textLength + distance / routeDurationCoveredByChildBagfiles * accessibleWidth}
                                 y={2 * y2 + ScaleTheme.sizeXXS} fill="black">{timeDisplayString[1]}</text>;
        lines.push(lastTextHtml);
      } else {
        textHtml =
          <text key={Math.random()} textLength={textLength} x={x1 - textLength * 0.6} y={2 * y2 + ScaleTheme.sizeXXS}
                fill="black">{timeDisplayString[0]}</text>;
      }
      let html;
      if (this.props.selectedEvent && this.props.selectedEvent.length > 0)
        html = <line key={idx} x1={x1} y1={y1} x2={x2} y2={y2} stroke={ColorScheme.colorChildBagfileDefault}
                     strokeWidth={ScaleTheme.sizeS}/>;
      else if (this.props.selectedChildbagfileGuid === this.props.route[idx].guid)
        html = <line key={idx} x1={x1} y1={y1} x2={x2} y2={y2} stroke={ColorScheme.colorChildBagfileSelected}
                     strokeWidth={ScaleTheme.sizeS}/>;
      else
        html = <line key={idx} x1={x1} y1={y1} x2={x2} y2={y2} stroke={ColorScheme.colorChildBagfileDefault}
                     strokeWidth={ScaleTheme.sizeS}/>;
      lines.push(html);
      lines.push(textHtml);
      last += distance / routeDurationCoveredByChildBagfiles * accessibleWidth + ScaleTheme.sizeXS;
    }
    return (lines);
  }

  renderEvents() {
    if (this.props.route === null || this.props.route === undefined) {
      console.log('no route given');
      return;
    }
    let eventDurationsInMS = this.props.route.getDurationsFromChildBagfileStartToEventInMs();
    let durationCoveredByChildBagfiles = this.props.route.getDurationCoveredByChildBagfiles();

    if (!eventDurationsInMS) {
      return;
    }
    let accessibleWidth = this.state.width - (2 * ScaleTheme.sizeS + (this.props.route.length + 1) * ScaleTheme.sizeXS);

    let events = [];
    for (let idx = 0; idx < eventDurationsInMS.length; idx++) {
      let childBagfiles = eventDurationsInMS[idx];
      let childEvents = Object.entries(this.props.route[idx].metadata);
      let isEventInFile = false;

      // To know whether the event is present in this childbagfile way before coloring the events in the timeline
      for (let idx2 = 0; idx2 < childBagfiles.length; idx2++) {
        let coordinates = Object.entries(childEvents[idx2][1])[0][1][0];
        let lat = coordinates.latitude;
        let long = coordinates.longitude;
        isEventInFile = this.props.selectedEvent && this.props.selectedEvent.length > 0
          && this.props.selectedEvent[0] === lat && this.props.selectedEvent[1] === long;
        if (isEventInFile)
          break;
      }

      for (let idx2 = 0; idx2 < childBagfiles.length; idx2++) {
        let xPos = ScaleTheme.sizeS + ScaleTheme.sizeXS
          + childBagfiles[idx2] / durationCoveredByChildBagfiles * accessibleWidth + idx * ScaleTheme.sizeXS;
        let x1 = xPos;
        let x2 = xPos + ScaleTheme.sizeXS;
        let y1 = ScaleTheme.sizeS + ScaleTheme.sizeXXS + ScaleTheme.sizeXXS;
        let y2 = y1;
        let html;
        let coordinates = Object.entries(childEvents[idx2][1])[0][1][0];
        let lat = coordinates.latitude;
        let long = coordinates.longitude;
        let color = ColorScheme.colorEventDefault;

        if (this.props.selectedEvent && this.props.selectedEvent.length > 0 && this.props.selectedEvent[0] === lat
          && this.props.selectedEvent[1] === long) {
          color = ColorScheme.colorEventSelected;
        } else {
          if (isEventInFile)
            color = ColorScheme.colorEventDefault;
          else if (this.props.selectedChildbagfileGuid === this.props.route[idx].guid)
            color = ColorScheme.colorEventChildBagfileSelected
        }
        html = <OverlayTrigger placement="top" key={x2} overlay={<Tooltip
          className="tooltip-timestamp">{this.renderTooltipContent(this.props.route[idx].events[idx2])}</Tooltip>}>
          <line className="event-information" key={xPos} x1={x1} y1={y1} x2={x2} y2={y2} stroke={color}
                strokeWidth={ScaleTheme.sizeXS}/>
        </OverlayTrigger>;
        events.push(html);
      }
    }
    return (events);
  }

  renderTooltipContent(data) {
    return (
      <div style={{textAlign: 'left'}}>
        <div className='m-1'>{data.feature} - {timestampConverter(data.timestamp)}</div>
        <div className='m-1'>{data.childBagfileName}</div>
      </div>
    )
  }

  render() {
    return (
      <div className="sub_div">
        {this.renderTimeline()}
      </div>
    );
  }
}

export default BagfileTimeline;
