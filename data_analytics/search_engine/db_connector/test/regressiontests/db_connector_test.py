"""Unittests for DBConnector"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from DANFUtils.logging import logger
from DBConnector import DBConnector
from DBConnector.exceptions import IndexOverwriteException, BulkOperationException, InvalidQueryException
from .utils import generate_testdata, generate_seq
#from . import testconfig_local
from . import testconfig_local

# increase log level for tests
logger.set_log_level('debug')


def cleanup_db():
    connector = DBConnector(testconfig_local.db_hostname,
                            testconfig_local.db_port,
                            testconfig_local.db_name,
                            testconfig_local.db_user,
                            testconfig_local.db_key,
                            testconfig_local.db_protocol,
                            ssl=False)
    with connector as con:
        con.db['bagfiles'].delete_many({"is_test_data": 1})
        con.db['sequences'].delete_many({"is_test_data": 1})
        con.db['child_bagfiles'].delete_many({"is_test_data": 1})
        con.db['extractor_requests'].delete_many({"is_test_data": 1})


class TestBagFileIO(unittest.TestCase):
    def setUp(self):
        self.connector = DBConnector(testconfig_local.db_hostname,
                                     testconfig_local.db_port,
                                     testconfig_local.db_name,
                                     testconfig_local.db_user,
                                     testconfig_local.db_key,
                                     testconfig_local.db_protocol,
                                     ssl=False)

    def test_write_update_read_delete_single_bagfile(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )

        cleanup_db()

        new_vim = "1234"
        with self.connector as con:
            # write single bagfile, read it back by guid
            con.add_bagfiles([bag_1])
            res_bagfile = con.get_bagfiles([bag_1["guid"]])[0]
            self.assertDictEqual(bag_1, res_bagfile,
                                 'Write-read failed for single bagfile!')

            # update single bagfile, read it back by guid
            con.update_bagfiles([bag_1["guid"]], [{
                "vehicle_id_num": new_vim,
                "is_test_data": 1
            }])
            res_bagfile = con.get_bagfiles([bag_1["guid"]])[0]
            self.assertEqual(new_vim, res_bagfile["vehicle_id_num"],
                             'Update failed for single bagfile!')

            # Create-Update single bagfile, read it back by guid
            con.update_bagfiles([bag_2["guid"]], [{
                "vehicle_id_num": new_vim,
                "is_test_data": 1
            }],
                                create_if_missing=True)
            res_bagfile = con.get_bagfiles([bag_2["guid"]])[0]
            self.assertEqual(new_vim, res_bagfile["vehicle_id_num"],
                             'Create-Update failed for single bagfile!')

            # delete single bagfile by guid
            con.delete_bagfiles([bag_1["guid"]])
            bagfile_count = con.get_bagfiles([bag_1["guid"]]).count()
            self.assertEqual(0, bagfile_count,
                             'Delete failed for single bagfile!')

        cleanup_db()

    def test_write_update_read_delete_multiple_bagfiles(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )

        cleanup_db()
        new_vim = "1234"
        with self.connector as con:
            # write and read back multiple bagfiles
            con.add_bagfiles([bag_1, bag_2])
            res_bagfiles_two = list(
                con.get_bagfiles([bag_1["guid"], bag_2["guid"]]))
            self.assertCountEqual(
                [bag_1, bag_2], res_bagfiles_two,
                'Write-read failed for multiple bagfiles (find by guid)!')

            # update and read back multiple bagfiles
            con.update_bagfiles([bag_1["guid"], bag_2["guid"]],
                                [{
                                    "vehicle_id_num": new_vim,
                                    "is_test_data": 1
                                }] * 2)
            res_bagfiles = con.get_bagfiles([bag_1["guid"], bag_2["guid"]])
            self.assertEqual(new_vim, res_bagfiles[0]["vehicle_id_num"],
                             'Update failed for multiple bagfile!')
            self.assertEqual(new_vim, res_bagfiles[1]["vehicle_id_num"],
                             'Update failed for multiple bagfile!')

            # Create-update and read back multiple bagfiles
            con.update_bagfiles([bag_1["guid"], bag_2["guid"], bag_3["guid"]],
                                [{
                                    "vehicle_id_num": new_vim,
                                    "is_test_data": 1
                                }] * 3,
                                create_if_missing=True)
            res_bagfiles = con.get_bagfiles(
                [bag_1["guid"], bag_2["guid"], bag_3["guid"]])
            self.assertEqual(new_vim, res_bagfiles[0]["vehicle_id_num"],
                             'Create-Update failed for multiple bagfile!')
            self.assertEqual(new_vim, res_bagfiles[1]["vehicle_id_num"],
                             'Create-Update failed for multiple bagfile!')
            self.assertEqual(new_vim, res_bagfiles[2]["vehicle_id_num"],
                             'Create-Update failed for multiple bagfile!')

            # delete multiple bagfiles by guid
            con.delete_bagfiles([bag_1["guid"], bag_2["guid"]])
            bagfile_count = con.get_bagfiles([bag_1["guid"],
                                              bag_2["guid"]]).count()
            self.assertEqual(0, bagfile_count,
                             'Delete failed for multiple bagfiles!')

        cleanup_db()

    def test_write_search_delete_multiple_bagfiles(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()

        with self.connector as con:
            # write and search multiple child bagfiles
            con.add_bagfiles([bag_1, bag_2, bag_3])
            query = {
                "$or": [{
                    "guid": bag_1["guid"]
                }, {
                    "guid": bag_2["guid"]
                }, {
                    "guid": bag_3["guid"]
                }]
            }
            res_bag_2 = list(
                con.search_bagfiles(query,
                                    limit=2,
                                    offset=1,
                                    sortby=[("guid", -1)]))
            self.assertCountEqual([bag_2, bag_1], res_bag_2,
                                  'Search failed for multiple bagfiles!')

            # delete multiple bagfiles by guid
            con.delete_bagfiles([bag_1["guid"], bag_2["guid"], bag_3["guid"]])
            bag_count = con.get_bagfiles(
                [bag_1["guid"], bag_2["guid"], bag_3["guid"]]).count()
            self.assertEqual(0, bag_count,
                             'Delete failed for multiple bagfiles!')
        cleanup_db()

    def test_write_search_project_delete_multiple_bagfiles(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()

        with self.connector as con:
            # write and search multiple child bagfiles
            con.add_bagfiles([bag_1, bag_2, bag_3])
            query = {
                "$or": [{
                    "guid": bag_1["guid"]
                }, {
                    "guid": bag_2["guid"]
                }, {
                    "guid": bag_3["guid"]
                }]
            }
            projection = {"guid": 1}
            res_bag_2 = list(
                con.search_bagfiles(query,
                                    limit=2,
                                    offset=1,
                                    sortby=[("guid", -1)],
                                    projection=projection))

            self.assertCountEqual([{
                'guid': bag_1['guid']
            }, {
                'guid': bag_2['guid']
            }], res_bag_2, 'Search failed for multiple bagfiles!')

            # delete multiple bagfiles by guid
            con.delete_bagfiles([bag_1["guid"], bag_2["guid"], bag_3["guid"]])
            bag_count = con.get_bagfiles(
                [bag_1["guid"], bag_2["guid"], bag_3["guid"]]).count()
            self.assertEqual(0, bag_count,
                             'Delete failed for multiple bagfiles!')
        cleanup_db()

    def test_search_bad_query(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        with self.connector as con:
            query = {
                "$or": [{
                    "parent_guid": bag_1["guid"]
                }, {
                    "guid": bag_2["guid"]
                }, {
                    "guid": bag_3["guid"]
                }]
            }

            with self.assertRaises(InvalidQueryException) as context:
                con.search_bagfiles(query, limit=5, offset=1)

        cleanup_db()

    def test_guid_overwrite(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()

        new_vim = "1234"
        with self.connector as con:
            con.add_bagfiles([bag_1])
            with self.assertRaises(IndexOverwriteException) as context:
                con.update_bagfiles([bag_1["guid"]], [{
                    "vehicle_id_num": new_vim,
                    'guid': "12345"
                }])
        cleanup_db()


class TestSequenceIO(unittest.TestCase):
    def setUp(self):
        self.connector = DBConnector(testconfig_local.db_hostname,
                                     testconfig_local.db_port,
                                     testconfig_local.db_name,
                                     testconfig_local.db_user,
                                     testconfig_local.db_key,
                                     testconfig_local.db_protocol,
                                     ssl=False)

    def test_write_update_read_delete_single_sequence(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        sequence_update = [{
            "start": 1475272801234,
            "extractors": {
                "ext1": {
                    "metadata": {
                        "somedata": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }],
                        "disengagement": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }]
                    }
                },
                "ext2": {
                    "metadata": {
                        "somedata": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }]
                    }
                },
            },
            "is_test_data": 1
        }]
        with self.connector as con:
            # write single sequence, read it back by guid
            con.add_sequences([seq_1, seq_2])
            res_sequence = con.get_sequences([seq_1["guid"]])[0]
            self.assertDictEqual(seq_1, res_sequence,
                                 'Write-read failed for single sequence!')

            # update single sequence, read it back by guid
            con.update_sequences([seq_1["guid"]], sequence_update)
            res_sequence = con.get_sequences([seq_1["guid"]])[0]
            self.assertEqual(sequence_update[0]['start'],
                             res_sequence["start"],
                             'Update failed for single sequence!')
            self.assertEqual(
                "LogRequest topic unavailable", res_sequence["extractors"]
                ["ext1"]["metadata"]["somedata"][0]["error msg"],
                'Create-Update failed for single sequence!')
            self.assertEqual(
                "LogRequest topic unavailable", res_sequence["extractors"]
                ["ext1"]["metadata"]["disengagement"][2]["error msg"],
                'Create-Update failed for single sequence!')
            self.assertEqual(
                "LogRequest topic unavailable", res_sequence["extractors"]
                ["ext2"]["metadata"]["somedata"][0]["error msg"],
                'Create-Update failed for single sequence!')

            # create-update single sequence, read it back by guid
            con.update_sequences([seq_2["guid"]],
                                 [{
                                     "start": sequence_update[0]['start'],
                                     "is_test_data": 1
                                 }],
                                 create_if_missing=True)
            res_sequence = con.get_sequences([seq_2["guid"]])[0]
            self.assertEqual(sequence_update[0]['start'],
                             res_sequence["start"],
                             'Create-Update failed for single sequence!')
            # delete single sequence by guid
            con.delete_sequences([seq_1["guid"]])
            sequence_count = con.get_sequences([seq_1["guid"]]).count()
            self.assertEqual(0, sequence_count,
                             'Delete failed for single sequence!')

        cleanup_db()

    def test_write_update_read_delete_multiple_sequences(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        sequence_update = [{
            "start": 1475272801234,
            "extractors": {
                "ext1": {
                    "metadata": {
                        "somedata": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }],
                        "disengagement": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }]
                    }
                },
                "ext2": {
                    "metadata": {
                        "somedata": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }]
                    }
                },
            },
            "is_test_data": 1
        }]

        with self.connector as con:
            # write and read back multiple sequences
            con.add_sequences([seq_1, seq_2])
            res_sequences_two = list(
                con.get_sequences([seq_1["guid"], seq_2["guid"]]))
            self.assertCountEqual(
                [seq_1, seq_2], res_sequences_two,
                'Write-read failed for multiple sequences (find by guid)!')

            # update and read back multiple sequences
            con.update_sequences([seq_1["guid"], seq_2["guid"]],
                                 sequence_update * 2)
            res_sequences = con.get_sequences([seq_1["guid"], seq_2["guid"]])
            self.assertEqual(sequence_update[0]['start'],
                             res_sequences[0]["start"],
                             'Update failed for multiple sequence!')
            self.assertEqual(sequence_update[0]['start'],
                             res_sequences[1]["start"],
                             'Update failed for multiple sequence!')

            self.assertEqual(
                "LogRequest topic unavailable", res_sequences[0]["extractors"]
                ["ext1"]["metadata"]["somedata"][0]["error msg"],
                'Create-Update failed for single sequence!')
            self.assertEqual(
                "LogRequest topic unavailable", res_sequences[1]["extractors"]
                ["ext1"]["metadata"]["somedata"][0]["error msg"],
                'Create-Update failed for single sequence!')
            self.assertEqual(
                "LogRequest topic unavailable", res_sequences[0]["extractors"]
                ["ext1"]["metadata"]["disengagement"][0]["error msg"],
                'Create-Update failed for single sequence!')
            self.assertEqual(
                "LogRequest topic unavailable", res_sequences[1]["extractors"]
                ["ext1"]["metadata"]["disengagement"][0]["error msg"],
                'Create-Update failed for single sequence!')
            self.assertEqual(
                "LogRequest topic unavailable", res_sequences[0]["extractors"]
                ["ext2"]["metadata"]["somedata"][0]["error msg"],
                'Create-Update failed for single sequence!')
            self.assertEqual(
                "LogRequest topic unavailable", res_sequences[1]["extractors"]
                ["ext2"]["metadata"]["somedata"][0]["error msg"],
                'Create-Update failed for single sequence!')

            # update and read back multiple sequences
            con.update_sequences([seq_1["guid"], seq_2["guid"], seq_3["guid"]],
                                 sequence_update * 3,
                                 create_if_missing=True)
            res_sequences = con.get_sequences(
                [seq_1["guid"], seq_2["guid"], seq_3["guid"]])
            self.assertEqual(sequence_update[0]['start'],
                             res_sequences[0]["start"],
                             'Update failed for multiple sequence!')
            self.assertEqual(sequence_update[0]['start'],
                             res_sequences[1]["start"],
                             'Update failed for multiple sequence!')
            self.assertEqual(sequence_update[0]['start'],
                             res_sequences[2]["start"],
                             'Update failed for multiple sequence!')

            # delete multiple sequences by guid
            con.delete_sequences([seq_1["guid"], seq_2["guid"]])
            sequence_count = con.get_sequences([seq_1["guid"],
                                                seq_2["guid"]]).count()
            self.assertEqual(0, sequence_count,
                             'Delete failed for multiple sequences!')

        cleanup_db()

    def test_get_update_sequences_by_bagfile_guid_and_index(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()

        sequence_update = [{
            "start": 1475272801234,
            "extractors": {
                "ext1": {
                    "metadata": {
                        "somedata": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }],
                        "disengagement": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }]
                    }
                },
                "ext2": {
                    "metadata": {
                        "somedata": [{
                            "ts":
                            1558962227473,
                            "type":
                            "Unknown",
                            "error msg":
                            "LogRequest topic unavailable"
                        }]
                    }
                },
            },
            "is_test_data": 1
        }]

        with self.connector as con:
            # write  sequences, read them back by index
            con.add_sequences([seq_1, seq_2])
            sequence_tuples = [(seq_1["bagfile_guid"], seq_1["index"]),
                               (seq_2["bagfile_guid"], seq_2["index"])]
            res_sequence = list(con.get_sequences_by_index(sequence_tuples))
            self.assertCountEqual([seq_1, seq_2], res_sequence,
                                  'Index based read failed for sequences!')

            sequence_tuples = [(seq_1["bagfile_guid"], seq_1["index"]),
                               (seq_3["bagfile_guid"], seq_3["index"])]

            # update sequences, read them back by index
            con.update_sequences_by_index(sequence_tuples,
                                          sequence_update * 2,
                                          create_if_missing=True)
            res_sequences = list(con.get_sequences_by_index(sequence_tuples))
            self.assertEqual(sequence_update[0]['start'],
                             res_sequences[0]["start"],
                             'Update failed for multiple sequence!')
            self.assertEqual(sequence_update[0]['start'],
                             res_sequences[1]["start"],
                             'Update failed for multiple sequence!')

        cleanup_db()

    def test_get_sequences_by_bagfile_guid(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()

        with self.connector as con:
            # write  sequences, read them back by bagfile_guid
            con.add_sequences([seq_1, seq_3, seq_2])
            res_sequence = list(
                con.get_sequences_by_bagfile_guid([seq_2["bagfile_guid"]]))
            # check for ascending order
            self.assertDictEqual(
                seq_2, res_sequence[0],
                'Bagfile guid based read failed for sequences!')
            self.assertDictEqual(
                seq_3, res_sequence[1],
                'Bagfile guid based read failed for sequences!')

        cleanup_db()

    def test_index_overwrite(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        new_start = 1475272801234

        # test for GUID overwrite
        with self.connector as con:
            con.add_sequences([seq_1])
            with self.assertRaises(IndexOverwriteException) as context:
                con.update_sequences([seq_1["guid"]], [{
                    "start": new_start,
                    'guid': "12345"
                }])

        # test for index overwrite
        with self.connector as con:
            con.add_sequences([seq_2])
            with self.assertRaises(IndexOverwriteException) as context:
                con.update_sequences_by_index(
                    [(seq_2["bagfile_guid"], seq_2["index"])],
                    [{
                        "bagfile_guid": "1234",
                        'index': "1000"
                    }])

        cleanup_db()

    def test_write_delete_5000_sequences(self):
        cleanup_db()
        sequences = [generate_seq(x) for x in range(0, 5000)]
        try:
            with self.connector as con:
                con.add_sequences(sequences)
                con.db['sequences'].delete_many({"is_test_data": 1})
        except BaseException as e:
            self.fail("An error occured while adding/deleting 5000 sequences" +
                      str(e))
        cleanup_db()


class TestExtRequestIO(unittest.TestCase):
    def setUp(self):
        self.connector = DBConnector(testconfig_local.db_hostname,
                                     testconfig_local.db_port,
                                     testconfig_local.db_name,
                                     testconfig_local.db_user,
                                     testconfig_local.db_key,
                                     testconfig_local.db_protocol,
                                     ssl=False)

    def test_write_update_read_delete_single_ext_req(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        new_start = 1475272801234

        with self.connector as con:
            # write single ext req, read it back by guid
            con.add_extractor_requests([ext_1])
            res_ext = con.get_extractor_requests([ext_1["guid"]])[0]
            self.assertDictEqual(ext_1, res_ext,
                                 'Write-read failed for single ext-req!')

            # update single ext req, read it back by guid
            con.update_extractor_requests([ext_1["guid"]], [{
                "start": new_start,
                "is_test_data": 1
            }])
            res_ext = con.get_extractor_requests([ext_1["guid"]])[0]
            self.assertEqual(new_start, res_ext["start"],
                             'Update failed for single ext req!')

            # create-update single ext req, read it back by guid
            con.update_extractor_requests([ext_2["guid"]], [{
                "start": new_start,
                "is_test_data": 1
            }],
                                          create_if_missing=True)
            res_ext = con.get_extractor_requests([ext_2["guid"]])[0]
            self.assertEqual(new_start, res_ext["start"],
                             'Create-Update failed for single ext req!')

            # delete single ext req by guid
            con.delete_extractor_requests([ext_1["guid"]])
            req_count = con.get_extractor_requests([ext_1["guid"]]).count()
            self.assertEqual(0, req_count, 'Delete failed for single ext req!')

        cleanup_db()

    def test_write_update_read_delete_multiple_ext_reqs(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        new_start = 1475272801234

        with self.connector as con:
            # write and read back multiple ext reqs
            con.add_extractor_requests([ext_1, ext_2])
            res_ext_2 = list(
                con.get_extractor_requests([ext_1["guid"], ext_2["guid"]]))
            self.assertCountEqual(
                [ext_1, ext_2], res_ext_2,
                'Write-read failed for multiple ext reqs (find by guid)!')

            # update and read back multiple ext reqs
            con.update_extractor_requests([ext_1["guid"], ext_2["guid"]],
                                          [{
                                              "start": new_start,
                                              "is_test_data": 1
                                          }] * 2)
            res_ext = con.get_extractor_requests(
                [ext_1["guid"], ext_2["guid"]])
            self.assertEqual(new_start, res_ext[0]["start"],
                             'Update failed for multiple ext reqs!')
            self.assertEqual(new_start, res_ext[1]["start"],
                             'Update failed for multiple ext reqs!')

            # update and read back multiple ext reqs
            con.update_extractor_requests(
                [ext_1["guid"], ext_2["guid"], ext_3["guid"]],
                [{
                    "start": new_start,
                    "is_test_data": 1
                }] * 3,
                create_if_missing=True)
            res_ext = con.get_extractor_requests(
                [ext_1["guid"], ext_2["guid"], ext_3["guid"]])
            self.assertEqual(new_start, res_ext[0]["start"],
                             'Update failed for multiple ext reqs!')
            self.assertEqual(new_start, res_ext[1]["start"],
                             'Update failed for multiple ext reqs!')
            self.assertEqual(new_start, res_ext[2]["start"],
                             'Update failed for multiple ext reqs!')

            # delete multiple ext reqs by guid
            con.delete_extractor_requests([ext_1["guid"], ext_2["guid"]])
            ext_count = con.get_extractor_requests(
                [ext_1["guid"], ext_2["guid"]]).count()
            self.assertEqual(0, ext_count,
                             'Delete failed for multiple ext reqs!')

        cleanup_db()

    def test_write_search_delete_multiple_ext_reqs(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()

        with self.connector as con:
            # write and search multiple ext reqs
            con.add_extractor_requests([ext_1, ext_2, ext_3])
            query = {
                "$or": [{
                    "bagfile_guid": bag_2["guid"]
                }, {
                    "bagfile_guid": bag_3["guid"]
                }]
            }
            res_ext_2 = list(
                con.search_extractor_requests(query,
                                              limit=5,
                                              sortby=[("guid", -1)]))
            self.assertCountEqual(
                [ext_2, ext_3], res_ext_2,
                'Search failed for multiple extractor requests!')

            # delete multiple ext reqs
            con.delete_extractor_requests(
                [ext_1["guid"], ext_2["guid"], ext_3["guid"]])
            ext_count = con.get_extractor_requests(
                [ext_1["guid"], ext_2["guid"], ext_3["guid"]]).count()
            self.assertEqual(
                0, ext_count,
                'Delete failed for multiple extraction requests!')
        cleanup_db()

    def test_index_overwrite(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        new_start = 1475272801234

        # test for GUID overwrite
        with self.connector as con:
            con.add_extractor_requests([ext_1])
            with self.assertRaises(IndexOverwriteException) as context:
                con.update_extractor_requests([ext_1["guid"]], [{
                    "start": new_start,
                    'guid': "12345"
                }])

        cleanup_db()

    def test_aggregation(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        with self.connector as con:
            # write and search multiple ext reqs
            con.add_extractor_requests([ext_1, ext_2, ext_3])
            pipeline = [{
                "$match": {
                    "bagfile_guid": {
                        "$eq": "f649ddd0-b07a-40b1-9841-fb67b470f2b2"
                    }
                }
            }, {
                "$group": {
                    "_id": None,
                    "avg_bva": {
                        "$avg": "$bva"
                    },
                }
            }]
            avg_bva = list(
                con.aggregate_extractor_requests(pipeline))[0]['avg_bva']
            self.assertEqual(
                8.0, avg_bva,
                'Aggregation pipeline for extractor requests failed!')
        cleanup_db()

    # This error is no longer raised??
    # def test_exceeds_max_document_size(self):
    #     bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
    #     )
    #     cleanup_db()
    #     with self.connector as con:
    #         with self.assertRaises(BulkOperationException) as context:
    #             con.add_extractor_requests([ext_large])
    #             pass
    #     cleanup_db()


class TestChildBagfileIO(unittest.TestCase):
    def setUp(self):
        self.connector = DBConnector(testconfig_local.db_hostname,
                                     testconfig_local.db_port,
                                     testconfig_local.db_name,
                                     testconfig_local.db_user,
                                     testconfig_local.db_key,
                                     testconfig_local.db_protocol,
                                     ssl=False)

    def test_write_update_read_delete_single_child_bag(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        new_start = 1475272801234

        with self.connector as con:
            # write single child bag, read it back by guid
            con.add_child_bagfiles([child_1])
            res_child = con.get_child_bagfiles([child_1["guid"]])[0]
            self.assertDictEqual(child_1, res_child,
                                 'Write-read failed for single ext-req!')

            # update single child bag, read it back by guid
            con.update_child_bagfiles([child_1["guid"]], [{
                "start": new_start,
                "is_test_data": 1
            }])
            res_child = con.get_child_bagfiles([child_1["guid"]])[0]
            self.assertEqual(new_start, res_child["start"],
                             'Update failed for single child bag!')

            # create-update single child bag, read it back by guid
            con.update_child_bagfiles([child_2["guid"]], [{
                "start": new_start,
                "is_test_data": 1
            }],
                                      create_if_missing=True)
            res_child = con.get_child_bagfiles([child_2["guid"]])[0]
            self.assertEqual(new_start, res_child["start"],
                             'Create-Update failed for single child bag!')

            # delete single child bag by guid
            con.delete_child_bagfiles([child_1["guid"]])
            child_count = con.get_child_bagfiles([child_1["guid"]]).count()
            self.assertEqual(0, child_count,
                             'Delete failed for single child bag!')

        cleanup_db()

    def test_write_update_read_delete_multiple_child_bag(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        new_start = 1475272801234

        with self.connector as con:
            # write and read back multiple child bags
            con.add_child_bagfiles([child_1, child_2])
            res_child_2 = list(
                con.get_child_bagfiles([child_1["guid"], child_2["guid"]]))
            self.assertCountEqual(
                [child_1, child_2], res_child_2,
                'Write-read failed for multiple child bags (find by guid)!')

            # update and read back multiple child bags
            con.update_child_bagfiles([child_1["guid"], child_2["guid"]],
                                      [{
                                          "start": new_start,
                                          "is_test_data": 1
                                      }] * 2)
            res_child = con.get_child_bagfiles(
                [child_1["guid"], child_2["guid"]])
            self.assertEqual(new_start, res_child[0]["start"],
                             'Update failed for multiple child bags!')
            self.assertEqual(new_start, res_child[1]["start"],
                             'Update failed for multiple child bags!')

            # update and read back multiple child bags
            con.update_child_bagfiles(
                [child_1["guid"], child_2["guid"], child_3["guid"]],
                [{
                    "start": new_start,
                    "is_test_data": 1
                }] * 3,
                create_if_missing=True)
            res_child = con.get_child_bagfiles(
                [child_1["guid"], child_2["guid"], child_3["guid"]])
            self.assertEqual(new_start, res_child[0]["start"],
                             'Update failed for multiple child bags!')
            self.assertEqual(new_start, res_child[1]["start"],
                             'Update failed for multiple child bags!')
            self.assertEqual(new_start, res_child[2]["start"],
                             'Update failed for multiple child bags!')

            # delete multiple child bags by guid
            con.delete_child_bagfiles([child_1["guid"], child_2["guid"]])
            child_count = con.get_child_bagfiles(
                [child_1["guid"], child_2["guid"]]).count()
            self.assertEqual(0, child_count,
                             'Delete failed for multiple child bags!')

        cleanup_db()

    def test_write_search_delete_multiple_child_bag(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()

        with self.connector as con:
            # write and read back multiple child bags
            con.add_child_bagfiles([child_1, child_2])
            query = {
                "$or": [{
                    "bva": 7
                }, {
                    "bva": 8
                }],
                "version": "1.0",
                "$and": [{
                    "start": 1475272800000
                }, {
                    "end": 1475272800000
                }]
            }
            res_child_2 = list(con.search_child_bagfiles(query))
            self.assertCountEqual([child_1, child_2], res_child_2,
                                  'Search failed for multiple child bags!')

            # delete multiple child bags by guid
            con.delete_child_bagfiles([child_1["guid"], child_2["guid"]])
            child_count = con.get_child_bagfiles(
                [child_1["guid"], child_2["guid"]]).count()
            self.assertEqual(0, child_count,
                             'Delete failed for multiple child bags!')

        cleanup_db()

    def test_write_search_bad_query(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()

        with self.connector as con:
            # write and read back multiple child bags
            con.add_child_bagfiles([child_1, child_2])
            query = {
                "$or": [{
                    "bva": 7
                }, {
                    "bva": 8
                }],
                "version": "1.0",
                "$and": [{
                    "strt": 1475272800000
                }, {
                    "end": 1475272800000
                }]
            }
            res_child_2 = con.search_child_bagfiles(query)
            self.assertIsNone(
                res_child_2,
                'Invalid search test failed for multiple child bags!')

            # delete multiple child bags by guid
            con.delete_child_bagfiles([child_1["guid"], child_2["guid"]])
            child_count = con.get_child_bagfiles(
                [child_1["guid"], child_2["guid"]]).count()
            self.assertEqual(0, child_count,
                             'Delete failed for multiple child bags!')

        cleanup_db()

    def test_index_overwrite(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        new_start = 1475272801234

        # test for GUID overwrite
        with self.connector as con:
            con.add_child_bagfiles([child_1])
            with self.assertRaises(IndexOverwriteException) as context:
                con.update_child_bagfiles([child_1["guid"]], [{
                    "start": new_start,
                    'guid': "12345"
                }])

        cleanup_db()

    def test_metadata_exists(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3 = generate_testdata(
        )
        cleanup_db()
        query = {
            'metadata.ext1_4.stereo_cnn.bounding_box_labels.1': {
                '$exists': True
            }
        }

        with self.connector as con:
            con.add_child_bagfiles([child_1, child_2, child_3])
            result = con.search_child_bagfiles(query=query)
            self.assertEqual(child_1, result[0], 'Metadata query failed!')

        cleanup_db()


if __name__ == '__main__':
    unittest.main()
