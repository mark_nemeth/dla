import React from 'react';
import Spinner from 'react-bootstrap/Spinner';
import Table from 'react-bootstrap/Table';
import './ScrollList.scss'

function ScrollList(props) {

  const stripped = props.striped === undefined ? true : props.striped;
  const bordered = props.bordered === undefined ? true : props.bordered;
  const hover = props.hover === undefined ? true : props.hover;
  const inlineStyle = {
    height: (props.listHeight === undefined) ? '100px' : props.listHeight,
    backgroundColor: (stripped) ? '#FAFAFA' : null
  };

  return (
    <div className={props.variant + " scroll-table"} style={inlineStyle}>
      {props.showLoading ?
        <div className="center-in-parent">
          <Spinner animation="border"/>
        </div>
        :
        <Table striped={stripped} bordered={bordered} hover={hover} size="sm">
          <tbody>
          {props.itemList.length > 0 && props.itemList.map((item, index) => {
            return (
              <tr key={index}>
                <td className={props.itemVariant + " scroll-table-entry"}>
                  {props.listItemComponent(item)}
                </td>
              </tr>
            )
          })}
          </tbody>
        </Table>
      }
    </div>
  );
}

export default ScrollList;