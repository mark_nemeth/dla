import React, {useEffect} from 'react';
import Sidebar from '../../components/Sidebar';
import ActionBar from '../../components/ActionBar';
import SearchBox from '../../components/SearchBox';
import Table from '../../components/Table';
import ErrorBanner from '../../components/ErrorBanner';
import Spinner from 'react-bootstrap/Spinner';
import Button from 'react-bootstrap/Button';
import featureToggle from '../../config/featureToggleHelper';
import ByFileFilter from '../../components/Sidebar/FilterElements/ByFileFilter';
import './HomePage.scss';

export function HomePage(props) {

  useEffect(() => {
    document.title = 'DANF - Home';
    props.refresh();
    props.fetchFeatureToggles();
    props.fetchOriginalFileFilters();
    props.fetchAlwaysonFileFilters();
    props.fetchChildBagfileFilters();
    props.fetchUserData();
  }, []);

  useEffect(() => {
    if (props.isDriveViewActive && featureToggle.showDriveFilters) props.fetchDriveFilters();
  }, [props.isDriveViewActive]);

  const showLoading = props.error === false && props.showLoading;

  return (
    <div className="home-page">
      <Sidebar />
      <div className="home-page-content">
        <div className="home-page-action-bar">
          <ByFileFilter/>
          <Button className="refresh-button btn-info" onClick={props.refresh}>Refresh</Button>
          <SearchBox/>
          {featureToggle.showActionBar && <ActionBar/>}
        </div>
        <div className="home-page-table-view">
          <div className="results-info">{props.total} results found</div>
          <Button className="export-button" onClick={props.exportResults} disabled={props.total === 0}>Export results</Button>
        </div>
        <div className="home-page-table">
          {showLoading && (
            <div className="loading-spinner-container">
              <Spinner animation="border" variant="info"/>
            </div>
          )}
          <Table currentFiles={props.currentFiles}/>
        </div>
      </div>
      <ErrorBanner/>
    </div>
  );
}
