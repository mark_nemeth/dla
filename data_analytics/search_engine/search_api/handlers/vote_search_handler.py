__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from handlers.db_data_handler import DBDataHandler
from handlers.mongodb_query_handler import MongoDBHandler


class VoteSearchHandler:

    def __init__(self):
        self.db_data_handler = DBDataHandler()
        self.mongo_db_query_handler = MongoDBHandler()

    def create_search_kwargs(self, query_args):
        _kwrds_func = {
            'limit': lambda value: abs(int(value[0])),
            'offset': lambda value: abs(int(value[0]))
        }

        # Parsing functionality keywords with value to be appended in the 'query' dictionary
        _query_func = {
            'guid': lambda value: value[0],
            'bagfile_guid': lambda value: self.mongo_db_query_handler.create_field_query(value),
            'extracted_event_guid': lambda value: self.mongo_db_query_handler.create_field_query(value),
            'account_name': lambda value: self.mongo_db_query_handler.create_field_query(value),
            'quota_document_guid': lambda value: self.mongo_db_query_handler.create_field_query(value)
        }

        # Parsing functionality keywords with range to be appended in the '$and' dictionary
        _range_func = {
            'quota[gte]': lambda value: {'quota': {"$gte": abs(int(value[0]))}},
            'quota[lte]': lambda value: {'quota': {"$lte": abs(int(value[0]))}},
            'start_ts[gte]': lambda value: {'start_ts': {"$gte": abs(int(value[0]))}},
            'start_ts[lte]': lambda value: {'start_ts': {"$lte": abs(int(value[0]))}},
            'end_ts[gte]': lambda value: {'end_ts': {"$gte": abs(int(value[0]))}},
            'end_ts[lte]': lambda value: {'end_ts': {"$lte": abs(int(value[0]))}}
        }

        # Parsing functionality keywords ascending or descending to be appended in the 'sortby' dictionary
        whitelist = ['guid', 'bagfile_guid', 'extracted_event_guid', 'account_name', 'quota_document_guid', 'quota']

        _sortby_func = {
            'sortby': lambda value: self.mongo_db_query_handler.sortby_key_value(value, whitelist)
        }

        parsed_query = self.mongo_db_query_handler.parse_to_query(
            query_args,
            _kwrds_func,
            _query_func,
            _range_func,
            {},
            _sortby_func)

        return parsed_query

    def perform_search(self, **search_kwargs):
        return self.db_data_handler.search_votes(**search_kwargs)
