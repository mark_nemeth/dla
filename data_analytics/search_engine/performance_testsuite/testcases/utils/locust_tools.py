__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import functools
import sys
import time

from locust import events


def wrap_with_time_tracking(func, request_type):
    """Decorate the specified function with time tracking for locust
    performance measurements.
    """

    def wrapper(*args, **kwargs):
        start_time = time.time()
        try:
            ret = func(*args, **kwargs)
            # exhaust pymongo cursor
            result = [x for x in ret]
        except Exception as e:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type=request_type,
                                        name=f"{func.__name__}({args}, {kwargs})",
                                        response_time=total_time,
                                        exception=e)
        else:
            result_size = int(sys.getsizeof(result))
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type=request_type,
                                        name=f"{func.__name__}({args}, {kwargs})",
                                        response_time=total_time,
                                        response_length=result_size)

    return wrapper


def time_tracked(request_type):
    def wrap(func):
        """Decorate the specified function with time tracking for locust
        performance measurements.
        """
        @functools.wraps(func)
        def wrapped_f(*args, **kwargs):
            start_time = time.time()
            try:
                func(*args, **kwargs)
            except Exception as e:
                total_time = int((time.time() - start_time) * 1000)
                events.request_failure.fire(request_type=request_type,
                                            name=f"{func.__name__}",
                                            response_time=total_time,
                                            exception=e)
            else:
                total_time = int((time.time() - start_time) * 1000)
                events.request_success.fire(request_type=request_type,
                                            name=f"{func.__name__}",
                                            response_time=total_time,
                                            response_length=0)
        return wrapped_f
    return wrap
