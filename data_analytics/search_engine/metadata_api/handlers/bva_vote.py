__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


import logging
from typing import List

from DANFUtils.utils import utils as danfutils

from handlers.accounts import AccountHandler
from handlers.db_data_handler import DB_Data_Handler
from model.accounts import Account
from model.bva_quota import QuotaResponse, QuotaDocument, QuotaUsage
from model.bva_vote import VoteResponse, VoteDocument, VoteRequest, \
    VotePatchRequest
from model.exceptions import DANFApiError
from model.types import Guid


logger = logging.getLogger(__name__)


class BvaVoteHandler:

    def __init__(self):
        self.db_handler = DB_Data_Handler()
        self.account_handler = AccountHandler()

    def create_new_vote(self, create_vote_req: VoteRequest, account: Account) -> QuotaResponse:
        vote_size = self._calculate_vote_size(create_vote_req.bagfile_guid,
                                              create_vote_req.start_ts,
                                              create_vote_req.end_ts)
        active_quota = self.db_handler.get_quotas(True)[0]
        usage = self._get_account_quota_usage(active_quota, account.name)

        calculated_used_size = usage.total - vote_size

        new_quota = self._update_quota(account.name, calculated_used_size, vote_size)
        if not new_quota:
            raise DANFApiError(409,
                               'Vote size exceeds total quota')

        new_usage = new_quota.get_usage_of_account(account.name)

        vote = VoteDocument(
            **create_vote_req.dict(),
            guid=danfutils.generate_guid(),
            account_name=account.name,
            quota_document_guid=self._get_active_quota_guid(),
            quota=vote_size,
        )

        self.db_handler.add_vote(vote)

        logger.info(f"New vote {vote} added for account {account.name}")

        return QuotaResponse(started_at=new_quota.started_at,
                             ended_at=new_quota.ended_at,
                             total=new_usage.total,
                             used=new_usage.used)

    def get_votes(self, account: Account) -> List[VoteResponse]:
        active_votes = self.db_handler.get_votes(self._get_active_quota_guid())

        account_votes = filter(lambda vote: self._is_account_vote(vote, account.name), active_votes)

        return [self._create_vote_response(vote) for vote in account_votes]

    def delete_vote(self, guid: Guid, account: Account) -> QuotaResponse:
        vote = self.db_handler.get_vote_by_guid(guid)
        active_quota = self.db_handler.get_quotas(True)[0]

        if vote.quota_document_guid != active_quota.guid:
            raise DANFApiError(400,
                               "Vote can not be deleted anymore. Voting is already closed")

        # Just for validation
        self._get_account_quota_usage(active_quota, account.name)

        response = self.db_handler.delete_vote_by_guid(guid)
        if response.deleted_count == 0:
            raise DANFApiError("Vote already deleted, will not update quota")

        new_quota = self._revert_quota(account.name, vote.quota)

        if not new_quota:
            raise DANFApiError('Quota could not be modified')

        new_usage = new_quota.get_usage_of_account(account.name)

        logger.info(
            f"Vote with guid {guid} deleted for account {account.name}")

        return QuotaResponse(started_at=new_quota.started_at,
                             ended_at=new_quota.ended_at,
                             total=new_usage.total,
                             used=new_usage.used)

    def update_vote(self, guid: Guid, update_vote_req: VotePatchRequest, account: Account) -> QuotaResponse:
        vote_to_be_updated = self.db_handler.get_vote_by_guid(guid)
        active_quota = self.db_handler.get_quotas(True)[0]

        if vote_to_be_updated.quota_document_guid != active_quota.guid:
            raise DANFApiError(400,
                               "Vote can not be updated anymore. Voting is already closed")

        usage = self._get_account_quota_usage(active_quota, account.name)

        new_start_ts = update_vote_req.start_ts
        new_end_ts = update_vote_req.end_ts

        if not new_start_ts:
            new_start_ts = vote_to_be_updated.start_ts

        if not new_end_ts:
            new_end_ts = vote_to_be_updated.end_ts

        new_vote_size = self._calculate_vote_size(
            vote_to_be_updated.bagfile_guid, new_start_ts,
            new_end_ts)
        size_change = -(vote_to_be_updated.quota - new_vote_size)

        calculated_used_size = usage.total - size_change

        new_quota = self._update_quota(account.name, calculated_used_size, size_change)

        if not new_quota:
            raise DANFApiError(409,
                               'Vote size exceeds total quota limit, can not update vote.')

        updated_vote = self._update_vote(guid, update_vote_req.start_ts,
                                         update_vote_req.end_ts, new_vote_size)

        if not updated_vote:
            raise DANFApiError(404,
                               'Vote could not be modified. Vote does not exist anymore.')

        new_usage = new_quota.get_usage_of_account(account.name)

        logger.info(
            f"Vote with guid {guid} updated for account {account.name}. Update request: {update_vote_req}")

        return QuotaResponse(started_at=new_quota.started_at,
                             ended_at=new_quota.ended_at,
                             total=new_usage.total,
                             used=new_usage.used)

    def _get_active_quota_guid(self) -> str:
        active_quotas = self.db_handler.get_quotas(True)
        return active_quotas[0].guid

    def _update_vote(self, guid: Guid, start_ts: int, end_ts: int, new_vote_size: int) -> VoteDocument:
        filter_query = {'guid': guid}
        update_request = {'$set': {"start_ts": start_ts, 'end_ts': end_ts,
                                   'quota': new_vote_size}}

        return self.db_handler.update_vote(filter_query, update_request)

    def _update_quota(self, account_name: str, calculated_size: int, vote_size: int) -> QuotaDocument:
        filter_query = {
            'active': True,
            f'quota_usage_per_account.{account_name}.quota.used': {'$lte': calculated_size}
        }
        update_request = {
            '$inc': {f'quota_usage_per_account.{account_name}.quota.used': vote_size}
        }
        return self.db_handler.update_quota(filter_query, update_request)

    def _revert_quota(self, account_name: str, vote_size: int) -> QuotaDocument:
        filter_query = {'active': True}
        update_request = {'$inc': {
            f'quota_usage_per_account.{account_name}.used': -vote_size}}
        return self.db_handler.update_quota(filter_query,
                                            update_request)

    def _calculate_vote_size(self, bagfile_guid: Guid, start_ts: int, end_ts: int) -> int:
        bagfile = self.db_handler.get_bagfiles([bagfile_guid])[0]

        bagfile_size = bagfile.get('size')
        bagfile_duration = bagfile.get('duration')
        estimated_size_per_millisecond = 0

        if bagfile_duration != 0:
            estimated_size_per_millisecond = bagfile_size / bagfile_duration

        vote_duration = end_ts - start_ts

        return estimated_size_per_millisecond * vote_duration

    @staticmethod
    def _is_account_vote(vote: VoteDocument, account_name: str) -> bool:
        return vote.account_name == account_name

    @staticmethod
    def _create_vote_response(vote: VoteDocument) -> VoteResponse:
        return VoteResponse(guid=vote.guid,
                            bagfile_guid=vote.bagfile_guid,
                            extracted_event_guid=vote.extracted_event_guid,
                            account_name=vote.account_name,
                            start_ts=vote.start_ts,
                            end_ts=vote.end_ts,
                            quota=vote.quota)

    @staticmethod
    def _get_account_quota_usage(quota_document: QuotaDocument, account_name: str) -> QuotaUsage:
        usage = quota_document.quota_usage_per_account.get(account_name)
        if not usage:
            raise DANFApiError(
                404,
                f'Account {account_name} not found in quota document')
        return usage
