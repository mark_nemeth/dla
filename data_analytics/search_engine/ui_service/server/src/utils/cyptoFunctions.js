import crypto from 'crypto';
import getLogger from './logging';

const logger = getLogger(__filename)

export function encryptStringData(stringData, key) {
  try {
    const iv = Buffer.from(crypto.randomBytes(16), 'utf8');
    const cipher = crypto.createCipheriv('aes-256-gcm', key, iv);
    const encrypted = Buffer.concat([cipher.update(stringData, 'utf8'), cipher.final()]);
    const authTag = cipher.getAuthTag();
    return Buffer.concat([iv, authTag, encrypted]).toString('base64');
  } catch (e) {
    logger.error(e);
    return null;
  }
}

export function decryptStringData(encryptedData, key) {
  try {
    const bufferedData = Buffer.from(encryptedData, 'base64');
    const iv = bufferedData.slice(0, 16);
    const tag = bufferedData.slice(16, 32);
    const data = bufferedData.slice(32);
    const decipher = crypto.createDecipheriv('aes-256-gcm', key, iv);
    decipher.setAuthTag(tag);
    return decipher.update(data, 'base64', 'utf8') + decipher.final('utf8');
  } catch (e) {
    logger.error(e);
    return null;
  }
}