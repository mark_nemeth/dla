from abc import ABCMeta, abstractmethod


class AbstractExecutor():
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def get_DF(self, files, topics):
        if not isinstance(files, list) and all(isinstance(x, str) for x in files):
            raise Exception("The files argument must be a list of strings")
        if not isinstance(topics, list) and all(isinstance(x, str) for x in topics):
            raise Exception("The topics argument must be a list of strings")
