import React from 'react';
import {defaultEmptyValue} from '../../Table';
import {secondsToTimeConverter} from '../../../utils/valueConverter';
import {isNumberValid} from '../../../utils/helperFunctions';

export default function SecondTimeEntry(props) {

  if (!isNumberValid(props.time_in_seconds)) return <span>{defaultEmptyValue}</span>;

  const formattedEstimatedTimeOfArrival = secondsToTimeConverter(props.time_in_seconds);

  return (
      <span>{formattedEstimatedTimeOfArrival}</span>
  )
}