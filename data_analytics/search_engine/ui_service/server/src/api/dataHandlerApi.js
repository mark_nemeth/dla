import BaseApi from './BaseApi';
import configHandlerSingleton from '../configHandler';

export default class DataHandlerApi {

  static triggerReprocessFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getReprocessingAPIConfig();
      config.headers = authHeader;
      config.method = 'POST';
      config.body = JSON.stringify(req.body);
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }
}