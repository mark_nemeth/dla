#!/bin/bash
if [ $# != 2 ] || [ -z $index_url_with_access ]
  then
    echo "Argument missing!"
    echo "You have to pass 2 arguments: docker_app_name and docker_image_version!"
    echo "Export the index_url_with_access with your pypi packaging url"   
  else
    docker_app_name=$1
    docker_image_version=$2

    # login azures
    # docker login dlacontainerregistryacrdev.azurecr.io
    az acr login --name dlacontainerregistryacrdev

    # build docker image
    docker build -t dlacontainerregistryacrdev.azurecr.io/$docker_app_name:$docker_image_version --network host --no-cache --build-arg EXTRA_INDEX_URL=$index_url_with_access .

    # push docker image to registry
    docker push dlacontainerregistryacrdev.azurecr.io/$docker_app_name:$docker_image_version

    # replace template and create/update deployment on Kubernetes cluster
    sed "s/latest/$docker_image_version/g" kubernetes/danf-api_az_dev.yaml > kubernetes/danf_api_az_dev_tmp.yaml
    sed "s/danf-api:/${docker_app_name}:/g" kubernetes/danf_api_az_dev_tmp.yaml > kubernetes/danf_api_az_dev_tmp_1.yaml
    
    kubectl apply -f kubernetes/danf_api_az_dev_tmp_1.yaml

    rm -f kubernetes/danf_api_az_dev_tmp*.yaml
fi
