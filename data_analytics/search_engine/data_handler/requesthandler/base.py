__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import logging
import traceback

import DANFUtils.utils
import tornado.web
from DANFUtils.constants import constants
from DANFUtils.exceptions import JSONValidationError
from DANFUtils.tracing import tracing

from config.config_handler import config_dict
from handlers.auth import auth
from handlers.utils import utils
from model.exceptions import DANFApiError, RequestNotValidError

logger = logging.getLogger(__name__)

debug = 'debug' == config_dict.get_value('log_level')


class BaseRequestHandler(tornado.web.RequestHandler):
    """
    Base request handler to introduce a common set of default values /
    methods / functionality which can be overridden by the specific
    request handlers
    """

    def prepare(self):
        """
        Method to perform common initialization regardless of the
        request method.

        :return: None
        """
        logger.info(f"Incoming request: {self.request}, "
                    f"body={self.request.body.decode('utf-8')}")

    def set_default_headers(self):
        """
        Method to configure default headers for responses

        :return: None
        """
        self.set_header("access-control-allow-origin", "*")
        self.set_header(
            "Access-Control-Allow-Headers",
            "x-requested-with,access-control-allow-origin,authorization,content-type"
        )

    def options(self, *args):
        utils.build_response(response=self,
                             err=None,
                             results=None,
                             status_code=constants["REQ_STATUS_CODE_OK"])

    def log_exception(self, typ, value, tb):
        """
        Automatic logging of uncaught exceptions

        :return None
        """
        prefix = (f"API Error {value.status_code}"
                  if isinstance(value, DANFApiError)
                  else "Uncaught exception")
        logger.exception(f"{prefix} ({typ.__name__}): {value}")

    def write_error(self, status_code, **kwargs):
        """
        Automatic generation of error response for uncaught exceptions.

        :return: None
        """
        exc_info = kwargs.get('exc_info')

        if exc_info and hasattr(exc_info[1], 'status_code'):
            status_code = exc_info[1].status_code

        error_json = {
            'code': status_code,
            'reason': self._reason
        }

        tracing_attributes = dict()

        if exc_info:
            error_json['error_message'] = str(exc_info[1])

            trace = "".join(traceback.format_exception(*kwargs['exc_info']))
            tracing_attributes['fatalError'] = trace

            if debug:
                error_json['traceback'] = trace

        tracing.end_all_tasks(attributes=tracing_attributes)
        utils.build_response(self,
                             err=error_json,
                             status_code=status_code)

    @staticmethod
    def load_json_schema(schema_name):
        """Loads and parses the specified json schema.
        First tries to retrieve schema location from config_dict with
        schema_name as key.
        If no such config value exists, searches for schema with file name
        schema_name in the default schema location.

        :param schema_name: config_dict key pointing to schema location or
            schema file name
        :return: dict representing the schema
        """
        schema_location = config_dict.get_value(schema_name,
                                                f"schema/{schema_name}")
        # load schema
        try:
            with open(schema_location) as f:
                schema = f.read()
        except (IOError, OSError):
            logger.exception(f"Could not read ${schema_name} schema")
            raise

        # parse schema
        try:
            return json.loads(schema)
        except ValueError:
            logger.exception(f"Bad schema: "
                             f"unable to parse ${schema_name} schema")
            raise

    @staticmethod
    def validate_json(json_string, schema):
        """Validates the specified json_string against the specified schema
        and deserializes it to a dict.

        :param json_string: JSON string to validate and deserialize
        :param schema: JSON schema to validate against
        :return: dict holding the parsed deserialized
        :raises
            RequestNotValidError: if json_string does not conform to schema
        """
        try:
            return DANFUtils.utils.validate_json(json_string, schema)
        except JSONValidationError as e:
            raise RequestNotValidError(str(e)) from e


class DummyRequestHandler(BaseRequestHandler):

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'GET')

    @auth.requires_scope(["openid"])
    def get(self):
        self.write("pong...")
