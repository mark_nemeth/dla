__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from typing import List, Optional

from DANFUtils.utils.utils import get_timestamp, generate_guid

from handlers.accounts import AccountHandler
from handlers.db_data_handler import DB_Data_Handler
from model.accounts import Account
from model.bva_quota import QuotaResponse, QuotaDocument, QuotaPatchRequest, \
    QuotaUsage
from model.types import Guid


logger = logging.getLogger(__name__)


class BvaQuotaHandler:

    def __init__(self):
        self.account_handler = AccountHandler()
        self.db_handler = DB_Data_Handler()

    def create_new_quota(self) -> None:
        """Creates and persists a new QuotaDocument and marks it as active.
        The previously active QuotaDocument will be marked as inactive.
        """
        new_voting_quota = self._create_new_quota()

        quota = self._stop_current_voting_quota()
        if not quota:
            logger.debug('No active quota to deactivate')

        self.db_handler.add_quota(new_voting_quota)
        logger.info(f"Created new quota {new_voting_quota}")

    def get_quota_usage_for_account(self,
                                    only_active_period: bool,
                                    account: Account) -> List[QuotaResponse]:
        """Returns the quota usage of the given account.

        :param only_active_period: If true, only the usage in the current
            voting period is returned. Otherwise, usage from all voting
            periods is returned.
        :param account: Account of the user performing this action
        :return: List of quota usages
        """
        quotas = self.db_handler.get_quotas(only_active_period)
        usages = [self._create_quota_response(q, account.name) for q in quotas]
        logger.debug(f"Quota for account {account.name} is {usages}")
        return usages

    def update_quota(self,
                     guid: Guid,
                     update_quota_req: QuotaPatchRequest):
        raise NotImplementedError("Update quota is not implemented yet "
                                  "because out of scope for ATTDATA-2271")

    def _create_quota_response(self,
                               quota: QuotaDocument,
                               account_name: str) -> QuotaResponse:
        """Enrich quota usage with start and end time of the voting period.
        """
        usage = quota.get_usage_of_account(account_name)
        return QuotaResponse(started_at=quota.started_at,
                             ended_at=quota.ended_at,
                             total=usage.total,
                             used=usage.used)

    def _create_new_quota(self) -> QuotaDocument:
        """Creates new QuotaDocument and assigns available quota to all
        accounts based on their configuration."""
        accounts = self.account_handler.get_all()
        usage_per_account = dict()
        for account in accounts:
            usage = QuotaUsage(total=account.available_quota)
            usage_per_account.update({account.name: usage})
        return QuotaDocument(guid=generate_guid(),
                             active=True,
                             started_at=get_timestamp(),
                             quota_usage_per_account=usage_per_account)

    def _stop_current_voting_quota(self) -> Optional[QuotaDocument]:
        filter_query = {'active': True}
        update_request = {'$set': {'active': False}}
        return self.db_handler.update_quota(filter_query=filter_query,
                                            update_request=update_request)
