# -*- coding: utf-8 -*-
# Copyright 2018-2019 Streamlit Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import streamlit as st
import pandas as pd
from pandas.io.json import json_normalize
import numpy as np
import json
from datetime import datetime, timedelta
import plotly.graph_objects as go
import requests

SEARCH_API_BASE_URL = "http://localhost:8889"
# If mock data should be used:
# SEARCH_API_BASE_URL = None

FEATURE_DICT = {
    "Velocity": "vel",
    "Acceleration": "acc",
    "Jerk": "jerk",
    "Engagement State": "eng_state"
}

FEATURE_RANGE = {
    "Velocity": [0, 100, 10, 20],
    "Acceleration": [-10, 10, -3, 3],
    "Engagement State": [0, 14, 3, 4]
}


def main():
    st.title('Feature Search Demo')

    if not SEARCH_API_BASE_URL:
        # Create a text element and let the reader know the data is loading.
        data_load_state = st.text('Loading data...')
        data = load_data()
        # Notify the reader that the data was successfully loaded.
        data_load_state.text('Loading data...done!')
        data_load_state.text('')
    else:
        data = None

    st.sidebar.title("Select a mode")
    app_mode = st.sidebar.selectbox("",
        ["Filter bagfiles", "Show features of one bagfile"])
    if app_mode == "Filter bagfiles":
        run_bagfile_selector(data)
    elif app_mode == "Show features of one bagfile":
        run_the_app(data)


def run_bagfile_selector(data):
    st.sidebar.title("Build a Filter")
    filter_1 = st.sidebar.selectbox(
        'Select first feature',
        ('Velocity', 'Acceleration', 'Engagement State')
    )
    # Add a slider to the sidebar for feature 1:
    add_slider_1 = st.sidebar.slider(
        f"select {filter_1} range",
        FEATURE_RANGE[filter_1][0], FEATURE_RANGE[filter_1][1], (FEATURE_RANGE[filter_1][2], FEATURE_RANGE[filter_1][3]),
        key="slider1"
    )

    # operator = st.sidebar.selectbox(
    #     'Select operator',
    #     ('AND', 'OR')
    # )

    filter_2 = st.sidebar.selectbox(
        'Select second feature',
        ('Acceleration', 'Velocity', 'Engagement State', None)
    )

    # Add a slider to the sidebar for feature 2:
    if filter_2:
        add_slider_2 = st.sidebar.slider(
            f"select {filter_2} range",
            FEATURE_RANGE[filter_2][0], FEATURE_RANGE[filter_2][1], (FEATURE_RANGE[filter_2][2], FEATURE_RANGE[filter_2][3]),
            key="slider2"
        )
    else:
        add_slider_2 = None
    search_bagfile = st.sidebar.button('Search for bagfiles', key="search")

    if search_bagfile:
        st.text("The following filter is applied:")
        st.text(f"{filter_1} in range {add_slider_1} AND {filter_2} in range {add_slider_2}")

        if SEARCH_API_BASE_URL:
            df_filtered = filter_bagfiles_using_features(filter_1, add_slider_1, filter_2, add_slider_2)
        else:
            df_filtered = data[data["processing_state"] == "SUCCESSFUL"][['file_name', 'file_type', 'size', 'duration', 'vehicle_id_num', 'guid']].head(10)

        st.table(df_filtered)


# This is the main app app itself, which appears when the user selects "Run the app".
def run_the_app(data):
    bagfile_guid = None

    # Select bagfile based on guid
    st.sidebar.title("Select a Bagfile")
    bagfile_guid = st.sidebar.text_input("Insert the GUID of the Bagfile", "e.g. 0102f13e-0307-486c-b505-0b549cf7f0c0")
    submit_bagfile = st.sidebar.button('Load', key="load")
    if submit_bagfile:
        st.text(f"Selected bagfile GUID: {bagfile_guid}")

        if SEARCH_API_BASE_URL:
            df_selected_bagfile = fetch_bagfile_data(bagfile_guid)
        else:
            # use mock data
            df_selected_bagfile = data[data['guid'] == bagfile_guid]

        if df_selected_bagfile.empty:
            st.text("The bagfile GUID does not exist!")
        else:
            st.text(df_selected_bagfile.link.iloc[0])
            # show the map
            if SEARCH_API_BASE_URL:
                map_data = fetch_loc_data(bagfile_guid)
                # st.table(map_data)
            else:
                map_data = pd.DataFrame(
                    np.random.randn(50, 2) / [100, 200] + [48.6780230716, 8.98778106197],
                    columns=['lat', 'lon'])

            st.map(map_data)

    # Select features to plot
    st.sidebar.title("Plot features")
    # Select first Plot
    add_feature_1 = st.sidebar.selectbox(
        'Select a feature',
        ('Velocity', 'Acceleration', 'Jerk', 'Engagement State', None)
    )
    # Select another plot
    add_feature_2 = st.sidebar.selectbox(
        'Select another feature',
        (None, 'Velocity', 'Acceleration', 'Jerk', 'Engagement State')
    )
    submit_feature_1 = st.sidebar.button('Show Plots', key="plot_1")
    if submit_feature_1:
        if add_feature_1 is not None:
            st.markdown(add_feature_1)
            if SEARCH_API_BASE_URL:
                df_feature_1 = fetch_feature_data(bagfile_guid, add_feature_1)
            else:
                df_feature_1 = generate_feature_data(data, bagfile_guid)

            fig = go.Figure([go.Scatter(x=df_feature_1['datetime'], y=df_feature_1['feature'])])
            st.plotly_chart(fig)
        if add_feature_2 is not None:
            st.markdown(add_feature_2)
            if SEARCH_API_BASE_URL:
                df_feature_2 = fetch_feature_data(bagfile_guid, add_feature_2)
            else:
                df_feature_2 = generate_feature_data(data, bagfile_guid)

            fig = go.Figure([go.Scatter(x=df_feature_2['datetime'], y=df_feature_2['feature'])])
            st.plotly_chart(fig)


@st.cache
def load_data():
    with open("bagfiles_1.json") as json_file:
        data = json.load(json_file)
        df_bagfiles = pd.DataFrame.from_dict(data["results"], orient='columns')
    return df_bagfiles


def generate_feature_data(data, bagfile_guid):
    df_selected_bagfile = data[data['guid'] == bagfile_guid]
    start_ts = datetime.fromtimestamp(df_selected_bagfile.start.iloc[0]/1e3)
    end_ts = datetime.fromtimestamp(df_selected_bagfile.end.iloc[0]/1e3)
    # st.text(start_ts)
    # st.text(end_ts)

    def datetime_range(start, end, delta):
        current = start
        while current < end:
            yield current
            current += delta
    timestamps = [dt for dt in datetime_range(start_ts, end_ts, timedelta(seconds=1))]

    feature_values = np.cumsum(np.random.uniform(low=-0.4, high=0.5, size=(len(timestamps),)))
    df_feature = pd.DataFrame(list(zip(timestamps, feature_values)), columns=['datetime', 'feature'])
    return df_feature


def filter_bagfiles_using_features(filter_1, range_1, filter_2, range_2):
    if not filter_2:
        query_data = {
            f'feat.{FEATURE_DICT[filter_1]}[gte]': range_1[0],
            f'feat.{FEATURE_DICT[filter_1]}[lte]': range_1[1]
        }
    else:
        query_data = {
            f'feat.{FEATURE_DICT[filter_1]}[gte]': range_1[0],
            f'feat.{FEATURE_DICT[filter_1]}[lte]': range_1[1],
            f'feat.{FEATURE_DICT[filter_2]}[gte]': range_2[0],
            f'feat.{FEATURE_DICT[filter_2]}[lte]': range_2[1]
        }
    res = requests.get(url=SEARCH_API_BASE_URL+'/search/features', params=query_data)
    res_json = res.json()['results']
    df = json_normalize(res_json)
    if df.empty:
        return df
    else:
        return df[['bagfile_guid', 'start_index', 'end_index']]


def fetch_bagfile_data(bagfile_guid):
    query_data = {'guid': bagfile_guid}
    res = requests.get(url=SEARCH_API_BASE_URL+'/search/bagfile', params=query_data)
    res_json = res.json()['results']
    df = json_normalize(res_json)
    return df


def fetch_feature_data(bagfile_guid, feature):
    query_data = {'bagfile_guid': bagfile_guid, "features": FEATURE_DICT[feature]}
    res = requests.get(url=SEARCH_API_BASE_URL+'/filter/features', params=query_data)
    res_json = res.json()['results']
    timestamps = [datetime.fromtimestamp(ts/1e3) for ts in res_json['ts']]
    feature_values = res_json[FEATURE_DICT[feature]]
    df_feature = pd.DataFrame(list(zip(timestamps, feature_values)), columns=['datetime', 'feature'])
    return df_feature.sort_values(by='datetime', ascending=True)


def fetch_loc_data(bagfile_guid):
    query_data = {'bagfile_guid': bagfile_guid, "features": "loc"}
    res = requests.get(url=SEARCH_API_BASE_URL+'/filter/features', params=query_data)
    res_json = res.json()['results']
    if res_json is not None:
        loc = [x for x in res_json['loc'] if x is not None]
        df = json_normalize(loc)
        df_loc = df.coordinates.apply(pd.Series)
        df_loc.columns = ['lon', 'lat']
    else:
        df_loc = None
    return df_loc


if __name__ == "__main__":
    main()