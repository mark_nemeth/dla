import rosbag

bag = rosbag.Bag('../bagfiles/20180227_151542_driving_in_lane.bag')

topics = bag.get_type_and_topic_info()[1].keys()

print(topics)

#topic = '/CalService/instance_0/GPSInfoInterface'

for i in range(len(topics)):
  filename = '../topics2018/Out'+topics[i].replace("/","_")+'.txt'
  print(topics[i])

  with open(filename, 'w') as f:
    for msg in bag.read_messages(topics=[topics[i]]):
      #print(msg.message)
      print >> f, msg

  f.close()

bag.close()    
