# Extractor for disengagement and auonomous miles driven

### Run in Docker:
Build docker image:
```console
sudo docker build -t parse_disengagement:test02 . --network host --no-cache --build-arg EXTRA_INDEX_URL=https://njuwfxcpjy7wz7zynoxyns3djob5655googzdpb4aue5dxt4jgzq@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/ --build-arg https_proxy=https://localhost:3128
```
then run it:
```console
sudo docker run -e BAGFILE_GUID="bagfile_guid" -e BAGFILE_PATH="path_to_bagfile.bag" parse_disengagement:test02
```
