import {connect} from 'react-redux';
import {filterByVIDs} from '../../../../redux/Home/HomeActions';
import {ByVIDFilter} from "./ByVIDFilter";
import {FileTypes} from "../ByFileFilter/ByFileFilter";

function mapStateToProps(state, ownProps) {
  let vehicleIDList;
  switch (state.fileType) {
    case FileTypes.ORIGINALFILE:
      vehicleIDList = state.filterOptions.originalFileVehicleIdList || [];
      break;
    case FileTypes.ALWAYSONFILE:
      vehicleIDList = state.filterOptions.alwaysonFileVehicleIdList || [];
      break;
    case FileTypes.CHILDBAGFILE:
      vehicleIDList = state.filterOptions.childBagfileVehicleIdList || [];
      break;
    case FileTypes.DRIVE:
      vehicleIDList = state.filterOptions.driveVehicleIdList || [];
      break;
  }
  const selectedVidList = state.filters.vehicle_id_num || [];
  return {vehicleIDList, selectedVidList};
}

export default connect(
  mapStateToProps,
  {filterByVIDs}
)(ByVIDFilter);