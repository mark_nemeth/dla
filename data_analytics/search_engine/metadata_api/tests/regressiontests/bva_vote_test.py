__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from copy import deepcopy

import requests
from DANFUtils.utils import generate_deterministic_guid

from tests.regressiontests.testutils import get_testdb_connector


# testdata
base_url = 'http://localhost:8887'
vote_url = base_url + '/bva/vote/'

test_quota_document_active = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "active": True,
    "started_at": 1577971500002,
    "ended_at": 1577971500003,
    "quota_usage_per_account": {
        "workstream_id1": {
            "total": 10000,
            "used": 0
        }
    }
}

test_quota_document_inactive = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "active": False,
    "started_at": 1577971500001,
    "ended_at": 1577971500000,
    "quota_usage_per_account": {
        "workstream_id1": {
            "total": 10000,
            "used": 100
        }
    }
}

test_vote_document = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "bagfile_guid": generate_deterministic_guid(),
    "extracted_event_guid": generate_deterministic_guid(),
    "account_name": 'workstream_id1',
    "quota_document_guid": 'tbr',
    "start_ts": 1577971500002,
    "end_ts": 1577971500004,
    "quota": 20
}

test_credentials = {
    "version": "0.0.1-testdata",
    "guid": generate_deterministic_guid(),
    "account_name": "workstream_id1",
    "account_key_hash": "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
    "available_quota": 20000,
}

test_bagfile = {
    'version': "1.0",
    'guid': generate_deterministic_guid(),
    'size': 1000,
    'duration': 100
}

header = {'X-account-key': 'test'}


def prepare_db():
    with get_testdb_connector() as con:
        con.db['bva_quotas'].delete_many(
            {'version': test_quota_document_active['version']})
        con.db['bva_votes'].delete_many(
            {'version': test_vote_document['version']})
        con.db['accounts'].delete_many(
            {'version': test_credentials['version']})
        con.db['bagfiles'].delete_many(
            {'version': test_bagfile['version']})


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['bva_quotas'].delete_many(
            {'version': test_quota_document_active['version']})
        con.db['bva_votes'].delete_many(
            {'version': test_vote_document['version']})
        con.db['accounts'].delete_many(
            {'version': test_credentials['version']})
        con.db['bagfiles'].delete_many(
            {'version': test_bagfile['version']})


class TestBvaGET(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_GET_votes_should_return_all_votes_in_the_current_voting_round(
            self):
        test_vote = deepcopy(test_vote_document)
        test_vote['quota_document_guid'] = test_quota_document_active.get(
            'guid')

        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_active)
            con.db['bva_votes'].insert_one(test_vote)
            con.db['accounts'].insert_one(test_credentials)

        res = requests.get(url=vote_url, headers=header)

        self.assertEqual(200, res.status_code)
        self.assertEqual(1, len(res.json()['results']))
        self.assertEqual(20, res.json()['results'][0].get('quota'))


class TestBvaPOST(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_POST_vote_should_create_a_new_vote_and_update_quota(self):
        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_active)
            con.db['accounts'].insert_one(test_credentials)
            con.db['bagfiles'].insert_one(test_bagfile)

        vote = {
            "bagfile_guid": test_bagfile.get('guid'),
            "extractor_id": 'ext_1',
            "start_ts": 1577971500002,
            "end_ts": 1577971500003
        }
        res = requests.post(url=vote_url, headers=header, json=vote)
        self.assertEqual(201, res.status_code)
        self.assertEqual(10, res.json()['results'].get('used'))

    @unittest.skip("test-case is broken")
    def test_POST_vote_should_not_create_a_new_vote_and_update_quota(self):
        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_active)
            con.db['accounts'].insert_one(test_credentials)
            con.db['bagfiles'].insert_one(test_bagfile)

        vote = {
            "bagfile_guid": test_bagfile.get('guid'),
            "extractor_id": 'ext_1',
            "start_ts": 1577971500000,
            "end_ts": 1577971501010
        }
        res = requests.post(url=vote_url, headers=header, json=vote)
        self.assertEqual(409, res.status_code)


class TestBvaDELETE(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_DELETE_vote_should_delete_vote_and_revert_quota(self):
        test_vote = deepcopy(test_vote_document)
        test_vote['quota_document_guid'] = test_quota_document_active.get(
            'guid')

        test_quota_active = deepcopy(test_quota_document_active)
        test_quota_active['accounts']['workstream_id1']['quota']['used'] = 20

        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_active)
            con.db['bva_votes'].insert_one(test_vote)
            con.db['accounts'].insert_one(test_credentials)

        res = requests.delete(url=vote_url + test_vote.get('guid'),
                              headers=header)
        self.assertEqual(200, res.status_code)
        self.assertEqual(0, res.json()['results'].get('used'))

    @unittest.skip("test-case is broken")
    def test_DELETE_vote_should_end_with_BAD_REQUEST_when_voting_is_already_closed(
            self):
        test_vote = deepcopy(test_vote_document)
        test_vote['quota_document_guid'] = test_quota_document_inactive.get(
            'guid')
        test_vote['bagfile_guid'] = test_bagfile.get('guid')

        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_active)
            con.db['bva_quotas'].insert_one(test_quota_document_inactive)
            con.db['bva_votes'].insert_one(test_vote)
            con.db['accounts'].insert_one(test_credentials)
            con.db['bagfiles'].insert_one(test_bagfile)

        res = requests.delete(url=vote_url + test_vote.get('guid'),
                              headers=header)
        self.assertEqual(400, res.status_code)


class TestBvaPATCH(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_PATCH_vote_should_adapt_vote_to_the_new_start_and_end_timestamps(
            self):
        test_vote = deepcopy(test_vote_document)
        test_vote['quota_document_guid'] = test_quota_document_active.get(
            'guid')
        test_vote['bagfile_guid'] = test_bagfile.get('guid')

        test_quota_active = deepcopy(test_quota_document_active)
        test_quota_active['accounts']['workstream_id1']['quota']['used'] = 20

        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_active)
            con.db['bva_votes'].insert_one(test_vote)
            con.db['accounts'].insert_one(test_credentials)
            con.db['bagfiles'].insert_one(test_bagfile)

        res = requests.patch(url=vote_url + test_vote.get('guid'),
                             json={'start_ts': 1577971500002,
                                   'end_ts': 1577971500003}, headers=header)
        self.assertEqual(200, res.status_code)
        self.assertEqual(10, res.json()['results'].get('used'))

    @unittest.skip("test-case is broken")
    def test_PATCH_vote_should_end_with_BAD_REQUEST_when_voting_is_already_closed(
            self):
        test_vote = deepcopy(test_vote_document)
        test_vote['quota_document_guid'] = test_quota_document_inactive.get(
            'guid')

        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_active)
            con.db['bva_quotas'].insert_one(test_quota_document_inactive)
            con.db['bva_votes'].insert_one(test_vote)
            con.db['accounts'].insert_one(test_credentials)

        res = requests.patch(url=vote_url + test_vote.get('guid'),
                             json={'start_ts': 1577971500002,
                                   'end_ts': 1577971500003}, headers=header)
        self.assertEqual(400, res.status_code)


if __name__ == '__main__':
    unittest.main()
