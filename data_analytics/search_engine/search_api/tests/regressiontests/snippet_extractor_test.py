"""Regressiontests for snippet bva route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import random as rd
import unittest

import requests

from .testutils import get_testdb_connector
from .utils import generate_testdata


def prepare_db():
    with get_testdb_connector() as con:
        con.db['bagfiles'].delete_many({'is_test_data': 1})
        con.db['extractor_requests'].delete_many({'is_test_data': 1})
    rd.seed(0)


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['bagfiles'].delete_many({'is_test_data': 1})
        con.db['extractor_requests'].delete_many({'is_test_data': 1})


base_url = 'http://localhost:8889'
snippet_extractor_url = base_url + '/snippet/extractor'


class TestSnippetExtractorPOST(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_snippet_extractor_POST(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_4, ext_5, child_1, child_2, child_3 = generate_testdata(
        )

        with get_testdb_connector() as con:
            con.add_bagfiles([bag_1, bag_2, bag_3])
            con.add_extractor_requests([ext_1, ext_2, ext_3])
        query_data = {
            'bagfile_guid': bag_2['guid'],
            'bva_limit': 7,
            'generate_child_bagfiles': False
        }

        res = requests.post(url=snippet_extractor_url,
                            json=query_data)
        self.assertEqual(200, res.status_code, 'Snippet extractor post failed')
        res_json = res.json()['results']
        del res_json[0]['guid']
        del res_json[1]['guid']
        expected_res = [{
            'version': '1.0',
            'parent_guid': '7a5b95e4-e837-4123-8823-c1189ecc40fc',
            'start': 1475272820000,
            'end': 1475272840000,
            'parent_index_start': 20,
            'parent_index_end': 40,
            'bva_max': 8,
            'metadata': {
                'disengagement': {},
                'silent_testing': {}
            }
        }, {
            'version': '1.0',
            'parent_guid': '7a5b95e4-e837-4123-8823-c1189ecc40fc',
            'start': 1475272840000,
            'end': 1475272860000,
            'parent_index_start': 40,
            'parent_index_end': 60,
            'bva_max': 8,
            'metadata': {
                'disengagement': {}
            }
        }]

        self.assertCountEqual(
            expected_res, res_json,
            'Snippet extractor post failed')

    def test_snippet_extractor_POST_with_filter(self):
        bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_4, ext_5, child_1, child_2, child_3 = generate_testdata(
        )

        with get_testdb_connector() as con:
            con.add_bagfiles([bag_1, bag_2, bag_3])
            con.add_extractor_requests([ext_4, ext_5])
        query_data = {
            'bagfile_guid': bag_2['guid'],
            'bva_limit': 7,
            'generate_child_bagfiles': False
        }

        res = requests.post(url=snippet_extractor_url,
                            json=query_data)
        self.assertEqual(200, res.status_code, 'Snippet extractor post failed')
        res_json = res.json()['results']
        del res_json[0]['guid']
        expected_res = [{
            'version': '1.0',
            'parent_guid': '7a5b95e4-e837-4123-8823-c1189ecc40fc',
            'start': 1475272840000,
            'end': 1475272860000,
            'parent_index_start': 40,
            'parent_index_end': 60,
            'bva_max': 8,
            'metadata': {
                'disengagement': {'ts': 1475272850000}
            }
        }]

        self.assertCountEqual(
            expected_res, res_json,
            'Snippet extractor post failed')

if __name__ == '__main__':
    unittest.main()
