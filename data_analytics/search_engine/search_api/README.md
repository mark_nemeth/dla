# DANF - Search API Microservice
### Version: see setup.py

## First steps:
1. create `Personal Access Tokens` in AzureDevOps page
2. copy the token you created.
3. add to the general url like this(it looks like: https://YOUR_TOKEN@pkgs.dev.azure.com/halfdome/halfdome/_packaging/DLA/pypi/simple/ )

```console
export index_url_with_access=https://<YOUR_KEY>@pkgs.dev.azure.com/halfdome/halfdome/_packaging/DLA/pypi/simple/
```

## Dockerize/Deploy Search API - Service to cluster
**If you want to build, run, and push the docker images manually, please add the prefix `man-`**

Grant permissions and run deployment script (Specify the version for the docker image, e.g. githash)
```console
chmod 755 kubernetes/deployment.sh
./kubernetes/deployment.sh <app-name> <app-version>
```

Script builds docker image, pushes image to the development registry of DOL and creates a new deployment and service in Kubernetes (in case those do not exist yet).

# How to test the Search-API locally

## Without containers

1. run service
 ```console
 $ export DANF_ENV=local
 $ export APPINSIGHTS_INSTRUMENTATIONKEY=APPINSIGHTS_DISABLE_KEY
 $ python3 index.py
 ```

2. run docker-compose in local_dev_env folder

3. run test

## Within containers

1. build service image
> docker build -t dlacontainerregistryacrdev.azurecr.io/man-search-api:0.0.1  --no-cache --build-arg EXTRA_INDEX_URL=$index_url_with_access .

2. run service and mongo images in containers in the local test network and supply required env variables

> docker run --rm -it \
--network test_net \
-e DANF_ENV='local' \
-e APPINSIGHTS_INSTRUMENTATIONKEY='APPINSIGHTS_DISABLE_KEY' \
-e db_hostname='local-mongo' \
-e AUTHENTICATION_METHOD='none' \
-p 8445:8445/tcp \
-p 8889:8889 dlacontainerregistryacrdev.azurecr.io/man-search-api:0.0.1

> docker run --network test_net \
-e MONGO_INITDB_ROOT_USERNAME='user' \
-e MONGO_INITDB_ROOT_PASSWORD='password' \
-p 27017:27017 \
--name local-mongo -d mongo:4.4

> docker run --network test_net -e ME_CONFIG_MONGODB_SERVER=local-mongo \
-e ME_CONFIG_MONGODB_ADMINUSERNAME='user' \
-e ME_CONFIG_MONGODB_ADMINPASSWORD='password' \
-p 8081:8081 mongo-express

# How to deploy the Search-api-service in Kubernetes

## manually 
1. build docker image:
> docker build -t dlacontainerregistryacrdev.azurecr.io/man-search-api:<version>  --network=host --no-cache  --build-arg EXTRA_INDEX_URL=$index_url_with_access .

2. push to the Azure Registry
> az acr login -n dlacontainerregistryacrdev
> docker push dlacontainerregistryacrdev.azurecr.io/man-search-api:${version}

3. connecting to aks
> az account set --subscription CC-AD-ICO2-HalfDome-Dev
> az aks get-credentials  --name dla-kubecluster-aks-dev --resource-group dla-engineering-dev 
> sed "s/{docker_image_version}/${version}/g" kubernetes/search_api_az_dev_man.yaml | kubectl  apply -f -

4. start the dashboard to monitor
> az aks browse --resource-group dla-engineering-dev --name dla-kubecluster-aks-dev

## Run scripts (only for Azure dev stage for testing)
> ./kubernetes/deployment.sh search-api ${version}

Do not forget to export index_url_with_access=https://<TOKEN>@pkgs.dev.azure.com/halfdome/halfdome/_packaging/DLA/pypi/simple/ before running the script.

# Authentication in Search API

DANF Search API is using Daimler GAS-OIDC for user authentication when
AUTHENTICATION_METHOD configuration parameter is set to "gas".
In this case client requests must provide a valid Oauth 2.0 bearer token
authorization header to access endpoints protected by the auth handler.
The validity of the access tokens are checked via the Introspection Endpoint.

DANF UI Service will use Authorization Code Flow to provide web login
for real users and request access tokens for them from GAS-OIDC Token Endpoint.
In case of machine-to-machine communications e.g. in extractors
Client Credentials Flow has been implemented in DANF ContainerClient
to automatically authenticate clients via client_id and client_secret.

Further information about Daimler GAS-OIDC is available on
https://team.sp.wp.corpintra.net/sites/05389/GAS-OIDC/SitePages/DaimlerHome.aspx

# Troubleshooting
1. Cannot find DBConnector?
    - Solution: manually install the DBConnector and generate whl file. please check the DBConnector repo


# Release Notes
- 0.2.86 : shift the version to config.constants :ATTDATA-2263