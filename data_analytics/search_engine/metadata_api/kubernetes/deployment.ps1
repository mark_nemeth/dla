# Windows Powershell-Script to deploy to the cluster from local
Write-Host "Started deployment script ..."

if ($args.count -ne 2) {
    Write-Host "Argument missing!"
    Write-Host "You have to pass 2 arguments: app_name and docker_image_version!"
    exit 1
}

if (-not (Test-Path env:index_url_with_access)) {
    Write-Host "Please define an environment-variable with the name 'index_url_with_access'"
    Write-Host "For example:"
    Write-Host "Set-Item -Path Env:index_url_with_access -Value ..."
    exit 2
}

# connect
az acr login --name dlacontainerregistryacrdev

$app_name=$args[0]
$docker_image_version=$args[1]

# use env-variable
$extra_index_url=$env:index_url_with_access

# print defined variables
Write-Host "Building and deploying with the following config: app_name=${app_name}, docker_image_version=${docker_image_version}"

# build docker image
docker build -t dlacontainerregistryacrdev.azurecr.io/${app_name}:${docker_image_version} --network=host --no-cache --build-arg EXTRA_INDEX_URL=${extra_index_url} --build-arg http_proxy=http://host.docker.internal:3128/ --build-arg https_proxy=http://host.docker.internal:3128/ .

# push docker image to registry
docker push dlacontainerregistryacrdev.azurecr.io/${app_name}:${docker_image_version}

# replace template + create / update deployment on Kubernetes cluster
(Get-Content .\kubernetes\metadata_api_az_dev.yaml).replace('latest', ${docker_image_version}) | Set-Content .\kubernetes\metadata_api_az_dev_tmp.yaml
(Get-Content .\kubernetes\metadata_api_az_dev_tmp.yaml).replace("metadata-api:", "${app_name}:") | Set-Content .\kubernetes\metadata_api_az_dev_tmp_1.yaml

kubectl  apply -f kubernetes\metadata_api_az_dev_tmp_1.yaml

Remove-Item -Force .\kubernetes\medatadata_api_az_dev_tmp*.yaml