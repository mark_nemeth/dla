"""Handler class for utility functions"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


import logging

from bson.json_util import dumps

logger = logging.getLogger(__name__)


DEFAULT_ERROR = "Server Error!"
ERROR_KEY_WORD = "error"
RESULTS_KEY_WORD = "results"
ERROR_MESSAGE_KEY_WORD = "error_message"
TOTAL_KEY_WORD = "total"


class Utils:

    def build_response(self,
                       response,
                       err=DEFAULT_ERROR,
                       results=None,
                       status_code=500,
                       location=None,
                       total=None):

        # bind status code
        response.set_header("Content-Type", "application/json")
        response.set_status(status_code)
        if location is not None:
            response.set_header("Location", location)

        if err is not None:
            res = {ERROR_KEY_WORD: err}
        else:
            res = {RESULTS_KEY_WORD: results}

        if total is not None:
            res[TOTAL_KEY_WORD] = total

        logger.debug(f"Response {res}")
        return response.finish(dumps(res))

    def build_error_from_message(self, message):
        return {
            ERROR_MESSAGE_KEY_WORD: message
        }

    def timestamp_to_sequence_index(self, ts, bagfile_start_ts):
        return (ts // 1000) - (bagfile_start_ts // 1000)

    def sequence_index_to_timestamp(self, index, bagfile_start_ts):
        return ((bagfile_start_ts // 1000) + index) * 1000


utils = Utils()
