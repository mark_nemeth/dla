import React from 'react';
import Button from 'react-bootstrap/Button';
import {defaultEmptyValue} from '../../Table';
import {copyToClipboard} from '../../../utils/copyToClipboard';
import {isStringValid} from '../../../utils/helperFunctions';

export default function PathEntry(props) {

  if (!isStringValid(props.filePath)) {
    if (props.buttonType) return <Button variant="secondary" disabled size="sm">Copy path</Button>;
    else return <span>{defaultEmptyValue}</span>;
  }

  function copyPathToClipboard() {
    // Set timer to change text back to normal after 2 seconds
    setTimeout(() => {
      document.getElementById(props.filePath).innerHTML = 'Copy path';
    }, 2000);

    // Call copy to clipboard with callbacks
    copyToClipboard(props.filePath, (success) => {
        document.getElementById(props.filePath).innerHTML = 'Copied!';
      }, (error) => {
        document.getElementById(props.filePath).innerHTML = 'Clipboard Error';
      }
    )
  }

  return (
    props.buttonType
      ? <Button id={props.filePath} variant="info" size="sm" onClick={copyPathToClipboard}>Copy path</Button>
      : <span>{props.filePath}</span>
  )
}