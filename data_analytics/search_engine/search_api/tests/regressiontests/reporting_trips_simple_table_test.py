"""Unittests for DBConnector"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import unittest

import requests
from DANFUtils.logging import logger
from datetime import timedelta, datetime
# Set env before importing db connector
# from .utils import generate_bagfile, generate_feature_buckets

os.environ["DANF_ENV"] = "local"
from .testutils import get_testdb_connector

# increase log level for tests
logger.set_log_level('debug')

base_url = 'http://localhost:8889'
report_kpi_url = base_url + '/reporting/trips/tables/simple'

bag_1 = {
    "version": "1.0",
    "guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "link": "testdata",
    "file_name": "testdata1.bag",
    "size": 21231,
    "num_messages": 4444,
    "start": 1597838471160,
    "end": 1597840223670,
    "duration": 3001,
    "vehicle_id_num": "V-123-234",
    "topics": ["topic2"],
    "drive_types": ["CID"],
    "is_test_data": 1,
    "custom_attributes": {
        "feature_extraction": {
            "version": "0.1.22",
            "number_of_planned_disengagements_total": 2,
            "number_of_manual_unplanned_disengagements_total": 3,
            "number_of_error_disengagements_total": 4,
            "number_of_public_planned_disengagements_total": 2,
            "number_of_public_manual_unplanned_disengagements_total": 3,
            "number_of_public_error_disengagements_total": 4,
            "total_engaged_distance": 12345.9203894549,
            "engaged_distance_in_public": 12345.9203894549,
            "duration_driven_in_engaged_mode": 706.360106,
            "country": "DE",
            "total_distance": 1636367.63637027
        }
    }
}

ext_1 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999999",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597838758163,
    "start": 1597838656213,
    "end": 1597838758163,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:04:16.213098+00:00",
        "end_ts": "2020-08-19 12:05:58.163086+00:00",
        "duration": 101.949988,
        "distance_public": 828.144621129744,
        "distance": 928.144621129744,
        "end_details": "not_filtered anymore",
        "end_planned": 0,
        "end_velocity": -4.96524948357546e-05,
        "country": "DE"
    },
    "is_test_data": 1
}

ext_2 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999998",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597838797863,
    "start": 1597838797853,
    "end": 1597838797863,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:06:37.853083+00:00",
        "end_ts": "2020-08-19 12:06:37.863079+00:00",
        "duration": 0.009996,
        "distance_public": 0.975415342082024,
        "distance": 0.975415342082024,
        "end_details": None,
        "end_planned": 0,
        "end_velocity": 10.3755190467834,
        "country": "DE",
        "vin": "W222"
    },
    "is_test_data": 1
}

ext_3 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999997",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597839172173,
    "start": 1597838956283,
    "end": 1597839172173,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:09:16.283082+00:00",
        "end_ts": "2020-08-19 12:12:52.173077+00:00",
        "duration": 215.889995,
        "distance_public": 2384.73693096242,
        "distance": 2384.73693096242,
        "end_details": "[51]Distronic lever pushed; ",
        "end_planned": True,
        "end_velocity": 23.8649177837372,
        "country": "DE",
        "manual_disengagement": True
    },
    "is_test_data": 1
}

ext_4 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999996",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597839182043,
    "start": 1597839180383,
    "end": 1597839182043,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:13:00.383084+00:00",
        "end_ts": "2020-08-19 12:13:02.043078+00:00",
        "duration": 1.659994,
        "distance_public": 36.3831270868416,
        "distance": 36.3831270868416,
        "end_details":
        "[52]Accelerator pedal pressed; [55]Acceleration or brake pedal pressed; ",
        "end_planned": False,
        "end_velocity": 22.9763528823853,
        "country": "DE",
        "manual_disengagement": True,
        "vin": "W123"
    },
    "is_test_data": 1
}

ext_5 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999995",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597839519453,
    "start": 1597839197663,
    "end": 1597839519453,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:13:17.663087+00:00",
        "end_ts": "2020-08-19 12:18:39.453095+00:00",
        "duration": 321.790008,
        "distance_public": 7675.85577658969,
        "distance": 7675.85577658969,
        "end_details": "Error ",
        "end_planned": False,
        "end_velocity": 21.2098003482819,
        "country": "DE",
        "manual_disengagement": False,
        "vin": "W123"
    },
    "is_test_data": 1
}

ext_6 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999994",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1497839538483,
    "start": 1497839527223,
    "end": 1497839538483,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:18:47.223082+00:00",
        "end_ts": "2020-08-19 12:18:58.483078+00:00",
        "duration": 11.259996,
        "distance_public": 283.271966150785,
        "distance": 283.271966150785,
        "end_details": "[54]Steering takeover; ",
        "end_planned": False,
        "end_velocity": 26.6848262023926,
        "country": "DE",
        "manual_disengagement": True,
        "vin": "W123"
    },
    "is_test_data": 1
}

ext_7 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999993",
    "version": "1.0",
    "extractor_version": "0.1.35",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "extractor_id": "feature_extraction_diseng_details",
    "ts": 1575934091251,
    "start": 1575933366311,
    "end": 1575934091251,
    "bva": 0,
    "metadata": {
        "start_ts": "2019-12-09 23:16:06.311597+00:00",
        "start_latitude": 37.3233391682291,
        "start_longitude": -121.945522957775,
        "start_loc_error": None,
        "end_ts": "2019-12-09 23:28:11.251592+00:00",
        "end_latitude": 37.324531661093,
        "end_longitude": -121.900646155361,
        "end_engagement_state": 0,
        "end_loc_error": None,
        "duration": 724.939995,
        "distance": 3994.41030566444,
        "distance_public": 3994.41030566444,
        "distance_private": 0.0,
        "end_source": 11,
        "end_cause": 1,
        "end_details": "[7.6]MC_Lon passive dis-engagement; ",
        "end_planned": False,
        "end_public_road": True,
        "end_velocity": 0.00107899970804283,
        "geofence": ["SJ pilot"],
        "country": "US",
        "state": "CA",
        "lr_ts": "2019-12-09 23:28:11.251592+00:00",
        "lr_ts_diff": 0.0,
        "manual_disengagement": False,
        "vin": "WDD2221591A470238"
    },
    "event_type": "metadata_enrichment",
    "is_test_data": 1
}


class TestReportingTripsSimpleTableGET(unittest.TestCase):
    def setUp(self):
        self.connector = get_testdb_connector()
        self.cleanup_db()

        self.connector.add_bagfiles([bag_1])
        self.connector.add_extractor_requests(
            [ext_1, ext_2, ext_3, ext_4, ext_5, ext_6, ext_7])

    def tearDown(self):
        self.cleanup_db()
        pass

    def cleanup_db(self):
        with self.connector as con:
            con.db['bagfiles'].delete_many({"is_test_data": 1})
            con.db['extractor_requests'].delete_many({"is_test_data": 1})

    def test_table_retrieval_invalid_param(self):
        query_data = {
            "start": "test",
            "end": 1603196749000,
            "country": "ger",
            "report_type": "dmv",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(400, res.status_code, 'KPI retrieval failed')

    def test_table_retrieval_invalid_location(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "test",
            "report_type": "dmv",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(500, res.status_code, 'KPI retrieval failed')

    def test_table_retrieval_missing_param(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "country": "ger",
            "report_type": "dmv",
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(400, res.status_code, 'KPI retrieval failed')

    def test_table_retrieval_no_return_dmv(self):
        query_data = {
            "start": 2475158400000,
            "end": 2575158400001,
            "location": "ger",
            "report_type": "dmv",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')
        res_json = res.json()['results']
        self.assertDictEqual({'tables': []}, res_json, 'KPI retrieval failed')

    def test_table_retrieval_all_filters(self):
        query_data = {
            "start": 1497839527223,
            "end": 1597839527223,
            "location": "ger",
            "report_type": "dmv",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')

        expected_res = [{
            'caption':
            'Unplanned Disengagement Details',
            'col_labels':
            ['VIN', 'Date', 'Disengagement type', 'Disengagement details'],
            'values': [[0, '2020-08-19', 'error', 'd4'],
                       [1, '2020-08-19', 'manual', 'd2'],
                       [1, '2020-08-19', 'error', 'd1'],
                       [1, '2020-08-19', 'manual', 'd3']],
            'key_value_map': {
                '0':
                'Unknown',
                '1':
                'W123',
                'd0':
                'Unknown error that caused the system to disengage.',
                'd1':
                'Error ',
                'd2':
                'The operator disengaged the system manually to remain in the '
                'operational design domain. This was achieved by pressing the '
                'accelerator pedal to increase velocity of the vehicle. '
                'This was accomplished by pressing the brake pedal to reduce '
                'the velocity of the vehicle.',
                'd3':
                'The operator disengaged the system manually to remain in the '
                'operational design domain. '
                'The driver performed a steering manouver to correct the '
                'trajectory of the vehicle.',
                'd4':
                'not_filtered anymore'
            }
        }]
        self.maxDiff = None
        self.assertCountEqual(expected_res, res_json["tables"],
                              'KPI retrieval failed')

    def test_table_retrieval_less_filters(self):
        query_data = {
            "start": 1497839527223,
            "end": 1597839527223,
            "location": "ger",
            "report_type": "dmv",
            "ign_dur": False,
            "ign_dist": False,
            "ign_no_info": False,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')

        expected_res = [{
            'caption':
            'Unplanned Disengagement Details',
            'col_labels':
            ['VIN', 'Date', 'Disengagement type', 'Disengagement details'],
            'values': [[0, '2020-08-19', 'error', 'd5'],
                       [2, '2020-08-19', 'error', 'd0'],
                       [1, '2020-08-19', 'manual', 'd3'],
                       [1, '2020-08-19', 'error', 'd2'],
                       [1, '2020-08-19', 'manual', 'd4']],
            'key_value_map': {
                '0':
                'Unknown',
                '1':
                'W123',
                '2':
                'W222',
                'd0':
                'Unknown error that caused the system to disengage.',
                'd1':
                'Unknown error that caused the system to disengage.',
                'd2':
                'Error ',
                'd3':
                'The operator disengaged the system manually to remain in the '
                'operational design domain. This was achieved by pressing the '
                'accelerator pedal to increase velocity of the vehicle. '
                'This was accomplished by pressing the brake pedal to '
                'reduce the velocity of the vehicle.',
                'd4':
                'The operator disengaged the system manually to remain in the '
                'operational design domain. '
                'The driver performed a steering manouver to correct '
                'the trajectory of the vehicle.',
                'd5':
                'not_filtered anymore'
            }
        }]
        logger.info(res_json)
        self.maxDiff = None
        self.assertCountEqual(expected_res, res_json["tables"],
                              'KPI retrieval failed')

    def test_table_retrieval_no_filters(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "ger",
            "report_type": "dmv",
            "ign_dur": False,
            "ign_dist": False,
            "ign_no_info": False,
            "ign_priv": False
        }
        res = requests.get(url=report_kpi_url, params=query_data)
        res_json = res.json()['results']
        logger.info(res_json)

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')

        expected_res = [{
            'caption':
            'Unplanned Disengagement Details',
            'col_labels':
            ['VIN', 'Date', 'Disengagement type', 'Disengagement details'],
            'values': [[0, '2020-08-19', 'error', 'd4'],
                       [2, '2020-08-19', 'error', 'd0'],
                       [1, '2020-08-19', 'manual', 'd3'],
                       [1, '2020-08-19', 'error', 'd2']],
            'key_value_map': {
                '0':
                'Unknown',
                '1':
                'W123',
                '2':
                'W222',
                'd0':
                'Unknown error that caused the system to disengage.',
                'd1':
                'Unknown error that caused the system to disengage.',
                'd2':
                'Error ',
                'd3':
                'The operator disengaged the system manually to remain in the '
                'operational design domain. This was achieved by pressing the accelerator pedal '
                'to increase velocity of the vehicle. This was accomplished by pressing the '
                'brake pedal to reduce the velocity of the vehicle.',
                'd4':
                'not_filtered anymore'
            }
        }]
        self.maxDiff = None

        self.assertCountEqual(expected_res, res_json["tables"],
                              'KPI retrieval failed')

    def test_table_retrieval_no_return_rp(self):
        query_data = {
            "start": 2475158400000,
            "end": 2575158400001,
            "location": "ger",
            "report_type": "rp",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')
        res_json = res.json()['results']
        self.assertDictEqual({'tables': []}, res_json, 'KPI retrieval failed')
