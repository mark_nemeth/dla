import request from 'request-promise-native';
import storeProvider from '../models/storeProvider';
import {getDevModeLocalRedisUserTokens} from '../utils/localDevHelper';
import configHandlerSingleton from '../configHandler'

export default class BaseApi {

  static getAuthHeader(req) {
    if (configHandlerSingleton.uiIsInDevMode()) return getDevModeLocalRedisUserTokens();
    return storeProvider.getUserTokenDataAsync(req.session.passport.user).then((userTokenData) => {
      return {'Authorization': userTokenData.token_type + ' ' + userTokenData.access_token}
    })
  }

  static doRequest(config) {
    return request(config).then((res) => {
      if(typeof res === 'object'){
        return res;
      }
      return JSON.parse(res);
    });
  }

}