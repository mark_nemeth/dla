"""Regression tests for cache origin route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests

base_url = 'http://localhost:8887'
cache_origin_url = base_url + '/cache/origins'


class TestCacheVehicleIdsPOST(unittest.TestCase):
    def test_post_cache_document(self):
        res = requests.post(url=cache_origin_url)
        self.assertEqual(200, res.status_code, 'post request is failed')
