import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByEventFilter} from './ByEventFilter';
import ScrollList from '../../../Utils/ScrollList/ScrollList';

describe('ByEventFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      eventList: new Map([
        ['Disengagement', 'post_ingest_ext_1.disengagement'],
        ['Silent Testing', 'ext1_2.silent_testing']]),
      selectedEventList: ['post_ingest_ext_1.disengagement']
    };
    const wrapper = shallow(<ByEventFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should renders vid list with selected column', () => {
    const reduxAction = jest.fn();
    const props = {
      eventList: new Map([
        ['Disengagement', 'post_ingest_ext_1.disengagement'],
        ['Silent Testing', 'ext1_2.silent_testing']]),
      selectedEventList: ['post_ingest_ext_1.disengagement'],
      filterByEvents: reduxAction
    };
    const wrapper = mount(<ByEventFilter {...props} />);
    const eventList = wrapper.find(ScrollList);
    const entries = eventList.find('tr');
    expect(entries.length).toBe(2);
    expect(entries.at(0).find('input').props().checked).toBe(true);
    expect(entries.at(1).find('input').props().checked).toBe(false);
  });


  it('should call action when vid is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      eventList: new Map([
        ['Disengagement', 'post_ingest_ext_1.disengagement'],
        ['Silent Testing', 'ext1_2.silent_testing']]),
      selectedEventList: ['post_ingest_ext_1.disengagement'],
      filterByEvents: reduxAction
    };
    const wrapper = mount(<ByEventFilter {...props} />);
    const input = wrapper.find('tr').at(1).find('input');
    input.simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

});
