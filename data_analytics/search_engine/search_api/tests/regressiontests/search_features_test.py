"""Unittests for DBConnector"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os
import unittest

import requests
from DANFUtils.logging import logger

# Set env before importing db connector
os.environ["DANF_ENV"] = "local"
from .testutils import get_testdb_connector

# increase log level for tests
logger.set_log_level('debug')

base_url = 'http://localhost:8889'
search_features_url = base_url + '/search/features'

bag = {
    "guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "link": "/test/test/test/test/test/test/test",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "version": "1.0",
    "is_test_data": 1
}

feat_bucket_docs = [{
    "version":
    "1.0",
    "guid":
    "22221111-aaaa-bbbb-cccc-999999999999",
    "bagfile_guid":
    bag["guid"],
    "start":
    1563262907000,
    "end":
    1563262911001,
    "start_index":
    60,
    "end_index":
    119,
    "features": [{
        "ts": 1563262907000,
        "vel": 0.4,
        "acc": 0.2,
        "eng_state": 5,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 5.1
    }, {
        "ts": 1563262908000,
        "vel": 0.5,
        "acc": 0.3,
        "eng_state": 11,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 5.1
    }],
    "is_test_data":
    1
}, {
    "version":
    "1.0",
    "guid":
    "22221111-aaaa-bbbb-cccc-888888888888",
    "bagfile_guid":
    bag["guid"],
    "start":
    1563263907000,
    "end":
    1563263911001,
    "start_index":
    600,
    "end_index":
    660,
    "features": [{
        "ts": 1563263907000,
        "vel": 20,
        "acc": -4,
        "eng_state": 7,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 100.1
    }, {
        "ts": 1563263908000,
        "vel": 20,
        "acc": -5,
        "eng_state": 8,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 100.1
    }],
    "is_test_data":
    1
}]


class TestFeatureSearchChildBagfilesGET(unittest.TestCase):
    def setUp(self):
        self.connector = get_testdb_connector()
        self.cleanup_db()
        # add
        self.connector.add_bagfiles([bag])
        self.connector.add_feature_buckets(feat_bucket_docs)

    def tearDown(self):
        self.cleanup_db()

    def cleanup_db(self):
        with self.connector as con:
            con.db['bagfiles'].delete_many({"is_test_data": 1})
            con.db['feature_buckets'].delete_many({"is_test_data": 1})

    def test_search_by_guid(self):
        query_data = {'guid': feat_bucket_docs[0]["guid"]}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'Featurs search by guid failed')
        self.assertDictEqual(feat_bucket_docs[0], res_json[0],
                             'Featurs search by guid failed')

    def test_search_by_bagfile_guid(self):
        query_data = {'bagfile_guid': bag["guid"]}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Featurs search by parent guid failed')
        self.assertCountEqual(feat_bucket_docs, res_json,
                              'Featurs search by parent guid failed')

    def test_search_by_time_range(self):
        query_data = {
            "start[gte]": feat_bucket_docs[0]["start"],
            "end[lte]": feat_bucket_docs[0]["end"]
        }
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by time range failed')
        self.assertDictEqual(feat_bucket_docs[0], res_json[0],
                             'Feature search by time range  failed')

    def test_search_by_empty_time_range(self):
        query_data = {
            "start[gte]": feat_bucket_docs[0]["start"] + 1000,
            "end[lte]": feat_bucket_docs[0]["end"]
        }
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Featurs search by empty time range  failed')
        self.assertCountEqual([], res_json,
                              'Featurs search by empty time range failed')

    def test_search_by_index_range(self):
        query_data = {
            "start_index[gte]": feat_bucket_docs[0]["start_index"],
            "end_index[lte]": feat_bucket_docs[0]["end_index"]
        }
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by index range failed')
        self.assertDictEqual(feat_bucket_docs[0], res_json[0],
                             'Feature search by index range  failed')

    def test_search_by_feature_value(self):
        query_data = {'feat.acc[gte]': 0}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by index range failed')
        self.assertDictEqual(feat_bucket_docs[0], res_json[0],
                             'Feature search by index range  failed')

    def test_search_by_feature_value_without_brackets(self):
        query_data = {'feat.eng_state': 5}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by index range failed')
        self.assertDictEqual(feat_bucket_docs[0], res_json[0],
                             'Feature search by index range  failed')

    def test_search_by_feature_value_range(self):
        query_data = {'feat.vel[gte]': 10, 'feat.vel[lte]': 30}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by index range failed')
        self.assertDictEqual(feat_bucket_docs[1], res_json[0],
                             'Feature search by index range  failed')

    def test_search_by_multiple_feature_values(self):
        query_data = {
            'feat.acc[gte]': 0,
            'feat.vel[lte]': 100,
            'feat.eng_state[]': 11
        }
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by index range failed')
        self.assertDictEqual(feat_bucket_docs[0], res_json[0],
                             'Feature search by index range  failed')

    def test_search_limit_sort(self):
        query_data = {'features.vel[gte]': 0, 'limit': 1, 'sortby': 'start'}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by index range failed')
        self.assertDictEqual(feat_bucket_docs[0], res_json[0],
                             'Feature search by index range  failed')

    def test_search_limit_sort_decreasing(self):
        query_data = {'feat.vel[gte]': 0, 'limit': 2, 'sortby': '-start_index'}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by index range failed')
        self.assertDictEqual(feat_bucket_docs[1], res_json[0],
                             'Feature search by index range  failed')

    def test_search_offset_sort(self):
        query_data = {'feat.vel[gte]': 0, 'offset': 1, 'sortby': 'start'}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by index range failed')
        self.assertDictEqual(feat_bucket_docs[1], res_json[0],
                             'Feature search by index range  failed')

    def test_search_by_empty_data(self):
        query_data = {}
        res = requests.get(url=search_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Feature search by empty data failed')
