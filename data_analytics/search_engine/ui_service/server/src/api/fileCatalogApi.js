import configHandlerSingleton from '../configHandler';
import BaseApi from './BaseApi';

export default class FileCatalogApi {

    static getFileByUrl(url) {
        let config = configHandlerSingleton.getFileCatalogApiConfig();

        url = url.replace('/mapr/', 'hdfs://');
        config.qs = {
            'url': url,
            'url_status': 'Available',
        }

        if (configHandlerSingleton.isNodeEnvLocal()) {
            let mockResponse = {
                results: [
                    {
                        "parent_guid": null,
                        "file_type": "data",
                        "file_size": 0,
                        "urls": [
                            {
                                "accessor_url": "hdfs://059-va2.mbc.de/data/input/rd/athena/IT4AD-integration-test/test0.txt",
                                "creation_date": 1234567890000,
                                "entry_created_by": "Test",
                                "url_status": "Available",
                                "last_changed": 1234567890000
                            },
                            {
                                "accessor_url": "hdfs://059-va2.mbc.de/data/input/rd/athena/IT4AD-integration-test/test1.txt",
                                "creation_date": 1234567890000,
                                "entry_created_by": "Test",
                                "url_status": "Available",
                                "last_changed": 1234567890000
                            },
                            {
                                "accessor_url": "hdfs://059-va2.mbc.de/data/input/rd/athena/IT4AD-integration-test/test2.txt",
                                "creation_date": 1234567890000,
                                "entry_created_by": "Test",
                                "url_status": "Available",
                                "last_changed": 1234567890000
                            }
                        ],
                        "tags": null,
                        "file_guid": "00000000-0000-380f-9d36-000000000001"
                    }
                ]

            }
            return new Promise((resolve, reject) => {

                let catalogFile = FileCatalogFile.createFromResponseData(mockResponse.results[0]);
                let file = File.createFromCatalogFile(catalogFile);
                resolve(file);
            });
        } else {
            return BaseApi.doRequest(config).then(response => {
                if (response.total === 0) {
                    return null;
                }

                let catalogFile = FileCatalogFile.createFromResponseData(response.results[0]);
                let file = File.createFromCatalogFile(catalogFile);
                return file;
            });
        }
    }
}

class File {
    constructor(fileGuid, locations) {
        this.fileGuid = fileGuid;
        this.locations = locations;
    }

    static createFromCatalogFile(catalogFile) {
        let locations = [
            new Location(new Cluster(CLUSTER_VAIHINGEN_ID, 'Vaihingen'), []),
            new Location(new Cluster(CLUSTER_SUNNYVALE_ID, 'Sunnyvale'), []),
            new Location(new Cluster(CLUSTER_ABSTATT_ID, 'Abstatt'), []),
            new Location(new Cluster(CLUSTER_BOEBLINGEN_ID, 'Boeblingen/HKS'), []),
        ]

        let urls = catalogFile.urls;
        urls.forEach(url => {
            let url_cluster = url.getCluster();
            locations.forEach(location => {
                if (location.cluster.id === url_cluster) {
                    if ([CLUSTER_VAIHINGEN_ID, CLUSTER_BOEBLINGEN_ID, CLUSTER_SUNNYVALE_ID].includes(location.cluster.id)) {
                        location.addPath(url.accessorUrl.replace('hdfs://', '/mapr/'));
                    } else {
                        location.addPath(url.accessorUrl.replace('abt', 'athena01'));
                    }
                }
            })
        });
        return new File(catalogFile.fileGuid, locations);
    }
}

class Location {
    constructor(cluster, paths) {
        this.cluster = cluster;
        this.paths = paths || [];
    }

    addPath(path) {
        this.paths.push(path);
    }
}

class Cluster {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}

let CLUSTER_VAIHINGEN_ID = 'VAI';
let CLUSTER_SUNNYVALE_ID = 'SV';
let CLUSTER_BOEBLINGEN_ID = 'HKS';
let CLUSTER_ABSTATT_ID = 'ABT';

class FileCatalogFile {
    constructor(fileGuid, parentGuid, fileType, fileSize, urls, tags) {
        this.fileGuid = fileGuid;
        this.parentGuid = parentGuid;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.urls = urls || [];
        this.tags = tags;
    }

    static createFromResponseData(data) {
        let urls = [];

        if (data.urls) {
            urls = data.urls.map(url => {
                return new FileCatalogUrl(url.accessor_url, url.creation_date, url.entry_created_by, url.url_status);
            });
        }

        return new FileCatalogFile(data.file_guid, data.parent_guid, data.file_type, data.file_size, urls, data.tags);
    }

}

class FileCatalogUrl {
    constructor(accessorUrl, creationDate, entryCreatedBy, urlStatus) {
        this.accessorUrl = accessorUrl;
        this.creationDate = creationDate;
        this.entryCreatedBy = entryCreatedBy;
        this.urlStatus = urlStatus;
    }

    getCluster() {
        if (this.accessorUrl.includes('059-va2.mbc.de')) {
            return CLUSTER_VAIHINGEN_ID;
        } else if (this.accessorUrl.includes('624.mbc.us')) {
            return CLUSTER_SUNNYVALE_ID;
        } else if (this.accessorUrl.includes('738.mbc.de')) {
            return CLUSTER_BOEBLINGEN_ID;
        } else if (this.accessorUrl.includes('abt')) {
            return CLUSTER_ABSTATT_ID;
        } else {
            return null;
        }
    }
}