/*
The MIT License (MIT)

Copyright (c) 2016 HERE Europe B.V.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

export const mapStyleTest = `
sources:
  omv:
    type: OMV
    max_zoom: 17
    min_display_zoom: 1
# global description of the map, in this example
# the map background color is white
scene:
  background:
    color: global.land_color
# section contains the style information for the layers
# that are present on the map
layers:
  # user defined name of the rendering layer
  water_areas:
    # the section defines where the rendering layer takes
    # its data from source: omv is mandatoru for the Vector Tile API
    # layer: water specifies what vector layer is taken
    # for the rendering see REST API documentation for the
    # list of available layers.
    data: { source: omv, layer: water }
    # section defines how to render the layer
    draw:
      polygons:
        order: 1 # z-order of the layer
        color: [0.055, 0.604, 0.914, 1.00]
  road:
    data: { source: omv, layer: roads }
    draw:
      lines:
        order: 2
        color: [0.561, 0.561, 0.561, 1.00]
        # the width is set in the world meters
        width: 15
    major_road:
      # the filter section narrows down to what features of the
      # data layer the style must be applied to
      filter:
        kind: "major_road"
        kind_detail: "primary"
      draw:
        lines:
          color: [0.882, 0.553, 0.086, 1.00]
          # the width is set in the screen pixels
          width: 5px
      tunnel:
        # the filter adds more specific rules to the
        # tunnel rendering
        filter:
          is_tunnel: true
        draw:
          lines:
            color: [0.192, 0.882, 0.086, 1.00]
    major_road_tertiary:
      filter:
        kind: "major_road"
        kind_detail: "tertiary"
      draw:
        lines:
          color: [0.882, 0.835, 0.086, 1.00]
          width: 3px
`