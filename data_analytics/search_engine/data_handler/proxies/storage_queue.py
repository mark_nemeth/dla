"""Interface to azure storage account queues"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from azure.storage.queue import QueueService
from azure.storage.queue.models import QueueMessageFormat

from config.config_handler import config_dict
from handlers.utils import utils

logger = logging.getLogger(__name__)


class EventListener:

    def __init__(self, queue_name: str):
        """Initialize a new EventListener for a specific queue.

        :param queue_name: Name of the queue listener should interact with
        """
        self.queue_name = queue_name
        self.account_name = config_dict.get_value('storage_queue_account')
        self.sas_token = utils.get_storage_account_access_key(self.account_name)
        self.queue_service = self.connect_azure()

    def connect_azure(self):
        """Connect with azure"""
        queue_service = QueueService(account_name=self.account_name,
                                     sas_token=self.sas_token,
                                     protocol='https',
                                     endpoint_suffix='core.windows.net')
        queue_service.encode_function = QueueMessageFormat.text_base64encode
        queue_service.decode_function = QueueMessageFormat.text_base64decode
        return queue_service

    def fetch_events(self, timeout: int, num: int = 1) -> list:
        """Retrieve events from storage queue.

        :param timeout: visibility timeout in seconds to use when
            retrieving events
        :param num: maximum number of events that should be retrieved
            from the storage queue
        :return: list of msgs
        """
        msgs = []
        try:
            msgs = self.queue_service.get_messages(self.queue_name,
                                                   num_messages=num,
                                                   visibility_timeout=timeout)
        except Exception as e:
            logger.exception(
                f"Retrieving events from storage queue failed: {e}")
        return msgs

    def delete_event(self, event_id: str, pop_receipt: str):
        """Delete a event from Storage Queue

        :param event_id: ID of event to delete
        :param pop_receipt: pop receipt of event to delete
        """
        try:
            self.queue_service.delete_message(
                self.queue_name, event_id, pop_receipt
            )
        except Exception as e:
            logger.exception(f"Deleting event from storage queue failed: {e}")
            return False
        return True
