# Data Crawler - Scanner

The purpose of the scanner container is to scan specific folder and extract metadata needed for the file catalog.
The extracted data is saved in a .csv file on mapr and is read by the second container (Collector), which will add the data to the file catalog.

The scanner defined by the Dockerfile is meant to be run on the Athena Kubernetes cluster in Sunnyvale and Vaihingen.
The scanner defined by the Dockerfile.abstatt is meant to be run on the Abstatt kubernetes cluster. It will also trigger a hadoop job to copy the result to mapr.

## Build Docker Image

Initial configuration of access to the Azure DevOps python package repository
Create and copy a `Personal Access Tokens` in Azure DevOps UI with Packaging (read) privileges.
Export the index URL including the token: `export index_url_with_access=https://<YOUR_TOKEN>@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/`

### Build Docker
#### Linux
```
docker build --build-arg https_proxy=http://localhost:3128 --build-arg http_proxy=http://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access --network="host" -t crawler_scanner:latest .
```

```
docker build --build-arg https_proxy=http://localhost:3128 --build-arg http_proxy=http://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access --network="host" -t crawler_scanner_abstatt:latest -f Dockerfile.abstatt .
```
#### Windows
```
docker build --build-arg https_proxy=http://docker.for.win.localhost:3128 --build-arg http_proxy=http://docker.for.win.localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access -t crawler_scanner:latest .
```
```
docker build --build-arg https_proxy=http://docker.for.win.localhost:3128 --build-arg http_proxy=http://docker.for.win.localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access -t crawler_scanner_abstatt:latest -f Dockerfile.abstatt .
```

## Push to Artifactory (Vaihingen Registry)
A manual push is not required. A merge to integration will trigger a build of the collector and apply the kubernetes int cronjobs on the specified clusters and merge to production will trigger a build of the collector and apply the kubernetes prod cronjobs on the specified clusters.
Remember to bump the versions or a new build will not be triggered! 

## Push to harbor (Abstatt Registry)
Harbor registry requires a manual push. Note, the harbor registry is only reachable from the rd network.
```
docker build --build-arg https_proxy=http://localhost:3128 --build-arg http_proxy=http://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access --network="host" -t registry-prod.athena.corpintra.net/dla-danf/crawler_scanner_abstatt:<version> -f Dockerfile.abstatt .
docker push registry-prod.athena.corpintra.net/dla-danf/crawler_scanner_abstatt:<version>
```

Use the corresponding yml to deploy the scanner.
```
kubectl apply -f ./kubernetes/cronjobs/int/abt/crawler-scanner-cronjob-abt.yml
kubectl apply -f ./kubernetes/cronjobs/prod/abt/crawler-scanner-cronjob-abt.yml
```

