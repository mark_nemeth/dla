__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import (ENV_SPAN_ID, ENV_TRACE_ID,
                                       ENV_TRACE_OPTIONS)
from kfp import dsl
from kubernetes.client.models import V1EnvVar

from config.config_handler import config_dict
from kubeflow.steps.common import (VM_LABEL_ATTRIBUTE, RETRY_NUM,
                                   PIPELINE_POD_LABEL,
                                   BAGFILE_POD_LABEL, FILE_NAME_POD_LABEL,
                                   sanitize_label, METADATA_API_BASE_URL,
                                   APPINSIGHTS_KEY,
                                   DANF_ENV, AUTH, AUTH_TOKEN_URL,
                                   CPU_RESOURCE_TITLE,
                                   MEMORY_RESOURCE_TITLE, TESTING_POD_LABEL,
                                   extract_file_name_from_path,
                                   AUTH_METADATA_AUDIENCE,
                                   AUTH_METADATA_CLIENT_CRED_ID,
                                   AUTH_METADATA_CLIENT_CRED_SECRET,
                                   CUSTOM_NODE_RESOURCE_TITLE,
                                   CUSTOM_STORAGE_VM_TITLE,
                                   MOUNT_DATA_BASE_PATH,
                                   attach_blobfuse_volume,
                                   get_pvc_volume,
                                   AUTH_DATAHANDLER_AUDIENCE,
                                   AUTH_DATAHANDLER_CLIENT_CRED_ID,
                                   AUTH_DATAHANDLER_CLIENT_CRED_SECRET,
                                   AUTH_SEARCH_AUDIENCE,
                                   AUTH_SEARCH_CLIENT_CRED_ID,
                                   AUTH_SEARCH_CLIENT_CRED_SECRET, BaseStep)
from kubeflow.steps.cv1_exporter import Cv1ExporterOp


class DanfExtractorInitOp(BaseStep):
    """Extracts basic bagfile metadata that can be taken from the bagfile
    header. Intended to run as a first flow step."""

    NAME = "danf-extractor-init"
    DOCKER_IMG = config_dict.get_value(NAME)

    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "1Gi"
    MEMORY_LIMIT = "2Gi"

    INPUT_MOUNT_BASE_PATH = "/data"
    OUTPUT_MOUNT_BASE_PATH = "/outputs"

    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               input_location,
               output_container,
               mount_id,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_location: path to the bagfile in the storage account
        :param output_container: container name where the result is saved
        :param mount_id: used to generate unique mount names
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the DanfExtractorInit operation
        """
        step = DanfExtractorInitOp(
            flow_run_guid,
            bagfile_guid,
            input_location.file,
            vin='undefined',
            arguments=['--update_metadata',
                       '--add_state_event']
        )

        if test_mode:
            step.enable_test_mode()

        step \
            .add_kubernetes_resources_for_pod(cls.CPU_REQUEST, cls.MEMORY_REQUEST, cls.CPU_LIMIT, cls.MEMORY_LIMIT) \
            .connect_metadata_api() \
            .enable_proxy() \
            .enable_tracing_integration() \
            .add_authentication_env_variables()

        step.add_env_variable(V1EnvVar(name="BAGFILE_PATH", value=f"{cls.INPUT_MOUNT_BASE_PATH}/{input_location.file}")) \
            .add_env_variable(V1EnvVar(name="OUTPUT_FOLDER", value=cls.OUTPUT_MOUNT_BASE_PATH))

        attach_blobfuse_volume(step,
                               cls.INPUT_MOUNT_BASE_PATH,
                               f"input-container-{mount_id}",
                               input_location.storage_account,
                               input_location.container)
        attach_blobfuse_volume(step,
                               cls.OUTPUT_MOUNT_BASE_PATH,
                               f"output-container-{mount_id}",
                               input_location.storage_account,
                               output_container,
                               False,
                               0,
                               mount_sub_path=input_location.container)
        return step


class DanfExtractorOp(BaseStep):
    """DANF wrapper for running multiple internal and external extractors
    without mono repo dependencies."""

    NAME = "danf-extractor"
    DOCKER_IMG = config_dict.get_value(NAME)
    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "1Gi"
    MEMORY_LIMIT = "3Gi"

    INPUT_MOUNT_BASE_PATH = "/data"
    OUTPUT_MOUNT_BASE_PATH = "/outputs"

    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               input_location,
               output_container,
               mount_id,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_location: path to the bagfile in the storage account
        :param output_container: container name where the result is saved
        :param mount_id: used to generate unique mount names
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the DanfExtractor operation
        """
        step = DanfExtractorOp(
            flow_run_guid,
            bagfile_guid,
            input_location.file,
            vin='undefined',
            arguments=['--update_metadata',
                       '--extractor',
                       'disengagement',
                       'fallback',
                       'dynamic_stop_point',
                       'silent_testing_cv1',
                       'logbook',
                       'guest_logbook',
                       'annotation',
                       '--add_state_event']
        )

        if test_mode:
            step.enable_test_mode()

        step \
            .add_kubernetes_resources_for_pod(cls.CPU_REQUEST, cls.MEMORY_REQUEST, cls.CPU_LIMIT, cls.MEMORY_LIMIT) \
            .connect_metadata_api() \
            .enable_proxy() \
            .enable_tracing_integration() \
            .add_authentication_env_variables()

        step.add_env_variable(V1EnvVar(name="BAGFILE_PATH", value=f"{cls.INPUT_MOUNT_BASE_PATH}/{input_location.file}")) \
            .add_env_variable(V1EnvVar(name="OUTPUT_FOLDER", value=cls.OUTPUT_MOUNT_BASE_PATH))

        attach_blobfuse_volume(step,
                               cls.INPUT_MOUNT_BASE_PATH,
                               f"input-container-{mount_id}",
                               input_location.storage_account,
                               input_location.container)
        attach_blobfuse_volume(step,
                               cls.OUTPUT_MOUNT_BASE_PATH,
                               f"output-container-{mount_id}",
                               input_location.storage_account,
                               output_container,
                               False,
                               0,
                               mount_sub_path=input_location.container)

        return step


# TODO: Move azure flow extractor to new extractor and remove this step
class ExtractorOp(dsl.ContainerOp):

    NAME = "danf-extractor"
    DOCKER_IMG = config_dict.get_value(NAME)
    FILTERED_CONTAINER_MOUNT_PATH = "/output_filtered"
    EXPORTED_CONTAINER_MOUNT_PATH = "/output_exported"
    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "2Gi"
    MEMORY_LIMIT = "8Gi"

    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               mount_id,
               input_storage_account,
               input_container,
               input_file_path,
               output_storage_account,
               output_containers,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param mount_id: used to generate unique mount names
        :param input_storage_account: storage acc. where bagfile is located
        :param input_container: name of the blob storage container where
            the bagfile is located
        :param input_file_path: path to the bagfile in the pod
        :param output_storage_account: storage acc. of output containers
        :param output_containers: dict of blob storage container names
            where dummy files indicating skipping should be written to
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the ExtractorOp
        """
        step = ExtractorOp() \
            .set_retry(RETRY_NUM) \
            .add_pod_label(name=PIPELINE_POD_LABEL, value=sanitize_label(cls.NAME)) \
            .add_pod_label(name=BAGFILE_POD_LABEL, value=sanitize_label(bagfile_guid)) \
            .add_pod_label(name=FILE_NAME_POD_LABEL, value=sanitize_label(extract_file_name_from_path(input_file_path)))

        if test_mode:
            step.add_pod_label(name=TESTING_POD_LABEL, value='true')

        step.add_env_variable(V1EnvVar(name="BAGFILE_PATH", value=input_file_path)) \
            .add_env_variable(V1EnvVar(name="BAGFILE_GUID", value=bagfile_guid)) \
            .add_env_variable(V1EnvVar(name="FLOW_RUN_GUID", value=flow_run_guid)) \
            .add_env_variable(V1EnvVar(name="METADATA_API_BASE_URL", value=METADATA_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="APPINSIGHTS_INSTRUMENTATIONKEY", value=APPINSIGHTS_KEY)) \
            .add_env_variable(V1EnvVar(name="DANF_ENV", value=DANF_ENV)) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_ID, value=tracing.get_propagation_vars().get(ENV_TRACE_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_SPAN_ID, value=tracing.get_propagation_vars().get(ENV_SPAN_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_OPTIONS, value=tracing.get_propagation_vars().get(ENV_TRACE_OPTIONS))) \
            .add_env_variable(V1EnvVar(name="EXPORTER_CUTOFF_DATE_TS", value=str(Cv1ExporterOp.CUTOFF_DATE_TS))) \
            .add_env_variable(V1EnvVar(name="FILTERED_CONTAINER_MOUNT_PATH", value=cls.FILTERED_CONTAINER_MOUNT_PATH)) \
            .add_env_variable(V1EnvVar(name="EXPORTED_CONTAINER_MOUNT_PATH", value=cls.EXPORTED_CONTAINER_MOUNT_PATH)) \
            .add_env_variable(V1EnvVar(name="AUTH", value=AUTH)) \
            .add_env_variable(V1EnvVar(name="AUTH_TOKEN_URL", value=AUTH_TOKEN_URL)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_AUDIENCE", value=AUTH_DATAHANDLER_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_ID", value=AUTH_DATAHANDLER_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_SECRET", value=AUTH_DATAHANDLER_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_AUDIENCE", value=AUTH_METADATA_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_ID", value=AUTH_METADATA_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_SECRET", value=AUTH_METADATA_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_AUDIENCE", value=AUTH_SEARCH_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_ID", value=AUTH_SEARCH_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_SECRET", value=AUTH_SEARCH_CLIENT_CRED_SECRET))

        step.add_resource_request(CUSTOM_NODE_RESOURCE_TITLE, "1") \
            .add_resource_request(CPU_RESOURCE_TITLE, cls.CPU_REQUEST) \
            .add_resource_request(MEMORY_RESOURCE_TITLE, cls.MEMORY_REQUEST) \
            .add_resource_limit(CUSTOM_NODE_RESOURCE_TITLE, "1") \
            .add_resource_limit(CPU_RESOURCE_TITLE, cls.CPU_LIMIT) \
            .add_resource_limit(MEMORY_RESOURCE_TITLE, cls.MEMORY_LIMIT) \
            .add_node_selector_constraint(VM_LABEL_ATTRIBUTE, CUSTOM_STORAGE_VM_TITLE)

        attach_blobfuse_volume(step,
                               MOUNT_DATA_BASE_PATH,
                               f"extraction-mount-{mount_id}",
                               input_storage_account,
                               input_container)
        attach_blobfuse_volume(step,
                               cls.FILTERED_CONTAINER_MOUNT_PATH,
                               f"extraction-mount-filter-{mount_id}",
                               output_storage_account,
                               output_containers.get("filtered_files_container"),
                               False,
                               0)
        attach_blobfuse_volume(step,
                               cls.EXPORTED_CONTAINER_MOUNT_PATH,
                               f"extraction-mount-export-{mount_id}",
                               output_storage_account,
                               output_containers.get("exported_files_container"),
                               False,
                               0)
        return step

    def __init__(self):
        super().__init__(
            name=self.NAME,
            image=self.DOCKER_IMG,
            file_outputs={'significant_metadata_exists': '/tmp/significant_metadata_exists',
                          'bagfile_recording_date': '/tmp/bagfile_recording_date'}
        )