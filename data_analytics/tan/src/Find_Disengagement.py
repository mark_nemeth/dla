import sys
import rosbag
from planner_uptime import evaluate_trajectories
from rosbag_parser import autonomous_trips_parser as atp
import psycopg2
import pandas as pd


def parse_dropouts(bagfile_id):
    bagfile = rosbag.Bag(sys.argv[1])

    topic_planning = ['/planning/PlannedTrajectory2D_layered_map']

    msg_list = {topic: [] for topic in topic_planning}
    for topic, msg, t in bagfile.read_messages(topics=topic_planning):
        msg_list[topic].append((t, msg))
    bagfile.close()

    eval = evaluate_trajectories(topic_planning, msg_list)

    print(eval.drop_out_count)
    print(eval.drop_out_timestamps)

    conn = psycopg2.connect(
        "host=localhost dbname=rca user=postgres password=postgres")
    cur = conn.cursor()
    print("Updating table rca_bagfilewithdropouts with dropout_count")
    cur.execute(
        "UPDATE rca_bagfilewithdropouts SET dropout_count=(%s) WHERE id =(%s)",
        (eval.drop_out_count, bagfile_id))

    if eval.drop_out_count > 0:
        for i in range(0, len(eval.drop_out_timestamps)):
            cur.execute("INSERT INTO rca_disengagements (timestamp, type, details, bagfile_id) VALUES (%s, %s, %s, %s)", (
                str(eval.drop_out_timestamps[i]), 3, "Planning Dropout", bagfile_id))

    conn.commit()


def parse_disengagements(bagfile_id):
    instance = atp.AutonomousTripsParser(sys.argv[1])
    instance.parse_autonomous_trips()
    # print(instance.summary)
    count_disengagement = len(instance.summary)
    print(count_disengagement)
    conn = psycopg2.connect(
        "host=localhost dbname=rca user=postgres password=postgres")
    cur = conn.cursor()
    print("Updating table rca_bagfilewithdropouts with disengagement_count")
    cur.execute(
        "UPDATE rca_bagfilewithdropouts SET disengagement_count=(%s) WHERE id =(%s)",
        (count_disengagement, bagfile_id))

    if count_disengagement > 0:
        df = pd.DataFrame(instance.summary)
        for index, row in df.iterrows():
            print(row['end_cause'], row['end_details'],
                  str(row['end_ts_s']) + str(row['end_ts_ns']))
            cur.execute(
                "INSERT INTO rca_disengagements (timestamp, type, details, distance, bagfile_id) VALUES (%s, %s, %s, %s, %s)",
                (str(row['end_ts_s']) + str(row['end_ts_ns']), row['end_cause'], row['end_details'], row['distance'], bagfile_id))

    conn.commit()

if __name__ == "__main__":

    bagfile = rosbag.Bag(sys.argv[1])
    topic_vin = '/vehicle_id'
    for msg in bagfile.read_messages(topics=[topic_vin]):
        vin = str(msg.message.data)

    conn = psycopg2.connect(
        "host=localhost dbname=rca user=postgres password=postgres")
    cur = conn.cursor()

    print("Inserting into table rca_bagfilewithdropouts")
    cur.execute(
        "INSERT INTO rca_bagfilewithdropouts (filename, vehicle) VALUES (%s, %s) RETURNING id",
        (sys.argv[1], vin))
    conn.commit()
    bagfile_id = cur.fetchone()[0]

    # parse_dropouts(bagfile_id)
    parse_disengagements(bagfile_id)
