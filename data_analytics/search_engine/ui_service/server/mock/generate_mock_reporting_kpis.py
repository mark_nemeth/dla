import json
import os
from generate_mock_filters import *

OUTPUT_FILENAME = "reporting_kpis.json"

data = {'kpis':[
 {
            'id': 'days_eng',
            'label': 'Days of engaged testing',
            'displayValue': 183
          },
          {
            'id': 'total_diseng',
            'label': 'Total number of disengagements',
            'displayValue': 1871
          },
          {
            'id': 'man_unplanned_diseng',
            'label': 'Number of manual unplanned disengagements',
            'displayValue': 731
          },
          {
            'id': 'err_diseng',
            'label': 'Number of error disengagements',
            'displayValue': 1140
          },
          {
            'id': 'man_planned_diseng',
            'label': 'Number of manual planned disengagements',
            'displayValue': 9087
          },
          {
            'id': 'miles_pub',
            'label': 'Autonomous miles on public roads',
            'displayValue': 9875.5
          },
          {
            'id': 'miles_per_diseng',
            'label': 'Miles per disengagement',
            'displayValue': 5.2782
          },
          {
            'id': 'trips_missing_diseng_info',
            'label': 'Public trips with invalid or no diseng. info',
            'displayValue': 65
          },
          {
            'id': 'trips_negl_distance',
            'label': 'Public trips with negligible distance',
            'displayValue': 230
          },
          {
            'id': 'trips_negl_duration',
            'label': 'Public trips with negligible duration',
            'displayValue': 366
          }
    
]
}

result = {"results": data}

outpath = os.path.join("public/mockdata")
if not os.path.exists(outpath):
    os.makedirs(outpath)

outfilePath = os.path.join(outpath, OUTPUT_FILENAME)
with open(outfilePath, 'w') as outfile:
    json.dump(result, outfile, indent=4)

print("Mock json file created: " + outfilePath)
