import React from 'react';
import {ProgressBar, Row} from 'react-bootstrap';
import CollapseContainer from '../../Utils/CollapsContainer/CollapseContainer';
import {sizeConverter, statusConverter, timestampConverter} from '../../../utils/valueConverter';
import LabelContainer from '../../Utils/LabelContainer/LabelContainer';
import LabelEntry from '../../AttributeFormatter/Label/LabelEntry';
import SecondTimeEntry from '../../AttributeFormatter/SecondTime/SecondTimeEntry';
import DateTimeEntry from '../../AttributeFormatter/DateTime/DateTimeEntry';
import SizeEntry from '../../AttributeFormatter/Size/SizeEntry';
import NameListEntry from '../../AttributeFormatter/Name/NameList/NameListEntry';
import IngestStatusEntry from "../IngestStatusEntry/IngestStatusEntry";
import './IngestDetailBox.scss';

export default function IngestDetailBox(props) {

  const collapseHeader = "Ingest " + props.ingest.id + "  -  "
      + sizeConverter(props.ingest.size)
      + (props.ingest.endTime
              ? "  -  End: " + timestampConverter(props.ingest.endTime)
              : "  -  Created: " + timestampConverter(props.ingest.created)
      ) + "  -  "
      + statusConverter(props.ingest.status, true)

  function getIngestFileList() {
    return props.ingest.files.map(file => {
      return {
        guid: undefined,
        name: file.target ? file.target : file.url
      };
    });
  }

  return (
      <div className='ingest-card mb-4'>
        <CollapseContainer label={collapseHeader}
                           active={false}
                           variant="list"
                           preexpanded={props.expanded}
                           useBodyStyle={true}
                           key={props.ingest.id}>
          <div className='ingest-card-body px-3'>
            <Row className='pt-2'>
              <LabelContainer label="Vehicle Name" variant="fifty-percent-card card-style-filled">
                <LabelEntry value={props.ingest.carName}/>
              </LabelContainer>
              <LabelContainer label="Transfer Status" variant="fifty-percent-card card-style-filled">
                <IngestStatusEntry status={props.ingest.status} ingestId={props.ingest.id}/>
              </LabelContainer>
            </Row>
            {
              props.ingest && props.ingest.status === 'STARTED' &&
              <Row className='pt-2'>
                <LabelContainer label="Progress" variant="seventy-percent-card card-style-filled-padding-left-right">
                  <div className="progress-bar-wrapper">
                    <ProgressBar variant="info" now={props.ingest.progress}
                                 label={`${(props.ingest.progress).toFixed(2)}%`}/>
                  </div>
                </LabelContainer>
                <LabelContainer label="Estimated Time of Arrival" variant="thirty-percent-card card-style-filled">
                  <SecondTimeEntry time_in_seconds={props.ingest.etaSeconds}/>
                </LabelContainer>
              </Row>
            }
            <Row className='pt-2'>
              <LabelContainer label="Start" variant="fifty-percent-card card-style-filled">
                <DateTimeEntry timestamp={props.ingest.startTime}/>
              </LabelContainer>
              {
                props.ingest && props.ingest.status !== 'STARTED' &&
                <LabelContainer label="End" variant="fifty-percent-card card-style-filled">
                  <DateTimeEntry timestamp={props.ingest.endTime}/>
                </LabelContainer>
              }
              {
                props.ingest && props.ingest.status === 'STARTED' &&
                <LabelContainer label="Last Updated" variant="fifty-percent-card card-style-filled">
                  <DateTimeEntry timestamp={props.ingest.lastUpdated}/>
                </LabelContainer>
              }
            </Row>
            <Row className='pt-2'>
              <LabelContainer label="Size" variant="fifty-percent-card card-style-filled">
                <SizeEntry size={props.ingest.size}/>
              </LabelContainer>
              <LabelContainer label="Ingest Cluster" variant="fifty-percent-card card-style-filled">
                <LabelEntry value={props.ingest.ingestCluster}/>
              </LabelContainer>
            </Row>
            <Row className='pt-2'>
              <LabelContainer label="Files" variant="hundred-percent-card card-style-filled">
                <NameListEntry fileType={undefined} files={getIngestFileList()} enableSearchBar={true}/>
              </LabelContainer>
            </Row>
          </div>
        </CollapseContainer>
      </div>
  )
};