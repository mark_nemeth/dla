__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from enum import Enum
import os


def danf_env():
    env = os.environ.get('DANF_ENV')
    if not env:
        raise ValueError("DANF_ENV environment variable missing")
    return DanfEnv(env)


class Platform(Enum):
    LOCAL = 'local'
    AZURE = 'az'
    ON_PREM = 'sv'
    ON_PREM_SV = 'sv'
    ON_PREM_VAI = "vai"


class Stage(Enum):
    DEV = 'dev'
    SBX = 'sbx'
    INT = 'int'
    PROD = 'prod'


PLATFORM_MAPPING = {
    'local': Platform.LOCAL,
    'az_dev' : Platform.AZURE,
    'dev': Platform.AZURE,
    'sbx': Platform.AZURE,
    'az_int': Platform.AZURE,
    'az_prd': Platform.AZURE,
    'prod': Platform.AZURE,
    'sv_prod': Platform.ON_PREM,
    'sv_int': Platform.ON_PREM,
    'vai_prod':Platform.ON_PREM_VAI,
    'vai_int': Platform.ON_PREM_VAI,
}


STAGE_MAPPING = {
    'local': Stage.DEV,
    'az_dev': Stage.DEV,
    'dev': Stage.DEV,
    'sbx': Stage.SBX,
    'az_int': Stage.INT,
    'az_prd': Stage.PROD,
    'prod': Stage.PROD,
    'sv_prod': Stage.PROD,
    'sv_int': Stage.INT,
    'vai_prod': Stage.PROD,
    'vai_int': Stage.INT
}


class DanfEnv:

    def __init__(self, danf_env_str: str):
        self.danf_env = danf_env_str
        """Raw DANF_ENV string"""

        self.stage = STAGE_MAPPING.get(danf_env_str)
        """Represents the stage of this environment, e.g. dev, prod, ..."""

        self.platform = PLATFORM_MAPPING.get(danf_env_str)
        """Represents the platform of this environment, e.g. azure,
        on premise, ..."""

        if not self.stage:
            raise ValueError(f"Unknown stage for DANF_ENV: {danf_env_str}")

        if not self.platform:
            raise ValueError(f"Unknown platform for DANF_ENV: {danf_env_str}")
