"""Search Engine and Metadata REST API child bagfiles route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants
from DANFUtils.exceptions import GUIDNotFoundException

from handlers.auth import auth
from handlers.child_bagfiles import ChildBagFilesHandler
from model.exceptions import GuidNotFoundError
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


class ChildbagfilesRequestHandler(BaseRequestHandler):
    """Request handler for child bagfiles route: /childbagfile"""

    _schema_post = BaseRequestHandler.load_json_schema(
        "child_bagfile_metadata_post.schema")

    child_bagfile_handler = ChildBagFilesHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('childbagfiles/post',
                                      context_headers=self.request.headers)
        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_post)

        guid = self.child_bagfile_handler.add_child_bagfile_to_db(data)
        res = 'Child Bagfile(s) succesfully added to db.'

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res,
            status_code=constants["REQ_STATUS_CODE_RESOURCE_CREATED"],
            location="/childbagfile/" + guid)


class ChildbagfileRequestHandler(BaseRequestHandler):
    """Request handler for child bagfile route: /childbagfile/{guid}"""

    _schema_guid = BaseRequestHandler.load_json_schema(
        "guid.schema")
    _schema_patch = BaseRequestHandler.load_json_schema(
        "child_bagfile_metadata_patch.schema")

    child_bagfile_handler = ChildBagFilesHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET, PATCH, DELETE')

    @auth.requires_entitlements(["openid"])
    def get(self, guid):
        tracing.start_task_in_context('childbagfiles/get',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        try:
            res = self.child_bagfile_handler.get_child_bagfile_from_db(guid)
        except GUIDNotFoundException as e:
            raise GuidNotFoundError(guid, 'childbagfile') from e

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def patch(self, guid):
        tracing.start_task_in_context('childbagfiles/patch',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_patch)

        try:
            self.child_bagfile_handler.update_child_bagfile_in_db(guid, data)
        except GUIDNotFoundException as e:
            raise GuidNotFoundError(guid, 'childbagfile') from e

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results="Child bagfile updated",
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def delete(self, guid):
        tracing.start_task_in_context('childbagfiles/delete',
                                      context_headers=self.request.headers)
        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        hard_delete = self.get_query_argument("hard_delete", default="False") == 'True'

        try:
            res = self.child_bagfile_handler.delete_child_bagfile_from_db(guid, hard_delete)
        except GUIDNotFoundException as e:
            raise GuidNotFoundError(guid, 'childbagfile') from e

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
