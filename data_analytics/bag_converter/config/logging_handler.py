__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

__doc__ = '''
This implementation is following the singloton design pattern
https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html
Handler class for logging
'''

import logging

from config import config_handler as configurator


class LoggingHandler:
    __loggger = None

    def __init__(self):
        """ Virtually private constructor. """
        if LoggingHandler.__loggger is None:
            raise Exception("This class is a singleton!, please call log")

    @classmethod
    def getHandler(self):
        """ Static access method. """
        if LoggingHandler.__loggger == None:
            logger_name = 'worker'
            fmt = '%(levelname).1s%(asctime)s.%(msecs).03d %(process)d ' \
                  '%(filename)s:%(lineno)d] %(message)s'
            datefmt = None
            log_level_env = configurator.get_value('log_level_env')

            log = logging.getLogger(name=logger_name)
            handler = logging.StreamHandler()
            formatter = logging.Formatter(fmt=fmt, datefmt=datefmt)
            handler.setFormatter(formatter)
            log.addHandler(handler)

            if log_level_env == 'info':
                log.setLevel(logging.INFO)
            else:
                log.setLevel(logging.DEBUG)
            LoggingHandler.__loggger = log
        return LoggingHandler.__loggger


def setLevel(level):
    lh = LoggingHandler.getHandler()
    lh.setLevel(level)


def debug(msg, *args, **kwargs):
    lh = LoggingHandler.getHandler()
    lh.debug(msg, *args, **kwargs)


def info(msg, *args, **kwargs):
    lh = LoggingHandler.getHandler()
    lh.info(msg, *args, **kwargs)


def warning(msg, *args, **kwargs):
    lh = LoggingHandler.getHandler()
    lh.warning(msg, *args, **kwargs)


def error(msg, *args, **kwargs):
    lh = LoggingHandler.getHandler()
    lh.error(msg, *args, **kwargs)


def exception(msg, *args, exc_info=True, **kwargs):
    lh = LoggingHandler.getHandler()
    lh.exception(msg, *args, exc_info, **kwargs)


def critical(msg, *args, **kwargs):
    lh = LoggingHandler.getHandler()
    lh.critical(msg, *args, **kwargs)


def log(level, msg, *args, **kwargs):
    lh = LoggingHandler.getHandler()
    lh.log(level, msg, *args, **kwargs)
