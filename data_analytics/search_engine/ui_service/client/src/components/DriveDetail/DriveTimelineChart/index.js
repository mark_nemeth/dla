import {connect} from 'react-redux';
import {DriveTimelineChart} from "./DriveTimelineChart";
import {showSelectedDriveItem} from "../../../redux/Overview/OverviewActions";

export default connect(
    null,
    {showSelectedDriveItem}
)(DriveTimelineChart);