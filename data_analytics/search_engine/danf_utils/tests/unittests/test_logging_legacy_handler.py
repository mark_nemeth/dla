__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from DANFUtils.logging import logger


class TestLegacyLoggingHandler(unittest.TestCase):

    def test_all_log_levels(self):
        logger.set_log_level('debug')
        logger.debug("debug log")
        logger.info("info log")
        logger.warning("warning log")
        logger.error("error log")
        try:
            dict()['nonexisting_key']
        except:
            logger.exception("exception log")

    def test_changing_log_levels(self):
        logger.set_log_level('warning')
        logger.debug("should not show")
        logger.info("should not show")
        logger.warning("should show")
        logger.error("should show")


if __name__ == '__main__':
    unittest.main()