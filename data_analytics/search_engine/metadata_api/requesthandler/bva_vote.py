__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from requesthandler.base_request_handler import BaseRequestHandler
from handlers.bva_vote import BvaVoteHandler
from handlers.auth import auth
from handlers.utils import utils
from model.bva_vote import VoteRequest, VotePatchRequest


class BvaVoteRequestHandler(BaseRequestHandler):
    _schema_guid = BaseRequestHandler.load_json_schema("guid.schema")

    def initialize(self):
        self._vote_handler = BvaVoteHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST, GET, PATCH, DELETE')

    @auth.requires_entitlements(["openid"], requires_account=True)
    def post(self):
        tracing.start_task_in_context('vote/post',
                                      context_headers=self.request.headers)

        create_vote_req = self.deserialize(self.request.body, VoteRequest)

        res = self._vote_handler.create_new_vote(create_vote_req,
                                                 self.current_user.account)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res.dict(),
            status_code=constants['REQ_STATUS_CODE_RESOURCE_CREATED'],
            location="/bva/votes")

    @auth.requires_entitlements(["openid"], requires_account=True)
    def get(self):
        tracing.start_task_in_context('vote/get',
                                      context_headers=self.request.headers)

        votes = self._vote_handler.get_votes(self.current_user.account)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=[vote.dict() for vote in votes],
            status_code=constants['REQ_STATUS_CODE_OK']
        )

    @auth.requires_entitlements(["openid"], requires_account=True)
    def delete(self, guid):
        tracing.start_task_in_context('vote/delete',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        res = self._vote_handler.delete_vote(guid, self.current_user.account)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res.dict(),
            status_code=constants['REQ_STATUS_CODE_OK']
        )

    @auth.requires_entitlements(["openid"], requires_account=True)
    def patch(self, guid):
        tracing.start_task_in_context('vote/patch',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        update_vote_req = self.deserialize(self.request.body, VotePatchRequest)

        res = self._vote_handler.update_vote(guid, update_vote_req,
                                             self.current_user.account)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res.dict(),
            status_code=constants['REQ_STATUS_CODE_OK']
        )
