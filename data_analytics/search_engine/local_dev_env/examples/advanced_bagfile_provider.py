from bagfile_provider import BagFileProvider


# Test data (used as template)
bag = {
    "version": "1.0",
    "guid": "39a758db-7f24-4350-ae7a-d16259ecbe95",
    "link": "testdata",
    "file_name": "testdata1.bag",
    "size": 21231,
    "num_messages": 4444,
    "start": 1563262908000,
    "end": 1563262911001,
    "duration": 3001,
    "vehicle_id_num": "V-123-234",
    "topics": ["topic2"],
    "drive_types": ["CID"],
    "is_test_data": 1
}

child = {
    "version": "1.0",
    "guid": "39a758db-7f24-4350-ae7a-d16259ecbe01",
    "parent_guid": "39a758db-7f24-4350-ae7a-d16259ecbe95",
    "link": "testdata",
    "file_name":
    "20200130_152349_cid_guest_drive_part2_0_EXTRACTEDBY_LogbookIssueInterface_at_20200130_152926.bag",
    "start": 1558962320000,
    "end": 1558962372000,
    "parent_index_start": 169,
    "parent_index_end": 220,
    "size": 32768,
    "vehicle_id_num": "V-123-234",
    "metadata": {
        "logbook": ""
    },
    "bva": 7,
    "is_test_data": 1
}

# take "template" and append end to it, while ensuring that len(created string)==len(template)
# so basically replacing the end of the template with the new_end
def replace_end(template, new_end):
    return template[0:-len(new_end)] + new_end


def build_childbags(amount, guid_offset, parent_guid, vehicle_id_num):
    child_bagfiles = []

    # to simplify the code in the calling method, we allow passing 0 as amount here
    # this basically means --> no childbags
    if amount > 0:
        guid_template = child['guid']
        for n in range(1, amount):
            next_childbag = child.copy()

            # make sure the guids do not start with 1 again for next parent
            guid = replace_end(guid_template, str(n + guid_offset))
            next_childbag['guid'] = guid
            next_childbag['parent_guid'] = parent_guid
            next_childbag['vehicle_id_num'] = vehicle_id_num
            child_bagfiles.append(next_childbag)

    return child_bagfiles


def create_data(max_bags, make_children):
    bagfiles =[]
    child_bagfiles = []

    # dict_items([('version', '1.0'), ('guid', '39a758db-7f24-4350-ae7a-d16259ecbe95'), ('link', 'testdata'), ('file_name', 'testdata1.bag'), ('size', 21231), ('num_messages', 4444), ('start', 1563262908000), ('end', 1563262911001), ('duration', 3001), ('vehicle_id_num', 'V-123-234'), ('topics', ['topic2']), ('drive_types', ['CID']), ('is_test_data', 1)])
    guid_template = bag['guid']
    vehicle_id_template = bag['vehicle_id_num']
    for n in range(1,max_bags + 1):
        next_bag = bag.copy()

        # make sure that the guid ends with a unique 3 digit (001 to 999)
        guid = replace_end(guid_template, f'{n:03}')

        # make sure that the vehicle_id ends with a unique 3 digit (001 to 999)
        vehicle_id_num = replace_end(vehicle_id_template, f'{n:03}')
        next_bag['guid'] = guid
        next_bag['vehicle_id_num'] = vehicle_id_num

        # make sure that the bags start at different times
        next_bag['start'] = next_bag['start'] + n
        next_bag['end'] = next_bag['end'] + n

        bagfiles.append(next_bag)

        # maximum are 9 child-bags (achieved with the modulo-operator)
        # we pass in how many child-bags we already have to avoid duplicate guids for the child-bags
        if (make_children):
            child_bagfiles.extend(build_childbags(n % 10, len(child_bagfiles), guid, vehicle_id_num))

    return bagfiles, child_bagfiles


# vehicle_id_num is V-123-nnn (where nnn is from 001 to 999)
# bag 1 has 1 child
# bag 2 has 2 children
# bag 3 has 3 children
# bag 4 has 4 children
# bag 5 has 5 children
# ...
# bag 9 has 9 children
# bag 10 has 0 children
# bag 11 has 1 child
# ...

class AdvancedBagFileProvider(BagFileProvider):

    def __init__(self, settings):
        super().__init__()
        bag_count = settings['bag_count']
        generate_child_bags = settings['include_child_bags']
        self.bagfiles, self.child_bagfiles = create_data(bag_count, generate_child_bags)

    def get_bagfiles(self):
       return self.bagfiles


    def get_child_bagfiles(self):
        return self.child_bagfiles