import httpMocks from 'node-mocks-http'
import userHandler from './userHandler';
import storeProvider from '../../models/storeProvider';

describe('userhandler', () => {

  it('should load default user information in dev mode', () => {
    const req = httpMocks.createRequest({
      headers: {origin: 'http://localhost:3000'},
    });

    return userHandler.getUserInformation(req).then(userInfo => {
      expect(userInfo).toStrictEqual({name: 'DANFTestUser', admin: true});
    });
  });

  it('should return empty user when no session exists', () => {
    const req = httpMocks.createRequest();
    return userHandler.getUserInformation(req).then(userInfo => {
      expect(userInfo).toStrictEqual({name: null, admin: false});
    });
  });

  it('should return real user from redis', () => {
    const req = httpMocks.createRequest({
      user: {id: 'rootUser123'}
    });
    storeProvider.getUserDataAsync = jest.fn(() => {return Promise.resolve({displayName: 'rootUser123'})});
    return userHandler.getUserInformation(req).then(userInfo => {
      expect(userInfo).toStrictEqual({name: 'rootUser123', admin: false});
    });
  });
});