import {connect} from 'react-redux';
import {filterByDriveType} from '../../../../redux/Home/HomeActions';
import {ByDriveTypeFilter} from './ByDriveTypeFilter';

const driveMap = new Map([
  ['CID', 'CID'],
  ['CTD', 'CTD']]
);

function mapStateToProps(state, ownProps) {
  const driveList = driveMap || [];
  const selectedDriveTypeList = state.filters.drive_types || [];
  return {driveList, selectedDriveTypeList};
}

export default connect(
  mapStateToProps,
  {filterByDriveType}
)(ByDriveTypeFilter);