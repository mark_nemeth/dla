import {connect} from 'react-redux';
import {filterByDrivers} from '../../../../redux/Home/HomeActions';
import {ByDriverFilter} from './ByDriverFilter';

function mapStateToProps(state, ownProps) {
  let driverList = state.filterOptions.driverList || [];
  const selectedDriverList = state.filters.driver || [];

  return {selectedDriverList, driverList};
}

export default connect(
  mapStateToProps,
  {filterByDrivers}
)(ByDriverFilter);