import React from 'react';
import Form from "react-bootstrap/Form";
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import InputRange from 'react-input-range';
import {isObjectValid} from '../../../../utils/helperFunctions';
import 'react-input-range/lib/css/index.css';
import './BySizeFilter.scss';

export const STATIC_LIMITS = {
  ORIGINALFILE: {
    min: 0,
    max: 300 * 1000000000
  },
  ALWAYSONFILE: {
    min: 0,
    max: 300 * 1000000000
  },
  CHILD_BAGFILE: {
    min: 0,
    max: 250 * 1000000000
  },
  DRIVE: {
    min: 0,
    max: 300 * 1000000000
  }
};

export function BySizeFilter(props) {

  function convertToByte(value) {
    return {
      min: value.min * 1000000000,
      max: value.max * 1000000000
    }
  }

  function onRangeSelection(value) {
    value = convertToByte(value);
    if (isObjectValid(value)) props.filterBySize(value);
    else props.filterBySize({min: null, max: null});
  }

  function resetRangeSelection() {
    props.setCurrentSizeRange(props.limits);
    props.filterBySize({min: null, max: null});
  }

  const subLabel = props.size.min + ' - ' + props.size.max + ' GB';
  const active = props.size.min !== props.limits.min || props.size.max !== props.limits.max;

  return (
    <CollapseContainer label={active ? "Size: " + subLabel : "Size"}
                       active={active}
                       resetCallback={resetRangeSelection}
    >
      <div className="track-container">
        <Form>
          <InputRange
            draggableTrack
            step={1}
            maxValue={props.limits.max}
            minValue={props.limits.min}
            onChange={value => props.setCurrentSizeRange(value)}
            onChangeComplete={onRangeSelection}
            value={props.size}
          />
        </Form>
      </div>
    </CollapseContainer>
  );
}