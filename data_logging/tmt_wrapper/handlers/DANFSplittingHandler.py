"""DANF Splitting Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from handlers.AbstractHandler import AbstractHandler
from constants import *


class DANFSplittingHandler(AbstractHandler):

    def __init__(self, config, tmt_path):

        super(DANFSplittingHandler, self).__init__(config, SCHEMA_FILE_DANF_SPLITTING, tmt_path)

    def validate_config_fields(self):
        for t in self.config['metadata_list']:
            # validate timestamps
            assert int(t['start']) < int(
                t['end']), "End timestamp {} must be greater than the start timestamp {}.".format(
                t['end'], t['start'])
            digits_start = self.count_digits(t['start'])
            digits_end = self.count_digits(t['end'])
            if digits_start == 13:
                t['start'] = t['start'] * 1000000
            elif digits_start != 19:
                raise ValueError("'start' must be provided as millisecond or nanosecond timestamp")
            if digits_end == 13:
                t['end'] = t['end'] * 1000000
            elif digits_end != 19:
                raise ValueError("'end' must be provided as millisecond or nanosecond timestamp")

    def build_cmd_list(self):
        cmd_list = []
        for t in self.config['metadata_list']:
            cmd = [self.tmt_path]
            cmd.extend(["--in", self.config['input_file_paths'][0]])
            cmd.extend(["--time", str(t['start']), str(t['end'])])
            cmd.extend(
                ["--out", "{}".format(
                    self.add_file_name_appendix(self.config['output_file_path'], "SPLIT_{}".format(t['start'])))])
            cmd_list.append(cmd)
        return cmd_list
