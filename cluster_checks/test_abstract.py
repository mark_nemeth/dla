#import os
from abc import ABCMeta, abstractmethod


class AbstractTest():
    __metaclass__ = ABCMeta

    def __init__(self, path):
        self.path = path

    @abstractmethod
    def run(self):
        pass