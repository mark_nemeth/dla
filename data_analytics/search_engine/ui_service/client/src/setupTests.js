import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';

configure({adapter: new Adapter()});

export function getTestFiltersList() {
  return {
    results: {
      "duration_min_max": [
          14,
          752728000
      ],
      "locations": [
          "DE"
      ],
      "size_min_max": [
          7533,
          200032184685
      ],
      "topics": [
          "/diagnostics_toplevel_state",
          "/radar/RadarLocationAndRSIService/RadarMmrRL_S/RadarLocationAndRSI",
          "/sem/SemComService/0/SemComInterface",
          "/stereo_vision_adc_DaddyROSGateway/RosExportImagestereoParameterConfig/parameter_updates",
          "/cal/CalService/0/DsmInterface",
          "/licam2_DaddyROSGateway/mrc_image_exporterMRCRL1ParameterConfig/parameter_updates",
          "/licam1_DaddyROSGateway/StixelLidarVeloRoofFLParameterConfig/parameter_descriptions",
          "/licam1_DaddyROSGateway/StixelCommunicatorLidarVeloRoofRRParameterConfig/parameter_updates",
          "/cal/CalService/0/VehicleDataInterface",
          "/lidar/LidarPointCloudService/LidarVeloRoofRL/LidarPointCloudInterface",
          "/introspection/grid_fusion_timings/FilteringLidarGroundHeightMap",
          "/radar/RadarCRRService/0/RadarCRRInterface",
          "/driver_logbook",
          "/sem/SemClientService/Rsi/SemClientInterface",
          "/stereo/StereoLostCargoInterfaceService/StereoCameraFront/StereoLostCargoInterface",
          "/licam1_DaddyROSGateway/mrc_image_exporterMRCRR2ParameterConfig/parameter_descriptions",
          "/radar/RadarARS4xxNearRangeService/48/ARS4xxTargetListNearInterface",
          "/fusion/FusionTrafficLightService/0/FusionTrafficLightInterface",
          "/sem/SemClientService/LocalTrajectoryProcessorHanf/SemClientInterface",
          "/radar/RadarARS4xxFarRangeService/44/ARS4xxTargetListFarInterface",
          "/stereo/TLDetector/brightspot/TLDetectorInterface",
          "/lidar/LidarPointCloudService/LidarVeloRoofRR/LidarPointCloudInterface",
          "/sem/SemReporterService/0/SemConfigInterface",
          "/stereo/git/runtime_tag",
          "/stereo/ColorImage/stereo/ColorImageRight",
          "/stereo/DisparityWithConfidenceRaw/StereoCameraFront/Trigger",
          "/sem/SemClientService/pedestrian_detection_MRCRR2/SemClientInterface",
          "/stereo_vision_adc_DaddyROSGateway/StereoSensorPluginSenderstereoParameterConfig/parameter_descriptions",
          "/environment_modeling/environment_model_at_t_fusion_raw",
          "/licam2_DaddyROSGateway/VelodyneDecoderLidarVeloRoofFRParameterConfig/parameter_descriptions",
          "/radar/RadarLocationAndRSIService/RadarMmrRL_SW/RadarLocationAndRSI",
          "/planning/PlanningEnvironmentModelService/0/PlanningEnvironmentModelInterface",
          "/vulnerable_road_users/PedestrianBoundingBoxService/MRCRR1/PedestrianBoundingBoxEvent",
          "/lidar/ObjectDetection/LidarVeloBumperFL/BoxList"
      ],
      "vehicle_ids": [
          "lab02",
          "v222-5253",
          "v222-5215",
          "V-123-234",
          "unknown",
          "v222-5214",
          "v222-5252",
          "v222-5410",
          "v222-5213",
          "v222-5218",
          "v222-5251",
          "v222-5217"
      ],
      "driven_by_values": [
        "driven_by-me-1",
        "driven_by-me-2",
        "driven_by-me-3",
        "driven_by-me-4",
        "unknown",
        "driven_by-me-5",
        "driven_by-me-6",
        "driven_by-me-7",
        "driven_by-me-8",
        "driven_by-me-9"
      ],
      "drivers": [
        "Driver-1",
        "Driver-2",
        "Driver-3",
        "Driver-4",
        "unknown",
        "Driver-5",
        "Driver-6",
        "Driver-7",
        "Driver-8",
        "Driver-9"
      ],
      "operators": [
        "Operator-1",
        "Operator-2",
        "Operator-3",
        "Operator-4",
        "unknown",
        "Operator-5",
        "Operator-6",
        "Operator-7",
        "Operator-8",
        "Operator-9"
      ],
      "hardware_releases": [
        "Release-1",
        "Release-2",
        "Release-3",
        "Release-4",
        "unknown",
        "Release-5",
        "Release-6",
        "Release-7",
        "Release-8",
        "Release-9"
      ]
    }
  }
}

export function getTestBagfileList(singleBagfile = false) {
  if (singleBagfile) {
    return {
      "bagfiles": [{
        "_id": "9217ea0ad7f44c2caa655c5110553779",
        "link": "/input/plog/WDD2221591A668818/2019/07/9/20190712_103708_test.bag",
        "size": 362172470,
        "num_messages": 5665488,
        "start": 1516299527033,
        "end": 1516301755313,
        "duration": 2228280,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4"
      }]
    }
  }
  return {
    "bagfiles": [
      {
        "_id": "9217ea0ad7f44c2caa655c5110553779",
        "link": "/input/plog/WDD2221591A668818/2019/07/9/20190712_103708_test.bag",
        "size": 362172470,
        "num_messages": 5665488,
        "start": 1516299527033,
        "end": 1516301755313,
        "duration": 2228280,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4"
      },
      {
        "_id": "95669fed260741768a45b0a86b1ab3d4",
        "link": "/input/plog/WDD2221591A431086/2019/07/24/20190712_103708_test.bag",
        "size": 627875310,
        "num_messages": 7186872,
        "start": 1533510779798,
        "end": 1533516556729,
        "duration": 5776931,
        "vehicle_id_num": "v222-8901",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "dc3effbd-cc8c-42fa-99de-cde7c21e3c90"
      },
      {
        "_id": "8b4f89f2d7a64bf085f180cda87b0816",
        "link": "/input/plog/WDD2221591A215870/2019/07/20/20190712_103708_test.bag",
        "size": 813197491,
        "num_messages": 2931648,
        "start": 1491594590826,
        "end": 1491599681911,
        "duration": 5091085,
        "vehicle_id_num": "v222-8901",
        "extractor": "ext1",
        "version": "0.1",
        "guid": "eb53f61a-b447-4a99-af4c-a7c45877cbf7"
      },
      {
        "_id": "70645295ca38463bb3a9460eeb385710",
        "link": "/input/plog/WDD2221591A162572/2019/07/21/20190712_103708_test.bag",
        "size": 258093942,
        "num_messages": 1436516,
        "start": 1495980458612,
        "end": 1495983300584,
        "duration": 2841972,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "75213b41-d855-4f5a-8449-920bcc6f6cba"
      },
      {
        "_id": "8fb9261374334a06862867f5644670e6",
        "link": "/input/plog/WDD2221591A983834/2019/07/1/20190712_103708_test.bag",
        "size": 363703293,
        "num_messages": 7437565,
        "start": 1538279975829,
        "end": 1538282178970,
        "duration": 2203141,
        "vehicle_id_num": "v222-1234",
        "extractor": "ext1",
        "version": "0.1",
        "guid": "f453abca-8e6e-430b-b0bc-b9e0af9e7823"
      },
      {
        "_id": "e65e8e75d155466995800fa43c3d18d8",
        "link": "/input/plog/WDD2221591A19805/2019/07/12/20190712_103708_test.bag",
        "size": 565453034,
        "num_messages": 2328162,
        "start": 1500707052674,
        "end": 1500713680114,
        "duration": 6627440,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "cfade75e-9b99-4ccb-a883-5e0807ca4bc6"
      },
      {
        "_id": "22e8ae442c5d4709969e4ed1820fe539",
        "link": "/input/plog/WDD2221591A324855/2019/07/14/20190712_103708_test.bag",
        "size": 753270018,
        "num_messages": 5503162,
        "start": 1504793789656,
        "end": 1504797137620,
        "duration": 3347964,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "78edd80c-f0db-4df2-b1a2-4fcd70460a17"
      },
      {
        "_id": "7f1ca6a2fbe04dfcb87e0a842cc29659",
        "link": "/input/plog/WDD2221591A857185/2019/07/1/20190712_103708_test.bag",
        "size": 538718048,
        "num_messages": 9194080,
        "start": 1505531458146,
        "end": 1505538265437,
        "duration": 6807291,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "dba9fa1a-cdec-4e4b-99f3-7c2f2edc7e57"
      },
      {
        "_id": "670ac974e53c423e9c0fbb5971f25b42",
        "link": "/input/plog/WDD2221591A478516/2019/07/13/20190712_103708_test.bag",
        "size": 177561948,
        "num_messages": 3063593,
        "start": 1506864952301,
        "end": 1506866469251,
        "duration": 1516950,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "c9a66fc1-e49c-463a-9c2e-2fba6530585b"
      },
      {
        "_id": "d42427107415442591b29c5e78e5a61e",
        "link": "/input/plog/WDD2221591A389239/2019/07/12/20190712_103708_test.bag",
        "size": 864530075,
        "num_messages": 3570997,
        "start": 1562345098262,
        "end": 1562347726950,
        "duration": 2628688,
        "vehicle_id_num": "v222-1337",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "00aa3aca-87ca-452e-8c63-c5d2c0a24909"
      }
    ]
  };
}

export function getTestOriginalFileList(singleBagfile = false) {
  if (singleBagfile) {
    return {
      "originalFiles": [{
        "_id": "9217ea0ad7f44c2caa655c5110553779",
        "link": "/input/plog/WDD2221591A668818/2019/07/9/20190712_103708_test.bag",
        "size": 362172470,
        "num_messages": 5665488,
        "start": 1516299527033,
        "end": 1516301755313,
        "duration": 2228280,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4"
      }]
    }
  }
  return {
    "originalFiles": [
      {
        "_id": "9217ea0ad7f44c2caa655c5110553779",
        "link": "/input/plog/WDD2221591A668818/2019/07/9/20190712_103708_test.bag",
        "size": 362172470,
        "num_messages": 5665488,
        "start": 1516299527033,
        "end": 1516301755313,
        "duration": 2228280,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4"
      },
      {
        "_id": "95669fed260741768a45b0a86b1ab3d4",
        "link": "/input/plog/WDD2221591A431086/2019/07/24/20190712_103708_test.bag",
        "size": 627875310,
        "num_messages": 7186872,
        "start": 1533510779798,
        "end": 1533516556729,
        "duration": 5776931,
        "vehicle_id_num": "v222-8901",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "dc3effbd-cc8c-42fa-99de-cde7c21e3c90"
      },
      {
        "_id": "8b4f89f2d7a64bf085f180cda87b0816",
        "link": "/input/plog/WDD2221591A215870/2019/07/20/20190712_103708_test.bag",
        "size": 813197491,
        "num_messages": 2931648,
        "start": 1491594590826,
        "end": 1491599681911,
        "duration": 5091085,
        "vehicle_id_num": "v222-8901",
        "extractor": "ext1",
        "version": "0.1",
        "guid": "eb53f61a-b447-4a99-af4c-a7c45877cbf7"
      },
      {
        "_id": "70645295ca38463bb3a9460eeb385710",
        "link": "/input/plog/WDD2221591A162572/2019/07/21/20190712_103708_test.bag",
        "size": 258093942,
        "num_messages": 1436516,
        "start": 1495980458612,
        "end": 1495983300584,
        "duration": 2841972,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "75213b41-d855-4f5a-8449-920bcc6f6cba"
      },
      {
        "_id": "8fb9261374334a06862867f5644670e6",
        "link": "/input/plog/WDD2221591A983834/2019/07/1/20190712_103708_test.bag",
        "size": 363703293,
        "num_messages": 7437565,
        "start": 1538279975829,
        "end": 1538282178970,
        "duration": 2203141,
        "vehicle_id_num": "v222-1234",
        "extractor": "ext1",
        "version": "0.1",
        "guid": "f453abca-8e6e-430b-b0bc-b9e0af9e7823"
      },
      {
        "_id": "e65e8e75d155466995800fa43c3d18d8",
        "link": "/input/plog/WDD2221591A19805/2019/07/12/20190712_103708_test.bag",
        "size": 565453034,
        "num_messages": 2328162,
        "start": 1500707052674,
        "end": 1500713680114,
        "duration": 6627440,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "cfade75e-9b99-4ccb-a883-5e0807ca4bc6"
      },
      {
        "_id": "22e8ae442c5d4709969e4ed1820fe539",
        "link": "/input/plog/WDD2221591A324855/2019/07/14/20190712_103708_test.bag",
        "size": 753270018,
        "num_messages": 5503162,
        "start": 1504793789656,
        "end": 1504797137620,
        "duration": 3347964,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "78edd80c-f0db-4df2-b1a2-4fcd70460a17"
      },
      {
        "_id": "7f1ca6a2fbe04dfcb87e0a842cc29659",
        "link": "/input/plog/WDD2221591A857185/2019/07/1/20190712_103708_test.bag",
        "size": 538718048,
        "num_messages": 9194080,
        "start": 1505531458146,
        "end": 1505538265437,
        "duration": 6807291,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "dba9fa1a-cdec-4e4b-99f3-7c2f2edc7e57"
      },
      {
        "_id": "670ac974e53c423e9c0fbb5971f25b42",
        "link": "/input/plog/WDD2221591A478516/2019/07/13/20190712_103708_test.bag",
        "size": 177561948,
        "num_messages": 3063593,
        "start": 1506864952301,
        "end": 1506866469251,
        "duration": 1516950,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "c9a66fc1-e49c-463a-9c2e-2fba6530585b"
      },
      {
        "_id": "d42427107415442591b29c5e78e5a61e",
        "link": "/input/plog/WDD2221591A389239/2019/07/12/20190712_103708_test.bag",
        "size": 864530075,
        "num_messages": 3570997,
        "start": 1562345098262,
        "end": 1562347726950,
        "duration": 2628688,
        "vehicle_id_num": "v222-1337",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "00aa3aca-87ca-452e-8c63-c5d2c0a24909"
      }
    ]
  };
}

export function getTestAlwaysonFileList(singleAlwaysonFile = false) {
  if (singleAlwaysonFile) {
    return {
      "alwaysonFiles": [{
        "_id": "9217ea0ad7f44c2caa655c5110553779",
        "link": "/input/plog/WDD2221591A668818/2019/07/9/20190712_103708_test.bag",
        "size": 362172470,
        "num_messages": 5665488,
        "start": 1516299527033,
        "end": 1516301755313,
        "duration": 2228280,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4"
      }]
    }
  }
  return {
    "alwaysonFiles": [
      {
        "_id": "9217ea0ad7f44c2caa655c5110553779",
        "link": "/input/plog/WDD2221591A668818/2019/07/9/20190712_103708_test.bag",
        "size": 362172470,
        "num_messages": 5665488,
        "start": 1516299527033,
        "end": 1516301755313,
        "duration": 2228280,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4"
      },
      {
        "_id": "95669fed260741768a45b0a86b1ab3d4",
        "link": "/input/plog/WDD2221591A431086/2019/07/24/20190712_103708_test.bag",
        "size": 627875310,
        "num_messages": 7186872,
        "start": 1533510779798,
        "end": 1533516556729,
        "duration": 5776931,
        "vehicle_id_num": "v222-8901",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "dc3effbd-cc8c-42fa-99de-cde7c21e3c90"
      },
      {
        "_id": "8b4f89f2d7a64bf085f180cda87b0816",
        "link": "/input/plog/WDD2221591A215870/2019/07/20/20190712_103708_test.bag",
        "size": 813197491,
        "num_messages": 2931648,
        "start": 1491594590826,
        "end": 1491599681911,
        "duration": 5091085,
        "vehicle_id_num": "v222-8901",
        "extractor": "ext1",
        "version": "0.1",
        "guid": "eb53f61a-b447-4a99-af4c-a7c45877cbf7"
      },
      {
        "_id": "70645295ca38463bb3a9460eeb385710",
        "link": "/input/plog/WDD2221591A162572/2019/07/21/20190712_103708_test.bag",
        "size": 258093942,
        "num_messages": 1436516,
        "start": 1495980458612,
        "end": 1495983300584,
        "duration": 2841972,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "75213b41-d855-4f5a-8449-920bcc6f6cba"
      },
      {
        "_id": "8fb9261374334a06862867f5644670e6",
        "link": "/input/plog/WDD2221591A983834/2019/07/1/20190712_103708_test.bag",
        "size": 363703293,
        "num_messages": 7437565,
        "start": 1538279975829,
        "end": 1538282178970,
        "duration": 2203141,
        "vehicle_id_num": "v222-1234",
        "extractor": "ext1",
        "version": "0.1",
        "guid": "f453abca-8e6e-430b-b0bc-b9e0af9e7823"
      },
      {
        "_id": "e65e8e75d155466995800fa43c3d18d8",
        "link": "/input/plog/WDD2221591A19805/2019/07/12/20190712_103708_test.bag",
        "size": 565453034,
        "num_messages": 2328162,
        "start": 1500707052674,
        "end": 1500713680114,
        "duration": 6627440,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "cfade75e-9b99-4ccb-a883-5e0807ca4bc6"
      },
      {
        "_id": "22e8ae442c5d4709969e4ed1820fe539",
        "link": "/input/plog/WDD2221591A324855/2019/07/14/20190712_103708_test.bag",
        "size": 753270018,
        "num_messages": 5503162,
        "start": 1504793789656,
        "end": 1504797137620,
        "duration": 3347964,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext2",
        "version": "0.1",
        "guid": "78edd80c-f0db-4df2-b1a2-4fcd70460a17"
      },
      {
        "_id": "7f1ca6a2fbe04dfcb87e0a842cc29659",
        "link": "/input/plog/WDD2221591A857185/2019/07/1/20190712_103708_test.bag",
        "size": 538718048,
        "num_messages": 9194080,
        "start": 1505531458146,
        "end": 1505538265437,
        "duration": 6807291,
        "vehicle_id_num": "v222-4242",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "dba9fa1a-cdec-4e4b-99f3-7c2f2edc7e57"
      },
      {
        "_id": "670ac974e53c423e9c0fbb5971f25b42",
        "link": "/input/plog/WDD2221591A478516/2019/07/13/20190712_103708_test.bag",
        "size": 177561948,
        "num_messages": 3063593,
        "start": 1506864952301,
        "end": 1506866469251,
        "duration": 1516950,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "c9a66fc1-e49c-463a-9c2e-2fba6530585b"
      },
      {
        "_id": "d42427107415442591b29c5e78e5a61e",
        "link": "/input/plog/WDD2221591A389239/2019/07/12/20190712_103708_test.bag",
        "size": 864530075,
        "num_messages": 3570997,
        "start": 1562345098262,
        "end": 1562347726950,
        "duration": 2628688,
        "vehicle_id_num": "v222-1337",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "00aa3aca-87ca-452e-8c63-c5d2c0a24909"
      }
    ]
  };
}

export function getTestChildBagfileList(singleChildBagfile = false) {
  if (singleChildBagfile) {
    return {
      "childBagfiles": [
        {
          "version": "1.0",
          "guid": "002d597a-bcb7-4af8-a911-b657b18ef53a",
          "parent_guid": "1891d97a-2634-4bcf-a52c-04b8397c0538",
          "start": 1564773728000,
          "end": 1564773745000,
          "parent_index_start": 212,
          "parent_index_end": 228,
          "bva_max": 10,
          "metadata": {
            "ext1_2": {
              "silent_testing": [
                {
                  "ts": 1564773738267,
                  "objects": [
                    {
                      "testcase": 4,
                      "position": [
                        970.9554443359375,
                        324.50616455078125,
                        1006.1823120117188,
                        358.4353332519531
                      ],
                      "label": 20
                    }
                  ]
                },
                {
                  "ts": 1564773738318,
                  "objects": [
                    {
                      "testcase": 4,
                      "position": [
                        972.9915161132812,
                        327.45953369140625,
                        1008.3295288085938,
                        361.5038146972656
                      ],
                      "label": 20
                    }
                  ]
                },
                {
                  "ts": 1564773738524,
                  "objects": [
                    {
                      "testcase": 4,
                      "position": [
                        971.681640625,
                        323.6733703613281,
                        1007.2056274414062,
                        357.88885498046875
                      ],
                      "label": 20
                    }
                  ]
                },
                {
                  "ts": 1564773741753,
                  "objects": [
                    {
                      "testcase": 6,
                      "position": [
                        963,
                        320,
                        976,
                        337
                      ],
                      "label": 18
                    }
                  ]
                }
              ]
            }
          },
          "gps_track": {
            "type": "LineString",
            "coordinates": [
              [
                -121.89555593159999,
                37.331354760699995
              ],
              [
                -91.42208509349999,
                27.99836215875
              ],
              [
                -121.89663299000001,
                37.330865722
              ],
              [
                -121.89720545550001,
                37.3305996143
              ]
            ]
          },
          "link":"dlazone2sadev/ins2-2019-10-15-mpmxw-filtered/2019_10_01__16_51_5664d2d02.bag",
          "file_name":"2019_10_01__16_51_5664d2d02.bag",
          "size":28626205993,
          "duration":31000
        }
      ]
    }
  }
  return {
    "childBagfiles": [
      {
        "version": "1.0",
        "guid": "002d597a-bcb7-5bt9-b234-b657b18ef53a",
        "parent_guid": "1891d97a-2634-4bcf-a52c-04b8397c0538",
        "start": 1564773728000,
        "end": 1564773745000,
        "parent_index_start": 212,
        "parent_index_end": 228,
        "bva_max": 10,
        "metadata": {
          "ext1_2": {
            "silent_testing": [
              {
                "ts": 1564773738267,
                "objects": [
                  {
                    "testcase": 4,
                    "position": [
                      970.9554443359375,
                      324.50616455078125,
                      1006.1823120117188,
                      358.4353332519531
                    ],
                    "label": 20
                  }
                ]
              },
              {
                "ts": 1564773738318,
                "objects": [
                  {
                    "testcase": 4,
                    "position": [
                      972.9915161132812,
                      327.45953369140625,
                      1008.3295288085938,
                      361.5038146972656
                    ],
                    "label": 20
                  }
                ]
              },
              {
                "ts": 1564773738524,
                "objects": [
                  {
                    "testcase": 4,
                    "position": [
                      971.681640625,
                      323.6733703613281,
                      1007.2056274414062,
                      357.88885498046875
                    ],
                    "label": 20
                  }
                ]
              },
              {
                "ts": 1564773741753,
                "objects": [
                  {
                    "testcase": 6,
                    "position": [
                      963,
                      320,
                      976,
                      337
                    ],
                    "label": 18
                  }
                ]
              }
            ]
          }
        },
        "gps_track": {
          "type": "LineString",
          "coordinates": [
            [
              -121.89555593159999,
              37.331354760699995
            ],
            [
              -91.42208509349999,
              27.99836215875
            ],
            [
              -121.89663299000001,
              37.330865722
            ],
            [
              -121.89720545550001,
              37.3305996143
            ]
          ]
        }
      },
      {
        "version": "1.0",
        "guid": "002d597a-bcb7-5bt9-b234-b657b18ef53a",
        "parent_guid": "1891d97a-2634-4bcf-a52c-04b8397c0538",
        "start": 1564773728000,
        "end": 1564773745000,
        "parent_index_start": 212,
        "parent_index_end": 228,
        "bva_max": 10,
        "metadata": {
          "ext1_2": {
            "silent_testing": [
              {
                "ts": 1564773738267,
                "objects": [
                  {
                    "testcase": 4,
                    "position": [
                      970.9554443359375,
                      324.50616455078125,
                      1006.1823120117188,
                      358.4353332519531
                    ],
                    "label": 20
                  }
                ]
              },
              {
                "ts": 1564773738318,
                "objects": [
                  {
                    "testcase": 4,
                    "position": [
                      972.9915161132812,
                      327.45953369140625,
                      1008.3295288085938,
                      361.5038146972656
                    ],
                    "label": 20
                  }
                ]
              },
              {
                "ts": 1564773738524,
                "objects": [
                  {
                    "testcase": 4,
                    "position": [
                      971.681640625,
                      323.6733703613281,
                      1007.2056274414062,
                      357.88885498046875
                    ],
                    "label": 20
                  }
                ]
              },
              {
                "ts": 1564773741753,
                "objects": [
                  {
                    "testcase": 6,
                    "position": [
                      963,
                      320,
                      976,
                      337
                    ],
                    "label": 18
                  }
                ]
              }
            ]
          }
        },
        "gps_track": {
          "type": "LineString",
          "coordinates": [
            [
              -121.89555593159999,
              37.331354760699995
            ],
            [
              -91.42208509349999,
              27.99836215875
            ],
            [
              -121.89663299000001,
              37.330865722
            ],
            [
              -121.89720545550001,
              37.3305996143
            ]
          ]
        }
      },
    ]
  }
}