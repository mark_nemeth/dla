#include <string.h>
#include <malloc.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "hps.h"

struct hpTcpStreamT
{
	hps_type type;
	int sock;
};

hpTcpStreamT *hps_tcp_open(const char *ip, int port)
{
	hpTcpStreamT *s = (hpTcpStreamT*)malloc(sizeof(hpTcpStreamT));
	if (s == NULL)
		return NULL;
	memset(s, 0, sizeof(*s));

	s->type = HPS_TCP;

	// Connect
    printf("Connecting to TCP://%s:%d\n", ip, port);
	s->sock = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in server;
    server.sin_addr.s_addr = inet_addr(ip);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    int e = connect(s->sock, (struct sockaddr *)&server, sizeof(server));
    if (e != 0)
    {
    	perror("Failed to connect");
    	close(s->sock);
    	free(s);
    	return NULL;
    }

	return s;
}

int hps_tcp_writev(hpTcpStreamT *s, const struct iovec *iov, int count)
{
	int n = writev(s->sock, iov, count);

	if (0 < n)
		statistics.txBytes += n;

	return n;
}

int hps_tcp_close(hpTcpStreamT *s)
{
	close(s->sock);
	free(s);
	return 0;
}
