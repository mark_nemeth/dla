from azure.storage.queue import QueueService
from azure.storage.queue.models import QueueMessageFormat
from config.config_handler import config_dict

queue_name = config_dict.get_value("queue_name")
storage_account = config_dict.get_value('storage_queue_account')
storage_access_key = config_dict.get_value(f"access_key_{storage_account}")

queue_service = QueueService(account_name=storage_account,
                             account_key=storage_access_key,
                             protocol='https')
queue_service.encode_function = QueueMessageFormat.text_base64encode
queue_service.decode_function = QueueMessageFormat.text_base64decode

try:
    msgs = queue_service.get_messages(queue_name, num_messages=1, visibility_timeout=5)
    print(msgs[0].content)
except Exception as exp:
    print(exp)

