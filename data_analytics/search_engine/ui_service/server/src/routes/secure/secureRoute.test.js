import httpMocks from 'node-mocks-http'
import secureRouteMiddleware from './secureRoute';

const secured = secureRouteMiddleware();

describe('Call secured route middleware', () => {

  it('should call next callback directly when in devMode', () => {
    const req = httpMocks.createRequest({
      headers: {origin: 'http://localhost:3000'},
      session: {}
    });
    const res = httpMocks.createResponse();
    const nextCallback = jest.fn();

    secured(req, res, nextCallback);

    expect(nextCallback).toHaveBeenCalled();
  });

  it('should call next callback when user session exist', () => {
    const req = httpMocks.createRequest({
      user: {
        name: "DanfUser"
      },
      session: {}
    });
    const res = httpMocks.createResponse();
    const nextCallback = jest.fn();

    secured(req, res, nextCallback);

    expect(nextCallback).toHaveBeenCalled();
  });

  it('should redirect to login when user session is not valid', () => {
    const req = httpMocks.createRequest({
      session: {},
      originalUrl: 'http://localhost:3001'
    });
    const res = httpMocks.createResponse();
    const nextCallback = jest.fn();

    secured(req, res, nextCallback);

    expect(req.session.returnTo).toBe('http://localhost:3001');
    expect(nextCallback).toHaveBeenCalledTimes(0);
  });

});