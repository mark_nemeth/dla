"""TMT Wrapper"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import sys
import os
import json
import logging
from handlers.ConversionHandler import ConversionHandler
from handlers.SplittingHandler import SplittingHandler
from handlers.DANFSplittingHandler import DANFSplittingHandler
from constants import *


def main():
    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.FileHandler(filename="/tmp/tmt_tool.log"),
                                  logging.StreamHandler()],
                        level=logging.INFO,
                        datefmt='%Y-%m-%d %H:%M:%S')
    try:
        args = parse_environment_variables()
        # load json config file
        try:
            config = json.load(open(args['config']))
            if isinstance(config, list):
                config = config[0]
        except (ValueError, IOError, OSError):
            logging.error('Bad json config: unable to read json config file {}'.format(args['config']))
            raise

        # Create handler
        if args['mode'] == MODE_SPLITTING:
            handler = SplittingHandler(config, args['tmt_path'])
        elif args['mode'] == MODE_CONVERSION:
            handler = ConversionHandler(config, args['tmt_path'])
        else:
            config['input_file_paths'] = [args['danf_input_path']]
            config['output_file_path'] = args['danf_output_path']
            handler = DANFSplittingHandler(config, args['tmt_path'])

    except Exception as e:
        logging.error("Parsing config from environment variables/config file failed, error:", exc_info=True)
        return 1

    try:
        handler.run()
        return 0

    except Exception as e:
        logging.error("TMT execution failed!, error:", exc_info=True)
        return 1


def parse_environment_variables():
    args = {}
    mode = os.getenv(ENV_MODE)
    assert mode in [MODE_SPLITTING,
                    MODE_CONVERSION,
                    MODE_DANF_SPLITTING], "Environment variable {} is required (Allowed values: [{}, {}])".format(
        ENV_MODE, MODE_SPLITTING, MODE_CONVERSION, MODE_DANF_SPLITTING)
    args['mode'] = mode

    config = os.getenv(ENV_CONFIG)
    assert isinstance(config, str), "Environment variable {} is required (e.g. /tmp/splitting.config)".format(
        ENV_CONFIG)
    config = os.path.abspath(config)
    assert os.path.isfile(config), "Path to config file seems to be invalid ".format(config)
    args['config'] = config

    tmt_path = os.getenv(ENV_TMT_PATH)
    assert isinstance(tmt_path, str), "Environment variable {} is required (e.g. /mapr/738.mbc.de/tmp/tmt)".format(
        ENV_TMT_PATH)
    tmt_path = os.path.abspath(tmt_path)
    assert os.path.isfile(tmt_path), "Path to tmt binary seems to be invalid ".format(tmt_path)
    args['tmt_path'] = tmt_path

    if mode == MODE_DANF_SPLITTING:
        input_path = os.getenv(ENV_DANF_INPUT_PATH)
        assert isinstance(input_path,
                          str), "Environment variable {} is required (e.g. /tmp/file.bag) when using mode '{}'".format(
            ENV_DANF_INPUT_PATH, mode)
        args['danf_input_path'] = input_path

        output_path = os.getenv(ENV_DANF_OUTPUT_PATH)
        assert isinstance(output_path,
                          str), "Environment variable {} is required (e.g. /tmp/file.bag) when using mode '{}'".format(
            ENV_DANF_OUTPUT_PATH, mode)
        args['danf_output_path'] = output_path

    return args


if __name__ == "__main__":
    sys.exit(main())
