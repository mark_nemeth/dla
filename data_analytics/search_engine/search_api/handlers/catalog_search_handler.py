"""Handler class for catalog search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging
from collections import defaultdict

from handlers.db_data_handler import DBDataHandler

logger = logging.getLogger(__name__)


class CatalogSearchHandler:

    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def create_search_kwargs(self, query_args):
        search_kwargs = {
            'query': None,
            'limit': 50,
            'offset': 0,
            'sortby': 'file_guid'
        }
        query = defaultdict(dict)
        for key, value in query_args.items():
            # Query filters
            if key in ('file_guid', 'file_type', 'tags', 'parent_guid'):
                query[key] = value[0]
            elif key == 'url_status':
                query[f'urls.url_status'] = value[0]
            elif key == 'url':
                query['urls.accessor_url'] = value[0]
            elif key == 'expiration_date':
                query['urls.expiration_date']['$eq'] = int(value[0])
            elif key == 'expiration_date[lte]':
                query['urls.expiration_date']['$lte'] = int(value[0])
            elif key == 'expiration_date[gte]':
                query['urls.expiration_date']['$gte'] = int(value[0])
            elif key == 'regex_url':
                query['urls.accessor_url'] = {'$regex': value[0]}

            # Pagination
            elif key == 'limit':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning('Invalid limit query parameter in catalog search - integer expected')
            elif key == 'offset':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning('Invalid offset query parameter in catalog search - integer expected')

            # Sort
            elif key == 'sortby':
                # Ascending order
                if value[0] in ['file_type', 'url', 'tags', 'urls.url_status', 'tags', 'parent_guid']:
                    search_kwargs[key] = value[0]
                elif value[0] in ['+file_type', '+url', '+tags', '+urls.url_status', '+tags', '+parent_guid']:
                    search_kwargs[key] = value[0][1:]
                # Descending order
                elif value[0] in ['-file_type', '-url', '-tags', '-urls.url_status', '-tags', '-parent_guid']:
                    search_kwargs[key] = [(value[0][1:], -1)]

            # no match
            else:
                logger.warning(f"unexpected parameter in catalog search: {key}, value: {value}")

        search_kwargs['query'] = dict(query)
        return search_kwargs

    def perform_search(self, **search_kwargs):
        files, total = self.db_data_handler.search_catalogs(**search_kwargs)

        if "urls.url_status" in search_kwargs['query']:
            url_status = search_kwargs['query']["urls.url_status"]
            for file in files:
                file["urls"] = [url for url in file["urls"] if url["url_status"] == url_status]

        for file in files:
            for url in file["urls"]:
                if "last_changed" not in url or not url["last_changed"]:
                    url["last_changed"] = url["creation_date"]
        return files, total
