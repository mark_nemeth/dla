import React from 'react';
import {shallow} from "enzyme/build";
import {ByEngagementStateFilter} from "./ByEngagementStateFilter";
import InputRange from 'react-input-range';

describe('ByEngagementStateFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      limits: {
        min: 0,
        max: 30
      },
      duration: {
        min: 1000,
        max: 2000
      },
    };
    const wrapper = shallow(<ByEngagementStateFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should call action when range is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      limits: {
        min: 0,
        max: 30
      },
      duration: {
        min: 1000,
        max: 2000
      },
      filterByEngagementState: reduxAction
    };
    const wrapper = shallow(<ByEngagementStateFilter {...props} />);
    wrapper.find(InputRange).simulate('onChange',{ target: {value: { min: 1000, max:2000 } }})
    wrapper.find(InputRange).simulate('onChangeComplete')
    // expect(reduxAction).toHaveBeenCalled();
  })

});
