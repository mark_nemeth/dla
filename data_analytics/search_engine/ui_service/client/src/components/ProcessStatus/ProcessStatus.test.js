import React from 'react';
import { shallow } from 'enzyme';
import ProcessStatus from './ProcessStatus';

describe('ProcessStatus component', () => {

  const processStates = [{
    "name": "post_ingest_extractor",
    "version": "0.1.8",
    "time": 1572516596967,
    "status": "SUCCESS",
    "report": {}
  }, {
    "name": "stripper",
    "version": "0.2.0",
    "time": 1572521813232,
    "status": "SUCCESS",
    "report": {}
  }, {
    "name": "splitter",
    "version": "unspecified",
    "time": 1576487811303,
    "status": "SKIPPED",
    "report": { "message": "No significant metadata" }
  }, {
    "name": "exporter-cv1",
    "version": "unspecified",
    "time": 1576846011630,
    "status": "SKIPPED",
    "report": { "message": "No significant metadata" }
  }];

  const processFlowRunStates = [
    {
      "version": "2.0",
      "guid": "f1a8e95e-bc63-43c3-a0f3-13de03e0d45f",
      "kubeflow_workflow_name": "danf-sv-processing-flow-v1-87j8r",
      "kubeflow_workflow_id": "ff8f438b-5dd9-11ea-a803-08f1ea8bef86",
      "started_at": 1583300141508,
      "completed_at": 1583312387724,
      "status": "SUCCESSFUL",
      "reference": {
        "type": "bagfile_guid",
        "value": "47f69c3e-fd26-4161-81be-602db6bd4968"
      },
      "flow_name": "danf-sv-processing-flow-v1",
      "flow_version": "1.0",
      "steps": [
        {
          "name": "danf-extractor-init",
          "image": "athena.daimler.com/dla_docker/extractor",
          "tag": "1.1.4",
          "max_retries": 0,
          "status": "SUCCESSFUL",
          "is_essential": true,
          "runs": [
            {
              "started_at": 1583300153602,
              "completed_at": 1583300154279,
              "status": "SUCCESSFUL",
              "report": {}
            },
            {
              "started_at": 1583300168286,
              "completed_at": 1583300168588,
              "status": "SUCCESSFUL",
              "report": {}
            }
          ]
        },
        {
          "name": "danf-extractor",
          "image": "athena.daimler.com/dla_docker/extractor",
          "tag": "1.1.4",
          "max_retries": 0,
          "status": "SUCCESSFUL",
          "is_essential": true,
          "runs": [
            {
              "started_at": 1583300168679,
              "completed_at": 1583304119720,
              "status": "SUCCESSFUL",
              "report": {}
            }
          ]
        },
        {
          "name": "danf-splitter",
          "image": "athena.daimler.com/dla_docker/splitter",
          "tag": "0.3.9",
          "max_retries": 0,
          "status": "SUCCESSFUL",
          "is_essential": true,
          "runs": [
            {
              "started_at": 1583304133351,
              "completed_at": 1583308289148,
              "status": "SUCCESSFUL",
              "report": {}
            }
          ]
        },
        {
          "name": "danf-stripper",
          "image": "athena.daimler.com/dla_docker/splitter",
          "tag": "0.3.9",
          "max_retries": 0,
          "status": "SUCCESSFUL",
          "is_essential": false,
          "runs": [
            {
              "started_at": 1583300166731,
              "completed_at": 1583312370505,
              "status": "SUCCESSFUL",
              "report": {}
            }
          ]
        }
      ]
    }
  ];

  it('should render empty placeholder when not status exist', () => {
    const props = {
      state: null
    };
    const wrapper = shallow(<ProcessStatus {...props} />);
    expect(wrapper.find('.process-state-empty-container').exists()).toBe(true);
  });

  it('should render process status with all validSteps', () => {
    const props = {
      state: processStates
    };
    const wrapper = shallow(<ProcessStatus {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('StepComponent').length).toBe(4);
  });

  it('should render flowRuns process status with all validSteps', () => {
    const props = {
      state: processFlowRunStates
    };
    const wrapper = shallow(<ProcessStatus {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('StepComponent').length).toBe(3);
  });

  it('should renders only 2 process status', () => {
    const props = {
      state: [{
        "name": "post_ingest_extractor",
        "version": "0.1.8",
        "time": 1572516596967,
        "status": "SUCCESS",
        "report": {}
      }, {
        "name": "splitter",
        "version": "unspecified",
        "time": 1576487811303,
        "status": "SKIPPED",
        "report": { "message": "No significant metadata" }
      }]
    };
    const wrapper = shallow(<ProcessStatus {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('StepComponent').length).toBe(2);
  });
});