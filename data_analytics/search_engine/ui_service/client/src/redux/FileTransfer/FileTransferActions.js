import { createAction } from '@reduxjs/toolkit';
import axios from 'axios';
import configHandler from '../../config/configHandler';
import { transferRequestIsOngoing, getTransferRequestId } from './FileTransferUtils'

export const requestFileTransferBegin = createAction('overview/requestFileTransferBegin');
export const requestFileTransferSuccess = createAction('overview/requestFileTransferSuccess');
export const requestFileTransferError = createAction('overview/requestFileTransferError');

export function requestFileTransfer(fileGuid, targetCluster) {
    return (dispatch, getState) => {
        if (transferRequestIsOngoing(getState(), fileGuid, targetCluster)) {
            return;
        }

        const requestId = getTransferRequestId(fileGuid, targetCluster);
        dispatch(requestFileTransferBegin({ requestId: requestId }));

        return axios.request(configHandler.getDataTransferAPI({ fileGuid: fileGuid, targetCluster: targetCluster }))
            .then(res => {
                if (res.data) {
                    dispatch(requestFileTransferSuccess({ requestId: requestId, data: res.data }));
                } else {
                    dispatch(requestFileTransferError({ requestId: requestId }));
                }
            })
            .catch(err => {
                console.log(err);
                dispatch(requestFileTransferError({ requestId: requestId, error: err }));
            });
    }
}
