__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from test.support import EnvironmentVarGuard
from unittest.mock import Mock

from DANFUtils.exceptions import RESTException

from cleanup.tasks.update_flow_run import UpdateFlowRunTask


class TestUpdateProcessingStateTask(unittest.TestCase):

    FLOW_RUN_GUID = '00000000-0000-0000-0000-000000000000'
    WORKFLOW_ID = '11111111-1111-1111-1111-111111111111'
    BAGFILE_GUID = '22222222-2222-2222-2222-222222222222'
    POD_NAME = 'danf-sv-processing-flow-v1-zn5jc-4235788942'
    EXPECTED_WORKFLOW_NAME = 'danf-sv-processing-flow-v1-zn5jc'

    def setUp(self):
        self.env = EnvironmentVarGuard()

        # prevent that initial unmocked container tries to obtain oauth token
        self.env.set('AUTH', 'none')

        self.mock_container_client = Mock()
        self.task = UpdateFlowRunTask()
        self.task.client = self.mock_container_client

        self.mock_container_client.get_flow_run.return_value = {'status': 'SUCCESSFUL'}

        self.env.set('FLOW_RUN_GUID', self.FLOW_RUN_GUID)
        self.env.set('BAGFILE_GUID', self.BAGFILE_GUID)
        self.env.set('POD_NAME', self.POD_NAME)

    def test_execute_when_pod_name_present_should_correctly_submit_derived_workflow_name(self):
        self.task.execute()

        self._assert_submitted_with_attr('kubeflow_workflow_name', self.EXPECTED_WORKFLOW_NAME)

    def test_execute_when_pod_name_not_present_should_submit_unknown_as_worklow_name(self):
        self.env.pop('POD_NAME')

        self.task.execute()

        self._assert_submitted_with_attr('kubeflow_workflow_name', 'unknown')

    def test_execute_when_flow_run_guid_not_present_should_raise_exception(self):
        self.env.pop('FLOW_RUN_GUID')

        self.assertRaises(ValueError, self.task.execute)

    def test_execute_when_submitting_flow_run_update_fails_should_raise_exception(self):
        exp = RESTException('Test', 'PATCH', '', 500, 'error')
        self.mock_container_client.update_flow_run.side_effect = exp

        self.assertRaises(RuntimeError, self.task.execute)

    def _assert_submitted_with_attr(self, attr, val):
        args, _ = self.mock_container_client.update_flow_run.call_args
        guid, data = args
        self.assertEqual(self.FLOW_RUN_GUID, guid, 'Wrong flow run guid')
        self.assertEqual(val, data.get(attr), 'Wrong value in data dict')


if __name__ == '__main__':
    unittest.main()
