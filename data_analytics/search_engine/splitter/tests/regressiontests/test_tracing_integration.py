
__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from subprocess import CalledProcessError
from test.support import EnvironmentVarGuard
from unittest.mock import patch, call, ANY

import index


class TestTracingInstrumentation(unittest.TestCase):

    def setUp(self):
        self.env = EnvironmentVarGuard()
        self.env.set('BVA_THRESHOLD', '10')
        self.env.set('BAGFILE_GUID', 'bagfile')
        self.env.set('BAGFILE_PATH', 'in_path')
        self.env.set('OUTPUT_FOLDER', 'out_path')
        self.env.set('APPINSIGHTS_INSTRUMENTATIONKEY', 'APPINSIGHTS_DISABLE_KEY')

        start_task_patcher = patch('index.tracing.start_task')
        self.mock_start_task = start_task_patcher.start()
        self.addCleanup(start_task_patcher.stop)

        end_task_patcher = patch('index.tracing.end_task')
        self.mock_end_task = end_task_patcher.start()
        self.addCleanup(end_task_patcher.stop)

        check_call_patcher = patch('index.check_call')
        self.mock_check_call = check_call_patcher.start()
        self.addCleanup(check_call_patcher.stop)

        container_client_patcher = patch('index.ContainerClient', autospec=True)
        self.mock_container_client = container_client_patcher.start()
        self.addCleanup(container_client_patcher.stop)

    def test_tracing_when_no_snippets_found_should_create_main_span(self):
        self.mock_container_client.return_value.get_snippets_bva.return_value = self.get_metadata_response(0)

        index.main()

        self.assert_successful_and_failed_span_count(1, 0)

    def test_tracing_when_snippets_found_should_create_main_span_and_child_span_per_snippet(self):
        self.mock_container_client.return_value.get_snippets_bva.return_value = self.get_metadata_response(2)

        index.main()

        self.assert_successful_and_failed_span_count(3, 0)

    def test_tracing_when_split_process_fails_should_not_mark_main_span_failed(self):
        self.mock_container_client.return_value.get_snippets_bva.return_value = self.get_metadata_response(2)
        self.mock_check_call.side_effect = CalledProcessError(1, 'Boom')

        index.main()

        self.assert_successful_and_failed_span_count(1, 2)

    def assert_successful_and_failed_span_count(self, count_success, count_fail):
        calls_success = count_success * [call()]
        calls_fail = count_fail * [call(success=False, attributes=ANY)]
        calls = calls_success + calls_fail
        self.assertEqual(self.mock_start_task.call_count, count_success + count_fail)
        self.mock_end_task.assert_has_calls(calls, any_order=True)

    def get_metadata_response(self, number_of_snippets):
        metadata = []
        for i in range(number_of_snippets):
            metadata.append({'guid': i, 'start': 1475272800000, 'end': 1475272810000})
        return metadata


if __name__ == '__main__':
    unittest.main()
