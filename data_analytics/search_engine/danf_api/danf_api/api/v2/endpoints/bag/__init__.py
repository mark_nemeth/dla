"""Bag api endpoints."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from fastapi import APIRouter

from .searching import router as search_router
from .tagging import router as tag_router

router = APIRouter()

router.include_router(search_router)
router.include_router(tag_router)
