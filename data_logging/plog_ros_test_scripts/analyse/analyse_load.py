#!/usr/bin/env python
# This script analyses output of the load file the time a load value captured is every 5 seconds.



import re
import os
import sys
import getopt
import matplotlib.pyplot as plt

list_one = []
list_five= []
list_15	 = []
time =[]
tstart=0;
inputfile = ''
crop_end= False
crop_start= False
export = False
try:
	    options, remainder = getopt.getopt(
		sys.argv[1:],
		'i:s:e:pgG',
		['input=',
		 'starttime=',
		 'endtime='
		 ])
except getopt.GetoptError as err:
	    print('ERROR:', err)
	    sys.exit(1)



for opt, arg in options:
	    if opt in ('-i', '--input'):
		inputfile = arg
	    elif opt in ('-s', '--starttime'):
		tstart = int(arg)
		crop_start = True
	    elif opt in ('-e', '--endtime'):
		tend = int(arg)
		crop_end= True
	    elif opt == "-p":
 		export= True


if inputfile != '':

	print "Analysing load output"
	with open(inputfile, 'r') as f:
		i=1;
		j=0;
		t=0;
		for line in f:
			list_line = line.split()
			if i==3 and j==0:
			   list_one.append(float(list_line[0].replace(",","."))) #appends first column
			   list_five.append(float(list_line[1].replace(",","."))) #appends second column
			   list_15.append(float(list_line[2].replace(",","."))) #appends third column
			   time.append(t)
			   j=1
			   i=0
			   t=t+5
			elif i==1 and j==1:
			   list_one.append(float(list_line[0].replace(",","."))) #appends first column
			   list_five.append(float(list_line[1].replace(",","."))) #appends second column
			   list_15.append(float(list_line[2].replace(",","."))) #appends third column
			   time.append(t)
			   i=0
                           t=t+5
			i=i+1


		if (crop_start==False and crop_end==False):

			#plot the output
			fig = plt.figure()
			ax = fig.add_subplot(111)
			line1, = ax.plot(time,list_one,label="Average  1 minute")
			line2, = ax.plot(time,list_five,label="Average 5 minutes")
			line3, = ax.plot(time,list_15,label="Average 15 minutes")
			fig.suptitle('Load output', fontsize=14, fontweight='bold')
			plt.xlabel('time in s')
			plt.xlim(0,time[-1])
			plt.legend(loc='best', fontsize=12)
			if export== False:
				plt.show()


		elif (crop_start == True and crop_end==False):
			#get the index
			tstart_ind=divmod(tstart,5)[0]
			#crop the list
			list_one_time=list_one[tstart_ind:]
			list_five_time=list_five[tstart_ind:]
			list_15_time=list_15[tstart_ind:]
			time_crop=time[tstart_ind:]
			#plot the output
			fig = plt.figure()
			ax = fig.add_subplot(111)
			line1, = ax.plot(time_crop,list_one_time,label="Average  1 minute")
			line2, = ax.plot(time_crop,list_five_time,label="Average 5 minutes")
			line3, = ax.plot(time_crop,list_15_time,label="Average 15 minutes")
			fig.suptitle('Load output', fontsize=14, fontweight='bold')
			plt.xlabel('time in s')
			plt.xlim(time_crop[0],time_crop[-1])
			plt.legend(loc='best', fontsize=12)
			if export== False:
				plt.show()

		elif (crop_end == True and crop_start == False):
			#get the  indices
			tstart_ind=divmod(tstart,5)[0]
			tend_ind=divmod(tend,5)[0]

			list_one_time_cut=list_one[:(tend_ind-tstart_ind)]
			list_five_time_cut=list_five[:(tend_ind-tstart_ind)]
			list_15_time_cut=list_15[:(tend_ind-tstart_ind)]
			time_cut=time[:(tend_ind-tstart_ind)]

			#plot the output
			fig = plt.figure()
			ax = fig.add_subplot(111)
			line1, = ax.plot(time_cut,list_one_time_cut,label="Average  1 minute")
			line2, = ax.plot(time_cut,list_five_time_cut,label="Average 5 minutes")
			line3, = ax.plot(time_cut,list_15_time_cut,label="Average 15 minutes")
			fig.suptitle('Load output', fontsize=14, fontweight='bold')
			plt.xlabel('time in s')
			plt.xlim(time_cut[0],time_cut[-1])
			plt.legend(loc='best', fontsize=12)
			if export== False:
				plt.show()



		elif (crop_end == True and crop_start==True):
			#get the  indices
			tstart_ind=divmod(tstart,5)[0]
			tend_ind=divmod(tend,5)[0]


			#crop and do the math
			#crop the list
			list_one_time=list_one[tstart_ind:]
			list_five_time=list_five[tstart_ind:]
			list_15_time=list_15[tstart_ind:]
			time_crop=time[tstart_ind:]


			list_one_time_cut=list_one_time[:(tend_ind-tstart_ind)]
			list_five_time_cut=list_five_time[:(tend_ind-tstart_ind)]
			list_15_time_cut=list_15_time[:(tend_ind-tstart_ind)]
			time_cut=time_crop[:(tend_ind-tstart_ind)]



			#plot the stuff
			fig = plt.figure()
			fig.suptitle('Load output', fontsize=20, fontweight='bold')
			ax = fig.add_subplot(111)
			fig.subplots_adjust(top=0.89)
			line1, = ax.plot(time_cut,list_one_time_cut,label="Average  1 minute")
			line2, = ax.plot(time_cut,list_five_time_cut,label="Average 5 minutes")
			line3, = ax.plot(time_cut,list_15_time_cut,label="Average 15 minutes")
			plt.xlabel('time in s')
			plt.xlim(time_cut[0],time_cut[-1])
			plt.legend(loc='best', fontsize=12)
			if export== False:
				plt.show()
		if export == True:
			fig.savefig(os.path.dirname(inputfile)+"/load.png", bbox_inches='tight')

else:
	print("Usage: ./analyse_load.py -i load_3401220.log")
	print("If you want to crop the plot you should use:")
	print("Usage: ./analyse_load.py -i load_3401220.log -s startime -e endtime")
	print("If you want to print the plot direct to a png file add the parameter -p")
