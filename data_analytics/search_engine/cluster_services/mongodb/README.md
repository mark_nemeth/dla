# DANF - Kubernetes MongoDB Setup

This Kubernetes Deployment will deploy a single instance MongoDB without any PVC to the cluster.
The deployment includes a configmap which acts as the init db script to set up the database and user. It is editable to fit every init db setup.

## Prerequisites
* Secrets to successfully run MongoDB in Kubernetes 
   ```
   apiVersion: v1
   kind: Secret
   metadata:
     name: mongo-secrets
   type: Opaque
   data:
     mongo.user.username :  <insert your mongoDB user>
     mongo.user.password :  <insert the corresponding password for your mongoDB user>
   ```
   ```
    apiVersion: v1
    kind: Secret
    metadata:
      name: mongo-root-secrets
    type: Opaque
    data:
      mongo.dbadm.username : <insert your mongoDB root user>
      mongo.dbadm.password : <insert the corresponding password for your mongoDB root user>
   ```
  Additionally for your services:
   ```
    apiVersion: v1
    kind: Secret
    metadata:
      name: <your service name>-credentials
    type: Opaque
    data:
      db_key:  <insert your the password of your mongoDB user>
   ```
  
## Configuration

* In the `mongodb-statefulset` you can change the database you want to create. The user in the `mongo-secrets` will be created via the `mongodb-configmap` and has read and write permission in the given database.

## Setup

1. Connect to the `cluster` and `namespace where you want to deploy the MongoDB`
2. Create the secret `mongo-secrets` as already mentioned in the prerequisites. If you already know your services you want to deploy, add the service secrets as well.
3. Create the `configmap`, `service` and the `statefulset`
   ```
   k apply -f mongodb
   ```
   