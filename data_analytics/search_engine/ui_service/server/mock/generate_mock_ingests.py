import json
import os
import random

NUMBER_OF_METADATA = 9
OUTPUT_FILENAME = "ingests.json"
VERSION = "0.1"

data = []
for i in range(0, NUMBER_OF_METADATA):
  file_name = str(random.randint(10000000, 999999999)) + "_mock_ingest_test.json"

  data.append({
    "id": str(random.randint(1, 100)),
    "ingest_cluster": "VAI",
    "car_name": "V-123-234",
    "status": "COMPLETED",
    "progress": 100.0,
    "vin": "WDK12345",
    "start_time": 1611568130261.0 * 1000,
    "end_time": 1611568886612.0 * 1000,
    "last_updated": 1611568886613.0 * 1000,
    "created": 1611568069266.0 * 1000,
    "eta_seconds": -1,
    "size": 15013,
    "error_message": None,
    "retry_count": 0,
    "files": [
      {
        "url": "/home/pid1083/.viminfo",
        "size": 15013
      },
      {
        "url": "/home/pid1083/sequences/plog/test_ingest_api.dat",
        "size": 1116893696
      },
      {
        "url": "/home/pid1083/sequences/logs/adgw/ADGW__2021_01_25_10_59_14.log",
        "size": 0
      }
    ]
  })

result = data

outpath = os.path.join("public/mockdata")
if not os.path.exists(outpath):
    os.makedirs(outpath)

outfilePath = os.path.join(outpath, OUTPUT_FILENAME)
with open(outfilePath, 'w') as outfile:
    json.dump(result, outfile, indent=4, sort_keys=True)

print("Mock json file created: " + outfilePath)
