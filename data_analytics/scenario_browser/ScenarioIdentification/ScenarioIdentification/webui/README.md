# Requirements
`pip install -r ../../../requirements.txt`
However you should be able to run it as long as you have (numpy, pandas, dash, plotly)
# Data
Make sure that the files mentioned in `data.py` are available in your ../Data directory.  
Specifically copy the following files from `/mapr/624.mbc.us/user/emtc_ad2_s_bag-crawl/tmp_csv/` directory to `../Data`:  
* ego_tracks_all_files.csv
* geom_features_w_lc.csv
* dynamic_objects_intersections_w_mo_small.csv
* map_objects.csv
* geom_features_all_files_timetraj_pivot.csv
* dyn_features_pivot.csv
* proximity_features_ego_pivot.csv
* tracks_intersection_dyn_obj_ego_w_relative.csv
# Deployment
This is a flask app. For details on server choices, please refer to [https://flask.palletsprojects.com/en/1.1.x/deploying/]
## Using default wsgi flask server
`python app.py`
The instance should run in [http://your_ip:8050]
## Using gunicorn
`gunicorn app:server`
The instance should run in [http://your_ip:8000]
For production, it is also possible to configure nginx to serve this.