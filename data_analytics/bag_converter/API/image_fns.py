__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from API.bagfile_fns import bag2slice_fn
from config import config_handler as configurator
from config import ExceptionHandler
from image_processing.image_processor import extract_images


def bag2img_fn(
        bagfile: str,
        topic: str,
        real_time: int = None,
        relative_time: int = None,
        overlay: bool = False,
        overlay_id: int = -1,
        apply_time_correct: bool = False
):
    """ Returns a single image based on a given bagfile, timestamp, and topic. Image can be either in BMP or
        JPEG format depending on the compression specified - default is a JPEG with compression 85. """

    if real_time is None and relative_time is None:
        raise ExceptionHandler('missing_relative_time_err')

    _end_relative = relative_time+1000 if relative_time is not None else None
    _end_time = real_time + configurator.get_value('bagfile_duration_app') if real_time is not None \
        else None

    bagfile_slice = bag2slice_fn(bagfile=bagfile,
                                topic=topic,
                                start_relative=relative_time,
                                end_relative=_end_relative,
                                start_time=real_time,
                                end_time=_end_time,
                                apply_time_correct=apply_time_correct)

    images = extract_images(bagfile=bagfile_slice,
                            overlay=overlay,
                            overlay_id=overlay_id,
                            max_messages=1,
                            )
    return images[0][0]



