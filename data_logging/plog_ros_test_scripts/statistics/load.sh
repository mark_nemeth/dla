#!/bin/sh
#This script gets the load status of the system and prints it to the output
TT=`date +%j%H%M%S`
TIME=5

function logload
{
	echo "Log the load average of the system every $TIME seconds"
	echo "1min	5min	15min"
	while true; do
	LOAD1MIN="$(uptime | awk -F "load average:" '{ print $2 }' | cut -d, -f1 | sed 's/ //g')"
	LOAD5MIN="$(uptime | awk -F "load average:" '{ print $2 }' | cut -d, -f2 | sed 's/ //g')"
	LOAD15MIN="$(uptime | awk -F "load average:" '{ print $2 }' | cut -d, -f3 | sed 's/ //g')"

	echo "$LOAD1MIN	$LOAD5MIN	$LOAD15MIN"
	sleep $TIME

	done

}

#logload | tee load_$TT.log
logload > load_$TT.log







