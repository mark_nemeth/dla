import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../Table';
import PathEntry from './PathEntry';

describe('PathEntry', () => {

  it('should format path correct and create a enabled button', () => {
    const props = {filePath: 'azure_folder/path_to_bagfile/test_bagfile_with_long_name_123423324234234254312312.bag', buttonType: true};
    const wrapper = shallow(<PathEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('Button').exists()).toBe(true);
  });

  it('should create a disabled button when path not exists', () => {
    const props = {filePath: null, buttonType: true};
    const wrapper = shallow(<PathEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('Button').props().disabled).toBe(true);
  });

  it('should format path correct and create a enabled button', () => {
    const props = {filePath: 'azure_folder/path_to_bagfile/test_bagfile_with_long_name_123423324234234254312312.bag'};
    const wrapper = shallow(<PathEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(props.filePath);
  });

  it('should create a disabled button when path not exists', () => {
    const props = {filePath: null};
    const wrapper = shallow(<PathEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});