"""Handler class for reading config files"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os


class ConfigHandler():

    def __init__(self):
        self.configs_obj = {}
        mode = os.getenv("DANF_ENV", "dev")
        if mode not in ["az_dev", "az_int", "dev", "prod", "sv_prod", "vai_prod", "vai_int"]:
            mode = "dev"
        try:
            path = os.path.dirname(os.path.abspath(__file__))
            with open(os.path.join(path, f'config_{mode}.init'), "r") as f:
                self.configs_obj = json.load(f)
        except Exception:
            print(f"config_{mode}.init is missing or incorrect! Did you provide one?")
            raise ValueError('Configs_init are missing! Crash app!')

    def get_value(self, key, default_value=None):
        value = os.getenv(key)
        if value is None:
            value = self.configs_obj.get(key, default_value)
        return value


config_dict = ConfigHandler()