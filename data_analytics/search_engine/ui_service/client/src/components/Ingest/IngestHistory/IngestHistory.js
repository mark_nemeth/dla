import React, {useEffect, useState} from 'react';
import {Container, Row, Spinner} from 'react-bootstrap';
import IngestDetailBox from '../IngestDetailBox/IngestDetailBox';
import IngestSupportBox from '../IngestSupportBox/IngestSupportBox';
import './IngestHistory.scss';

export function IngestHistory(props) {

  const [selectedVin, setSelectedVin] = useState();

  const minDate = new Date(Date.now() - 12096e5).toISOString().substring(0, 10); // 14 days ago

  useEffect(() => {
    props.fetchIngests({carName: props.selectedVehicle, dateFrom: minDate});
    props.refresh();
  }, [props.selectedVehicle]);

  useEffect(() => {
    if (props.nextIngestRefreshIn === 1) {
      props.fetchIngests({carName: props.selectedVehicle, dateFrom: minDate});
    }
  }, [props.nextIngestRefreshIn]);

  useEffect(() => {
    if (props.ingests[0]) {
      setSelectedVin(props.ingests[0].vin);
    }
  }, [props.ingests[0]]);

  function compareIngestsByEndDate(i1, i2) {
    if (!i1.endTime && !i2.endTime) {
      return i2.created - i1.created;
    }
    if (!i1.endTime) {
      return -1;
    }
    if (!i2.endTime) {
      return 1;
    }
    return i2.endTime - i1.endTime;
  }

  const sortedIngests = [...props.ingests];
  sortedIngests.sort(compareIngestsByEndDate);
  const ingestBoxes = sortedIngests.map((ingest, index) => {
    return (<IngestDetailBox ingest={ingest} expanded={index === 0} key={ingest.id}/>);
  });

  const showLoading = !props.selectedVehicle || !props.ingests;

  return (
      <Container fluid className="ingest-history">
        {
          showLoading ?
              (
                  <div className="loading-spinner-container">
                    <Spinner animation="border" variant="info"/>
                  </div>
              ) :
              (
                  <div>
                    <Row className="ingest-history-title mx-2">
                      <div className="mt-2 fifty-percent-card">
                        {"Ingest History for " + props.selectedVehicle + " (" + selectedVin + ")"}
                      </div>
                      <div className="fifty-percent-card">
                        <IngestSupportBox/>
                      </div>
                    </Row>
                    <div className="mt-2">
                      {
                        props.ingests.length === 0 ?
                            (
                                <div className="mx-2">
                                  {"No Ingest in the last 2 weeks!"}
                                </div>
                            ) :
                            (
                                ingestBoxes
                            )
                      }
                    </div>
                  </div>
              )
        }
      </Container>
  );
}
