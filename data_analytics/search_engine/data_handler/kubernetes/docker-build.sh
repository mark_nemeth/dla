if [ $# -eq 0 ]
  then
    echo "Version argument missing!"
  else
    docker_image_version=$1
    docker_app_name=$2

    # connect
    az acr login --name dalaregistry

    # build docker image
    docker build -t $docker_app_name:$docker_image_version . --no-cache --network host

    #tag the image
    docker tag $docker_app_name dlaregistry.azurecr.io/$docker_app_name:docker_image_version

    # push docker image to registry
    docker push $docker_app_name dlaregistry.azurecr.io/$docker_app_name:docker_image_version

fi
