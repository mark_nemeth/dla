# DANF - code style check
## Install hooks for automatic code check before committing
```console
./setup_hooks.sh
```
Execute the setup_hooks script for enabling the hooks (pre-commit)


precommit:
Now after each commit your code will be checked accorded to the style guidelines defined in the .style.yapf file 
in the folder "code_check".



If you want to extend the hooks, just put your file in the folder "hooks" and add a line in the setup_hook.sh script for creating a symlink in the .git/hooks/ folder to your defined file (in the hooks folder).

## Deactivate hooks
```console
./deactivate_hooks.sh
```
Execute the script to deactivate hooks. 
If you added additional hooks in the setup_hooks.sh script dont forget to modify the deactivate_hooks.sh script. 
