__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from rosbag import Bag

from API.bagfile_fns import bag2slice_fn
from config import ExceptionHandler
from config import config_handler as configurator
from config import logging_handler as logger


def get_overlay(bagfile, timestamp, oid, apply_time_correct=False):
    """ Retrieves overlay info for a given bagfile and time. """
    bagfile_slice = bag2slice_fn(bagfile=bagfile.name,
                                 topic='/stereo/SilentTesting/StereoCameraFront/DetectedObjectsOfInterest',
                                 start_relative=None,
                                 end_relative=None,
                                 start_time=timestamp - 0.1,
                                 end_time=-1,
                                 apply_time_correct=apply_time_correct)
    try:
        pybag = Bag(bagfile_slice.name)
    except Exception as e:
        raise ExceptionHandler(code='dla_converter_err', debug_msg=e)


    data = []
    for topic, msg, t in pybag.read_messages():
        for d in msg.detected_objects_of_interest:
            this_id = d.test_case
            if int(this_id) == int(oid):
                data.append({})
                data[-1]['id'] = d.test_case
                data[-1]['label_id'] = d.label.id
                data[-1]['confidence'] = d.label.confidence
                data[-1]['upper_left'] = (d.position_image.top_left)
                data[-1]['bottom_right'] = (d.position_image.bottom_right)
                data[-1]['timestamp'] = int('{}{}'.format(msg.header.timestamp.s, msg.header.timestamp.ns))
                data[-1]['tx_timestamp'] = int('{}{}'.format(msg.header.tx_timestamp.s, msg.header.tx_timestamp.ns))
                data[-1]['test_case'] = int(d.test_case)
    return data

def get_test_case(i):
    """ Returns a string describing the test case based on test_case attribute of Detected Objects of Interest"""
    test_cases = configurator.get_value('test_cases_app')

    if i < len(test_cases):
        return test_cases[i]
    else:
        return 'UNKNOWN TEST CASE'


def get_msg_type(msg):
    """ Takes a ROS msg and returns its type, stripping temporary filepath """
    # Strip the temporary file path from the class type by treating it as a string
    t = str(type(msg))
    t = t.split('.')[1]
    t = t.split('\'')[0]

    logger.debug(f'ROS msg type: {t}')
    if t in configurator.get_value('supported_msg_types_app'):
        return t
    else:
        raise ExceptionHandler(code='unsupported_type_err', debug_msg=f'Message Type: {t}')

