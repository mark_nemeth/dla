#!/bin/bash
set -e
ln -s -f ../../.githooks/pre-commit ../../dla_repo/.git/hooks/pre-commit   #symlink in .git/hooks/
ln -s -f ../code_check/.style.yapf ./.style.yapf     # creates symlink to the style file in folder code_check
