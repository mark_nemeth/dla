"""Unittests for DBConnector"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os
import unittest

import requests
from DANFUtils.logging import logger
from datetime import timedelta, datetime
# Set env before importing db connector
from .utils import generate_bagfile, generate_feature_buckets

os.environ["DANF_ENV"] = "local"
from .testutils import get_testdb_connector

# increase log level for tests
logger.set_log_level('debug')

base_url = 'http://localhost:8889'
search_bagfile_features_url = base_url + '/search/bagfile/features'

bag = {
    "guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "link": "/test/test/test/test/test/test/test",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "version": "1.0",
    "is_test_data": 1
}

feat_bucket_docs = [{
    "version":
    "1.0",
    "guid":
    "22221111-aaaa-bbbb-cccc-999999999999",
    "bagfile_guid":
    bag["guid"],
    "start":
    1563262907000,
    "end":
    1563262911001,
    "start_index":
    60,
    "end_index":
    119,
    "features": [{
        "ts": 1563262907000,
        "vel": 0.4,
        "acc": 0.2,
        "eng_state": 5,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 5.1
    }, {
        "ts": 1563262908000,
        "vel": 0.5,
        "acc": 0.3,
        "eng_state": 11,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 5.1
    }],
    "is_test_data":
    1
}, {
    "version":
    "1.0",
    "guid":
    "22221111-aaaa-bbbb-cccc-888888888888",
    "bagfile_guid":
    bag["guid"],
    "start":
    1563263907000,
    "end":
    1563263911001,
    "start_index":
    600,
    "end_index":
    660,
    "features": [{
        "ts": 1563263907000,
        "vel": 20,
        "acc": -4,
        "eng_state": 7,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 100.1
    }, {
        "ts": 1563263908000,
        "vel": 20,
        "acc": -5,
        "eng_state": 8,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 100.1
    }],
    "is_test_data":
    1
}]


class TestFeatureSearchChildBagfilesGET(unittest.TestCase):
    def setUp(self):
        self.connector = get_testdb_connector()
        self.cleanup_db()
        # add
        self.connector.add_bagfiles([bag])
        self.connector.add_feature_buckets(feat_bucket_docs)

    def tearDown(self):
        self.cleanup_db()

    def cleanup_db(self):
        with self.connector as con:
            con.db['bagfiles'].delete_many({"is_test_data": 1})
            con.db['feature_buckets'].delete_many({"is_test_data": 1})

    def test_search_bagfiles_by_features(self):
        query_data = {
            'feat.vel[gte]': 10,
            'feat.vel[lte]': 30,
            'eng_state[]': 8
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertDictEqual(bag, res_json[0],
                             'Bagfile search by features failed')

    def test_search_bagfiles_by_features_and_condition(self):
        query_data = {
            'duration[gte]': 700000,
            'duration[lte]': 800000,
            'feat.vel[gte]': 10,
            'feat.vel[lte]': 30,
            'eng_state[]': 8
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertDictEqual(bag, res_json[0],
                             'Bagfile search by features failed')

    def test_search_bagfiles_by_condition_only(self):
        query_data = {'duration[gte]': 700000, 'duration[lte]': 800000}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertDictEqual(bag, res_json[0],
                             'Bagfile search by features failed')

    def test_search_bagfiles_invalid_feature(self):
        query_data = {
            'durtion[gte]': 700000,
            'duration[kte]': 800000,
            'feat.ve[gte]': 10,
            'feat.vel[kte]': 30
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        self.assertEqual(
            200, res.status_code,
            'Bagfile search by features should have failed, but did not')

    def test_search_single_bagfile_by_guid(self):
        query_data = {'guid': bag["guid"]}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        self.assertEqual(200, res.status_code,
                         'Bagfile search by single guid failed')
        res_json = res.json()['results']

        self.assertDictEqual(bag, res_json[0],
                             'Bagfile search by single guid failed')

    def test_search_bagfiles_query_bags_without_feature_buckets(self):
        self.cleanup_db()
        # prepare large query
        NUM_FEAT_BAGFILES = 10
        feat_bags = []
        feat_buckets = []
        for _ in range(NUM_FEAT_BAGFILES):
            bag = generate_bagfile()
            self.connector.add_bagfiles([bag])
            feat_bags.append(bag)
            buckets = generate_feature_buckets(
                bag["guid"], timedelta(0, bag["duration"]),
                datetime.fromtimestamp(bag["start"] / 1000))
            self.connector.add_feature_buckets(buckets)
            feat_buckets.extend(buckets)
        # create  bagfiles without feature buckets
        bag = generate_bagfile()
        self.connector.add_bagfiles([bag])

        # query with feature parameters (only bagfiles that match the features will be returned)
        query_data = {'duration[gte]': 0, 'feat.vel[gte]': 0}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertCountEqual(feat_bags, res_json,
                              'Bagfile search by features failed')
        query_data = {'feat.vel[gte]': 0}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertCountEqual(feat_bags, res_json,
                              'Bagfile search by features failed')

        feat_bags.append(bag)
        # query without feature parameters (all bagfiles that match will be returned)
        query_data = {'duration[gte]': 0}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertCountEqual(feat_bags, res_json,
                              'Bagfile search by features failed')

    def test_search_bagfiles_query_bags_without_feature_buckets_with_limit(
            self):
        self.cleanup_db()
        # prepare large query
        NUM_FEAT_BAGFILES = 10
        feat_bags = []
        feat_buckets = []
        for _ in range(NUM_FEAT_BAGFILES):
            bag = generate_bagfile()
            self.connector.add_bagfiles([bag])
            feat_bags.append(bag)
            buckets = generate_feature_buckets(
                bag["guid"], timedelta(0, bag["duration"]),
                datetime.fromtimestamp(bag["start"] / 1000))
            self.connector.add_feature_buckets(buckets)
            feat_buckets.extend(buckets)
        # create  bagfiles without feature buckets
        bag = generate_bagfile()
        self.connector.add_bagfiles([bag])

        # query with feature parameters (only bagfiles that match the features will be returned)
        query_data = {'duration[gte]': 0, 'feat.vel[gte]': 0, 'limit': 5}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertEqual(5, len(res_json), 'Bagfile search by features failed')

        query_data = {'feat.vel[gte]': 0, 'limit': 5}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertEqual(5, len(res_json), 'Bagfile search by features failed')

        feat_bags.append(bag)
        # query without feature parameters (all bagfiles that match will be returned)
        query_data = {'duration[gte]': 0, 'limit': 5}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertEqual(5, len(res_json), 'Bagfile search by features failed')

    def test_search_bagfiles_many_bags_many_features(self):
        # check for limit issues
        self.cleanup_db()
        # prepare large query
        NUM_BAGFILES = 120
        bags = []
        feat_buckets = []
        for _ in range(NUM_BAGFILES):
            bag = generate_bagfile()
            self.connector.add_bagfiles([bag])
            bags.append(bag)
            buckets = generate_feature_buckets(
                bag["guid"], timedelta(0, bag["duration"]),
                datetime.fromtimestamp(bag["start"] / 1000))
            self.connector.add_feature_buckets(buckets)
            feat_buckets.extend(buckets)

        # query with default limit
        query_data = {
            'duration[gte]': 0,
            'feat.vel[gte]': 0,
            'feat.vel[kte]': 100
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertEqual(50, len(res_json),
                         'Bagfile search by features failed')

        # query with default limit and custom offset
        query_data = {
            'duration[gte]': 0,
            'feat.vel[gte]': 0,
            'feat.vel[kte]': 100,
            'offset': 100
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertEqual(NUM_BAGFILES - 100, len(res_json),
                         'Bagfile search by features failed')

        # query with custom limit
        query_data = {
            'duration[gte]': 0,
            'feat.vel[gte]': 0,
            'feat.vel[kte]': 100,
            'limit': 500
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertCountEqual(bags, res_json,
                              'Bagfile search by features failed')

        # query with custom limit and custom offset
        query_data = {
            'duration[gte]': 0,
            'feat.vel[gte]': 0,
            'feat.vel[kte]': 100,
            'limit': 500,
            'offset': 5
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertEqual(
            len(bags) - 5, len(res_json), 'Bagfile search by features failed')

    def test_search_bagfiles_super_many_bags_few_features(self):
        # check for limit issues
        self.cleanup_db()
        # prepare large query
        NUM_BAGFILES = 2000
        bags = []
        feat_buckets = []
        for _ in range(NUM_BAGFILES):
            bag = generate_bagfile()
            bags.append(bag)
        self.connector.add_bagfiles(bags)
        buckets = generate_feature_buckets(
            bags[0]["guid"], timedelta(0, bags[0]["duration"]),
            datetime.fromtimestamp(bags[0]["start"] / 1000))
        self.connector.add_feature_buckets(buckets)
        feat_buckets.extend(buckets)

        # bag query with default limit
        query_data = {'duration[gte]': 0}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertEqual(50, len(res_json),
                         'Bagfile search by features failed')

        # bag query with custom limit + offset
        query_data = {'duration[gte]': 0, 'offset': 1500, 'limit': 200}
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertEqual(200, len(res_json),
                         'Bagfile search by features failed')

        # feature query with default limit
        query_data = {
            'feat.vel[gte]': 0,
            'feat.vel[kte]': 100,
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertCountEqual([bags[0]], res_json,
                              'Bagfile search by features failed')

        # feature query with too big offset + limit
        query_data = {
            'feat.vel[gte]': 0,
            'feat.vel[kte]': 100,
            'offset': 10,
            'limit': 200
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertCountEqual([], res_json,
                              'Bagfile search by features failed')

        # feature query with default limit
        query_data = {
            'feat.vel[gte]': 0,
            'feat.vel[kte]': 100,
            'duration[gte]': 0
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertCountEqual([bags[0]], res_json,
                              'Bagfile search by features failed')

        # check for correct handling of start key
        query_data = {
            'start[gte]': int(bags[1]['start']),
            'start[lte]': int(bags[1]['start'])
        }
        res = requests.get(url=search_bagfile_features_url, params=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code,
                         'Bagfile search by features failed')
        self.assertCountEqual([bags[1]], res_json,
                              'Bagfile search by features failed')
