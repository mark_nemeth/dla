"""Search Engine REST API filters route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing

from handlers.auth import auth
from handlers.childbagfile_filters_handler import ChildBagFileFiltersHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler

logger = logging.getLogger(__name__)



class ChildBagFileFiltersRequestHandler(BaseRequestHandler):

    def initialize(self):
        self.filters_handler = ChildBagFileFiltersHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'GET')

    @auth.requires_entitlements(["openid"])
    def get(self):
        logger.info(f"Request {self.request}")
        tracing.start_task_in_context('childbagfile_filters/get',
                                      context_headers=self.request.headers)

        res = self.filters_handler.get_filters()

        logger.debug(f"Response {res}")
        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
