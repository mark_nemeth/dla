import configureMockStore from 'redux-mock-store';
import * as actions from './HomeActions';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configHandler from '../../config/configHandler';
import {FileTypes} from '../../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import {
  getTestOriginalFileList,
  getTestChildBagfileList,
  getTestAlwaysonFileList,
  getTestFiltersList,
} from '../../setupTests';
import {insertString} from "../../assets/string/stringHelper";
import strings from "../../assets/string/default";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const mock = new MockAdapter(axios);

describe('Home Actions', () => {

  describe('fetchFilters action', () => {

    const apiPath = "/api/v1/originalfile_filters";
    configHandler.getOriginalFileFiltersAPI = jest.fn(() => {
      return {url: apiPath}
    });
    const mockBagRequest = mock.onGet(apiPath);

    it('creates fetchFiltersSuccess action when fetching filters succeeded', () => {
      const filtersPayload = getTestFiltersList();

      mockBagRequest.reply(200, filtersPayload);

      const expectedActions = [{
        type: actions.fetchOriginalFileFiltersSuccess.type,
        payload: filtersPayload.results
      }];

      const store = mockStore({});
      return store.dispatch(actions.fetchOriginalFileFilters()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('creates fetchFiltersError action when fetching vehicleids failed', () => {
      mockBagRequest.reply(418, {});

      const expectedActions = [{
        type: actions.fetchOriginalFileFiltersError.type,
        payload: undefined
      }];

      const store = mockStore({});
      return store.dispatch(actions.fetchOriginalFileFilters()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });
  });

  describe('refresh action ', () => {

    it('should load original files when refresh action is called', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfiles = getTestOriginalFileList(true).originalFiles;
      const total = bagfiles.length;
      mockBagRequest.reply(200, {results: {bagfiles, total}});

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {},
        appliedFilters: {
          file_type: 'ORIGINAL'
        }
      });

      const expectedActions = [{
        type: actions.showLoading.type
      }, {
        type: actions.fetchOriginalFilesSuccess.type,
        payload: {bagfiles, total}
      }];

      return store.dispatch(actions.refresh()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('should load alwaysonfiles when refresh action is called', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfiles = getTestAlwaysonFileList(true).alwaysonFiles;
      const total = bagfiles.length;
      mockBagRequest.reply(200, {results: {bagfiles, total}});

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ALWAYSONFILE,
        filters: {},
        appliedFilters: {
          file_type: 'ALWAYSON'
        }
      });

      const expectedActions = [{
        type: actions.showLoading.type
      }, {
        type: actions.fetchAlwaysonFilesSuccess.type,
        payload: {bagfiles, total}
      }];

      return store.dispatch(actions.refresh()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('should load childbagfiles when refresh action is called', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/childbagfiles";
      configHandler.getChildBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const childBagfiles = getTestChildBagfileList(true).childBagfiles;
      const total = childBagfiles.length;
      mockBagRequest.reply(200, {results: {childBagfiles, total}});

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.CHILDBAGFILE,
        filters: {},
        appliedFilters: {}
      });

      const expectedActions = [{
        type: actions.showLoading.type
      }, {
        type: actions.fetchChildBagfilesSuccess.type,
        payload: {childBagfiles, total}
      }];

      return store.dispatch(actions.refresh()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('trigger error message when some network error happens', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      mockBagRequest.reply(418, {});

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {},
        appliedFilters: {
          file_type: 'ORIGINAL'
        }
      });

      const expectedActions = [{
        type: actions.showLoading.type
      }, {
        type: actions.fetchOriginalFilesError.type,
        payload: insertString(strings.connection_error)
      }];

      return store.dispatch(actions.refresh()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('trigger common error message when no results are found', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      mockBagRequest.reply(200, {results: {bagfiles: [], total: 0}});

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {},
        appliedFilters: {
          file_type: 'ORIGINAL'
        }
      });

      const expectedActions = [{
        type: actions.showLoading.type
      }, {
        type: actions.fetchOriginalFilesEmpty.type,
        payload: insertString(strings.no_results_found, '')
      }];

      return store.dispatch(actions.refresh()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('trigger common error message when no results are found for file name', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      mockBagRequest.reply(200, {results: {bagfiles: [], total: 0}});

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {},
        appliedFilters: {
          file_name: 'abc',
          file_type: 'ORIGINAL'
        }
      });

      const expectedActions = [{
        type: actions.showLoading.type
      }, {
        type: actions.fetchOriginalFilesEmpty.type,
        payload: insertString(strings.no_results_found, ' "abc" or')
      }];

      return store.dispatch(actions.refresh()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

  });

  describe('apply filter action ', () => {

    it('should apply the set filters and start a refresh of the data', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfilePayload = getTestOriginalFileList(true);
      mockBagRequest.reply(200, bagfilePayload);

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {
          vehicle_id_num: 'VID-123',
          file_type: 'ORIGINAL'
        },
        appliedFilters: {}
      });

      const expectedActions = [{
        type: actions.setAppliedFilters.type
      }, {
        type: actions.showLoading.type
      }];

      store.dispatch(actions.applyFilters());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('sortBy action ', () => {

    it('should apply the filters and start a refresh of the data', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfilePayload = getTestOriginalFileList(true);
      mockBagRequest.reply(200, bagfilePayload);

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {},
        appliedFilters: {
          file_type: 'ORIGINAL'
        }
      });

      const expectedActions = [{
        type: actions.setSortBy.type,
        payload: 'desc+vehicle_id_num'
      }, {
        type: actions.showLoading.type
      }];

      store.dispatch(actions.sortBy('desc+vehicle_id_num'));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('filter by Type action ', () => {

    it('should apply the filters and start a refresh of the data', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfilePayload = getTestOriginalFileList(true);
      mockBagRequest.reply(200, bagfilePayload);

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.CHILDBAGFILE,
        filters: {},
        appliedFilters: {}
      });

      const expectedActions = [{
        type: actions.setFilterByFileType.type,
        payload: FileTypes.ORIGINALFILE
      }, {
        type: actions.showLoading.type
      }];

      store.dispatch(actions.filterByFileType(FileTypes.ORIGINALFILE));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('filter by search input action ', () => {

    it('should apply the filters and start a refresh of the data', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfilePayload = getTestOriginalFileList(true);
      mockBagRequest.reply(200, bagfilePayload);

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {},
        appliedFilters: {
          file_type: 'ORIGINAL'
        }
      });

      const expectedActions = [{
        type: actions.setFilterBySearchQuery.type,
        payload: 'ABC'
      }, {
        type: actions.showLoading.type
      }];

      store.dispatch(actions.filterBySearchQuery('ABC'));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('apply filter action ', () => {

    it('should apply the set filters and start a refresh of the data', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfilePayload = getTestOriginalFileList(true);
      mockBagRequest.reply(200, bagfilePayload);

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {
          vehicle_id_num: 'VID-123',
          file_type: 'ORIGINAL'
        },
        appliedFilters: {}
      });

      const expectedActions = [{
        type: actions.setAppliedFilters.type
      }, {
        type: actions.showLoading.type
      }];

      store.dispatch(actions.applyFilters());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('sortBy action ', () => {

    it('should apply the filters and start a refresh of the data', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfilePayload = getTestOriginalFileList(true);
      mockBagRequest.reply(200, bagfilePayload);

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {},
        appliedFilters: {
          file_type: 'ORIGINAL'
        }
      });

      const expectedActions = [{
        type: actions.setSortBy.type,
        payload: 'desc+vehicle_id_num'
      }, {
        type: actions.showLoading.type
      }];

      store.dispatch(actions.sortBy('desc+vehicle_id_num'));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('filter by Type action ', () => {

    it('should apply the filters and start a refresh of the data', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfilePayload = getTestOriginalFileList(true);
      mockBagRequest.reply(200, bagfilePayload);

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.CHILDBAGFILE,
        filters: {},
        appliedFilters: {}
      });

      const expectedActions = [{
        type: actions.setFilterByFileType.type,
        payload: FileTypes.ORIGINALFILE
      }, {
        type: actions.showLoading.type
      }];

      store.dispatch(actions.filterByFileType(FileTypes.ORIGINALFILE));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('filter by serarch input action ', () => {

    it('should apply the filters and start a refresh of the data', () => {
      // Init mock api request
      const bagApiPath = "/api/v1/bagfiles";
      configHandler.getBagfileAPI = jest.fn(() => {
        return {url: bagApiPath}
      });
      const mockBagRequest = mock.onGet(bagApiPath);
      const bagfilePayload = getTestOriginalFileList(true);
      mockBagRequest.reply(200, bagfilePayload);

      // Init mock redux store
      const store = mockStore({
        fileType: FileTypes.ORIGINALFILE,
        filters: {},
        appliedFilters: {
          file_type: 'ORIGINAL'
        }
      });

      const expectedActions = [{
        type: actions.setFilterBySearchQuery.type,
        payload: 'ABC'
      }, {
        type: actions.showLoading.type
      }];

      store.dispatch(actions.filterBySearchQuery('ABC'));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('trigger reprocessing flow ', () => {

    const apiPath = "/api/v1/reprocessfiles";
    configHandler.getReprocessAPI = jest.fn(() => {
      return {url: apiPath}
    });
    const mockBagRequest = mock.onGet(apiPath);

    it('creates reprocessingBagfileSuccess action when calling reprocess succeeded', () => {
      const response = {
        results: {
          results: [
            {
              file_path: '/mapr/624.mbc.us/data/input/rd/athena/08_robotaxi/plog/byName/v222-5215/2018/12/11/20181211_133439_alwayson.bag',
              bagfile_guid: 'da3f6136-8ad7-49eb-bf4a-f8577f280601',
              result: 'SUCCESSFUL'
            }
          ]
        }
      };
      mockBagRequest.reply(200, response);

      const expectedActions = [{
        type: actions.reprocessingBagfileSuccess.type,
        payload: 'Reprocessing successfully triggered'
      }];

      const store = mockStore({});
      return store.dispatch(actions.reprocessBagfile()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('creates reprocessingBagfileError action when calling reprocess return with failing result ', () => {
      const response = {
        results: {
          results: [
            {
              file_path: '/mapr/624.mbc.us/data/input/rd/athena/08_robotaxi/plog/byName/v222-5215/2018/12/11/20181211_133439_alwayson.bag',
              bagfile_guid: 'da3f6136-8ad7-49eb-bf4a-f8577f280601',
              result: 'FAILED'
            }
          ]
        }
      };
      mockBagRequest.reply(200, response);

      const expectedActions = [{
        type: actions.reprocessingBagfileError.type,
        payload: 'Reprocessing failed for the triggered file/s'
      }];

      const store = mockStore({});
      return store.dispatch(actions.reprocessBagfile()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('creates reprocessingBagfileError action when calling reprocess failed', () => {
      mockBagRequest.reply(418, {});

      const expectedActions = [{
        type: actions.reprocessingBagfileError.type,
        payload: 'Unfortunately a connection error occurred, please try again'
      }];

      const store = mockStore({});
      return store.dispatch(actions.reprocessBagfile()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });
  });
});