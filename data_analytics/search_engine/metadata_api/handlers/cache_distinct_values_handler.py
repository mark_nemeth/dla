"""Handler class for cache drivers"""
__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DB_Data_Handler

logger = logging.getLogger(__name__)


class CacheDistinctValuesHandler:
    def __init__(self, collection: str, field: str):
        self.db_data_handler = DB_Data_Handler()
        self.collection = collection
        self.field = field
        self.cache_type_name = f"{collection}_{field}_cache"

    def update_cache(self):
        logger.info(f'fetching all the values for field {self.field} from {self.collection} collection')

        values = self.db_data_handler.get_distinct_field_values(self.collection, self.field)
        values = [value for value in values if ((value.strip() != '') and (value != 'null'))]
        logger.info(f'value list: {values}')

        search_kwargs = {'query': {'type': self.cache_type_name}}
        cached_docs = self.db_data_handler.search_documents(**search_kwargs)
        if len(cached_docs) > 0:
            guid = cached_docs[0]['guid']
        else:
            guid = generate_guid()

        documents = [{'type': self.cache_type_name, 'values': values}]
        self.db_data_handler.update_documents([guid], documents, True)
        return guid

    def delete_cache_document(self, guid):
        logger.info('deleting the cache document')
        guids = [guid]
        return self.db_data_handler.delete_documents(guids)

    def get_cache_document(self, guid):
        logger.info('get the cache document')
        guids = [guid]
        documents = self.db_data_handler.get_documents(guids)
        return documents
