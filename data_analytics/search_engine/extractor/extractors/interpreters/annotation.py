"""Logbook Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from extractors.utils import charlist2str, rospytime2int, timestamp2ms, ros_timestamp2ms
from extractors.base_interpreter import Interpreter

BVA_HMI_ANNOTATION = 10
SECONDS_BEFORE_HMI_ANNOTATION = 15
SECONDS_AFTER_HMI_ANNOTATION = 5


class AnnotationInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.bva = BVA_HMI_ANNOTATION

    def feed_new_msg(self, topic, msg, t):
        entry = {}
        entry["ts"] = ros_timestamp2ms(msg.header.stamp)
        # entry["title"] = msg.title
        # entry["image"] = msg.image
        # entry["is_critical_error"] = msg.is_critical_error
        # entry["annotation_event_type"] = msg.event_type
        # entry["annotation_event_timestamp"] = msg.event_timestamp

        entry = self.calc_start_end_ts_snippet(entry)
        self.event_list += [entry]

    @staticmethod
    def calc_start_end_ts_snippet(event):
        event["start_ts"] = int(event["ts"] - (SECONDS_BEFORE_HMI_ANNOTATION * 1e3))
        event["end_ts"] = int(event["ts"] + (SECONDS_AFTER_HMI_ANNOTATION * 1e3))
        event["ts"] = int(event["ts"])
        return event

