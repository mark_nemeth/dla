import React from 'react';
import DateTimeEntry from "../../../AttributeFormatter/DateTime/DateTimeEntry";
import LabelEntry from "../../../AttributeFormatter/Label/LabelEntry";
import LabelContainer from "../../../Utils/LabelContainer/LabelContainer";
import {Row} from "react-bootstrap";
import Card from "react-bootstrap/Card";
import './AnnotationMetadataView.scss';

export function AnnotationMetadataView(props) {

  return (
      <Card className="annotation-information-section">
        <span className="annotation-information-section-label">
          Annotation Information
        </span>
        <Row className='information-row'>
          <LabelContainer label="Annotation Guid" variant="seventy-percent-card card-style-filled">
            <LabelEntry value={props.annotation.guid}/>
          </LabelContainer>
          <LabelContainer label="Severity" variant="thirty-percent-card card-style-filled">
            <LabelEntry value={props.annotation.severity}/>
          </LabelContainer>
        </Row>
        <Row className='information-row'>
          <LabelContainer label="Timestamp" variant="fifty-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.annotation.timestamp}/>
          </LabelContainer>
          <LabelContainer label="Title" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.annotation.title}/>
          </LabelContainer>
        </Row>
        <Row className='information-row'>
          <LabelContainer label="Description" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.annotation.description}/>
          </LabelContainer>
        </Row>
      </Card>
  )
}