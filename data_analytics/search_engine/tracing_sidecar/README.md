# DANF tracing sidecar
### Version: see setup.py

## First steps
1. create `Personal Access Tokens` in AzureDevOps page
2. copy the token you created.
3. add to the general url like this(it looks like: https://YOUR_TOKEN@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/ )

```console
export index_url_with_access=https://<YOUR_KEY>@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```

The DANF tracing sidecar can be build as docker image and deployed as a sidecar to arbitrary pods running
to completion in kubernetes. The sidecar enables tracing for individual containers, that are not explicitly
instrumented, e.g. because they are supplied externally.

For each traced container, a single span will be created. The span starts approx. when the container enters waiting
state and is finished when the container terminates. Based on the exit code, the span will be marked
as successful or failed.

## Input

### Environment variables

|Parameter                  |Required   |Description|
|---|---|---|
|DANF_TRACING_TRACE_ID      |no         |see DANFTracingClient documentation|
|DANF_TRACING_SPAN_ID       |no         |see DANFTracingClient documentation|
|DANF_TRACING_TRACE_OPTIONS |no         |see DANFTracingClient documentation|
|POD_NAME                   |yes        |name of the pod (best set via k8s downward API)|
|NAMESPACE                  |yes        |namespace where the pod is running (best set via k8s downward API)|
|BAGFILE_GUID               |no         |guid of the currently processed file. will be attached to the span|

### Command line arguments

|Parameter          |Description                                    |
|---|---|
|`-- task`          |task name to use for tracing                   |
|`-- container`     |name of the container that should be monitored |
 

## Docker

Build and push image
```console
$ az acr login -n DlaRegistry
$ docker build -t dlaregistrydev.azurecr.io/tracingsidecar:<version> . --network=host --no-cache --build-arg https_proxy=https://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access
$ docker push dlaregistrydev.azurecr.io/tracingsidecar:${version}
```
