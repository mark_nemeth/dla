import {connect} from 'react-redux';
import {filterByStatus} from '../../../../redux/Home/HomeActions';
import {ByStatusFilter} from './ByStatusFilter';

function mapStateToProps(state, ownProps) {
  const statusList = ["NOT_PROCESSED", "BEING_PROCESSED", "SKIPPED", "PROCESSING_DEACTIVATED", "FAILED", "SUCCESSFUL"];
  const selectedStatus = state.filters.processing_state || [];
  return {statusList, selectedStatus};
}

export default connect(
  mapStateToProps,
  {filterByStatus}
)(ByStatusFilter);