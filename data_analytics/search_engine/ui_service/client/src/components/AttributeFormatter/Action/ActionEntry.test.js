import React from 'react';
import {shallow} from 'enzyme';
import {FileTypes} from '../../Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import ActionEntry from './ActionEntry';

describe('Bagfile Table ActionEntry', () => {

  it('should show action menu with correct items when no admin', () => {
    const bannerAction = jest.fn();
    const reprocessAction = jest.fn();
    const props = {
      guid: '1337',
      adminUser: false,
      fileType: FileTypes.ORIGINALFILE,
      link: 'azure_folder/path_to_bagfile/test_bagfile_with_long_name_123423324234234254312312.bag',
      showBanner: bannerAction,
      reprocessCallback: reprocessAction
    };
    const wrapper = shallow(<ActionEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('CopyPathItem').shallow().exists()).toBe(true);
    expect(wrapper.find('ShowPathItem').shallow().exists()).toBe(true);
    expect(wrapper.find('ViewOverviewPageItem').shallow().exists()).toBe(true);
    expect(wrapper.find('ReprocessFileItem').shallow().isEmptyRender()).toBe(true);
  });

  it('should show action menu with correct items when admin', () => {
    const bannerAction = jest.fn();
    const reprocessAction = jest.fn();
    const props = {
      guid: '1337',
      adminUser: true,
      fileType: FileTypes.ORIGINALFILE,
      link: 'azure_folder/path_to_bagfile/test_bagfile_with_long_name_123423324234234254312312.bag',
      showBanner: bannerAction,
      reprocessCallback: reprocessAction
    };
    const wrapper = shallow(<ActionEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('CopyPathItem').shallow().exists()).toBe(true);
    expect(wrapper.find('ShowPathItem').shallow().exists()).toBe(true);
    expect(wrapper.find('ViewOverviewPageItem').shallow().exists()).toBe(true);
    expect(wrapper.find('ReprocessFileItem').shallow().isEmptyRender()).toBe(false);
  });

  it('should trigger banner when user click copy item', () => {
    const bannerAction = jest.fn();
    const reprocessAction = jest.fn();
    const props = {
      guid: '1337',
      adminUser: false,
      fileType: FileTypes.ORIGINALFILE,
      link: 'azure_folder/path_to_bagfile/test_bagfile_with_long_name_123423324234234254312312.bag',
      showBanner: bannerAction,
      reprocessCallback: reprocessAction
    };
    const wrapper = shallow(<ActionEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    wrapper.find('CopyPathItem').shallow().simulate('click');
    expect(bannerAction).toHaveBeenCalled();
  });

  it('should trigger reprocess when user click reprocess item', () => {
    const bannerAction = jest.fn();
    const reprocessAction = jest.fn();
    const props = {
      guid: '1337',
      adminUser: true,
      fileType: FileTypes.ORIGINALFILE,
      link: 'azure_folder/path_to_bagfile/test_bagfile_with_long_name_123423324234234254312312.bag',
      showBanner: bannerAction,
      reprocessCallback: reprocessAction
    };
    const wrapper = shallow(<ActionEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    wrapper.find('ReprocessFileItem').shallow().simulate('click');
    expect(reprocessAction).toHaveBeenCalled();
  });
});
