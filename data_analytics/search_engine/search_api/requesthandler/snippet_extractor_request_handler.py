"""Search Engine REST API snippet extractor route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing

from handlers.auth import auth
from handlers.snippet_extractor_handler import SnippetExtractorHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


class SnippetExtractorRequestHandler(BaseRequestHandler):

    _schema_post = BaseRequestHandler.load_json_schema(
        "snippet_extractor_post.schema")

    def initialize(self):
        self.snippet_extractor_handler = SnippetExtractorHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        # TODO: change to snippet_extractor/post
        tracing.start_task_in_context('snippets_bva/post',
                                      context_headers=self.request.headers)

        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_post)

        res = self.snippet_extractor_handler.get_snippets_extractor(
            data['bagfile_guid'],
            data['bva_limit'],
            extractors=data.get('extractors', None),
            generate_child_bagfiles=data.get('generate_child_bagfiles', True)
        )

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
