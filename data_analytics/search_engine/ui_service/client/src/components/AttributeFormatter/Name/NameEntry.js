import React from 'react';
import {Link} from 'react-router-dom';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import {defaultEmptyValue} from '../../Table';
import {FileTypes} from '../../Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import featureToggle from '../../../config/featureToggleHelper';
import {isStringValid} from '../../../utils/helperFunctions';

export default function NameEntry(props) {

  if (!isStringValid(props.fileName)) return <span>{defaultEmptyValue}</span>;

  let path;
  switch (props.fileType) {
    case FileTypes.ORIGINALFILE:
      path = `/originalfile/${props.guid}`;
      break;
    case FileTypes.ALWAYSONFILE:
      path = `/alwaysonfile/${props.guid}`;
      break;
    case FileTypes.CHILDBAGFILE:
      path = `/childbagfile/${props.guid}`;
      break;
    case FileTypes.DRIVE:
      path = `/drive/${props.guid}`;
      break;
    default:
      path = `/originalfile/${props.guid}`;
      break;
  }

  const withTooltip = props.disableTooltip === undefined ? true : !props.disableTooltip;
  const withLink = (featureToggle.showOverviewPage && !props.withoutLink && props.guid) || false;

  const NameField = () => (
      withLink
          ? <Link to={path}>{props.fileName}</Link>
          : <span>{props.fileName}</span>
  );

  return (
      <span>
      {withTooltip ?
          <OverlayTrigger placement="top" delay={{show: 100, hide: 0}} overlay={
            <Tooltip id={props.guid} outOfBoundaries>{props.fileName}</Tooltip>
          }>
            {NameField()}
          </OverlayTrigger>
          : <NameField/>}
    </span>
  )
}