# DANF - code style check
## Install yapf for usage in pycharm
```console
pip install yapf
```
Install yapf (yet another python formatter) via pip
```console
>> which yapf 
/Users/theys/.pyenv/shims/yapf
```
1. Check the location of yapf. 
2. Download the latest yapf plugin for pycharm at https://plugins.jetbrains.com/plugin/10960-yapf
3. Press Ctr+Alt+s for opening settings and add the yapf plugin under "plugins".
4. Insert your Path (Example: "/Users/theys/.pyenv/shims/yapf") at "Executeable Path" under plug in settings
5. Now you can find at "code" a command named "reformat code with YAPF". (codestyle file (.style.yapf) in top directory will be used) 



## Check code for code style and create diff

Run the codecheck.sh script by using -d option to create a diff (*.diff) of all *.py files in folder and subfolders
```console
./codecheck.sh -d
```
Script create *.diff files with a diff of the *.py file according to the code style defined in .style.yapf (pep8).
The logfile include the path of the created *.diff files.

# How to clean up *.diff files and logs
Run the codecheck.sh script by using -c option to cleanup (deletes all *.diff and *.log files)

```console
./codecheck.sh -c
```

# How to check a single *.py file
Check specific file (*.py) for issues according to the code style guideline and creates a single *.dff file.
```console
./codecheck.sh -s /path/to/your/file.py
```
# How to replace a single *.py file with formatted one according to code style guideline
```console
./codecheck.sh -a /path/to/your/file.py
```
Replace *.py file inline with formatted one according to code style guideline.
# How to replace  ALL *.py files with formatted ones according to code style guideline
Replace ALL *.py files with formatted ones according to code style guideline. Use with care.
```console
./codecheck.sh -r
```