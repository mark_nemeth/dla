__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import logging
import os
import re
import sys
import time
import traceback
from datetime import datetime, timezone
from pathlib import Path
from shutil import copy2
from subprocess import CalledProcessError, check_call

import pytz
from config.constants import *
from ContainerClient.container_client import ContainerClient
from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.logging import configure_root_logger
from DANFUtils.tracing import tracing
from DANFUtils.utils import utils as danfutils
from managers.metadata_manager import MetadataManager

DLA_CONVERTER = "dla_converter/dla_converter"

logger = logging.getLogger(__name__)


class Splitter:
    def __init__(self,
                 bva_limit,
                 bagfile_guid,
                 bagfile_path,
                 output_folder,
                 childbagfile_link_template):
        self.bva_limit = bva_limit
        self.bagfile_guid = bagfile_guid
        self.bagfile_path = bagfile_path
        self.output_folder = output_folder
        self.childbagfile_link_template = childbagfile_link_template
        self.client = ContainerClient()
        self.metadata_manager = MetadataManager(bagfile_guid)

    @staticmethod
    def _convert_timezone(time, old_tz, new_tz):
        old_timezone = pytz.timezone(old_tz)
        new_timezone = pytz.timezone(new_tz)
        return old_timezone.localize(time).astimezone(new_timezone)

    def _get_event_type_from_snippet(self, snippet):
        key = "unknown_type"
        try:
            key = list(snippet["metadata"].keys())[0]
            return key
        except KeyError:
            logger.exception("The snippet does not contain the key metadata, event type cannot be determined!")
        except IndexError:
            logger.exception("The metadata list is empty, event type cannot be determined!")
        return key

    def _get_event_ts_from_snippet(self, snippet):
        event_ts = 0
        try:
            event_ts = list(snippet["metadata"].values())[0]["ts"] / 1000.
            return event_ts
        except KeyError:
            logger.exception("The snippet does not contain the key metadata, event ts cannot be determined!")
        except IndexError:
            logger.exception("The metadata list is empty, event ts cannot be determined!")
        return event_ts

    def _get_file_name(self, start_ts, index, event_type, event_ts):
        try:
            # set default
            filename_stamp = index
            ts_in_filename = event_ts

            # try parse time
            if os.environ["DANF_ENV"] == "sv_prod":
                converted_datetime = self._convert_timezone(
                    datetime.fromtimestamp(int(ts_in_filename)), "GMT", "US/Pacific")
            else:
                converted_datetime = self._convert_timezone(
                    datetime.fromtimestamp(int(ts_in_filename)), "GMT", "Europe/Berlin")
            filename_stamp = converted_datetime.strftime('%Y%m%d_%H%M%S')

            # get input file name
            input_filename = self.bagfile_path.split('/')[-1]
            pattern = re.compile(re.escape("original"), re.IGNORECASE)
            name = pattern.sub("split", input_filename).replace(".bag", "")

            st_subfolder = os.environ.get("SILENT_TESTING_SUBFOLDER")
            if event_type == "logbook":
                return f"{OUTPUT_PREFIX}{name}{FILENAME_STR_LOGBOOK}{filename_stamp}.bag"
            if event_type == "guest_logbook":
                return f"{OUTPUT_PREFIX}{name}{FILENAME_STR_GUEST_LOGBOOK}{filename_stamp}.bag"
            if event_type == "silent_testing_cv1" and st_subfolder is not None:
                # create silent testing subfolder under output folder
                st_output_dir = Path(f"{self.output_folder}/{st_subfolder}")
                if not st_output_dir.exists():
                    logger.info(f"creating output directory {st_output_dir} for silent testing snippets")
                    st_output_dir.mkdir(parents=True, exist_ok=True)
                return f"{st_subfolder}/{OUTPUT_PREFIX}{name}.{filename_stamp}.{event_type}.bag"
            elif event_type == "silent_testing_mrc" and st_subfolder is not None:
                # create silent testing subfolder under output folder
                st_output_dir = Path(f"{self.output_folder}/{st_subfolder}")
                if not st_output_dir.exists():
                    logger.info(f"creating output directory {st_output_dir} for silent testing snippets")
                    st_output_dir.mkdir(parents=True, exist_ok=True)
                return f"{st_subfolder}/{OUTPUT_PREFIX}{name}.{filename_stamp}.{event_type}.bag"
            else:
                return f"{OUTPUT_PREFIX}{name}.{filename_stamp}.{event_type}.bag"
        except Exception:
            logger.exception(f"Error constructing childbagfile name!")

    def _generate_childbagfile(self, file_name, output_path, start_ts, end_ts, snippet):
        tracing.start_task("split-subprocess")
        logger.info(f"Splitting bagfile with start ts {start_ts} and end ts {end_ts}")
        try:
            if os.path.isfile(output_path):
                logger.warning(f"The target file {output_path} "
                               f"already exists! "
                               f"No new child bagfile will be generated")
            else:
                check_call([
                    DLA_CONVERTER,
                    "--in", self.bagfile_path,
                    "--time", str(int(start_ts * 1e9)), str(int(end_ts * 1e9)),
                    "--latched",
                    "--out", output_path
                ])
                logger.info(f"bagfile {output_path} created")
            # Metadata will be submitted anyway, regardless if the file already exists
            childbagfile_metadata = self._generate_childbagfile_metadata(file_name, output_path, snippet)
            tracing.end_task()
            return childbagfile_metadata
        except CalledProcessError as e:
            stacktrace = traceback.format_exc()
            tracing.end_task(success=False,
                             attributes={'fatalError', stacktrace})
            logger.exception(f"Bagfile splitting failed for child bagfile {output_path}!")
            raise e

    def _generate_childbagfile_metadata(self, file_name, output_path, snippet):
        return self.metadata_manager.collect_metadata_to_submit(self.childbagfile_link_template.format(file_name),
                                                                output_path,
                                                                file_name,
                                                                snippet)

    def _get_snippets(self, bagfile_guid, bva_limit):
        snippets = self.client.get_snippets_extractor(bagfile_guid,
                                                      bva_limit, generate_child_bagfiles=False)
        return snippets

    def _split(self, snippets):
        """Loop over all snippets, generate one splitted bagfile for each snippet
        and submit metadata to the childbagfile collection
        """
        if not snippets:
            logger.warning("List of snippets is empty. "
                           "Won't create any child bagfiles.")
            return

        for index, s in enumerate(snippets):
            start_ts = s['start'] / 1000.
            end_ts = s['end'] / 1000.
            file_name = self._get_file_name(start_ts,
                                            index,
                                            self._get_event_type_from_snippet(s),
                                            self._get_event_ts_from_snippet(s))
            full_output_file_path = f"{self.output_folder}/{file_name}"

            if os.path.isfile(full_output_file_path):
                logger.warning(f"The target file {full_output_file_path} "
                               f"already exists! "
                               f"No new child bagfile will be generated")
                continue

            # Generate the childbagfile
            child_metadata = self._generate_childbagfile(file_name,
                                                         full_output_file_path,
                                                         start_ts,
                                                         end_ts,
                                                         s)
            # Submit metadata for the childbagfile
            if child_metadata:
                file_guid = self.metadata_manager.submit_child_bagfile_to_file_catalog(child_metadata,
                                                                                       full_output_file_path)
                child_metadata["file_guid"] = file_guid
                self.metadata_manager.submit_metadata(child_metadata)

    def execute(self):
        # Get metadata for the parent file
        self.metadata_manager.load_parent_bagfile_metadata()
        if self.metadata_manager.check_if_parent_indexed() is False:
            logger.warning("Parent bagfile is unindexed! Exiting processing.")
            return
        # Get snippets
        snippets = self._get_snippets(self.bagfile_guid,
                                      self.bva_limit)
        logger.info(f"Snippets: {snippets}")
        # Generate splitted files
        self._split(snippets)


class Stripper:
    def __init__(self, bagfile_guid, bagfile_path, output_folder, childbagfile_link_template, profile):
        self.bagfile_guid = bagfile_guid
        self.bagfile_path = bagfile_path
        self.output_folder = output_folder
        self.childbagfile_link_template = childbagfile_link_template
        self.profile = profile
        self.metadata_manager = MetadataManager(bagfile_guid)

    def _get_profiles(self):
        # Checking whether phoenix_io is superset or not
        if not self._is_phoenix_io_superset(["planning_io", "planning_io_no_camera", "fusion_io", "fusion_io_no_camera",
                                            "localization_and_map_creation_io",
                                            "localization_and_map_creation_io_no_camera"]):
            logger.warning("Phoenix_io needs to be updated.")
        profiles = ([self.profile]
                    if self.profile is not None
                    else STRIPPER_PROFILES)
        return profiles

    @staticmethod
    def _get_stripping_profiles_json():
        if Path(TOPIC_PROFILES_MOUNT_PATH).is_file():
            logger.info("bva_stripping_profiles.json is mounted, use it instead predefined profiles")
            topic_profiles_json = TOPIC_PROFILES_MOUNT_PATH
        else:
            logger.info("Fetching topic profiles from predefined json file.")
            topic_profiles_json = PROFILE_JSON_PATH
        return topic_profiles_json

    def _strip_for_profile(self, profile):
        tracing.start_task("strip-subprocess")
        try:
            logger.info(f"Stripping bagfile for profile {profile}")
            input_file_name = self.bagfile_path.split('/')[-1]
            output_file_name = self._get_file_name(input_file_name, profile)
            output_path = f"{self.output_folder}/{output_file_name}"

            # Generate file for profile
            if not os.path.isfile(output_path):
                if profile in STRIPPER_PROFILES_APPLY_TIME_CORRECTION:
                    logger.info(f"Apply time correction during stripping of the profile {profile}.")
                    check_call([
                        DLA_CONVERTER,
                        "--in", self.bagfile_path,
                        "--profile", self._get_stripping_profiles_json(), str(profile),
                        "--topics", "/vehicle_id", "/vehicle_vin",
                        "--fix-time",
                        "--out", output_path
                    ])
                else:
                    check_call([
                        DLA_CONVERTER,
                        "--in", self.bagfile_path,
                        "--profile", self._get_stripping_profiles_json(), str(profile),
                        "--topics", "/vehicle_id", "/vehicle_vin",
                        "--out", output_path
                    ])
                logger.info(f"Stripped bagfile {output_path} created")
            else:
                logger.exception(f"The target file {output_path} already exists!"
                                 f"No new stripped bagfile will be generated!")

            # Submit metadata for generated file
            child_metadata = self._generate_childbagfile_metadata(output_file_name,
                                                                  output_path,
                                                                  self.childbagfile_link_template.format(output_file_name))
            if child_metadata:
                snippet_lifetime = PROFILE_EXPIRATION_DAYS.get(profile)
                expiration_date = int(time.time() * 1000) + snippet_lifetime * 1000 * 3600 * 24 if snippet_lifetime \
                    else None
                tags = [tag for tag, profiles in APPLY_TAGS.items() if profile in profiles]
                file_guid = self.metadata_manager.submit_child_bagfile_to_file_catalog(
                    child_metadata, output_path, expiration_date, tags
                )
                child_metadata["file_guid"] = file_guid
                self.metadata_manager.submit_metadata(child_metadata)

            tracing.end_task()
        except CalledProcessError as e:
            stacktrace = traceback.format_exc()
            tracing.task_attribute('fatalError', stacktrace)
            tracing.end_task(success=False)
            logger.exception(f"Bagfile stripping failed for "
                             f"bagfile {self.bagfile_path} and profile {profile}!")
            raise e

    @staticmethod
    def _get_file_name(input_filename, profile):
        name = input_filename.split(".")[0]
        pattern = re.compile(re.escape("original"), re.IGNORECASE)
        return pattern.sub("stripped", f"{OUTPUT_PREFIX}{name}_{profile}.bag")

    def _generate_childbagfile_metadata(self, file_name, output_path, childbagfile_link_template):
        return self.metadata_manager.collect_metadata_to_submit(childbagfile_link_template,
                                                                output_path,
                                                                file_name)

    def _is_phoenix_io_superset(self, superset_check_profiles):
        superset_list = []
        with open(self._get_stripping_profiles_json()) as f:
            topic_profiles = json.load(f)
        try:
            for topic_profile in superset_check_profiles:
                superset_list.extend(topic_profiles[topic_profile])
            if set(superset_list) == set(topic_profiles["phoenix_io"]):
                return True
            else:
                logger.warning("Phoenix_io should contain these topics", set(superset_list))
                return False
        except Exception as e:
            logger.exception(f"Failed to check the topic profile.", e)

    def execute(self):
        # Get metadata for the parent file
        self.metadata_manager.load_parent_bagfile_metadata()
        if self.metadata_manager.check_if_parent_indexed() is False:
            logger.warning("Parent bagfile is unindexed! Exiting processing.")
            return
        # Get profiles
        profiles = self._get_profiles()
        # Generate stripped files
        for p in profiles:
            self._strip_for_profile(p)


def build_state_event(status, message=None):
    # unix timestamp in ms resolution
    now = int(datetime.now(tz=timezone.utc).timestamp() * 1000)

    state = {
        'name': os.getenv("MODE", "splitter"),
        'version': VERSION,
        'time': now,
        'status': status,
        'report': {}
    }
    if message:
        state['report']['message'] = message
    return state


def _submit_state_updates(
        client, bagfile_guid, status, started_at, message=None):
    legacy_name = os.getenv('MODE', "splitter")

    run_update, state_update = _build_state_updates(
        status, started_at, message)

    if 'FLOW_RUN_GUID' in os.environ:
        client.add_flow_run_step_run(os.environ['FLOW_RUN_GUID'],
                                     'danf-' + legacy_name,
                                     run_update)

    client.add_bagfile_state_event(bagfile_guid,
                                   state_update)


def _build_state_updates(status, started_at, message=None):
    legacy_name = os.getenv('MODE', "splitter")
    map_status_to_legacy_status = {
        'SUCCESSFUL': 'SUCCESS',
        'FAILED': 'FAILED'
    }

    now = danfutils.get_timestamp()
    report = {}
    if message:
        report['message'] = message

    flow_update = {
        'started_at': started_at,
        'completed_at': now,
        'status': status,
        'report': report
    }
    state_update = {
        'name': legacy_name,
        'version': VERSION,
        'time': now,
        'status': map_status_to_legacy_status[status],
        'report': report
    }
    return flow_update, state_update


def ensure_directory_exists(directory_path: str):
    directory = Path(directory_path)
    if directory.exists():
        logger.info(f"directory {directory} already exists")
    else:
        logger.info(f"creating directory {directory}")
        directory.mkdir(parents=True, exist_ok=True)


def execute_splitter():
    tracing.start_task('split')
    started_at = danfutils.get_timestamp()

    # read input configuration from environment
    bva_limit = int(os.environ["BVA_THRESHOLD"])
    input_bagfile_guid = os.environ['BAGFILE_GUID']
    bagfile_path = os.environ['BAGFILE_PATH']
    output_folder = os.environ['OUTPUT_FOLDER']
    childbagfile_link_template = os.environ.get('CHILDBAGFILE_LINK_TEMPLATE', '{}')

    ensure_directory_exists(output_folder)

    client = ContainerClient()
    splitter = Splitter(bva_limit,
                        input_bagfile_guid,
                        bagfile_path,
                        output_folder,
                        childbagfile_link_template)

    try:
        splitter.execute()
    except Exception:
        logger.exception(f"Failed to split bagfile guid: {input_bagfile_guid}")
        stacktrace = traceback.format_exc()
        tracing.task_attribute('fatalError', stacktrace)
        _submit_state_updates(
            client, input_bagfile_guid, 'FAILED', started_at, message=stacktrace)
        tracing.end_task(success=False)
        sys.exit(1)
    else:
        _submit_state_updates(
            client, input_bagfile_guid, 'SUCCESSFUL', started_at)
        tracing.end_task()


def execute_stripper():
    tracing.start_task("strip")
    started_at = danfutils.get_timestamp()

    # read input configuration from environment
    input_bagfile_guid = os.environ['BAGFILE_GUID']
    bagfile_path = os.environ["BAGFILE_PATH"]
    output_folder = os.environ["OUTPUT_FOLDER"]
    profile = os.environ.get("STRIPPER_PROFILE")
    childbagfile_link_template = os.environ.get('CHILDBAGFILE_LINK_TEMPLATE', '{}')

    ensure_directory_exists(output_folder)

    client = ContainerClient()
    stripper = Stripper(input_bagfile_guid, bagfile_path, output_folder, childbagfile_link_template, profile)

    try:
        stripper.execute()
    except Exception:
        logger.exception(f"Failed to strip bagfile guid: {input_bagfile_guid}")
        stacktrace = traceback.format_exc()
        tracing.task_attribute('fatalError', stacktrace)
        _submit_state_updates(
            client, input_bagfile_guid, 'FAILED', started_at, message=stacktrace)
        tracing.end_task(success=False)
        sys.exit(1)
    else:
        _submit_state_updates(
            client, input_bagfile_guid, 'SUCCESSFUL', started_at)
        tracing.end_task()


def execute_migrate():
    tracing.start_task("migrate")
    started_at = danfutils.get_timestamp()

    # read input configuration from environment
    input_bagfile_guid = os.environ['BAGFILE_GUID']
    bagfile_path = os.environ["BAGFILE_PATH"]
    output_folder = os.environ["OUTPUT_FOLDER"]
    childbagfile_link_template = os.environ.get('CHILDBAGFILE_LINK_TEMPLATE', '{}')

    ensure_directory_exists(output_folder)
    client = ContainerClient()
    metadata_manager = MetadataManager(input_bagfile_guid)

    input_file_name = bagfile_path.split('/')[-1]
    output_file_path = f"{output_folder}/{input_file_name}"
    copy_file = True
    if os.path.isfile(output_file_path):
        if os.path.getsize(output_file_path) == os.path.getsize(bagfile_path):
            copy_file = False
            logger.warning(f"The target file {output_file_path} already exists!"
                           f"Bagfile will not be copied")
            _submit_state_updates(
                client, input_bagfile_guid, 'SUCCESSFUL', started_at)
            tracing.end_task()
    if copy_file:
        try:
            copy2(bagfile_path, output_folder)
            metadata_manager.update_bagfile_path(childbagfile_link_template)
            metadata_manager.add_accessor_url_to_file_catalog(output_folder)
        except Exception as e:
            stacktrace = traceback.format_exc()
            tracing.task_attribute('fatalError', stacktrace)
            logger.exception(f"Execution of migrating files failed, due to {e}")
            _submit_state_updates(
                client, input_bagfile_guid, 'FAILED', started_at, message=stacktrace)
            tracing.end_task(success=False)
            sys.exit(1)
        else:
            logger.info(f"Migrating file {bagfile_path} to {output_folder}")
            _submit_state_updates(
                client, input_bagfile_guid, 'SUCCESSFUL', started_at)
            tracing.end_task()


def main():
    operation_mode = os.environ.get('MODE')
    if not operation_mode:
        logger.warning("MODE env variable not set. "
                       "Using 'splitter' as operation mode")
        operation_mode = 'splitter'

    logger.info(f"Running in {operation_mode} mode!")

    if operation_mode == 'splitter':
        execute_splitter()
    elif operation_mode == 'stripper':
        execute_stripper()
    elif operation_mode == 'migrate':
        execute_migrate()
    else:
        logger.error(f"MODE {operation_mode} is not valid, "
                     f"must be set to 'splitter' or 'stripper'!")
        sys.exit(1)


if __name__ == '__main__':
    if "LOG_FILE" in os.environ:
        log_file = os.getenv("LOG_FILE")
        configure_root_logger(
            log_level='INFO',
            enable_azure_log_aggregation=danf_env().platform != Platform.LOCAL,
            file_path=log_file
        )
    else:
        configure_root_logger(
            log_level='INFO',
            enable_azure_log_aggregation=danf_env().platform != Platform.LOCAL
        )

    log_level = 'INFO'
    logger.setLevel(log_level)
    logger.info(f"Log-Level set to {log_level}")
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")

    main()

    logger.info("Done")