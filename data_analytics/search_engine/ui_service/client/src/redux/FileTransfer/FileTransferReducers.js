import { createReducer } from '@reduxjs/toolkit';
import * as actions from './FileTransferActions';
import { getTransferRequestById } from './FileTransferUtils'

export const FileTeansferReducer = createReducer({}, {
    [actions.requestFileTransferSuccess]: (state, action) => {
        let file = state.overview.file;
        let createdTransfer = action.payload.data;

        for (let i = 0; i < file.locations.length; i++) {
            let location = file.locations[i];
            if (location.cluster.id === createdTransfer.targetCluster) {
                location.transfer = createdTransfer;
                location.status = "Transfer Requested";
            }
        }
        state.overview.file = file;
        state.error = false;
        markRequestAsFinished(state, action.payload.requestId)
    },
    [actions.requestFileTransferError]: (state, action) => {
        markRequestAsFinished(state, action.payload.requestId)
        state.error = true;
        state.errorMessageType = 'danger';
        if (action.payload.error.response.data.type === "apiHealthError") {
            state.errorMessage = action.payload.error.response.data.message
        } else {
            state.errorMessage = "Transfer could not be requested.";
        }
    },

    [actions.requestFileTransferBegin]: (state, action) => {
        const requestId = action.payload.requestId;

        let existingRequest = getTransferRequestById(state, requestId);

        if (existingRequest) {
            existingRequest.ongoing = true;
            return;
        }

        const request = {
            requestId: requestId,
            ongoing: true
        }
        state.httpRequests.requestFileTransferRequests.ongoing.push(request);
    },
});

function markRequestAsFinished(state, requestId) {
    let request = getTransferRequestById(state, requestId);
    if (!request) {
        return;
    }
    request.ongoing = false;
}