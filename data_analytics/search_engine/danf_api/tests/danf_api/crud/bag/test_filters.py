"""Testing Bag related filters."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import pytest

from search_engine.danf_api.danf_api.crud.bag.filters import (
    _construct_guid_filter,
    _construct_vehicle_id_num_filter,
    _construct_topics_filter,
    _construct_drive_types_filter,
    _construct_file_name_filter,
    _construct_origin_filter,
    _construct_start_filter,
    _construct_end_filter,
    _construct_duration_filter,
    _construct_size_filter,
    _construct_num_messages_filter,
    _construct_url_filter,
    construct_bag_filter,
)
from search_engine.danf_api.danf_api.models.base import OpValue
from search_engine.danf_api.danf_api.models.bag import BagFilterParams
from search_engine.danf_api.danf_api.core.errors import RHSColonOperatorError, RHSColonValueError


def test_guid_filter_op_correct():
    """Testing construct guid filter with proper operator and value

    GIVEN the correct operator eq (equal) and value
    WHEN run the _construct_guid_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="eq", value="test_value")

    # run
    return_value = _construct_guid_filter([opvalue])

    # assert
    assert return_value == {"guid": {"$in": ["test_value"]}}


def test_guid_filter_both_op_correct():
    """Testing both of the correct operators for guid filter construction

    GIVEN correct operators and values
    WHEN run the _construct_guid_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value_1")
    opvalue_2 = OpValue(op="", value="test_value_2")

    # run
    return_value = _construct_guid_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"guid": {"$in": ["test_value_1", "test_value_2"]}}


def test_guid_filter_incorrect_op():
    """Testing construct guid filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_guid_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="test_value")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_guid_filter([opvalue])


def test_guid_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value")
    opvalue_2 = OpValue(op="pre", value="test_value_2")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_guid_filter([opvalue_1, opvalue_2])

    return_value = _construct_guid_filter([opvalue_1])

    # assert
    assert return_value == {"guid": {"$in": ["test_value"]}}


def test_vehicle_id_num_filter_op_correct():
    """Testing construct vehicle_id_num filter with proper operator and value

    GIVEN the correct operator eq (equal) and value
    WHEN run the _construct_vehicle_id_num_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="eq", value="test_value")

    # run
    return_value = _construct_vehicle_id_num_filter([opvalue])

    # assert
    assert return_value == {"vehicle_id_num": {"$in": ["test_value"]}}


def test_vehicle_id_num_filter_both_op_correct():
    """Testing both of the correct operators for vehicle_id_num filter construction

    GIVEN correct operators and values
    WHEN run the _construct_vehicle_id_num_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value_1")
    opvalue_2 = OpValue(op="", value="test_value_2")

    # run
    return_value = _construct_vehicle_id_num_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"vehicle_id_num": {"$in": ["test_value_1", "test_value_2"]}}


def test_vehicle_id_num_filter_incorrect_op():
    """Testing construct vehicle_id_num filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_vehicle_id_num_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="test_value")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_vehicle_id_num_filter([opvalue])


def test_vehicle_id_num_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value")
    opvalue_2 = OpValue(op="pre", value="test_value_2")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_vehicle_id_num_filter([opvalue_1, opvalue_2])

    return_value = _construct_vehicle_id_num_filter([opvalue_1])

    # assert
    assert return_value == {"vehicle_id_num": {"$in": ["test_value"]}}


def test_topics_filter_op_correct():
    """Testing construct topics filter with proper operator and value

    GIVEN the correct operator eq (equal) and value
    WHEN run the _construct_topics_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="eq", value="test_value")

    # run
    return_value = _construct_topics_filter([opvalue])

    # assert
    assert return_value == {"topics": {"$all": ["test_value"]}}


def test_topics_filter_both_op_correct():
    """Testing both of the correct operators for topics filter construction

    GIVEN correct operators and values
    WHEN run the _construct_topics_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value_1")
    opvalue_2 = OpValue(op="eq", value="test_value_2")

    # run
    return_value = _construct_topics_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"topics": {"$all": ["test_value_1", "test_value_2"]}}


def test_topics_filter_incorrect_op():
    """Testing construct topics filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_topics_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="test_value")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_topics_filter([opvalue])


def test_topics_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value")
    opvalue_2 = OpValue(op="pre", value="test_value_2")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_topics_filter([opvalue_1, opvalue_2])

    return_value = _construct_topics_filter([opvalue_1])

    # assert
    assert return_value == {"topics": {"$all": ["test_value"]}}


def test_drive_types_filter_op_correct():
    """Testing construct drive_types filter with proper operator and value.

    GIVEN the correct operator eq (equal) and value
    WHEN run the _construct_drive_types_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="eq", value="test_value")

    # run
    return_value = _construct_drive_types_filter([opvalue])

    # assert
    assert return_value == {"drive_types": {"$in": ["test_value"]}}


def test_drive_types_filter_both_op_correct():
    """Testing both of the correct operators for drive_types filter construction

    GIVEN correct operators and values
    WHEN run the _construct_drive_types_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value_1")
    opvalue_2 = OpValue(op="", value="test_value_2")

    # run
    return_value = _construct_drive_types_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"drive_types": {"$in": ["test_value_1", "test_value_2"]}}


def test_drive_types_filter_incorrect_op():
    """Testing construct drive_types filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_drive_types_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="test_value")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_drive_types_filter([opvalue])


def test_drive_types_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value")
    opvalue_2 = OpValue(op="pre", value="test_value_2")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_drive_types_filter([opvalue_1, opvalue_2])

    return_value = _construct_drive_types_filter([opvalue_1])

    # assert
    assert return_value == {"drive_types": {"$in": ["test_value"]}}


def test_file_name_filter_op_correct():
    """Testing construct file_name filter with proper operator and value.

    GIVEN the correct operator eq (equal) and value
    WHEN run the _construct_file_name_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="eq", value="test_value")

    # run
    return_value = _construct_file_name_filter([opvalue])

    # assert
    assert return_value == {"$or": [{"file_name": {"$eq": "test_value"}}]}


def test_file_name_filter_both_op_correct():
    """Testing both of the correct operators for file_name filter construction

    GIVEN correct operators and values
    WHEN run the _construct_file_name_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value_1")
    opvalue_2 = OpValue(op="", value="test_value_2")

    # run
    return_value = _construct_file_name_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"$or": [{"file_name": {"$eq": "test_value_1"}}, {"file_name": {"$eq": "test_value_2"}}]}


def test_file_name_filter_incorrect_op():
    """Testing construct file_name filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_file_name_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="test_value")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_file_name_filter([opvalue])


def test_file_name_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value")
    opvalue_2 = OpValue(op="pre", value="test_value_2")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_file_name_filter([opvalue_1, opvalue_2])

    return_value = _construct_file_name_filter([opvalue_1])

    # assert
    assert return_value == {"$or": [{"file_name": {"$eq": "test_value"}}]}


def test_file_name_regex_op_correct():
    """Testing construct file_name filter with regex operator

    GIVEN the correct operator re (regex) and value
    WHEN run the _construct_file_name_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="re", value="test_value")

    # run
    return_value = _construct_file_name_filter([opvalue])

    # assert
    assert return_value == {"$or": [{"file_name": {"$regex": "test_value"}}]}


def test_origin_filter_op_correct():
    """Testing construct origin filter with proper operator and value.

    GIVEN the correct operator eq (equal) and value
    WHEN run the _construct_origin_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="eq", value="test_value")

    # run
    return_value = _construct_origin_filter([opvalue])

    # assert
    assert return_value == {"origin": {"$in": ["test_value"]}}


def test_origin_filter_both_op_correct():
    """Testing both of the correct operators for origin filter construction

    GIVEN correct operators and values
    WHEN run the _construct_origin_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value_1")
    opvalue_2 = OpValue(op="", value="test_value_2")

    # run
    return_value = _construct_origin_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"origin": {"$in": ["test_value_1", "test_value_2"]}}


def test_origin_filter_incorrect_op():
    """Testing construct origin filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_origin_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="test_value")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_origin_filter([opvalue])


def test_origin_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="test_value")
    opvalue_2 = OpValue(op="pre", value="test_value_2")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_origin_filter([opvalue_1, opvalue_2])

    return_value = _construct_origin_filter([opvalue_1])

    # assert
    assert return_value == {"origin": {"$in": ["test_value"]}}


def test_start_filter_with_op_correct():
    """
    Testing construct start filter with proper operator and value

    GIVEN the correct operator gte (greater than equal) and value
    WHEN run the _construct_start_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="gte", value="123")

    # run
    return_value = _construct_start_filter([opvalue])

    # assert
    assert return_value == {"start": {"$gte": 123}}


def test_start_filter_both_eq_op_correct():
    """Testing both of the correct operators for start filter construction

    GIVEN correct operators and values
    WHEN run the _construct_start_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="123")
    opvalue_2 = OpValue(op="", value="456")

    # run
    return_value = _construct_start_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"start": {"$eq": 456}}


def test_start_filter_both_int_op_correct():
    """Testing both of the correct operators for start filter construction

    GIVEN correct operators and values
    WHEN run the _construct_start_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="lt", value="456")

    # run
    return_value = _construct_start_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"start": {"$gte": 123, "$lt": 456}}


def test_start_filter_incorrect_op():
    """Testing construct start filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_start_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="123")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_start_filter([opvalue])


def test_start_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="pre", value="456")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_start_filter([opvalue_1, opvalue_2])

    return_value = _construct_start_filter([opvalue_1])

    # assert
    assert return_value == {"start": {"$gte": 123}}


def test_start_filter_incorrect_value():
    """Testing construct start filter with correct operator, but incorrect value

    GIVEN valid operator and incorrect value
    WHEN run the _construct_start_filter function with RHSColonValueError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="gt", value="test_value")

    # run
    with pytest.raises(RHSColonValueError):
        _construct_start_filter([opvalue])


def test_end_filter_with_op_correct():
    """
    Testing construct end filter with proper operator and value

    GIVEN the correct operator gte (greater than equal) and value
    WHEN run the _construct_end_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="gte", value="123")

    # run
    return_value = _construct_end_filter([opvalue])

    # assert
    assert return_value == {"end": {"$gte": 123}}


def test_end_filter_both_eq_op_correct():
    """Testing both of the correct operators for end filter construction

    GIVEN correct operators and values
    WHEN run the _construct_end_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="123")
    opvalue_2 = OpValue(op="", value="456")

    # run
    return_value = _construct_end_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"end": {"$eq": 456}}


def test_end_filter_both_int_op_correct():
    """Testing both of the correct operators for end filter construction

    GIVEN correct operators and values
    WHEN run the _construct_end_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="lt", value="456")

    # run
    return_value = _construct_end_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"end": {"$gte": 123, "$lt": 456}}


def test_end_filter_incorrect_op():
    """Testing construct end filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_end_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="123")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_end_filter([opvalue])


def test_end_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="pre", value="456")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_end_filter([opvalue_1, opvalue_2])

    return_value = _construct_end_filter([opvalue_1])

    # assert
    assert return_value == {"end": {"$gte": 123}}


def test_end_filter_incorrect_value():
    """Testing construct end filter with correct operator, but incorrect value

    GIVEN valid operator and incorrect value
    WHEN run the _construct_end_filter function with RHSColonValueError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="gt", value="test_value")

    # run
    with pytest.raises(RHSColonValueError):
        _construct_end_filter([opvalue])


def test_duration_filter_with_op_correct():
    """
    Testing construct duration filter with proper operator and value

    GIVEN the correct operator gte (greater than equal) and value
    WHEN run the _construct_duration_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="gte", value="123")

    # run
    return_value = _construct_duration_filter([opvalue])

    # assert
    assert return_value == {"duration": {"$gte": 123}}


def test_duration_filter_both_eq_op_correct():
    """Testing both of the correct operators for duration filter construction

    GIVEN correct operators and values
    WHEN run the _construct_duration_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="123")
    opvalue_2 = OpValue(op="", value="456")

    # run
    return_value = _construct_duration_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"duration": {"$eq": 456}}


def test_duration_filter_both_int_op_correct():
    """Testing both of the correct operators for duration filter construction

    GIVEN correct operators and values
    WHEN run the _construct_duration_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="lt", value="456")

    # run
    return_value = _construct_duration_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"duration": {"$gte": 123, "$lt": 456}}


def test_duration_filter_incorrect_op():
    """Testing construct duration filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_duration_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="123")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_duration_filter([opvalue])


def test_duration_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="pre", value="456")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_duration_filter([opvalue_1, opvalue_2])

    return_value = _construct_duration_filter([opvalue_1])

    # assert
    assert return_value == {"duration": {"$gte": 123}}


def test_duration_filter_incorrect_value():
    """Testing construct duration filter with correct operator, but incorrect value

    GIVEN valid operator and incorrect value
    WHEN run the _construct_duration_filter function with RHSColonValueError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="gt", value="test_value")

    # run
    with pytest.raises(RHSColonValueError):
        _construct_duration_filter([opvalue])


def test_size_filter_with_op_correct():
    """
    Testing construct size filter with proper operator and value

    GIVEN the correct operator gte (greater than equal) and value
    WHEN run the _construct_size_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="gte", value="123")

    # run
    return_value = _construct_size_filter([opvalue])

    # assert
    assert return_value == {"size": {"$gte": 123}}


def test_size_filter_both_eq_op_correct():
    """Testing both of the correct operators for size filter construction

    GIVEN correct operators and values
    WHEN run the _construct_size_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="123")
    opvalue_2 = OpValue(op="", value="456")

    # run
    return_value = _construct_size_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"size": {"$eq": 456}}


def test_size_filter_both_int_op_correct():
    """Testing both of the correct operators for size filter construction

    GIVEN correct operators and values
    WHEN run the _construct_size_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="lt", value="456")

    # run
    return_value = _construct_size_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"size": {"$gte": 123, "$lt": 456}}


def test_size_filter_incorrect_op():
    """Testing construct size filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_size_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="123")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_size_filter([opvalue])


def test_size_filter_list_with_one_incorrect_op():
    """Testing with correct and incorrect operators, if the incorrect one is
    catched by the proper error handling and the correct value returns

    GIVEN two op-value pairs
    WHEN test for error handling with pytest.raises and use the construct function
    THEN check if the expected and the return value matches
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="pre", value="456")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_size_filter([opvalue_1, opvalue_2])

    return_value = _construct_size_filter([opvalue_1])

    # assert
    assert return_value == {"size": {"$gte": 123}}


def test_size_filter_incorrect_value():
    """Testing construct size filter with correct operator, but incorrect value

    GIVEN valid operator and incorrect value
    WHEN run the _construct_size_filter function with RHSColonValueError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="gt", value="test_value")

    # run
    with pytest.raises(RHSColonValueError):
        _construct_size_filter([opvalue])


def test_num_messages_filter_with_op_correct():
    """
    Testing construct num_messages filter with proper operator and value

    GIVEN the correct operator gte (greater than equal) and value
    WHEN run the _construct_num_messages_filter function with the given opvalue
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue = OpValue(op="gte", value="123")

    # run
    return_value = _construct_num_messages_filter([opvalue])

    # assert
    assert return_value == {"num_messages": {"$gte": 123}}


def test_num_messages_filter_both_eq_op_correct():
    """Testing both of the correct operators for num_messages filter construction

    GIVEN correct operators and values
    WHEN run the _construct_num_messages_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="eq", value="123")
    opvalue_2 = OpValue(op="", value="456")

    # run
    return_value = _construct_num_messages_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"num_messages": {"$eq": 456}}


def test_num_messages_filter_both_int_op_correct():
    """Testing both of the correct operators for num_messages filter construction

    GIVEN correct operators and values
    WHEN run the _construct_num_messages_filter function with the given opvalues
    THEN examine if the return_value matches the expected value
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="lt", value="456")

    # run
    return_value = _construct_num_messages_filter([opvalue_1, opvalue_2])

    # assert
    assert return_value == {"num_messages": {"$gte": 123, "$lt": 456}}


def test_num_messages_filter_incorrect_op():
    """Testing construct num_messages filter with incorrect operator, but correct value

    GIVEN not existing operator and valid value
    WHEN run the _construct_num_messages_filter function with RHSColonOperatorError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="abc", value="123")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_num_messages_filter([opvalue])


def test_num_messages_filter_list_with_one_incorrect_op():
    """Testing construct num_messages filter with one correct and one incorrect operator

    GIVEN a valid opvalue pair and another one with not existing operator
    WHEN run the _construct_num_messages_filter function with the given opvalues
    THEN examine if the expected correct value is matching with the return value and catch the fault with the expected
    error handler
    """
    # setup env
    opvalue_1 = OpValue(op="gte", value="123")
    opvalue_2 = OpValue(op="pre", value="456")

    # run
    with pytest.raises(RHSColonOperatorError):
        _construct_num_messages_filter([opvalue_1, opvalue_2])

    return_value = _construct_num_messages_filter([opvalue_1])

    # assert
    assert return_value == {"num_messages": {"$gte": 123}}


def test_num_messages_filter_incorrect_value():
    """Testing construct num_messages filter with correct operator, but incorrect value

    GIVEN valid operator and incorrect value
    WHEN run the _construct_num_messages_filter function with RHSColonValueError raise
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    opvalue = OpValue(op="gt", value="test_value")

    # run
    with pytest.raises(RHSColonValueError):
        _construct_num_messages_filter([opvalue])


def test_construct_bag_filter_empty():
    """
    Testing construct bag filter with empty BagFilterParams

    GIVEN the filter_params without values to each filters
    WHEN run the construct_bag_filter function with the given filter_params
    THEN examine if the return_value matches the expected value, in this case empty dict
    """
    # setup env
    filter_params = BagFilterParams()
    # run
    return_value = construct_bag_filter(filter_params)

    # assert
    assert return_value == {}


def test_construct_bag_some_filters():
    """
    Testing construct bag filter with some filter in BagFilterParams

    GIVEN the filter_params with some filter values
    WHEN run the construct_bag_filter function with the given filter_params
    THEN examine if the return_value matches the expected value
    """
    # setup env
    filter_params = BagFilterParams(
        guid=["eq:test_value"],
        start=["gte:123"],
        end=["lte:456"],
    )
    # run
    return_value = construct_bag_filter(filter_params)

    # assert
    assert return_value == {
        "$and": [{"guid": {"$in": ["test_value"]}}, {"start": {"$gte": 123}}, {"end": {"$lte": 456}}]
    }


def test_construct_bag_all_filters():
    """
    Testing construct bag filter with all filter added in BagFilterParams

    GIVEN the filter_params with all filter values
    WHEN run the construct_bag_filter function with the given filter_params
    THEN examine if the return_value matches the expected value
    """
    # setup env
    filter_params = BagFilterParams(
        guid=["eq:test_value"],
        start=["gte:123"],
        end=["lte:456"],
        duration=["eq:333"],
        vehicle_id_num=["eq:test_value_1"],
        topics=["eq:test_value_2"],
        size=["gte:1"],
        num_messages=["lt:6"],
        drive_types=["eq:test_value_3"],
        file_name=["re:test_value_4"],
        origin=["eq:DE"],
    )
    # run
    return_value = construct_bag_filter(filter_params)

    # assert
    assert return_value == {
        "$and": [
            {"guid": {"$in": ["test_value"]}},
            {"start": {"$gte": 123}},
            {"end": {"$lte": 456}},
            {"duration": {"$eq": 333}},
            {"vehicle_id_num": {"$in": ["test_value_1"]}},
            {"topics": {"$all": ["test_value_2"]}},
            {"size": {"$gte": 1}},
            {"num_messages": {"$lt": 6}},
            {"drive_types": {"$in": ["test_value_3"]}},
            {"$or": [{"file_name": {"$regex": "test_value_4"}}]},
            {"origin": {"$in": ["DE"]}},
        ]
    }


def test_construct_bag_filters_with_one_incorrect_op():
    """
    Testing construct bag filter with all filter added in BagFilterParams

    GIVEN the filter_params with all filter values, but the start filter has wrong operator
    WHEN run the construct_bag_filter function with the given filter_params
    THEN examine if the function runs correctly with the expected error handler
    """
    # setup env
    filter_params = BagFilterParams(
        guid=["eq:test_value"],
        start=["pre:123"],
        end=["lte:456"],
        duration=["eq:333"],
        vehicle_id_num=["eq:test_value_1"],
        topics=["eq:test_value_2"],
        size=["gte:1"],
        num_messages=["lt:6"],
        drive_types=["eq:test_value_3"],
        file_name=["re:test_value_4"],
        origin=["eq:DE"],
    )

    # run and assert
    with pytest.raises(RHSColonOperatorError):
        construct_bag_filter(filter_params)
