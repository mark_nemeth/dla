"""Regressiontests for child bagfile routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from copy import deepcopy
import os

import requests
from DANFUtils.utils import generate_deterministic_guid, generate_guid
os.environ["DANF_ENV"] = "local"
from tests.regressiontests.testutils import get_testdb_connector

# testdata
# can change the localhost into external ip:
base_url = 'http://localhost:8887'
feature_bucket_url = base_url + '/featurebucket/'
features_url_template = feature_bucket_url + '{}/features'

raw_data_bagfile = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "origin": "DE"
}

raw_data_feature_bucket = {
    "version": "1.0",
    "bagfile_guid": raw_data_bagfile['guid'],
    "start": 1475272825000,
    "end": 1475272867000,
    "start_index": 90,
    "end_index": 120,
    "file_name": "test_filename",
    "link": "test_link/test_filename",
    "size": 123456,
    "vehicle_id_num": "v222_test",
    "num_messages": 12345,
    "topics": ["topic1", "topic2"],
    "features": ["tag1", "tag2"],
    "drive_types": ["drive_types1", "drive_types2"],
    "parent_index_start": 25,
    "parent_index_end": 67,
    "metadata": {
        "is_test_data": 1
    }
}


def generate_deterministic_bucket_guid(bagfile_guid, start, end):
    unique_key = f"{bagfile_guid}_{start}_{end}"
    return generate_guid(unique_url=unique_key)


raw_data_feature_bucket["guid"] = generate_deterministic_bucket_guid(
    raw_data_feature_bucket["bagfile_guid"], raw_data_feature_bucket["start"],
    raw_data_feature_bucket["end"])


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
        con.db['feature_buckets'].delete_many({'metadata.is_test_data': 1})
        con.db['bagfiles'].insert_one(raw_data_bagfile)


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
        con.db['feature_buckets'].delete_many({'metadata.is_test_data': 1})


#works
class TestFeatureBucketPOST(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_feature_bucket_POST(self):
        data = deepcopy(raw_data_feature_bucket)

        res = requests.post(url=feature_bucket_url, json=data)
        self.assertEqual(201, res.status_code, 'Feature Bucket post failed')
        self.assertRegex(
            res.headers['Location'],
            '/featurebucket/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_feature_bucket_POST_remove_unrequired_attributes(self):
        data = deepcopy(raw_data_feature_bucket)
        del data['topics']
        del data['size']

        res = requests.post(url=feature_bucket_url, json=data)

        self.assertEqual(201, res.status_code, 'Feature Bucket post failed')
        self.assertRegex(
            res.headers['Location'],
            '/featurebucket/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_feature_bucket_POST_missing_attributes(self):
        data = deepcopy(raw_data_feature_bucket)
        del data['guid']
        del data['bagfile_guid']

        res = requests.post(url=feature_bucket_url, json=data)

        self.assertEqual(400, res.status_code,
                         'Missing attributes not detected')


#works
class TestFeatureBucketGET(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_feature_bucket_GET(self):
        data = deepcopy(raw_data_feature_bucket)
        connector = get_testdb_connector()
        with connector as con:
            con.add_feature_buckets([data])
        res = requests.get(url=feature_bucket_url +
                           raw_data_feature_bucket['guid'])
        self.assertDictEqual(raw_data_feature_bucket,
                             res.json()['results'][0],
                             'Feature Bucket get failed')

    def test_feature_bucket_GET_invalid(self):
        res = requests.get(url=feature_bucket_url +
                           '12341234-1234-1234-1234-123412341235')
        self.assertEqual(404, res.status_code, 'Invalid GUID not detected')


#works
class TestFeatureBucketsPATCH(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_feature_bucket_PATCH(self):
        data = deepcopy(raw_data_feature_bucket)
        connector = get_testdb_connector()
        with connector as con:
            con.add_feature_buckets([data])
        patch_data = deepcopy(raw_data_feature_bucket)
        del patch_data['guid']
        patch_data['version'] = "2.0"
        res = requests.patch(url=feature_bucket_url +
                             raw_data_feature_bucket['guid'],
                             json=patch_data)
        self.assertEqual(200, res.status_code, 'Feature Bucket patch failed')

    def test_feature_bucket_PATCH_when_unknown_guid_should_return_404(self):
        data = deepcopy(raw_data_feature_bucket)
        del data['guid']
        data['version'] = "2.0"
        res = requests.patch(url=feature_bucket_url +
                             '12341234-1234-1234-1234-123412341235',
                             json=data)
        self.assertEqual(404, res.status_code, 'Invalid GUID not detected')


#works
class TestFeatureBucketDELETE(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_feature_bucket_DELETE_should_delete(self):
        data = deepcopy(raw_data_feature_bucket)
        connector = get_testdb_connector()
        with connector as con:
            con.add_feature_buckets([data])

        res = requests.delete(url=feature_bucket_url +
                              raw_data_feature_bucket['guid'])
        self.assertEqual(200, res.status_code, 'Feature Bucket delete failed')

    def test_feature_bucket_DELETE_when_unknown_guid_should_return_404(self):
        res = requests.delete(url=feature_bucket_url +
                              '12341234-1234-1234-1234-123412341235')
        self.assertEqual(404, res.status_code, 'Invalid GUID not detected')


if __name__ == '__main__':
    unittest.main()
