/* *******************************************************************************
 * COPYRIGHT: (c) 2017-2019 Daimler AG and Robert Bosch GmbH
 *
 * The reproduction, distribution and utilization of this file as well as the
 * communication of its contents to others without express authorization is
 * prohibited. Offenders will be held liable for the payment of damages and can
 * be prosecuted. All rights reserved particularly in the event of the grant of
 * a patent, utility model or design.
 *
 * @file aiostream.h
 *
 * @brief      This file defines the class AIO stream API
 *
 * @author     Jurij Gera, Roman Pavlov
 * @date       Jan 29 2019
 *********************************************************************************/

#ifndef AIOSTREAM_H_
#define AIOSTREAM_H_

#ifdef __cplusplus
extern "C" {
#endif

// Maximum buffer space that can be requested via single hps_aio_write_begin() call [bytes].
#define MAX_MESSAGE_SIZE		(64 * 1024 * 1024)

struct aioStreamT;

/*
 * Open AIO stream.
 * AIO file stream is optimized for high speed sequential file writing.
 * You must provide either file name or existing file descriptor.
 * If fd is -1, you must provide file name. The AIO stream will create the file internally
 * and close it when hps_aio_close() is called. If fd is provided, the AIO stream will use
 * the provided file descriptor and will not close it in the hps_aio_close().
 * @return AIO stream pointer (handle).
 */
aioStreamT *hps_aio_open(const char *uri, int fd);

/*
 * This function is used to begin writing to the stream.
 * Usage pattern:
 * 1) Call hps_aio_write_begin(X) to request X byte space.
 *    The function will return pointer to sufficient linear space within internal AIO buffer.
 * 2) Copy at most Y <= X bytes of data into the provided buffer space.
 * 3) Call hps_aio_write_end(Y) to commit Y bytes to the file.
 * Maximum message size that can be requested is MAX_MESSAGE_SIZE.
 * This function locks and keeps internal mutex in locked state. After each
 * hps_aio_write_begin() call you must also call hps_aio_write_end() to unlock the mutex.
 * @return 0 upon success, -1 otherwise.
 */
int hps_aio_write_begin(aioStreamT *s, void **buffer, unsigned size);

/*
 * End write, previously started with hps_aio_write_begin().
 * Application may end the write with less data, than was requested.
 * @return 0 upon success, -1 otherwise.
 */
int hps_aio_write_end(aioStreamT *s, unsigned size);

/*
 * Write IOV of multiple data blocks.
 * @return 0 upon success, -1 otherwise.
 */
int hps_aio_writev(aioStreamT *s, const struct iovec *iov, int count);

/*
 * Single write operation.
 * Here the size can be arbitrary large and is not limited to MAX_MESSAGE_SIZE.
 * @return 0 upon success, -1 otherwise.
 */
int hps_aio_write(aioStreamT *s, void *data, size_t size);

/*
 * Close AIO stream.
 * This function is non-thread safe. Application must ensure that
 * no other thread is using the stream prior to calling this function.
 * @return 0 upon success, -1 otherwise.
 */
int hps_aio_close(aioStreamT *s);

#ifdef __cplusplus
}
#endif

#endif /* AIOSTREAM_H_ */
