__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from kfp import dsl

from kubeflow.flows.common import get_mount_id, BaseFlow
from kubeflow.steps.cleanup import CleanupOp
from kubeflow.steps.cv1_exporter import Cv1ExporterOp
from model.paths import BlobStoragePath
from proxies.blob_storage import BlobStorageHandler


logger = logging.getLogger(__name__)


class DanfAzSnippetFlow(BaseFlow):

    NAME = "danf-az-snippet-flow"
    VERSION = "1.0"
    DESCRIPTION = "Exporting data from snippets"

    @classmethod
    def create(cls,
               bagfile: dict,
               flow_run_guid: str,
               input_location: BlobStoragePath,
               test_mode: bool):
        """Creates a new instance of this flow. The flow can run on a single
        bagfile or on a whole container.

        :param bagfile:
        :param flow_run_guid:
        :param input_location: path to either to bagfile or the container on
            which this flow should be run
        :param test_mode:
        :return:
        """

        # derive 'virtual' sub directory within output container from the
        # input container name. The agreed pattern for input containers is:
        # 'snip-yyyymmdd_hhmmss_<vin>'. The output sub dir should be
        # 'yyyymmdd_hhmmss_<vin>'.
        if input_location.container.startswith('snip-'):
            output_container_sub_path = input_location.container[5:]
        else:
            logger.warning(
                f"unexpected input container name. Expected "
                f"snip-yyyymmdd_hhmmss_<vin>, got {input_location.container}")
            output_container_sub_path = input_location.container

        input_container = BlobStoragePath(f"/{input_location.storage_account}/{input_location.container}")
        if input_location.is_container():
            # input location is container, run exporter on all files in input
            # container -> set input_files to None
            input_files = None
        elif input_location.is_file():
            # run exporter only on a specific file
            input_files = [input_location.file]
        else:
            raise ValueError(f"input_location {input_location} is neither "
                             f"container, nor a file")

        BlobStorageHandler()._ensure_container_exists(storage_account=input_location.storage_account, container_name="exp-perception-continuous-v1-001")
        output_container = BlobStoragePath(f"/{input_location.storage_account}/exp-perception-continuous-v1-001")

        return cls._build_pipeline(
            bagfile['guid'],
            flow_run_guid,
            input_container,
            input_files,
            output_container,
            output_container_sub_path,
            test_mode
        )

    @classmethod
    def _build_pipeline(cls,
                        parent_bagfile_guid,
                        flow_run_guid,
                        input_container: BlobStoragePath,
                        input_files,
                        output_container: BlobStoragePath,
                        output_mount_sub_path,
                        test_mode):
        @dsl.pipeline(
            name=cls.NAME,
            description=cls.DESCRIPTION
        )
        def processing_pipeline():
            # initialize params
            mount_id = get_mount_id()

            cleanup_step = CleanupOp.create_for_azure(
                tasks=['update-flow-run'],
                bagfile_guid=parent_bagfile_guid,
                flow_run_guid=flow_run_guid,
                bagfile_name='unknown',
                test_mode=test_mode
            )

            with dsl.ExitHandler(cleanup_step):
                cv1_exporter_step = Cv1ExporterOp.create(
                    bagfile_guid=parent_bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    mount_id=mount_id,
                    input_container=input_container,
                    input_files=input_files,
                    output_container=output_container,
                    output_mount_sub_path=output_mount_sub_path,
                    test_mode=test_mode
                )

        return DanfAzSnippetFlow(processing_pipeline)
