"""Search Engine and Metadata REST API drive route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from handlers.auth import auth
from handlers.drives import DriveHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


class DrivesRequestHandler(BaseRequestHandler):
    """Request handler for drive route: /drive"""

    _schema_post = BaseRequestHandler.load_json_schema(
        'drive_metadata_post.schema')

    drive_handler = DriveHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('drives/post',
                                      context_headers=self.request.headers)

        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_post)
        guid = self.drive_handler.add_drive_to_db(data)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results='Drive(s) succesfully added to db.',
            status_code=constants["REQ_STATUS_CODE_RESOURCE_CREATED"],
            location="/drive/" + guid)


class DriveRequestHandler(BaseRequestHandler):
    """Request handler for drive route: /drive/{guid}"""

    _schema_guid = BaseRequestHandler.load_json_schema(
        'guid.schema')
    _schema_patch = BaseRequestHandler.load_json_schema(
        'drive_metadata_patch.schema')

    drive_handler = DriveHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET, PATCH, DELETE')

    @auth.requires_entitlements(["openid"])
    def get(self, guid):
        tracing.start_task_in_context('drives/get',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}',
                           self._schema_guid)
        res = self.drive_handler.get_drive_from_db(
            guid)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def patch(self, guid):
        tracing.start_task_in_context('drives/patch', context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_patch)
        self.drive_handler.update_drive_in_db(guid, data)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results="Drive updated",
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def delete(self, guid):
        tracing.start_task_in_context('drives/delete', context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        cascade_to_list = self.get_query_arguments("cascade_to")
        hard_delete = self.get_query_argument("hard_delete", default="False") == 'True'
        res = self.drive_handler.delete_drive_from_db(guid, cascade_to_list, hard_delete)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
