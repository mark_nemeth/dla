"""Guest Logbook Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config.constants import *
from extractors.utils import charlist2str, rospytime2int, timestamp2ms, aracom_timestamp2ms
from extractors.base_interpreter import Interpreter

BVA_LOGBOOK = 10
SECONDS_BEFORE_LOGBOOK = 8
SECONDS_AFTER_LOGBOOK = 4


class GuestLogbookInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.bva = BVA_LOGBOOK

    def feed_new_msg(self, topic, msg, t):
        entry = {}
        entry["ts"] = aracom_timestamp2ms(msg.header.timestamp)
        entry["reporter"] = charlist2str(msg.reporter)
        entry["summary"] = charlist2str(msg.summary)
        entry["timestamp"] = charlist2str(msg.timestamp)
        entry["disengagement"] = msg.disengagement
        entry["car"] = charlist2str(msg.car)
        entry["car_vin"] = charlist2str(msg.car_vin)
        entry["event"] = charlist2str(msg.event)
        entry["payload"] = charlist2str(msg.payload)

        entry = self.calc_start_end_ts_snippet(entry)
        self.event_list += [entry]

    @staticmethod
    def calc_start_end_ts_snippet(event):
        event["start_ts"] = int(event["ts"] - (SECONDS_BEFORE_LOGBOOK * 1e3))
        event["end_ts"] = int(event["ts"] + (SECONDS_AFTER_LOGBOOK * 1e3))
        event["ts"] = int(event["ts"])
        return event
