"""Utils"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import re

logger = logging.getLogger(__name__)


def charlist2str(chrlist):
    """Converts list of integers into a string assuming the integers represent
    chars"""
    list_of_char = [chr(x) for x in chrlist]
    string = ''.join(list_of_char).rstrip('\0')
    return string


def rospytime2int(timestamp):
    """Converts rospy time to int (in ms)"""
    return int(str(timestamp)[0:13])


def timestamp2ms(timestamp):
    """Converts timestamp to milliseconds (int)"""
    return int(str(timestamp).replace(".", "")[0:13])


def timestamp2s(timestamp):
    """Converts timestamp to seconds (int)"""
    return int(str(timestamp).replace(".", "")[0:10])


def aracom_timestamp2ms(timestamp):
    """Converts aracom type header timestamp (with s and ns) to milliseconds (int)"""
    return timestamp2ms(int(timestamp.s*1e9 + timestamp.ns))


def ros_timestamp2ms(timestamp):
    """Converts ros type header timestamp (with s and ns) to milliseconds (int)"""
    return timestamp2ms(int(timestamp.secs*1e9 + timestamp.nsecs))


def set_origin(storage_account):
    """Set origin to US or DE based on storage Account region"""
    input_sa_pattern = re.compile(r"baseinfra(abt|syv).*")

    sa_match = input_sa_pattern.fullmatch(storage_account)
    
    if sa_match:
        sa_region = sa_match.group(1)  # eg: abt or syv
        if sa_region in ["syv"]:
            origin = "US"
        elif sa_region in ["abt"]:
            origin = "DE"
        else:
            origin = "DE"
    else:
        origin = "DE"
    logger.info(f"Setting the origin to {origin} as the storage_account is {storage_account}")
    return origin


def get_storage_account(path: str):
    path = path.strip('/')  # remove leading and trailing slashes
    segments = path.split('/')
    logger.info(f"The path {path} has these segments {segments}")
    if len(segments) == 0:
        raise ValueError(f"Path {path} does not match")

    if len(segments) > 3:
        raise ValueError(f"Path {path} does not match")

    storage_account = segments[0]
    return storage_account
