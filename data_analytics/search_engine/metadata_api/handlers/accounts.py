__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List

from handlers.db_data_handler import DB_Data_Handler
import hashlib
from DANFUtils.utils import utils as danfutils

from model.accounts import (Account, CreateAccountRequest, UpdateAccountRequest)


class AccountHandler:

    def __init__(self):
        self.db_handler = DB_Data_Handler()

    def get(self, guid: str) -> Account:
        return self.db_handler.get_account_by_guid(guid)

    def get_by_key(self, account_key: str) -> Account:
        key_hash = self._compute_key_hash(account_key)
        return self.db_handler.get_account_by_key_hash(key_hash)

    def get_all(self) -> List[Account]:
        return self.db_handler.get_all_accounts()

    def create(self, req: CreateAccountRequest) -> Account:
        account = Account(
            **req.dict(exclude={'key'}),
            guid=danfutils.generate_guid(),
            key_hash=self._compute_key_hash(req.key)
        )
        self.db_handler.add_account(account)
        return account

    def update(self, guid: str, req: UpdateAccountRequest) -> Account:
        updates = req.dict(exclude_unset=True, exclude={'account_key'})
        if req.key:
            key_hash = self._compute_key_hash(req.key)
            updates['key_hash'] = key_hash

        account = self.get(guid)
        new_account = Account.parse_obj({**account.dict(), **updates})

        self.db_handler.update_account(guid, updates)
        return new_account

    @staticmethod
    def _compute_key_hash(account_key: str) -> str:
        return hashlib.sha256(account_key.encode('utf-8')).hexdigest()
