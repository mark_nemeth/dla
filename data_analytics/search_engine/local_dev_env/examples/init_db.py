"""
This script highlights how you can use the DBConnector to add test data to a local mongo db instance
"""
import sys

from DBConnector import DBConnector
from simple_bagfile_provider import SimpleBagFileProvider
from advanced_bagfile_provider import AdvancedBagFileProvider

# DB Connection Settings
HOST = "localhost"
PORT = 27017
DB_NAME = "metadata"
USER = "user"
KEY = "password"
PROTOCOL = "mongodb"
SSL = False

# Test data
raw_data_flow = {
    "guid": "39a758db-7f24-4350-ae7a-d16259ecbe99",
    "name": "danf-integration-test-flow",
    "flow_version": "1",
    "kubeflow_experiment_id": "11111111-1111-1111-1111-111111111111",
    "steps": [{
        "name": "rca-extractor"
    }],
    'is_test_data': 1
}

raw_data_flow_run = {
    "version":
    "2.0",
    "guid":
    "39a758db-7f24-4350-ae7a-d16259ecbe88",
    "started_at":
    1577971500000,
    "status":
    "IN_PROGRESS",
    "reference": {
        "type": "bagfile_guid",
        "value": "11111111-1111-1111-1111-111111111111"
    },
    "flow_name":
    raw_data_flow["name"],
    "flow_version":
    raw_data_flow["flow_version"],
    "steps": [{
        "name": "rca-extractor",
        "image": "rca-extractorimg",
        "tag": "1.0",
        "is_essential": True,
        "max_retries": 3,
        "status": "PENDING"
    }],
    'is_test_data':
    1
}


def main(bag_file_provider):
    connector = DBConnector(HOST, PORT, DB_NAME, USER, KEY, PROTOCOL, SSL)
    with connector as con:
        con.db['bagfiles'].delete_many({'is_test_data': 1})
        con.db['child_bagfiles'].delete_many({'is_test_data': 1})
        con.db['flows'].delete_many({'is_test_data': 1})
        con.db['flow_runs'].delete_many({'is_test_data': 1})
        con.add_bagfiles(bag_file_provider.get_bagfiles())
        if bag_file_provider.get_child_bagfiles():
            con.add_child_bagfiles(bag_file_provider.get_child_bagfiles())
        con.add_flows([raw_data_flow])
        con.add_flow_runs([raw_data_flow_run])


def parse_settings(args):
    parsed_settings = dict()
    parsed_settings['include_child_bags'] = True

    for arg in args[1:]:
        key_val = arg.split('=')

        if key_val[0] == "bag_count":
            bag_count = int(key_val[1])
            if bag_count > 999:
                raise ValueError("max_bags must be > 0 and < 1000")
            parsed_settings['bag_count'] = bag_count
        else:
            if key_val[1].lower() == "true":
                parsed_settings[key_val[0]] = True
            elif key_val[1].lower() == "false":
                parsed_settings[key_val[0]] = False
            else:
                parsed_settings[key_val[0]] = key_val[1]

    return parsed_settings


if __name__ == '__main__':
    # take the SimpleBagFileProvider as default
    provider = SimpleBagFileProvider()

    if len(sys.argv) > 1:
        print("This script does not validate any params.")
        print("Valid params are e.g.: bag_count=50 include_child_bags=False")
        settings = parse_settings(sys.argv)

        if settings:
            # replace with AdvancedBagFileProvider
            provider = AdvancedBagFileProvider(settings)

    main(provider)