import numpy as np
import pandas as pd
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '../common'))
from disengagements import *


dis_ego_full = pd.read_pickle("./dis_ego_full.pkl")

relevant_topics = {
    'ego_state': '/CalService/instance_0/EgoVehStateInterface',
    'log_request': '/LoggingLogRequestService/instance_0/LogRequestInterface',
    'sem_state': '/SemSystemStatusService/instance_0/SemSystemStatusInterface'
}

filenames = (dis_ego_full.drop_duplicates('filename')['filename']).tolist()
cars = (dis_ego_full.drop_duplicates('filename')['car']).tolist()


df_ego_var_full = None
path_base = '/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/'
for idx, (fname, car) in enumerate(zip(filenames, cars)):
    path = os.path.join(path_base, car, fname)
    print idx, path

    ts = dis_ego_full[dis_ego_full['filename'] == fname]['ts']
    ts = [t.to_datetime64() for t in ts.tolist()]
    interval = np.timedelta64(3, 's')

    try:
        bag = Bagfile(path)
        df_ego_crop = bag.crop_topic(
            relevant_topics['ego_state'], ts, interval, 'after')
        df_ego_crop['filename'] = fname
        df_ego_crop['car'] = car
        if df_ego_var_full is None:
            df_ego_var_full = df_ego_crop
        else:
            df_ego_var_full = df_ego_var_full.append(
                df_ego_crop, ignore_index=True)
            df_ego_var_full.to_pickle("./df_ego_var_full_checkpoint.pkl")
    except Exception as inst:
        print(inst)
