__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from datetime import datetime

from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import (ENV_SPAN_ID, ENV_TRACE_ID,
                                       ENV_TRACE_OPTIONS)
from kubernetes.client import V1VolumeMount
from kubernetes.client.models import V1EnvVar

from config.config_handler import config_dict
from kubeflow.steps.common import (BaseStep, extract_tag_from_docker_img,
                                   RETRY_NUM, PIPELINE_POD_LABEL, sanitize_label,
                                   BAGFILE_POD_LABEL, TESTING_POD_LABEL, DANF_ENV,
                                   GPU_RESOURCE_TITLE, VM_LABEL_ATTRIBUTE,
                                   GPU_VM_TITLE, attach_blobfuse_volume,
                                   get_host_path_volume)
from kubeflow.steps.tracing_sidecar import TracingSidecar
from model.paths import BlobStoragePath


class Cv1ExporterOp(BaseStep):
    NAME = "cv1-exporter"
    DOCKER_IMG = config_dict.get_value(NAME)
    CUTOFF_DATE = datetime.strptime(config_dict.get_value("exporter_cutoff_date", "3000-01-01"), '%Y-%m-%d')
    CUTOFF_DATE_TS = int(datetime.timestamp(CUTOFF_DATE)) * 1000  # unix timestamp in ms
    INPUT_BASE_PATH = "/input"
    OUTPUT_BASE_PATH = "/output"
    EXPORT_TMP_DATA_PATH = "/mnt/stereo"

    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               mount_id,
               input_container: BlobStoragePath,
               input_files,
               output_container: BlobStoragePath,
               output_mount_sub_path,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process (currently unclear if
            this should be the parent bagfile guid or the child bagfile guid
            if step is executed on a single snippet)
        :param flow_run_guid: id of the flow run in which this step is executed
        :param mount_id: used to generate unique mount names
        :param input_container: location of blob storage container where
            the childbagfiles are located
        :param input_files: list of bagfile names located in the
            input_container. The exporter will be configured to run on only
            those files. If set to None to run exporter on all files in the
            input container the file within the container on
            which the exporter should be executed. Either input_files or
            input_folder should be used (other can be None)
        :param output_container: location of the blob storage container where
            exported data should be written to
        :param output_mount_sub_path: virtual sub path to mount within
            output_container
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the CV1 exporter operation
        """
        # TODO: Add bagfile pod label (of parent or child bag?)

        version = extract_tag_from_docker_img(cls.DOCKER_IMG)
        bagfile_name = ','.join(input_files) if input_files else 'all'
        step = Cv1ExporterOp(vin='undefined',
                             bagfile_guid=bagfile_guid,
                             bagfile_name=bagfile_name,
                             flow_run_guid=flow_run_guid) \
            .set_image_pull_policy('Always')

        step.enable_tracing_integration() \
            .add_input_and_output_labels(input_container.path, output_container.path)

        if test_mode:
            step.add_pod_label(name=TESTING_POD_LABEL, value='true')

        step.add_env_variable(V1EnvVar(name="OUTPUT_CONTAINER", value=output_container.container)) \
            .add_env_variable(V1EnvVar(name="BAGFILE_GUID", value=bagfile_guid)) \
            .add_env_variable(V1EnvVar(name="FLOW_RUN_ID", value=flow_run_guid)) \
            .add_env_variable(V1EnvVar(name="TMP_DIR", value=cls.EXPORT_TMP_DATA_PATH))

        if input_files:
            input_files_str = ','.join(input_files)
            step.add_env_variable(V1EnvVar(name="INPUT_LIST", value=input_files_str))

        step.add_resource_request(GPU_RESOURCE_TITLE, "1") \
            .add_resource_limit(GPU_RESOURCE_TITLE, "1") \
            .add_node_selector_constraint(VM_LABEL_ATTRIBUTE, GPU_VM_TITLE)

        attach_blobfuse_volume(step,
                               cls.INPUT_BASE_PATH,
                               f"exporting-mount-input-{mount_id}",
                               input_container.storage_account,
                               input_container.container)
        attach_blobfuse_volume(step,
                               cls.OUTPUT_BASE_PATH,
                               f"exporting-mount-output-{mount_id}",
                               output_container.storage_account,
                               output_container.container,
                               False,
                               0,
                               mount_sub_path=output_mount_sub_path)
        step.add_volume(get_host_path_volume(volume_name=f"tmp-mount-cv1-exp-{mount_id}", host_path=f"/mnt/stereo/{mount_id}")) \
            .add_volume_mount(V1VolumeMount(mount_path=cls.EXPORT_TMP_DATA_PATH, name=f"tmp-mount-cv1-exp-{mount_id}"))

        return step
