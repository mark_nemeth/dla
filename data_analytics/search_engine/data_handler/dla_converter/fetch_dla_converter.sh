#!/bin/bash
echo "Before you use this script, check that you've access rights, to fetch from the ATTDATADR_general folder/repo (artifactory)."
echo -n "Enter the path of your *.pem certificate [ENTER]: "
read cert
echo -n "Enter your username for the artifactory [ENTER]: "
read user
echo -n "Enter your password for the artifactory [ENTER]: "; stty -echo; read pw; stty echo; echo

wget --certificate $cert \
    --user=$user --password=$pw \
    -d https://athena.daimler.com/artifactory/ATTDATADR_general/dla_converter/dla_converter