#ifndef HPS_H_
#define HPS_H_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DEFAULT_TCP_PORT	6000
#define DEFAULT_UDP_PORT	6001
#define DEFAULT_RDMA_PORT	6002

#define DEFAULT_RECORD_URI	"NULL"
#define DEFAULT_SERVER_URI	"TCP:*:6000"
#define DEFAULT_CLIENT_URI	"TCP:192.168.77.100:6000"

#define MTU_SIZE			(9000)
#define MAX_UDP_SIZE		(MTU_SIZE - 28)
//#define MAX_UDP_SIZE		(65535 - (14 + 20 + 8))

#define MIN(a, b)				((a) < (b) ? (a) : (b))
#define MAX(a, b)				((a) > (b) ? (a) : (b))

typedef enum
{
	HPS_NULL,
	HPS_TCP,
	HPS_UDP,
	HPS_RAW,
	HPS_RDMA,
	HPS_AIO
} hps_type;

struct hpStreamT
{
	hps_type type;
};

struct hpServerT
{
	hps_type type;
};

typedef struct
{
	long long t;
	long long writeBytes;
	long long txBytes;
	long long rxBytes;
	long long rxMessages;
	long long rxLostMessages;
} hps_statistics;

extern hps_statistics statistics;

struct hpNullStreamT;

hpNullStreamT *hps_null_open();
int hps_null_write_begin(hpNullStreamT *s, void **buffer, unsigned size);
int hps_null_write_end(hpNullStreamT *s, unsigned size);
int hps_null_writev(hpNullStreamT *s, const struct iovec *iov, int count);
int hps_null_close(hpNullStreamT *s);

struct hpTcpStreamT;

hpTcpStreamT *hps_tcp_open(const char *ip, int port);
int hps_tcp_writev(hpTcpStreamT *s, const struct iovec *iov, int count);
int hps_tcp_close(hpTcpStreamT *s);

struct hpUdpStreamT;
hpUdpStreamT *hps_udp_open(const char *ip, int port);
int hps_udp_writev(hpUdpStreamT *s, const struct iovec *iov, int count);
int hps_udp_close(hpUdpStreamT *s);

struct hpRdmaStreamT;
hpRdmaStreamT *hps_rdma_open(const char *ip, int port);
int hps_rdma_write_begin(hpRdmaStreamT *s, void **buffer, unsigned size);
int hps_rdma_write_end(hpRdmaStreamT *s, unsigned size);
int hps_rdma_writev(hpRdmaStreamT *s, const struct iovec *iov, int count);
int hps_rdma_close(hpRdmaStreamT *s);

struct hpStreamT;

hpStreamT *hps_open(const char *uri);
int hps_write_begin(hpStreamT *s, void **buffer, unsigned size);
int hps_write_end(hpStreamT *s, unsigned size);
int hps_writev(hpStreamT *s, const struct iovec *iov, int count);
int hps_write(hpStreamT *s, void *data, unsigned size);
int hps_close(hpStreamT *s);

// ============================================================================

struct hpTcpServerT;

hpTcpServerT* hps_tcp_start_server(const char *name, int port, const char *out);
int hps_tcp_close_server(hpTcpServerT *server);

struct hpUdpServerT;

hpUdpServerT* hps_udp_start_server(const char *name, int port, const char *out);
int hps_udp_close_server(hpUdpServerT *server);

struct hpWriteServerT;

hpWriteServerT* hps_write_start_server(const char *file);
int hps_write_close_server(hpWriteServerT *server);

struct hpRawServerT;

hpRawServerT* hps_raw_start_server(const char *dev, const char *out);
int hps_raw_close_server(hpRawServerT *server);

struct hpRdmaServerT;

hpRdmaServerT* hps_rdma_start_server(const char *ip, int port, const char *out);
int hps_rdma_close_server(hpRdmaServerT *server);

struct hpServerT;

hpServerT* hps_start_server(const char *uri, const char *out);
int hps_close_server(hpServerT *server);

void hps_get_statistics(hps_statistics *stat);

#ifdef __cplusplus
}
#endif

#endif /* HPS_H_ */
