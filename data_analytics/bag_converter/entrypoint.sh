#!/bin/bash

set -e
echo "======================"
echo "Bag Converter API"
echo "======================"

arguments="$@"
echo $arguments
exec uvicorn API.main:converter --reload --host 0.0.0.0 --port 8891 $arguments
#--reload --host 0.0.0.0 --port 80 --ssl-keyfile .certs/image_processing-key.pem  --ssl-certfile  .certs/image_processing-cert.pem