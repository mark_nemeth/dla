"""Unittest for utils"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import unittest

from handlers.utils import Utils

ERROR_MESSAGE_KEY_WORD = "error_message"
DEFAULT_ERROR = "Server Error!"


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.msg = "This is an test error message!"
        self.ut = Utils()

    def test_build_error_from_message(self):
        #Test message building with string
        self.assertEqual({'error_message': 'This is an test error message!'},
                         self.ut.build_error_from_message(self.msg))
        #Test message building with empty string
        self.assertEqual({'error_message': ''},
                         self.ut.build_error_from_message(""))

    def test_verify_values_in_dict(self):
        #TODO Insert some usefull test here
        return

    def test_timestamp_to_sequence_index(self):
        #test timestamp2seq_index
        self.assertEqual(
            self.ut.timestamp_to_sequence_index(1558962372000, 1558962370000),
            2)

    def test_sequence_index_to_timestamp(self):
        #test conversion
        self.assertEqual(self.ut.sequence_index_to_timestamp(2, 1558962370000),
                         1558962372000)

    def test_build_response(self):
        #TODO Add check for >> self.ut.build_response(...) <<
        return


if __name__ == '__main__':

    unittest.main()
