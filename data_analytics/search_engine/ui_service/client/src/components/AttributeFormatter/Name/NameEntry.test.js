import React from 'react';
import {shallow} from 'enzyme';
import {FileTypes} from '../../Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import NameEntry from './NameEntry';

describe('Bagfile Table NameEntry', () => {

  it('should format name correct and create link', () => {
    const props = {
      guid: '1337',
      fileName: 'test_bagfile_with_long_name_123423324234234254312312.bag',
      fileType: FileTypes.ORIGINALFILE
    };
    const wrapper = shallow(<NameEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('Link').exists()).toBe(true);
  });

  it('should return default value when file is not correct', () => {
    const props = {
      guid: null,
      fileName: null,
      fileType: FileTypes.ORIGINALFILE
    };
    const wrapper = shallow(<NameEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('Link').exists()).toBe(false);
  });
});