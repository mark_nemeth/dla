import express from 'express';
import secured from './secure/secureApi';
import SearchApi from '../api/searchApi';
import DataHandlerApi from '../api/dataHandlerApi';
import userHandler from './handler/userHandler';
import {isUserPermitted} from './secure/permissonHelper';
import {isObjectValid} from '../utils/helperFunctions';
import {localDevGetSingleFileIfNecessary} from '../utils/localDevHelper';
import FileCatalogApi from '../api/fileCatalogApi';
import DataTransferApi from '../api/dataTransferApi'
import configHandlerSingleton from '../configHandler';
import getLogger from '../utils/logging';

const logger = getLogger(__filename)

const router = express.Router();

router.get('/bagfiles', secured(), async function (req, res) {
  let response;
  try {
    response = await SearchApi.getBagfilesFromAPI(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let searchResults = {
    bagfiles: response.results,
    total: response.total
  }

  if (isGetByIdRequest(req)) {
    searchResults = localDevGetSingleFileIfNecessary(req, 'bagfiles', searchResults);
  }

  res.status(200).send({ results: searchResults });
});

router.get('/files/search', async function (req, res) {
  let bagPath = req.query.bagPath;

  if (!bagPath) {
    return res.status(404).send();
  }

  let file = null;

  try {
    file = await FileCatalogApi.getFileByUrl(bagPath);
  } catch (error) {
    logger.error(error);
    if (error.statusCode === 404) {
      return res.status(404).send();
    }
    return returnDefaultError(res, error.toString());
  }

  if (!file) {
    return res.status(404).send();
  }

  let transfers = null;
  try {
    transfers = await DataTransferApi.getTransfersByDataCatalogFileId(file.fileGuid);

    file.locations.forEach(location => {
      location.transfer = null;
      transfers.forEach(transfer => {

        if (transfer.status === "COMPLETED") {
          return;
        }

        if (location.cluster.id === transfer.targetCluster &&
          (location.transfer === null || location.transfer.created < transfer.created)) {
          location.transfer = transfer;
        }
      });
    });
    res.status(200).send(file);
  } catch (error) {
    try {
      await DataTransferApi.getApiHealth().then(apiHealth => {
        if (apiHealth === "UP") {
          return returnDefaultError(res, error.toString());
        } else {
          return returnApiHealthError(res, error.toString(), apiHealth)
        }
      });
    } catch (error) {
      return returnApiHealthError(res, error.toString(), "Unreachable")
    }
  }
});

router.post('/transfers', async function (req, res) {

  const fileGuid = req.query.fileGuid;
  const targetCluster = req.query.targetCluster;

  if (!fileGuid || !targetCluster) {
    return res.status(400).send();
  }

  let userInfo;
  try {
    userInfo = await userHandler.getUserInformation(req);
  } catch (error) {
    logger.error(`Failed to load user information.${error.toString()}`);
    return returnDefaultError(res, error.toString())
  }

  try {
    const createdTransfer = await DataTransferApi.createTransfer(fileGuid, targetCluster, userInfo.name);
    res.status(200).send(createdTransfer);
  } catch (error) {
    await DataTransferApi.getApiHealth().then(apiHealth => {
      if (apiHealth === "UP") {
        return returnDefaultError(res, error.toString());
      } else {
        return returnApiHealthError(res, error.toString(), apiHealth)
      }
    }).catch(error => {
      return returnApiHealthError(res, error.toString(), "Unreachable")
    });
  }
});

router.get('/ingests/latest', secured(), async function (req, res) {
  let response;
  try {
    response = await DataTransferApi.getLatestIngestPerCarFromApi(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let searchResults = response;

  res.status(200).send(searchResults);
});

router.get('/ingests', secured(), async function (req, res) {
  let response;
  try {
    response = await DataTransferApi.getIngestsFromApi(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let searchResults = response;

  res.status(200).send(searchResults);
});

router.get('/reporting/trips/kpi', secured(), async function (req, res) {

  let response;
  try {
    response = await SearchApi.getReportingKPIsFromAPI(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let kpiResults = {
    kpis: response.results.kpis
  }
  res.status(200).send({ results: kpiResults });
});

router.get('/reporting/trips/tables/simple', secured(), async function (req, res) {

  let response;
  try {
    response = await SearchApi.getReportingTablesSimpleFromAPI(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let reportingResults = {
    tables: response.results.tables
  }
  res.status(200).send({ results: reportingResults });
});

router.get('/reporting/trips/tables/agg', secured(), async function (req, res) {

  let response;
  try {
    response = await SearchApi.getReportingTablesAggregationFromAPI(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let reportingResults = {
    tables: response.results.tables
  }
  res.status(200).send({ results: reportingResults });
});

router.get('/childbagfiles', secured(), async function (req, res) {


  let response;
  try {
    response = await SearchApi.getChildBagfilesFromAPI(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let searchResults = {
    childBagfiles: response.results,
    total: response.total
  }

  if (isGetByIdRequest(req)) {
    searchResults = localDevGetSingleFileIfNecessary(req, 'childBagfiles', searchResults);
  }

  res.status(200).send({results: searchResults});
});

router.get('/filter/features', secured(), async function (req, res) {


  let response;
  try {
    response = await SearchApi.getFilterFeaturesFromAPI(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let searchResults = {
    features: response.results,
    total: response.total
  }

  if (isGetByIdRequest(req)) {
    searchResults = localDevGetSingleFileIfNecessary(req, '/filter/features', searchResults);
  }

  res.status(200).send({results: searchResults});
});


router.get('/drives', secured(), async function (req, res) {


  let response;
  try {
    response = await SearchApi.getDrivesFromAPI(req);
  } catch (error) {
    return returnDefaultError(res, error.toString());
  }

  let searchResults = {
    drives: response.results,
    total: response.total
  }

  if (isGetByIdRequest(req)) {
    searchResults = localDevGetSingleFileIfNecessary(req, 'drives', searchResults);
  }

  res.status(200).send({results: searchResults});
});

router.get('/bagfile_filters', secured(), function (req, res) {
  SearchApi.getFiltersFromAPI(req)
    .then(response => {
      if (isObjectValid(response.results)) {
        res.status(200).send({
            results: response.results
          })
        } else returnDefaultError(res, 'Invalid Filter response from search API')
      })
      .catch(error => returnDefaultError(res, error))
});

router.get('/originalfile_filters', secured(), function (req, res) {
  SearchApi.getFiltersFromAPI(req)
    .then(response => {
      if (isObjectValid(response.results)) {
        res.status(200).send({
          results: response.results
        })
      } else returnDefaultError(res, 'Invalid Filter response from search API')
    })
    .catch(error => returnDefaultError(res, error))
});

router.get('/alwaysonfile_filters', secured(), function (req, res) {
  SearchApi.getFiltersFromAPI(req)
      .then(response => {
        if (isObjectValid(response.results)) {
          res.status(200).send({
            results: response.results
          })
        } else returnDefaultError(res, 'Invalid Filter response from search API')
      })
      .catch(error => returnDefaultError(res, error))
});

router.get('/childbagfile_filters', secured(), function (req, res) {
  SearchApi.getChildBagfileFiltersFromAPI(req)
    .then(response => {
      if (isObjectValid(response.results)) {
        res.status(200).send({
          results: response.results
        })
      } else returnDefaultError(res, 'Invalid Childbagfile Filter response from search API')
    })
    .catch(error => returnDefaultError(res, error))
});

router.get('/drive_filters', secured(), function (req, res) {
  SearchApi.getDriveFiltersFromAPI(req)
      .then(response => {
        if (isObjectValid(response.results)) {
          res.status(200).send({
            results: response.results
          })
        } else returnDefaultError(res, 'Invalid Drive Filter response from search API')
      })
      .catch(error => returnDefaultError(res, error))
});

router.get('/user', secured(), function (req, res) {

  userHandler.getUserInformation(req)
    .then((userInfo) => {
      if (isObjectValid(userInfo)) {
        logger.debug(`Return user information to client: name: ${userInfo.name} isAdmin: ${userInfo.admin}`);
        res.status(200).send(userInfo);
      } else {
        returnDefaultError(res, 'Something went wrong');
      }
    })
    .catch(error => {
      logger.error(error);
      returnDefaultError(res, error);
    })

});

router.post('/reprocessfiles', secured(), function (req, res) {
  isUserPermitted(req.user).then(permitted => {
    if (!permitted) return returnForbiddenError(res, 'User not permitted to do this action');
    DataHandlerApi.triggerReprocessFromAPI(req)
      .then(response => {
        if (response) {
          res.status(200).send({
            results: response
          })
        } else returnDefaultError(res, 'Invalid response for reprocessing from search API')
      })
      .catch(error => returnDefaultError(res, error))
  });
});

router.get('/flowrun', secured(), function (req, res) {
  SearchApi.getFlowRunsFromAPI(req)
    .then(response => {
      if (isObjectValid(response.results)) {
        res.status(200).send({
          results: response.results
        })
      } else returnDefaultError(res, 'Invalid Filter response from search API')
    })
    .catch(error => returnDefaultError(res, error))
});

router.get('/versions', secured(), function (req, res) {
  let versionInformation = configHandlerSingleton.getVersionInformation();
  res.status(200).send(versionInformation);
})

router.get('/feature_toggles', secured(), function (req, res) {
  let featureToggles = configHandlerSingleton.getFeatureToggles();
  res.status(200).send(featureToggles);
})

function returnForbiddenError(res, error) {
  logger.error(error);
  res.status(403).send({
    data: error
  })
}

function returnDefaultError(res, error) {
  logger.error(error);
  res.status(503).send({
    data: error
  })
}

function returnApiHealthError(res, error, status) {
  let errorMessage = "Data Transfer API temporarily unavailable. Status: " + status + "\n";
  logger.error(errorMessage);
  logger.debug(error);
  res.status(503).send({
    type: "apiHealthError",
    message: errorMessage
  })
}

function isGetByIdRequest(req) {
  return req.query && req.query.hasOwnProperty('guid') && !req.query.hasOwnProperty('parent_guid');
}

export default router;