/********************************************************************
 * COPYRIGHT: (c) Daimler AG and Robert Bosch GmbH
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages and can be prosecuted. All rights reserved
 * particularly in the event of the grant of a patent, utility model
 * or design.
 *
 ********************************************************************/

#include <vector>
#include <algorithm>
#include <mutex>
#include <cassert>

#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <libaio.h>
#include "common.h"
#include "aiostream.h"
#include "hps.h"

using namespace std;

#define KB						(1024)
#define MB						(1024 * 1024)
#define GB						(1024llu * 1024llu * 1024llu)
#define CACHE_LINE_SIZE			(64)

// Configuration
#define BLOCK_SIZE				(8 * MB)
#define BUFFER_SIZE				(1 * GB)
#define MAX_QUEUE_DEPTH			(128)
#define FILE_TRUNCATE_TRESHOLD	(BUFFER_SIZE)
#define FILE_TRUNCATE_SIZE		(BUFFER_SIZE)
#define AIO_MAX_EVENTS			(MAX_QUEUE_DEPTH)

#define CTX_COUNT				(BUFFER_SIZE / BLOCK_SIZE)

static void *streamHandler(void *arg);

// I/O Dispatcher context
struct IODispatcher
{
	volatile bool shutdown;
	bool threadStarted;
	pthread_t thread;
	vector<aioStreamT*> streams;
	mutex streamsLock; // protects streams

	void Add(aioStreamT *s)
	{
		lock_guard<mutex> lock(streamsLock);

		streams.push_back(s);

		// Start I/O handler thread
		if (!threadStarted)
		{
			if (pthread_create(&thread, NULL, streamHandler, NULL) != 0)
			{
				perror("Failed to start I/O dispatcher thread");
				throw exception();
			}
			threadStarted = true;
		}
	}

	void Remove(aioStreamT *s)
	{
		lock_guard<mutex> lock(streamsLock);

		streams.erase(remove(streams.begin(), streams.end(), s), streams.end());

		// Shutdown dispatcher thread if all streams are closed
		if (streams.size() == 0 && threadStarted)
		{
			// Shutdown handler thread
			shutdown = true;
			void *rc;
			pthread_join(thread, &rc);
			threadStarted = false;
			shutdown = false;
		}
	}
};

static IODispatcher dp;

typedef enum
{
	AIOS_UNUSED = 0,
	AIOS_SHUTDOWN_REQUEST
} aioStreamStateT;

static volatile int streamCount = 0;

struct aioStreamT
{
	hps_type type;
	// File
	int fd;
	bool fdOwner;
	int sh;
	long long fileSize;
	// Buffer
	uint8_t *buffer;
	uint8_t *mirror;
	// AIO contexts
	bool aioCtxInitialized;
	io_context_t ctx;
	struct iocb iocb[CTX_COUNT];
	// Inter-thread variables
	volatile aioStreamStateT state;
	pthread_mutex_t syncMutex;
	pthread_cond_t condvar;
	int queueDepth;
	// Read offset (incremented in BLOCK_SIZE granularity)
	long long r;
	// Complete offset (incremented in BLOCK_SIZE granularity)
	volatile long long __attribute__((aligned(CACHE_LINE_SIZE))) c;
	// Write offset
	volatile long long __attribute__((aligned(CACHE_LINE_SIZE))) w;

	aioStreamT()
	{
		memset(this, 0, sizeof(aioStreamT));
		fd = -1;
		sh = -1;

		streamCount++;
	}

	~aioStreamT()
	{
		int ec;

		// Request stream shutdown
		pthread_mutex_lock(&syncMutex);
		state = AIOS_SHUTDOWN_REQUEST;
		// Wait until all data is written
		while (c < w)
			pthread_cond_wait(&condvar, &syncMutex);
		pthread_mutex_unlock(&syncMutex);

		// Unregister stream from I/O dispatcher
		dp.Remove(this);

		if (aioCtxInitialized)
			io_destroy(ctx);

		if (fd != -1)
		{
			// Truncate file
			if (ftruncate(fd, w) != 0)
				ec = -1;
			// Close file if we are the owner
			if (fdOwner)
				close(fd);
		}

		if (mirror != NULL && mirror != MAP_FAILED)
			munmap(mirror, BUFFER_SIZE);
		if (buffer != NULL && buffer != MAP_FAILED)
			munmap(buffer, BUFFER_SIZE);

		if (sh != -1)
			close(sh);

		pthread_cond_destroy(&condvar);
		pthread_mutex_destroy(&syncMutex);

		memset(this, 0, sizeof(aioStreamT));

		streamCount--;
	}
};

aioStreamT *hps_aio_open(const char *uri, int fd)
{
	int e;
	aioStreamT *s = new aioStreamT();
	s->type = HPS_AIO;

    char *fileName = getFileName(uri);
	remove(fileName);

	if (fd == -1)
	{
		// Open file
		s->fd = open(fileName, O_CREAT | O_RDWR | O_NOATIME | O_EXCL | O_TRUNC | O_DIRECT, S_IRUSR | S_IWUSR);
		if (s->fd < 0)
		{
			hps_aio_close(s);
			return NULL;
		}
		s->fdOwner = true;
	}
	else
		s->fd = fd;

	e = posix_fadvise(s->fd, 0, 0, POSIX_FADV_DONTNEED);
	if (e != 0)
		printf("Warning: posix_fadvise failed to set POSIX_FADV_DONTNEED\n");
	e = posix_fadvise(s->fd, 0, 0, POSIX_FADV_SEQUENTIAL);
	if (e != 0)
		printf("Warning: posix_fadvise failed to set POSIX_FADV_DONTNEED\n");

	// Map buffer
	{
		s->sh = shm_open("/aio_internal_shm", O_CREAT | O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
		if (s->sh == -1)
		{
			hps_aio_close(s);
			return NULL;
		}

		if (shm_unlink("/aio_internal_shm") != 0)
		{
			hps_aio_close(s);
			return NULL;
		}

		if (ftruncate(s->sh, 2 * BUFFER_SIZE) == -1)
		{
			hps_aio_close(s);
			return NULL;
		}

		// Map buffer memory
		s->buffer = (uint8_t*)mmap64(NULL, 2 * BUFFER_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, s->sh, 0);
		if (s->buffer == MAP_FAILED)
		{
			hps_aio_close(s);
			return NULL;
		}
		// Lock buffer memory
		if (mlock(s->buffer, BUFFER_SIZE) != 0)
		{
			perror("Warning: failed to lock memory");
		}
		// Map mirror
		s->mirror = (uint8_t*)mmap64(s->buffer + BUFFER_SIZE, BUFFER_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, s->sh, 0);
		if (s->mirror != s->buffer + BUFFER_SIZE)
		{
			hps_aio_close(s);
			return NULL;
		}
	}

	// Create AIO context
	if (io_setup(AIO_MAX_EVENTS, &s->ctx) != 0)
	{
		hps_aio_close(s);
		return NULL;
	}
	s->aioCtxInitialized = true;

	// Prepare IOCBs
	for (long i = 0; i < CTX_COUNT; i++)
	{
		void *block = (void*)((uintptr_t)s->buffer + i * BLOCK_SIZE);
		io_prep_pwrite(&s->iocb[i], s->fd, block, BLOCK_SIZE, 0);
		s->iocb[i].data = &s->iocb[i];
		s->iocb[i].u.c.offset = 0xffffffffffffffff;
	}

	pthread_cond_init(&s->condvar, NULL);
	pthread_mutex_init(&s->syncMutex, NULL);

	// Register stream in the dispatcher
	dp.Add(s);

	return s;
}

/**
 * I/O dispatcher function for writing data from the internal buffer to file.
 * The dispatcher is executed on its own thread.
 */
static void *streamHandler(void *arg)
{
	struct timespec timeout = { .tv_sec = 0, .tv_nsec = 0 };
	struct io_event events[AIO_MAX_EVENTS];
	iocb *iocbs[MAX_QUEUE_DEPTH];

	// The I/O handler shall not process SIGTERM signals.
	sigset_t mask;
	sigemptyset(&mask);
	sigaddset(&mask, SIGTERM);
	sigprocmask(SIG_BLOCK, &mask, NULL);

//	// Set core affinity
//    cpu_set_t cpuset;
//    CPU_ZERO(&cpuset);
//    int cores = sysconf(_SC_NPROCESSORS_ONLN);
//    for (int core = max(0, cores - 2); core < cores; core++)
//    	CPU_SET(core, &cpuset);
//    pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);

    while (!dp.shutdown)
    {
    	dp.streamsLock.lock();

    	if (dp.streams.size() == 0)
    	{
    		dp.streamsLock.unlock();
    		usleep(10000);
    		continue;
    	}

    	for(auto const& s: dp.streams)
		{
    		long long w = s->w;

			// Advance file size
			if ((s->state != AIOS_SHUTDOWN_REQUEST) && (s->fileSize <= w + FILE_TRUNCATE_TRESHOLD))
			{
				if (fallocate64(s->fd, 0, s->fileSize, FILE_TRUNCATE_SIZE) == 0)
					s->fileSize += FILE_TRUNCATE_SIZE;
				else
				{
					s->state = AIOS_SHUTDOWN_REQUEST;
					perror("ftruncate() failed");
				}
			}

			// Process completion events
			if (0 < s->queueDepth)
			{
				int n = io_getevents(s->ctx, 1, AIO_MAX_EVENTS, events, &timeout);
				if (0 < n)
				{
					for (int i = 0; i < n; i++)
					{
						assert(events[i].res == BLOCK_SIZE);
						assert(events[i].res2 == 0);
						// Clear offset to indicate that this block is available again
						((struct iocb*)events[i].data)->u.c.offset = 0xffffffffffffffff;
					}
					s->queueDepth -= n;

					// Iterate completed blocks and advance completed offset
					long long c = s->c;
					while (c < s->r)
					{
						int idx = (c % BUFFER_SIZE) / BLOCK_SIZE;
						if (s->iocb[idx].u.c.offset != 0xffffffffffffffff)
							break;
						c += BLOCK_SIZE;
					}
					if (s->c < c)
					{
						s->c = c;

						// Signal that new space is available
						pthread_mutex_lock(&s->syncMutex);
						pthread_cond_signal(&s->condvar);
						pthread_mutex_unlock(&s->syncMutex);
					}
				}
				assert(0 <= n);
			}

			// Enqueue new blocks writes
			int writeBlockCount = 0;
			long long r = s->r;
			long long rMax = w & ~(BLOCK_SIZE - 1);
			if (s->state == AIOS_SHUTDOWN_REQUEST)
				rMax = w;
			while (r < rMax && s->queueDepth + writeBlockCount < MAX(4, (MAX_QUEUE_DEPTH / MAX(1, streamCount))))
			{
				// Prepare write
				int idx = (r % BUFFER_SIZE) / BLOCK_SIZE;
				s->iocb[idx].u.c.offset = r;
				iocbs[writeBlockCount] = &s->iocb[idx];
				r += BLOCK_SIZE;
				writeBlockCount++;
			}
			// Submit block writes
			if (0 < writeBlockCount)
			{
				int n = io_submit(s->ctx, writeBlockCount, iocbs);
				if (0 < n)
				{
					s->r += n * BLOCK_SIZE;
					s->queueDepth += n;
				}
				else if (n < 0)
				{
					if (n == -(EAGAIN))
					{
						// Insufficient resources are available to queue any iocbs
					}
					else
					{
						printf("io_submit() failed (%d)\n", n);
					}
				}
			}
		}

    	dp.streamsLock.unlock();

		// Be nice
    	usleep(1000);
		//pthread_yield();
	}

	return NULL;
}

int hps_aio_write_begin(aioStreamT *s, void **buffer, unsigned size)
{
	if (s == NULL || buffer == NULL || BUFFER_SIZE < size)
		return -1;

	if (s->state == AIOS_SHUTDOWN_REQUEST)
		return -1;

	long long space;
	// Wait for free space
	pthread_mutex_lock(&s->syncMutex);
	while (1)
	{
		// Check free space
		space = s->c + BUFFER_SIZE - s->w;
		if (size <= space)
			break;

		pthread_cond_wait(&s->condvar, &s->syncMutex);
	}
	pthread_mutex_unlock(&s->syncMutex);

	*buffer = (void*)((uintptr_t) s->buffer + (s->w % BUFFER_SIZE));

	return size ? size : space;
}

int hps_aio_write_end(aioStreamT *s, unsigned size)
{
	if (s == NULL)
		return -1;

	if (s->state == AIOS_SHUTDOWN_REQUEST)
		return -1;

	s->w += size;

	__sync_add_and_fetch(&statistics.writeBytes, size);

	return 0;
}

int hps_aio_writev(aioStreamT *s, const struct iovec *iov, int count)
{
	if (s == NULL || iov == NULL)
		return -1;

	if (s->state == AIOS_SHUTDOWN_REQUEST)
		return -1;

	for (int i = 0; i < count; i++)
	{
		uint8_t *src = (uint8_t*)iov[i].iov_base;
		size_t rem = iov[i].iov_len;
		while (0 < rem)
		{
			// Compute free space
			size_t space = s->c + BUFFER_SIZE - s->w;
			size_t n = MIN(rem, space);

			if (0 < n)
			{
				// Copy chunk of data
				memcpy((void*)((uintptr_t)s->buffer + (s->w % BUFFER_SIZE)), src, n);
				s->w += n;
				rem -= n;
				src += n;
			}
			else
			{
				// Wait for free space
				pthread_mutex_lock(&s->syncMutex);
				pthread_cond_wait(&s->condvar, &s->syncMutex);
				pthread_mutex_unlock(&s->syncMutex);
			}
		}
	}

	return 0;
}

int hps_aio_write(aioStreamT *s, void *data, size_t size)
{
	if (s == NULL || data == NULL)
		return -1;

	if (s->state == AIOS_SHUTDOWN_REQUEST)
		return -1;

	uint8_t *src = (uint8_t*)data;
	size_t rem = size;
	while (0 < rem)
	{
		// Compute free space
		size_t space = s->c + BUFFER_SIZE - s->w;
		size_t n = MIN(rem, space);

		if (0 < n)
		{
			// Copy chunk of data
			memcpy((void*)((uintptr_t)s->buffer + (s->w % BUFFER_SIZE)), src, n);
			s->w += n;
			rem -= n;
			src += n;
		}
		else
		{
			// Wait for free space
			pthread_mutex_lock(&s->syncMutex);
			pthread_cond_wait(&s->condvar, &s->syncMutex);
			pthread_mutex_unlock(&s->syncMutex);
		}
	}

	return 0;
}

int hps_aio_close(aioStreamT *s)
{
	if (s == NULL)
		return -1;

	delete s;

	return 0;
}
