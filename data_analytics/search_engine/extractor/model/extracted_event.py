__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List, Dict, Any

from pydantic import BaseModel

from config.constants import VERSION, EXTRACTED_EVENT_MODEL_VERSION
from model.types import Guid, UnixTimestampMilliseconds


class ExtractedEvent(BaseModel):
    version: str = EXTRACTED_EVENT_MODEL_VERSION
    bagfile_guid: Guid
    extractor_id: str
    extractor_version: str = VERSION
    ts: UnixTimestampMilliseconds
    start: UnixTimestampMilliseconds
    end: UnixTimestampMilliseconds
    metadata: Dict[str, Any]
    bva: int = 0
    tags: List[str] = []
    event_type: str
    is_deleted: bool = False
