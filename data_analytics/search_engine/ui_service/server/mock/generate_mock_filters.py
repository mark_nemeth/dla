import random
import uuid
import json
import os

OUTPUT_FILENAME = "filters.json"
VERSION = "0.1"

filters_info = {
  "duration_min_max": [
      14,
      752728000
  ],
  "locations": [
      "DE"
  ],
  "size_min_max": [
      7533,
      200032184685
  ],
  "topics": [
      "/diagnostics_toplevel_state",
      "/radar/RadarLocationAndRSIService/RadarMmrRL_S/RadarLocationAndRSI",
      "/sem/SemComService/0/SemComInterface",
      "/stereo_vision_adc_DaddyROSGateway/RosExportImagestereoParameterConfig/parameter_updates",
      "/cal/CalService/0/DsmInterface",
      "/licam2_DaddyROSGateway/mrc_image_exporterMRCRL1ParameterConfig/parameter_updates",
      "/licam1_DaddyROSGateway/StixelLidarVeloRoofFLParameterConfig/parameter_descriptions",
      "/licam1_DaddyROSGateway/StixelCommunicatorLidarVeloRoofRRParameterConfig/parameter_updates",
      "/cal/CalService/0/VehicleDataInterface",
      "/lidar/LidarPointCloudService/LidarVeloRoofRL/LidarPointCloudInterface",
      "/introspection/grid_fusion_timings/FilteringLidarGroundHeightMap",
      "/radar/RadarCRRService/0/RadarCRRInterface",
      "/driver_logbook",
      "/sem/SemClientService/Rsi/SemClientInterface",
      "/stereo/StereoLostCargoInterfaceService/StereoCameraFront/StereoLostCargoInterface",
      "/licam1_DaddyROSGateway/mrc_image_exporterMRCRR2ParameterConfig/parameter_descriptions",
      "/radar/RadarARS4xxNearRangeService/48/ARS4xxTargetListNearInterface",
      "/fusion/FusionTrafficLightService/0/FusionTrafficLightInterface",
      "/sem/SemClientService/LocalTrajectoryProcessorHanf/SemClientInterface",
      "/radar/RadarARS4xxFarRangeService/44/ARS4xxTargetListFarInterface",
      "/stereo/TLDetector/brightspot/TLDetectorInterface",
      "/lidar/LidarPointCloudService/LidarVeloRoofRR/LidarPointCloudInterface",
      "/sem/SemReporterService/0/SemConfigInterface",
      "/stereo/git/runtime_tag",
      "/stereo/ColorImage/stereo/ColorImageRight",
      "/stereo/DisparityWithConfidenceRaw/StereoCameraFront/Trigger",
      "/sem/SemClientService/pedestrian_detection_MRCRR2/SemClientInterface",
      "/stereo_vision_adc_DaddyROSGateway/StereoSensorPluginSenderstereoParameterConfig/parameter_descriptions",
      "/environment_modeling/environment_model_at_t_fusion_raw",
      "/licam2_DaddyROSGateway/VelodyneDecoderLidarVeloRoofFRParameterConfig/parameter_descriptions",
      "/radar/RadarLocationAndRSIService/RadarMmrRL_SW/RadarLocationAndRSI",
      "/planning/PlanningEnvironmentModelService/0/PlanningEnvironmentModelInterface",
      "/vulnerable_road_users/PedestrianBoundingBoxService/MRCRR1/PedestrianBoundingBoxEvent",
      "/lidar/ObjectDetection/LidarVeloBumperFL/BoxList"
  ],
  "vehicle_ids": [
      "lab02",
      "v222-5253",
      "v222-5215",
      "V-123-234",
      "unknown",
      "v222-5214",
      "v222-5252",
      "v222-5410",
      "v222-5213",
      "v222-5218",
      "v222-5251",
      "v222-5217"
        ]
  }
result = {"results": filters_info}

outpath = os.path.join("public/mockdata")
if not os.path.exists(outpath):
    os.makedirs(outpath)

outfilePath = os.path.join(outpath, OUTPUT_FILENAME)
with open(outfilePath, 'w') as outfile:
    json.dump(result, outfile, indent=4)

print("Mock json file created: " + outfilePath)
