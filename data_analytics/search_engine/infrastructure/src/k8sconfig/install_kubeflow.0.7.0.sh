#!/bin/bash
# Download AKS credentials
az aks get-credentials --name "dla-kubecluster-aks-$ENVIRONMENT" --resource-group $RESOURCEGROUP

# Download Kubeflow
wget https://github.com/kubeflow/kubeflow/releases/download/v0.7.0/kfctl_v0.7.0_linux.tar.gz -O kfctl.tar.gz
# Extract Kubeflow
tar -xvf kfctl.tar.gz
# Construct Kubeflow Path
KFPWD=`pwd`; export PATH=$PATH:$KFPWD
# Set KF_NAME to the name of your Kubeflow deployment.
export KF_NAME='dla-kubeflow'
export BASE_DIR=`pwd`/kubeflow; mkdir -p $BASE_DIR
export KF_DIR=${BASE_DIR}/${KF_NAME}

# Set the configuration file to use, such as the file specified below:
export KF_CONFIG_URI="https://raw.githubusercontent.com/kubeflow/manifests/v0.7-branch/kfdef/kfctl_k8s_istio.0.7.0.yaml"

# Generate and deploy Kubeflow:
mkdir -p ${KF_DIR}; cd ${KF_DIR}
kfctl build -V -f ${KF_CONFIG_URI}
kfctl apply -V -f kfctl_k8s_istio.0.7.0.yaml