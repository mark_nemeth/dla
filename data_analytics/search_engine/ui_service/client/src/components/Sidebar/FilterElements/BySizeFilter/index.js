import {connect} from 'react-redux';
import {filterBySize, setCurrentSizeRange} from '../../../../redux/Home/HomeActions';
import {FileTypes} from '../ByFileFilter/ByFileFilter';
import {BySizeFilter, STATIC_LIMITS} from './BySizeFilter';
import {toGBConverter} from '../../../../utils/valueConverter';

function mapStateToProps(state, ownProps) {
  let limits;
  switch (state.fileType) {
    case FileTypes.ORIGINALFILE:
      limits = toGBConverter(state.filterOptions.originalFileSizeLimits ?
          {
            min: state.filterOptions.originalFileSizeLimits[0],
            max: state.filterOptions.originalFileSizeLimits[1]
          }
          : STATIC_LIMITS.ORIGINALFILE);
      break;
    case FileTypes.DRIVE:
      limits = toGBConverter(state.filterOptions.driveSizeLimits ?
          {
            min: state.filterOptions.driveSizeLimits[0],
            max: state.filterOptions.driveSizeLimits[1]
          }
          : STATIC_LIMITS.DRIVE);
      break;
    case FileTypes.ALWAYSONFILE:
      limits = toGBConverter(state.filterOptions.alwaysonFileSizeLimits ?
          {
            min: state.filterOptions.alwaysonFileSizeLimits[0],
            max: state.filterOptions.alwaysonFileSizeLimits[1]
          }
          : STATIC_LIMITS.ALWAYSONFILE);
      break;
    case FileTypes.CHILDBAGFILE:
      limits = toGBConverter(state.filterOptions.childBagfileSizeLimits ?
          {
            min: state.filterOptions.childBagfileSizeLimits[0],
            max: state.filterOptions.childBagfileSizeLimits[1]
          }
        : STATIC_LIMITS.CHILD_BAGFILE);
      break;
  }

  const size = state.currentSizeRange || {
    'min': limits.min,
    'max': limits.max
  };
  return {size, limits};
}

export default connect(
  mapStateToProps,
  {setCurrentSizeRange, filterBySize}
)(BySizeFilter);