"""Extractor"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse
import logging
import os
import sys
import traceback

from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.logging import configure_root_logger
from DANFUtils.tracing import tracing
from DANFUtils.utils import utils as danfutils

from config.constants import VERSION
from extractors.extractor import Extractor
from managers.metadata_manager import MetadataManager
from config.constants import DANF_VERSION_TAG

logger = logging.getLogger(__name__)


def _get_commandline_args():
    parser = argparse.ArgumentParser(description=f'Extractor Version {VERSION}')
    parser.add_argument('--update_metadata',
                        action='store_true',
                        required=False,
                        help='Specify to update bagfile metadata in the database.')
    parser.add_argument('--extractors',
                        nargs='*',
                        required=False,
                        help='Specify which extractors should be executed.')
    parser.add_argument('--add_state_event',
                        action='store_true',
                        required=False,
                        help='Specify to add bagfile state event to the database.')
    logger.info(f"Cli arguments: {parser.parse_args()}")
    return parser.parse_args()


def load_env_variables():
    path = os.environ["BAGFILE_PATH"]
    logger.info(f"Extracting file {path}")

    guid = os.environ["BAGFILE_GUID"]
    logger.info(f"BAGFILE_GUID is set to {guid}")

    return path, guid


def execute_init_step(args, path, guid, metadata_manager):
    step = 'danf-extractor-init'
    started_at = danfutils.get_timestamp()
    try:
        tracing.start_task(step)
        metadata_manager.check_if_bagfile_is_valid(path, guid)
        if metadata_manager.bagfile_valid:
            metadata_manager.load_bagfile_data(path, guid)
            metadata_manager.update_bagfile_metadata(guid)
            metadata_manager.submit_bagfile_to_file_catalog(guid, path)
    except Exception:
        logger.exception(f"{step} failed")
        stacktrace = traceback.format_exc()
        tracing.task_attribute('fatalError', stacktrace)
        tracing.end_task(success=False)
        if args.add_state_event:
            metadata_manager.submit_state_updates(
                step,
                os.environ['BAGFILE_GUID'],
                os.environ['FLOW_RUN_GUID'],
                'FAILED',
                started_at,
                stacktrace)
        sys.exit(1)
    else:
        tracing.end_task()
        if args.add_state_event:
            metadata_manager.submit_state_updates(step,
                                                  os.environ['BAGFILE_GUID'],
                                                  os.environ['FLOW_RUN_GUID'],
                                                  'SUCCESSFUL',
                                                  started_at)


def execute_extractor(args, path, guid, metadata_manager):
    step = 'danf-extractor'
    started_at = danfutils.get_timestamp()
    try:
        tracing.start_task(step)
        # Pass existing bagfile metadata to the extractor
        # If update_metadata was run in the last step, get the metadata directly from the dict
        # If not, fetch it with the search API, since it's more efficient than reading from the bagfile
        if metadata_manager.bagfile_metadata:
            bagfile_metadata_dict = metadata_manager.bagfile_metadata.dict()
        else:
            bagfile_metadata_dict = metadata_manager.get_bagfile_metadata_from_api(guid)

        extractor = Extractor(path, guid, args.extractors, bagfile_metadata_dict)
        logger.info("Extractor initialized")
        extractor.execute()
        if extractor.results:
            metadata_manager.submit_extracted_metadata(guid, extractor.results)
    except Exception:
        logger.exception(f"{step} failed")
        stacktrace = traceback.format_exc()
        tracing.task_attribute('fatalError', stacktrace)
        tracing.end_task(success=False)
        if args.add_state_event:
            metadata_manager.submit_state_updates(
                step,
                os.environ['BAGFILE_GUID'],
                os.environ['FLOW_RUN_GUID'],
                'FAILED',
                started_at,
                stacktrace)
        sys.exit(1)
    else:
        tracing.end_task()
        if args.add_state_event:
            metadata_manager.submit_state_updates(step,
                                                  os.environ['BAGFILE_GUID'],
                                                  os.environ['FLOW_RUN_GUID'],
                                                  'SUCCESSFUL',
                                                  started_at)


def main(args):
    if not (args.update_metadata or args.extractors):
        logger.warning("No actions will be executed due to missing "
                       "cli arguments. Exiting program.")
        return

    path, guid = load_env_variables()
    metadata_manager = MetadataManager()
    if args.update_metadata:
        execute_init_step(args, path, guid, metadata_manager)

    if args.extractors:
        execute_extractor(args, path, guid, metadata_manager)


if __name__ == '__main__':
    configure_root_logger(
        log_level='INFO',
        enable_azure_log_aggregation=danf_env().platform != Platform.LOCAL,
    )
    logger.setLevel('INFO')
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")

    cli_args = _get_commandline_args()
    main(cli_args)
