ARG DOWNLOADER_IMAGE=python:3.7
ARG IMAGE=python:3.7
FROM $DOWNLOADER_IMAGE as downloader
ARG INDEX_URL=http://hksmirror.rd.corpintra.net:4040/root/pypi
ARG EXTRA_INDEX_URL
ARG CERT
ARG DAP_BUILD="false"
ARG DANF_VERSION_TAG="DANF_VERSION_NEEDS_TO_BE_DEFINED"

#STAGE 1
# Copy certificates
COPY ${CERT} /certs/athena.daimler.com.pem
COPY . /source
RUN rm /source/*.pem -f
RUN sed -i 's/DANF_VERSION_TAG="WILL_BE_TAKEN_FROM_GIT_TAG"/DANF_VERSION_TAG = "'"$DANF_VERSION_TAG"'"/g' source/config/constants.py

# Copy setup.py
COPY setup.py /downloads/setup.py
COPY config /downloads/config

# Permissions
RUN chmod a+r /certs/athena.daimler.com.pem \
&&  chmod a+r /downloads/setup.py

# Copy requirements to download folder
RUN python /downloads/setup.py egg_info \
&&  cp /TracingSidecar.egg-info/requires.txt /downloads

# Download the depenencys
RUN if [ "$DAP_BUILD" = "true" ] ; \
    then pip download -r /downloads/requires.txt \
    -d /downloads \
    --client-cert=/certs/athena.daimler.com.pem \
    --extra-index-url $EXTRA_INDEX_URL \
    --index-url=http://hksmirror.rd.corpintra.net:4040/root/pypi \
    --trusted-host hksmirror.rd.corpintra.net; \
    else pip download -r /downloads/requires.txt \
    -d /downloads \
    --extra-index-url $EXTRA_INDEX_URL ; fi

# STAGE2
FROM $IMAGE
ARG workdir=/home/appuser

WORKDIR $workdir

# install tools and add user
RUN pip install --upgrade --index-url=$INDEX_URL --trusted-host hksmirror.rd.corpintra.net \
    pip \
    setuptools \
    wheel \
&& groupadd --gid 1000 appuser \
&& useradd --uid 1000 --gid 1000 --home $workdir appuser \
&& chown -R appuser $workdir

USER appuser

# copy src
COPY --chown=appuser:appuser --from=downloader /source .

# copy dependencys
COPY --chown=appuser:appuser --from=downloader /downloads $workdir/downloads

# install
RUN pip install . --no-cache-dir --no-index -f $workdir/downloads --user \
&& rm $workdir/downloads -r


# start app
CMD [ "python", "-u", "-m", "TracingSidecar.main" ]
