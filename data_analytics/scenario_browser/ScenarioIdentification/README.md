# Scenario Browser.

GUI that allows users to filter and search specific scenarios.

### Hosted on:
http://53.255.104.170:8050/ (Scenario Browser)  
http://53.255.104.170:8051/ (Map Browser)  
http://53.255.104.170:8052/ (Jaywalking Browser)  

## Getting Started

### Dependencies:
ScenenarioBrowser runs on python3.
It does not require Athena image, eg no rosbag necessary.

Install python deps as:
```
pip3 install -r requirements.txt
```


### Data:
##### Copy current data from this directory in Data folder
/mapr/624.mbc.us/user/emtc_ad2_s_bag-crawl/tmp_csv/

##### Alternatively, extract data from your own files:
    python run_extractors.py #change path in line 7 to bagfiles' location

Then create a database based on deepmaps:
[MySQL/postgres installation; you should have it already.]
[Install more stuff:]
- sudo apt-get install qgis
- wget -O - https://qgis.org/downloads/qgis-2017.gpg.key | gpg --import
- gpg --fingerprint CAEB3DC3BDF7FB45
- gpg --export --armor CAEB3DC3BDF7FB45 | sudo apt-key add -
- sudo apt-get update && sudo apt-get install qgis python-qgis qgis-plugin-grass
- sudo apt-get install pgadmin4 pgadmin4-apache2
[Get a version of maps and uncompress it:] 
- wget https://athena.us624.corpintra.net/artifactory/mapdb/automaps/20200131-deepmap-2.0.29-sj/20200131-deepmap-2.0.29-sj.sql.bz2
- bzip2 -d 20200131-deepmap-2.0.29-sj.sql.bz2
[Create empty db:]
sudo -u postgres psql -c "CREATE DATABASE mapsdb"
sudo -u postgres psql -h localhost -d mapsdb -U postgres -f 20200131-deepmap-2.0.29-sj.sql
(password ‘postgres’)
Next run:
    python map_object_extractor.py
    
Finally, see setup under /mappers folder to create pivot files for search


### Run the UI
Navigate to webui folder and change the IP/Port in the app.py file as needed
    python app.py
The main map gui is found at https://your_ip:your_port
The api is found at https://your_ip:your_port/docs
The search ui is found at https://your_ip:your_port/search
