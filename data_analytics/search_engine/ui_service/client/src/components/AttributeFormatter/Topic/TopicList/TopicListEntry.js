import React, {useState} from 'react';
import FormControl from 'react-bootstrap/FormControl';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import {defaultEmptyValue} from '../../../Table';
import {isObjectValid} from '../../../../utils/helperFunctions';
import './TopicListEntry.scss'

export default function TopicListEntry(props) {

  const [filteredTopicList, setFilteredTopicList] = useState(props.topics);

  if (!isObjectValid(props.topics)) return <span>{defaultEmptyValue}</span>;

  const topicItem = (item) => (
    <OverlayTrigger placement="right" delay={{show: 400, hide: 0}} outOfBoundaries overlay={
      <Tooltip id={item} className="topic-list-tooltip" outOfBoundaries>{item}</Tooltip>
    }>
      <div className="select-topic-label">{item}</div>
    </OverlayTrigger>
  );

  function onSearchTopic(event) {
    const filteredTopics = (props.topics.filter(topic => topic.toLowerCase().includes(event.target.value.toLowerCase())));
    if (filteredTopics.length === 0) filteredTopics.push('No results found');
    setFilteredTopicList(filteredTopics)
  }

  return (
    <div className="topic-search-list">
      <FormControl placeholder="Search available topics..."
                   aria-label="Search query"
                   aria-describedby="basic-addon2"
                   onChange={onSearchTopic}
                   onClick={onSearchTopic}
      />
      <ScrollList {...props}
                  showLoading={false}
                  itemList={filteredTopicList}
                  listItemComponent={topicItem}
      />
    </div>
  )
}