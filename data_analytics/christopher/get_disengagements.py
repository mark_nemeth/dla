import glob
import os
import datetime
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '../common'))
from disengagements import *


rootPath = '/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/'
cars = ('hks01', 'hks03', 'hks04', 'sv05')

dis_ego_full = None
dis_sem_full = None
for car in cars:
    carPath = os.path.join(rootPath, car)
    print(carPath)
    for idx, bagFile in enumerate(glob.glob(os.path.join(carPath, '*alwayson.bag'))):
        print(idx, bagFile)
        # if os.path.getsize(bagFile) >= 5.0e09:
        #    print('Not analysing this file: size larger than ~5G')
        #    continue
        try:
            dis = DisengagementStatistics(bagFile)
            # ego topic
            dis_ego = dis.disengagements_ego()
            if (dis_ego_full is None) and (dis_ego is not None):
                dis_ego['car'] = car
                dis_ego_full = dis_ego
            elif dis_ego is not None:
                dis_ego['car'] = car
                dis_ego_full = dis_ego_full.append(dis_ego, ignore_index=True)
            # sem topic
            dis_sem = dis.disengagements_sem()
            if (dis_sem_full is None) and (dis_sem is not None):
                dis_sem['car'] = car
                dis_sem_full = dis_sem
            elif dis_sem is not None:
                dis_sem['car'] = car
                dis_sem_full = dis_sem_full.append(dis_sem, ignore_index=True)
        except Exception as inst:
            print('Exception:', inst)
    # serialize data frames after each car
    if dis_ego_full is not None:
        dis_ego_full.to_pickle("./dis_ego_full_checkpoint.pkl")
    if dis_sem_full is not None:
        dis_sem_full.to_pickle("./dis_sem_full_checkpoint.pkl")
