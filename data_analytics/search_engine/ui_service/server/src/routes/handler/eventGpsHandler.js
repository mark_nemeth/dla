import {areNestedKeysPresent} from '../../utils/helperFunctions';

// ChildBagfile Object Keys
const METADATA = "metadata";
const EXTRACTOR_ID_DISENGAGEMENT = "ext1_1";
const EXTRACTOR_ID_ST = "ext1_2";
const EXTRACTOR_ID_GPS = "ext1_3";
const DISENGAGEMENT_LIST = "disengagement";
const ST_LIST = "silent_testing";
const GPS_LIST = "gps";

class EventGpsHandler {

  addGpsToEvents(childBagfiles) {

    function addLatLng(event, gps) {
      const closestIndex = getIndexOfClosest(event.ts, gps.map(x => x.ts));
      event.latitude = gps[closestIndex].latitude;
      event.longitude = gps[closestIndex].longitude;
    }

    function getIndexOfClosest(value, list) {
      let index = 0;
      for (let i = 0; i < list.length; i++) {
        if (Math.abs(list[i] - value) < Math.abs(list[index] - value))
          index = i;
      }
      return index;
    }

    for (let childBagfile of childBagfiles) {
      if (areNestedKeysPresent(childBagfile, METADATA, EXTRACTOR_ID_GPS, GPS_LIST)) {
        if (areNestedKeysPresent(childBagfile, METADATA, EXTRACTOR_ID_DISENGAGEMENT)) {
          for (let disengagement of childBagfile[METADATA][EXTRACTOR_ID_DISENGAGEMENT][DISENGAGEMENT_LIST]) {
            addLatLng(disengagement, childBagfile[METADATA][EXTRACTOR_ID_GPS][GPS_LIST]);
          }
        }
        if (areNestedKeysPresent(childBagfile, METADATA, EXTRACTOR_ID_ST)) {
          for (let silentTesting of childBagfile[METADATA][EXTRACTOR_ID_ST][ST_LIST]) {
            addLatLng(silentTesting, childBagfile[METADATA][EXTRACTOR_ID_GPS][GPS_LIST]);
          }
        }
      }
    }
    return childBagfiles;
  }

}

export default new EventGpsHandler();