__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import re

from DANFUtils.tracing import tracing
from config.config_handler import config_dict
from handlers.drivetype import determine_drive_types
from handlers.filetype import determine_bagfile_type, is_file_type_whitelisted, \
    BagfileType
from handlers.utils import utils
from kubeflow.flow_creation import FlowManager
from model.exceptions import EventProcessingError
from model.models import NewBagfileEvent
from model.paths import BlobStoragePath
from proxies.metadata_api import MetadataApiProxy
from proxies.storage_queue import EventListener
from DANFUtils.utils.utils import extract_file_guid

logger = logging.getLogger(__name__)


def get_cloud_event_processor():
    return CloudBagfileEventProcessor()


class CloudBagfileEventProcessor:

    def __init__(self):
        queue_name = config_dict.get_value("queue_name")
        self.event_agent = EventListener(queue_name)
        self.metadata_api_proxy = MetadataApiProxy()
        self.flow_manager = FlowManager()
        self.input_sa_pattern = re.compile(
            r"baseinfra(abt|syv)(fltr|raw)\d\dsa(dev|qa|prod)")

    def process_new_file_event(self, new_bagfile_event: NewBagfileEvent):
        """
        Process new physical files and create initial metadata entries.
        """
        logger.info(f"Processing new bagfile event {new_bagfile_event}")
        try:
            res = self._process_new_file_event(new_bagfile_event)
            # only delete msg from queue, if processed successfully (no exp)
            # and if msg identifiers are specified
            if new_bagfile_event.msg_id and new_bagfile_event.msg_pop_receipt:
                self.event_agent.delete_event(
                    new_bagfile_event.msg_id,
                    new_bagfile_event.msg_pop_receipt)
            else:
                logger.debug(f"Missing msg identifier. "
                             f"Skipping deletion of event {new_bagfile_event}")
            return res
        except Exception:
            logger.exception(f"Failed to process new bagfile event "
                             f"{new_bagfile_event}")

    def process_new_bagfile_metadata_event(self, bagfile: dict, is_childbag: bool, test_mode: bool = False):
        """
        Start the processing flows.
        """
        try:
            if is_childbag:
                logger.info(f"Launch flow for childbagfile {bagfile}")
                return self._process_new_childbagfile_metadata_event(bagfile, test_mode)
            else:
                logger.info(f"Launch flow for bagfile {bagfile}")
                return self._process_new_bagfile_metadata_event(bagfile, test_mode)
        except Exception:
            logger.exception(f"Failed to launch flow for bagfile "
                             f"{bagfile}")

    def _process_new_file_event(self,
                                new_bagfile_event: NewBagfileEvent):
        file_name = new_bagfile_event.file_name
        storage_account = new_bagfile_event.storage_account
        container = new_bagfile_event.container_name
        file_path = f"/{storage_account}/{container}/{file_name}"
        file_type = determine_bagfile_type(new_bagfile_event.file_name)

        sa_match = self.input_sa_pattern.fullmatch(storage_account)
        if sa_match is None:
            raise EventProcessingError(
                f"Received new file event from SA with unexpected format: "
                f"{storage_account}. Ignoring file {file_path}")
        sa_region, sa_type, sa_stage = sa_match.group(1, 2, 3)  # eg abt, raw, qa

        if sa_type not in ['fltr', 'raw']:
            raise EventProcessingError(
                f"Received new file event from unsupported SA type: "
                f"{storage_account} Ignoring file {file_path}")

        if file_type == BagfileType.ORIGINAL or file_type == BagfileType.ALWAYSON:
            # create bagfile metadata
            logger.info(f"Creating metadata for bagfile {file_name}")

            drive_types = determine_drive_types(new_bagfile_event.file_name)
            priority = self._determine_processing_priority(file_name, file_type)
            # the flag is used to activate or deactivate the complete processing of a bagfile
            processing_activated = config_dict.get_value("processing_activated", "False") == "True"

            if not processing_activated:
                processing_state = 'PROCESSING_DEACTIVATED'
            elif is_file_type_whitelisted(file_type):
                processing_state = 'NOT_PROCESSED'
            else:
                processing_state = 'SKIPPED'
            # create bagfile metadata
            bagfile_guid = self.metadata_api_proxy.upsert_bagfile(file_path,
                                                                  file_name,
                                                                  file_type.name,
                                                                  drive_types,
                                                                  processing_state,
                                                                  priority)
            return bagfile_guid
        elif file_type == BagfileType.SNIPPET:
            logger.info(f"Child bag file {file_path} metadata update will be done by splitter/stripper.")
        else:
            logger.info(f"Received new file event from SA with unexpected file_type: "
                        f"{storage_account}. Ignoring file {file_path}")

    def _process_new_childbagfile_metadata_event(self,
                                                 childbagfile: dict,
                                                 test_mode: bool):
        # launch flow
        tracing.start_task_in_context(
            'launch_kubeflow',
            attributes={
                'bagfile_guid': childbagfile['guid'],
                'bagfile_path': childbagfile['link']
            })
        # Before we launch the flow, we need to check the validation of input
        blob_path = BlobStoragePath(childbagfile['link'])
        if not utils.validate_sa(blob_path.storage_account):
            logger.info(
                f"The input storage account {blob_path.storage_account} is invalid")
            # update the record of the bagfile in DB
            self.metadata_api_proxy.update_childbagfile_processing_state(
                childbagfile['guid'],
                "SKIPPED")
            return None

        flow = self.flow_manager.launch_danf_az_snippet_flow(
            childbagfile,
            blob_path,
            test_mode)

        self.metadata_api_proxy.update_childbagfile_processing_state(
            childbagfile['guid'],
            "BEING_PROCESSED")
        tracing.end_task()
        return flow

        # function for processing alwayson and original bagfiles

    def _process_new_bagfile_metadata_event(self, bagfile, test_mode):
        # launch flow
        tracing.start_task_in_context(
            'launch_kubeflow',
            attributes={
                'bagfile_guid': bagfile['guid'],
                'bagfile_path': bagfile['link']
            })
        # Before we launch the flow, we need to validate the input
        blob_path = BlobStoragePath(bagfile['link'])
        if not utils.validate_sa(blob_path.storage_account):
            logger.info(
                f"The input storage account {blob_path.storage_account} is invalid")
            # update the record of the bagfile in DB
            self.metadata_api_proxy.update_bagfile_processing_state(
                bagfile['guid'],
                "SKIPPED")
            return None

        file_type = BagfileType[bagfile['file_type']]
        if file_type == BagfileType.ORIGINAL:
            flow = self.flow_manager.launch_danf_az_original_processing_flow(
                bagfile, blob_path, test_mode)
        elif file_type == BagfileType.ALWAYSON:
            flow = self.flow_manager.launch_az_alwayson_processing_flow(
                bagfile, blob_path, test_mode)

        self.metadata_api_proxy.update_bagfile_processing_state(
            bagfile['guid'],
            "BEING_PROCESSED")
        tracing.end_task()
        return flow

    @staticmethod
    def _determine_processing_priority(file_name: str, file_type: BagfileType):
        if '_prio' in file_name.lower():
            return 1
        if config_dict.get_value("prioritize_original_processing", "True") == "True" \
                and file_type == BagfileType.ORIGINAL:
            return 1
        return 0
