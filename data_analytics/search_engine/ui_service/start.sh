#!/bin/bash
#######GLOBALS#######################################################################################
CURRENT_DIR="$(cd "$(dirname "$0")" && pwd)"    #get path of batch file....
DIR_DLA_REPO=${CURRENT_DIR%/*/*/*}			#dla repo folder.....
DIR_DATA_ANALYTICS="$DIR_DLA_REPO/data_analytics" #data_analytics folder...
DIR_SEARCH_ENGINE="$DIR_DATA_ANALYTICS/search_engine" #search_engine folder...

#####################################################################################################

#######HELP##########################################################################################
display_help() {
    echo "Usage: $0 [Command]" >&2
    echo
    echo "   unittest                 Launch unittests located in ../tests/unittests/."
    echo "   start                    Launch microservice ui_service ."
    echo "   help or h   	            Display help."
    echo "   check  	            Perform a code check. Different options are available. [check with $0 check help]"

    exit 1
}

#####################################################################################################

#######FUNCTIONS#####################################################################################       <---------------------- Add additional functions
######Launch run_test.py script for starting unittests located in */tests/unittests/<name>_unittest.py#######
launch_all_unittests(){
printf 'Starting unittests.........\n'
echo '---------------------------------------------------------------------'
python3 "${CURRENT_DIR}/tests/unittests/run_tests.py" #"-cov=True" #"-html=True"   #"-html=True" -- create textfile report  , "-html=True" create html report      #opts for activating extra log.txt file in folder or html report

exit $?
}
#####################################################################

######Launch microservice###########################################
launch_microservice(){
#python3 "${DIR_SEARCH_ENGINE}/data_handler/index.py"
python3 "${DIR_SEARCH_ENGINE}/ui_service/index.py"
#python3 "${DIR_SEARCH_ENGINE}/metadata_api/index.py"
#python3 "${DIR_SEARCH_ENGINE}/ui_service/index.py"

}
#####################################################################

######CodeCheck######################################################
display_help_codecheck(){
    echo "Usage: $0 code_check [Command]" >&2
    echo
    echo "   replace_all                    Replace all python files with formatted ones."
    echo "   create_diff                    Create a diff for all *.py files (create *.diff files)."
    echo "   clean_up   	                  Delete all created *.diff files"
    exit 1
}



code_check(){

case $1
in
replace_all)sh ${DIR_DLA_REPO}/code_check/codecheck.sh -r ;;
create_diff) sh ${DIR_DLA_REPO}/code_check/codecheck.sh -d ;;
clean_up) sh ${DIR_DLA_REPO}/code_check/codecheck.sh -c ;;
*) echo 'Wrong command...'
   echo 'Possible commands are.....'
   display_help_codecheck ;;
esac
}

#####################################################################



#######^^FUNCTIONS^^^################################################################################

#####MENU############################################################################################
echo "$2"

case $1
in
unittest) launch_all_unittests ;;
start) launch_microservice ;;
help) display_help	;;
h) display_help	;;
check) code_check $2 ;;
"") launch_microservice ;;
*) echo 'Wrong command...'
   echo 'Possible commands are.....'
   display_help ;;
esac


##^^^MENU^^^#########################################################################################




