import React from 'react';
import {shallow} from "enzyme";
import {HomePage} from "./HomePage";
import {getTestOriginalFileList} from "../../setupTests";
import {Table} from "../../components/Table/Table";

describe('HomePage component', () => {

  it('should renders table when there are original files and no error', () => {
    const props = {
      error: false,
      currentFiles: getTestOriginalFileList(true)
    };
    const wrapper = shallow(<HomePage {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('Bootstrap(Spinner)').first().exists()).toBe(false);
  });

  it('should render loading spinners when there are no error and should show loading', () => {
    const props = {error: false, currentFiles: [], showLoading: true};
    const wrapper = shallow(<HomePage {...props} />);
    expect(wrapper.find('Bootstrap(Spinner)').first().exists()).toBe(true);
    expect(wrapper.find('Table').exists()).toBe(false);
  });

  it('should perform refresh action when click on refresh button', () => {
    const reduxAction = jest.fn();
    const props = {
      error: false,
      currentFiles: getTestOriginalFileList(),
      showLoading: true,
      refresh: reduxAction
    };
    const wrapper = shallow(<HomePage {...props} />);
    const refreshButton = wrapper.find('Button').first();
    refreshButton.simulate("click");
    expect(reduxAction).toHaveBeenCalled();
  });
});

