"""App logging."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging
from typing import Optional, Union

from colorlog import ColoredFormatter


def create_logger(
    name: Optional[str] = None,
    level: Union[int, str] = logging.INFO,
    fmt: str = "%(asctime)s - %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s (%(name)s)",
    datefmt: Optional[str] = None,
) -> logging.Logger:
    """Create logger for the application.

    Args:
        level: defines the log level
        name: name of the logger
        fmt: log formatter string
        datefmt: date format specifier

    Returns:
        logging.Logger: Custom logger object.
    """
    logger = logging.getLogger(name)
    logger.setLevel(level)

    # Add a console handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    console_formatter = ColoredFormatter(fmt=fmt, datefmt=datefmt)
    console_handler.setFormatter(console_formatter)
    logger.addHandler(console_handler)

    return logger
