import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByStatusFilter} from "./ByStatusFilter";
import ScrollList from "../../../Utils/ScrollList/ScrollList";

describe('ByStatusFilter component', () => {

  it('should render without crashing', () => {
    const props = {
      statusList: ["NOT_PROCESSED", "BEING_PROCESSED", "SKIPPED"],
      selectedStatus: ['BEING_PROCESSED']
    };
    const wrapper = shallow(<ByStatusFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should renders status list with selected column', () => {
    const reduxAction = jest.fn();
    const props = {
      statusList: ["NOT_PROCESSED", "BEING_PROCESSED", "SKIPPED","PROCESSING_DEACTIVATED", "PROCESSING_FAILED", "PROCESSING_SUCCESSFUL"],
      selectedStatus: ['BEING_PROCESSED'],
      filterByStatus: reduxAction
    };
    const wrapper = mount(<ByStatusFilter {...props} />);
    const statuslist = wrapper.find(ScrollList);
    const entries = statuslist.find('tr');
    expect(entries.length).toBe(6);
    expect(entries.at(0).find('input').props().checked).toBe(false);
    expect(entries.at(1).find('input').props().checked).toBe(true);
    expect(entries.at(2).find('input').props().checked).toBe(false);
  });


  it('should call action when status is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      statusList: ["NOT_PROCESSED", "BEING_PROCESSED", "SKIPPED","PROCESSING_DEACTIVATED", "PROCESSING_FAILED", "PROCESSING_SUCCESSFUL"],
      selectedStatus: ['BEING_PROCESSED'],
      filterByStatus: reduxAction
    };
    const wrapper = mount(<ByStatusFilter {...props} />);
    const input = wrapper.find('tr').at(1).find('input');
    input.simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

});
