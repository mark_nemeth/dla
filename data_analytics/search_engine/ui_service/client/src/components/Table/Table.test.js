import React from 'react';
import {Table} from './Table';
import {shallow,mount} from 'enzyme/build';
import {getTestOriginalFileList} from '../../setupTests';

const tableColums = [{
  dataField: 'guid',
  text: 'ID',
  sort: true
}, {
  dataField: 'link',
  text: 'Path',
  sort: true
}, {
  dataField: 'size',
  text: 'Size',
  sort: true
}, {
  dataField: 'num_messages',
  text: 'No. Messages',
  sort: true
}, {
  dataField: 'start',
  text: 'Start Time',
  sort: true
}, {
  dataField: 'end',
  text: 'End Time',
  sort: true
}, {
  dataField: 'duration',
  text: 'Duration',
  sort: true
}, {
  dataField: 'vehicle_id_num',
  text: 'VID',
  sort: true
}];

describe('Table component', () => {

  it('should renders Table with props', () => {
    const props = {
      currentFiles: getTestOriginalFileList(true),
      columns: tableColums
    };
    const wrapper = shallow(<Table {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should hide pagination for zero bagfiles', () => {
    const props = {
      currentFiles: [],
      columns: tableColums
    };
    const wrapper = shallow(<Table {...props} />);
    expect(wrapper.find('SizePerPageDropdownStandalone').exists()).toBe(false);
  })
});
