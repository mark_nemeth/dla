import React from 'react';
import {shallow} from 'enzyme/build';
import {ByFileFilter, FileTypes} from './ByFileFilter';

describe('ByFileFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      tableType: FileTypes.ORIGINALFILE
    };
    const wrapper = shallow(<ByFileFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

});
