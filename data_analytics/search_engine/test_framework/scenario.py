import configparser
import json
import traceback
import requests
from pyassert import *
import logging
import time
from datetime import datetime

from config.config_handler import config_dict
from ContainerClient import ContainerClient

config = configparser.ConfigParser()

file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                         'test_cases',
                         config_dict.get_value("testcase_file") + '.txt')
config.read(file_path)

message = {'failed': '#### ***Test case is failed*** ❌  reason: {}{}',
           'info': '#### ** {} scenario for {} environment ** {}',
           'status_message': '--- API: {} , status code : {} :white_check_mark: {}'}


class Scenario:

    def __init__(self):
        self.result = ''
        self.client = ContainerClient()

    def insert_bagfile(self, tag):
        env = config.get(tag, 'env')
        api = config.get(tag, 'api')
        self.result = f'{self.result}{message["info"].format(tag, env, os.linesep)}'

        try:
            # getting the standard dictionary definition of the api post_bagfile
            bagfile = json.loads(config.get(tag, 'value'))
            guid = self.client.create_bagfile(bagfile)

            # adding the guid filed in the bagfile dictionary
            bagfile['guid'] = guid
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format(api, res.status_code, os.linesep)}'

            actual_value = self.client.get_bagfile(guid)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("get_bagfile", res.status_code, os.linesep)}'
            expected_value = bagfile
            assert_that(actual_value).is_equal_to(expected_value)

            # deleting the inserted document based on guid
            self.client.delete_bagfile(guid)
            self.result = f'{self.result}{message["status_message"].format("delete_bagfile", res.status_code, os.linesep)}'

        except Exception as e:
            logging.error("Exception: {}".format(traceback.format_exc()))
            self.result = f'{self.result}{message["failed"].format(e, os.linesep)}'

    def extraction_request(self, tag):
        env = config.get(tag, 'env')
        api = config.get(tag, 'api')
        self.result = f'{self.result}{message["info"].format(tag, env, os.linesep)}'

        try:
            input_data = json.loads(config.get(tag, 'value'))
            bagfile = input_data['bagfile']
            extraction = input_data['extraction']

            guid = self.client.create_bagfile(bagfile)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("post_bagfile", res.status_code, os.linesep)}'

            extraction['bagfile_guid'] = guid

            extractor_guids = self.client.submit_metadata(extraction)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format(api, res.status_code, os.linesep)}'

            # deleting the inserted bagfiles based on guid
            self.client.delete_bagfile(guid)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("delete_bagfile", res.status_code, os.linesep)}'

            for guid in extractor_guids:
                self.client.delete_extractor(guid)
                res = self.client._last_response

                self.result = f'{self.result}' \
                    f'{message["status_message"].format(f"delete_extractor for guid {guid}", res.status_code, os.linesep)}'
        except Exception as e:
            logging.error("Exception: {}".format(traceback.format_exc()))
            self.result = f'{self.result}{message["failed"].format(e, os.linesep)}'

    def search_request(self, tag):
        env = config.get(tag, 'env')
        api = config.get(tag, 'api')
        self.result = f'{self.result}{message["info"].format(tag, env, os.linesep)}'

        try:
            input_data = json.loads(config.get(tag, 'value'))
            bagfile = input_data['bagfile']
            extraction = input_data['extraction']

            bg_uid = self.client.create_bagfile(bagfile)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("post_bagfile", res.status_code, os.linesep)}'

            extraction['bagfile_guid'] = bg_uid
            extractor_guid = self.client.submit_metadata(extraction)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("post_extractionrequest", res.status_code, os.linesep)}'

            data = self.client.get_snippets_bva(bagfile_guid=bg_uid, bva_limit=5)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format(api, res.status_code,os.linesep)}'

            # deleting the inserted childbagfiles based on guid
            for child_bagfile in data:
                self.client.delete_childbagfile(child_bagfile['guid'])
                res = self.client._last_response
                self.result = f'{self.result}{message["status_message"].format("delete_childbagfile", res.status_code, os.linesep)}'

            # deleting the inserted bagfiles based on guid
            self.client.delete_bagfile(bg_uid)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("delete_bagfile", res.status_code,os.linesep)}'
            self.client.delete_extractor(extractor_guid)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("delete_extractor", res.status_code, os.linesep)}'

        except Exception as e:
            logging.error("Exception: {}".format(traceback.format_exc()))
            self.result = f'{self.result}{message["failed"].format(e, os.linesep)}'

    def test_full_flow_on_prem(self, tag):
        env = config.get(tag, 'env')
        self.result = f'{self.result}{message["info"].format(tag, env, os.linesep)}'

        try:
            input_data = json.loads(config.get(tag, 'value'))
            path = input_data['path']
            output_dir = input_data['output_directory'] + datetime.now().strftime('%Y%m%d_%H%M%S')

            # Trigger the full flow
            childbagfiles_path = [os.path.join(output_dir, cbf) for cbf in input_data['childbagfiles']]

            data = self.client.simulate_new_file(path, output_dir)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("post_simulate_new_file", res.status_code, os.linesep)}'

            # Sleep as the process takes about 15 minutes to complete
            time.sleep(900)

            # Query for the Bagfile guid to get status of tasks
            guid = data['bagfile_guid']
            actual_value = self.client.get_bagfile(bagfile_guid=guid)
            res = self.client._last_response

            self.result = f'{self.result}{message["status_message"].format("get_bagfile", res.status_code, os.linesep)}'

            if actual_value:
                # checking state value for the applied operations
                state = actual_value['state']
                steps = input_data['steps']  # Getting the run steps
                for step in steps:
                    step_exists = False
                    for task in state:
                        if step == task['name']:
                            step_exists = True
                            if task['status'] == "FAILED":
                                self.result = f'{self.result}{message["failed"].format(task["report"]["message"], os.linesep)}'
                                return
                    if not step_exists:
                        self.result = f'{self.result}{message["failed"].format(f"step {step} does not exists", os.linesep)}'
                        return

                # Verify the generated child bagfiles
                data = self.client.file_check(paths=childbagfiles_path, delete_files=True)
                res = self.client._last_response
                self.result = f'{self.result}{message["status_message"].format("post_file_check", res.status_code, os.linesep)}'

                for item in data:
                    if not item['exists']:
                        self.result += '#### ***Test case is failed*** ❌  reason: ' + \
                                       f'File path {item["path"]} does not exist' + '\n'
                        path = item["path"]
                        self.result = f'{self.result}{message["failed"].format(f"File path {path} does not exist", os.linesep)}'

        except Exception as e:
            logging.error("Exception: {}".format(traceback.format_exc()))
            self.result = f'{self.result}{message["failed"].format(e, os.linesep)}'
        finally:
            logging.info(self.result)
            logging.info('sending the long running process notification to mattermost')
            webhook_url = config_dict.get_value("webhook_url")
            logging.info("webhook url: {}".format(webhook_url))
            requests.post(webhook_url, json={'text': self.result})

    def test_full_flow_on_cloud(self, tag):
        env = config.get(tag, 'env')
        self.result = f'{self.result}{message["info"].format(tag, env, os.linesep)}'

        try:
            input_data = json.loads(config.get(tag, 'value'))
            path = input_data['path']
            output_dir = input_data['output_directory'] + datetime.now().strftime('%Y%m%d_%H%M%S')

            # Trigger the full flow
            data = self.client.simulate_new_file(path, output_dir)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("post_simulate_new_file", res.status_code, os.linesep)}'

            # Sleep as the process takes about 15 minutes to complete
            time.sleep(900)

            # Query for the Bagfile guid to get status of tasks
            guid = data['bagfile_guid']
            actual_value = self.client.get_bagfile(bagfile_guid=guid)
            res = self.client._last_response
            self.result = f'{self.result}{message["status_message"].format("get_bagfile", res.status_code, os.linesep)}'

            if actual_value:
                # checking state value for the applied operations
                state = actual_value['state']
                steps = input_data['steps']  # Getting the run steps
                for step in steps:
                    step_exists = False
                    for task in state:
                        if step == task['name']:
                            step_exists = True
                            if task['status'] == "FAILED":
                                self.result = f'{self.result}{message["failed"].format(task["report"]["message"], os.linesep)}'
                                return
                    if not step_exists:
                        self.result = f'{self.result}{message["failed"].format(f"step {step} does not exists", os.linesep)}'
                        return

        except Exception as e:
            logging.error("Exception: {}".format(traceback.format_exc()))
            self.result = f'{self.result}{message["failed"].format(e, os.linesep)}'
        finally:
            logging.info(self.result)
            logging.info('sending the long running process notification to mattermost')
            webhook_url = config_dict.get_value("webhook_url")
            logging.info("webhook url: {}".format(webhook_url))
            requests.post(webhook_url, json={'text': self.result})
