import json
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from config.config_handler import config_dict
from azure.storage.queue import (QueueService, QueueMessageFormat)
from azure.storage.blob import BlockBlobService
import argparse
from handlers.utils import utils


class MessageGenerator:

    def __init__(self, container_name, input_storage_account, end_index, start_index):
        self.queue_name = config_dict.get_value("queue_name")
        self.storage_queue_account = config_dict.get_value('storage_queue_account')
        self.queue_service = QueueService(
            account_name=self.storage_queue_account,
            account_key=utils.get_storage_account_access_key(self.storage_queue_account),
            protocol='https',
            endpoint_suffix='core.windows.net')
        self.queue_service.encode_function = QueueMessageFormat.text_base64encode
        self.queue_service.decode_function = QueueMessageFormat.text_base64decode

        self.input_storage_account = input_storage_account
        self.storage_service = BlockBlobService(
            account_name=self.input_storage_account,
            account_key=utils.get_storage_account_access_key(self.input_storage_account),
            protocol='https')
        self.container_name = container_name
        self.blobs = self.storage_service.list_blobs(self.container_name)

        self.end_index = end_index if end_index is not None else len(list(self.blobs))

        self.start_index = start_index
        self.body = dict()

    def generate_message_body(self, file_name, event_tpe, visibility_timeout=15):

        self.body[
            "topic"] = f"/subscriptions/e596f025-7931-4a4b-8643-4a1c0be2a8ff/resourceGroups/dla-datalake-dev/providers/Microsoft.Storage/storageAccounts/{self.input_storage_account}"
        self.body["eventType"] = f"Microsoft.Storage.{event_tpe}"
        self.body["id"] = "66213c90-a01e-0076-0fd1-30b1ab066a84"
        self.body["dataVersion"] = ""
        self.body["metadataVersion"] = 1
        # signal: file_none: -1, file_not_exist: 0, file_exist: 1
        file_valid = 0
        for counter, blob in enumerate(self.blobs):
            if file_valid == 1:
                break
            if file_name is not None:
                if blob.name == file_name:
                    file_valid = 1
                else:
                    continue
            if self.start_index <= counter <= self.end_index:
                self._generate_and_publish_message(blob, visibility_timeout)

        if file_name is None:
            file_valid = -1
        if file_valid == 0:
            raise Exception("cannot find the filename in the blob storage!")

    def _generate_and_publish_message(self, blob, visibility_timeout):
        self.body["subject"] = f"/blobServices/default/containers/{self.container_name}/blobs/{blob.name}"
        self.body["eventTime"] = str(blob.properties.creation_time)
        data = {"api": "CreateBlob", "requestId": "66213c90-a01e-0076-0fd1-30b1ab000000", "blobType": "BlockBlob",
                "sequencer": "000000000000000000000000000001E500000000007f8217",
                "storageDiagnostics": {"batchId": "5dc3a9b9-a2d9-460f-9654-942369387603"}}

        data["contentType"] = blob.properties.content_settings.content_type
        data["url"] = f"https://{self.input_storage_account}.blob.core.windows.net/{self.container_name}/{blob.name}"
        self.body["data"] = data
        print(self.body)
        res = self.queue_service.put_message(self.queue_name, json.dumps(self.body), visibility_timeout, -1)


def _get_commandline_args():
    parser = argparse.ArgumentParser(description='Generate event messages in azure storage queue')
    parser.add_argument('--container_name',
                        action='store',
                        required=True,
                        help='container_name name is required')
    parser.add_argument('--file_name',
                        action='store',
                        help='file_name is optional, otherwise will retrieve all the files')
    parser.add_argument('--event_type',
                        action='store',
                        choices=['BlobCreated', 'BlobDeleted'],
                        default='BlobCreated',
                        help='This argument specifies the event type of the message which will be generated')
    parser.add_argument('--storage_account',
                        action='store',
                        choices=['dlastorage1sadev', 'dlastorage2sadev', 'dlastorage3sadev',
                                 'dlastorage4sadev', 'dlastorage5sadev'],
                        default='dlastorage1sadev',
                        help='This argument specifies the blob storage account where the given file or container is stored. Default: dlastorage1sadev')
    parser.add_argument('--end_index',
                        action='store',
                        default=None,
                        type=int,
                        help='This argument specifies the end index of the bagfiles in the container')
    parser.add_argument('--start_index',
                        action='store',
                        default=0,
                        type=int,
                        help='This argument specifies the start index of the bagfiles in the container')

    return parser.parse_args()


def main(container_name, file_name, event_type, input_storage_account, end_index, start_index):
    if file_name is None:
        print("file name is missing, all the files in the blob will be retrieved")

    mg = MessageGenerator(container_name, input_storage_account, end_index, start_index)
    mg.generate_message_body(file_name, event_type, 15)


if __name__ == "__main__":
    args = _get_commandline_args()
    main(args.container_name, args.file_name, args.event_type, args.storage_account, args.end_index, args.start_index)
