"""Main script for running offline bagfile splitting using extractor/tmt"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import sys
import os
import argparse
import logging
import subprocess
from pathlib import Path

DEFAULT_DOCKER_IMAGE_EXTRACTOR = "offline_extractor:latest"
DEFAULT_DOCKER_IMAGE_SPLITTER = "tmt:latest"
DEFAULT_INPUT_FILE_PATTERN = "*.bag"


def main():
    args = parse_args(sys.argv[1:])
    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO

    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.FileHandler(filename=os.path.expanduser("~/tmt_tool.log")),
                                  logging.StreamHandler()],
                        level=log_level,
                        datefmt='%Y-%m-%d %H:%M:%S')

    if not os.path.isdir(args.input_dir):
        logging.error("Input directory {} doesn't exist.".format(args.output_dir))
        return 1

    if not os.path.isdir(args.output_dir):
        logging.error("Output directory {} doesn't exist.".format(args.output_dir))
        return 1

    # check if relevant docker images are available in local repo
    err_str = "Docker image {} could not be found in local docker repo! Please build/pull it first!"
    if not docker_image_exists(args.docker_img_extractor):
        logging.error(err_str.format(args.docker_img_extractor))
    if not docker_image_exists(args.docker_img_splitter):
        logging.error(err_str.format(args.docker_img_splitter))

    mnt_dirs = {args.output_dir: "/output"}

    processed_files = []
    failed_files = []
    skipped_files = []
    logging.info(
        "Processing all files that match pattern {} in directory {}".format(args.input_file_pattern, args.input_dir))
    for filename in Path(args.input_dir).glob('**/{}'.format(args.input_file_pattern)):
        # extraction
        logging.debug("Extracting events from file {}".format(filename))
        mnt_dirs[str(filename.parents[0])] = "/input"
        env_vars = {
            "BAGFILE_PATH": "/input/{}".format(filename.name),
            "OFFLINE_OUTPUT_PATH": "/output/"
        }
        if run_docker_container(args.docker_img_extractor, mnt_dirs, env_vars, args.show_container_stdout):
            # extraction failed
            logging.error("Extracting events from file {} FAILED".format(filename))
            failed_files.append(filename)
        else:
            # splitting
            splitting_config = "{}/{}.json".format(args.output_dir, filename.stem)
            splitting_config_docker = "/output/{}.json".format(filename.stem)
            if os.path.exists(splitting_config):
                logging.debug("Splitting file {} based on events from {}".format(filename, splitting_config))
                env_vars = {
                    "TMT_MODE": "danf-splitting",
                    "TMT_CONFIG": splitting_config_docker,
                    "INPUT_PATH": "/input/{}".format(filename.name),
                    "OUTPUT_PATH": "/output/{}".format(filename.name)
                }
                if run_docker_container(args.docker_img_splitter, mnt_dirs, env_vars, args.show_container_stdout, use_current_user=True):
                    # splitting failed
                    logging.error("Splitting file {} based on events from {} FAILED".format(filename, splitting_config))
                    failed_files.append(str(filename))
                else:
                    processed_files.append(filename)
                os.remove(splitting_config)
            else:
                # extraction provided no relevant events
                logging.debug(
                    "Extractor request json file not found {}. ".format(splitting_config))
                logging.warning(
                    "No relevant events found in file {}. Skipping file splitting...".format(filename))
                skipped_files.append(str(filename))

    if len(processed_files) == 0 and len(failed_files) == 0 and len(skipped_files) == 0:
        logging.info(
            "Directory {} contains no files that match the pattern {}".format(args.input_dir, args.input_file_pattern))
    else:
        logging.info("File processing completed")
        if len(failed_files) > 0:
            logging.info("Failed files:\n\t{}".format("\n\t".join(failed_files)))
        if len(skipped_files) > 0:
            logging.info(
                "Skipped files (containing no relevant events for splitting):\n\t{}".format("\n\t".join(skipped_files)))
    return 0


def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-dir',
                        '-i',
                        type=str,
                        required=True,
                        help='Directory containing the input bagfiles'
                        )
    parser.add_argument('--output-dir',
                        '-o',
                        type=str,
                        required=True,
                        help='Directory that will be used to store the splitted bagfiles'
                        )
    parser.add_argument('--docker-img-extractor',
                        type=str,
                        default=DEFAULT_DOCKER_IMAGE_EXTRACTOR,
                        help='The name:tag of the docker image that will be used for extracting the timestamps.'
                        )
    parser.add_argument('--docker-img-splitter',
                        type=str,
                        default=DEFAULT_DOCKER_IMAGE_SPLITTER,
                        help='The name:tag of the docker image that will be used for splitting.'
                        )
    parser.add_argument('--input-file-pattern',
                        type=str,
                        default=DEFAULT_INPUT_FILE_PATTERN,
                        help='The pattern (*.bag) that will be applied for fetching the files from the input folder'
                        )
    parser.add_argument('--verbose',
                        '-v',
                        action='store_true',
                        help='Verbose Logging'
                        )
    parser.add_argument('--show-container-stdout',
                        action='store_true',
                        help='Redirect container STDOUT'
                        )
    return parser.parse_args(args)


def docker_image_exists(image_name):
    try:
        cmd = ["docker", "images", "-q", image_name]
        output = subprocess.check_output(cmd).decode("utf-8").split('\n')
        if output[0] != '':
            return True
        else:
            return False
    except subprocess.CalledProcessError as e:
        logging.error("An error occured when checking the existence of docker image {}:".format(image_name),
                      exc_info=True)
        return False


def run_docker_container(image_name, mount_dirs=None, env_vars=None, show_stdout=False, use_current_user=False):
    # build run command
    cmd = ["docker", "run"]
    if mount_dirs:
        for k, v in mount_dirs.items():
            cmd.extend(["-v", "{}:{}".format(k, v)])
    if env_vars:
        for k, v in env_vars.items():
            cmd.extend(["-e", "{}={}".format(k, v)])

    # force docker to run with current user
    if use_current_user:
        cmd.extend(["--user", "{}:{}".format(os.geteuid(), os.getegid())])
    cmd.extend([image_name])

    # run docker container
    logging.debug("Starting docker container... [ {} ]".format(' '.join(cmd)))
    try:
        if show_stdout:
            sub_proc = subprocess.Popen(cmd, shell=False)
        else:
            sub_proc = subprocess.Popen(cmd, stdout=open(os.devnull, 'w'), stderr=open(os.devnull, 'w'), shell=False)

        sub_proc.wait()
    except subprocess.CalledProcessError as e:
        logging.error("An error occured when running the docker image {}".format(image_name), exc_info=True)
        return 1
    return 0


if __name__ == "__main__":
    sys.exit(main())
