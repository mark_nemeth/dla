#include "ros/ros.h"
#include "std_msgs/String.h"
#include <getopt.h>
#include <stdio.h>
#include <string>
#include <sstream>

int main(int argc, char *argv[])
{
    double packet_size = 64*1024;
    double bandwidth = 1e9;
    std::string topic_suffix = "";
    std::string topic = "SyntheticData";

    // parsing command options

      int opt;
      while ((opt = getopt(argc, argv, "b:p:t:")) != -1)
        {
          switch (opt)
            {
            case 'b':
              bandwidth = atof(optarg);
              break;

            case 'p':
              packet_size = atof(optarg);
              break;

            case 't':
              topic_suffix = std::string(optarg);
              break;

            default:
              printf("talker -b <bandwidth> -p <size> -t <suffix>");
              abort ();
            }
        }

    // Actually starting work with ROS now
    ros::init(argc, argv, "talker",ros::init_options::AnonymousName);

    ros::NodeHandle n;
    ros::Publisher pub = n.advertise<std_msgs::String>(topic.append(topic_suffix), 10); // Publish under topic SyntheticData with queue_size 10

    ros::Rate loop_rate(bandwidth/(packet_size*8));
    while (ros::ok())
    {
        std_msgs::String msg;
        std::stringstream data_pack;
        data_pack << std::string(int(packet_size),'0'); // assemble dummy data of '0' string stream with size packet_size
        msg.data = data_pack.str();

        pub.publish(msg);

        ros::spinOnce();
        loop_rate.sleep();
     }

     return 0;
}
