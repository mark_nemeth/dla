import {isObjectValid} from '../../utils/helperFunctions';

/**
 * Replace string template placeholders with real values
 * Strings placeholders need to be defined as $NUMBER$ to be replaced be a real value.
 * The NUMBER should match the index of the real value inside placeholderValues array.
 * @param string with placeholders
 * @param placeHolderValues array of values which should be included
 * @returns the string with the real values included, or just the string if no or wrong values are used
 */
export function insertString(string, ...placeHolderValues) {
  if (!isObjectValid(placeHolderValues) && placeHolderValues.length <= 0) return string;
  for (let i = 0; i < placeHolderValues.length; i++) {
    const placeholderKey = '$' + (i+1) + '$';
    string = string.replace(placeholderKey, placeHolderValues[i]);
  }
  return string;
}