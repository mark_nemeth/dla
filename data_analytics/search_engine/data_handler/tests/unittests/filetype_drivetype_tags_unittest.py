__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from handlers.filetype import determine_bagfile_type, BagfileType
from handlers.drivetype import determine_drive_types


class TestFiletypeDrivetype(unittest.TestCase):
    def setUp(self):
        self.filename_orig_validation = "/mapr/624.mbc.us/data/buffer/rd/athena/08_robotaxi/plog/byName/v222-5469/2020/02/04/20200204_110136_CID_central_return_uk_0_ORIGINAL_validation.bag"
        self.filename_dml_training = "/mapr/624.mbc.us/data/buffer/rd/athena/08_robotaxi/plog/byName/v222-5469/2020/02/04/20190801_004041_DML_FTST_SJ-Route_cv1_1.ORIGINAL_training.bag"
        self.filename_alwayson_test = "/mapr/624.mbc.us/data/input/rd/athena/08_robotaxi/plog/byName/v222-5215/2018/12/11/20181211_140121_CTD_alwayson_CV1_DML_test.bag"
        self.filename_unknown = "/mapr/624.mbc.us/data/input/rd/athena/08_robotaxi/plog/byName/v222-5251/2019/11/26/20191126_072737_CID_CTDJa, ic_planning_io.20191126_100341.disengagement_training_VALIDATION_test.bag"

    def test_filentype_original(self):
        file_type = determine_bagfile_type(self.filename_orig_validation)
        self.assertEqual(file_type, BagfileType.ORIGINAL)

    def test_filentype_dml(self):
        file_type = determine_bagfile_type(self.filename_dml_training)
        self.assertEqual(file_type, BagfileType.DML_CV1)

    def test_filentype_alwayson(self):
        file_type = determine_bagfile_type(self.filename_alwayson_test)
        self.assertEqual(file_type, BagfileType.ALWAYSON)

    def test_filentype_unkown(self):
        file_type = determine_bagfile_type(self.filename_unknown)
        self.assertEqual(file_type, BagfileType.UNKNOWN)

    def test_drive_types_cid(self):
        drive_types = determine_drive_types(self.filename_orig_validation)
        self.assertEqual(drive_types, ["CID"])

    def test_drive_types_ctd(self):
        drive_types = determine_drive_types(self.filename_alwayson_test)
        self.assertEqual(drive_types, ["CTD"])

    def test_drive_types_multiple(self):
        drive_types = determine_drive_types(self.filename_unknown)
        self.assertEqual(drive_types, ["CID", "CTD"])


if __name__ == '__main__':
    unittest.main()