__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import (ENV_SPAN_ID, ENV_TRACE_ID,
                                       ENV_TRACE_OPTIONS)
from kfp import dsl
from kubernetes.client import V1VolumeMount, V1SecurityContext
from kubernetes.client.models import V1EnvVar

from config.config_handler import config_dict
from kubeflow.steps.common import (
    RETRY_NUM, PIPELINE_POD_LABEL, BAGFILE_POD_LABEL, FILE_NAME_POD_LABEL,
    sanitize_label, METADATA_API_BASE_URL, APPINSIGHTS_KEY, DANF_ENV, AUTH,
    AUTH_TOKEN_URL, CPU_RESOURCE_TITLE, MEMORY_RESOURCE_TITLE,
    TESTING_POD_LABEL, extract_file_name_from_path, AUTH_METADATA_AUDIENCE,
    AUTH_METADATA_CLIENT_CRED_ID, AUTH_METADATA_CLIENT_CRED_SECRET,
    VIN_POD_LABEL, HTTP_PROXY, HTTPS_PROXY, NO_PROXY, get_pvc_volume,
    AUTH_DATAHANDLER_AUDIENCE, AUTH_DATAHANDLER_CLIENT_CRED_ID,
    AUTH_DATAHANDLER_CLIENT_CRED_SECRET, AUTH_SEARCH_AUDIENCE,
    AUTH_SEARCH_CLIENT_CRED_ID, AUTH_SEARCH_CLIENT_CRED_SECRET,
    SEARCH_API_BASE_URL, extract_tag_from_docker_img)


class AtpftstExtractorOp(dsl.ContainerOp):

    INPUT_MOUNT_BASE_PATH = "/input"
    NAME = "atpftst-extractor"
    DOCKER_IMG = config_dict.get_value(NAME)
    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "1Gi"
    MEMORY_LIMIT = "3Gi"
    VOLUME_NAME = "atpftst-mapr-mount"

    @classmethod
    def create(cls, bagfile_guid, flow_run_guid, input_file_path,
               input_mount_sub_path, pvc_name, user_id, vin, test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_file_path: path to the bagfile in the pod
        :param input_mount_sub_path: path to mount within input volume
        :param pvc_name: name of the pvc to mount
        :param user_id: step will be executed with this user id
        :param vin: of the vehicle from which the processed bagfile originates
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the AtpftstExtractor operation
        """
        step = AtpftstExtractorOp() \
            .set_retry(RETRY_NUM) \
            .add_pod_label(name=PIPELINE_POD_LABEL, value=sanitize_label(cls.NAME)) \
            .add_pod_label(name=BAGFILE_POD_LABEL, value=sanitize_label(bagfile_guid)) \
            .add_pod_label(name=VIN_POD_LABEL, value=sanitize_label(vin)) \
            .add_pod_label(name=FILE_NAME_POD_LABEL, value=sanitize_label(extract_file_name_from_path(input_file_path))) \
            .set_security_context(V1SecurityContext(run_as_user=user_id))

        if test_mode:
            step.add_pod_label(name=TESTING_POD_LABEL, value='true')

        step.add_env_variable(V1EnvVar(name="BAGFILE_PATH", value=input_file_path)) \
            .add_env_variable(V1EnvVar(name="BAGFILE_GUID", value=bagfile_guid)) \
            .add_env_variable(V1EnvVar(name="USER", value=str(user_id))) \
            .add_env_variable(V1EnvVar(name="METADATA_API_BASE_URL", value=METADATA_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="SEARCH_API_BASE_URL", value=SEARCH_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="APPINSIGHTS_INSTRUMENTATIONKEY", value=APPINSIGHTS_KEY)) \
            .add_env_variable(V1EnvVar(name="DANF_ENV", value=DANF_ENV)) \
            .add_env_variable(V1EnvVar(name="HTTP_PROXY", value=HTTP_PROXY)) \
            .add_env_variable(V1EnvVar(name="HTTPS_PROXY", value=HTTPS_PROXY)) \
            .add_env_variable(V1EnvVar(name="no_proxy", value=NO_PROXY)) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_ID, value=tracing.get_propagation_vars().get(ENV_TRACE_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_SPAN_ID, value=tracing.get_propagation_vars().get(ENV_SPAN_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_OPTIONS, value=tracing.get_propagation_vars().get(ENV_TRACE_OPTIONS))) \
            .add_env_variable(V1EnvVar(name="AUTH", value=AUTH)) \
            .add_env_variable(V1EnvVar(name="AUTH_TOKEN_URL", value=AUTH_TOKEN_URL)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_AUDIENCE", value=AUTH_DATAHANDLER_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_ID", value=AUTH_DATAHANDLER_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_SECRET", value=AUTH_DATAHANDLER_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_AUDIENCE", value=AUTH_METADATA_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_ID", value=AUTH_METADATA_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_SECRET", value=AUTH_METADATA_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_AUDIENCE", value=AUTH_SEARCH_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_ID", value=AUTH_SEARCH_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_SECRET", value=AUTH_SEARCH_CLIENT_CRED_SECRET))

        step.add_resource_request(CPU_RESOURCE_TITLE, cls.CPU_REQUEST) \
            .add_resource_request(MEMORY_RESOURCE_TITLE, cls.MEMORY_REQUEST) \
            .add_resource_limit(CPU_RESOURCE_TITLE, cls.CPU_LIMIT) \
            .add_resource_limit(MEMORY_RESOURCE_TITLE, cls.MEMORY_LIMIT)

        step.add_volume(get_pvc_volume(cls.VOLUME_NAME, pvc_name, read_only=True)) \
            .add_volume_mount(V1VolumeMount(
                mount_path=cls.INPUT_MOUNT_BASE_PATH,
                sub_path=input_mount_sub_path,
                name=cls.VOLUME_NAME))

        return step

    def __init__(self):
        super().__init__(name=self.NAME, image=self.DOCKER_IMG)
