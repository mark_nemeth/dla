import {connect} from 'react-redux';
import {OverviewPage} from './OverviewPage';
import {fetchFileByGuid, loadFlowRunsFromApi, fetchFileByBagPath, getGPSdata, cleanMapComponentData} from '../../redux/Overview/OverviewActions';
import {FileTypes} from '../../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import {isObjectValid} from '../../utils/helperFunctions';

function mapStateToProps(state, ownProps) {
  let fileType;
  if (ownProps.location.pathname.includes('childbagfile')) {
    fileType = FileTypes.CHILDBAGFILE;
  } else if (ownProps.location.pathname.includes('alwaysonfile')) {
    fileType = FileTypes.ALWAYSONFILE;
  } else if (ownProps.location.pathname.includes('originalfile')) {
    fileType = FileTypes.ORIGINALFILE;
  } else {
    fileType = FileTypes.DRIVE;
  }

  const error = state.error || false;
  const errorMessage = state.errorMessage || '';
  let overviewFile;
  if (fileType === FileTypes.ORIGINALFILE || fileType === FileTypes.ALWAYSONFILE || fileType === FileTypes.DRIVE) {
    overviewFile = (isObjectValid(state.overview.bagfile)) ? state.overview.bagfile : null;
  } else {
    overviewFile = (isObjectValid(state.overview.childbagfiles)) ? state.overview.childbagfiles[0] : null;
  }
  const childbagfiles = state.overview.childbagfiles || []; // TODO can be removed when map refactor
  return {fileType, overviewFile, childbagfiles, error, errorMessage};
}

export default connect(
  mapStateToProps,
  {fetchFileByGuid, loadFlowRunsFromApi, fetchFileByBagPath, getGPSdata, cleanMapComponentData}
)(OverviewPage);