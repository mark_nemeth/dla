import React from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ByVIDFilter from './FilterElements/ByVIDFilter';
import ByDurationFilter from './FilterElements/ByDurationFilter';
import ByDateFilter from './FilterElements/ByDateFilter';
import BySizeFilter from './FilterElements/BySizeFilter';
import ByTopicFilter from './FilterElements/ByTopicFilter';
import ByStatusFilter from './FilterElements/ByStatusFilter';
import ByEventFilter from './FilterElements/ByEventFilter';
import ByDriveTypeFilter from './FilterElements/ByDriveTypeFilter';
import ByVelocityFilter from './FeatureFilterElements/ByVelocityFilter';
import ByAccelerationFilter from './FeatureFilterElements/ByAccelerationFilter';
import ByJerkFilter from './FeatureFilterElements/ByJerkFilter';
import ByEngagementStateFilter from "./FeatureFilterElements/ByEngagementStateFilter";
import ByLaneChangeFilter from "./FeatureFilterElements/ByLaneChangeFilter";
import ByDriverFilter from "./FilterElements/ByDriverFilter";
import ByOperatorFilter from "./FilterElements/ByOperatorFilter";
import ByDrivenByValueFilter from "./FilterElements/ByDrivenByValueFilter";
import ByHardwareReleaseFilter from "./FilterElements/ByHardwareReleaseFilter";
import {FileTypes} from './FilterElements/ByFileFilter/ByFileFilter';
import featureToggle from '../../config/featureToggleHelper';
import './Sidebar.scss';

export function Sidebar(props) {

  function resetFilters() {
    props.resetFilters();
    props.applyFilters();
  }

  function onClickApplyFilters() {
    props.applyFilters();
  }

  return (
      (props.fileType !== FileTypes.DRIVE || featureToggle.showDriveFilters) ?
          <div className="sidebar">
            <div className="scrollable-filters">
              <div className="sidebar-title">Filter</div>
              <ByVIDFilter/>
              <ByDateFilter/>
              <ByDurationFilter/>
              {(featureToggle.showBagfileStatusFilter && props.fileType !== FileTypes.CHILDBAGFILE && props.fileType !== FileTypes.DRIVE) &&
              <ByStatusFilter/>}
              {(featureToggle.showDriveTypeFilter && props.fileType !== FileTypes.CHILDBAGFILE) && <ByDriveTypeFilter/>}
              {props.fileType !== FileTypes.DRIVE && <BySizeFilter/>}
              {props.fileType !== FileTypes.DRIVE && <ByTopicFilter/>}
              {(featureToggle.showEventFilter && props.fileType === FileTypes.CHILDBAGFILE) && <ByEventFilter/>}
              {(props.fileType === FileTypes.DRIVE && <ByDriverFilter/>)}
              {(props.fileType === FileTypes.DRIVE && <ByOperatorFilter/>)}
              {(props.fileType === FileTypes.DRIVE && <ByDrivenByValueFilter/>)}
              {(props.fileType === FileTypes.DRIVE && <ByHardwareReleaseFilter/>)}
              {(featureToggle.showFeatureFilter && props.fileType === FileTypes.ALWAYSONFILE) &&
              <div>
                <div className="sidebar-title">Feature Filter</div>
                <ByVelocityFilter/>
                <ByAccelerationFilter/>
                <ByJerkFilter/>
                <ByLaneChangeFilter/>
                <ByEngagementStateFilter/>
              </div>}
            </div>
            <ButtonGroup className="sidebar-footer">
              <Button className="sidebar-reset-button" onClick={resetFilters}>Reset</Button>
              <Button className="sidebar-apply-button" onClick={onClickApplyFilters}>Apply</Button>
            </ButtonGroup>
          </div>
          :
          <div className="sidebar">
            <div className="sidebar-title">No Filters</div>
          </div>
  );
}