import rosbag
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import json
import pickle
import argparse
import logging
import math
from pyproj import Proj, transform

import warnings
warnings.simplefilter('ignore', np.RankWarning)
np.seterr(divide='ignore', invalid='ignore')

import plotly.graph_objects as go
mapbox_access_token = 'pk.eyJ1IjoiYWJoaXJ1cGtncCIsImEiOiJjazBwc3BpMzcwMTdsM2htbHJndTFtb204In0.sSm1F8rW1cm_0LNtGxIKrg'

import plotly.io as pio
pio.renderers.default = "browser"

class GeometricFeatureExtractor:
    '''Feature extractor for geometric features from ego track'''
    
    # class attributes
    ecef_model = Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    lla_model = Proj(proj='latlong', ellps='WGS84', datum='WGS84', radians=False)
    @staticmethod
    def lon_lat_from_ecef_proj(x_ecef, y_ecef, z_ecef):
        '''Transform ecef to lon lat using pyproj, returns (lon, lat)'''
        lon, lat, alt = transform(GeometricFeatureExtractor.ecef_model, GeometricFeatureExtractor.lla_model, *(x_ecef, y_ecef, z_ecef))
        return lon, lat
    
    
    def __init__(self, bag_path, guid=None, sampling_interval=20, coef_cutoff=700, window_size=100, smoothing_window=10):
        self.bag_path = bag_path
        self.bag = self._get_bag_object()
        self.Lon = []
        self.Lat = []
        self.T = []
        self.Turn = []
        self.T_turn = []
        self.Turnind = []
        self.Velo_x = []
        self.Veloxind = []
        self.Accel_x = []
        self.Accelxind = []
        self.Accel_y = []
        self.Accelyind = []
        self.Steer_angle = []
        self.Steerind = []
        self.Yaw = []
        self.Yawind = []
        
        self.topic_dict = {
            'loc': '/localization/LocalizationService/0/LocDataInterface',
            'egostate': '/cal/CalService/0/EgoVehStateInterface',
#             'gps': '/cal/CalService/0/GpsInfoInterface'
                          }
        self.readers_dict = {
            '/localization/LocalizationService/0/LocDataInterface': self.read_loc,
            '/cal/CalService/0/EgoVehStateInterface' : self.read_states,
#             '/cal/CalService/0/GpsInfoInterface': self.read_gps
        }
        self.bag_start = self.bag.get_start_time()
        self.bag_end = self.bag.get_end_time()
        self.check_bag_for_topics()
        self.ingest_bag()
        self.ego_track = self.get_ego_track()
        self.lat_bar, self.lon_bar, self.T_bar = self.ego_track.mean(axis=0)[0:3]
        self.sampling_interval = sampling_interval
        self.coef_cutoff = coef_cutoff
        self.window_size = window_size
        self.smoothing_window = smoothing_window
        self.ego_track_geom_features = self.generate_geom_features(sampling_interval=self.sampling_interval,
                                                                   coef_cutoff=self.coef_cutoff,
                                                                   window_size=self.window_size,
                                                                   smoothing_window=self.smoothing_window)        

    
    def _get_bag_object(self):
        """gets bag object from bag path"""
        return rosbag.Bag(self.bag_path, 'r')
    
    def check_bag_for_topics(self):
        """Generic checker for topics"""
        types_and_topics_in_bag = self.bag.get_type_and_topic_info()
        topics_in_bag = types_and_topics_in_bag.topics.keys()
        for topic in self.topic_dict.values():
            if topic not in topics_in_bag:
                raise Exception('Topic not found: ', topic)
    
    def read_loc(self, msg):
        ecef = np.matrix(msg.loc_state[0].ecef_T_front_axle).reshape(4, 4)
        lon, lat = self.lon_lat_from_ecef_proj(ecef[0, 3]/ecef[3, 3], ecef[1, 3]/ecef[3, 3], ecef[2, 3]/ecef[3, 3])
        self.Lat.append(lat)
        self.Lon.append(lon)
        self.T.append(msg.header.timestamp.s + msg.header.timestamp.ns*1e-9 - self.bag_start)

    def read_states(self, msg):
        #TODO:convert turn ind enum
        #turn_ind_enum = {0:'OFF', 1:'LEFT', 2:'RIGHT', 3:'HAZARD'}
        #self.Turn.append(turn_ind_enum[int(msg.indicator_state)])
        self.Turn.append(msg.indicator_state)
        self.Velo_x.append(msg.velocity_x)
        self.Accel_x.append(msg.acceleration_x)
        self.Accel_y.append(msg.acceleration_y)
        self.Yaw.append(msg.yaw_rate)
        self.Steer_angle.append(msg.vehicle_steer_angle)
        self.T_turn.append(msg.header.timestamp.s + msg.header.timestamp.ns*1e-9 - self.bag_start)
            
    def read_gps(self, msg):
        if (msg.header.timestamp.s + msg.header.timestamp.ns*1e-9 - self.bag_start) >= 0:
            self.Lat_gps.append(msg.latitude)
            self.Lon_gps.append(msg.longitude)
            self.T_gps.append(msg.header.timestamp.s + msg.header.timestamp.ns*1e-9 - self.bag_start)            
        
    def get_ego_track(self):
        for i in self.T:
            idx = np.searchsorted(self.T_turn, i, side="left")
            self.Turnind.append(self.Turn[idx])
            self.Veloxind.append(self.Velo_x[idx])
            self.Accelxind.append(self.Accel_x[idx])
            self.Accelyind.append(self.Accel_y[idx])
            self.Yawind.append(self.Yaw[idx])
            self.Steerind.append(self.Steer_angle[idx])
        ego_track = pd.DataFrame(
            {'Lon': self.Lon,
             'Lat': self.Lat,
             'T': self.T,
             'Turn_Ind': self.Turnind, #0: OFF, 1:LEFT, 2:RIGHT, 3:HAZARD
             'Velocity_X': self.Veloxind,
             'Accel_X': self.Accelxind,
             'Accel_Y': self.Accelyind,
             'Yaw_Rate': self.Yawind,
             'Steer_Angle': self.Steerind
            })
        return ego_track
    
    def get_ego_states(self):

        turn_indicator = pd.DataFrame(
            {}
        )
        return turn_indicator

    def ingest_bag(self):
        for topic, message, timestamp in self.bag.read_messages(topics=self.topic_dict.values()):
            if (timestamp.to_sec() - self.bag_start) >= 0:
                self.readers_dict[topic](message)

    def generate_geom_features(self, sampling_interval=20, coef_cutoff=700, window_size=100, smoothing_window=10):
        """
        Calculates curvature based features from ego trajectory
        Parameters
        ----------
        sampling_interval: int
        coef_cutoff: int
        window_size: int
        smoothing_window: int

        Returns
        -------
        pd.DataFrame() object with columns as ('Lon', 'Lat', 'T', 'Turn_Ind', 'Velocity_X', 'Accel_X', 'Accel_Y', 'Yaw_Rate', 'Steer_Angle', 
        'd2y_dx2', 'd2y_dx2_sm', 'coef0', 'coef1', 'coef2', 'Turn', 'Turn_Index', 'Turn_Type')
        Each row indicates one sampling interval, with features.
        """
        ego_track_data = self.ego_track.copy()
        ego_track_data['orig_index'] = range(ego_track_data.shape[0])
        ego_track_sampled = ego_track_data.iloc[:ego_track_data.shape[0] - window_size]
        ego_track_sampled = ego_track_sampled[ego_track_data['orig_index'] % sampling_interval == 0].copy()
        ego_track_sampled['Lon_sm'] = ego_track_sampled['Lon'].rolling(smoothing_window).mean()
        ego_track_sampled['Lat_sm'] = ego_track_sampled['Lat'].rolling(smoothing_window).mean()

        ego_track_sampled['dy_dx'] = np.gradient(ego_track_sampled.Lat, ego_track_sampled.Lon)
        ego_track_sampled['d2y_dx2'] = np.log(
            np.absolute(np.gradient(ego_track_sampled['dy_dx'], ego_track_sampled.Lon)))

        ego_track_sampled['dy_dx_sm'] = np.gradient(ego_track_sampled.Lat_sm, ego_track_sampled.Lon_sm)
        ego_track_sampled['d2y_dx2_sm'] = np.log(
            np.absolute(np.gradient(ego_track_sampled['dy_dx_sm'], ego_track_sampled.Lon_sm)))
        ego_track_sampled['coef0_polyfit'] = None
        ego_track_sampled['coef1_polyfit'] = None
        ego_track_sampled['coef2_polyfit'] = None
        ego_track_sampled['coef0_lstsq'] = None
        ego_track_sampled['coef2_lstsq'] = None
        # pdb.set_trace()
        for window_index, _ in ego_track_sampled.iterrows():
            ego_window = ego_track_data.loc[range(window_index, window_index + window_size)].copy()
            # pdb.set_trace()
            try:
                # Change np.polyfit to np.linalg
                coef2_polyfit, coef1_polyfit, coef0_polyfit = np.polyfit(ego_window['Lon'], ego_window['Lat'], 2)
                # These two lines approximate the old polyfit
                #XX = np.vstack((ego_window['Lon'] ** 2, ego_window['Lon'], np.ones_like(ego_window['Lon']))).T
                #coef2, coef1, coef0 = np.linalg.lstsq(XX, ego_window['Lat'], len(ego_window['Lat'])*2e-16)[0]
                # The next lines use only the square and bias coefficients
                XX = np.vstack((ego_window['Lon'] ** 2, np.ones_like(ego_window['Lon']))).T
                coef2_lstsq, coef0_lstsq = np.linalg.lstsq(XX, ego_window['Lat'], len(ego_window['Lat'])*2e-16)[0]
            except Exception as err:
                print(err)
            ego_track_sampled.loc[window_index, ['coef0_polyfit']] = coef0_polyfit
            ego_track_sampled.loc[window_index, ['coef1_polyfit']] = coef1_polyfit
            ego_track_sampled.loc[window_index, ['coef2_polyfit']] = coef2_polyfit                
            ego_track_sampled.loc[window_index, ['coef0_lstsq']] = coef0_lstsq
            ego_track_sampled.loc[window_index, ['coef2_lstsq']] = coef2_lstsq
        ego_track_sampled['Turn'] = abs(ego_track_sampled['coef2_lstsq']) > coef_cutoff

        # logic for grouping turns for consecutive windows
        ego_track_sampled['reindex'] = range(ego_track_sampled.shape[0])
        ego_track_sampled['Turn_Index'] = None
        ego_track_sampled['Turn_Type'] = None
        turn_index = 0
        index_in_turn = 0
        current_turn_type = None
        for idx, row in ego_track_sampled[ego_track_sampled['Turn']].iterrows():
            current_turn = self.ego_track.loc[idx:idx + sampling_interval].copy()
            ego_track_sampled.loc[idx, 'Turn_Type'] = self.sign_turn(current_turn)

        for idx, row in ego_track_sampled[ego_track_sampled['Turn']].iterrows():
            if (row['reindex'] > index_in_turn) & (row['Turn_Type'] != current_turn_type):
                turn_index += 1
                index_in_turn = row['reindex']
                current_turn_type = row['Turn_Type']

            ego_track_sampled.loc[idx, 'Turn_Index'] = turn_index
            index_in_turn += 1

        return ego_track_sampled[['Lon', 'Lat', 'T', 'Turn_Ind', 'Velocity_X', 'Accel_X', 'Accel_Y', 'Yaw_Rate',
                                  'Steer_Angle', 'd2y_dx2', 'd2y_dx2_sm', 'coef0_polyfit', 'coef1_polyfit', 
                                  'coef2_polyfit','coef0_lstsq', 'coef2_lstsq' , 'Turn',
                                  'Turn_Index', 'Turn_Type']]

                
    def plot_ego_track(self, ego_track):
        fig = go.Figure(go.Scattermapbox(
        lat=ego_track.Lat,
        lon=ego_track.Lon,
        mode='markers',
        marker=go.scattermapbox.Marker(
            size=3,
            color='slategray',
        ),
        name='EGO',
        text=zip(ego_track['T']
        )))
        fig.update_layout(
            autosize=True,
            hovermode='closest',
            mapbox=go.layout.Mapbox(
                #accesstoken=mapbox_access_token,
                bearing=0,
                center=go.layout.mapbox.Center(
                    lat=self.lat_bar, # 37.323579, 
                    lon=self.lon_bar # -121.922051
                ),
                pitch=0,
                zoom=14.8,
                style='carto-positron'
            ))
        fig.show()
        
    @staticmethod
    def sign_turn(turn_data):
        point_a = (turn_data.iloc[0]['Lon'], turn_data.iloc[0]['Lat'])
        point_b = (turn_data.iloc[int(turn_data.shape[0]/2)]['Lon'], turn_data.iloc[int(turn_data.shape[0]/2)]['Lat'])
        point_c = (turn_data.iloc[turn_data.shape[0]-1]['Lon'], turn_data.iloc[turn_data.shape[0]-1]['Lat'])
        line_ab = (point_b[0] - point_a[0], point_b[1] - point_a[1])
        line_bc = (point_c[0] - point_b[0], point_c[1] - point_b[1])
        ab_cross_bc = (line_ab[0]*line_bc[1] - line_ab[1]*line_bc[0])
        if ab_cross_bc > 0:
            turn_dir = 'Left'
        else:
            turn_dir = 'Right'
        return turn_dir
    
    # detect turns v2, based on polyfit
    # TODO: remove coef_cutoff filter logic, this should be outside.
    def detect_turns_v2(self, sampling_interval=20, coef_cutoff=700, window_size=100):
        ego_track_data = self.ego_track.copy()
        ego_track_data['orig_index'] = range(ego_track_data.shape[0])
        ego_track_sampled = ego_track_data.iloc[:ego_track_data.shape[0]-window_size]
        ego_track_sampled = ego_track_sampled[ego_track_data['orig_index']%sampling_interval == 0].copy()
        ego_track_sampled['coef0'] = None
        ego_track_sampled['coef1'] = None
        ego_track_sampled['coef2'] = None
        #pdb.set_trace()
        for window_index, _ in ego_track_sampled.iterrows():
            ego_window = ego_track_data.loc[range(window_index, window_index+window_size)].copy()
            #pdb.set_trace()
            try:
                coef2, coef1, coef0 = np.polyfit(ego_window['Lon'], ego_window['Lat'], 2)
            except Exception as err:
                print(err)
            #pdb.set_trace()
            ego_track_sampled.loc[window_index, ['coef0']] = coef0
            ego_track_sampled.loc[window_index, ['coef1']] = coef1
            ego_track_sampled.loc[window_index, ['coef2']] = coef2
        ego_track_sampled['Turn'] = abs(ego_track_sampled['coef2']) > coef_cutoff

        # logic for grouping turns for consecutive windows
        ego_track_sampled['reindex'] = range(ego_track_sampled.shape[0])
        ego_track_sampled['Turn_Index'] = None
        ego_track_sampled['Turn_Type'] = None
        turn_index = 0
        index_in_turn = 0
        current_turn_type = None
        for idx, row in ego_track_sampled[ego_track_sampled['Turn']].iterrows():
            current_turn = self.ego_track.loc[idx:idx+sampling_interval].copy()
            ego_track_sampled.loc[idx, 'Turn_Type'] = self.sign_turn(current_turn)
        
        
        for idx, row in ego_track_sampled[ego_track_sampled['Turn']].iterrows():    
            if (row['reindex'] > index_in_turn) & (row['Turn_Type'] != current_turn_type):
                turn_index += 1
                index_in_turn = row['reindex']
                current_turn_type = row['Turn_Type']

            ego_track_sampled.loc[idx, 'Turn_Index'] = turn_index
            index_in_turn += 1

        return ego_track_sampled[['Lon', 'Lat', 'T', 'coef0', 'coef1', 'coef2', 'Turn', 'Turn_Index', 'Turn_Type']]
    