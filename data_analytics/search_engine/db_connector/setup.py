"""Setup file"""
from setuptools import setup, find_packages
from config.constants import VERSION


setup(name='DBConnector',
      version=VERSION,
      packages=find_packages(exclude=["tests"]),
      package_data={'': ['schema/*']},
      long_description=open('README.md', 'r').read(),
      long_description_content_type="text/markdown",
      test_suite="tests",
      install_requires=[
          'pymongo==3.12.2',
          'numpy>=1.16.1',
          'DANFUtils>=0.2.29',
          'coverage>=4.5.4'
      ])
