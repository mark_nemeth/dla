"""Silent Testing Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config.constants import *
from extractors.utils import timestamp2ms, aracom_timestamp2ms
from extractors.base_interpreter import Interpreter

BVA_ST = 10
SECONDS_BEFORE_SILENT_TESTING_EVENT = 1.5
SECONDS_AFTER_SILENT_TESTING_EVENT = 1.5
# see athena/stereo/interface/include/stereo_interfaces/stereo_silent_testing.h
SILENT_TESTING_TC_TO_SKIP = []
BREAK_AFTER_SILENT_TESTING_EVENT = 60


class SilentTestingInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.last_silent_testing_event_ts = None
        self.bva = BVA_ST

    def feed_new_msg(self, topic, msg, t):
        silent_testing = {}
        silent_testing["ts"] = aracom_timestamp2ms(msg.header.timestamp)

        objects = []
        for obj in msg.detected_objects_of_interest:
            object = {}

            # testcase number from test cataloque,
            # see athena/stereo/interface/include/stereo_interfaces/stereo_silent_testing.h
            object["testcase"] = obj.test_case

            # position: [top, left, bottom, right]
            object["position"] = [
                obj.position_image.top_left[0], obj.position_image.top_left[1],
                obj.position_image.bottom_right[0],
                obj.position_image.bottom_right[1]
            ]
            object["label"] = obj.label.id

            # append the object if the testcase should not be skipped:
            if object["testcase"] not in SILENT_TESTING_TC_TO_SKIP:
                objects.append(object)

        silent_testing["objects"] = objects
        if len(silent_testing["objects"]) > 0:
            if self.last_silent_testing_event_ts is None \
                    or silent_testing["ts"] - self.last_silent_testing_event_ts > BREAK_AFTER_SILENT_TESTING_EVENT * 1e3:
                silent_testing = self.calc_start_end_ts_snippet(silent_testing)
                self.event_list.append(silent_testing)
                self.last_silent_testing_event_ts = silent_testing["ts"]

        return self.event_list

    @staticmethod
    def calc_start_end_ts_snippet(event):
        event["start_ts"] = int(event["ts"] - (SECONDS_BEFORE_SILENT_TESTING_EVENT * 1e3))
        event["end_ts"] = int(event["ts"] + (SECONDS_AFTER_SILENT_TESTING_EVENT * 1e3))
        return event
