"""Regression tests for cache size duration time route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from tests.regressiontests.testutils import get_testdb_connector

base_url = 'http://localhost:8887'
cache_childbagfile_size_duration_time_url = base_url + '/cache/childbagfile-size-duration-time'

TEST_DATA_LINK = 'testdata'

raw_bagfile = {
    'version': '1.0',
    'guid': generate_deterministic_guid(),
    'link': TEST_DATA_LINK,
    'size': 10000,
    'start': 1400000000000,
    'end': 1400000500000,
    'duration': 500000,
}

raw_child_bagfile_1 = {
    'version': '1.0',
    'guid': generate_deterministic_guid(),
    'parent_guid': raw_bagfile['guid'],
    'link': TEST_DATA_LINK,
    'size': 200,
    'start': 1400000000030,
    'end': 1400000000040,
    'duration': 10
}

raw_child_bagfile_2 = {
    'version': '1.0',
    'guid': generate_deterministic_guid(),
    'parent_guid': raw_bagfile['guid'],
    'link': TEST_DATA_LINK,
    'size': 100,
    'start': 1400000000010,
    'end': 1400000000050,
    'duration': 80
}


def prepare_db():
    with get_testdb_connector() as con:
        con.db['bagfiles'].delete_many({'link': TEST_DATA_LINK})
        con.db['child_bagfiles'].delete_many({'link': TEST_DATA_LINK})
        con.db['documents'].delete_many({'type': {'$regex': '.*_cache'}})
        con.add_bagfiles([raw_bagfile])
        con.add_child_bagfiles([raw_child_bagfile_1, raw_child_bagfile_2])


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['bagfiles'].delete_many({'link': TEST_DATA_LINK})
        con.db['child_bagfiles'].delete_many({'link': TEST_DATA_LINK})
        con.db['documents'].delete_many({'type': {'$regex': '.*_cache'}})


class TestChildbagfileCacheSizeDurationPOST(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_post_cache_document_should_successfully_create(self):
        res = requests.post(url=cache_childbagfile_size_duration_time_url)
        self.assertEqual(200, res.status_code, 'post request is failed')

    def test_post_cache_document_should_correctly_compute_size_cache(self):
        requests.post(url=cache_childbagfile_size_duration_time_url)
        with get_testdb_connector() as con:
            cache = con.search_documents(query={'type': 'childbagfile_size_cache'})[0]
        self.assertEqual(raw_child_bagfile_2['size'], cache['min'])
        self.assertEqual(raw_child_bagfile_1['size'], cache['max'])

    def test_post_cache_document_should_correctly_compute_duration_cache(self):
        requests.post(url=cache_childbagfile_size_duration_time_url)
        with get_testdb_connector() as con:
            cache = con.search_documents(query={'type': 'childbagfile_duration_cache'})[0]
        self.assertEqual(raw_child_bagfile_1['duration'], cache['min'])
        self.assertEqual(raw_child_bagfile_2['duration'], cache['max'])

    def test_post_cache_document_should_correctly_compute_time_cache(self):
        requests.post(url=cache_childbagfile_size_duration_time_url)
        with get_testdb_connector() as con:
            cache = con.search_documents(query={'type': 'childbagfile_time_cache'})[0]
        self.assertEqual(raw_child_bagfile_2['start'], cache['min'])
        self.assertEqual(raw_child_bagfile_1['start'], cache['max'])


if __name__ == '__main__':
    unittest.main()
