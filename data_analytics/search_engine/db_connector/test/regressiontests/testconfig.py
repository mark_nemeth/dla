__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os

db_hostname = 'dla-cosmos-db-test.documents.azure.com'
db_port = 10256
db_name = 'metadata'
db_user = 'dla-cosmos-db-test'
db_key = os.getenv('db_key')
db_protocol = 'mongodb'