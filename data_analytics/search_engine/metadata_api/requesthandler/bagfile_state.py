"""Search Engine REST API caching topics route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from handlers.auth import auth
from handlers.bagfiles import BagFilesHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


class BagfileStateRequestHandler(BaseRequestHandler):

    _schema_guid = BaseRequestHandler.load_json_schema('guid.schema')
    _schema_post = BaseRequestHandler.load_json_schema('bagfile_metadata_state_event_post.schema')
    bagfile_handler = BagFilesHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'POST')

    @auth.requires_entitlements(["openid"])
    def post(self, bagfile_guid):
        tracing.start_task_in_context('bagfiles/state/post',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(bagfile_guid) + '"}',
                           self._schema_guid)
        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_post)
        self.bagfile_handler.add_state_event(bagfile_guid, data)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results="bagfile state events updated",
                             status_code=constants["REQ_STATUS_CODE_OK"])
