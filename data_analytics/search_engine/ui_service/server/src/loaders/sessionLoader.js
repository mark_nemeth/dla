import session from 'express-session';
import configHandlerSingleton from '../configHandler';
import storeProvider from '../models/storeProvider'
import getLogger from '../utils/logging';

const logger = getLogger(__filename)

export default (app) => {

  const sessionConfig = {
    secret: configHandlerSingleton.getSessionClientSecret(),
    cookie: {
      httpOnly: true,
      maxAge: 86400000, // 24 hours
      sameSite: 'none'
    },
    resave: false,
    saveUninitialized: false,
    store: storeProvider.getSessionStore()
  };

  sessionConfig.cookie.secure = !!(configHandlerSingleton.getDanfEnv());

  app.use(session(sessionConfig));
  logger.info('SESSIONS: enabled');

  return app;
}