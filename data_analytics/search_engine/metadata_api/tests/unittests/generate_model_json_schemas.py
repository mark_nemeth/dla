__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

from model.flow_runs import FlowRunStepRun, FlowRun, CreateFlowRunRequest, \
    UpdateFlowRunRequest, FlowRunStep
from model.flows import CreateFlowRequest, Flow


def _generate_and_print_schema(schema_cls):
    schema = schema_cls.schema_json(indent=2)
    print(f"### {schema_cls.__name__} - Schema ###")
    print(schema)
    print()


class TestFlows(unittest.TestCase):

    def test_generate_create_flow_request_schema(self):
        _generate_and_print_schema(CreateFlowRequest)

    def test_generate_flow_schema(self):
        _generate_and_print_schema(Flow)


class TestFlowRuns(unittest.TestCase):

    def test_generate_flow_run_step_run_schema(self):
        _generate_and_print_schema(FlowRunStepRun)

    def test_generate_flow_run_step_schema(self):
        _generate_and_print_schema(FlowRunStep)

    def test_generate_flow_run_schema(self):
        _generate_and_print_schema(FlowRun)

    def test_generate_create_flow_run_request_schema(self):
        _generate_and_print_schema(CreateFlowRunRequest)

    def test_generate_flow_run_update_request_schema(self):
        _generate_and_print_schema(UpdateFlowRunRequest)


if __name__ == '__main__':
    unittest.main()
