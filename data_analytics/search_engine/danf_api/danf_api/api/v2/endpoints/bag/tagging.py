"""Endpoint definitions for bag tagging."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import Optional
from uuid import UUID

from fastapi import APIRouter, Query, Path, Body, HTTPException, status

from .....crud.bag import tag_bag, find_bag_tags
from .....models.bagtag import ManyBagTags
from .....models.tag import Tag, AddTagResponse, TagFilterParams, TagQueryParams

router = APIRouter()


@router.get(
    "/metadata/projects/danf/bagtags",
    tags=["bags"],
    status_code=status.HTTP_200_OK,
    response_model=ManyBagTags,
)
async def search_tags(
    namespace: Optional[str] = Query(
        default=None,
        regex=TagFilterParams.__fields__["namespace"].field_info.regex,
        description="Namespace of the tag. E.g. `FOO.BAR`.",
    ),
    value: Optional[str] = Query(
        default=None,
        description="""Value of the tag.
        Allowed operators: **EQ** and **REGEX** groups.
        E.g. `BAZ.BOO` for an exact match, `re:^BOO.*` for using a regex.""",
    ),
    limit: int = Query(
        default=100,
        gt=0,
        le=1000,
        description="Pagination limit: the maximum number of results to return.",
    ),
    offset: int = Query(
        default=0,
        ge=0,
        description="""Pagination offset: the number of results to omit (from the start of the result set) when
        returning them.""",
    ),
) -> ManyBagTags:
    """Find bag tags matching a given criteria
    ---

    Filtering can be done on certain fields of the metadata such as namespace or value..

    Results are ordered by (namespace, value) in this order.

    Some parameters allow the usage of different operators on top of simply specifying a value (which usually means an
    implicit equality operator). E.g.: we can ask the endpoint to search for tags where the value matches a given regex.
    The API uses the RHS Colon operator syntax, which means that when a parameter allows special operators then
    the passed string should look like this: `OP:VALUE` (or just `VALUE` without an explicit operator for using the
    default one).

    **Note:** do not add a space between the colon and the value!

    The allowed operators for each parameter are detailed at the description of the exact parameter. These descriptions
    might refer to the following groups of operators:

    - **EQ**: no operator or the `eq` operator explicitly (both meaning equality)
    - **REGEX**: `re`
    """
    query_params = TagQueryParams(
        filter_params=TagFilterParams(
            namespace=namespace,
            value=value,
        ),
        order_by=[],
        limit=limit,
        offset=offset,
    )
    return await find_bag_tags(query_params)


@router.post(
    "/metadata/projects/danf/bags/{guid}/tags",
    tags=["bags"],
    status_code=status.HTTP_201_CREATED,
    response_model=AddTagResponse,
)
async def add_tag(
    guid: UUID = Path(
        default=...,
        description="The unique ID of the bag. E.g. `79a758cg-7Z24-4350-bc7a-d15364agha95`",
    ),
    tag: Tag = Body(
        default=...,
        description="""Tag to be added.
        A tag consist of a namespace and a value. Both of them are mandatory so tags always belong to a tag namespace.
        Both the namespace and the value MUST NOT contain colon (`:`) or forward slash (`/`).""",
    ),
) -> AddTagResponse:
    """Add a single tag to a bag
    ---

    Tags are great for creating datasets or adding custom labels. You can search bags based on the existing tags.

    **Note:** The response of this operation provides the ID of the newly created tag, which can be then used for
    searching bags based on the tag but you will also need this tag if you want to delete it. The ID is not cryptic,
    so you don't need to save it, you will be able to construct it yourself too. The format is `NAMESPACE::VALUE`

    **Further note:** If the tag already exists for the given bag, you will just get back the ID the same way as if it
    was created now. No error will occur.
    """
    try:
        await tag_bag(guid=guid, tag=tag)
    except KeyError as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=e.args[0])
    return AddTagResponse(tag_id=tag.id)


@router.delete(
    "/metadata/projects/danf/bags/{guid}/tags/{tag_id}",
    tags=["bags"],
    status_code=status.HTTP_200_OK,
)
async def delete_tag(
    guid: UUID = Path(
        default=...,
        description="The unique ID of the bag. E.g. `79a758cg-7Z24-4350-bc7a-d15364agha95`",
    ),
    tag_id: str = Path(
        default=...,
        regex=Tag.RE_ID,
        description="The ID of the tag to be removed (format: `namespace::value`). E.g. `FOO.BAR::baz.trainset`",
    ),
) -> None:
    """Delete a single tag from a bag
    ---

    For further details check the docs of the Add Tag endpoint.
    """
    try:
        await tag_bag(guid=guid, tag=Tag.from_id(tag_id), delete=True)
    except (KeyError, ValueError) as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=e.args[0])
