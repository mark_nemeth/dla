if [ "$ROS_DISTRO" != "melodic" ]; then
  if [ ! -d /opt/ros/melodic ]; then
    echo "ROS Melodic not found. Skipping..."
    exit 1
  fi
  . /opt/ros/melodic/setup.sh
fi

if [ -d build ]; then
  rm -r build
fi

if [ -d devel ]; then
  rm -r devel
fi

BuildType=Release
if [ "$1" == "Debug" ]; then
  BuildType=Debug
fi

echo "ROS version:     $ROS_DISTRO "
echo "ROS root:        $ROS_ROOT"
echo "Build type:      $BuildType"

catkin_make --force-cmake -G"Eclipse CDT4 - Unix Makefiles" --cmake-args -DCMAKE_BUILD_TYPE=$BuildType -DCMAKE_ECLIPSE_VERSION=4.2

. devel/setup.bash

awk -f $(rospack find mk)/eclipse.awk build/.project > build/.project_with_env && mv build/.project_with_env build/.project

sed -i -e "s/Project-$BuildType@/DLA-ROS-Melodic@/g" build/.project
