__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse
import os
import sys
from datetime import datetime


from azure.storage.blob.baseblobservice import BaseBlobService

locations = ['abt']
indecies = ['01', '02', '03', '04', '05']
storage_accounts = []
_, env = os.getenv("DANF_ENV").split("_")
for location in locations:
    for i in indecies:
        storage_accounts.append(f'baseinfra{location}fltr{i}sa{env}')


def check_sa_access(sa):
    """Check if token to access storage account works"""
    token = os.environ.get(f'access_key_{sa}')
    if not token:
        print(f"{sa}: TOKEN NOT FOUND")
        return

    api = BaseBlobService(account_name=sa, sas_token=token, protocol='https')
    try:
        api.exists(container_name='test')
    except Exception as e:
        print(f"{sa}: NO ACCESS - {e}")
    else:
        print(f"{sa}: OK")


if __name__ == '__main__':
    for sa in storage_accounts:
        check_sa_access(sa)
