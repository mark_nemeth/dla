import configureMockStore from 'redux-mock-store';
import * as actions from './OverviewActions';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configHandler from '../../config/configHandler';
import {getTestOriginalFileList, getTestChildBagfileList} from '../../setupTests';
import {insertString} from "../../assets/string/stringHelper";
import strings from "../../assets/string/default";
import {FileTypes} from "../../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const mock = new MockAdapter(axios);

describe('Overview Actions', () => {

  describe('fetchFileByGuid action', () => {

    const bagApiPath = "/api/v1/originalfiles";
    configHandler.getOriginalFileAPI = jest.fn(() => {
      return {url: bagApiPath}
    });
    const mockBagRequest = mock.onGet(bagApiPath);
    const childbagApiPath = "/api/v1/childbagfiles";
    configHandler.getChildBagfileAPI = jest.fn(() => {
      return {url: childbagApiPath}
    });
    const mockChildbagRequest = mock.onGet(childbagApiPath);

    describe('original files type', () => {

      it('creates fetchFileByGuidError action when fetching file failed', () => {
        const guid = '1337';
        const fileType = FileTypes.ORIGINALFILE;
        mockBagRequest.reply(502, {});
        mockChildbagRequest.reply(502, {});

        const expectedActions = [{
          type: actions.fetchFileByGuidError.type,
          payload: insertString(strings.no_bagfile_error, guid)
        }];

        const store = mockStore({overview: {}});
        return store.dispatch(actions.fetchFileByGuid(guid, fileType)).then(() => {
          // return of async actions
          expect(store.getActions()).toEqual(expectedActions)
        })
      });

      it('creates fetchFileByGuidSuccess action when fetching files from API succeeded without local store', () => {
        const guid = '1337';
        const fileType = FileTypes.ORIGINALFILE;
        const bagfilePayload = getTestOriginalFileList(true);
        bagfilePayload.originalFiles[0].guid = guid;
        const childbagfilePayload = getTestChildBagfileList(true);
        childbagfilePayload.childBagfiles[0].parent_guid = guid;

        mockBagRequest.reply(200, {results: bagfilePayload});
        mockChildbagRequest.reply(200, {results: childbagfilePayload});

        const expectedActions = [{
          type: actions.fetchFileByGuidSuccess.type,
          payload: {bagfile: bagfilePayload.originalFiles[0], childbagfiles: childbagfilePayload.childBagfiles}
        }];

        const store = mockStore({overview: {}});
        return store.dispatch(actions.fetchFileByGuid(guid, fileType)).then(() => {
          // return of async actions
          expect(store.getActions()).toEqual(expectedActions)
        })
      });

      it('creates fetchSingleBagfileByGuidSuccess action when fetching bagfile from local store succeeded', () => {
        const guid = '1337';
        const fileType = FileTypes.ORIGINALFILE;
        const bagfilePayload = getTestOriginalFileList(true);
        bagfilePayload.originalFiles[0].guid = guid;
        const childbagfilePayload = getTestChildBagfileList(true);
        childbagfilePayload.childBagfiles[0].parent_guid = guid;

        const expectedActions = [{
          type: actions.fetchFileByGuidSuccess.type,
          payload: {bagfile: bagfilePayload.originalFiles[0], childbagfiles: childbagfilePayload.childBagfiles}
        }];

        const store = mockStore({
          overview: {},
          originalFiles: bagfilePayload.originalFiles,
          childbagfiles: childbagfilePayload.childBagfiles
        });

        return store.dispatch(actions.fetchFileByGuid(guid, fileType)).then(() => {
          // return of async actions
          expect(store.getActions()).toEqual(expectedActions)
        })
      });
    });

    describe('childbagfile type', () => {

      it('creates fetchFileByGuidError action when fetching file failed', () => {
        const guid = '1337';
        const fileType = FileTypes.CHILDBAGFILE;
        mockBagRequest.reply(502, {});
        mockChildbagRequest.reply(502, {});

        const expectedActions = [{
          type: actions.fetchFileByGuidError.type,
          payload: insertString(strings.no_childbagfile_error, guid)
        }];

        const store = mockStore({overview: {}});
        return store.dispatch(actions.fetchFileByGuid(guid, fileType)).then(() => {
          // return of async actions
          expect(store.getActions()).toEqual(expectedActions)
        })
      });

      it('creates fetchFileByGuidSuccess action when fetching files from API succeeded without local store', () => {
        const guid = '1337';
        const fileType = FileTypes.CHILDBAGFILE;
        const bagfilePayload = getTestOriginalFileList(true);
        bagfilePayload.originalFiles[0].guid = guid;
        const childbagfilePayload = getTestChildBagfileList(true);
        childbagfilePayload.childBagfiles[0].parent_guid = guid;

        mockBagRequest.reply(200, {results: bagfilePayload});
        mockChildbagRequest.reply(200, {results: childbagfilePayload});

        const expectedActions = [{
          type: actions.fetchFileByGuidSuccess.type,
          payload: {bagfile: bagfilePayload.originalFiles[0], childbagfiles: childbagfilePayload.childBagfiles}
        }];

        const store = mockStore({overview: {}});
        return store.dispatch(actions.fetchFileByGuid(guid, fileType)).then(() => {
          // return of async actions
          expect(store.getActions()).toEqual(expectedActions)
        })
      });

      it('creates fetchSingleBagfileByGuidSuccess action when fetching bagfile from local store succeeded', () => {
        const guid = '1337';
        const fileType = FileTypes.CHILDBAGFILE;
        const bagfilePayload = getTestOriginalFileList(true);
        bagfilePayload.originalFiles[0].guid = guid;
        const childbagfilePayload = getTestChildBagfileList(true);
        childbagfilePayload.childBagfiles[0].parent_guid = guid;

        const expectedActions = [{
          type: actions.fetchFileByGuidSuccess.type,
          payload: {bagfile: bagfilePayload.originalFiles[0], childbagfiles: childbagfilePayload.childBagfiles}
        }];

        const store = mockStore({
          overview: {},
          originalFiles: bagfilePayload.originalFiles,
          childbagfiles: childbagfilePayload.childBagfiles
        });

        return store.dispatch(actions.fetchFileByGuid(guid, fileType)).then(() => {
          // return of async actions
          expect(store.getActions()).toEqual(expectedActions)
        })
      });
    });
  });
});