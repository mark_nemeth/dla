import os

RDA_PATH = "/mapr/738.mbc.de/var/mdm/robodrive/libraries/robotic-drive-analyzer-2.1.4-it4ad.jar"
os.environ[
    "PYSPARK_SUBMIT_ARGS"] = "--conf spark.sql.catalogImplementation=in-memory --jars=" + RDA_PATH + " pyspark-shell"

from dlalib._abstract_executor import *
from robodrive.RoboDriveAnalyserForRos import RoboDriveAnalyserForRos as rda


class LocalExecutor(AbstractExecutor):

    def __init__(self):
        super().__init__()

    def get_DF(self, files, topics):
        super().get_DF(files, topics)

        rda_obj = rda(",".join(files), "/tmp/indexes")
        df_dict = {}
        for topic in topics:
            df_dict[topic] = rda_obj.buildTopicByName(topic)
        return df_dict


class SparkSubmitExecutor(AbstractExecutor):

    def __init__(self):
        super().__init__()

    def get_DF(self, files, topics):
        pass
