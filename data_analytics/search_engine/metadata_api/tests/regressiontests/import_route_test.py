__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import random as rd
import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid
from DBConnector import DBConnector

from config.config_handler import config_dict

# credentials
hostname = config_dict.get_value('db_hostname')
port = config_dict.get_value('db_port')
db_name = config_dict.get_value('db_name')
user = config_dict.get_value('db_user')
key = config_dict.get_value('db_key')
protocol = config_dict.get_value('db_protocol')

# testdata
# can change the localhost into external ip:
base_url = 'http://localhost:8887'
import_url = base_url + '/import'

rd.seed("test")

raw_data_bagfile = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "file_name": "test.bag",
    "file_type": "ORIGINAL",
    "sequences": [generate_deterministic_guid(),generate_deterministic_guid()],
    "childbagfiles": [generate_deterministic_guid(),generate_deterministic_guid()],
    "tags": ["tag", "tag2"],
    "processing_priority": 1
}
raw_data_sequence = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "bagfile_guid": raw_data_bagfile['guid'],
    "start": 1475272800000,
    "end": 1475272800000,
    "type": "testdata",
    "index": 1,
    "extractor": "ext1"
}


class TestImportPost(unittest.TestCase):

    def setUp(self) -> None:
        rd.seed(0)
        connector = DBConnector(hostname, port, db_name, user, key, protocol)
        with connector as con:
            con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
            con.db['sequences'].delete_many({'type': raw_data_sequence['type']})

    def tearDown(self) -> None:
        connector = DBConnector(hostname, port, db_name, user, key, protocol)
        with connector as con:
            con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
            con.db['sequences'].delete_many({'type': raw_data_sequence['type']})

    @unittest.skip("test-case is broken")
    def test_import_new_data_post(self):
        bagfile_data = raw_data_bagfile.copy()
        sequence_data = raw_data_sequence.copy()
        data = {"bagfiles": [bagfile_data], "sequences": [sequence_data]}
        res = requests.post(url=import_url, json=data)
        self.assertEqual(200, res.status_code, 'Import post failed')

        connector = DBConnector(hostname, port, db_name, user, key, protocol)
        with connector as con:
            res = con.db['bagfiles'].find_one(filter={'guid': raw_data_bagfile["guid"]})
        del res["_id"]
        self.assertEqual(res, bagfile_data)

    @unittest.skip("test-case is broken")
    def test_import_existing_data_post(self):
        bagfile_data = raw_data_bagfile.copy()
        bagfile_data["vehicle_id_num"] = "V-200-200"
        sequence_data = raw_data_sequence.copy()
        data = {"bagfiles": [bagfile_data], "sequences": [sequence_data]}

        connector = DBConnector(hostname, port, db_name, user, key, protocol)
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])

        res = requests.post(url=import_url, json=data)
        self.assertEqual(200, res.status_code, 'Import post failed')

        with connector as con:
            res = con.db['bagfiles'].find_one(filter={'guid': raw_data_bagfile["guid"]})
        self.assertEqual(res["vehicle_id_num"], bagfile_data["vehicle_id_num"], "Upsert did not work")
