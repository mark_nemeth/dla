__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config import ExceptionHandler
from dla_converter import dla_conv
from utils.dir_utils import check_mapr


def bag2slice_fn(
        bagfile: str,
        topic: str = None,
        profile: str = None,
        start_relative: int = None,
        end_relative: int = None,
        start_time: int = None,
        end_time: int = None,
        apply_time_correct: bool = False
):
    check_mapr()
    if topic is not None:
        if start_relative is not None and end_relative is not None:
            _, bagfile_slice = dla_conv.sliceby_topic_relative_time(bagfile=bagfile,
                                                                    topic=topic,
                                                                    start_relative=start_relative,
                                                                    end_relative=end_relative)
        elif start_time is not None and end_time is not None:
            _, bagfile_slice = dla_conv.sliceby_topic_time(bagfile=bagfile,
                                                           topic=topic,
                                                           start_time=start_time,
                                                           end_time=end_time)
        else:
            _, bagfile_slice = dla_conv.sliceby_topic(bagfile=bagfile,
                                                      topic=topic)

    elif profile is not None:
        if start_relative is not None and end_relative is not None:
            _, bagfile_slice = dla_conv.sliceby_profile_relative_time(bagfile=bagfile,
                                                                      profile=profile,
                                                                      start_relative=start_relative,
                                                                      end_relative=end_relative,
                                                                      apply_time_correct=apply_time_correct)
        elif start_time is not None and end_time is not None:
            _, bagfile_slice = dla_conv.sliceby_profile_time(bagfile=bagfile,
                                                             profile=profile,
                                                             start_time=start_time,
                                                             end_time=end_time,
                                                             apply_time_correct=apply_time_correct)
        else:
            _, bagfile_slice = dla_conv.sliceby_profile(bagfile=bagfile,
                                                        profile=profile,
                                                        apply_time_correct=apply_time_correct)
    else:
        # Mutually exclude topic and profile
        raise ExceptionHandler('missing_topic_profile_err')

    bagfile_slice.seek(0)
    return bagfile_slice
