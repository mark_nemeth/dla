"""Regression tests for search flow route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from .testutils import get_testdb_connector

search_url = 'http://localhost:8889/search/flow'

flow_1 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "name": "danf-integration-test-flow1",
    "flow_version": "1",
    "kubeflow_experiment_id": "11111111-1111-1111-1111-111111111111",
    "steps": [
        {"name": "extractor",
         "version": "1.0"},
        {"name": "splitter",
         "version": "5.1"}
    ]
}

flow_2 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "name": "danf-integration-test-flow2",
    "flow_version": "1",
    "kubeflow_experiment_id": "11111111-1111-1111-1111-111111111111",
    "steps": [
        {"name": "extractor",
         "version": "1.0"},
        {"name": "splitter",
         "version": "5.1"}
    ]
}

flow_3 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "name": "danf-integration-test-flow3",
    "flow_version": "2",
    "kubeflow_experiment_id": "11111111-1111-1111-1111-111111111111",
    "steps": [
        {"name": "extractor",
         "version": "1.0"},
        {"name": "splitter",
         "version": "5.1"}
    ]
}

flow_4 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "name": "danf-integration-test-flow4",
    "flow_version": "1",
    "kubeflow_experiment_id": "11111111-1111-1111-1111-111111111111",
    "steps": [
        {"name": "extractor",
         "version": "1.0"},
        {"name": "splitter",
         "version": "5.1"}
    ]
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['flows'].delete_many(
            {'kubeflow_experiment_id': '11111111-1111-1111-1111-111111111111'}
        )
        con.add_flows([flow_1, flow_2, flow_3, flow_4])


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['flows'].delete_many(
            {'kubeflow_experiment_id': '11111111-1111-1111-1111-111111111111'}
        )


class TestSearchFlowsGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_guid(self):
        query_data = {'guid': flow_1['guid']}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_1], res_json)

    def test_search_by_name(self):
        query_data = {'name': flow_3['name']}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_3], res_json)

    def test_search_by_kubeflow_experiment_id(self):
        query_data = {'kubeflow_experiment_id': '11111111-1111-1111-1111-111111111111'}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_1, flow_2, flow_3, flow_4], res_json)

    def test_search_by_flow_version(self):
        query_data = {'flow_version': '1'}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_1, flow_2, flow_4], res_json)

    def test_search_limit_sort_ascending(self):
        query_data = {
            'kubeflow_experiment_id': '11111111-1111-1111-1111-111111111111',
            'limit': 2,
            'sortby': '+name'
        }
        
        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_1, flow_2], res_json)

    def test_search_limit_sort_descending(self):
        query_data = {
            'kubeflow_experiment_id': '11111111-1111-1111-1111-111111111111',
            'limit': 2,
            'sortby': '-name'
        }

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_4, flow_3], res_json)

    def test_search_offset_limit_sort(self):
        query_data = {
            'kubeflow_experiment_id': '11111111-1111-1111-1111-111111111111',
            'limit': 2,
            'offset': 1,
            'sortby': '+name'
        }

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_2, flow_3], res_json)

    def test_search_by_empty_data(self):
        query_data = {}
        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_1, flow_2, flow_3, flow_4], res_json)


if __name__ == '__main__':
    unittest.main()
