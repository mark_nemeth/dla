"""Configuration loading."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import os

API_VERSION = 2

LOGLEVEL = os.getenv("LOGLEVEL", "DEBUG").upper()

DB_URL = os.getenv("DB_URL")
if not DB_URL:
    _DB_HOST = os.getenv("DB_HOST", "localhost")
    _DB_PORT = os.getenv("DB_PORT", 27017)
    _DB_USER = os.getenv("DB_USER", "user")
    _DB_PASS = os.getenv("DB_PASS", "password")
    DB_URL = f"mongodb://{_DB_USER}:{_DB_PASS}@{_DB_HOST}:{_DB_PORT}/?ssl=true&replicaSet=globaldb"

DB_NAME = os.getenv("DB_NAME", "metadata")

FILECATALOG_COLLECTION_NAME = "filecatalog"
BAGFILE_COLLECTION_NAME = "bagfiles"
CHILDBAGFILE_COLLECTION_NAME = "child_bagfiles"
