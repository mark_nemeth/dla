import numpy as np
import pandas as pd
import shapely
import geopandas as gpd
import geopy
from geopy import distance

pivot_traj = pd.read_csv('../Data/geom_features_all_files_timetraj_pivot.csv')
map_objects=pd.read_csv('../Data/map_objects.csv')

pivot_traj = pivot_traj[['Filename', 'time_id', 'Lat_0', 'Lon_0']]

def calculate_distance_of_nearest_mapobj(ego_pivot_row):
    distance_from_point = map_objects_selected.apply(lambda _: geopy.distance.geodesic(
        distance.lonlat(*(ego_pivot_row['Lon_0'], ego_pivot_row['Lat_0'])),
        distance.lonlat(*(_['Lon'],_['Lat']))).meters, axis=1)
    return np.min(distance_from_point)

for mo_type in ['CROSSWALK', 'PARKINGSPACE']:
    map_objects_selected = map_objects[map_objects['object_type']==mo_type].copy()
    pivot_traj['Proximity_'+mo_type] = pivot_traj.apply(calculate_distance_of_nearest_mapobj, axis=1)

pivot_traj.to_csv('../Data/proximity_features_ego_pivot.csv')