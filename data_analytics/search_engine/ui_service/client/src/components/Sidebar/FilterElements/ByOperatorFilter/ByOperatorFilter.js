import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import './ByOperatorFilter.scss'

export function ByOperatorFilter(props) {

  function onOperatorSelect(operator) {
    if (props.selectedOperatorList.includes(operator)) props.filterByOperators(props.selectedOperatorList.filter(t => t !== operator));
    else props.filterByOperators(props.selectedOperatorList.concat(operator));
  }

  function resetOperatorList() {
    props.filterByOperators([]);
  }

  const showLoading = props.operatorList === undefined || props.operatorList.length <= 0;
  const subLabel = props.selectedOperatorList.length + ' selected';
  const active = props.selectedOperatorList.length > 0;

  const listItemComponent = (item) => (
      <label className="operator-list-item">
        <input type="checkbox" value={item}
               onChange={event => onOperatorSelect(event.target.value)}
               checked={props.selectedOperatorList.includes(item)}/>
        &nbsp; {item}
      </label>
  );

  return (
      <CollapseContainer label={active > 0 ? "Operator: " + subLabel : "Operator"}
                         active={active}
                         variant="list"
                         resetCallback={resetOperatorList}
      >
        <ScrollList
            showLoading={showLoading}
            itemList={props.operatorList}
            listItemComponent={listItemComponent}
        />
      </CollapseContainer>
  );
}