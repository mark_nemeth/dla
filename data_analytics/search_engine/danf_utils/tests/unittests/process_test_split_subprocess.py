__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

# This file is part of the tracing_unittest and will be executed via
# subprocess call

from random import random
from time import sleep

from DANFUtils.tracing import tracing

tracing.start_task('split-subprocess')
sleep(random() * 5)
tracing.end_task()

