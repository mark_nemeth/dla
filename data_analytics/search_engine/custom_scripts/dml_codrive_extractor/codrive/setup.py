from setuptools import setup, find_packages
 
setup(name='codrive',
      version="0.0.7",
      packages=find_packages(),
      test_suite="tests",
      install_requires=[
          'ContainerClient==0.6.102',
          'DANFTracingClient==0.1.19',
          'DANFUtils==0.2.29',
      ])
