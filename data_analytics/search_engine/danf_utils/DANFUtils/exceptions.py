"""DANF Exceptions"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.constants import *


class DANFBaseException(Exception):
    """Generic exception for DANF"""

    def __init__(self, component, msg):
        msg = "An error occured in {}: {}".format(component, msg)
        super(DANFBaseException, self).__init__(msg)
        self.source_component = component
        self.msg = msg

    def __str__(self):
        return self.msg


class GUIDNotFoundException(DANFBaseException):
    """Generic exception for DANF"""

    def __init__(self, component, guid, msg):
        msg_tmp = "GUID {} could not be found. ({})".format(guid, msg)
        super(GUIDNotFoundException, self).__init__(component, msg_tmp)


class JSONParseError(DANFBaseException):
    """
        Exception class for handling errors during JSON parsing
    """

    def __init__(self, component, msg):
        super(JSONParseError, self).__init__(component, msg)


class JSONValidationError(DANFBaseException):
    """
        Exception class for handling invalid json schema validation
    """

    def __init__(self, component, msg):
        super(JSONValidationError, self).__init__(component, msg)


class RESTException(DANFBaseException):
    """
        Exception class for handling connection problems with an RESTAPI
    """
    def __init__(self, component, request_type, url, status_code, msg):
        self.status_code = status_code
        self.request_type = request_type
        self.url = url
        if self.status_code == REQ_STATUS_CODE_BAD_REQ:
            code_str = 'BAD REQUEST 400'
        elif self.status_code == REQ_STATUS_CODE_SERVER_ERROR:
            code_str = 'SERVER ERROR 500'
        elif self.status_code == REQ_STATUS_CODE_NOT_FOUND:
            code_str = 'NOT FOUND 404'
        elif self.status_code == REST_CONNECTION_ERROR:
            code_str = 'Connection Error'
        elif self.status_code == REST_HTTP_ERROR:
            code_str = 'Http Error'
        elif self.status_code == REST_TIMEOUT_ERROR:
            code_str = 'Timeout'
        elif self.status_code == REST_TOO_MANY_REDIRECTS_ERROR:
            code_str = 'Too many redirects'
        else:
            code_str = 'unknown error'
        msg_tmp = "{} request to {} failed ({}): {}".format(request_type, url, code_str, msg)
        super(RESTException, self).__init__(component, msg_tmp)


class MissingEnvironmentVariableException(DANFBaseException):
    """
        Exception class for handling mandatory environment variables that have not been set
    """

    def __init__(self, component, variable_name):
        msg_tmp = "Environment variable {} must be set in order to use the {}".format(variable_name, component)
        super(MissingEnvironmentVariableException, self).__init__(component, msg_tmp)
