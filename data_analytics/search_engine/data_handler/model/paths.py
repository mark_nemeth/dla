__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import re
import logging
from typing import Optional
logger = logging.getLogger(__name__)


class BlobStoragePath:
    """A instance of this class points to a location in azure blob storage.

    Valid locations are:

    - A storage account, e.g. /dlastorage1sadev
    - A container, e.g. /dlastorage1sadev/2019-11-25
    - A file, e.g. /dlastorage1sadev/2019-11-25/file.bag
    """

    def __init__(self, path: str):
        """

        :param path:
        """
        self.storage_account, self.container, self.file = self._parse_path(
            path)
        self.path = self._build_str_representation()

    def __str__(self):
        return self.path

    def __repr__(self):
        return self.path

    def is_storage_account(self):
        return self.container is None

    def is_container(self):
        return self.file is None

    def is_file(self):
        return self.file is not None

    @staticmethod
    def _parse_path(path: str):
        path = path.strip('/')  # remove leading and trailing slashes
        segments = path.split('/')
        logger.debug(f"The path {path} has these segments {segments}")
        if len(segments) == 0:
            raise ValueError(f"Path {path} does not match")

        if len(segments) > 3:
            raise ValueError(f"Path {path} does not match")

        storage_account = segments[0]
        container = segments[1] if len(segments) > 1 else None
        file = segments[2] if len(segments) > 2 else None

        return storage_account, container, file

    def _build_str_representation(self):
        path = '/' + self.storage_account
        if self.container:
            path += '/' + self.container
        if self.file:
            path += '/' + self.file
        return path


class MapRPath:
    """A instance of this class points to a location within a MapR cluster.

    Valid locations are:

    - A day level folder of the input or buffer zone
      (only vin-path, no symlinked paths, e.g. byName, byVehicleId).
    - A file or directory within a day level folder of the input or buffer zone
      (only vin-path, no symlinked paths, e.g. byName, byVehicleId).
    - An arbitrary location within the tmp storage
    """

    INPUT_OR_BUFFER_ZONE_PATH_EXP = re.compile(
        r"^(/mapr/624\.mbc\.us)/data/(input|buffer)/rd/athena/08_robotaxi/plog/([^/]+)/(\d\d\d\d)/(\d\d?)/(\d\d?).*")

    TMP_ZONE_PATH_EXP = re.compile(r"^(/mapr/624\.mbc\.us)/tmp/.*")

    def __init__(self, path: str):
        self.path = path

        match_tmp = self.TMP_ZONE_PATH_EXP.match(path)
        match_buffer_input = self.INPUT_OR_BUFFER_ZONE_PATH_EXP.match(path)

        if not match_tmp and not match_buffer_input:
            raise ValueError(
                f"Path {path} matches none of the supported patterns for tmp, "
                f"input and buffer zone paths.")

        if match_tmp:
            self.zone: str = 'tmp'
            self.cluster: str = match_tmp.group(1)
            self.vin: Optional[str] = None
            self.year: Optional[str] = None
            self.month: Optional[str] = None
            self.day: Optional[str] = None

        if match_buffer_input:
            self.cluster: str = match_buffer_input.group(1)
            self.zone: str = match_buffer_input.group(2)
            self.vin: Optional[str] = match_buffer_input.group(3)
            self.year: Optional[str] = match_buffer_input.group(4)
            self.month: Optional[str] = match_buffer_input.group(5)
            self.day: Optional[str] = match_buffer_input.group(6)

        last_segment = path.split('/')[-1]
        self.file: str = last_segment if '.' in last_segment else None

        self.local_path: str = re.sub('/mapr/624\.mbc\.us', '/mapr', path)

    def __str__(self):
        return self.path

    def __repr__(self):
        return self.path
