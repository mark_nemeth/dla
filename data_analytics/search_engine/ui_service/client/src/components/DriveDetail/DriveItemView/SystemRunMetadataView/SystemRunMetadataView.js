import React from 'react';
import DateTimeEntry from "../../../AttributeFormatter/DateTime/DateTimeEntry";
import LabelEntry from "../../../AttributeFormatter/Label/LabelEntry";
import LabelContainer from "../../../Utils/LabelContainer/LabelContainer";
import {Row} from "react-bootstrap";
import Card from "react-bootstrap/Card";
import './SystemRunMetadataView.scss';
import ComponentListEntry from "../../../AttributeFormatter/ComponentListEntry/ComponentListEntry";

export function SystemRunMetadataView(props) {

  return (
      <Card className="system-run-information-section">
        <span className="system-run-information-section-label">
          System Run Information
        </span>
        <Row className='information-row'>
          <LabelContainer label="System Run Guid" variant="hundred-percent-card card-style-filled">
            <LabelEntry value={props.system_run.guid}/>
          </LabelContainer>
        </Row>
        <Row className='information-row'>
          <LabelContainer label="Start" variant="fifty-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.system_run.start}/>
          </LabelContainer>
          <LabelContainer label="End" variant="fifty-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.system_run.end}/>
          </LabelContainer>
        </Row>
        <Row className='information-row'>
          <LabelContainer label="Test Drive Type" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.system_run.test_drive_type}/>
          </LabelContainer>
          <LabelContainer label="Global Software Version" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.system_run.global_software_version}/>
          </LabelContainer>
        </Row>
        <LabelContainer label="Components" variant="card-style-filled">
          <ComponentListEntry components={props.system_run.components}/>
        </LabelContainer>
      </Card>
  )
}