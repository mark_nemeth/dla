#!/bin/bash
if [ $# != 2 ]
  then
    echo "Argument missing!"
    echo "You have to pass 2 arguments: app_name and docker_image_version!"
  else
    #login azure
    az acr login --name dlacontainerregistryacrdev

    app_name=$1
    docker_image_version=$2

    # build docker image
    docker build -t dlacontainerregistryacrdev.azurecr.io/$app_name:$docker_image_version  --no-cache --build-arg EXTRA_INDEX_URL=$index_url_with_access .

    #tag the image
    #docker tag $app_name:$docker_image_version dlacontainerregistryacrdev.azurecr.io/$app_name:docker_image_version

    # push docker image to registry
    docker push dlacontainerregistryacrdev.azurecr.io/$app_name:$docker_image_version

    # replace template + create / update deployment on Kubernetes cluster
    sed "s/latest/$docker_image_version/g" kubernetes/metadata_api_az_dev.yaml > kubernetes/metadata_api_az_dev_tmp.yaml
    sed "s/metadata-api:/${app_name}:/g" kubernetes/metadata_api_az_dev_tmp.yaml > kubernetes/metadata_api_az_dev_tmp_1.yaml
    
    kubectl  apply -f kubernetes/metadata_api_az_dev_tmp_1.yaml

    rm -f kubernetes/metadata_api_az_dev_tmp*.yaml
fi
