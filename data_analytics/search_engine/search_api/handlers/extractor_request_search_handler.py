"""Handler class for bagfile search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from handlers.db_data_handler import DBDataHandler


class ExtractorRequestSearchHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def create_search_kwargs(self, query_args):
        search_kwargs = {
            'query': None,
        }
        query = {}
        for key, value in query_args.items():
            # Query filters
            if key == 'bagfile_guid':
                query[key] = value[0]

        search_kwargs['query'] = query
        return search_kwargs

    def perform_search(self, **search_kwargs):
        if search_kwargs['query'].get("bagfile_guid"):
            return self.db_data_handler.get_extractor_requests_by_bagfile_guid(
                search_kwargs['query'].get("bagfile_guid"))
        else:
            return list()
