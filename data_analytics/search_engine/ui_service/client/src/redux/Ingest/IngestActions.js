import {createAction} from '@reduxjs/toolkit';
import configHandler from '../../config/configHandler';
import axios from 'axios';
import {insertString} from '../../assets/string/stringHelper';
import strings from '../../assets/string/default';

//region Load ingests Actions
export const fetchIngestsSuccess = createAction('ingests/fetchIngestsSuccess');
export const fetchIngestsError = createAction('ingests/fetchIngestsError');

export const setSelectedVehicle = createAction('ingests/setSelectedVehicle');

export const fetchLatestIngestsSuccess = createAction('ingests/fetchLatestIngestsSuccess');
export const fetchLatestIngestsError = createAction('ingests/fetchLatestIngestsError');
//endregion

//region Filter Actions
export const showLoading = createAction('filter/showLoading');

export function refresh() {
  return (dispatch, getState) => {
    dispatch(showLoading());
    return loadLatestIngests(dispatch);
  };

  // Load Ingests
  function loadLatestIngests(dispatch) {
    return axios.request(configHandler.getLatestIngestsAPI())
        .then(res => {
          dispatch(fetchLatestIngestsSuccess(res.data));
        })
        .catch(err => {
          handleError(dispatch, err, fetchLatestIngestsError(insertString(strings.connection_error)));
        });
  }
}

// Load Ingests
export function fetchIngests(query) {
  return (dispatch, getState) => {
    axios.request(configHandler.getIngestAPI(query))
        .then(res => {
          dispatch(fetchIngestsSuccess(res.data));
        })
        .catch(err => {
          handleError(dispatch, err, fetchIngestsError(insertString(strings.connection_error)));
        });
  }
}

//region Login
export function showLoginPage() {
  window.location.reload();
}

//endregion

function handleError(dispatch, err, errorTask) {
  console.log(err);
  if (err.response && err.response.status === 401) dispatch(showLoginPage());
  else dispatch(errorTask);
}