__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import uuid
from abc import ABC, abstractmethod
from typing import Any

from config.config_handler import config_dict
from model.exceptions import EventProcessingError
from model.paths import BlobStoragePath
from proxies.blob_storage import BlobStorageHandler


class BaseFlow(ABC):
    """Base class that should be implemented by all flows.

    All flows should provide a class method 'create' that will be used to
    initialize the flow. The following arguments will be passed to create in
    this order: the bagfile, the flow_run_guid, followed by arbitrary
    additional parameters defined by the individual flows.
    """

    @property
    @classmethod
    @abstractmethod
    def NAME(cls) -> str:
        """Name of the flow"""
        raise NotImplementedError

    @property
    @classmethod
    @abstractmethod
    def VERSION(cls) -> str:
        """Name of the flow"""
        raise NotImplementedError

    @classmethod
    def get_config(cls) -> dict:
        return config_dict.get_value('pipelines', {}).get(cls.NAME, {})

    def __init__(self, pipeline: str):
        self._pipeline = pipeline

    @property
    def pipeline(self) -> Any:
        """Kubeflow dsl.pipeline function"""
        return self._pipeline


def get_mount_id():
    return str(uuid.uuid4())


def configure_blobstorage_output_locations(bagfile: dict):
    input_file_path = BlobStoragePath(bagfile['link'])

    input_output_map = config_dict.get_value(
        'storage_account_input_output_mapping', {})
    output_storage_account = input_output_map.get(
        input_file_path.storage_account)

    if output_storage_account is None:
        raise EventProcessingError(f"No output storage account configured "
                                   f"for input account "
                                   f"{input_file_path.storage_account}")

    try:
        output_containers = BlobStorageHandler().create_output_containers(
            input_file_path.storage_account,
            output_storage_account,
            input_file_path.container,
            input_file_path.file,
            bagfile['guid'])
    except Exception as e:
        raise EventProcessingError(f"Error creating output containers for "
                                   f"bagfile {bagfile['guid']}") from e

    return output_storage_account, output_containers
