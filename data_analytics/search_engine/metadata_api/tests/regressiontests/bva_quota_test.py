__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from tests.regressiontests.testutils import get_testdb_connector

# testdata
base_url = 'http://localhost:8887'
quota_url = base_url + '/bva/quotas'

test_quota_document_active = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "active": True,
    "started_at": 1577971500002,
    "ended_at": 1577971500003,
    "quota_usage_per_account": {
        "workstream_id1": {
            "total": 10000,
            "used": 0
        }
    }
}

test_quota_document_inactive = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "active": False,
    "started_at": 1577971500001,
    "ended_at": 1577971500000,
    "quota_usage_per_account": {
        "workstream_id1": {
            "total": 10000,
            "used": 0
        }
    }
}

test_credentials = {
    "version": "0.0.1-testdata",
    "guid": generate_deterministic_guid(),
    "account_name": "workstream_id1",
    "account_key_hash": "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
    "available_quota": 20000,
}

header = {'X-account-key': 'test'}


def prepare_db():
    with get_testdb_connector() as con:
        con.db['bva_quotas'].delete_many(
            {'version': test_quota_document_active['version']})
        con.db['accounts'].delete_many(
            {'version': test_credentials['version']})


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['bva_quotas'].delete_many(
            {'version': test_quota_document_active['version']})
        con.db['accounts'].delete_many(
            {'version': test_credentials['version']})


class TestBvaGET(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_GET_quotas_without_workstream_key_should_be_400(
            self):
        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_active)

        res = requests.get(url=quota_url)
        self.assertEqual(400, res.status_code)

    @unittest.skip("test-case is broken")
    def test_GET_quotas_should_return_active_quota(self):
        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_active)
            con.db['accounts'].insert_one(test_credentials)

        res = requests.get(url=quota_url, headers=header)
        self.assertEqual(1, len(res.json()['results']))
        self.assertEqual(10000, res.json()['results'][0].get("total"))
        self.assertEqual(0, res.json()['results'][0].get("used"))

    @unittest.skip("test-case is broken")
    def test_GET_quotas_with_active_is_false_should_return_inactive_quotas(
            self):
        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_active)
            con.db['accounts'].insert_one(test_credentials)

        res = requests.get(url=quota_url, params={'active': 'false'},
                           headers=header)
        self.assertEqual(404, res.status_code)

        with get_testdb_connector() as con:
            con.db['bva_quotas'].insert_one(test_quota_document_inactive)

        res = requests.get(url=quota_url, params={'active': 'false'},
                           headers=header)
        self.assertEqual(1, len(res.json()['results']))
        self.assertEqual(10000, res.json()['results'][0].get("total"))
        self.assertEqual(0, res.json()['results'][0].get("used"))


class TestBvaPOST(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_POST_quotas_should_create_new_voting_round_and_close_current(self):
        with get_testdb_connector() as con:
            con.db['accounts'].insert_one(test_credentials)

        res = requests.post(url=quota_url)
        self.assertEqual(201, res.status_code)

        with get_testdb_connector() as con:
            quota = con.db['bva_quotas'].find_one({'active': True})

        res = requests.post(url=quota_url)
        self.assertEqual(201, res.status_code)

        with get_testdb_connector() as con:
            quota2 = con.db['bva_quotas'].find_one({'active': True})

        self.assertNotEqual(quota.get('guid'), quota2.get('guid'))


if __name__ == '__main__':
    unittest.main()
