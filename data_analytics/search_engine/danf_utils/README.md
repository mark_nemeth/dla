# DANFUtils

DANFUtils provides functionality that is required by many components of DANF.

## DANFUtils Modules / Features

- [Logging](DANFUtils/logging/README.md)
- [Tracing](DANFUtils/tracing/README.md)
- DANF environment support
- Shared exceptions and constants
- Generic utility functions


## Installation

### Automated Installation 
Requires update of pip configuration: https://athena.daimler.com/confluence/display/ATROB/How-to%3A+Setting+up+the+DANF+dev+environment)
```
pip install DANFUtils
```

### Manually building and installing wheel files

Install prerequisites
```
pip install --upgrade setuptools wheel
```

Generate `DANFUtils-${version}-py3-none-any.whl` file
```
python3 setup.py sdist bdist_wheel
```

Install local wheel file
```
pip install --upgrade dist/DANFUtils-${version}-py3-none-any.whl 

```

