"""Unittests for DBConnector"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

from DANFUtils.logging import logger

from DBConnector import DBConnector
from DBConnector.exceptions import InvalidQueryException
from . import testconfig_local

# increase log level for tests
logger.set_log_level('debug')

bag = {
    "guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "link": "/test/test/test/test/test/test/test",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "version": "1.0",
    "is_test_data": 1
}

feat_bucket_doc = {
    "version":
    "1.0",
    "guid":
    "22221111-aaaa-bbbb-cccc-999999999999",
    "bagfile_guid":
    bag["guid"],
    "start":
    1563262907000,
    "end":
    1563262911001,
    "start_index":
    60,
    "end_index":
    119,
    "tags": ["", ""],
    "features": [{
        "ts": 1563262907000,
        "vel": 0.4,
        "acc": 0.2,
        "eng_state": 5,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 5.1
    }, {
        "ts": 1563262908000,
        "vel": 0.5,
        "acc": 0.3,
        "eng_state": 11,
        "loc": {
            "type": "Point",
            "coordinates": [48.775244, 9.177595]
        },
        "altitude": 5.1
    }],
    "is_test_data":
    1
}


class TestBagFileIO(unittest.TestCase):
    def setUp(self):
        self.connector = DBConnector(testconfig_local.db_hostname,
                                     testconfig_local.db_port,
                                     testconfig_local.db_name,
                                     testconfig_local.db_user,
                                     testconfig_local.db_key,
                                     testconfig_local.db_protocol,
                                     ssl=False)
        self.cleanup_db()

    def tearDown(self):
        self.cleanup_db()

    def cleanup_db(self):
        with self.connector as con:
            con.db['bagfiles'].delete_many({"is_test_data": 1})
            con.db['feature_buckets'].delete_many({"is_test_data": 1})

    def test_write_update_read_delete_feature_buckets(self):

        with self.connector as con:
            # add
            con.add_bagfiles([bag])
            con.add_feature_buckets([feat_bucket_doc])

            # get
            res_feat_buckets = con.get_feature_buckets(
                [feat_bucket_doc["guid"]])[0]
            self.assertDictEqual(
                feat_bucket_doc, res_feat_buckets,
                'Write-read failed for single feat bucket doc!')

            # get by bag guid
            res_feat_buckets = con.get_feature_buckets_by_bagfile_guid(
                [bag["guid"]]).next()
            self.assertDictEqual(
                feat_bucket_doc, res_feat_buckets,
                'Write-read failed for single feature bucket doc!')

            # update single
            new_data = "new_data"
            con.update_feature_buckets([feat_bucket_doc["guid"]],
                                       [{
                                           "new_field": new_data,
                                           "is_test_data": 1
                                       }])
            res_feat_buckets = con.get_feature_buckets(
                [feat_bucket_doc["guid"]])[0]
            self.assertEqual(new_data, res_feat_buckets["new_field"],
                             'Update failed for single feature bucket!')

            # delete single
            con.delete_feature_buckets([feat_bucket_doc["guid"]])
            feature_count = con.get_feature_buckets([feat_bucket_doc["guid"]
                                                     ]).count()
            self.assertEqual(0, feature_count,
                             'Delete failed for single feature bucket!')

    def test_search_feature_buckets(self):

        with self.connector as con:
            # add
            con.add_bagfiles([bag])
            con.add_feature_buckets([feat_bucket_doc])
            # search by guid
            query_1 = {
                "$or": [{
                    "guid": feat_bucket_doc["guid"]
                }, {
                    "guid": "22221111-aaaa-bbbb-cccc-999999121212"
                }]
            }
            res_feat_bucket = con.search_feature_buckets(query_1,
                                                         limit=2,
                                                         offset=0,
                                                         sortby=[("guid", -1)
                                                                 ])[0]
            self.assertDictEqual(feat_bucket_doc, res_feat_bucket,
                                 'Search failed')

            query_1 = {
                "bagfile_guid": {
                    "$in": [
                        feat_bucket_doc["bagfile_guid"],
                        "22221111-aaaa-bbbb-cccc-999999121212"
                    ]
                }
            }
            res_feat_bucket = con.search_feature_buckets(query_1,
                                                         limit=2,
                                                         offset=0,
                                                         sortby=[("guid", -1)
                                                                 ])[0]
            self.assertDictEqual(feat_bucket_doc, res_feat_bucket,
                                 'Search failed')

            # search by single feature
            query = {"features": {"$elemMatch": {"acc": 0.3}}}
            res_feat_bucket = con.search_feature_buckets(query,
                                                         limit=2,
                                                         offset=0,
                                                         sortby=[("guid", -1)
                                                                 ])[0]
            self.assertDictEqual(feat_bucket_doc, res_feat_bucket,
                                 'Search failed')

            # search by single feature bucket with projection
            query = {"features": {"$elemMatch": {"acc": 0.3}}}
            proj = {"features.ts": 1, "features.acc": 1}
            res_feat_bucket = con.search_feature_buckets(query,
                                                         limit=2,
                                                         offset=0,
                                                         sortby=[("guid", -1)],
                                                         projection=proj)[0]
            self.assertDictEqual(
                {
                    "features": [{
                        'ts': 1563262907000,
                        'acc': 0.2
                    }, {
                        'ts': 1563262908000,
                        'acc': 0.3
                    }]
                }, res_feat_bucket, 'Search failed')

            # search by multiple feature buckets
            query = {
                "features": {
                    "$elemMatch": {
                        "acc": {
                            "$lt": 0.3
                        },
                        "eng_state": 5,
                        "altitude": {
                            "$lt": 50
                        }
                    }
                }
            }
            res_feat_bucket = con.search_feature_buckets(query,
                                                         limit=2,
                                                         offset=0,
                                                         sortby=[("guid", -1)
                                                                 ])[0]
            self.assertDictEqual(feat_bucket_doc, res_feat_bucket,
                                 'Search failed')

    def test_search_feature_buckets_invalid_query(self):

        with self.connector as con:
            # add
            con.add_bagfiles([bag])
            con.add_feature_buckets([feat_bucket_doc])

            query = {
                "$or": [{
                    "guid": feat_bucket_doc["guid"]
                }, {
                    "parent_guid": "22221111-aaaa-bbbb-cccc-999999121212"
                }]
            }
            with self.assertRaises(InvalidQueryException):
                res_feat = con.search_feature_buckets(query,
                                                      limit=2,
                                                      offset=0,
                                                      sortby=[("guid", -1)])[0]

    def test_search_feature_buckets_new_feature(self):

        with self.connector as con:
            # add
            con.add_bagfiles([bag])
            con.add_feature_buckets([feat_bucket_doc])

            query = {
                "features": {
                    "$elemMatch": {
                        "acc": {
                            "$lt": 0.3
                        },
                        "lane_change": "l"
                    }
                }
            }
            res_feat = con.search_feature_buckets(query,
                                                  limit=2,
                                                  offset=0,
                                                  sortby=[("guid", -1)])
