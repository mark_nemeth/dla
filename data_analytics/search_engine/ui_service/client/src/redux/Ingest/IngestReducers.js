import {createReducer} from '@reduxjs/toolkit';
import * as actions from './IngestActions';

export const IngestReducer = createReducer({}, {
  [actions.fetchLatestIngestsSuccess]: (state, action) => {
    state.error = false;
    state.latestIngests = action.payload;
    state.showLoading = false;
  },
  [actions.fetchLatestIngestsError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.latestIngests = [];
    state.showLoading = false;
  },
  [actions.fetchIngestsSuccess]: (state, action) => {
    state.error = false;
    state.ingests = action.payload;
    state.showLoading = false;
  },
  [actions.fetchIngestsError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.ingests = [];
    state.showLoading = false;
  },
  [actions.showLoading]: (state, action) => {
    state.showLoading = true;
  }
});