
__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse
import logging
import os
from collections import namedtuple

from ContainerClient import ContainerClient
from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.logging import configure_root_logger
from DANFUtils.tracing import tracing
from DANFUtils.utils import utils

from config.constants import DANF_VERSION_TAG
from TracingSidecar.kube_watcher import PodWatcher, \
    ContainerStateMachine, ContainerState

logger = logging.getLogger(__name__)


def _get_commandline_args():
    parser = argparse.ArgumentParser(
        description='Monitors and reports execution of a container')
    parser.add_argument('--container',
                        action='store',
                        required=True,
                        help='name of the container that should be monitored')
    parser.add_argument('--name',
                        action='store',
                        required=True,
                        help='name of the component, will be used as task '
                             'name for tracing and in state events')
    parser.add_argument('--version',
                        action='store',
                        required=True,
                        help='version of the monitored component')
    parser.add_argument('--bagfile',
                        action='store',
                        required=True,
                        help='guid of the bagfile that is processed')
    return parser.parse_args()


map_step_name_to_legacy_name = {
    'lidar-exporter': 'exporter-lidar',
    'cv1-exporter': 'exporter-cv1'
}


class ContainerStartCallback:
    def __init__(self, step_name):
        self.step_name = map_step_name_to_legacy_name.get(step_name, step_name)

    def __call__(self, waiting_state_details):
        tracing.start_task(self.step_name)


class ContainerStopCallback:

    map_status_to_legacy_status = {
        'SUCCESSFUL': 'SUCCESS',
        'FAILED': 'FAILED'
    }

    def __init__(
            self, step_name, version, bagfile_guid, flow_run_guid, started_at):
        self.step_name = step_name
        self.legacy_step_name = map_step_name_to_legacy_name.get(step_name, step_name)
        self.version = version
        self.bagfile_guid = bagfile_guid
        self.flow_run_guid = flow_run_guid
        self.started_at = started_at
        self.client = ContainerClient()

    def __call__(self, terminated_state_details):
        if not terminated_state_details:
            logger.warning(f"No state details available on "
                           f"container termination, assume container failed")
            tracing.end_task(success=False)
            self._submit_state_updates('FAILED', self.started_at)
            return

        if terminated_state_details.exit_code > 0:
            error_msg = (f"container failed with reason: "
                         f"{terminated_state_details.reason}")
            tracing.task_attribute("fatalError", error_msg)
            tracing.end_task(success=False)
            self._submit_state_updates('FAILED', self.started_at, error_msg)
            return

        tracing.end_task()
        self._submit_state_updates('SUCCESSFUL', self.started_at)

    def _submit_state_updates(self, status, started_at, message=None):
        run_update, state_update = self._build_state_updates(
            status, started_at, message)

        if self.flow_run_guid:
            self.client.add_flow_run_step_run(self.flow_run_guid,
                                              self.step_name,
                                              run_update)

        if self.bagfile_guid and self._should_submit_legacy_state():
            self.client.add_bagfile_state_event(self.bagfile_guid,
                                                state_update)

    def _build_state_updates(self, status, started_at, message=None):
        now = utils.get_timestamp()
        report = {}
        if message:
            report['message'] = message

        flow_update = {
            'started_at': started_at,
            'completed_at': now,
            'status': status,
            'report': report
        }
        state_update = {
            'name': self.legacy_step_name,
            'version': self.version,
            'time': now,
            'status': self.map_status_to_legacy_status[status],
            'report': report
        }
        return flow_update, state_update

    def _should_submit_legacy_state(self):
        # don't submit in azure because flows are run on childbagfiles
        return danf_env().platform != Platform.AZURE


def _get_env_vars():
    pod_name = os.environ.get('POD_NAME')
    namespace = os.environ.get('NAMESPACE')
    bagfile_guid = os.environ.get('BAGFILE_GUID')
    flow_run_guid = os.environ.get('FLOW_RUN_GUID')

    env_tuple = namedtuple(
        'EnvVars', ['pod_name', 'namespace', 'bagfile_guid', 'flow_run_guid'])
    return env_tuple(pod_name, namespace, bagfile_guid, flow_run_guid)


def main(name, version, target_container, bagfile_guid):
    started_at = utils.get_timestamp()
    env = _get_env_vars()

    logger.debug(
        f"sidecar config: target container {target_container} in pod"
        f"{env.pod_name} in namespace {env.namespace}")
    container_state_machine = ContainerStateMachine(target_container)
    watcher = PodWatcher(env.pod_name,
                         env.namespace,
                         pod_event_handler=container_state_machine)

    # register callbacks for state changes
    start_callback = ContainerStartCallback(name)
    container_state_machine.register_callback_on_state_change(
        ContainerState.WAITING, start_callback)

    stop_callback = ContainerStopCallback(
        name, version, bagfile_guid, env.flow_run_guid, started_at)
    container_state_machine.register_callback_on_state_change(
        ContainerState.TERMINATED, stop_callback)

    # start watching for events
    watcher.start()


if __name__ == '__main__':
    configure_root_logger(
        log_level='INFO',
        enable_azure_log_aggregation=danf_env().platform != Platform.LOCAL,
    )
    logger.setLevel('INFO')
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")

    args = _get_commandline_args()
    main(args.name, args.version, args.container, args.bagfile)
