"""ExtractorRequest Collection Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import json
from typing import List

from pymongo import UpdateOne
from pymongo.errors import BulkWriteError

from DANFUtils.utils import validate_json
from DANFUtils.logging import logger
from DANFUtils.exceptions import DANFBaseException
from DBConnector.exceptions import IndexOverwriteException, BulkOperationException, InvalidQueryException, \
    InvalidConfigurationException
from DBConnector.utils import calculate_request_size, MAXIMUM_REQUEST_BATCH_SIZE


class ExtractorRequestCollectionHandler:
    """
    ExtractorRequestCollectionHandler
    """

    # Extractor Request index
    EXT_REQUESTS_COLLECTION_NAME = 'extractor_requests'
    EXT_REQUESTS_INDEX_NAME = 'extractor_requests_guid_index'
    EXT_REQUESTS_INDEX_FIELD_GUID = 'guid'
    EXT_REQUESTS_BAGFILE_GUID = 'bagfile_guid'

    def __init__(self, db):
        self.db = db

        col = self.db[self.EXT_REQUESTS_COLLECTION_NAME]
        col.create_index(self.EXT_REQUESTS_INDEX_FIELD_GUID,
                         unique=True,
                         name=self.EXT_REQUESTS_INDEX_NAME)
        try:
            base_path = os.path.abspath(os.path.dirname(__file__))
            schema_file = f"{base_path}/../schema/extractor_request_query.schema"

            with open(schema_file) as f:
                schema_query = f.read()
        except (IOError, OSError) as e:
            error_message = f'Could not read extractor request query schema - Error: {e}'
            logger.exception(error_message)
            raise InvalidConfigurationException(error_message)
        try:
            self.schema_query = json.loads(schema_query)
        except ValueError as e:
            error_message = f'Bad schema: unable to parse document query schema - Error: {e}'
            logger.exception(error_message)
            raise InvalidConfigurationException(error_message)

    def add(self, requests):
        assert isinstance(
            requests, list), 'Data inserted into db has to be a list of dicts'
        for request in requests:
            assert isinstance(
                request,
                dict), 'Data inserted into db has to be a list of dicts'
            assert 'guid' in request, 'Documents inserted into db must have guid key'
        col = self.db[self.EXT_REQUESTS_COLLECTION_NAME]

        try:
            num_docs = len(requests)
            request_size = calculate_request_size(requests)
            logger.debug(
                f"Adding {num_docs} documents to collection "
                f"\"{self.EXT_REQUESTS_COLLECTION_NAME}\" (Request size: {request_size:.2f} kb)"
            )
            col.insert_many(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

        for x in requests:
            del x['_id']

    def get(self, guids):
        col = self.db[self.EXT_REQUESTS_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        request = {self.EXT_REQUESTS_INDEX_FIELD_GUID: {"$in": guids}}
        request_size = calculate_request_size(request)
        logger.debug("Retrieving " + str(len(guids)) +
                     " documents from collection \"" +
                     self.EXT_REQUESTS_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {'_id': 0})

    def search(self, query=None, limit=100, offset=0, sortby="_id"):
        col = self.db[self.EXT_REQUESTS_COLLECTION_NAME]
        assert isinstance(query, dict), 'Expected dict query'
        assert isinstance(limit, int), 'Expected int limit'
        assert isinstance(offset, int), 'Expected int offset'
        assert isinstance(sortby, (str, list)), 'Expected str or list sortby'
        try:
            request = validate_json(json.dumps(query), self.schema_query)
        except DANFBaseException as e:
            error_message = f'Bad request: invalid search query {query} - Error: {e}'
            logger.exception(error_message)
            raise InvalidQueryException(error_message)
        request_size = calculate_request_size(request)
        logger.debug(
            f"Searching documents in collection \""
            f"{self.EXT_REQUESTS_COLLECTION_NAME} \" (Request size: {request_size:.2f} kb)"
        )
        return col.find(request, {
            '_id': 0
        }).sort(sortby).skip(offset).limit(limit)

    def update(self, guids, request_data, create_if_missing=False):
        col = self.db[self.EXT_REQUESTS_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        assert isinstance(request_data,
                          list), 'Expected a list of request dicts.'
        assert all(isinstance(x, dict)
                   for x in request_data), 'Expected a list of request dicts.'
        assert isinstance(create_if_missing,
                          bool), 'create_if_missing has to be of type bool'
        assert len(guids) == len(
            request_data
        ), 'List guids and list request_data should be of same length'
        for d in request_data:
            if self.EXT_REQUESTS_INDEX_FIELD_GUID in d.keys():
                msg = 'Overwriting index fields is not permitted!'
                logger.error(msg)
                raise IndexOverwriteException(msg)

        requests = []
        for g, d in zip(guids, request_data):
            # Add GUID to dict and create update request
            requests.append(
                UpdateOne(
                    {self.EXT_REQUESTS_INDEX_FIELD_GUID: g},
                    {"$set": {
                        **d, self.EXT_REQUESTS_INDEX_FIELD_GUID: g
                    }},
                    upsert=create_if_missing))

        try:
            # TODO: Compute request size
            logger.debug("Updating  " + str(len(guids)) +
                         " documents in collection \"" +
                         self.EXT_REQUESTS_COLLECTION_NAME + "\"")
            col.bulk_write(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

    def delete(self, guids):
        col = self.db[self.EXT_REQUESTS_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'

        num_docs = len(guids)
        if num_docs > 0:
            request = {'guid': {"$in": guids}}
            request_size = calculate_request_size(request)
            logger.debug("Deleting  " + str(num_docs) +
                         " documents from collection \"" +
                         self.EXT_REQUESTS_COLLECTION_NAME +
                         "\" (Request size: {:.2f}".format(request_size) +
                         " kb)")
            if num_docs <= MAXIMUM_REQUEST_BATCH_SIZE:
                col.delete_many(request)
            else:
                # batch delete

                logger.debug(
                    "Number of delete operations exceeds maximum for single request."
                    + "Performing document deletion in batches of size " +
                    str(MAXIMUM_REQUEST_BATCH_SIZE))
                i = 0
                while i < num_docs:
                    start_id = i
                    i += MAXIMUM_REQUEST_BATCH_SIZE
                    end_id = i
                    if end_id >= num_docs:
                        end_id = num_docs
                    logger.debug("Deleting batch objects " + str(end_id) +
                                 " of " + str(num_docs))
                    request = {'guid': {"$in": guids[start_id:end_id]}}
                    col.delete_many(request)

    def get_by_bagfile_guids(self, bagfile_guids):
        col = self.db[self.EXT_REQUESTS_COLLECTION_NAME]
        assert isinstance(bagfile_guids,
                          list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in bagfile_guids), 'Expected a list of guid strings.'
        request = {self.EXT_REQUESTS_BAGFILE_GUID: {"$in": bagfile_guids}}
        request_size = calculate_request_size(request)
        logger.debug("Retrieving " + str(len(bagfile_guids)) +
                     " documents from collection \"" +
                     self.EXT_REQUESTS_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")

        return col.find(request, {'_id': 0})

    def aggregate(self, pipeline: List[dict]):
        col = self.db[self.EXT_REQUESTS_COLLECTION_NAME]
        assert isinstance(pipeline, list), 'Expected list pipeline'
        request_size = calculate_request_size(pipeline)
        logger.debug(
            f"Aggregating documents in collection \""
            f"{self.EXT_REQUESTS_COLLECTION_NAME} \" (Request size: {request_size:.2f} kb)"
        )
        return col.aggregate(pipeline)
