__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import logging
from kubegc.config.config_handler import config_dict
from kubegc.removers import PodRemover
from kubegc.removers import WorkflowRemover
from config.constants import DANF_VERSION_TAG

logger = logging.getLogger(__name__)

def main():
    namespace = config_dict.get_value('TARGET_NAMESPACE')
    pod_threshold = int(config_dict.get_value('POD_THRESHOLD'))
    workflow_threshold = int(config_dict.get_value('SUCCEDED_WORKFLOW_THRESHOLD'))

    pod_remover = PodRemover(namespace, pod_threshold)
    workflow_remover = WorkflowRemover(namespace, workflow_threshold)

    pod_remover.remove_pods()
    workflow_remover.remove_workflows()


if __name__ == '__main__':
    logger.setLevel('INFO')
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    main()
