"""Exceptions"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.exceptions import DANFBaseException


class DBConnectorBaseException(DANFBaseException):
    """
        DB Connector Base Exception class
    """

    def __init__(self, msg):
        super().__init__('DBConnector', msg)


class IndexOverwriteException(DBConnectorBaseException):
    """
        Exception class for handling the attempted overwrite of guids
    """

    def __init__(self, msg):
        super().__init__(msg)


class BulkOperationException(DBConnectorBaseException):
    """
        Exception class for handling errors that appear during bulk operations (e.g. bulk insert, bulk update)
    """

    def __init__(self, msg):
        super().__init__(msg)


class InvalidQueryException(DBConnectorBaseException):
    """
        Exception class for handling invalid query errors
    """

    def __init__(self, msg):
        super().__init__(msg)


class InvalidConfigurationException(DBConnectorBaseException):
    """
        Exception class for handling invalid configuration errors
    """

    def __init__(self, msg):
        super().__init__(msg)
