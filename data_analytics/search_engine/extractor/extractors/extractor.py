"""Extractor"""

__copyright__ = '''
COPYRIGHT: (c) 2019-2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from typing import List
from pathlib import Path
import rosbag

from extractors.base_interpreter import Interpreter
from extractors.interpreters.annotation import AnnotationInterpreter
from extractors.interpreters.disengagement import DisengagementInterpreter
from extractors.interpreters.fallback import FallbackInterpreter
from extractors.interpreters.dynamic_stop_point import DynamicStopPointInterpreter
from extractors.interpreters.gps import GpsInterpreter
from extractors.interpreters.logbook import LogbookInterpreter
from extractors.interpreters.guest_logbook import GuestLogbookInterpreter
from extractors.interpreters.silent_testing_cv1 import SilentTestingInterpreter
from extractors.interpreters.silent_testing_lidar import \
    LidarSilentTestingInterpreter
from extractors.interpreters.silent_testing_traffic_lights import \
    TrafficLightsSilentTestingInterpreter
from extractors.interpreters.silent_testing_mrc import MrcSilentTestingInterpreter
from extractors.interpreters.stereo_cnn import StereoCnnInterpreter
from model.extracted_event import ExtractedEvent
from subprocess import check_call
from DANFUtils.danf_environment import danf_env, Stage

logger = logging.getLogger(__name__)


AVAILABLE_INTERPRETERS = {
    "disengagement": DisengagementInterpreter,
    "fallback": FallbackInterpreter,
    "dynamic_stop_point": DynamicStopPointInterpreter,
    "logbook": LogbookInterpreter,
    "guest_logbook": GuestLogbookInterpreter,
    "silent_testing_cv1": SilentTestingInterpreter,
    "gps": GpsInterpreter,
    "stereo_cnn": StereoCnnInterpreter,
    "silent_testing_lidar": LidarSilentTestingInterpreter,
    "silent_testing_traffic_lights": TrafficLightsSilentTestingInterpreter,
    "silent_testing_mrc": MrcSilentTestingInterpreter,
    "annotation": AnnotationInterpreter
}

TMP_STRIPPED_BAG = "tmp_stripped.bag"
TMP_WHITELIST = "tmp_whitelist.txt"

class Extractor:

    def __init__(self, file_path, file_guid, args, bagfile_metadata_dict):
        self.file_path = file_path
        self.file_guid = file_guid
        self.interpreters_to_load = args if args is not None else []
        self.results = None
        self.bag = None
        self.bagfile_metadata_dict = bagfile_metadata_dict
        self.start_ts = bagfile_metadata_dict.get("start")
        self.end_ts = bagfile_metadata_dict.get("end")
        self.topics_list = bagfile_metadata_dict.get("topics")

    def execute_dla_converter(self, topics_whitelist):
        with open(TMP_WHITELIST, 'w') as f:
            for item in topics_whitelist:
                f.write("%s\n" % item)

        # for debugging, print to screen what is in the whitelist
        check_call(["cat", TMP_WHITELIST])

        check_call([
            "dla_converter/dla_converter",
            "--in", self.file_path,
            "--whitelist", TMP_WHITELIST,
            "--out", TMP_STRIPPED_BAG
        ])

    def execute(self):
        """Execute the extractor
        """
        # check if the bagfile is unindexed, if yes, stop the processing
        if self.bagfile_metadata_dict.get("indexed") is False:
            logger.warning(f"Bagfile {self.file_path} is unindexed! "
                           f"Exiting processing.")
            return

        # extract needed topics and run interpreters
        try:
            self.results = self._extract_bag_file_topics()
        except Exception as e:
            raise RuntimeError(f"Error extracting topics from file "
                               f"{self.file_path}: {e}") from e

    def _load_interpreters(self) -> List[Interpreter]:
        """Compares the list of interpreters to be loaded (specified through
        cli argument) to the list of available interpreters and loads the
        existing ones.

        :return: List of interpreters
        """
        existing_interpreters = []
        for to_load in self.interpreters_to_load:
            if to_load in AVAILABLE_INTERPRETERS:
                # TODO: remove after successful integration test
                if to_load == "silent_testing_mrc" and danf_env().stage == Stage.PROD:
                    logger.info(f"silent testing mrc interpreter was skipped")
                    continue
                existing_interpreters.append(to_load)
            else:
                logger.warning(f"Unknown interpreter: {to_load}! "
                               f"It will be ignored for further processing.")

        logger.info(f"The following interpreters will be loaded: "
                    f"{existing_interpreters}")
        return list(map(lambda x: AVAILABLE_INTERPRETERS[x](x), existing_interpreters))

    def _extract_bag_file_topics(self) -> List[ExtractedEvent]:
        """Main loop over the messages in the bagfile.

        :return: List of extracted events with metadata
        """
        # load interpreters
        interpreters = self._load_interpreters()

        # include all topics to be extracted
        topics_whitelist = set()
        topics = dict()
        for interpreter in interpreters:
            try:
                interpreter_topics = interpreter.get_topics_and_handler(self.start_ts, self.topics_list)
                topics[interpreter.name] = interpreter_topics
                if interpreter_topics:
                    topics_whitelist.update(interpreter_topics)
            except Exception as exp:
                logger.exception(f"Topic cannot be set for {interpreter.name}, "
                                 f"check config! Error: {exp}")

        logger.info(f"Topics whitelist: {topics_whitelist}")

        # RUN DLA_CONVERTER HERE:
        self.execute_dla_converter(topics_whitelist)
        if Path(TMP_STRIPPED_BAG).is_file():
            self.bag = rosbag.Bag(TMP_STRIPPED_BAG, 'r')

            # main loop over all filtered messages
            for topic, message, t in self.bag.read_messages(topics=topics_whitelist):
                for interpreter in interpreters:
                    if topics[interpreter.name]:
                        if topic in topics[interpreter.name]:
                            interpreter.feed_new_msg(topic, message, t)
        else:
            logger.warning("The bagfile does not contain any whitelisted topic!")

        extracted_events = []
        for interpreter in interpreters:
            # additional operation for disengagement interpreter
            if interpreter.name == "disengagement":
                interpreter.join_disengagement_with_logrequest()
            # generate metadata list
            extracted_events.extend(interpreter.get_extracted_events(self.file_guid, self.start_ts, self.end_ts))
        return extracted_events

    def close_extractor(self):
        """close the bagfile

        :return: None
        """
        if hasattr(self, 'bag') and self.bag:
            self.bag.close()

    def __del__(self):
        try:
            self.close_extractor()
        except Exception:
            logger.exception(f"can not close file! path: {self.file_path}")
