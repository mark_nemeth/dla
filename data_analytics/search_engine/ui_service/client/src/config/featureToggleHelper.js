class FeatureToggleHelper {

  // App
  showVersion = false;

  // HomePage
  showActionBar = false;
  showSelectColumn = false;
  showPathColumn = false;
  showPathActions = true;

  // Filter
  showEventFilter = true;
  showBagfileStatusFilter = true;
  showDriveTypeFilter = true;
  showFeatureFilter = true;
  showDriveFilters = true;

  // Overview
  showOverviewPage = true;
  showLeafLetMap = true;
  showTagInformation = false;

  // Ingest
  showIngestPage = true;
}

const featureToggle = new FeatureToggleHelper();
export default featureToggle;