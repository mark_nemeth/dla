import fire
import logging

from scanner import scan
from config.constants import DANF_VERSION_TAG

if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel('INFO')
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    logger.info("################################")
    logger.info("######## data crawler scanner ########")
    logger.info("################################")
    fire.Fire(scan)
