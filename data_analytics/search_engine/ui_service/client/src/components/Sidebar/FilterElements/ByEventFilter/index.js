import {connect} from 'react-redux';
import {filterByEvents} from '../../../../redux/Home/HomeActions';
import {ByEventFilter} from './ByEventFilter';

const eventMap = new Map([
  ['Disengagement', 'metadata.disengagement'],
  ['Silent Testing', 'metadata.silent_testing_cv1'],
  ['Fallback', 'metadata.fallback'],
  ['Logbook', 'metadata.logbook'],
  ['Annotation', 'metadata.annotation'],
]);

function mapStateToProps(state, ownProps) {
  const eventList = state.eventList || eventMap;
  const selectedEventList = state.filters.event || [];
  return {eventList, selectedEventList};
}

export default connect(
  mapStateToProps,
  {filterByEvents}
)(ByEventFilter);