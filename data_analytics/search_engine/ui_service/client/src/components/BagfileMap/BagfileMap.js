import React, { Component } from 'react';
import LeafLetMap from './LeafLetMap';
import BagfileTimeline from '../BagfileTimeline';
import { RouteData } from './Models/RouteModels';

/**
 * TODO:
 * access secret from .env file/ environment variables!
 */

class BagfileMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      route: props.route,
      selectedChildbagfileGuid: this.selectedChildbagfileGuid,
      selectedEvent: this.selectedEvent
    }
  }

  render() {
    return (
      <>
        <LeafLetMap
          lat={this.props.lat}
          lng={this.props.lng}
          zoom={this.props.zoom}
          route={new RouteData(this.props.route)}
          onEventHover={this.props.onEventHover}
          onChildBagfileClick={this.props.onChildBagfileClick}
          onChildBagfileHover={this.props.onChildBagfileHover}
          selectedChildbagfileGuid={this.props.selectedChildbagfileGuid}
          highlightSelectedChildBagfile={this.props.highlightSelectedChildBagfile}
          highlightSelectedEvent={this.props.highlightSelectedEvent}
          selectedEvent={this.props.selectedEvent}
        />
        <BagfileTimeline
          route={new RouteData(this.props.route)}
        />
      </>
    );
  }
}

export default BagfileMap;