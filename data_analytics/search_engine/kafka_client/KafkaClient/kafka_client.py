__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


from confluent_kafka import Consumer, KafkaError, KafkaException
from atexit import register
from DANFUtils.logging import logger


class KafkaClient:
    """
    KafkaClient class for connecting the message producing
    """
    def __init__(self, **conf):
        self.conf = conf
        self.consumer = Consumer(**self.conf)
        register(_cleanup, self.consumer)

    def subscribe(self, topics):
        self.consumer.subscribe(topics)

    def poll_msg(self, timeout):
        """
        expose the client poll
        :return:
        """
        return self.consumer.poll(timeout)

def _cleanup(consumer):
    consumer.close()
    logger.info("the kafka consumer is safely closed")


class KafkaClientError(KafkaError):
    logger.error("The consumer client encounters an error")
    pass


class KafkaClientException(KafkaException):
    logger.exception("The consumer client encounters an exception")
    pass
