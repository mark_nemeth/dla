__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import time

import cv2
import numpy as np
from PIL import Image, ImageFont, ImageDraw
from cv_bridge import CvBridge
from rosbag import Bag

from config import ExceptionHandler
from config import config_handler as configurator
from image_processing.bagfile_utils import get_msg_type, get_overlay, get_test_case


def extract_images(bagfile,
                   overlay=False,
                   overlay_id=-1,
                   max_messages=-1,
                   ):
    try:
        pybag = Bag(bagfile.name)
    except Exception as e:
        raise ExceptionHandler(code='dla_converter_err', debug_msg=e)

    messages = 0
    images = []
    for topic, msg, t in pybag.read_messages():
        if get_msg_type(msg) == '_sensor_msgs__CompressedImage':
            try:
                img = CvBridge().compressed_imgmsg_to_cv2(msg, desired_encoding='rgb8')
            except cv2.error as e:
                raise ExceptionHandler(code='failed_convert_img_err', debug_code=e.code, debug_cmd=e.err, debug_msg=e.msg)

            size = (len(img[0]), len(img))
            data = Image.frombytes('RGB', size, img)
            images.append([data, t])

        elif get_msg_type(msg) == '_stereo__stereo__RawImage16':
            # image dimensions are used to generate overlay msg, leave these be if changing logic
            image_height = get_height(msg.meta)
            image_width = get_width(msg.meta)
            size = (image_width, image_height)
            val = msg.data
            pixel_type = np.uint16

            # BEGIN SLOW PART - these two lines take nearly 0.1s, significantly slowing down video retrieval
            values = np.array(val, dtype=pixel_type)
            corr = 255.0 / max(val)
            # END SLOW PART

            int_val = values * corr
            imgarr = np.reshape(int_val, (image_height, image_width)).astype('uint8')
            data = Image.frombytes('L', size, imgarr)
            data = data.convert('RGB')
            images.append([data, t])

        # Add timestamp overlay to image
        if configurator.get_value('timestamp_overlay_app'):

            font_type = configurator.get_value('font_type_app')
            fontsize = configurator.get_value('font_size_app')
            padding = configurator.get_value('padding_app')
            txt_color = tuple(configurator.get_value('txt_color_app'))

            # Build the message
            timestamp = float('{}.{}'.format(images[-1][1].secs, images[-1][1].nsecs))
            human_readable = time.strftime('%Z %Y-%m-%d %H:%M:%S', time.localtime(timestamp))
            human_readable += '.{}'.format(str(images[-1][1].nsecs)[:2])
            message = '{0:.2f}\n{1}'.format(timestamp, human_readable)

            # Find lower left corner
            width, height = images[-1][0].size
            textpos = height - (fontsize * len(message.split('\n')) + padding)

            # Draw overlay
            img = images[-1][0]
            draw = ImageDraw.Draw(img)
            font = ImageFont.truetype(font_type, fontsize)
            draw.text((padding, textpos), message, txt_color, font=font)
            images[-1][0] = img

        # Add overlay to image
        if overlay:
            # get target position TODO - very inefficient! PoC only
            img = images[-1][0]
            draw = ImageDraw.Draw(img, 'RGBA')
            timestamp = float('{}.{}'.format(images[-1][1].secs, images[-1][1].nsecs))

            def print_string(message, x, y, fontsize=configurator.get_value('font_size_app'), text_color=tuple(configurator.get_value('txt_color_app'))):
                font_type = configurator.get_value('font_type_app')
                font = ImageFont.truetype(font_type, fontsize)

                longest_line = max(message.split('\n'), key=len)
                text_width, text_height = font.getsize(longest_line)
                text_height *= 1.2

                # Calculate offset to ensure message does not go off screen
                x0 = x
                y0 = y - text_height * len(message.split('\n')) - 1
                x1 = x0 + text_width
                y1 = y0 + text_height * len(message.split('\n')) - 1

                if x1 > image_width:
                    x0 = image_width - text_width
                    x1 = image_width
                if y1 > image_height:
                    y0 = image_height - text_height
                    y1 = image_height
                elif y0 < 0:
                    y0 = 0
                    y1 = text_height * len(message.split('\n'))

                # Draw a black rectangle to serve as text backdrop
                draw.rectangle([x0, y0, x1, y1], fill=(0, 0, 0, 150))

                # Draw the text
                draw.text((x0, y0), message, text_color, font=font)

            try:
                data = get_overlay(bagfile, timestamp, overlay_id)

                if data:
                    i = 0
                    for i in range(0, len(data)):
                        # Draw the selection rectangle
                        draw.rectangle([data[i]['upper_left'][0],
                                        data[i]['upper_left'][1],
                                        data[i]['bottom_right'][0],
                                        data[i]['bottom_right'][1]],
                                       (255, 0, 0, 100),
                                       (255, 0, 0, 255))

                        # Prepare message
                        testcase_string = get_test_case(data[i]['test_case'])
                        message = '{}\ntestcase: {}, label_id: {}, confidence: {}'.format(testcase_string,
                                                                                          data[i]['test_case'],
                                                                                          data[i]['label_id'],
                                                                                          data[i]['confidence'])

                        print_string(message, data[i]['upper_left'][0], data[i]['upper_left'][1])
                        break
                else:
                    message = 'NO OBJECTS WITH ID {} FOUND AT THIS TIMESTAMP'.format(overlay_id)
                    print_string(message, 30, 30, 40)

            except ExceptionHandler:
                message = 'NO OBJECTS OF INTEREST WITH ANY ID FOUND AT THIS TIMESTAMP'
                print_string(message, 30, 30, 40)

            images[-1][0] = img

        messages += 1
        if max_messages >= messages:
            break

    if images == []:
        raise ExceptionHandler(code='no_images_returned_err')

    return images


def get_width(image_meta):
    return int((image_meta.parent_width
                - image_meta.parent_region_of_interest.left
                - image_meta.parent_region_of_interest.right)
               / image_meta.parent_subsampling.width)

def get_height(image_meta):
    return int((image_meta.parent_height
                - image_meta.parent_region_of_interest.top
                - image_meta.parent_region_of_interest.bottom)
               / image_meta.parent_subsampling.height)
