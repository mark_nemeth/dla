
# DANF - UI Microservice


## Table of Contents

  1. [Prerequisites](#prerequisites)
  1. [Setup](#setup)
  1. [Run Project in development](#run-project-in-development)
  1. [Deployment](#deployment)
  1. [Testing](#testing)
  1. [Style](#style)
  1. [Access redis in cloud](#access-redis-in-cloud)
  1. [Troubleshooting](#troubleshooting)
  1. [Useful links](#useful-links)


## Prerequisites

### Download and install node.js
```console
sudo mkdir -p "/usr/local/lib/nodejs"
wget -c https://nodejs.org/download/release/v11.11.0/node-v11.11.0-linux-x64.tar.xz  -O - | sudo tar -xJv -C "/usr/local/lib/nodejs"
```

### Set environment variable (maybe add to ~/.bashrc for convenience)
```console
export PATH="/usr/local/lib/nodejs/node-v11.11.0-linux-x64/bin:${PATH}"
```

### configure npm for cntlm usage (optional)
```console
npm config set proxy http://127.0.0.1:3128
npm config set http-proxy http://127.0.0.1:3128
npm config set https-proxy http://127.0.0.1:3128
npm config set registry http://registry.npmjs.org
```

### change permissions
```console
sudo chmod a+wrx -R /usr/local/lib/nodejs/node-v11.11.0-linux-x64/
```

## Setup

### Client
Setup client by execute this command 
```console
cd client 
npm install
```

### Server
Setup server by execute this command 
```console
cd Server 
npm install
```

### Local HTTPS
To set up HTTPS for your dev environment you need to create a self-signed SSL certificate for your machine.
This is required for using the Daimler Login flow, which is needed in the future. <br>
- Create a self-signed SSL certificate with this commands
``` console
openssl genrsa -out danf-ui-service.key 2048
openssl req -new -x509 -key danf-ui-service.key -out danf-ui-service.cert -days 3650 -subj /CN=localhost
```
- Move the new created files ```danf-ui-service.key``` and ```danf-ui-service.cert``` to the ```ui-service/server/config/certs``` directory. Create the ```certs``` directory when its not exists.
- IMPORTANT Please never commit this files to the remote git repo. This files should be exist only in your local development folder.  

Now your server will start with HTTPS -> ```https://localhost:3001```

### Redis (for user management)
##### - MAC <br>
``` console
brew install redis          // install redis via brew
redis-server                // starts local redis server on localhost:6379
// optinal
redis-cli                   // connect to redis server to access content
```
##### - Windows <br>
Redis for Windows is not full supported, but with this way you get a working redis instance
1. Download redis Redis-x64-3.2.100.msi from: https://github.com/MicrosoftArchive/redis/releases
1. Install redis via msi file. <br> IMPORTANT please select: ```[ ] Add the Redis installation folder to the PATH enviroment variable```
1. Check if redis is working. Windows search for ```Services``` and inside this search for redis. <br> Also check if you can start redis by calling ```redis-server.exe``` from your terminal
1. OPTIONAL open ```redis-cli.exe``` to get access to the redis shell. <br>

If you have problems installing redis read this document https://www.c-sharpcorner.com/article/installing-redis-cache-on-windows/
##### - Linux <br>
``` console
sudo apt-get install redis-server          // install redis via brew
redis-server                               // starts local redis server on localhost:6379
// optinal
redis-cli                                  // connect to redis server to access content
```

### Authorisation and Secrets
In the cloud all secrets are injected over some environment variables with the deployment scripts. <br>
For the local development you need to add the secret values inside the ```.env``` file in the server root directory ```ui-service/server``` <br>
To get the necessary secrets just ask someone form the development team. 

#### Login
The ui-service is protected by a openId connect login mechanism. To see the website and get data from the APIs you need to be logged in. <br>
The get the login work in development you need to provide the correct auth urls and secrets inside your local ```.env``` file: [Authorisation and Secrets](#authorisation-and-secrets)

- Currently we only support the Auth0 login https://auth0.com/ <br>
  To get a valid account for this you can ask someone from the ui-service development team or Bassel. 


## Run Project in development

There are some different ways how to run the project for local development.

### Run client
You can run your frontend code by executing npm scripts in the client/package.json file.
```console
cd client
npm start
```
This will starts the client in development mode.

##### Get data from API in client development mode
The API is secured with a user login. To get real data from the API when you start the client via ```npm start``` you need a valid access token to request data from the api.
This token should be saved in your local redis instance. <br>
- You can check if you have a valid token by starting your local redis instance ```npm run redis``` and run ```redis-cli``` to connect to redis. 
After that enter ```keys *``` inside the redis console and check if the output contains a key with something like ```ui-token:``` as prefix. <br>
If not this steps will produce such token data in redis:

1. run ```npm run build``` in client directory and wait till finish
1. run ```npm run redis``` in server directory, if redis is not already running
1. run ```npm run dev``` in server directory
1. open localhost:3001 url to open the website, you will see a login mask. If not click the logout button and open the website again.
1. Enter your user credentials
1. After the redirect to the website you will have a valid access token inside your local redis instance



### Run server 
Keep in mind that the server will serve all files inside the public and the clients folder. So if they are empty no website will be shown. 
If you want so see the website on the server deploy the client project to create some website files in the server folders.
[Deploy client](#deploy-client)

#### Run server with local data 
You can start the server without the need of some external APIs. Just start the npm local script. 
This will create some mockfiles with a python script. Python needs to be installed on you machine to work.
The server will then only serve the lockal mockfiles when calling his API form the clients.
```console
cd server
npm run dev-local
```

#### Run server in development mode
You can run the server in development mode. This will enabled hot-reloading when changing some code. 
It will set the "NODE_ENV" environment variable to "development"
```console
cd server
npm run dev
```

#### Run server in production mode
You can build the server for production usage. Hot-reloading on code changes will not be possible anymore.
It will minify the build and set the "NODE_ENV" environment variable to "production"
```console
cd server
npm start
// or
npm run prod
```
#### Debug Server in Visual Studio Code
To debug the server first create a debugging configuration (launch.json). Add the following 
configuration:
```
{
    "name": "Debug UI Server",
    "request": "launch",
    "runtimeArgs": [
        "run-script",
        "debug"
    ],
    "runtimeExecutable": "npm",
    "skipFiles": [
        "<node_internals>/**"
    ],
    "type": "pwa-node",
    "outFiles": [
        "${workspaceRoot}/server/dist/**/*.js"
    ],
    "cwd": "${workspaceFolder}/server/"
}
```
Make sure all the pathes match with your local setup. Then compile the server code with "npm run compile". After compiling start the debugger "Debug UI Server" in the VS Code Debug Tab.

#### Run server with DANF Local Dev Environment

You can set up multiple components using DANF Local Dev Environment, such as a local mongoDB instance and a Search API instance running locally. To be able test API calls, you can set up a specific version of Search API and connect the UI microservice to it.

In the Local Dev Environment folder run:
```
docker compose up mongo
docker compose up search_api
```
To connect the UI microservice, you should run:
```
cd server
npm run dev-port-forward-local
```
This way the UI microservice is going to use http://localhost:8889 as the Search API host. To view or modify the bagfiles stored in mongoDB, you can use mongo express (see DANF Local Dev Environment for more information).

#### Run server connected to Integration Environment

You can also connect the server to the integration environment forwarding the port of the Search API:
```
while true; do kubectl port-forward deployment/search-api 8445:8445 -n dla-danf; done
```
 and running:
 ```
 cd server
 npm run dev-port-forward-int
 ```
 
## Deployment 

### Deploy client 
Run npm build script in client folder, this will automatically build the react client app and convert it to a minified version.
After that the output will be copied to the server clients folder which than can serve the website
```console
cd client
npm run build
```

### Deploy server 
After the client was deployed we can deploy the NodeJS server which than can serve the react website.<br>
By running this code you build a production build of the server code. 
```console
cd server
npm start
// or
npm run prod
```
To deploy the server we use the build pipeline. This will convert the production code to a docker image with the Dockerfile included in this project.
To run the docker-image in the cluster we have some kubernetes yaml files for each cluster environment. 
The environment will be separated by a environment variable which are included in the kubernetes yamls which is called "DANF_ENV".
"DANF_ENV" can have two values "dev" and prod for each environment.

The get the correct APIs for each environment there are different config files which will are configured by this  environment variables. 

#### Check UI Base Imge
* In the build pipelines for on-premise clusters we are currently not able to access https://www.npmjs.com/. 
* Because of this the pipeline uses a pre-build base image which contains all needed dependencies of server and client.
* If you change any dependencies please execute <b>check_base_image.sh</b>. If dependencies differ it will create a new base image for the pipelines.

## Testing
Create test for every module component you write.<br>
This tests don't need to cover 100% of your code. But they should verify that your functionality works as expected, 
even if there are some refactorings or code changes around them. So keep in mind that you are responsible for the quality of the writen code. 

### Run client tests
```console
cd client
npm test
```

### Run server tests
<span style="color: yellow">TODO:<span> No test are currently implemented on server side. This needs to be setup.<span>
```console
cd server
npm test
```

## Style

#### Install lint extension in vscode (optional)
Click on ```extensions -> ESLint -> Install```

#### Install lint extension in intellij/webstorm (optional)
Click on ```Preferences -> Plugins -> ESLint -> Install```

#### [client styleguide -> ](client/docs/STYLEGUIDE.md)

## Access redis in cloud
If you are behind a cooperate proxy, you need to set up a ssh tunnel to connect to redis instance in the cloud. <br>
For this you need a rsa key file. Ask Bassel if you need them. <br>
- Change the values to your proxy host:port and the path to rsa key file and this in your console: 
```console
ssh -N -L 6379:dlaredisdbdev.redis.cache.windows.net:6379 -o "ProxyCommand=nc -X connect -x PROXY_HOST:PROXY_PORT %h %p" -p 443 dlaadmin@52.233.189.117 -i ~/PATH_TO_RSA_KEY
```
To access the redis storage use the redis-cli command with the following parameters: (REDIS_PRIMARY_ACCESS_KEY can be found in azure)
```console
redis-cli -a REDIS_PRIMARY_ACCESS_KEY -p 6379
```
If you have trouble with the connection try the following commands:
- Checks if you have connection to the DLA jump host in azure
```console
ssh -o "ProxyCommand=nc -X connect -x PROXY_HOST:PROXY_PORT %h %p" -p 443 dlaadmin@52.233.189.117 -i ~/PATH_TO_RSA_KEY
```
- Check if Jump host can connect to redis instance 
```console
telnet dlaredisdbdev.redis.cache.windows.net 6379
```

## Troubleshooting

##### npm cntlm fix 
- cntlm will otherwise crash, due to 1000s of requests created by npm i
    ```console
    npm config set strict-ssl false
    npm set progress=false 
    npm install -g npm
    ```

##### if installation fails with npm. Run yard installation for your OS
- [Yarn installation link](https://yarnpkg.com/lang/en/docs/install/)

##### connection could not be established or a connection error happen
- Go to ```ui-service/server/conig/proxy.json``` and change the values to something thats works for your system. 

##### Login: Connection could not be established on 
- Go to ```ui-service/server/.env``` and check if all variables are set. If not ask someone from the development team to provide you the correct values. 

##### Redis: Error: Redis connection to localhost:6379 failed - connect ECONNREFUSED 127.0.0.1:6379
- Check if your redis instance is running:
    - For local development use the ```npm run redis``` command or read the [Setup](#setup) section. <br>
    - To use the redis instance in the azure cloud read this section: [Access redis in cloud](#access-redis-in-cloud) <br> 
    (CAREFUL: THIS ACCESS REAL USER DATA)

##### StatusCodeError: 401 - "{\"error\": \"Access denied\"}"
- This error happens you you do not have a valid access token to receive the user data from the API.<br>
To fix this run look here [Login](#login) or here [Get data from API in client development mode](#get-data-from-api-in-client-development-mode)

##### Git ignore local changes for proxy and .env files
- Run this git command inside you terminal to ignore changes to this files. So you dont unintentionally check in your local changes to this files.
    ```console
    git update-index --skip-worktree server/.env 
    git update-index --skip-worktree server/config/proxy.json 
    ```

## Useful links

#### Client
- Create React app: https://facebook.github.io/create-react-app
- React documentation: https://reactjs.org/docs/hello-world.html
- Redux starter kit: https://redux-starter-kit.js.org/introduction/quick-start
- Redux documentation: https://redux.js.org/introduction/getting-started

#### Testing
- Javascript test framework: https://jestjs.io/

#### Server
- NodeJs Express: http://expressjs.com/en/starter/hello-world.html
- Build NodeJs server with ES6: https://www.freecodecamp.org/news/how-to-enable-es6-and-beyond-syntax-with-node-and-express-68d3e11fe1ab
- NodeJS Project Structure: https://softwareontheroad.com/ideal-nodejs-project-structure