node('SMTCA01551') {
 stage('Get repo...') {
    checkout scm
 }

 stage('Launch kubeconfig cli...') {
 try{
    withCredentials([usernamePassword(credentialsId: 'tech_user_dla', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
    withCredentials([usernamePassword(credentialsId: 'tech_user_dla_k8', usernameVariable: 'KUSERNAME', passwordVariable: 'KPASSWORD')]) {
    withCredentials([file(credentialsId: 'tech_user_cert', variable: 'cert')]) {
        //START KUBECTL DOCKER AND LOGIN
        sh"""
        docker login athena.daimler.com --username="$USERNAME" --password="$PASSWORD"
        docker pull athena.daimler.com/dla_docker/kubectl:latest
        docker tag athena.daimler.com/dla_docker/kubectl:latest dla-danf-kubectl
        docker run --rm -itd --name dla-danf-kubectl dla-danf-kubectl
        docker cp data_analytics/search_engine/splitter/config/bva_stripping_profiles.json dla-danf-kubectl:/home/appuser/
        docker exec -t dla-danf-kubectl kubelogin --user oidc --username "$KUSERNAME" --password "$KPASSWORD" --insecure-skip-tls-verify
        """
        //SYNC topic-profiles to INT & PROD Stage
        sh"""
        docker exec -t dla-danf-kubectl kubectl config use-context oidc@vai-int
        docker exec -t dla-danf-kubectl kubectl create configmap bva-stripping-profiles --from-file=bva_stripping_profiles.json -n kubeflow || true
        docker exec dla-danf-kubectl /bin/sh -c 'kubectl create configmap bva-stripping-profiles --from-file bva_stripping_profiles.json -o yaml --dry-run -n kubeflow | kubectl replace -f - -n kubeflow'
        docker exec -t dla-danf-kubectl kubectl config use-context oidc@vai-prod
        docker exec -t dla-danf-kubectl kubectl create configmap bva-stripping-profiles --from-file=bva_stripping_profiles.json -n kubeflow || true
        docker exec dla-danf-kubectl /bin/sh -c 'kubectl create configmap bva-stripping-profiles --from-file bva_stripping_profiles.json -o yaml --dry-run -n kubeflow | kubectl replace -f - -n kubeflow'
        docker exec -t dla-danf-kubectl kubectl config use-context oidc@sv-int
        docker exec -t dla-danf-kubectl kubectl create configmap bva-stripping-profiles --from-file=bva_stripping_profiles.json -n kubeflow || true
        docker exec dla-danf-kubectl /bin/sh -c 'kubectl create configmap bva-stripping-profiles --from-file bva_stripping_profiles.json -o yaml --dry-run -n kubeflow | kubectl replace -f - -n kubeflow'
        docker exec -t dla-danf-kubectl kubectl config use-context oidc@sv-prod
        docker exec -t dla-danf-kubectl kubectl create configmap bva-stripping-profiles --from-file=bva_stripping_profiles.json -n kubeflow || true
        docker exec dla-danf-kubectl /bin/sh -c 'kubectl create configmap bva-stripping-profiles --from-file bva_stripping_profiles.json -o yaml --dry-run -n kubeflow | kubectl replace -f - -n kubeflow'
        """
        //LOGOUT
        sh'docker kill dla-danf-kubectl'
        sh'docker logout athena.daimler.com'
    }
    }
    }
    }
    catch (e) {
    sh"""
    docker kill dla-danf-kubectl
    docker logout athena.daimler.com
    """
    deleteDir()
    throw e
    }
    deleteDir()
 }
}