import React from 'react';
import {defaultEmptyValue} from '../../Table';
import {statusConverter} from '../../../utils/valueConverter';
import {isStringValid} from '../../../utils/helperFunctions';
import configHandler from "../../../config/configHandler";

export default function IngestStatusEntry(props) {

  if (!isStringValid(props.status)) return <span>{defaultEmptyValue}</span>;

  const formattedStatus = statusConverter(props.status);

  const jira_search_query = "(labels in (ingest_incident)) AND (summary ~ \"Ingest Incident ID: +" + props.ingestId + "\")";

  return (
      <span className="state-entry">
        {formattedStatus}
        {
          props.status === "ERROR" &&
          <span>
            {" - "}
            <a href={configHandler.getJiraIssueSearchUrl(jira_search_query)}
               target="_blank"
               rel="noreferrer">
              {"Find your Jira ticket"}
            </a>
          </span>
        }
      </span>
  )
}