import React, {useEffect} from 'react';
import './ErrorPage.scss'

function ErrorPage() {

  useEffect(() => {
    document.title = 'DANF - Error';
  }, []);

  return (
    <div className="error-page">
      Page not found
    </div>
  );
}

export default ErrorPage;
