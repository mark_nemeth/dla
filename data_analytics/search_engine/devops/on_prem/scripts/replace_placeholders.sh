#!/bin/bash
# This script replaces all placeholders of the components defined in "components"- array with the versions
# defined in the versions array
# Argument: $1 = configmap with placeholders
# base path: /data_analytics/search_engine/devops/on_prem/scripts/
danfVersion=$(git describe --tag --abbrev=0)
components=(   extractor
               splitter
               cleaning_script
               tracing-sidecar)
versions=(     $(grep -Po "^VERSION\s*=\s*\"\K.*?(?=\")" ../../../../../data_analytics/search_engine/extractor/config/constants.py)
               $(grep -Po "^VERSION\s*=\s*\"\K.*?(?=\")" ../../../../../data_analytics/search_engine/splitter/config/constants.py)
               $(grep -Po "^VERSION\s*=\s*\"\K.*?(?=\")" ../../../../../data_analytics/search_engine/cleaning_script/config/constants.py)
               $(grep -Po "^VERSION\s*=\s*\"\K.*?(?=\")" ../../../../../data_analytics/search_engine/tracing_sidecar/config/constants.py))
echo "Replace components placeholder"
for ((i=0;i<${#components[@]};++i)); do
sed -i "s/<placeholder_${components[i]}_version>/${versions[i]}/g" ../../../../../$1
done
echo "Replace danf_version placeholder"
sed -i "s/<placeholder_danf_version>/$danfVersion/g" ../../../../../$1