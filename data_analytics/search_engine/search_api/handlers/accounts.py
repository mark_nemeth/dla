__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import hashlib

from handlers.db_data_handler import DBDataHandler
from model.accounts import Account


class AccountHandler:

    def __init__(self):
        self.db_handler = DBDataHandler()

    def get_by_key(self, account_key: str) -> Account:
        key_hash = self._compute_key_hash(account_key)
        return self.db_handler.get_account_by_key_hash(key_hash)

    @staticmethod
    def _compute_key_hash(account_key: str) -> str:
        return hashlib.sha256(account_key.encode('utf-8')).hexdigest()
