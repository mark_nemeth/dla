"""Search Engine REST API caching duration and time route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing

from handlers.auth import auth
from handlers.cache_drive_duration_time_handler import CacheDriveDurationTimeHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


logger = logging.getLogger(__name__)


class CacheDriveDurationRequestHandler(BaseRequestHandler):
    def initialize(self):
        """
        Hook for size duration time Cache RequestHandler initialization

        :return: None
        """
        self.caching_handler = CacheDriveDurationTimeHandler()

    def set_default_headers(self):
        """
        Configure default headers for responses
        :return:
        """
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        """
          Hook for cache POST method
          :return: None
          """


        # Request Interpretation
        tracing.start_task_in_context('cachedriveduration/post', context_headers=self.request.headers)

        # Process Request
        try:
            res = self.caching_handler.cache_drive_duration_time()
        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            tracing.end_task(success=False)
            logger.exception(error_message)
            utils.build_response(self,
                                 utils.build_error_from_message(error_message),
                                 None,
                                 constants["REQ_STATUS_CODE_SERVER_ERROR"])

        # RESPONSE
        logger.info("request served successfully")
        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_RESOURCE_CREATED"])

    def _handle_error(self, error_msg, status_code):
        logger.exception(error_msg)
        tracing.end_task(success=False)
        utils.build_response(self, utils.build_error_from_message(error_msg),
                             None, status_code)
