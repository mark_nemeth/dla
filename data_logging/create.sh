#!/bin/bash
#set -x

clear

if [ -d build ]; then
  rm -r build
fi

echo ===[ DLA Debug ]=====================================================
mkdir -p build/debug
pushd build/debug > /dev/null
cmake ../.. -G "Eclipse CDT4 - Unix Makefiles" \
	-D_ECLIPSE_VERSION=4.4 \
	-DCMAKE_CXX_FLAGS="-std=c++11" \
	-DCMAKE_BUILD_TYPE=Debug
sed -i -e 's/Project-Debug@/DLA@/g' .project 
cmake --build .
popd > /dev/null

echo ===[ DLA Release ]===================================================
mkdir -p build/release
pushd build/release > /dev/null
cmake ../.. -G "Eclipse CDT4 - Unix Makefiles" \
	-D_ECLIPSE_VERSION=4.4 \
	-DCMAKE_CXX_FLAGS="-std=c++11" \
	-DCMAKE_BUILD_TYPE=Release
sed -i -e 's/Project-Release@/DLA@/g' .project 
cmake --build .
popd > /dev/null

echo ===[ ROS ]===========================================================
pushd ros
./create.sh "$@"
popd
