# Cluster Services

This directory includes supporting Services, Deamons and CronJobs which are running as infrastructure in the cluster alongside the search engine components.

- `Kube Garbage Collector` (CronJob) to automatically remove successfully terminated pods and workflows if a defined threshold is reached.
- `Kube cache refresh` (CronJob) to run once in two week to send the request to caching endpoint to calculate the options of different filters
- `MongoDB` (Database) Introduces the MongoDB into the on prem cluster
- `MongoDB sync` (CronJob) to sync changes in the on prem MongoDB to the Azure CosmosDB (Mongo API)