__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config import code_handler as coder
from config import config_handler as configurator
from config import logging_handler as logger


class ExceptionHandler(Exception):
    def __init__(self, code='', debug_cmd=None, debug_code=None, debug_msg=None):
        self.code = code
        self.debug_cmd = debug_cmd
        self.debug_code = debug_code
        self.debug_msg = debug_msg

        if coder.get_value(self.code) is None:
            self.msg = 'Undefined error: {}'.format(code)
        else:
            self.msg = coder.get_value(self.code)

        if configurator.get_value('log_level_env_env') == 'debug':
            if self.debug_cmd:
                self.msg += '\n'
                self.msg += '============================= debug command ============================='
                self.msg += '\n'
                self.msg += f'command: {self.debug_cmd}.'
            if self.debug_code:
                self.msg += '\n'
                self.msg += '============================= debug code ============================='
                self.msg += '\n'
                self.msg += f'returncode: {self.debug_code}.'
            if self.debug_msg:
                self.msg += '\n'
                self.msg += '============================= debug message ============================='
                self.msg += '\n'
                self.msg += f'output: {self.debug_msg}.'

            logger.debug(self.msg)
