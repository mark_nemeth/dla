/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2008, Willow Garage, Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of Willow Garage, Inc. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
********************************************************************/

#include "ros/console.h"
#include "athena_rosbag/chunked_file.h"

#include <iostream>

#include <boost/format.hpp>
#include <boost/make_shared.hpp>

//#include <ros/ros.h>
#ifdef _WIN32
#    ifdef __MINGW32__
#      define fseeko fseeko64
#      define ftello ftello64
//     not sure if we need a ftruncate here yet or not
#    else
#        include <io.h>
#        define fseeko _fseeki64
#        define ftello _ftelli64
#        define fileno _fileno
#        define ftruncate _chsize
#    endif
#endif

#include <cstring>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>

#define OPENFLAGS1      O_CREAT | O_RDWR | O_EXCL | O_TRUNC | O_DIRECT

using std::string;
using boost::format;
using boost::shared_ptr;
using ros::Exception;

namespace athena_rosbag {

ChunkedFile::ChunkedFile() :
  file_(NULL),
  offset_(0),
  compressed_in_(0),
  unused_(NULL),
  nUnused_(0),
  bytes_sent_to_write_(0),
  aiostream_activated_(true)
{
  stream_factory_ = boost::make_shared<StreamFactory>(this);
}

ChunkedFile::~ChunkedFile() {
  close();
}

void ChunkedFile::openReadWrite(string const& filename) { open(filename, "r+b"); }
void ChunkedFile::openWrite    (string const& filename) { open(filename, "w+b");  }
void ChunkedFile::openRead     (string const& filename) { open(filename, "rb");  }

int open_with_flags(std::string const& filename)
{
  int flags = O_CREAT | O_RDWR | O_TRUNC | O_DIRECT;
  const int mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;

  return open(filename.c_str(), flags, mode);
}

void ChunkedFile::open(string const& filename, string const& mode) {
  // Check if file is already open
  if (file_)
    throw BagIOException((format("File already open: %1%") % filename_.c_str()).str());

  // Open the file
  if (mode == "r+b") {
    // Read + write requires file to exists.  Create a new file if it doesn't exist.
#if defined(_MSC_VER) && (_MSC_VER >= 1400 )
    fopen_s( &file_, filename.c_str(), "r" );
#else
    file_ = fopen(filename.c_str(), "r");
#endif
    if (file_ == NULL)
#if defined(_MSC_VER) && (_MSC_VER >= 1400 )
      fopen_s( &file_, filename.c_str(), "w+b" );
#else
      file_ = fopen(filename.c_str(), "w+b");
#endif
    else {
      fclose(file_);
#if defined(_MSC_VER) && (_MSC_VER >= 1400 )
      fopen_s( &file_, filename.c_str(), "w+b" );
#else
      file_ = fopen(filename.c_str(), "r+b");
#endif
    }
  }
  else
#if defined(_MSC_VER) && (_MSC_VER >= 1400 )
    fopen_s( &file_, filename.c_str(), mode.c_str() );
#else
  {
    if(!isAioActivated()){
      file_ = fopen(filename.c_str(), mode.c_str());
    }
    else{
      int fdescr = open_with_flags(filename);
      file_ = fdopen(fdescr, mode.c_str());
      file_aios_ = aios_open(filename.c_str(), fdescr);
      if (file_aios_ == NULL)
      {
        throw BagIOException((format("Error opening aiostream: %1%") % filename.c_str()).str());
      }
    }
  }
#endif

  if (!file_)
    throw BagIOException((format("Error opening file: %1%") % filename.c_str()).str());

  read_stream_  = boost::make_shared<UncompressedStream>(this);
  write_stream_ = boost::make_shared<UncompressedStream>(this);
  filename_     = filename;
  offset_       = ftello(file_);
}

bool ChunkedFile::good() const {
  return feof(file_) == 0 && ferror(file_) == 0;
}

bool ChunkedFile::isAioActivated() const {
  return aiostream_activated_;
}

bool   ChunkedFile::isOpen()      const { return file_ != NULL; }
string ChunkedFile::getFileName() const { return filename_;     }

void ChunkedFile::close() {
  if (!file_)
    return;

  // Close any compressed stream by changing to uncompressed mode
  setWriteMode(compression::Uncompressed);

  // Close the file
  int success = fclose(file_);
  if (success != 0)
  {
    throw BagIOException((format("Error closing file: %1%") % filename_.c_str()).str());
  }
  file_ = NULL;
  filename_.clear();

  clearUnused();
}

void ChunkedFile::activateAio(bool activate) {
  aiostream_activated_ = activate;
}

// Read/write modes

void ChunkedFile::setWriteMode(CompressionType type) {
  if (!file_)
  {
    throw BagIOException("Can't set compression mode before opening a file");
  }

  if (type != write_stream_->getCompressionType())
  {
    write_stream_->stopWrite();
    shared_ptr<Stream> stream = stream_factory_->getStream(type);
    stream->startWrite();
    write_stream_ = stream;
  }
}

void ChunkedFile::setReadMode(CompressionType type) {
  if (!file_)
  {
    throw BagIOException("Can't set compression mode before opening a file");
  }

  if (type != read_stream_->getCompressionType())
  {
    read_stream_->stopRead();
    shared_ptr<Stream> stream = stream_factory_->getStream(type);
    stream->startRead();
    read_stream_ = stream;
  }
}

void ChunkedFile::seek(uint64_t offset, int origin) {
  if (!file_)
  {
    throw BagIOException("Can't seek - file not open");
  }

  setReadMode(compression::Uncompressed);

  int success = fseeko(file_, offset, origin);

  if (success != 0)
  {
    throw BagIOException("Error seeking");
  }

  offset_ = ftello(file_);
}

uint64_t ChunkedFile::getOffset()            const { return isAioActivated() ? bytes_sent_to_write_ : offset_; }
uint32_t ChunkedFile::getCompressedBytesIn() const { return compressed_in_; }

void ChunkedFile::write(string const& s)        { write((void*) s.c_str(), s.size()); }

void ChunkedFile::finishWriting()
{
  if (aios_close(file_aios_))
  {
    perror("Error closing AIO stream");
  }

  activateAio(false);
}

void* ChunkedFile::write_begin(size_t size)
{
	if (!isAioActivated())
	{
		throw BagIOException("write_begin must be used with activated aiostream");
	}

	void *ptr;
	int e = aios_write_begin(file_aios_, &ptr, size);
	if (e != 0)
	{
		ROS_ERROR_THROTTLE(1, "aios_write_begin() failed (%p, %lu, %d, %d)", file_aios_, size, e, errno);
		return nullptr;
	}

	return ptr;
}

void ChunkedFile::write_end(size_t size)
{
	int e = aios_write_end(file_aios_, size);
	if (e == 0)
	{
		bytes_sent_to_write_ += size;
	}
	else
	{
		ROS_ERROR_THROTTLE(1, "aios_write_end() failed (%p, %lu, %d, %d)", file_aios_, size, e, errno);
	}
}

void ChunkedFile::write(void* ptr, size_t size)
{
	if (!isAioActivated())
	{
		write_stream_->write(ptr, size);
	}
	else
	{
		int e = aios_write(file_aios_, ptr, size);
		if (e == 0)
		{
			bytes_sent_to_write_ += size;
		}
		else
		{
			ROS_ERROR("aios_write() failed (%p, %p, %lu, %d, %d)", file_aios_, ptr, size, e, errno);
		}
	}
}

void ChunkedFile::read(void* ptr, size_t size)  { read_stream_->read(ptr, size);      }

bool ChunkedFile::truncate(uint64_t length) {
  int fd = fileno(file_);
  return ftruncate(fd, length) == 0;
}

//! \todo add error handling
string ChunkedFile::getline() {
  char buffer[1024];
  if(fgets(buffer, 1024, file_))
  {
    string s(buffer);
    offset_ += s.size();
    return s;
  }
  else
    return string("");
}

void ChunkedFile::decompress(CompressionType compression, uint8_t* dest, unsigned int dest_len, uint8_t* source, unsigned int source_len) {
  stream_factory_->getStream(compression)->decompress(dest, dest_len, source, source_len);
}

void ChunkedFile::clearUnused() {
  unused_ = NULL;
  nUnused_ = 0;
}

} // namespace athena_rosbag
