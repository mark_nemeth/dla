from flask import Flask, make_response, render_template, request, redirect, url_for, flash
# import analyse_2018
# import StringIO
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import subprocess
import os
import json
import plotly
import plotly.utils as pu
import pandas as pd
import numpy as np


app = Flask(__name__)
app.debug = True


@app.route("/")
def index():
    return render_template("index.html")


@app.route('/excuteAnalysisScript', methods=['POST'])
def excuteAnalysisScript():
    filename = request.get_data()
    filename2 = 'bagfiles/' + filename
    print(filename)
    directory = 'static/images/' + filename2
    if not os.path.exists(directory):
        subprocess.call(["python", "analyse_2018.py", filename2])
    return json.dumps({'status': 'OK', 'filename': filename})
    # return subprocess.call(["python", "extract/extract_GPS.py", filename2])


# @app.route("/fig1")
# def fig1():
#     canvas=FigureCanvas(analyse_2018.fig1)
#     png_output = StringIO.StringIO()
#     canvas.print_png(png_output)
#     response=make_response(png_output.getvalue())
#     response.headers['Content-Type'] = 'image/png'
#     return response

# @app.route("/simple.png")
# def simple():
#     import datetime
#     import StringIO
#     import random
#
#     from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
#     from matplotlib.figure import Figure
#     from matplotlib.dates import DateFormatter
#
#     fig=Figure()
#     ax=fig.add_subplot(111)
#     x=[]
#     y=[]
#     now=datetime.datetime.now()
#     delta=datetime.timedelta(days=1)
#     for i in range(10):
#         x.append(now)
#         now+=delta
#         y.append(random.randint(0, 1000))
#     ax.plot_date(x, y, '-')
#     ax.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d'))
#     fig.autofmt_xdate()
#     canvas=FigureCanvas(fig)
#     png_output = StringIO.StringIO()
#     canvas.print_png(png_output)
#     response=make_response(png_output.getvalue())
#     response.headers['Content-Type'] = 'image/png'
#     return response


@app.route("/plotly")
def plotly():
    # rng = pd.date_range('1/1/2011', periods=7500, freq='H')
    ts = pd.Series(np.random.randn(50))

    gps_data = np.genfromtxt(
        'csv/Out_MapsService_instance_0_LocalizationHorizonInterface_delimiter.csv',
        delimiter=',', skip_header=0, skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'])

    graphs = [
        dict(
            data=[
                dict(
                    x=[1, 2, 3],
                    y=[10, 20, 30],
                    type='scatter'
                ),
            ],
            layout=dict(
                title='first graph'
            )
        ),

        dict(
            data=[
                dict(
                    x=[1, 3, 5],
                    y=[10, 50, 30],
                    type='bar'
                ),
            ],
            layout=dict(
                title='second graph'
            )
        ),

        dict(
            data=[
                dict(
                    x=ts.index,  # Can use the pandas data structures directly
                    y=ts
                )
            ]
        ),

        dict(
            data=[
                dict(
                    x=gps_data[
                        'c'],  # Can use the pandas data structures directly
                    y=gps_data['d'],
                    type='scatter'
                )
            ]
        )
    ]

    # Add "ids" to each of the graphs to pass up to the client
    # for templating
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]

    # Convert the figures to JSON
    # PlotlyJSONEncoder appropriately converts pandas, datetime, etc
    # objects to their JSON equivalents
    graphJSON = json.dumps(graphs, cls=pu.PlotlyJSONEncoder)

    return render_template("plotly.html",
                           ids=ids,
                           graphJSON=graphJSON)


if __name__ == "__main__":
    app.run()
