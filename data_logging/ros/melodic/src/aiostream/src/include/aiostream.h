#ifndef AIOSTREAM_H_
#define AIOSTREAM_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct aioStreamS aioStreamT;

/*
 * Open AIO stream.
 * AIO file stream is optimized for high speed sequential file writing.
 * You must provide either file name or existing file descriptor.
 * If fd is -1, you must provide file name. The AIO stream will create the file internally
 * and close it when aios_close() is called. If fd is provided, the AIO stream will use
 * the provided file descriptor and will not close it in the aios_close().
 * @return AIO stream pointer (handle).
 */
aioStreamT *aios_open(const char *fileName, int fd);

/*
 * This function is used to begin writing to the stream.
 * Usage pattern:
 * 1) Call aios_write_begin(X) to request X byte space.
 *    The function will return pointer to sufficient linear space within internal AIO buffer.
 * 2) Copy at most Y <= X bytes of data into the provided buffer space.
 * 3) Call aios_write_end(Y) to commit Y bytes to the file.
 * Maximum message size that can be requested is MAX_MESSAGE_SIZE.
 * This function locks and keeps internal mutex in locked state. After each
 * aios_write_begin() call you must also call aios_write_end() to unlock the mutex.
 * @return 0 upon success, -1 otherwise.
 */
int aios_write_begin(aioStreamT *s, void **buffer, unsigned size);

/*
 * End write, previously started with aios_write_begin().
 * Application may end the write with less data, than was requested.
 * @return 0 upon success, -1 otherwise.
 */
int aios_write_end(aioStreamT *s, unsigned size);

/*
 * Single write operation.
 * Here the size can be arbitrary large and is not limited to MAX_MESSAGE_SIZE.
 * @return 0 upon success, -1 otherwise.
 */
int aios_write(aioStreamT *s, void *data, unsigned size);

/*
 * Close AIO stream.
 * This function is non-thread safe. Application must ensure that
 * no other thread is using the stream prior to calling this function.
 * @return 0 upon success, -1 otherwise.
 */
int aios_close(aioStreamT *s);

#ifdef __cplusplus
}
#endif

#endif /* AIOSTREAM_H_ */
