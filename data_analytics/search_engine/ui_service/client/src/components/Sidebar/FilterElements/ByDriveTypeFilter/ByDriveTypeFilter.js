import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import './ByDriveTypeFilter.scss'

export function ByDriveTypeFilter(props) {

  function onDriveTypeSelect(event) {
    const value = props.driveList.get(event);
    if (props.selectedDriveTypeList.includes(value)) props.filterByDriveType(props.selectedDriveTypeList.filter(t => t !== value));
    else props.filterByDriveType(props.selectedDriveTypeList.concat(value));
  }

  function isDriveTypeSelected(item) {
    const driveValue = props.driveList.get(item);
    return props.selectedDriveTypeList.includes(driveValue);
  }

  function resetDriveSelection() {
    props.filterByDriveType([]);
  }

  const showLoading = props.driveList === undefined || props.driveList.length <= 0;
  const subLabel = props.selectedDriveTypeList.length + ' selected';
  const active = props.selectedDriveTypeList.length > 0;

  const listItemComponent = (item) => (
    <label className="drive-type-list-item">
      <input type="checkbox" value={item}
             onChange={event => onDriveTypeSelect(event.target.value)}
             checked={isDriveTypeSelected(item)}/>
      &nbsp; {item}
    </label>
  );

  return (
      <CollapseContainer label={active > 0 ? "Drive Type: " + subLabel : "Drive Type"}
                         active={active}
                         variant="list"
                         resetCallback={resetDriveSelection}
      >
        <ScrollList
            showLoading={showLoading}
            itemList={Array.from(props.driveList.keys())}
            listItemComponent={listItemComponent}
        />
      </CollapseContainer>
  );
}