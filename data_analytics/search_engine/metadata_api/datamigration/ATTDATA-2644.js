/*
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
*/

// https://athena.daimler.com/jira/browse/ATTDATA-2644

db.bagfiles.remove({"file_type": "UNKNOWN", "link": {$regex: ".*_STRIPPED_.*"}}, {"justOne": false});
db.bagfiles.remove({"file_type": "UNKNOWN", "link": {$regex: ".*fallback.bag"}}, {"justOne": false});
db.bagfiles.remove({"file_type": "UNKNOWN", "link": {$regex: ".*.disengagement.bag"}}, {"justOne": false});
db.bagfiles.remove({"file_type": "UNKNOWN", "link": {$regex: ".*EXTRACTEDBY_LogbookIssueInterface.*"}}, {"justOne": false});
db.bagfiles.remove({"file_type": "UNKNOWN", "link": {$regex: ".*silent_testing_cv1.bag"}}, {"justOne": false});
