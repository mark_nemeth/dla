import React from 'react';
import {TablePagination} from './TablePagination';
import {shallow} from 'enzyme/build';

describe('TablePagination component', () => {

  //region Rendering
  it('should contain 1 selected page button if the number of results <= page limit', () => {
    const props = {
      total: 10,
      limit: 50,
      offset: 0,
      maxVisiblePages: 9
    };
    const wrapper = shallow(<TablePagination {...props} />);
    const pageButtons = wrapper.find('ButtonGroup').first();
    expect(pageButtons.exists()).toBe(true);
    expect(pageButtons.find('.pagination-button-selected').length).toBe(1);
    expect(pageButtons.find('.pagination-button-unselected').length).toBe(0);
  });

  it('should contain maximum 1 selected- and 9 (= 8 number + 1 arrow) unselected page buttons if there is no offset', () => {
    const props = {
      total: 10000,
      limit: 50,
      offset: 0,
      maxVisiblePages: 9
    };
    const wrapper = shallow(<TablePagination {...props} />);
    const pageButtons = wrapper.find('ButtonGroup').first();
    expect(pageButtons.exists()).toBe(true);
    expect(pageButtons.find('.pagination-button-selected').length).toBe(1);
    expect(pageButtons.find('.pagination-button-unselected').length).toBe(9);
  });

  it('should contain maximum 1 selected- and 10 (= 8 number + 2 arrow) unselected page buttons if there is an offset >= page limit', () => {
    const props = {
      total: 10000,
      limit: 50,
      offset: 50,
      maxVisiblePages: 9
    };
    const wrapper = shallow(<TablePagination {...props} />);
    const pageButtons = wrapper.find('ButtonGroup').first();
    expect(pageButtons.exists()).toBe(true);
    expect(pageButtons.find('.pagination-button-selected').length).toBe(1);
    expect(pageButtons.find('.pagination-button-unselected').length).toBe(10);
  });

  it('should contain maximum 1 selected- and 9 (= 8 number + 1 arrow) unselected page buttons if last page is loaded', () => {
    const props = {
      total: 999,
      limit: 50,
      offset: 950,
      maxVisiblePages: 9
    };
    const wrapper = shallow(<TablePagination {...props} />);
    const pageButtons = wrapper.find('ButtonGroup').first();
    expect(pageButtons.exists()).toBe(true);
    expect(pageButtons.find('.pagination-button-selected').length).toBe(1);
    expect(pageButtons.find('.pagination-button-unselected').length).toBe(9);
  });

  it('should contain all pages and left arrow, when all pages < max visible pages and last page is loaded', () => {
    const props = {
      total: 199,
      limit: 50,
      offset: 150,
      maxVisiblePages: 9
    };
    const wrapper = shallow(<TablePagination {...props} />);
    const pageButtons = wrapper.find('ButtonGroup').first();
    expect(pageButtons.exists()).toBe(true);
    expect(pageButtons.find('.pagination-button-selected').length).toBe(1);
    expect(pageButtons.find('.pagination-button-unselected').length).toBe(4);
  });
  //endregion

  //region Functionality
  it('should load next page when clicking on \'›\' arrow', () => {
    const setPaginationOffsetAction = jest.fn();
    const filterWithPaginationAction = jest.fn();
    const props = {
      total: 10000,
      limit: 50,
      offset: 50,
      maxVisiblePages: 9,
      setPaginationOffset: setPaginationOffsetAction,
      filterWithPagination: filterWithPaginationAction
    };
    const wrapper = shallow(<TablePagination {...props} />);
    const pageButtons = wrapper.find('ButtonGroup').first();
    const unselectdButtons = pageButtons.find('.pagination-button-unselected');
    const nextButton = unselectdButtons.findWhere(n => n.text().includes('›')).first();
    expect(nextButton.exists()).toBe(true);

    nextButton.simulate('click');
    expect(setPaginationOffsetAction).toHaveBeenCalled();
    expect(filterWithPaginationAction).toHaveBeenCalled();
  });
  //endregion
});