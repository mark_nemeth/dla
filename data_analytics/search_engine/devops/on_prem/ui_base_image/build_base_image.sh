#!/bin/sh
printf "Type in the base ui image version.\n"
printf "Version: "
read version
printf "Creating a ui base image.....\n"
printf "Docker Image: athena.daimler.com/dla_docker/ui-base-image:$version\n"
mkdir -p client
mkdir -p server
cp ../../../ui_service/client/package.json  client/
cp ../../../ui_service/server/package.json  server/
docker build -t athena.daimler.com/dla_docker/ui-base-image:$version . --network host --no-cache --build-arg http_proxy=http://localhost:3128 --build-arg https_proxy=https://localhost:3128
printf "Pushing athena.daimler.com/dla_docker/ui-base-image:$version.....\n"
docker push athena.daimler.com/dla_docker/ui-base-image:$version
docker tag athena.daimler.com/dla_docker/ui-base-image:$version athena.daimler.com/dla_docker/ui-base-image:latest
printf "Pushing athena.daimler.com/dla_docker/ui-base-image:latest.....\n"
docker push athena.daimler.com/dla_docker/ui-base-image:latest

