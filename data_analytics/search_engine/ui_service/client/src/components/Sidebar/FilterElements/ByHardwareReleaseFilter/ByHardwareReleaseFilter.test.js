import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByHardwareReleaseFilter} from "./ByHardwareReleaseFilter";
import ScrollList from "../../../Utils/ScrollList/ScrollList";

describe('ByHardwareReleaseFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      hardwareReleaseList: ['Release-1', 'Release-2', 'Release-3'],
      selectedHardwareReleaseList: ['Release-2']
    };
    const wrapper = shallow(<ByHardwareReleaseFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should renders hardware release list with selected column', () => {
    const reduxAction = jest.fn();
    const props = {
      hardwareReleaseList: ['Release-1', 'Release-2', 'Release-3'],
      selectedHardwareReleaseList: ['Release-2'],
      fetchAllHardwareReleases: reduxAction,
      filterByHardwareReleases: reduxAction
    };
    const wrapper = mount(<ByHardwareReleaseFilter {...props} />);
    const hardwareReleaseList = wrapper.find(ScrollList);
    const entries = hardwareReleaseList.find('tr');
    expect(entries.length).toBe(3);
    expect(entries.at(0).find('input').props().checked).toBe(false);
    expect(entries.at(1).find('input').props().checked).toBe(true);
    expect(entries.at(2).find('input').props().checked).toBe(false);
  });


  it('should call action when hardwareRelease is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      hardwareReleaseList: ['Release-1', 'Release-2', 'Release-3'],
      selectedHardwareReleaseList: ['Release-2'],
      fetchAllHardwareReleases: jest.fn(),
      filterByHardwareReleases: reduxAction
    };
    const wrapper = mount(<ByHardwareReleaseFilter {...props} />);
    const input = wrapper.find('tr').at(1).find('input');
    input.simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

});
