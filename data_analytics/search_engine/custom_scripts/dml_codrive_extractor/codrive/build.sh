#!/bin/bash

# This file builds and runs the ATPDML Co-Passenger-Annotated-Event extractor 
# on the local filesystem and abstracts away the DANF APIs by using 
# a simple HTTPS echo docker image (mendhak/http-https-echo

# Set environment variables for docker build & run
here=`dirname $(readlink -f $0)`
extractor_name=${1:-codrive}
extractor_version=${2:-1.0}


# Build docker locally and tag with extractor version
docker build $here \
   --network host \
   --build-arg HTTP_PROXY=$HTTP_PROXY \
   --build-arg HTTPS_PROXY=$HTTPS_PROXY \
   --build-arg INDEX_URL_WITH_ACCESS=https://ATTDATA:<TO BE INSERTED>@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/ \
   -t $extractor_name:$extractor_version -t $extractor_name:latest
