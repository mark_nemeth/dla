__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os

from ContainerClient.container_client import ContainerClient
from DANFUtils.exceptions import RESTException

logger = logging.getLogger(__name__)


class SetSyncFlagTask:

    def __init__(self):
        self.client = ContainerClient()

    def execute(self):
        """Marks a bagfile with a sync flag.
        """
        logger.info("Executing set sync flag")
        guid = os.getenv('BAGFILE_GUID', None)

        if not guid:
            logger.error("Set sync flag failed. Required env var BAGFILE_GUID "
                         "is missing")
            return

        try:
            self.client.add_sync_flag(guid)
        except (RESTException, AssertionError):
            logger.exception(f"Set sync flag failed. Bagfile guid: {guid}")
            raise  # fail task in order to fail step
        else:
            logger.info(f"Set sync flag successful. Bagfile guid: {guid}")
