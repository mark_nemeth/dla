import configHandlerSingleton from '../configHandler';
import getLogger from '../utils/logging';

const logger = getLogger(__filename)
export default (app) => {

  if (configHandlerSingleton.isNodeEnvInProduction()) return app;

  // Enable cors requests for dev mode
  const allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*'); // allow requests from any other server
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE'); // allow these verbs
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Cache-Control');
    next();
  };
  app.use(allowCrossDomain);
  logger.info('CORS: enabled');

  return app;
}