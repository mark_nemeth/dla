import rosbag
import pandas as pd
import numpy as np
import yaml
import datetime
from roslib.message import get_message_class


class BagFile(object):
    def __init__(self, path, required_topics=[]):
        """Constructor.

        Args:
            path -- path to the bagfile
            required_topics   -- list of topics that should be parsed
        """
        self._path = path
        self._bagfile = self._load_bagfile()
        self._info_dict = self._info_on_bag()
        self._topics_types = self.topics_and_types()
        missing = self.valid_bagfile(required_topics)
        if missing:
            raise ValueError('The following required topics are missing:\n' +
                             '\n'.join(map(str, missing)))

    def _load_bagfile(self):
        """Return handle to bagfile."""
        return rosbag.Bag(self._path)

    def close(self):
        """Close bag file."""
        self._bagfile.close()

    def _info_on_bag(self):
        """Return info dict on bag."""
        self.info_dict = yaml.load(self._bagfile._get_yaml_info())
        return self.info_dict

    def topics_and_types(self):
        """Return dict of all topic names and its types."""
        info = self._bagfile.get_type_and_topic_info()
        return {topic_name: val.msg_type
                for topic_name, val in info.topics.iteritems()}

    def valid_bagfile(self, required_topics):
        """Return list of missing topics."""
        missing = []
        for topic in required_topics:
            if not self.topic_in_bagfile(topic):
                missing.append(topic)
        return missing

    def topic_in_bagfile(self, topic):
        """Return true if topic is in bagfile and false otherwise."""
        return topic in self._topics_types.keys()

    def _ros2numpy(self, ros_type):
        """Convert primitive ros type to numpy type."""
        ros2numpy = {
            'bool': 'bool_',
            'int8': 'int8',
            'uint8': 'uint8',
            'int16': 'int16',
            'uint16': 'uint16',
            'int32': 'int32',
            'uint32': 'uint32',
            'int64': 'int64',
            'uint64': 'uint64',
            'float32': 'float32',
            'float64': 'float64',
            'string': 'object',
            'duration': 'timedelta64[ns]',
            'time': 'datetime64[ns]',
            'int8[256]': 'object'
        }
        # TODO add arrays (use object as type)
        try:
            numpy_type = ros2numpy[ros_type]
        except KeyError:
            raise ValueError('Unkown ROS type: {}'.format(ros_type))
        return numpy_type

    def topic_name(self, topic):
        """Convert topic name to better readable form."""
        if topic[0] == '/':
            topic = topic[1:]
        return topic.replace('/', '_')

    def pandas_columns(self, topic, add=False):
        """Return dict of pandas column names of topic and corresponding field names and data types.

        Args:
            topic -- name of topic
            add   -- add full topic name to variable names
        Return:
            dict: {pandas_column_name_1: (field_1, field_1 type), pandas_column_name_2: (field_2, field_2 type), ...}
        """
        message_class = self._message_class(topic)
        fields = self.fields(message_class)
        if add:
            topic_name = self.topic_name(topic)
            return {topic_name + '.' + field_name: (field_name, field_type)
                    for field_name, field_type in fields.iteritems()}
        else:
            return {field_name: (field_name, field_type)
                    for field_name, field_type in fields.iteritems()}

    def fields(self, msg_class, prefix=''):
        """Return fields of message class and its numpy data types.

        Function converts ROS data types to numpy data types.
        Example for naming convention for fields:
          - 'x' is attribute of class2
          - class2 is attribute of class1 with name 'geo'
          - class1 is message type of topic
          - Resulting name for field: geo.x

        Args:
            msg_class -- instance of message class
            prefix    -- prefix for field names of all current class attributes
        Returns:
            dict: {field_1: field_1 type, field_2: field_2 type, ...}
        """
        fields_info = dict()
        for field_name, field_type in zip(self._field_names(msg_class), self._field_types(msg_class)):
            if field_type not in (
                    'bool', 'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64', 'float32',
                    'float64',
                    'string', 'duration',
                    'geometry_msgs/Pose', 'geometry_msgs/Point', 'geometry_msgs/Quaternion', 'geometry_msgs/Vector3',
                    'std_msgs/ColorRGBA',
                    'aracom_interface_shm/aracom_interface_shm__SystemStatusType', 'int8[256]'):
                continue
            # do not add header information to pandas data frame
            if field_type == 'std_msgs/Header':
                continue
            value = getattr(msg_class, field_name)
            # check if attribute is another message class (recursion)
            if hasattr(value, '__slots__') and field_type not in ('duration', 'time'):
                # prefix for next call of fields
                prefix_next = prefix + field_name + '.'
                # call fields for message class which is attribute of current message class and results to fields_info
                for k, v in self.fields(value, prefix_next).iteritems():
                    fields_info[k] = v
            else:
                # primitive ROS data type: lookup table for numpy data type
                # note: field_type = ros_type
                numpy_type = self._ros2numpy(field_type)
                fields_info[prefix + field_name] = numpy_type
        return fields_info

    def topics_to_pandas(self, topics=[], add=False):
        """Create a Pandas data frame for each given topic containing all of its messages.

        Args:
            topics -- a list of topic names
            add   -- add full topic name to variable names
       Returns:
            A dictionary, where each key represents a topic name and each value represents a pandas data frame holding the messages of that topic
        """
        dfs = {}
        # iterate over all topics and extract messages
        for topic in topics:
            dfs[topic] = self.topic_to_pandas(topic, add)
        return dfs

    def topic_to_pandas(self, topic, add=False):
        """Create a Pandas data frame for a given topic containing all of its messages.

        Args:
            topic -- a topic name
            add   -- add full topic name to variable names
       Returns:
            A pandas data frame holding the messages of the provided topic
        """
        no_messages = self.number_of_messages(topic)
        pandas_cols = self.pandas_columns(topic, add)
        pandas_dict = {col_name: np.empty(no_messages, dtype=numpy_type)
                       for col_name, (field_name, numpy_type) in pandas_cols.iteritems()}

        # create column of data frame based on message timestamps
        ts = np.empty(no_messages, dtype='datetime64[ns]')
        # loop over all messages and fill numpy arrays
        for idx, (topic, msg, mt) in enumerate(self._bagfile.read_messages(topics=topic)):
            try:
                ts[idx] = msg.header.stamp.to_nsec()
            except:
                ts[idx] = mt.to_nsec()  # correct timestamp?
            for col_name in pandas_dict.keys():
                field_name, field_type = pandas_cols[col_name]
                pandas_dict[col_name][idx] = self.data_value(msg, field_name, field_type)

        df = pd.DataFrame(data=pandas_dict)
        df.insert(0, 'ts', ts)
        return df

    @staticmethod
    def datetime64ns2epoch(timeSt):
        """Convert datetime64[ns] to unix timestamp.
        
        Returns:
            tuple: (epochs, ns since last second)
        """
        assert np.dtype(timeSt) == np.dtype('<M8[ns]')
        timeSt = timeSt.astype('int64') / 1e9
        frac, whole = np.modf(timeSt)
        epochs = np.int64(whole)
        ns_since_last_sec = np.int64(frac * 1e9)
        return (epochs, ns_since_last_sec)

    def _start_end_point(self, timestamp, interval, direction):
        """Compute start and endpoints of interval."""
        if direction == 'before':
            start = timestamp - interval
            stop = timestamp
        elif direction == 'after':
            start = timestamp
            stop = timestamp + interval
        elif direction == 'middle':
            start = timestamp - interval / 2
            stop = timestamp + interval / 2
        return start, stop

    def crop_topic(self, topic, timestamps, interval, direction):
        """Iterate over timestamps and crop topic in selected interval.
        
        Args:
        topic      -- topic name
        timestamps -- list of timestamps (required type np.datetime64)
        interval   -- interval in which messages of bagfile are considered (required type np.timedelta64)
        direction  -- defines if start point of interval is before, after or aligned with each timestamp 
                      (allowed values: before, after, middle)
        Returns:
            pandas data frame
        """
        assert direction in ('before', 'after', 'middle')
        if not self.topic_in_bagfile(topic):
            raise ValueError('Topic {topic} not found.'.format(topic=topic))
        df = self.topic_to_pandas(topic)
        df_crop_all = None
        for timestamp in timestamps:
            ts_start, ts_end = self._start_end_point(timestamp, interval, direction)
            df_crop_single = df[(df['ts'] > ts_start) & (df['ts'] < ts_end)]
            if df_crop_all is None:
                df_crop_all = df_crop_single
            else:
                df_crop_all = df_crop_all.append(df_crop_single, ignore_index=True)
        return df_crop_all

    def data_value(self, msg, field_name, field_type):
        """Get actual value of field_name.

        field_name should point to a primitive type.

        Args:
            msg        -- instance of message class
            field_name -- path to field
            field_type -- primitive type of field
        """
        data = msg
        for attr in field_name.split('.'):
            data = getattr(data, attr)
        if field_type in ('timedelta64[ns]', 'datetime64[ns]'):
            return data.to_nsec()
        if field_type in ('object'):
            return self.int2ascii(data)
        return data

    def _field_names(self, msg):
        """Return all relevant field names of message class."""
        return msg.__slots__

    def _field_types(self, msg):
        """Return all relevant field types of message class."""
        return msg._slot_types

    def _message_class(self, topic):
        """Return instance of message class."""
        topic_type = self.topics_and_types()[topic]
        message_class = get_message_class(topic_type)
        if message_class is None:
            raise ValueError('No message class for type {} found.'.format(topic_type))
        return message_class()

    def number_of_messages(self, topic):
        """Return number of messages for topic."""
        length = [int(topic_info['messages']) for topic_info in self._info_dict['topics']
                  if topic == topic_info['topic']]
        if not length:
            raise ValueError('No topic found in info_dict for {}'.format(topic))
        return length[0]

    def int2ascii(self, integer_list):
        """Convert list of integers to a single string (assuming ASCII code)."""
        s = [chr(c) for c in integer_list if c != 0]
        s = ''.join(s)
        return s

    @staticmethod
    def join_data_frames(df1, df2, tolerance='0.02s'):
        """Wrapper function for merge_asof. Joins to data frames based on timestamp.  
        
        Joins row of df2 with row df1 if timestamps do not differ more than time span defined by tolerance.
        """
        return pd.merge_asof(df1, df2, on='ts', direction='nearest', tolerance=pd.Timedelta(tolerance))

    @staticmethod
    def get_timestamps_for_topic_value(df, topic, topic_values, additional_return_topics=[]):
        """Return data frame with timestamps for all occurences of certain topic values in the given dataframe.

            Args:
            df -- dataframe containing bag-file topics
            topic -- topic name
            topic_values -- a list of values of the given topic for which timestamps should be extracted
            additional_return_topics -- list of topics that should be extracted in addition to the provided topic and the timestamp

            Returns:
                list of timestamps matching the given topic value
        """
        if not topic in df.columns:
            return None
        current_state = df[topic].isin(topic_values)
        timestamps = df[current_state]
        return_topics = ['ts', topic]
        return_topics.extend(additional_return_topics)
        timestamps = timestamps[return_topics]
        if timestamps.empty:
            return None
        return timestamps

    @staticmethod
    def get_timestamps_for_topic_value_change(df, topic, topic_values, topic_values_new, additional_return_topics=[]):
        """Return data frame with timestamps for all situations in which a topic changed its value from a set of provided
        values to another set of provided values.

            Args:
            df -- dataframe
            topic -- topic name
            topic_values -- a list of possible initial values
            topic_values_new -- a list of possible new values after the change
            additional_return_topics -- list of topics that should be extracted in addition to the provided topic and the timestamp

            Returns:
                list of timestamps matching the given topic value change
        """
        if not topic in df.columns:
            return None
        current_state = df[topic].isin(topic_values_new)
        shifted = df[topic].shift()
        # remove first line to handle nan value from shift
        shifted.pop(0)
        previous_state = pd.concat([pd.Series([False]), shifted.isin(topic_values)])
        timestamps = df[current_state & previous_state]
        return_topics = ['ts', topic]
        return_topics.extend(additional_return_topics)
        timestamps = timestamps[return_topics]
        if timestamps.empty:
            return None
        return timestamps

    @staticmethod
    def extract_windows_from_interval(interval, window_length, overlap, skip_last_ms=0,
                                      add_interval_to_output=False):
        """ Extracts sliding windows from the given time interval with the specified overlap

            Args:
            interval -- tuple(start,end)
            window_length -- length of the windows that should be extracted in ms
            overlap -- overlap between the windows in ms
            skip_last_ms -- defines how many ms should be ignored at the end of the interval
            add_interval_to_output -- add interval timestamps to resulting window tuples

            Returns:
                list of timestamp tuples (start, end) representing the found windows
        """
        if overlap > window_length:
            return None
        (w_start, w_end) = interval
        # convert timestamps
        w_len = datetime.timedelta(milliseconds=window_length)
        w_overlap = datetime.timedelta(milliseconds=overlap)
        w_skip_last_ms = datetime.timedelta(milliseconds=skip_last_ms)
        w_timestamps = []
        # extract matching windows
        while w_start + w_len <= w_end - w_skip_last_ms:
            if add_interval_to_output:
                w_timestamps.append((interval[0], interval[1] - w_skip_last_ms, w_start, w_start + w_len))
            else:
                w_timestamps.append((w_start, w_start + w_len))
            w_start = w_start + w_len - w_overlap
        return w_timestamps

    @staticmethod
    def assign_window_labels(df, window_timestamps, window_column_name='window_labels'):
        """Adds a window label column to the dataframe containing a string of comma separated labels, which links each row to
        one or multiple windows provided by the timestamps. Hence splitting the dataframe into windows without actually
        creating a separate dataframe for each window.

            Args:
            df -- dataframe
            window_timestamps -- list of timestamp tuples (start, end) indicating the start/end of the window

            Returns:
                pandas data frame
        """
        window_count = 0
        # label encoded windows
        df[window_column_name] = ''
        # construct label string based on timestamps
        for (start, end) in window_timestamps:
            df.loc[(start < df['ts']) & (df['ts'] <= end), window_column_name] += str(window_count) + ','
            window_count += 1
        return df[df[window_column_name] != '']
