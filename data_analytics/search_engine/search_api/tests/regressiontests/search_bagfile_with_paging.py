"""Regression tests for search bagfile route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from .testutils import get_testdb_connector

base_url = 'http://localhost:8889'
search_bag_url = base_url + '/search/bagfile'

bag_1 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata1.bag",
    "size": 1,
    "num_messages": 4444,
    "start": 1563262908000,
    "end": 1563262911001,
    "duration": 3001,
    "vehicle_id_num": "V-123-234",
    "topics": ["topic2"]
}

bag_2 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata2.bag",
    "size": 2,
    "num_messages": 4777,
    "start": 1563262925000,
    "end": 156326290833001,
    "duration": 8001,
    "vehicle_id_num": "V-123-777",
    "topics": ["topic1", "topic2", "topic3", "topic4"]
}

bag_3 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata3.bag",
    "size": 3,
    "num_messages": 5555,
    "start": 1563262942000,
    "end": 1563262948001,
    "duration": 6001,
    "vehicle_id_num": "V-123-777",
    "topics": ["topic2", "topic3"],
    "processing_state": "TEST_STATE"
}

bag_4 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata4.bag",
    "size": 4,
    "num_messages": 8888,
    "start": 1563262955000,
    "end": 1563262956001,
    "duration": 1001,
    "vehicle_id_num": "V-123-777",
    "topics": []
}


bag_5 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata5.bag",
    "size": 5,
    "num_messages": 123456,
    "start": 1563262958000,
    "end": 1563262956001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}

bag_6 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata6.bag",
    "size": 6,
    "num_messages": 123456,
    "start": 1563262968000,
    "end": 1563262976001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}

bag_7 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata7.bag",
    "size": 7,
    "num_messages": 123456,
    "start": 1563262986001,
    "end": 1563262996001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}

bag_8 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata8.bag",
    "size": 8,
    "num_messages": 123456,
    "start": 1563262106001,
    "end": 1563262116001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}

bag_9 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata9.bag",
    "size": 9,
    "num_messages": 123456,
    "start": 1563262126001,
    "end": 1563262136001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}

bag_10 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata10.bag",
    "size": 10,
    "num_messages": 123456,
    "start": 1563262146001,
    "end": 1563262156001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}

bag_11 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata11.bag",
    "size": 11,
    "num_messages": 123456,
    "start": 1563262166001,
    "end": 1563262176001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}


bag_12 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata12.bag",
    "size": 12,
    "num_messages": 123456,
    "start": 1563262186001,
    "end": 1563262196001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}


bag_13 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata13.bag",
    "size": 13,
    "num_messages": 123456,
    "start": 15632621986001,
    "end": 1563262206001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}


bag_14 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata14.bag",
    "size": 14,
    "num_messages": 123456,
    "start": 15632622186001,
    "end": 1563262316001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}

bag_15 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata15.bag",
    "size": 15,
    "num_messages": 123456,
    "start": 15632624186001,
    "end": 1563262516001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}


bag_16 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata16.bag",
    "size": 16,
    "num_messages": 123456,
    "start": 15632626186001,
    "end": 1563262716001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': 'testdata'})
        con.add_bagfiles([bag_1, bag_2, bag_3, bag_4, bag_5, bag_6, bag_7, bag_8, bag_9, bag_10, bag_11, bag_12, \
                          bag_13, bag_14, bag_15, bag_16])


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': 'testdata'})


class TestSearchBagfilesWithPagingGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_regex_search_by_file_name_limit3_page1(self):
        page_num = 1
        query_data = {'file_name': 'testdata', 'limit': 3, 'offset': 3*(page_num-1), 'sortby': '+size'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        total = res.json()['total']

        self.assertEqual(200, res.status_code, 'Bagfile search using file name failed')
        self.assertCountEqual([bag_1, bag_2, bag_3], res_json, 'Bagfile search using file name failed')
        self.assertEqual(total, 16, 'search should return total count as 16')
        self.assertEqual(len(res_json), 3, 'the page result count should be 3' )

    def test_regex_search_by_file_name_limit3_page2(self):
        page_num = 2
        query_data = {'file_name': 'testdata', 'limit': 3, 'offset': 3*(page_num-1), 'sortby': '+size'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        total = res.json()['total']

        self.assertEqual(200, res.status_code, 'Bagfile search using file name failed')
        self.assertCountEqual([bag_4, bag_5, bag_6], res_json, 'Bagfile search using file name failed')
        self.assertEqual(total, 16, 'search should return total count as 16')
        self.assertEqual(len(res_json), 3, 'the page result count should be 3' )

    def test_regex_search_by_file_name_limit3_page3(self):
        page_num = 3
        query_data = {'file_name': 'testdata', 'limit': 3, 'offset': 3*(page_num-1), 'sortby': '+size'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        total = res.json()['total']

        self.assertEqual(200, res.status_code, 'Bagfile search using file name failed')
        self.assertCountEqual([bag_7, bag_8, bag_9], res_json, 'Bagfile search using file name failed')
        self.assertEqual(total, 16, 'search should return total count as 16')
        self.assertEqual(len(res_json), 3, 'the page result count should be 3' )

    def test_regex_search_by_file_name_limit3_page4(self):
        page_num = 4
        query_data = {'file_name': 'testdata', 'limit': 3, 'offset': 3*(page_num-1), 'sortby': '+size'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        total = res.json()['total']

        self.assertEqual(200, res.status_code, 'Bagfile search using file name failed')
        self.assertCountEqual([bag_10, bag_11, bag_12], res_json, 'Bagfile search using file name failed')
        self.assertEqual(total, 16, 'search should return total count as 16')
        self.assertEqual(len(res_json), 3, 'the page result count should be 3' )

    def test_regex_search_by_file_name_limit3_page5(self):
        page_num = 5
        query_data = {'file_name': 'testdata', 'limit': 3, 'offset': 3*(page_num-1), 'sortby': '+size'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        total = res.json()['total']

        self.assertEqual(200, res.status_code, 'Bagfile search using file name failed')
        self.assertCountEqual([bag_13, bag_14, bag_15], res_json, 'Bagfile search using file name failed')
        self.assertEqual(total, 16, 'search should return total count as 16')
        self.assertEqual(len(res_json), 3, 'the page result count should be 3' )

    def test_regex_search_by_file_name_limit3_page6(self):
        page_num = 6
        query_data = {'file_name': 'testdata', 'limit': 3, 'offset': 3*(page_num-1), 'sortby': '+size'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        total = res.json()['total']

        self.assertEqual(200, res.status_code, 'Bagfile search using file name failed')
        self.assertCountEqual([bag_16], res_json, 'Bagfile search using file name failed')
        self.assertEqual(total, 16, 'search should return total count as 16')
        self.assertEqual(len(res_json), 1, 'the page result count should be 1' )