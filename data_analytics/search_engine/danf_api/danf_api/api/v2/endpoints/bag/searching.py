"""Endpoint definitions for bag searching."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import List, Optional
from uuid import UUID

from fastapi import APIRouter, Query, Path, HTTPException, status

from .....crud.bag import find_bags, get_bag_by_id
from .....models.bag import ManyBags, BagFilterParams, BagQueryParams, BagType, Bag
from .....models.base import OpValue
from .....models.tag import TagFilterParams

router = APIRouter()


@router.get(
    "/metadata/projects/danf/bags",
    response_model=ManyBags,
    tags=["bags"],
    response_model_exclude_unset=True,
)
async def search_bags(
    guid: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""The unique ID of the bags.
        Multiple filters are considered with logical OR relation.
        Allowed operators: **EQ** group.
        E.g. `eq:79a758cg-7Z24-4350-bc7a-d15364agha95`, `79a758cg-7Z24-4350-bc7a-d15364agha95`""",
    ),
    start: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Start timestamp (milliseconds) or date-time (ISO format - UTC unless specified) of the recording.
        Multiple filters are considered with logical AND relation.
        Allowed operators: **EQ** and **INTERVAL** groups (always use an explicit operator for ISO date-time format).
        E.g. `gt:1620151203887`, `1620151203887`, `gt:2021-03-12T18:15:01`""",
    ),
    end: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""End timestamp (milliseconds) or date-time (ISO format - UTC unless specified) of the recording.
        Multiple filters are considered with logical AND relation.
        Allowed operators: **EQ** and **INTERVAL** groups (always use an explicit operator for ISO date-time format).
        E.g. `gt:1620151203887`, `1620151203887`, `gt:2021-03-12T18:15:01`""",
    ),
    duration: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Duration of the recording in milliseconds.
        Multiple filters are considered with logical AND relation.
        Allowed operators: **EQ** and **INTERVAL** groups.
        E.g. `gt:60000`, `997660`""",
    ),
    vehicle_id_num: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Vehicle specifier.
        Multiple filters are considered with logical OR relation.
        Allowed operators: **EQ** group.
        E.g. `eq:v200-b-36`, `v200-b-36`""",
    ),
    topics: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Required ROS topic specifier.
        Multiple filters are considered with logical AND relation.
        Allowed operators: **EQ** group.
        E.g. `eq:/foo/bar/topic1`, `/foo/bar/topic1`""",
    ),
    size: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Size of the bag in bytes.
        Multiple filters are considered with logical AND relation.
        Allowed operators: **EQ** and **INTERVAL** groups.
        E.g `gte:10000000000`, `1898123012`""",
    ),
    num_messages: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Number of ROS messages.
        Multiple filters are considered with logical AND relation.
        Allowed operators: **EQ** and **INTERVAL** groups.
        For example `gte:1130230`, `123889`""",
    ),
    drive_types: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Specific drive types.
        Multiple filters are considered with logical OR relation.
        Allowed operators: **EQ** group.
        For example `eq:CID` or `CID`""",
    ),
    file_name: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Name of the file.
        Multiple filters are considered with logical OR relation.
        Allowed operators: **EQ** and **REGEX** groups.
        E.g. `re:.*_planning_io.bag`, `20210504_111937_FBI_att1055_pethurt_STRIPPED_1_planning_io_no_camera.bag`""",
    ),
    origin: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""Origin location of the bag, use the country codes. 
        Multiple filters are considered with logical OR relation.
        Allowed operators: **EQ** group.
        E.g. `DE` as Germany.""",
    ),
    tag: Optional[List[str]] = Query(
        default=None,
        regex=TagFilterParams.RE_ID_FORMAT,
        description="""Tag of the bag.
        Multiple filters are considered with logical AND relation.
        The format is `NAMESPACE::[OP:]VALUE`. The namespace is always considered as an exact match. 
        Allowed operators: **EQ** and **REGEX** groups.
        E.g. `FOO.BAR::BAZ.BOO` for an exact match, `FOO.BAR::re:^BOO.*` for bags having a tag in the `FOO.BAR`
        namespace matching the given regex.""",
    ),
    no_tag: Optional[List[str]] = Query(
        default=None,
        regex=TagFilterParams.RE_ID_FORMAT,
        description="""No such tag should exist for the bag.
        Multiple filters are considered with logical AND relation.
        The format is `NAMESPACE::[OP:]VALUE`. The namespace is always considered as an exact match. 
        Allowed operators: **EQ** and **REGEX** groups.
        E.g. `FOO.BAR::BAZ.BOO` for an exact match, `FOO.BAR::re:^BOO.*` for bags not having any tag in the `FOO.BAR`
        namespace matching the given regex.""",
    ),
    bag_type: Optional[BagType] = Query(
        default=None,
        description="""By default both matching bagfiles and childbagfiles are returned. Choose `bagfile` or
        `childbagfile` to restrict results to just one of these.""",
    ),
    fields: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""The API defines a default projection for each resource type, meaning that only a given set of
        metadata fields are returned. The default projection can be modified with values passed to the fields parameter.
        Allowed operators: `inc` (include), `exc` (exclude) or no operator.
        The `inc` and the `exc` operators define a relative projection to the default one, but if any of the passed 
        fields has no operator specified then the default projection will be ignored and the passed fields will be 
        considered as an absolute projection definition.""",
    ),
    order_by: List[str] = Query(
        default=["desc:start"],
        min_length=1,
        description="""Sort the results based on one or multiple fields.
        Allowed operators: `asc` (ascending), `desc` (descending) or no operator (default descending).
        **Note: when searching for both bagfile and childbagfile bag_type, the ordering is applied separately for
        the matching bagfiles and childbagfiles. Bagfiles will come before the childbagfiles in all cases.**
        E.g. `asc:duration`""",
    ),
    limit: int = Query(
        default=20,
        gt=0,
        le=100,
        description="Pagination limit: the maximum number of results to return.",
    ),
    offset: int = Query(
        default=0,
        ge=0,
        description="""Pagination offset: the number of results to omit (from the start of the result set) when
        returning them.""",
    ),
) -> ManyBags:
    """Find bags matching a given criteria
    ---

    Filtering can be done on certain fields of the metadata such as guid, start time, file name etc.

    Some parameters allow the usage of different operators on top of simply specifying a value (which usually means an
    implicit equality operator). E.g.: we can ask the endpoint to search for bags longer that 7 seconds with the `gt:7`
    string. The API uses the RHS Colon operator syntax, which means that when a parameter allows special operators then
    the passed string should look like this: `OP:VALUE` (or just `VALUE` without an explicit operator for using the
    default one).

    **Note:** do not add a space between the colon and the value!

    The allowed operators for each parameter are detailed at the description of the exact parameter. These descriptions
    might refer to the following groups of operators:

    - **EQ**: no operator or the `eq` operator explicitly (both meaning equality)
    - **INTERVAL**: `gt` (greater than), `gte` (greater than equal), `lt` (less than), `lte` (less than equal)
    - **REGEX**: `re`
    """
    query_params = BagQueryParams(
        filter_params=BagFilterParams(
            guid=guid,
            start=start,
            end=end,
            duration=duration,
            vehicle_id_num=vehicle_id_num,
            topics=topics,
            size=size,
            num_messages=num_messages,
            drive_types=drive_types,
            file_name=file_name,
            origin=origin,
            tag=tag,
            no_tag=no_tag,
        ),
        bag_type=bag_type,
        fields=fields,
        order_by=order_by,
        limit=limit,
        offset=offset,
    )
    return await find_bags(query_params)


@router.get(
    "/metadata/projects/danf/bags/{guid}",
    response_model=Bag,
    tags=["bags"],
    response_model_exclude_unset=True,
)
async def get_bag(
    guid: UUID = Path(
        default=...,
        description="The unique ID of the bag. E.g. `79a758cg-7Z24-4350-bc7a-d15364agha95`",
    ),
    fields: Optional[List[str]] = Query(
        default=None,
        min_length=1,
        description="""The API defines a default projection for each resource type, meaning that only a given set of
        metadata fields are returned. The default projection can be modified with values passed to the fields parameter.
        Allowed operators: `inc` (include), `exc` (exclude) or no operator.
        The `inc` and the `exc` operators define a relative projection to the default one, but if any of the passed 
        fields has no operator specified then the default projection will be ignored and the passed fields will be 
        considered as an absolute projection definition.""",
    ),
) -> Bag:
    """Get a single bagfile or childbagfile based on it's guid
    ---

    Some parameters allow the usage of different operators on top of simply specifying a value (which usually means an
    implicit equality operator). The API uses the RHS Colon operator syntax, which means that when a parameter allows
    special operators then the passed string should look like this: `OP:VALUE` (or just `VALUE` without an explicit
    operator for using the default one).

    **Note:** do not add a space between the colon and the value!
    """
    fields = fields and [OpValue.from_rhs_colon(f) for f in fields]
    try:
        return await get_bag_by_id(guid=guid, fields=fields)
    except KeyError as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e))
