"""Handler class for child bagfile search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import json
import logging

from bson import SON

from handlers.db_data_handler import DBDataHandler

logger = logging.getLogger(__name__)



class ChildBagfileSearchHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def create_search_kwargs(self, query_args):
        search_kwargs = {
            'query': None,
            'limit': 50,
            'offset': 0,
            'sortby': 'guid'
        }
        query = {}
        range_query = []
        or_query = []
        # By default we should return not deleted documents
        if 'is_deleted' not in query_args:
            query_args['is_deleted'] = ['false']

        for key, value in query_args.items():
            # Query filters
            if key in ['parent_guid', 'guid', 'processing_state']:
                query[key] = value[0]
            elif key == 'is_deleted':
                query[key] = self.create_is_deleted_query(value[0])
            elif key == 'vehicle_id_num':
                query[key] = self.create_vin_query(
                    query_args[key])
            elif key == 'event':
                # test for the existence of the event fields in childbag documents e.g. metadata.logbook
                for event in query_args[key]:
                    or_query.append({event: {'$exists': 1}})

            elif key == 'topics':
                query['topics'] = self.create_topic_query(query_args[key])
            elif key in ['file_name', 'file_path']:
                if key == 'file_path':
                    key = 'link'
                query[key] = {'$regex': f'{value[0]}', '$options': 'i'}
            elif key == "size[gte]":
                try:
                    range_query.append({'size': {"$gte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid size[gte] query parameter in bagfile search - integer expected'
                    )
            elif key == "size[lte]":
                try:
                    range_query.append({'size': {"$lte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid size[lte] query parameter in bagfile search - integer expected'
                    )
            elif key == "start[gte]":
                try:
                    range_query.append({'start': {"$gte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid start[gte] query parameter in bagfile search - integer expected'
                    )
            elif key == "start[lte]":
                try:
                    range_query.append({'start': {"$lte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid start[lte] query parameter in bagfile search - integer expected'
                    )
            elif key == "duration[gte]":
                try:
                    range_query.append(
                        {'duration': {
                            "$gte": abs(int(value[0]))
                        }})
                except ValueError:
                    logger.warning(
                        'Invalid duration[gte] query parameter in bagfile search - integer expected'
                    )
            elif key == "duration[lte]":
                try:
                    range_query.append(
                        {'duration': {
                            "$lte": abs(int(value[0]))
                        }})
                except ValueError:
                    logger.warning(
                        'Invalid duration[lte] query parameter in bagfile search - integer expected'
                    )

            elif key == "cv1_classifier":
                query['metadata.ext1_4.stereo_cnn.bounding_box_labels.' + str(value[0])] = {'$exists': True}

            # Pagination
            elif key == 'limit':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning(
                        'Invalid limit query parameter in child bagfile search - integer expected'
                    )
            elif key == 'offset':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning(
                        'Invalid offset query parameter in child bagfile search - integer expected'
                    )
            # search based on GPS polygon input i.e [[0,0], [0,7], [6,7], [6,0], [0,0]]
            elif key == 'gps_polygon':
                logger.debug(f' gps value: {value[0]}')
                gps_coordinates = json.loads(value[0])
                query['gps_track'] = {
                    '$geoIntersects': {
                        '$geometry':
                            SON([("type", "Polygon"),
                                 ("coordinates", [gps_coordinates])])
                    }
                }
            # Sort
            elif key == 'sortby':
                # Ascending order
                if value[0] in [
                    'guid', 'start', 'duration', 'vehicle_id_num', 'size'
                ]:
                    search_kwargs[key] = value[0]
                elif value[0] in [
                    '+guid', '+start', '+duration', '+vehicle_id_num',
                    '+size'
                ]:
                    search_kwargs[key] = value[0][1:]
                # Descending order
                elif value[0] in [
                    '-guid', '-start', '-duration', '-vehicle_id_num',
                    '-size'
                ]:
                    search_kwargs[key] = [(value[0][1:], -1)]

        if len(range_query) >= 1:
            query['$and'] = range_query

        if len(or_query) >= 1:
            query['$or'] = or_query

        search_kwargs['query'] = query
        return search_kwargs

    def create_search_query(self, query_fields):
        field_list = []
        for key, value in query_fields.items():
            if key == 'bva_min':
                field_list.append({"bva": {"$gte": value}})
            elif key == 'file_path':
                field_list.append(
                    {"link": {
                        '$regex': f'{value}',
                        '$options': 'i'
                    }})
            elif key == 'file_name':
                field_list.append(
                    {key: {
                        '$regex': f'{value}',
                        '$options': 'i'
                    }})
            # for post request use body i.e
            # {
            # 	"gps_polygon": [[0,0], [0,7], [6,7], [6,0], [0,0]]
            # }
            elif key == 'gps_polygon':
                gps_coordinates = value
                field_list.append({
                    'gps_track': {
                        '$geoIntersects': {
                            '$geometry':
                                SON([("type", "Polygon"),
                                     ("coordinates", [gps_coordinates])])
                        }
                    }
                })

            elif key == 'topics':
                field_list.append({key: self.create_topic_query(value)})
            elif key == 'vehicle_id_num':
                field_list.append({key: self.create_vin_query(value)})
            elif key == 'cv1_classifier':
                field_list.append({'metadata.ext1_4.stereo_cnn.bounding_box_labels.' + str(value): {'$exists': True}})
            else:
                field_list.append({key: value})
        if len(field_list) > 1:
            return {"$and": field_list}
        elif len(field_list) == 1:
            return field_list[0]
        return {}

    @staticmethod
    def create_vin_query(vins_field):
        """
        to search for a list of VINs, query must be changed including an "$in"
        (true, if any VINs from query are in a database entry)
        :param vins_field: dict element containing the key == vin
        :return:
        """
        if isinstance(vins_field, list):
            vins_field = {"$in": vins_field}
        return vins_field

    @staticmethod
    def create_topic_query(topics_field):
        """
        to search for a list of topics, query must be changed including a "$all"
        (true, if all topics from query are in a database entry)
        :param topics_field: dict element containing the key == topics
        :return:
        """
        topics_field = {"$all": topics_field}
        return topics_field

    @staticmethod
    def create_is_deleted_query(is_deleted_field):
        """
        To either search for bagfiles that are not soft deleted (default)
        or to specifically search for the deleted ones.
        :param is_deleted_field: if 'false', then "$ne: true" returns bagfiles with is_deleted = false OR missing field
        :return:
        """
        if str.lower(is_deleted_field) == 'false':
            is_deleted_field = {"$ne": True}
        else:
            is_deleted_field = {"$eq": True}
        return is_deleted_field

    def perform_search(self, **search_kwargs):
        return self.db_data_handler.search_child_bagfiles(**search_kwargs)
