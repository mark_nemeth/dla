__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from model.exceptions import RequestNotValidError
from requesthandler.base_request_handler import BaseRequestHandler
from handlers.bva_quota import BvaQuotaHandler
from handlers.auth import auth
from handlers.utils import utils
from model.bva_quota import QuotaPatchRequest


class BvaQuotaRequestHandler(BaseRequestHandler):

    _schema_guid = BaseRequestHandler.load_json_schema("guid.schema")
    _quota_handler = BvaQuotaHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST, GET, PATCH')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('quota/post',
                                      context_headers=self.request.headers)

        res = self._quota_handler.create_new_quota()

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res,
            status_code=constants['REQ_STATUS_CODE_RESOURCE_CREATED'],
            location="/bva/quotas")

    @auth.requires_entitlements(["openid"], requires_account=True)
    def get(self):
        tracing.start_task_in_context('quota/get',
                                      context_headers=self.request.headers)

        active_vote = self._to_bool(
            self.get_query_argument(name='active', default='true'))

        quotas = self._quota_handler.get_quota_usage_for_account(
            active_vote,
            self.current_user.account)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=[quota.dict() for quota in quotas],
            status_code=constants['REQ_STATUS_CODE_OK']
        )

    @auth.requires_entitlements(["openid"])
    def patch(self, guid):
        tracing.start_task_in_context('vote/patch',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        update_quota_req = self.deserialize(self.request.body,
                                            QuotaPatchRequest)

        res = self._quota_handler.update_quota(guid, update_quota_req)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res.dict(),
            status_code=constants['REQ_STATUS_CODE_OK']
        )

    @staticmethod
    def _to_bool(bool_as_str):
        bool_as_str = bool_as_str.lower()
        if bool_as_str == 'true':
            return True
        elif bool_as_str == 'false':
            return False
        else:
            raise RequestNotValidError(f'{bool_as_str} is not a valid boolean')
