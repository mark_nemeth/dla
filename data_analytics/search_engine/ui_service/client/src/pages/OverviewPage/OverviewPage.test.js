import React from 'react';
import {shallow} from 'enzyme';
import {FileTypes} from '../../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import {OverviewPage} from './OverviewPage';

describe('OverviewPage component', () => {

  it('should render without error', () => {
    const props = {
      error: false,
      fileType: FileTypes.ORIGINALFILE,
      selectedFile: {
        guid: "1337"
      },
      match: {
        params: {
          guid: "1337"
        }
      }
    };
    const wrapper = shallow(<OverviewPage {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should show loading when there are no file in store', () => {
    const props = {
      error: false,
      fileType: FileTypes.ORIGINALFILE,
      selectedFile: {},
      match: {
        params: {
          guid: "1337"
        }
      }
    };
    const wrapper = shallow(<OverviewPage {...props} />);
    expect(wrapper.find('Bootstrap(Spinner)').first().exists()).toBe(true);
  });

  it('should render error message when there are no file is selected', () => {
    const props = {
      error: true,
      errorMessage: "No original file was selected",
      fileType: FileTypes.ORIGINALFILE,
      selectedFile: {},
      match: {
        params: {
          guid: "1337"
        }
      }
    };
    const wrapper = shallow(<OverviewPage {...props} />);
    expect(wrapper.find('Connect(ErrorBanner)').exists()).toBe(true);
  });
});