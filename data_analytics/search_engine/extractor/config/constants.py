"""Constants"""

__copyright__ = '''
COPYRIGHT: (c) 2019-2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

EXTRACTOR_NAME = "extractor"

# VERSION is the version of the extractor image.
# It should be bumped if there is any change in this component.
VERSION = "1.1.48"
DANF_VERSION_TAG = "WILL_BE_TAKEN_FROM_GIT_TAG"


# EXTRACTED_EVENT_MODEL_VERSION is the schema version of the ExtractedEvent model,
# defined in model/extracted_event. If there is any change in this model,
# this version should be bumped.
EXTRACTED_EVENT_MODEL_VERSION = "1.2"

# Maximum length of the extracted event list in one extraction request
MAX_LENGTH_EXTRACTED_EVENT_LIST = 1000
