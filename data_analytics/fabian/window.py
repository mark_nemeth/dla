#!/usr/bin/python

import rosbag
import pandas as pd
import os
import sys

timestamp_s_start_list = []
timestamp_s_end_list = []
timestamp_ns_start_list = []
timestamp_ns_end_list = []
file__list = []

file_list = os.listdir(
    '/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/hks04/')
file_list = [x for x in file_list if (('.bag' in x) and ('alwayson' in x))]
for file in file_list:

    print("Processing file {} ({}/{})".format(
        file, file_list.index(file) + 1, len(file_list)))

    try:
        bag = rosbag.Bag(
            '/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/hks04/' + file)
    except rosbag.bag.ROSBagUnindexedException:
        continue
    except rosbag.bag.ROSBagException:
        print("rosbag.bag.ROSBagException on file {}".format(file))
        exit(1)
# file_list = os.listdir('/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/sv05/')
# file_list = [x for x in file_list if (('.bag' in x)and('20180514_135200_drive1.bag' in x))]
# for file in file_list:
    # try:
        # bag = rosbag.Bag('/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/sv05/' + file)
    # bag = rosbag.Bag('/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/sv05/' + file)
    # except rosbag.bag.ROSBagUnindexedException:
        # continue
    # except rosbag.bag.ROSBagException:
        # print("rosbag.bag.ROSBagException on file {}".format(file))
        # exit(1)
    topic_cal = '/CalService/instance_0/EgoVehStateInterface'

    cal_list_timestamp_s = []
    cal_list_timestamp_ns = []
    cal_list_ad_engagement_state = []

    for msg in bag.read_messages(topics=[topic_cal]):
        cal_list_timestamp_s.append(str(msg.message.header.timestamp.s))
        cal_list_timestamp_ns.append(str(msg.message.header.timestamp.ns))
        cal_list_ad_engagement_state.append(
            str(msg.message.ad_engagement_state))

    dict_cal_dropout = {'timestamp_s': cal_list_timestamp_s, 'timestamp_ns':
                        cal_list_timestamp_ns, 'ad_engagement_state': cal_list_ad_engagement_state}

    cal_dropout = pd.DataFrame(dict_cal_dropout)

    timestamp_s_temporary_start = 0
    timestamp_s_temporary_end = 0
    timestamp_ns_temporary_start = 0
    timestamp_ns_temporary_end = 0
    f = 0

    for i in cal_dropout.index:
        if cal_dropout.ad_engagement_state[i] == 2 and f == 0:
            timestamp_s_temporary_start = cal_dropout.timestamp_s[i]
            timestamp_ns_temporary_start = cal_dropout.timestamp_ns[i]
            f = 1
            continue
        elif cal_dropout.ad_engagement_state[i] == 2 and (cal_dropout.timestamp_s[i] - timestamp_s_temporary_start) == 7 and f == 1:
            timestamp_s_temporary_end = cal_dropout.timestamp_s[i]
            timestamp_ns_temporary_end = cal_dropout.timestamp_ns[i]
            continue
        elif cal_dropout.ad_engagement_state[i] == 2 and (cal_dropout.timestamp_s[i] - timestamp_s_temporary_start) == 12 and f == 1:
            timestamp_s_start_list.append(timestamp_s_temporary_start)
            timestamp_s_end_list.append(timestamp_s_temporary_end)
            timestamp_ns_start_list.append(timestamp_ns_temporary_start)
            timestamp_ns_end_list.append(timestamp_ns_temporary_end)
            file__list.append(file)
            f = 0
            timestamp_s_temporary_start = 0
            timestamp_s_temporary_end = 0
            timestamp_ns_temporary_start = 0
            timestamp_ns_temporary_end = 0
            continue
        elif cal_dropout.ad_engagement_state[i] != 2:
            f = 0
            timestamp_s_temporary_start = 0
            timestamp_s_temporary_end = 0
            timestamp_ns_temporary_start = 0
            timestamp_ns_temporary_end = 0
            continue


dict = {'file': file__list, 'timestamp_s_start': timestamp_s_start_list, 'timestamp_s_end':
        timestamp_s_end_list, 'timestamp_ns_start': timestamp_ns_start_list, 'timestamp_ns_end': timestamp_ns_end_list}
window = pd.DataFrame(dict)
print(window)

window.to_pickle('./window.pkl')
