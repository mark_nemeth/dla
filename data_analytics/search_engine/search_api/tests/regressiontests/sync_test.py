__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests

BASE_URL = 'http://localhost:8889'
SYNC_URL = BASE_URL + '/bagfiles/guids/synchronizable'
DELETE_URL = BASE_URL + '/bagfiles/guids/deletable'


class TestSync(unittest.TestCase):

    def test_get_bagfile_guids_to_sync(self):
        res = requests.get(url=SYNC_URL)
        self.assertEqual(200, res.status_code)
        res_json = res.json()['results']
        self.assertIsInstance(res_json, list)

    def test_get_bagfile_guids_to_delete(self):
        res = requests.get(url=DELETE_URL)
        self.assertEqual(200, res.status_code)
        res_json = res.json()['results']
        self.assertIsInstance(res_json, list)
