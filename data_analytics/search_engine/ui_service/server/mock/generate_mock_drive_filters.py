import json
import os

OUTPUT_FILENAME = "drive_filters.json"
VERSION = "0.1"

filters_info = {
    "duration_min_max": [
        14,
        752728000
    ],
    "vehicle_ids": [
        "lab02",
        "v222-5253",
        "v222-5215",
        "V-123-234",
        "v222-5214",
        "v222-5252",
        "v222-5410",
        "v222-5213",
        "v222-5218",
        "v222-5251",
        "v222-5217"
    ],
    "driven_by_values": [
        "driven_by-me-1",
        "driven_by-me-2",
        "driven_by-me-3",
        "driven_by-me-4",
        "unknown",
        "driven_by-me-5",
        "driven_by-me-6",
        "driven_by-me-7",
        "driven_by-me-8",
        "driven_by-me-9"
    ],
    "drivers": [
        "Driver-1",
        "Driver-2",
        "Driver-3",
        "Driver-4",
        "unknown",
        "Driver-5",
        "Driver-6",
        "Driver-7",
        "Driver-8",
        "Driver-9"
    ],
    "operators": [
        "Operator-1",
        "Operator-2",
        "Operator-3",
        "Operator-4",
        "unknown",
        "Operator-5",
        "Operator-6",
        "Operator-7",
        "Operator-8",
        "Operator-9"
    ],
    "hardware_releases": [
        "Release-1",
        "Release-2",
        "Release-3",
        "Release-4",
        "unknown",
        "Release-5",
        "Release-6",
        "Release-7",
        "Release-8",
        "Release-9"
    ]
}
result = {"results": filters_info}

outpath = os.path.join("public/mockdata")
if not os.path.exists(outpath):
    os.makedirs(outpath)

outfilePath = os.path.join(outpath, OUTPUT_FILENAME)
with open(outfilePath, 'w') as outfile:
    json.dump(result, outfile, indent=4)

print("Mock json file created: " + outfilePath)
