"""Constants"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

EXTRACTOR_NAME = "disengagement_report"

# VERSION is the version of the extractor image.
# It should be bumped if there is any change in this component.
VERSION = "0.2.15"

SCHEMA_VERSION = "2.0"
