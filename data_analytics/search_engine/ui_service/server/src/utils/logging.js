import { createLogger, format, transports } from 'winston'
const { combine, timestamp, label, printf } = format;

var path = require('path');

const logFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} - ${label} - ${level.toUpperCase()} - ${message}`;
});

function getLoggerForModule(modulePath) {

    const n = modulePath.search("src");
    const loggerLabel = modulePath.slice(n);

    const loggerConfig = {
        level: 'debug',
        format: combine(timestamp({ format: 'YYYY-MM-DD HH:MM:SS,sss' }), label({ label: loggerLabel }), logFormat),
        transports: [
            new transports.Console()
        ]
    }

    return createLogger(loggerConfig);
}

export default getLoggerForModule