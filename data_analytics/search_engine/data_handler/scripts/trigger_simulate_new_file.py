"""Regressiontests for datahandler endpoint"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse
import requests


class EndPointTest():

    def __init__(self, url, path):
        self.url = url
        self.path = path

    def run(self):
        res = requests.post(url=self.url, json=self.path)
        return res


def _get_commandline_args():
    parser = argparse.ArgumentParser(description='send list of paths to the endpoint')
    parser.add_argument('-u', '--url',
                        action='store',
                        default="http://localhost:8888/testing/simulate-new-file",
                        help='the DH url is required')
    parser.add_argument('-p', '--paths', nargs='+',
                        action='store',
                        required=True,
                        help='required a list of paths')
    return parser.parse_args()


if __name__ == '__main__':
    args = _get_commandline_args()
    for p in args.paths:
        print(f"the file path is: {p}")
        data = dict()
        data["path"] = p
        end_point_test = EndPointTest(args.url, path=data)
        res = end_point_test.run()
        print(res.content)
