#include <algorithm>
#include <sys/errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/mman.h>
#include <pthread.h>
#include <sys/resource.h>
#include <time.h>
#include "common.h"
#include "aiostream.h"
#include "hps.h"

using namespace std;

#define MAX_MSG_SIZE		(1024 * 1024)

static int loops = 10;
static int minMessageCount = 1;
static int maxMessageCount = 10000;
static int minMessageSize = 4;
static int maxMessageSize = MAX_MSG_SIZE - 8;
static bool showStatistics = 0;

#define RAND(min, max)		((min) + (rand() % ((max) - (min) + 1)))
#define _(e)				if (e) { printf("Error: " #e "\n"); exit(-1); }

typedef struct
{
	uint32_t size;
	uint32_t sequence;
} headerT;

uint8_t *testData = NULL;

static int verify(const char *name)
{
	int n;
	uint8_t *buffer = (uint8_t*)malloc(maxMessageSize);

	printf("Verifying %s ", name);

	FILE *f = fopen(name, "rb");
	if (f == NULL)
	{
		printf("Error: failed to open %s\n", name);
		return -1;
	}

	fseek(f, 0, SEEK_END);
	long long fileSize = ftell(f);
	printf("%lld MB\n", fileSize / 1024 / 1024);
	fseek(f, 0, SEEK_SET);

	headerT header;
	long long fileOffset = 0;
	int msgCount = 0;
	while (1)
	{
		n = fread(&header, 1, sizeof(header), f);
		if (n != sizeof(header))
		{
			printf("Error: message %d, failed to read header\n", msgCount);
			return -1;
		}

		fileOffset += sizeof(header);

		if (maxMessageSize < header.size)
		{
			printf("Error: message %d, invalid message size %u\n", msgCount, header.size);
			return -1;
		}

		if (header.size == 0)
		{
			msgCount++;
			break;
		}

		n = fread(buffer, 1, header.size, f);
		if (n != header.size)
		{
			printf("Error: message %d, failed to read message data\n", msgCount);
			return -1;
		}

		fileOffset += header.size;

		for (int i = 0; i < header.size; i++)
		{
			uint8_t expected = (uint8_t)(header.sequence + i);
			if (buffer[i] != expected)
			{
				printf("Error: message %d, invalid data at offset %d (file offset %lld), expected %u, got %u\n",
						msgCount, i, fileOffset, expected, buffer[i]);
				for (int j = max(0, i - 16); j < min((int)header.size, i + 16); j++)
					printf("%d ", buffer[j]);
				printf("\n");
				return -1;
			}
		}

		msgCount++;
	}

	n = fread(buffer, 1, maxMessageSize, f);
	if (n != 0)
	{
		printf("Error: unexpected end of file\n");
		return -1;
	}

	fclose(f);
	free(buffer);

	printf("Processed %d messages, OK\n", msgCount);

	return 0;
}

int test_aiostream_write(const char *name, int msgCount)
{
	aioStreamT *s;
	headerT header = { 0 };

	// Open stream
	_((s = hps_aio_open(name, -1)) == NULL);

	long long totalBytes = 0;
	long long lastTotalBytes = 0;
	volatile uint64_t t1 = getTimestampNs();

	for (int c = 0; c < msgCount; c++)
	{
		// Random message size (multiple of 8)
		header.size = RAND(minMessageSize, maxMessageSize) & ~0x7;
		if (c == msgCount - 1)
			header.size = 0; // Last message has size 0

		_(hps_aio_write(s, &header, sizeof(header)));
		_(hps_aio_write(s, &testData[header.sequence & 0xFF], header.size));

		header.sequence++;

		if (showStatistics)
		{
			totalBytes += sizeof(header) + header.size;
			volatile uint64_t t2 = getTimestampNs();
			double dtS = ((double) (t2 - t1) / (double) 1e9);
			if (1.0 < dtS)
			{
				double MBPS = (double)(totalBytes - lastTotalBytes) / 1024 / 1024 / dtS;
				t1 = t2;
				lastTotalBytes = totalBytes;
				printf("%.3f MB/s\n", MBPS);
			}
		}
	}

	// Cleanup
	_(hps_aio_close(s));

    return 0;
}

int main(int argc, char * const*argv)
{
	// Parse options
	int opt;
	while ((opt = getopt(argc, argv, "l:c:s:v")) != -1)
	{
		switch (opt)
		{
		case 'l':
			loops = atoi(optarg);
			break;
		case 'c':
			maxMessageCount = atoi(optarg);
			break;
		case 's':
			maxMessageSize = atoi(optarg);
			break;
		case 'v':
			showStatistics = true;
			loops = 1;
			minMessageCount = 100000;
			maxMessageCount = 100000;
			minMessageSize = MAX_MSG_SIZE;
			maxMessageSize = MAX_MSG_SIZE;
			break;
		default:
			fprintf(stderr, "Usage: %s [-l loops] [-s size] [-v] [file]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	const char *name = optind < argc ? argv[optind] : "test.tmp";

	srand(time(NULL));

    // Set limits
    struct rlimit lim;
    lim.rlim_cur = RLIM_INFINITY;
    lim.rlim_max = RLIM_INFINITY;
    if (setrlimit(RLIMIT_MEMLOCK, &lim))
        perror("Failed to set RLIMIT_MEMLOCK");

	// Generate test data
	testData = (uint8_t *)malloc(maxMessageSize + 256);
	for (int i = 0; i < maxMessageSize + 256; i++)
		testData[i] = (uint8_t)i;

	for (int loop = 0; loop < loops; loop++)
	{
		remove(name);

		int msgCount = RAND(minMessageCount, maxMessageCount);
		printf("Loop %d: %d messages\n", loop, msgCount);

		_(test_aiostream_write(name, msgCount));

		if (!showStatistics)
		{
			// Verify file
			_(verify(name));
		}
	}

	printf("Test PASSED\n");

	free(testData);
	remove(name);

    return 0;
}
