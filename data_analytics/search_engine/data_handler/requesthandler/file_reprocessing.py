__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.utils import validate_json

from handlers.auth import auth
from handlers.file_reprocessing import FileReprocessingHandler
from handlers.utils import utils
from requesthandler.base import BaseRequestHandler


class FileReprocessingRequestHandler(BaseRequestHandler):

    def initialize(self):
        self.handler = FileReprocessingHandler()
        self.schema = self.load_json_schema('files_reprocess_post.schema')

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'POST')

    @auth.requires_scope(["openid"])
    def post(self):
        data = validate_json(self.request.body.decode('utf-8'), self.schema)
        result = self.handler.reprocess_all(data['file_paths'])
        utils.build_response(response=self, results=result)
