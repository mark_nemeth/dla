__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import (ENV_SPAN_ID, ENV_TRACE_ID,
                                       ENV_TRACE_OPTIONS)
from kfp import dsl
from kubernetes.client import V1ObjectFieldSelector, V1EnvVarSource
from kubernetes.client.models import V1EnvVar

from config.config_handler import config_dict
from kubeflow.steps.common import (APPINSIGHTS_KEY,
                                   DANF_ENV, AUTH, AUTH_TOKEN_URL,
                                   AUTH_DATAHANDLER_AUDIENCE,
                                   AUTH_DATAHANDLER_CLIENT_CRED_ID,
                                   AUTH_DATAHANDLER_CLIENT_CRED_SECRET,
                                   AUTH_METADATA_AUDIENCE,
                                   AUTH_METADATA_CLIENT_CRED_ID,
                                   AUTH_METADATA_CLIENT_CRED_SECRET,
                                   AUTH_SEARCH_AUDIENCE,
                                   AUTH_SEARCH_CLIENT_CRED_ID,
                                   AUTH_SEARCH_CLIENT_CRED_SECRET,
                                   SEARCH_API_BASE_URL, METADATA_API_BASE_URL,
                                   HTTP_PROXY, HTTPS_PROXY, NO_PROXY)


class TracingSidecar(dsl.Sidecar):

    NAME = "danf-tracing-sidecar"
    DOCKER_IMG = config_dict.get_value(NAME)

    @classmethod
    def create_for_sv(
            cls, target_container, task_name, version, bagfile_guid, flow_run_guid):
        """Create tracing sidecar to monitor the execution of a container

        :param target_container: name of the container to monitor
        :param task_name: task name to use for tracing and state updates
        :param version: version of the container to monitor
        :param bagfile_guid: of the file that is processed
        :param flow_run_guid: id of the flow run in which this step is executed
        :return: new instance of the TracingSidecar
        """
        step = cls._create_base_instance(
            target_container, task_name, version, bagfile_guid, flow_run_guid)

        step.add_env_variable(V1EnvVar(name="HTTP_PROXY", value=HTTP_PROXY)) \
            .add_env_variable(V1EnvVar(name="HTTPS_PROXY", value=HTTPS_PROXY)) \
            .add_env_variable(V1EnvVar(name="no_proxy", value=NO_PROXY))

        return step

    @classmethod
    def create_for_az(
            cls, target_container, task_name, version, bagfile_guid, flow_run_guid):
        """Create tracing sidecar to monitor the execution of a container

        :param target_container: name of the container to monitor
        :param task_name: task name to use for tracing and state updates
        :param version: version of the container to monitor
        :param bagfile_guid: of the file that is processed
        :param flow_run_guid: id of the flow run in which this step is executed
        :return: new instance of the TracingSidecar
        """
        step = cls._create_base_instance(
            target_container, task_name, version, bagfile_guid, flow_run_guid)
        return step



    @classmethod
    def _create_base_instance(
            cls, target_container, task_name, version, bagfile_guid, flow_run_guid):
        sidecar = TracingSidecar(target_container, task_name, version, bagfile_guid) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_ID, value=tracing.get_propagation_vars().get(ENV_TRACE_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_SPAN_ID, value=tracing.get_propagation_vars().get(ENV_SPAN_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_OPTIONS, value=tracing.get_propagation_vars().get(ENV_TRACE_OPTIONS))) \
            .add_env_variable(V1EnvVar(name="FLOW_RUN_GUID", value=flow_run_guid)) \
            .add_env_variable(V1EnvVar(name="SEARCH_API_BASE_URL", value=SEARCH_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="METADATA_API_BASE_URL", value=METADATA_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="AUTH", value=AUTH)) \
            .add_env_variable(V1EnvVar(name="AUTH_TOKEN_URL", value=AUTH_TOKEN_URL)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_AUDIENCE", value=AUTH_DATAHANDLER_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_ID", value=AUTH_DATAHANDLER_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_SECRET", value=AUTH_DATAHANDLER_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_AUDIENCE", value=AUTH_METADATA_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_ID", value=AUTH_METADATA_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_SECRET", value=AUTH_METADATA_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_AUDIENCE", value=AUTH_SEARCH_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_ID", value=AUTH_SEARCH_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_SECRET", value=AUTH_SEARCH_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="APPINSIGHTS_INSTRUMENTATIONKEY", value=APPINSIGHTS_KEY)) \
            .add_env_variable(V1EnvVar(name="DANF_ENV", value=DANF_ENV)) \
            .add_env_variable(V1EnvVar(
                name="POD_NAME",
                value_from=V1EnvVarSource(field_ref=V1ObjectFieldSelector(field_path='metadata.name')))) \
            .add_env_variable(V1EnvVar(
                name="NAMESPACE",
                value_from=V1EnvVarSource(field_ref=V1ObjectFieldSelector(field_path='metadata.namespace'))))
        return sidecar

    def __init__(self, target_container, task_name, version, bagfile_guid):
        super().__init__(
            name=self.NAME,
            image=self.DOCKER_IMG,
            command=['python', '-u', '-m', 'TracingSidecar.main'],
            args=['--container', target_container,
                  '--name', task_name,
                  '--version', version,
                  '--bagfile', bagfile_guid]
        )
