import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../Table';
import SecondTimeEntry from './SecondTimeEntry';

describe('Bagfile Table SecondTimeEntry', () => {

  it('should format time as more than 1 day', () => {
    const formattedResult = 'More than 1 day';
    const props = {time_in_seconds: 186725};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should format time as rounded hours', () => {
    const formattedResult = '21 hours';
    const props = {time_in_seconds: 76598};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should format time as rounded 10 minute step', () => {
    const formattedResult = '30 minutes';
    const props = {time_in_seconds: 1989};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should format time as rounded 5 minute step', () => {
    const formattedResult = '15 minutes';
    const props = {time_in_seconds: 879};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should format time as rounded 1 minute step', () => {
    const formattedResult = '1 minute(s)';
    const props = {time_in_seconds: 89};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should format time as less than 1 minute', () => {
    const formattedResult = 'Less than 1 minute';
    const props = {time_in_seconds: 59};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should format time as finished', () => {
    const formattedResult = 'Finished';
    const props = {time_in_seconds: 0};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should format time as not started', () => {
    const formattedResult = 'Not started';
    const props = {time_in_seconds: -1};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should create a disabled button when time in seconds does not exists', () => {
    const props = {duration: null};
    const wrapper = shallow(<SecondTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});
