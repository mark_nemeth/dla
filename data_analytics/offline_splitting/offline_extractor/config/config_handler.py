"""Config Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os
from datetime import datetime
from DANFUtils.logging import logger


class ConfigHandler:
    def __init__(self):
        self.configs_obj = {}

        try:
            path = os.path.abspath(os.path.dirname(__file__))
            with open("{}/version.json".format(path), "r") as f:
                self.configs_obj = json.load(f)

        except Exception:
            raise ValueError(
                "Config file for topics versioning not available, please provide one!"
            )

    @staticmethod
    def get_version_based_on_ts(ts, conf):

        bagfile_datetime = datetime.fromtimestamp(ts)

        logger.info("Bagfile recording started on " + str(bagfile_datetime))

        version = None

        for key, value in conf.items():
            if version is None:
                time_range_start = datetime.strptime(value['date_start'],
                                                     '%d-%m-%Y')
                time_range_end = datetime.strptime(value['date_end'],
                                                   '%d-%m-%Y')

                if (time_range_start <= bagfile_datetime <= time_range_end):
                    version = key

        return version

    @staticmethod
    def get_topic_for_version(conf, topic, version):
        return conf.get(version)['topics'].get(topic)

    def get_config_for_disengagement(self, start_ts, topics_list):

        disengagement_conf = self.configs_obj['disengagement']

        version = self.get_version_based_on_ts(start_ts, disengagement_conf)

        logger.info("Set disengagement version to: " + str(version))

        if version is None:
            raise ValueError(
                "Version cannot be set! Check metadata and version extractor_config file!"
            )

        try:
            logger.info("Set disengagement topics to: " +
                        str(disengagement_conf.get(version)['topics']))
            logger.info(
                "Set engagement handler to: " +
                str(disengagement_conf.get(version)['engagement_handler']))

            engagement_state_topic = self.get_topic_for_version(
                disengagement_conf, "engagement_state_topic", version)
            log_request_topic = self.get_topic_for_version(
                disengagement_conf, "log_request_topic", version)

            # addtional check for v2 and v3:
            # check if the renamed topic is really in the topic list, if not, switch from v2 to v1 / from v3 to v2
            # since new sw release could be deployed in different vehicles
            if version in ["v2", "v3"]:
                if engagement_state_topic not in topics_list:
                    if version == "v2":
                        version = "v1"
                    else:
                        version = "v2"
                    logger.warning(
                        "topics not available in the bagfile, set version to previous version instead."
                    )

                    engagement_state_topic = disengagement_conf.get(
                        version)['topics'].get("engagement_state_topic")
                    log_request_topic = disengagement_conf.get(
                        version)['topics'].get("log_request_topic")

                    logger.info("Set disengagement topics to: " +
                                str(disengagement_conf.get(version)['topics']))

            engagement_handler = disengagement_conf.get(
                version)['engagement_handler']

        except Exception as exp:
            raise ValueError("Topics and handler cannot be set!", exp)

        return engagement_state_topic, log_request_topic, engagement_handler
