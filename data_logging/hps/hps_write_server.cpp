#include <thread>
#include <sys/errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "common.h"
#include "hps.h"

using namespace std;

struct hpWriteServerT
{
	hps_type type = HPS_UDP;
	hpStreamT *s = NULL;
	thread handlerThread;
	bool shutdownRequest = false;

	hpWriteServerT(const char *file)
	{
		printf("FILE server writing file %s\n", file);

		string uri = "FILE:" + string(file);
		s = hps_open(uri.c_str());
    	if (s == NULL)
    	{
    		perror("AIO stream failed to open file");
    		throw exception();
    	}

		handlerThread = thread(&hpWriteServerT::run, this);
	}

	~hpWriteServerT()
	{
	    shutdownRequest = true;
	    handlerThread.join();
		// Close stream
	    if (s != NULL)
	    	hps_close(s);
	}

	void run()
	{
	    while (!shutdownRequest)
	    {
			// Begin write
			void *buffer;
			int n = hps_write_begin(s, &buffer, 8 * 1024 * 1024);
			if (n < 0)
			{
				perror("hps_write_begin() failed");
				break;
			}

			// End write
			if (0 < n)
			{
				hps_write_end(s, n);
			}
	    }
	}
};

hpWriteServerT* hps_write_start_server(const char *file)
{
	try
	{
		return new hpWriteServerT(file);
	}
	catch (exception&)
	{
		return NULL;
	}
}

int hps_write_close_server(hpWriteServerT *server)
{
    delete server;

    return 0;
}
