import React from 'react';
import {shallow} from 'enzyme/build';
import {FileTypes} from '../Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import {getTestChildBagfileList, getTestOriginalFileList} from '../../setupTests';
import {FileInformationSection} from './FileInformationSection';

describe('FileInformationSection', () => {

  it('should renders correct number of information container when open a bagfile', () => {
    const guid = '1337';
    const originalFile = getTestOriginalFileList(true).originalFiles[0];
    originalFile.guid = guid;
    const childbagfiles = getTestChildBagfileList().childBagfiles;
    for (let childbagfile of childbagfiles) {
      childbagfile.parent_guid = guid;
    }
    const props = {
      fileType: FileTypes.ORIGINALFILE,
      overviewFile: originalFile,
      bagfile: originalFile,
      childbagfiles: childbagfiles,
      selectedChildbagfileGuid: null,
      showError: jest.fn()
    };
    const wrapper = shallow(<FileInformationSection {...props}/>);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('LabelContainer')).toHaveLength(10);
    expect(wrapper.find('ScrollList').shallow().find('tr')).toHaveLength(childbagfiles.length);
  });

  it('should renders correct number of information container when open a childbagfile', () => {
    const guid = '1337';
    const originalFile = getTestOriginalFileList(true).originalFiles[0];
    originalFile.guid = guid;
    const childbagfile = getTestChildBagfileList(true).childBagfiles[0];
    childbagfile.parent_guid = guid;
    const props = {
      fileType: FileTypes.CHILDBAGFILE,
      overviewFile: childbagfile,
      parentBagfile: originalFile,
      childbagfiles: childbagfile,
      selectedChildbagfileGuid: null,
      showError: jest.fn()
    };
    const wrapper = shallow(<FileInformationSection {...props}/>);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('LabelContainer')).toHaveLength(9);
  });

  it('should only render name when no file was provided', () => {
    const props = {
      fileType: FileTypes.ORIGINALFILE,
      overviewFile: null,
    };
    const wrapper = shallow(<FileInformationSection {...props}/>);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span')).toHaveLength(1);
  });

  it('should call show banner when clock on copy path component', () => {
    const bannerAction = jest.fn();
    const originalFile = getTestOriginalFileList(true).originalFiles[0];
    const childbagfile = getTestChildBagfileList(true).childBagfiles[0];
    const props = {
      fileType: FileTypes.ORIGINALFILE,
      overviewFile: originalFile,
      bagfile: originalFile,
      childbagfiles: childbagfile,
      selectedChildbagfileGuid: null,
      showError: bannerAction
    };
    const wrapper = shallow(<FileInformationSection {...props}/>);
    expect(wrapper.exists()).toBe(true);
    const pathLabelContainer = wrapper.find('LabelContainer').at(1).shallow();
    pathLabelContainer.find('label').at(1).simulate('click');
    expect(bannerAction).toHaveBeenCalled();
  });
});