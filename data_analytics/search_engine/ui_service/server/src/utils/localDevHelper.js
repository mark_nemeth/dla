import configHandlerSingleton from '../configHandler';
import storeProvider from '../models/storeProvider';
import {isObjectValid} from './helperFunctions';

/* Local dev helper
* Class that helps Devs to work with the current DANF setup locally.
* Includes helper methods for login and mock some filter methods from API
*/
export function getDevModeLocalRedisUserTokens() {
  return storeProvider._getSingleRandomTokenAsync().then(userTokenKey => {
      return storeProvider.getUserTokenDataAsync(userTokenKey).then((userTokenData) => {
        return {'Authorization': userTokenData.token_type + ' ' + userTokenData.access_token}
      });
    })
    .catch(err => console.log(`LOCAL DEV MODE ERROR: ${err}`));
}

export function localDevGetSingleFileIfNecessary(req, type, data) {
  // Check if we are in local dev mode and has valid data
  if (!configHandlerSingleton.isNodeEnvLocal() || !isObjectValid(req.query)) return data;

  switch (type) {
    case 'bagfiles':
      return getFilesWithGUI(req.query.guid, 'guid', type, data.bagfiles);
    case 'originalfiles':
      return getFilesWithGUI(req.query.guid, 'guid', type, data.originalFiles);
    case 'alwaysonfiles':
      return getFilesWithGUI(req.query.guid, 'guid', type, data.alwaysonFiles);
    case 'drives':
      return getFilesWithGUI(req.query.guid, 'guid', type, data.drives);
    default:
      if (req.query.hasOwnProperty('guid')) {
        return getFilesWithGUI(req.query.guid, 'guid', type, data.childBagfiles);
      } else {
        return getFilesWithGUI(req.query.parent_guid, 'parent_guid', type, data.childBagfiles);
      }
  }

  function getFilesWithGUI(searchedGUID, attKey, typeKey, data) {
    let returnData = [];
    for (let file of data) {
      if (file[attKey] === searchedGUID) returnData.push(file);
    }
    if (returnData.length === 0) {
      data[0][attKey] = searchedGUID;
      returnData.push(data[0]);
    }
    return {[typeKey]: returnData, 'total': returnData.length};
  }
}

