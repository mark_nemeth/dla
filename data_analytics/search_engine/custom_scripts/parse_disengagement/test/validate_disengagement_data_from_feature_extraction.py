__copyright__ = """COPYRIGHT: (c) 2017-2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import pymongo


def get_bag_data(db):
    print("Fetching bag data")
    bag_col = db["bagfiles"]
    bag_pipeline = [{
        "$match": {
            "file_type": "ALWAYSON"
        }
    }, {
        "$project": {
            "guid":
            "$guid",
            "number_of_total_disengagements":
            "$custom_attributes.disengagement_report.number_of_total_disengagements",
            "number_of_planned_disengagements": {
                "$subtract": [
                    "$custom_attributes.disengagement_report.number_of_total_disengagements",
                    {
                        "$add": [
                            "$custom_attributes.disengagement_report.number_of_manual_unplanned_disengagements",
                            "$custom_attributes.disengagement_report.number_of_error_disengagements"
                        ]
                    }
                ]
            },
            "number_of_manual_unplanned_disengagements":
            "$custom_attributes.disengagement_report.number_of_manual_unplanned_disengagements",
            "number_of_error_disengagements":
            "$custom_attributes.disengagement_report.number_of_error_disengagements",
            "total_engaged_distance":
            "$custom_attributes.disengagement_report.total_engaged_distance"
        }
    }]

    return list(bag_col.aggregate(bag_pipeline))


def get_feature_data(db, bag_guids):
    print("Fetching feature data")
    feature_bucket_col = db["feature_buckets"]
    feature_pipeline = [{
        "$match": {
            "bagfile_guid": {
                "$in": bag_guids
            }
        }
    }, {
        "$unwind": "$features"
    }, {
        "$group": {
            "_id": "$bagfile_guid",
            "number_of_planned_disengagements": {
                "$sum": "$features.number_of_planned_disengagement"
            },
            "number_of_manual_unplanned_disengagements": {
                "$sum": "$features.number_of_manual_unplanned_disengagement"
            },
            "number_of_error_disengagements": {
                "$sum": "$features.number_of_error_disengagement"
            },
            "total_engaged_distance": {
                "$sum": "$features.engaged_distance"
            }
        }
    }, {
        "$project": {
            "number_of_total_disengagements": {
                "$add": [
                    "$number_of_planned_disengagements",
                    "$number_of_manual_unplanned_disengagements",
                    "$number_of_error_disengagements"
                ]
            },
            "number_of_planned_disengagements":
            "$number_of_planned_disengagements",
            "number_of_manual_unplanned_disengagements":
            "$number_of_manual_unplanned_disengagements",
            "number_of_error_disengagements":
            "$number_of_error_disengagements",
            "total_engaged_distance": "$total_engaged_distance"
        }
    }]
    return list(db.feature_buckets.aggregate(feature_pipeline))


def compare_stats(bag_stats, feature_stats):
    if bag_stats['number_of_total_disengagements'] == feature_stats['number_of_total_disengagements'] \
            and bag_stats['number_of_planned_disengagements'] == feature_stats['number_of_planned_disengagements'] \
            and bag_stats['number_of_manual_unplanned_disengagements'] == feature_stats[
        'number_of_manual_unplanned_disengagements'] \
            and bag_stats['number_of_error_disengagements'] == feature_stats['number_of_error_disengagements']:
        return
    print(f"Stats differ for bagfile {bag_stats['guid']}")
    print(f"Bag data: {bag_stats}")
    print(f"Feature data: {feature_stats}")
    print("=" * 30)


client = pymongo.MongoClient("mongodb://localhost:27017/")
db = client["metadata"]
bag_data = get_bag_data(db)
bag_guids = [r["guid"] for r in bag_data]

feature_data = get_feature_data(db, bag_guids)
print(len(bag_data))
print(len(feature_data))

print("Processing results")
for f in feature_data:
    for b in bag_data:
        if f["_id"] == b["guid"]:
            compare_stats(b, f)
