"""Splitting Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from handlers.AbstractHandler import AbstractHandler
from constants import *


class SplittingHandler(AbstractHandler):

    def __init__(self, config, tmt_path):
        super(SplittingHandler, self).__init__(config, SCHEMA_FILE_SPLITTING, tmt_path)

    def validate_config_fields(self):
        if 'time' in self.config:
            for t in self.config['time']:
                assert t['start'] < t['end'], "End timestamp {} must be greater than the start timestamp {}."
                digits_start = self.count_digits(t['start'])
                digits_end = self.count_digits(t['end'])
                if digits_start == 13:
                    t['start'] = t['start'] * 1000000
                elif digits_start != 19:
                    raise ValueError("'start' must be provided as millisecond or nanosecond timestamp")
                if digits_end == 13:
                    t['end'] = t['end'] * 1000000
                elif digits_end != 19:
                    raise ValueError("'end' must be provided as millisecond or nanosecond timestamp")

    def build_cmd_list(self):
        cmd_list = []
        if 'split_size' in self.config:
            cmd = [self.tmt_path]
            cmd.extend(["--in", self.config['input_file_paths'][0]])
            cmd.extend(["--split", str(self.config['split_size'])])
            cmd.extend(["--out", self.config['output_file_path']])
            cmd_list.append(cmd)
        else:
            for t in self.config['time']:
                cmd = [self.tmt_path]
                cmd.extend(["--in", self.config['input_file_paths'][0]])
                cmd.extend(["--time", str(t['start']), str(t['end'])])
                cmd.extend(
                    ["--out", "{}".format(self.add_file_name_appendix(self.config['output_file_path'], t['start']))])
                cmd_list.append(cmd)
        return cmd_list
