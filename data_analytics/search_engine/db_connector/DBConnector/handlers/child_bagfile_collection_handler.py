"""ChildBagfile Collection Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os

from DANFUtils.exceptions import DANFBaseException
from DANFUtils.logging import logger
from DANFUtils.utils import validate_json
from DBConnector.exceptions import IndexOverwriteException, BulkOperationException
from DBConnector.handlers.bagfile_collection_handler_base import BagfileCollectionBaseHandler
from DBConnector.utils import calculate_request_size, MAXIMUM_REQUEST_BATCH_SIZE, delete_all_documents_from_collection
from pymongo import UpdateOne
from pymongo.errors import BulkWriteError


class ChildBagfileCollectionHandler(BagfileCollectionBaseHandler):
    """
    ChildBagfileCollectionHandler
    """

    # ChildBagfile index
    CHILD_BAG_COLLECTION_NAME = 'child_bagfiles'
    CHILD_BAG_INDEX_NAME = 'child_bagfiles_guid_index'
    CHILD_BAG_START_INDEX_NAME = 'child_bagfiles_start_index'
    CHILD_BAG_INDEX_FIELD_GUID = 'guid'
    CHILD_BAG_INDEX_FIELD_START = 'start'
    CHILD_BAG_PARENT_GUID = 'parent_guid'

    def __init__(self, db):
        BagfileCollectionBaseHandler.__init__(self, db,
                                              self.CHILD_BAG_COLLECTION_NAME,
                                              self.CHILD_BAG_INDEX_NAME,
                                              self.CHILD_BAG_START_INDEX_NAME)
        try:
            base_path = os.path.abspath(os.path.dirname(__file__))
            schema_file = f"{base_path}/../schema/child_bagfile_query.schema"

            with open(schema_file) as f:
                schema_query = f.read()
        except (IOError, OSError) as e:
            error_message = 'Could not read child bagfile query schema'
            logger.exception(error_message)
            return
        try:
            self.schema_query = json.loads(schema_query)
        except ValueError as e:
            error_message = 'Bad schema: unable to parse child bagfile query schema'
            logger.exception(error_message)

    def add(self, child_bagfiles):
        assert isinstance(
            child_bagfiles,
            list), 'Data inserted into db has to be a list of dicts'
        assert all(
            isinstance(x, dict) for x in
            child_bagfiles), 'Data inserted into db has to be a list of dicts'
        col = self.db[self.CHILD_BAG_COLLECTION_NAME]

        try:
            num_docs = len(child_bagfiles)

            request_size = calculate_request_size(child_bagfiles)
            logger.debug("Adding " + str(num_docs) +
                         " documents to collection \"" +
                         self.CHILD_BAG_COLLECTION_NAME +
                         "\" (Request size: {:.2f}".format(request_size) +
                         " kb)")
            col.insert_many(child_bagfiles)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

        for x in child_bagfiles:
            del x['_id']

    def get(self, guids):
        col = self.db[self.CHILD_BAG_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        request = {self.CHILD_BAG_INDEX_FIELD_GUID: {"$in": guids}}
        request_size = calculate_request_size(request)
        logger.debug("Retrieving " + str(len(guids)) +
                     " documents from collection \"" +
                     self.CHILD_BAG_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {'_id': 0})

    def get_distinct_field_values(self, field: str):
        return self.col.distinct(field)

    def get_max_field_value(self, field: str):
        try:
            return list(self.col.aggregate([{"$group": {"_id": None, "max": {"$max": "$" + field}}}]))[0].get("max",
                                                                                                              None)
        except IndexError:
            return None

    def get_min_field_value(self, field: str):
        try:
            return list(self.col.aggregate([{"$group": {"_id": None, "min": {"$min": "$" + field}}}]))[0].get("min",
                                                                                                              None)
        except IndexError:
            return None

    def search(self, query=None, limit=100, offset=0, sortby="_id"):
        col = self.db[self.CHILD_BAG_COLLECTION_NAME]
        assert isinstance(query, dict), 'Expected dict query'
        assert isinstance(limit, int), 'Expected int limit'
        assert isinstance(offset, int), 'Expected int offset'
        assert isinstance(sortby, (str, list)), 'Expected str or list sortby'
        try:
            request = validate_json(json.dumps(query), self.schema_query)
        except DANFBaseException as e:
            error_message = 'Bad request: ' + str(e)
            logger.exception(error_message)
            return
        request_size = calculate_request_size(request)
        logger.debug("Searching documents in collection \"" +
                     self.CHILD_BAG_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {
            '_id': 0
        }).sort(sortby).skip(offset).limit(limit)

    def update(self, guids, request_data, create_if_missing=False):
        col = self.db[self.CHILD_BAG_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        assert isinstance(request_data,
                          list), 'Expected a list of sequence dicts.'
        assert all(isinstance(x, dict)
                   for x in request_data), 'Expected a list of sequence dicts.'
        assert isinstance(create_if_missing,
                          bool), 'create_if_missing has to be of type bool'
        assert len(guids) == len(
            request_data
        ), 'List guids and list sequence_data should be of same length'
        for d in request_data:
            if self.CHILD_BAG_INDEX_FIELD_GUID in d.keys():
                msg = 'Overwriting index fields is not permitted!'
                logger.error(msg)
                raise IndexOverwriteException(msg)

        requests = []
        for g, d in zip(guids, request_data):
            # Add GUID to dict and create update request
            requests.append(
                UpdateOne({self.CHILD_BAG_INDEX_FIELD_GUID: g},
                          {"$set": {
                              **d, self.CHILD_BAG_INDEX_FIELD_GUID: g
                          }},
                          upsert=create_if_missing))

        try:
            # TODO: Compute request size
            logger.debug("Updating  " + str(len(guids)) +
                         " documents in collection \"" +
                         self.CHILD_BAG_COLLECTION_NAME + "\"")
            col.bulk_write(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

    def delete(self, guids):
        col = self.db[self.CHILD_BAG_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'

        num_docs = len(guids)
        if num_docs > 0:
            request = {'guid': {"$in": guids}}
            request_size = calculate_request_size(request)
            logger.debug("Deleting  " + str(num_docs) +
                         " documents from collection \"" +
                         self.CHILD_BAG_COLLECTION_NAME +
                         "\" (Request size: {:.2f}".format(request_size) +
                         " kb)")
            if num_docs <= MAXIMUM_REQUEST_BATCH_SIZE:
                col.delete_many(request)
            else:
                # batch delete
                logger.debug(
                    "Number of delete operations exceeds maximum for single request."
                    + "Performing document deletion in batches of size " +
                    str(MAXIMUM_REQUEST_BATCH_SIZE))
                i = 0
                while i < num_docs:
                    start_id = i
                    i += MAXIMUM_REQUEST_BATCH_SIZE
                    end_id = i
                    if end_id >= num_docs:
                        end_id = num_docs
                    logger.debug("Deleting batch objects " + str(end_id) +
                                 " of " + str(num_docs))
                    request = {'guid': {"$in": guids[start_id:end_id]}}
                    col.delete_many(request)

    def delete_all(self):
        delete_all_documents_from_collection(self.CHILD_BAG_COLLECTION_NAME,
                                             self.db)

    def get_by_bagfile_guids(self, bagfile_guids):
        col = self.db[self.CHILD_BAG_COLLECTION_NAME]
        assert isinstance(bagfile_guids,
                          list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in bagfile_guids), 'Expected a list of guid strings.'
        request = {self.CHILD_BAG_PARENT_GUID: {"$in": bagfile_guids}}
        request_size = calculate_request_size(request)
        logger.debug("Retrieving " + str(len(bagfile_guids)) +
                     " documents from collection \"" +
                     self.CHILD_BAG_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {'_id': 0})
