"""Handler class for bagfiles"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List

from DANFUtils.utils import generate_bag_guid

from handlers.bagfile_flag_handler import BagFilesFlagHandler
from handlers.db_data_handler import DB_Data_Handler


class BagFilesHandler:

    def __init__(self):
        self.db_data_handler = DB_Data_Handler()
        self.bagfile_flag_handler = BagFilesFlagHandler()

    def add_bagfile_to_db(self, bagfile):
        bagfile['guid'] = generate_bag_guid(bagfile['link'])
        bagfile['state'] = []
        self.db_data_handler.add_bagfiles([bagfile])
        return bagfile['guid']

    def get_bagfile_from_db(self, guid, whitelist=None):
        whitelist = whitelist or []
        return self._filter_bag_properties(
            self.db_data_handler.get_bagfiles([guid])[0], whitelist)

    def update_bagfile_in_db(self, guid, data):
        self.db_data_handler.update_bagfiles([guid], [data])

    def delete_bagfile_from_db(self, guid, cascade_to_list, hard_delete):
        self.db_data_handler.delete_bagfiles([guid], cascade_to_list, hard_delete)
        self.bagfile_flag_handler.add_flag(guid, "delete")

    def update_field(self, bag_guids, values, field, append=False):
        self.db_data_handler.update_field(bag_guids, values, field, append)

    def add_state_event(self, guid, event):
        """Appends the given state event to the state event list of the
        metadata of the bagfile with the given guid.

        :param guid: bagfile to update
        :param event: to append to the bagfile metadata
        :return: None
        """
        bagfile = self.db_data_handler.get_bagfiles([guid])[0]

        if 'state' not in bagfile:
            bagfile['state'] = []
        bagfile['state'].append(event)

        # remove not updateable fields
        bagfile.pop('guid', None)

        self.update_bagfile_in_db(guid, bagfile)

    def add_custom_attributes(self, guid, account_name, data):
        """Add the custome attributes to the bagfile from the external extractor

        :param guid: bagfile to update
        :param account_name: name of the extractor
        :param data: custom attributes to be added to the bagfile
        :return: None
        """
        update_query = dict()
        for key, value in data.custom_attributes.items():
            update_query['custom_attributes.' + key] = value
        self.update_bagfile_in_db(guid, update_query)

    def _filter_bag_properties(self, child_bagfile, whitelist=[]):
        filter_list = ["sequences", "childbagfiles"
                       ]  # this properties will be filtered out of a bagfile.
        for white in whitelist:  # whitelist allow to modify the filter.
            if white in filter_list:
                filter_list.remove(white)
        for filter_field in filter_list:
            if filter_field in child_bagfile:
                del child_bagfile[filter_field]
        ret = [child_bagfile]
        return ret
