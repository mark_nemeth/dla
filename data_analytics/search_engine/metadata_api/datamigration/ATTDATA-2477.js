/*
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
*/

// https://athena.daimler.com/jira/browse/ATTDATA-2477
// https://athena.daimler.com/jira/browse/ATTDATA-3559
// create indices in mongo DB

db.bagfiles.createIndex({"link": 1});
db.bagfiles.createIndex({"file_name": 1});
db.bagfiles.createIndex({"file_type": 1});
db.bagfiles.createIndex({"drive_types": 1});
db.bagfiles.createIndex({"size": 1});
db.bagfiles.createIndex({"num_messages": 1});
db.bagfiles.createIndex({"start": 1});
db.bagfiles.createIndex({"duration": 1});
db.bagfiles.createIndex({"vehicle_id_num": 1});
db.bagfiles.createIndex({"topics": 1});
db.bagfiles.createIndex({"origin": 1});
db.bagfiles.createIndex({"processing_state": 1});
db.bagfiles.createIndex({"processing_priority": 1});

db.child_bagfiles.createIndex({"link": 1});
db.child_bagfiles.createIndex({"file_name": 1});
db.child_bagfiles.createIndex({"size": 1});
db.child_bagfiles.createIndex({"start": 1});
db.child_bagfiles.createIndex({"duration": 1});
db.child_bagfiles.createIndex({"vehicle_id_num": 1});
db.child_bagfiles.createIndex({"origin": 1});
db.child_bagfiles.createIndex({"parent_guid": 1});

db.extractor_requests.createIndex({"bagfile_guid": 1});

db.filecatalog.createIndex({"urls.accessor_url": 1});
