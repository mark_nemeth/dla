# splitter

Splitter package contains 3 modes:
* splitter
* stripper
* migrate

*Note*: set the environment variable(in DOCKERFILE, Kubernetes yaml file, or locally export the env): MODE; default is splitter


## First steps:
1. create `Personal Access Tokens` in AzureDevOps page
2. copy the token you created.
3. add to the general url like this(it looks like: https://YOUR_TOKEN@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/ )

```console
export index_url_with_access=https://<YOUR_KEY>@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```

## Build docker:
```console
docker build -t dlaregistrydev.azurecr.io/splitter:<version> . --network=host --no-cache --build-arg https_proxy=https://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access
```

## Run docker (locally):
```console
docker run -v /folder/with/input/file:/input -v /folder/to/save/output/files:/output -e BAGFILE_GUID=xxxxxx -e BAGFILE_PATH=xxxxxxxxxxx -e OUTPUT_FOLDER=/output/folder -e METADATA_API_BASE_URL=http://xx.xx.xxxx.xx:8887/ -e SEARCH_API_BASE_URL=http://xx.xx.xxx.xx:8889/  -e BVA_THRESHOLD=x -e MODE=stripper -e STRIPPER_PROFILE=fusion_io --network host dlaregistry.azurecr.io/splitter:${latest_version}
```
where 
* /folder/with/input/file: the folder with input bagfile to be splitted on the host machine, mapped to /input folder within the docker
* /folder/to/save/output/files: the desired folder to save splitted bagfiles, it is mapped to OUTPUT_FOLDER in the docker container. The env variable OUTPUT_FOLDER must be set
* BAGFILE_GUID is the guid of the input bagfile
* BAGFILE_PATH is the name of the bagfile inside /folder/with/input/file
* METADATA_API_BASE_URL is the url of the Metadata API
* SEARCH_API_BASE_URL is the url of the Search API
* Set MODE to splitter or stripper to switch the operation mode. If the variable is not set, the splitter mode will be executed on default
* If STRIPPER_PROFILE is set to a profile, one stripped bagfile with the profile will be generated. If the variable is not set, the default profile list will be used to generate a several bagfiles (see STRIPPER_PROFILES in config/constants.py)
* DANF_ENV must be set, since the origin/timezone will be derived from it. If it is set to "sv_prod", the timezone for SV will be used
* SILENT_TESTING_SUBFOLDER can be set, if the childbagfiles for silent testing events should be created in a separate subfolder, e.g. SILENT_TESTING_SUBFOLDER="ST"
* LOG_FILE can be set, with this you tell the python-logging to log to a file, e.g. 'log'

Example:
```console
docker run -v /mapr/738.mbc.de/data/input/rd/athena/08_robotaxi:/input -v $PWD/output:/output -e BAGFILE_GUID=f0d97bc5-c2bf-4e65-b52e-30dc13bc9907 -e BAGFILE_PATH=/input/plog/WDD2221591A411952/2019/05/27/20190527_150227_5214_104-12_CTD_Mission-3_3_ORIGINAL_STRIPPED_cid_bageval.bag -e OUTPUT_FOLDER=/output -e METADATA_API_BASE_URL=http://53.54.129.25:8887/ -e SEARCH_API_BASE_URL=http://53.54.129.25:8889/ -e BVA_THRESHOLD=10 -e APPINSIGHTS_INSTRUMENTATIONKEY=APPINSIGHTS_DISABLE_KEY -e DANF_ENV=prod -e SILENT_TESTING_SUBFOLDER="ST" --network host dlaregistrydev.azurecr.io/splitter:${latest_version}
```
