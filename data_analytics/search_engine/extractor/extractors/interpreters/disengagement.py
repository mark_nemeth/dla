"""Disengagement Interepeter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from config.config_handler import ConfigHandler
from extractors.base_interpreter import Interpreter
from extractors.utils import charlist2str, aracom_timestamp2ms

logger = logging.getLogger(__name__)

BVA_DISENGAGEMENT = 10
SECONDS_BEFORE_DISENGAGEMENT = 5
SECONDS_AFTER_DISENGAGEMENT = 5


class DisengagementInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.engagement_handler = None
        self.last_state = None
        self.current_state = None
        self.disengagements = []
        self.timestamp = None
        self.logrequests = []
        self.bva = BVA_DISENGAGEMENT

    def get_topics_and_handler(self, bagfile_start_ts, topics_list):
        config = ConfigHandler()
        evs_topic, log_topic, eng_handler = config.get_config_for_disengagement(
            bagfile_start_ts, topics_list)
        self.engagement_handler = eng_handler
        return evs_topic, log_topic

    def feed_new_msg(self, topic, message, t):
        if hasattr(message, "ad_engagement_state") or hasattr(message, "eSystemStatus"):
            self.find_disengagement(message, t)
        elif hasattr(message, "cause"):
            self.collect_log_msg(message, t)
        else:
            logger.exception("Topic does not contain engagement state or log request, check config!")

    def set_engagement_state(self, message, t):
        try:
            self.timestamp = aracom_timestamp2ms(message.header.timestamp)
            self.last_state = self.current_state

            # Set engagement state depending on engagement handler! (different logic for CAL and SEM)
            # 0: AD System is initializing
            # 1: AD System is ready to engage
            # 2: AD System is in the autonomous driving mode => Engaged
            # 3: AD System has error
            # disenagement: transition from 2 -> 1 (manual disengagement) or 2 -> 3 (error disengagement)
            if (self.engagement_handler == "CAL"):
                self.current_state = message.ad_engagement_state
            elif (self.engagement_handler == "SEM"):
                if (message.eSystemStatus.eSystemState != 2):  # Initializing
                    self.current_state = 0
                elif (message.eSystemStatus.eSystemState == 2
                      and message.eSystemStatus.eDegradationLevel == 2 and
                      message.eSystemStatus.eOperationMode == 4):  # Engaged
                    self.current_state = 2
                elif (message.eSystemStatus.eSystemState == 2
                      and message.eSystemStatus.eDegradationLevel == 2
                      and message.eSystemStatus.eOperationMode != 4):  # Ready
                    self.current_state = 1
                elif (message.eSystemStatus.eSystemState == 2
                      and message.eSystemStatus.eDegradationLevel !=
                      2):  # System is degraded -> Error!
                    self.current_state = 3
            else:
                raise ValueError("Engagement handler not set properly, must be CAL or SEM!")
        except BaseException:
            logger.exception("Engagement state cannot be set!")

    def find_disengagement(self, message, t):
        self.set_engagement_state(message, t)

        attributes = {}
        if (self.last_state == 2 and self.current_state != self.last_state):
            attributes["ts"] = self.timestamp
            if (self.current_state == 1):
                logger.info(f"manual disengagement found, ts: {self.timestamp}")
                attributes["type"] = "Manual"
            elif (self.current_state == 3):
                logger.info(f"error disengagement found, ts: {self.timestamp}")
                attributes["type"] = "Error"
            # Engaged -> Initializing should actually never happen
            elif (self.current_state == 0):
                logger.info(f"unknown disengagement found, ts: {self.timestamp}")
                attributes["type"] = "Unknown"
            self.disengagements += [attributes]

    def collect_log_msg(self, message, t):
        try:
            # Filter only messages from CAL (source = 1) or SEM (source = 11).
            if (self.engagement_handler == "CAL"):
                source = 1
            elif (self.engagement_handler == "SEM"):
                source = 11
            else:
                raise ValueError("Engagement handler not set properly, must be CAL or SEM!")
            if (message.cause == 0
                    or message.cause == 1) and message.source == source:
                attributes = {}
                attributes["ts"] = aracom_timestamp2ms(message.header.timestamp)
                attributes["cause"] = message.cause
                attributes["details"] = charlist2str(message.details)
                self.logrequests += [attributes]
        except Exception:
            logger.exception("LogRequest Message doesn't have required values")
            return None

    def join_disengagement_with_logrequest(self):

        for disengagement in self.disengagements:
            result = disengagement

            ts_diff_min = None
            closest_logrequest = None
            for logrequest in self.logrequests:
                if ((ts_diff_min is None)
                        or (abs(disengagement["ts"] - logrequest["ts"]) <
                            ts_diff_min)):
                    ts_diff_min = abs(disengagement["ts"] - logrequest["ts"])
                    closest_logrequest = logrequest

            # ts difference between disengagement and corresponding logrequest message should not be greater than 0.5s
            try:
                if ts_diff_min < 500:
                    result["error_msg"] = closest_logrequest["details"]
                else:
                    logger.info(
                        f"No logrequest message found for the disengagement at {disengagement['ts']}," \
                        f"closest logrequest message has a time diff of {ts_diff_min} ns")
                    result["error_msg"] = "No associated LogRequest msg"
            except Exception as e:
                result["error msg"] = "LogRequest topic unavailable"
                logger.warning("LogRequest topic unavailable. (ignored for processing)")

            self.event_list += [result]
            self.calc_start_end_ts_snippet()

    def calc_start_end_ts_snippet(self):
        for event in self.event_list:
            event["start_ts"] = int(event["ts"] - (SECONDS_BEFORE_DISENGAGEMENT * 1e3))
            event["end_ts"] = int(event["ts"] + (SECONDS_AFTER_DISENGAGEMENT * 1e3))
