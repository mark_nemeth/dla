"""Setup file"""
from setuptools import setup, find_packages

setup(name='offline_extractor',
      version='0.0.2',
      packages=find_packages(),
      install_requires=[
          'pycrypto',
          'gnupg',
          'ContainerClient>=0.6.98',
          'DANFTracingClient>=0.1.3',
          'DANFUtils>=0.2.29'
      ])
