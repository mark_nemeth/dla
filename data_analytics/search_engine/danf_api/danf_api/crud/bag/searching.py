"""Bag related high level crud operations."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import List, Optional
from uuid import UUID

from .filters import construct_bag_filter
from ...crud.base import get_record, count_records, find_records
from ...db.mongodb import get_bagfile_collection, get_childbagfile_collection
from ...models.bag import BagType, Bag, ManyBags, BagQueryParams, BagFilterParams, DEFAULT_BAGFILE_PROJECTION, \
    DEFAULT_CHILDBAGFILE_PROJECTION
from ...models.base import OpValue


async def get_bag_by_id(guid: UUID, fields: Optional[List[OpValue]] = None) -> Bag:
    """Get a single bag.

    Args:
        guid: guid of the bag
        fields: modifies the default set of fields to be returned

    Returns:
        the metadata of the matching bag

    Raises:
        KeyError: if there is no bag with the given guid
    """
    try:
        return await _get_bagfile_bag(guid=guid, fields=fields)
    except KeyError:
        pass

    try:
        return await _get_childbagfile_bag(guid=guid, fields=fields)
    except KeyError:
        pass

    raise KeyError(f"No bag was found with guid: {guid}")


async def _get_bagfile_bag(guid: UUID, fields: Optional[List[OpValue]]) -> Bag:
    record = await get_record(
        collection=get_bagfile_collection(),
        key="guid",
        value=str(guid),
        default_projection=DEFAULT_BAGFILE_PROJECTION,
        projection_modifiers=fields,
    )
    return Bag(bag_type=BagType.BAGFILE, **record)


async def _get_childbagfile_bag(guid: UUID, fields: Optional[List[OpValue]]) -> Bag:
    record = await get_record(
        collection=get_childbagfile_collection(),
        key="guid",
        value=str(guid),
        default_projection=DEFAULT_CHILDBAGFILE_PROJECTION,
        projection_modifiers=fields,
    )
    return Bag(bag_type=BagType.CHILDBAGFILE, **record)


async def find_bags(query_params: BagQueryParams) -> ManyBags:
    """Get bags matching the criteria.

    Args:
        query_params: object holding the query settings

    Returns:
        object holding the matching bags and some extra information about the query as a whole
    """
    get_bagfile_bags = query_params.bag_type != BagType.CHILDBAGFILE
    get_childbagfile_bags = query_params.bag_type != BagType.BAGFILE

    matching_bagfile_bag_count = await _count_bagfile_bags(query_params.filter_params) if get_bagfile_bags else 0
    matching_childbagfile_bag_count = (
        await _count_childbagfile_bags(query_params.filter_params) if get_childbagfile_bags else 0
    )

    matched_bags = ManyBags(bags=[], total_count=matching_bagfile_bag_count + matching_childbagfile_bag_count)

    if get_bagfile_bags and query_params.offset < matching_bagfile_bag_count:
        matched_bags.bags.extend(await _find_bagfile_bags(query_params=query_params))

    if not (missing_count := query_params.limit - len(matched_bags.bags)):
        return matched_bags

    if get_childbagfile_bags:
        query_params.limit = missing_count
        query_params.offset = max(0, query_params.offset - matching_bagfile_bag_count)
        matched_bags.bags.extend(await _find_childbagfile_bags(query_params=query_params))

    return matched_bags


async def _count_bagfile_bags(filter_params: BagFilterParams) -> int:
    return await count_records(
        collection=get_bagfile_collection(),
        filter_constructor=construct_bag_filter,
        filter_params=filter_params,
    )


async def _count_childbagfile_bags(filter_params: BagFilterParams) -> int:
    return await count_records(
        collection=get_childbagfile_collection(),
        filter_constructor=construct_bag_filter,
        filter_params=filter_params,
    )


async def _find_bagfile_bags(query_params: BagQueryParams) -> List[Bag]:
    records = find_records(
        collection=get_bagfile_collection(),
        filter_constructor=construct_bag_filter,
        default_projection=DEFAULT_BAGFILE_PROJECTION,
        query_params=query_params,
    )
    return [Bag(bag_type=BagType.BAGFILE, **r) async for r in records]


async def _find_childbagfile_bags(query_params: BagQueryParams) -> List[Bag]:
    records = find_records(
        collection=get_childbagfile_collection(),
        filter_constructor=construct_bag_filter,
        default_projection=DEFAULT_CHILDBAGFILE_PROJECTION,
        query_params=query_params,
    )
    return [Bag(bag_type=BagType.CHILDBAGFILE, **r) async for r in records]
