__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from ContainerClient import ContainerClient
from DANFUtils.exceptions import RESTException
from DANFUtils.logging import logger


class RefillQuotaTask:

    def __init__(self):
        self.client = ContainerClient()

    def __call__(self):
        try:
            self.client.refill_quota()
        except RESTException as e:
            logger.exception(f"Failed to refill quota. Reason: {e}")
