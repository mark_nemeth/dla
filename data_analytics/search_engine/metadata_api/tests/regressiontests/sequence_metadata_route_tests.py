"""Regressiontests for sequence routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import random as rd
import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from handlers.sequences import SequenceHandler
from tests.regressiontests.testutils import get_testdb_connector

# testdata
# can change the localhost into external ip:
bagfile_url = 'http://localhost:8887/bagfile/'
sequence_url = 'http://localhost:8887/sequence/'
https_url = 'https://localhost:8443/sequence/'


rd.seed("test")

raw_data_bagfile = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1"
}
raw_data_sequence = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "index": 0,
    "bagfile_guid": raw_data_bagfile['guid'],
    "start": 1475272800000,
    "end": 1475272800000,
    "type": "testdata",
    "extractor": "ext1"
}


# def getCert():
#     try:
#         cert = config_dict.get_value("testing_ssl_cert", "")
#         return cert
#     except (IOError, OSError) as e:
#         error_message = 'Could not read cert path from config!'
#         logger.error(error_message)
#         return


# cert = getCert()


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
        con.db['sequences'].delete_many({'type': raw_data_sequence['type']})


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
        con.db['sequences'].delete_many({'type': raw_data_sequence['type']})


class TestSequencePOST(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_sequence_POST(self):
        data = raw_data_sequence.copy()
        del data['guid']
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        res = requests.post(url=sequence_url, json=data)
        self.assertEqual(201, res.status_code, 'Sequence post failed')
        self.assertRegex(
            res.headers['Location'],
            '/sequence/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_sequence_POST_invalid_bagfile_guid(self):
        data = {
            "version": "1.0",
            "bagfile_guid": "12341234-1234-1234-1234-123412341235",
            "start": 1475272800000,
            "end": 1475272800001,
            "type": "basic",
            "extractor": "ext1"
        }

        res = requests.post(url=sequence_url, json=data)
        self.assertEqual(500, res.status_code,
                         'Fake bagfile_guid not detected')

    def test_sequence_POST_missing_attributes(self):
        data = raw_data_sequence.copy()
        del data['guid']
        del data['start']
        del data['end']

        res = requests.post(url=sequence_url, json=data)
        self.assertEqual(400, res.status_code,
                         'Missing attributes not detected')

    def test_sequence_POST_sequence_guids_added_to_bagfile(self):
        sq = SequenceHandler()
        data1 = raw_data_sequence.copy()
        data2 = raw_data_sequence.copy()
        data1["index"] = 0
        data2["index"] = 1

        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])

        seq_guid1 = sq.add_sequence_to_db(data1)
        seq_guid2 = sq.add_sequence_to_db(data2)

        with connector as con:
            bag = list(con.get_bagfiles([raw_data_bagfile["guid"]]))
        self.assertCountEqual([seq_guid1, seq_guid2], bag[0]["sequences"],
                         'Not all sequences added to bagfile!')
        self.assertEqual(
            seq_guid1, bag[0]["sequences"][0],
            'Sequence 1 guid not equal in bagfile!')
        self.assertEqual(
            seq_guid2, bag[0]["sequences"][1],
            'Sequence 2 guid not equal in bagfile!')


class TestSequenceGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_sequence_GET(self):
        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences([raw_data_sequence])
        res = requests.get(url=sequence_url + raw_data_sequence['guid'])
        self.assertDictEqual(raw_data_sequence,
                             res.json()['results'][0], 'Sequence post failed')

    def test_sequence_GET_invalid(self):
        res = requests.get(url=sequence_url +
                           '12341234-1234-1234-1234-123412341235')
        self.assertEqual(404, res.status_code, 'Invalid GUID not detected')


class TestSequencePATCH(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_sequence_PATCH(self):
        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences([raw_data_sequence])
        data = raw_data_sequence.copy()
        del data['guid']
        del data['bagfile_guid']
        del data['index']
        data['start'] = 1475272800000
        data['end'] = 1475272800001
        res = requests.patch(url=sequence_url + raw_data_sequence['guid'],
                             json=data)
        self.assertEqual(200, res.status_code, 'Sequence patch failed')

    def test_sequence_PATCH_invalid(self):
        data = raw_data_sequence.copy()
        del data['guid']
        del data['bagfile_guid']
        del data['index']
        res = requests.patch(url=sequence_url +
                             '12341234-1234-1234-1234-123412341235',
                             json=data)
        self.assertEqual(500, res.status_code, 'Invalid GUID not detected')


class TestSequenceDELETE(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_sequence_DELETE(self):
        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences([raw_data_sequence])
        res = requests.delete(url=sequence_url + raw_data_sequence['guid'])
        self.assertEqual(200, res.status_code, 'Sequence delete failed')

    def test_sequence_GET_invalid(self):
        res = requests.delete(url=sequence_url +
                              '12341234-1234-1234-1234-123412341235')
        self.assertEqual(500, res.status_code, 'Invalid GUID not detected')


class TestSequenceOPTIONS(unittest.TestCase):
    def test_sequence_OPTIONS(self):
        res = requests.options(url=sequence_url)
        self.assertEqual(200, res.status_code,
                         'Fetching Sequence route options failed')
        expected_result = 'OPTIONS, GET, POST, PATCH, DELETE'
        self.assertEqual(expected_result,
                         res.headers['Access-Control-Allow-Methods'],
                         'Fetching Sequence route options failed')


if __name__ == '__main__':
    unittest.main()
