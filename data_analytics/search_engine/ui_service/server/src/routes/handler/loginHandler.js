import passport from 'passport';
import url from 'url';
import util from 'util';
import querystring from 'querystring';

import {DAIMLER_STRATEGY_ID, getOAuthStrategyId} from '../../loaders/oAuthLoader';
import storeProvider from '../../models/storeProvider';
import configHandlerSingleton from '../../configHandler';

class LoginHandler {

  callLogin() {
    return passport.authenticate(getOAuthStrategyId(),
      getOAuthStrategyId() === DAIMLER_STRATEGY_ID
        ? {}
        : {audience: 'https://danf-metadata-api'}
    )
  }

  callAuthentication(req, res, next) {
    passport.authenticate(getOAuthStrategyId(), function (err, user, info) {
      if (err) return next(err);
      if (!user) return res.redirect('/login');
      req.logIn(user, function (err) {
        if (err) return next(err);
        const returnTo = req.session.returnTo;
        delete req.session.returnTo;
        res.redirect(returnTo || '/');
      });
    })(req, res, next);
  }

  callLogout(req, res) {
    deleteLoginData(req);
    // Log out with Daimler or Auth0
    getOAuthStrategyId() === DAIMLER_STRATEGY_ID
      ? daimlerLogout(res)
      : auth0Logout(req, res);

    /* Logout helper functions */
    function deleteLoginData(req) {
      try { storeProvider.deleteUserTokenData(req.session.passport.user) }
      catch (e) { console.log('Can not delete user from session: %s', e) }
      try { storeProvider.deleteUserData(req.session.passport.user) }
      catch (e) { console.log('Can not delete user data from session: %s', e) }
      try { req.logout() }
      catch (e) { console.log('Can not log out user: %s', e) }
      try { req.session.destroy() }
      catch (e) { console.log('Can not destroy user session: %s', e) }
    }

    function daimlerLogout(res) {
      res.redirect(configHandlerSingleton.getAuthLogoutURL());
    }

    function auth0Logout(req, res) {
      let returnTo = req.protocol + '://' + req.hostname;
      if (!configHandlerSingleton.getDanfEnv()) {
        const port = req.connection.localPort;
        if (port !== undefined && port !== 80 && port !== 443) {
          returnTo += ':' + port;
        }
      }
      const logoutURL = new url.URL(
        util.format('https://%s/v2/logout', configHandlerSingleton.getAuthDomain())
      );
      logoutURL.search = querystring.stringify({
        client_id: configHandlerSingleton.getAuthClientID(),
        returnTo: returnTo
      });
      res.redirect(logoutURL);
    }
  }
}

export default new LoginHandler();
