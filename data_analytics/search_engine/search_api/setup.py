"""Setup file"""
from setuptools import setup, find_packages
from config.constants import VERSION

setup(name='search_api',
      version=VERSION,
      packages=find_packages(),
      test_suite="tests",
      install_requires=[
          'numpy>=1.16.1', 'tornado>=6.0.1', 'requests>=2.21.0',
          'Authlib>=0.11', 'pymongo==3.12.2', 'DANFUtils>=0.2.29',
          'DBConnector>=0.4.74', 'pydantic>=1.2,<2.0', 'coverage>=4.5.4',
          'packaging>=20.1'
      ])
