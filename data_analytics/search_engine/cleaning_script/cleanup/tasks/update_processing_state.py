__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import itertools
import logging
import os
from typing import Any, Iterable, Dict

from ContainerClient.container_client import ContainerClient
from DANFUtils.exceptions import RESTException

logger = logging.getLogger(__name__)


class UpdateProcessingStateTask:

    def __init__(self):
        self.client = ContainerClient()

    def execute(self):
        """Updates the processing_state attribute of the bagfile to the
        appropriate terminal state. The terminal state is determined based
        on the state array of the bagfile metadata.
        """
        logger.info("Executing update processing state")

        guid = os.getenv('BAGFILE_GUID', None)
        if not guid:
            raise ValueError("Update processing state failed. "
                             "Required env var BAGFILE_GUID is missing")

        try:
            bagfile = self.client.get_bagfile(guid)
        except (RESTException, AssertionError) as e:
            # fail task in order to fail step
            raise RuntimeError(f"Update processing state failed. "
                               f"Bagfile guid: {guid}") from e

        state = self._determine_terminal_state(bagfile)

        try:
            self.client.update_bagfile(guid, {'processing_state': state})
        except (RESTException, AssertionError) as e:
            # fail task in order to fail step
            raise RuntimeError(f"Update processing state failed. "
                               f"Bagfile guid: {guid}") from e

        logger.info(f"Update processing state successful. "
                    f"Bagfile guid: {guid}. Calculated state: {state}, "
                    f"based on state array: {bagfile.get('state')}")

    def _determine_terminal_state(self, bagfile: Dict[str, Any]) -> str:
        """Determines the appropriate terminal processing_state for the given
        bagfile based on the state array of the bagfile.

        :param bagfile: bagfile metadata. Expected structure:
            {
              "state" : [
                {
                    "name" : "splitter",
                    "status" : "SUCCESSFUL",
                },
                ...
              ],
              ...
            }
        :return: 'SUCCESSFUL' or 'FAILED'
        """
        states = bagfile['state']

        # group state events by step (each step might have multiple state
        # events due to retries)
        states_by_step = itertools.groupby(states, lambda state: state['name'])

        all_steps_successful = True
        for step, step_states in states_by_step:
            if not self._is_step_successful(list(step_states)):
                all_steps_successful = False

        return 'SUCCESSFUL' if all_steps_successful else 'FAILED'

    @staticmethod
    def _is_step_successful(states: Iterable[Dict[str, Any]]) -> bool:
        """Determines if an individual step is successful based on all state
        events related to it.

        :param states: all state events that belong to a specific step
        :return: True if successful, otherwise false
        """

        # if at least one state event for the given step is successful,
        # the step is considered successful
        successful = any([state['status'] == 'SUCCESS' for state in states])
        if successful:
            return True

        # if the step was skipped, it counts as successful in the context
        # of determining the terminal processing_state
        skipped = all([state['status'] == 'SKIPPED' for state in states])
        if skipped:
            return True

        # all other cases are considered as failed
        return False
