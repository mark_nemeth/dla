import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../Table';
import SizeEntry from './SizeEntry';

describe('Bagfile Table SizeEntry', () => {

  it('should format size correct ', () => {
    const formattedResult = '623.33 MB';
    const props = {size: 623330938};
    const wrapper = shallow(<SizeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should create a disabled button when path not exists', () => {
    const props = {size: null};
    const wrapper = shallow(<SizeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});