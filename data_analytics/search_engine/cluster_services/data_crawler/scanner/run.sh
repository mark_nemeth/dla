#!/bin/sh
umask 002
time python3 -u scanner/scanner.py "$OUTPUT_PATH" "$DIRECTORIES" "$NUM_PROCESSES" "$NUM_WORKERS" "$CHUNK_SIZE" --whitelist "$PATTERN"