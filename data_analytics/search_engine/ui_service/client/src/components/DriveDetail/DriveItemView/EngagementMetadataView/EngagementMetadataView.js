import React from 'react';
import DateTimeEntry from "../../../AttributeFormatter/DateTime/DateTimeEntry";
import LabelEntry from "../../../AttributeFormatter/Label/LabelEntry";
import LabelContainer from "../../../Utils/LabelContainer/LabelContainer";
import {Row} from "react-bootstrap";
import Card from "react-bootstrap/Card";
import './EngagementMetadataView.scss';

export function EngagementMetadataView(props) {

  return (
      <Card className="engagement-information-section">
        <span className="engagement-information-section-label">
          Engagement Information
        </span>
        <Row className='information-row'>
          <LabelContainer label="Engagement Guid" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.engagement.guid}/>
          </LabelContainer>
          <LabelContainer label="Disengagement Type" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.engagement.disengagement_type}/>
          </LabelContainer>
        </Row>
        <Row className='information-row'>
          <LabelContainer label="Engagement" variant="fifty-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.engagement.engagement}/>
          </LabelContainer>
          <LabelContainer label="Disengagement" variant="fifty-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.engagement.disengagement}/>
          </LabelContainer>
        </Row>
      </Card>
  )
}