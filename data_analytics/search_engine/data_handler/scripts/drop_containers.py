__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse
import os
import sys
from datetime import datetime

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from azure.storage.blob import BlockBlobService
from config.config_handler import config_dict
from handlers.utils import utils


class ContainerDropper:

    def __init__(self, storage_account, from_date, to_date):
        self.from_date = from_date
        self.to_date = to_date
        self.storage_account = storage_account
        self.storage_service = BlockBlobService(
            account_name=self.storage_account,
            account_key=utils.get_storage_account_access_key(storage_account),
            protocol='https')

    def delete_all_containers(self):
        for container in self.storage_service.list_containers():
            if self.from_date < container.properties.last_modified < self.to_date:
                self.storage_service.delete_container(container.name)
        print(f"All containers deleted in storage_account: {self.storage_account}")


def main(storage_account, allow_input_storage_accounts, from_date, to_date):
    input_storage_account_list = list(config_dict.get_value("storage_account_input_output_mapping").keys())

    if storage_account is None:
        print("Storage account is missing. Aborting...")
        return

    if not allow_input_storage_accounts and any(storage_account == s for s in input_storage_account_list):
        print(
            "You inserted an input storage account. If you really want to drop all containers from the input storage"
            " account please add \"--allow_input_storage_accounts\". Aborting...")
        return

    cd = ContainerDropper(storage_account, from_date, to_date)
    cd.delete_all_containers()


def _get_commandline_args():
    parser = argparse.ArgumentParser(description='Delete all containers in storage account')
    parser.add_argument('--storage_account',
                        action='store',
                        required=True,
                        help='storage_account name is required')
    parser.add_argument('--allow_input_storage_accounts',
                        action='store_true',
                        required=False,
                        help='This flag allows you to delete containers in input storage accounts')
    parser.add_argument('--to_date',
                        action='store',
                        required=False,
                        type=lambda d: datetime.strptime(f"{d}-+0000", '%Y-%m-%d-%z'),
                        default="3000-01-01",
                        help='This flag allows you to delete containers until the given date [exclusive]. Format: YYYY-MM-DD')
    parser.add_argument('--from_date',
                        action='store',
                        required=False,
                        default="1000-01-01",
                        type=lambda d: datetime.strptime(f"{d}-+0000", '%Y-%m-%d-%z'),
                        help='This flag allows you to delete containers from the given date [inclusive]. Format: YYYY-MM-DD')

    return parser.parse_args()


if __name__ == '__main__':
    args = _get_commandline_args()
    main(args.storage_account, args.allow_input_storage_accounts, args.from_date, args.to_date)
