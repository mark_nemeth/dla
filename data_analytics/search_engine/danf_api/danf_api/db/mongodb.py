"""Db access."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorDatabase, AsyncIOMotorCollection

from ..core.config import DB_NAME, BAGFILE_COLLECTION_NAME, CHILDBAGFILE_COLLECTION_NAME, FILECATALOG_COLLECTION_NAME


class _DataBase:
    client: AsyncIOMotorClient = None


db = _DataBase()


def get_db_client() -> AsyncIOMotorClient:
    return db.client


def get_db() -> AsyncIOMotorDatabase:
    return get_db_client()[DB_NAME]


def get_filecatalog_collection() -> AsyncIOMotorCollection:
    return get_db()[FILECATALOG_COLLECTION_NAME]


def get_bagfile_collection() -> AsyncIOMotorCollection:
    return get_db()[BAGFILE_COLLECTION_NAME]


def get_childbagfile_collection() -> AsyncIOMotorCollection:
    return get_db()[CHILDBAGFILE_COLLECTION_NAME]
