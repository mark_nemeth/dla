# MongoDB sync

The MongoDB sync service syncs the generated metadata from the on prem cluster to the Azure CosmosDB (Mongo API).

It utilises the endpoints from metadata-API and search-API located on prem and in Azure to create, update and delete metadata 
in the Azure CosmosDB according to the changes in the on prem mongoDB. 

> Current version: 0.0.1, please replace the ${version} below

## How to deploy the MongoDB sync in Kubernetes

## First steps:
1. Press 'Connect to feed' button on this page: https://daimler.visualstudio.com/Athena/_packaging?_a=feed&feed=ATTDATA
2. Select 'Python' from the left column in newly opened window.
3. In section 'Get packages with pip' press 'Generate Python credentials' and copy the value of index-url (it looks like: https://ATTDATA:******@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/ )
4. Define environment variable and set a value taken from the previous step.

```console
export index_url_with_access_from_azure_devops=https://ATTDATA:PASSWORD-TAKEN-FROM-AZURE-DEVOPS-ARTIFACTS-CONNECT-TO-FEED-BUTTON@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```


### Manually
 
 1. build docker image:
 
     ```console
     docker build -t athena.daimler.com/dla_docker/mongodb_sync:{version} . --network=host --build-arg INDEX_URL_WITH_ACCESS=$index_url_with_access_from_azure_devops
     ```
 
 2. push to the Athena Registry
 
     ```console
     docker push athena.daimler.com/dla_docker/mongodb_sync:{version}
     ```
 
 3. connecting to on prem cluster and applying to Kubernetes
 
     ```console
     k apply -f /kubernetes/cronjob_sv_prod.yaml
     ```
