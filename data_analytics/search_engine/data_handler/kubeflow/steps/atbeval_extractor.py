__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


from kfp import dsl
from kubernetes.client import V1VolumeMount, V1SecurityContext
from kubernetes.client.models import V1EnvVar
from config.config_handler import config_dict

# constants and common functions
from DANFUtils.tracing import tracing
import DANFUtils.tracing.tracing as dutt
import kubeflow.steps.common as ksc


class AtbevalExtractorOp(dsl.ContainerOp):

    INPUT_MOUNT_BASE_PATH = "/input"
    NAME = "atbeval-extractor"
    DOCKER_IMG = config_dict.get_value(NAME)
    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "1Gi"
    MEMORY_LIMIT = "3Gi"
    VOLUME_NAME = "mapr-mount"

    @classmethod
    def get_pod_label_settings(cls, bagfile_guid, vin, input_file_path,
                               test_mode):
        pod_labels = {
            ksc.PIPELINE_POD_LABEL: cls.NAME,
            ksc.BAGFILE_POD_LABEL: bagfile_guid,
            ksc.VIN_POD_LABEL: vin,
            ksc.FILE_NAME_POD_LABEL:
                ksc.extract_file_name_from_path(input_file_path)
        }
        if test_mode:
            pod_labels["TESTING_POD_LABEL"] = "true"
        return pod_labels

    @classmethod
    def get_v1_env_var_settings(cls, input_file_path, bagfile_guid, user_id):
        run_replay = str(config_dict.get_value(
            "atbeval_extractor_run_replay", False))
        adstats_url = config_dict.get_value(
            "atbeval_extractor_adstats_url", "")
        if config_dict.get_value("atbeval_extractor_use_sonder_proxy"):
            http_proxy = ksc.SONDER_PROXY
            https_proxy = ksc.SONDER_PROXY
        else:
            http_proxy = ksc.HTTP_PROXY
            https_proxy = ksc.HTTPS_PROXY
        # hack: the only Daimler IP we are using is ADstats which DOES need to
        # be routed through the proxy -> make sure that no_proxy rule is not
        # overinclusive
        no_proxy = ",".join(entry for entry in ksc.NO_PROXY.split(",")
                            if "53.0.0.0/8" not in entry)
        return {
            "BAGFILE_PATH": input_file_path,
            "BAGFILE_GUID": bagfile_guid,
            "USER": user_id,
            "METADATA_API_BASE_URL": ksc.METADATA_API_BASE_URL,
            "SEARCH_API_BASE_URL": ksc.SEARCH_API_BASE_URL,
            "APPINSIGHTS_INSTRUMENTATIONKEY": ksc.APPINSIGHTS_KEY,
            "DANF_ENV": ksc.DANF_ENV,
            "RUN_REPLAY": run_replay,
            "ADSTATS_URL": adstats_url,
            "HTTP_PROXY": http_proxy,
            "HTTPS_PROXY": https_proxy,
            "http_proxy": http_proxy,
            "https_proxy": https_proxy,
            "no_proxy": no_proxy,
            "AUTH": ksc.AUTH,
            "AUTH_TOKEN_URL": ksc.AUTH_TOKEN_URL,
            "AUTH_DATAHANDLER_AUDIENCE": ksc.AUTH_DATAHANDLER_AUDIENCE,
            "AUTH_DATAHANDLER_CLIENT_CRED_ID":
                ksc.AUTH_DATAHANDLER_CLIENT_CRED_ID,
            "AUTH_DATAHANDLER_CLIENT_CRED_SECRET":
                ksc.AUTH_DATAHANDLER_CLIENT_CRED_SECRET,
            "AUTH_METADATA_AUDIENCE":
                ksc.AUTH_METADATA_AUDIENCE,
            "AUTH_METADATA_CLIENT_CRED_ID":
                ksc.AUTH_METADATA_CLIENT_CRED_ID,
            "AUTH_METADATA_CLIENT_CRED_SECRET":
                ksc.AUTH_METADATA_CLIENT_CRED_SECRET,
            "AUTH_SEARCH_AUDIENCE": ksc.AUTH_SEARCH_AUDIENCE,
            "AUTH_SEARCH_CLIENT_CRED_ID": ksc.AUTH_SEARCH_CLIENT_CRED_ID,
            "AUTH_SEARCH_CLIENT_CRED_SECRET":
                ksc.AUTH_SEARCH_CLIENT_CRED_SECRET,
            ksc.ENV_TRACE_ID:
                tracing.get_propagation_vars().get(ksc.ENV_TRACE_ID),
            ksc.ENV_SPAN_ID:
                tracing.get_propagation_vars().get(ksc.ENV_SPAN_ID),
            dutt.ENV_TRACE_OPTIONS: tracing.get_propagation_vars().get(
                dutt.ENV_TRACE_OPTIONS)
        }

    @classmethod
    def get_resource_request_settings(cls):
        return {
            ksc.CPU_RESOURCE_TITLE: cls.CPU_REQUEST,
            ksc.MEMORY_RESOURCE_TITLE: cls.MEMORY_REQUEST
        }

    @classmethod
    def get_resource_limit_settings(cls):
        return {
            ksc.CPU_RESOURCE_TITLE: cls.CPU_LIMIT,
            ksc.MEMORY_RESOURCE_TITLE: cls.MEMORY_LIMIT
        }

    @classmethod
    def get_volume_settings(cls, pvc_name):
        return [
            ksc.get_pvc_volume(cls.VOLUME_NAME, pvc_name, read_only=True)
        ]

    @classmethod
    def get_volume_mount_settings(cls):
        return [
            V1VolumeMount(
                mount_path=cls.INPUT_MOUNT_BASE_PATH,
                name=cls.VOLUME_NAME)
        ]

    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               input_file_path,
               pvc_name,
               user_id,
               vin,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_file_path: path to the bagfile in the pod
        :param pvc_name: name of the pvc to mount
        :param user_id: step will be executed with this user id
        :param vin: of the vehicle from which the processed bagfile originates
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the AtpftstExtractor operation
        """

        pod_labels = cls.get_pod_label_settings(
            bagfile_guid, vin, input_file_path, test_mode)
        v1_env_vars = cls.get_v1_env_var_settings(
            input_file_path, bagfile_guid, user_id)
        resource_requests = cls.get_resource_request_settings()
        resource_limits = cls.get_resource_limit_settings()
        volumes = cls.get_volume_settings(pvc_name)
        volume_mounts = cls.get_volume_mount_settings()

        step = AtbevalExtractorOp()
        step.set_security_context(V1SecurityContext(run_as_user=user_id))
        for name, value in pod_labels.items():
            step.add_pod_label(
                name=str(name), value=ksc.sanitize_label(str(value)))
        for name, value in v1_env_vars.items():
            step.add_env_variable(V1EnvVar(name=str(name), value=str(value)))
        for name, value in resource_requests.items():
            step.add_resource_request(name, value)
        for name, value in resource_limits.items():
            step.add_resource_limit(name, value)
        for volume in volumes:
            step.add_volume(volume)
        for volume_mount in volume_mounts:
            step.add_volume_mount(volume_mount)

        return step

    def __init__(self):
        super().__init__(
            name=self.NAME,
            image=self.DOCKER_IMG
        )
