__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
import os
import argparse
import coverage
import sys
from io import StringIO
import contextlib
#TODO DIRTY CODE, maybe find better possibility to make PYTHONPATH
tests_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
for i in range(2):
    tests_path = os.path.abspath(os.path.join(tests_path, '..'))
sys.path.append(tests_path)  # add .../search_api Path
print(tests_path)
from data_handler.config.config_handler import config_dict
from DANFUtils.logging import logger

# Globals
LOGGING_FOLDER_KEY = "logging_folder"
LOG_LEVEL_KEY = 'log_level'
NAME_OF_TEST = "splitter"
# set logging folder
logging_folder = config_dict.get_value(LOGGING_FOLDER_KEY)
if logging_folder:
    logger.set_logging_folder(logging_folder)
else:
    logger.set_logging_folder(
        os.path.abspath(os.path.dirname(__file__)) + '/logs')

# set log level
log_level = config_dict.get_value(LOG_LEVEL_KEY)
if log_level:
    logger.set_log_level(log_level)
    logger.debug("Setting log level to " + log_level)


@contextlib.contextmanager
def stdout_redirect(where):
    sys.stdout = where
    try:
        yield where
    finally:
        sys.stdout = sys.__stdout__


parser = argparse.ArgumentParser()
parser.add_argument('--cov',
                    '-cov',
                    type=str,
                    help='Calculate coverage of executed tests.')
parser.add_argument(
    '--html',
    '-html',
    type=str,
    help=
    'Generate a html report for calculated coverage. (must be used in combination with -cov="True")'
)
parser.add_argument(
    '--txt',
    '-txt',
    type=str,
    help=
    'Generate a txt report in folder unittests for calcualted covergae. (must be used in combination with -cov="True")'
)

args = parser.parse_args()
cov = coverage.Coverage()
include = list(['*/search_engine/' + NAME_OF_TEST + '/*'
                ])  # count only files from search_api
exclude = list(['*/*_unittest.py',
                '*/config/*'])  # dont count *_unittest.py files

dir_path = os.path.dirname(os.path.realpath(__file__))

if __name__ == '__main__':

    suite = unittest.TestLoader().discover(str(dir_path),
                                           pattern="*_unittest.py")

    with stdout_redirect(StringIO()) as new_stdout:
        if args.cov == "True": cov.start()
        print("RUNNING UNITTESTS FOR " + NAME_OF_TEST)
        print(
            "--------------------------------------------------------------------------"
        )
        result = unittest.TextTestRunner(sys.stdout, verbosity=2).run(suite)

        if args.cov == "True":
            cov.stop()
            cov.save()
            print("\nTEST COVERAGE FOR " + NAME_OF_TEST)
            print("----------------------------")
            cov.report(include=include, omit=exclude)

        if (args.cov == "True") and (args.html == "True"):
            cov.html_report(directory=os.path.dirname(__file__) +
                            "/COVERAGE_HTML_REPORT",
                            include=include,
                            omit=exclude)

    new_stdout.seek(0)
    res = new_stdout.read()

    logger.info(res)

    if (args.cov == "True") and (args.txt == "True"):
        coverage_report = open(dir_path + '/COVERAGE_REPORT.txt', 'w')
        coverage_report.write(res)
        coverage_report.close()

    if not (result.wasSuccessful()):
        exit(1)
