# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/app/altros/devel/.private/rosbag/include;/app/altros/src/rosbag/include".split(';') if "/app/altros/devel/.private/rosbag/include;/app/altros/src/rosbag/include" != "" else []
PROJECT_CATKIN_DEPENDS = "rosbag_storage;rosconsole;roscpp;std_srvs;topic_tools;xmlrpcpp;message_runtime;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lrosbag".split(';') if "-lrosbag" != "" else []
PROJECT_NAME = "rosbag"
PROJECT_SPACE_DIR = "/app/altros/devel/.private/rosbag"
PROJECT_VERSION = "1.14.3"
