import React from 'react';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import './TablePagination.scss'

export function TablePagination(props) {

  const totalNumberOfPages = Math.ceil(props.total / props.limit);
  const currentPageIndex = Math.floor(props.offset / props.limit);
  let firstShownPageIndex = Math.max(currentPageIndex - (props.maxVisiblePages - 1) / 2, 0);
  const lastShownPageIndex = Math.min(firstShownPageIndex + props.maxVisiblePages - 1, totalNumberOfPages - 1);
  const pagesLeftToShow = props.maxVisiblePages - (lastShownPageIndex - firstShownPageIndex + 1);
  firstShownPageIndex = Math.max(firstShownPageIndex - pagesLeftToShow, 0);

  function createPaginationButtonList() {
    let paginationButtons = [];
    if (currentPageIndex > 0) paginationButtons.push(
      <Button key='&lsaquo;' className="pagination-button-unselected"
              onClick={() => {
                props.setPaginationOffset((currentPageIndex - 1) * props.limit);
                props.filterWithPagination();
              }}>
        &lsaquo;
      </Button>
    );
    for (let i = firstShownPageIndex; i <= lastShownPageIndex; i++) {
      paginationButtons.push(
        <Button key={i}
                className={currentPageIndex === i ? "pagination-button-selected" : "pagination-button-unselected"}
                onClick={() => {
                  props.setPaginationOffset(i * props.limit);
                  props.filterWithPagination();
                }}>
          {i + 1}
        </Button>
      )
    }
    if (totalNumberOfPages > 1 && currentPageIndex < lastShownPageIndex) paginationButtons.push(
      <Button key='&rsaquo;' className="pagination-button-unselected"
              onClick={() => {
                props.setPaginationOffset((currentPageIndex + 1) * props.limit);
                props.filterWithPagination();
              }}>
        &rsaquo;
      </Button>
    );
    return paginationButtons;
  }

  return (
    <ButtonToolbar className="pagination-bar">
      <ButtonGroup>
        {createPaginationButtonList()}
      </ButtonGroup>
    </ButtonToolbar>
  );
}