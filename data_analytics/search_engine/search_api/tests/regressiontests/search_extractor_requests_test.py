__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from .testutils import get_testdb_connector

base_url = 'http://localhost:8889'
search_seq_url = base_url + '/search/extractorrequest'

ext_1 = {
    "guid": generate_deterministic_guid(),
    "version": "0.0.1",
    "bagfile_guid": generate_deterministic_guid(),
    "extractor_id": "ext_test",
    "start": 1545293763543,
    "end": 1545293783234,
    "bva": 0,
    "link": "testdata",
    "metadata": {
        "disengagement": {
            "ts": 1545293768234,
            "type": "Error",
            "error_msg": "This is a test error"
        }
    }
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['extractor_requests'].delete_many({'link': 'testdata'})
        con.add_extractor_requests([ext_1])


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['extractor_requests'].delete_many({'link': 'testdata'})


class TestSearchExtractorRequestsGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_bagfile_guid(self):
        query_data = {
            'bagfile_guid': ext_1['bagfile_guid']
        }
        res = requests.get(url=search_seq_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'Bagfile search by guid failed')
        self.assertCountEqual([ext_1], res_json,
                              'Bagfile search by guid failed')
