// Auto-generated. Do not edit!

// (in-package rosbag.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class RosbagRecorderState {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.wall_time = null;
      this.recording_start_time = null;
      this.recording_duration = null;
      this.recording = null;
      this.timeshift_enabled = null;
      this.disk_capacity = null;
      this.disk_available = null;
      this.recorder_buffer_used = null;
      this.recorder_buffer_capacity = null;
      this.droppedMessagesSinceStart = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('wall_time')) {
        this.wall_time = initObj.wall_time
      }
      else {
        this.wall_time = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('recording_start_time')) {
        this.recording_start_time = initObj.recording_start_time
      }
      else {
        this.recording_start_time = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('recording_duration')) {
        this.recording_duration = initObj.recording_duration
      }
      else {
        this.recording_duration = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('recording')) {
        this.recording = initObj.recording
      }
      else {
        this.recording = false;
      }
      if (initObj.hasOwnProperty('timeshift_enabled')) {
        this.timeshift_enabled = initObj.timeshift_enabled
      }
      else {
        this.timeshift_enabled = false;
      }
      if (initObj.hasOwnProperty('disk_capacity')) {
        this.disk_capacity = initObj.disk_capacity
      }
      else {
        this.disk_capacity = 0;
      }
      if (initObj.hasOwnProperty('disk_available')) {
        this.disk_available = initObj.disk_available
      }
      else {
        this.disk_available = 0;
      }
      if (initObj.hasOwnProperty('recorder_buffer_used')) {
        this.recorder_buffer_used = initObj.recorder_buffer_used
      }
      else {
        this.recorder_buffer_used = 0;
      }
      if (initObj.hasOwnProperty('recorder_buffer_capacity')) {
        this.recorder_buffer_capacity = initObj.recorder_buffer_capacity
      }
      else {
        this.recorder_buffer_capacity = 0;
      }
      if (initObj.hasOwnProperty('droppedMessagesSinceStart')) {
        this.droppedMessagesSinceStart = initObj.droppedMessagesSinceStart
      }
      else {
        this.droppedMessagesSinceStart = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RosbagRecorderState
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [wall_time]
    bufferOffset = _serializer.time(obj.wall_time, buffer, bufferOffset);
    // Serialize message field [recording_start_time]
    bufferOffset = _serializer.time(obj.recording_start_time, buffer, bufferOffset);
    // Serialize message field [recording_duration]
    bufferOffset = _serializer.duration(obj.recording_duration, buffer, bufferOffset);
    // Serialize message field [recording]
    bufferOffset = _serializer.bool(obj.recording, buffer, bufferOffset);
    // Serialize message field [timeshift_enabled]
    bufferOffset = _serializer.bool(obj.timeshift_enabled, buffer, bufferOffset);
    // Serialize message field [disk_capacity]
    bufferOffset = _serializer.uint64(obj.disk_capacity, buffer, bufferOffset);
    // Serialize message field [disk_available]
    bufferOffset = _serializer.uint64(obj.disk_available, buffer, bufferOffset);
    // Serialize message field [recorder_buffer_used]
    bufferOffset = _serializer.uint64(obj.recorder_buffer_used, buffer, bufferOffset);
    // Serialize message field [recorder_buffer_capacity]
    bufferOffset = _serializer.uint64(obj.recorder_buffer_capacity, buffer, bufferOffset);
    // Serialize message field [droppedMessagesSinceStart]
    bufferOffset = _serializer.uint64(obj.droppedMessagesSinceStart, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RosbagRecorderState
    let len;
    let data = new RosbagRecorderState(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [wall_time]
    data.wall_time = _deserializer.time(buffer, bufferOffset);
    // Deserialize message field [recording_start_time]
    data.recording_start_time = _deserializer.time(buffer, bufferOffset);
    // Deserialize message field [recording_duration]
    data.recording_duration = _deserializer.duration(buffer, bufferOffset);
    // Deserialize message field [recording]
    data.recording = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [timeshift_enabled]
    data.timeshift_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [disk_capacity]
    data.disk_capacity = _deserializer.uint64(buffer, bufferOffset);
    // Deserialize message field [disk_available]
    data.disk_available = _deserializer.uint64(buffer, bufferOffset);
    // Deserialize message field [recorder_buffer_used]
    data.recorder_buffer_used = _deserializer.uint64(buffer, bufferOffset);
    // Deserialize message field [recorder_buffer_capacity]
    data.recorder_buffer_capacity = _deserializer.uint64(buffer, bufferOffset);
    // Deserialize message field [droppedMessagesSinceStart]
    data.droppedMessagesSinceStart = _deserializer.uint64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 66;
  }

  static datatype() {
    // Returns string type for a message object
    return 'rosbag/RosbagRecorderState';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'd5e30b7ec94cbb4dcb4898b09b7dc723';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    time wall_time
    
    time recording_start_time
    duration recording_duration
    
    bool recording
    bool timeshift_enabled
    
    uint64 disk_capacity
    uint64 disk_available
    
    # the same buffer is used in timeshift mode as in normal recording mode
    uint64 recorder_buffer_used
    uint64 recorder_buffer_capacity 
    
    uint64 droppedMessagesSinceStart
    
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RosbagRecorderState(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.wall_time !== undefined) {
      resolved.wall_time = msg.wall_time;
    }
    else {
      resolved.wall_time = {secs: 0, nsecs: 0}
    }

    if (msg.recording_start_time !== undefined) {
      resolved.recording_start_time = msg.recording_start_time;
    }
    else {
      resolved.recording_start_time = {secs: 0, nsecs: 0}
    }

    if (msg.recording_duration !== undefined) {
      resolved.recording_duration = msg.recording_duration;
    }
    else {
      resolved.recording_duration = {secs: 0, nsecs: 0}
    }

    if (msg.recording !== undefined) {
      resolved.recording = msg.recording;
    }
    else {
      resolved.recording = false
    }

    if (msg.timeshift_enabled !== undefined) {
      resolved.timeshift_enabled = msg.timeshift_enabled;
    }
    else {
      resolved.timeshift_enabled = false
    }

    if (msg.disk_capacity !== undefined) {
      resolved.disk_capacity = msg.disk_capacity;
    }
    else {
      resolved.disk_capacity = 0
    }

    if (msg.disk_available !== undefined) {
      resolved.disk_available = msg.disk_available;
    }
    else {
      resolved.disk_available = 0
    }

    if (msg.recorder_buffer_used !== undefined) {
      resolved.recorder_buffer_used = msg.recorder_buffer_used;
    }
    else {
      resolved.recorder_buffer_used = 0
    }

    if (msg.recorder_buffer_capacity !== undefined) {
      resolved.recorder_buffer_capacity = msg.recorder_buffer_capacity;
    }
    else {
      resolved.recorder_buffer_capacity = 0
    }

    if (msg.droppedMessagesSinceStart !== undefined) {
      resolved.droppedMessagesSinceStart = msg.droppedMessagesSinceStart;
    }
    else {
      resolved.droppedMessagesSinceStart = 0
    }

    return resolved;
    }
};

module.exports = RosbagRecorderState;
