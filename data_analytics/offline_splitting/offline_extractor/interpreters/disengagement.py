"""Disengagement Interepeter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import rospy

from DANFUtils.logging import logger
from config.config_handler import ConfigHandler
from config.constants import *
from utils import charlist2str, rospytime2int, timestamp2ms


class DisengagementInterpreter:
    def __init__(self):
        self.engagement_handler = None
        self.last_state = None
        self.current_state = None
        self.disengagements = []
        self.timestamp = None
        self.logrequests = []
        self.disengagements_details = []
        self.results = {}

    def set_config(self, bagfile_start_ts, topics_list):
        config = ConfigHandler()
        evs_topic, log_topic, eng_handler = config.get_config_for_disengagement(
            bagfile_start_ts, topics_list)
        self.engagement_handler = eng_handler
        return evs_topic, log_topic

    def feed_new_msg(self, message, t):
        try:
            self.timestamp = t
            self.last_state = self.current_state

            # Set engagement state depending on engagement handler! (different logic for CAL and SEM)
            # 0: AD System is initializing
            # 1: AD System is ready to engage
            # 2: AD System is in the autonomous driving mode => Engaged
            # 3: AD System has error
            # disenagement: transition from 2 -> 1 (manual disengagement) or 2 -> 3 (error disengagement)
            if (self.engagement_handler == "CAL"):
                self.current_state = message.ad_engagement_state
            elif (self.engagement_handler == "SEM"):
                if (message.eSystemStatus.eSystemState != 2):  # Initializing
                    self.current_state = 0
                elif (message.eSystemStatus.eSystemState == 2
                      and message.eSystemStatus.eDegradationLevel == 2 and
                      message.eSystemStatus.eOperationMode == 4):  # Engaged
                    self.current_state = 2
                elif (message.eSystemStatus.eSystemState == 2
                      and message.eSystemStatus.eDegradationLevel == 2
                      and message.eSystemStatus.eOperationMode != 4):  # Ready
                    self.current_state = 1
                elif (message.eSystemStatus.eSystemState == 2
                      and message.eSystemStatus.eDegradationLevel !=
                      2):  # System is degraded -> Error!
                    self.current_state = 3
            else:
                msg = "Engagement handler not set properly, must be CAL or SEM!"
                logger.error(msg)
                raise ValueError(msg)

        except BaseException:
            msg = "Message doesn't have ad_engagement_state value!"
            logger.error(msg)
            raise ValueError(msg)

        self.find_disengagement(self.timestamp, self.last_state,
                                self.current_state)

        return self.disengagements

    def collect_log_msg(self, message, t):
        try:
            # Filter only messages from CAL (with source == 1).
            if (message.cause == 0
                    or message.cause == 1) and message.source == 1:
                attributes = {}
                attributes["ts"] = t
                attributes["cause"] = message.cause
                attributes["details"] = charlist2str(message.details)

                self.logrequests.append(attributes)

        except Exception:
            logger.error("LogRequest Message doesn't have required values")
            return None

    def find_disengagement(self, ts, last_state, current_state):
        attributes = {}

        if (last_state == 2 and current_state != last_state):
            attributes["ts"] = ts

            if (current_state == 1):
                logger.info("manual disengagement found, ts:" + str(ts))
                attributes["type"] = "Manual"

            elif (current_state == 3):
                logger.info("error disengagement found, ts:" + str(ts))
                attributes["type"] = "Error"

            # Engaged -> Initializing should actually never happen
            elif (current_state == 0):
                logger.info("unknown disengagement found, ts:" + str(ts))
                attributes["type"] = "Unknown"

            self.disengagements.append(attributes)

    def join_disengagement_with_logrequest(self):

        for disengagement in self.disengagements:
            result = disengagement

            ts_diff_min = None
            closest_logrequest = None
            for logrequest in self.logrequests:
                if ((ts_diff_min is None)
                        or (abs(disengagement["ts"] - logrequest["ts"]) <
                            ts_diff_min)):
                    ts_diff_min = abs(disengagement["ts"] - logrequest["ts"])
                    closest_logrequest = logrequest

            # ts difference between disengagement and corresponding logrequest message should not be greater than 0.5s
            try:
                if (ts_diff_min < rospy.Duration.from_sec(0.5)):
                    result["error_msg"] = closest_logrequest["details"]
                else:
                    logger.info(
                        "No logrequest message found for the disengagement at {}, closest logrequest message has a time diff of {} ns"
                        .format(disengagement["ts"], ts_diff_min))
                    result["error_msg"] = "No associated LogRequest msg"
            except Exception as e:
                result["error msg"] = "LogRequest topic unavailable"
                logger.warning(
                    "LogRequest topic unavailable. (ignored for processing)")

            self.disengagements_details.append(result)

    def generate_report(self, bagfile_start_ts, bagfile_end_ts):
        metadata_list = []

        for disengagement in self.disengagements_details:
            dict = {}
            metadata = {}

            # Make sure that the start/end time of the sequence is not over the boundary of the bagfile start/end time
            dict["start"] = max(
                rospytime2int(
                    disengagement["ts"] -
                    rospy.Duration.from_sec(SECONDS_BEFORE_DISENGAGEMENT)),
                timestamp2ms(bagfile_start_ts))
            dict["end"] = min(
                rospytime2int(
                    disengagement["ts"] +
                    rospy.Duration.from_sec(SECONDS_AFTER_DISENGAGEMENT)),
                timestamp2ms(bagfile_end_ts))

            disengagement["ts"] = rospytime2int(disengagement["ts"])
            dict["type"] = EXTRACTOR_TYPE

            metadata["disengagement"] = disengagement
            dict["metadata"] = metadata
            dict["bva"] = BVA_DISENGAGEMENT

            metadata_list.append(dict)

        self.results["extractor_id"] = EXTRACTOR_ID_DISENGAGEMENT
        self.results["version"] = EXTRACTOR_VERSION
        self.results["metadata_list"] = metadata_list

    def get_num_of_fed_messages(self):
        return self.num_of_fed_msgs
