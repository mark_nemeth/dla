import React from 'react';
import Form from 'react-bootstrap/Form';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import {isObjectValid} from '../../../../utils/helperFunctions';
import InputRange from 'react-input-range';
import './ByVelocityFilter.scss';

export const STATIC_LIMITS = {
  min: -2,
  max: 20
};

export function ByVelocityFilter(props) {

  function onRangeSelection(value) {
    if (isObjectValid(value)) props.filterByVelocity(value);
    else props.filterByVelocity({min: null, max: null})
  }

  function resetRangeSelection() {
    props.setCurrentVelocityRange(props.limits);
    props.filterByVelocity({min: null, max: null})
  }

  const subLabel = props.velocity.min.toFixed(1) + ' - ' + props.velocity.max.toFixed(1) + ' m/s';
  const active = props.velocity.min !== props.limits.min || props.velocity.max !== props.limits.max;

  return (
    <CollapseContainer label={active ? "Velocity: " + subLabel : "Velocity"}
                       active={active}
                       resetCallback={resetRangeSelection}
    >
      <div className="track-container">
        <Form>
          <InputRange
            className="input-range"
            draggableTrack
            step={props.steps}
            maxValue={props.limits.max}
            minValue={props.limits.min}
            onChange={value => props.setCurrentVelocityRange(value)}
            onChangeComplete={onRangeSelection}
            value={props.velocity}
          />
        </Form>
      </div>
    </CollapseContainer>
  );
}