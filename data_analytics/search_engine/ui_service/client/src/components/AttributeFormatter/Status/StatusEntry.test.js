import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../Table';
import StatusEntry from './StatusEntry';

describe('Bagfile Table StatusEntry', () => {

  it('should format status string correct', () => {
    const formattedResult = 'being processed';
    const props = {status: 'BEING_PROCESSED'};
    const wrapper = shallow(<StatusEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should create a disabled button when status not exists', () => {
    const props = {status: null};
    const wrapper = shallow(<StatusEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});
