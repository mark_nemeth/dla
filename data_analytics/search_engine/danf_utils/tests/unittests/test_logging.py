__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os

os.environ['APPINSIGHTS_INSTRUMENTATIONKEY'] = '0664adf6-0372-4683-befa-b2d0e46f0806'

import logging
import unittest

from DANFUtils.logging import configure_root_logger, LogStructure
from DANFUtils.tracing import tracing


class TestLogging(unittest.TestCase):

    def test_all_log_levels(self):
        configure_root_logger(log_level=logging.DEBUG)
        logger = logging.getLogger(__name__)
        logger.debug("debug log")
        logger.info("info log")
        logger.warning("warning log")
        logger.error("error log")
        try:
            dict()['nonexisting_key']
        except:
            logger.exception("exception log")

    def test_all_log_levels_json_logging(self):
        configure_root_logger(log_level=logging.DEBUG,
                              log_structure=LogStructure.JSON)
        logger = logging.getLogger(__name__)
        logger.debug("debug log")
        logger.info("info log")
        logger.warning("warning log")
        logger.error("error log")
        try:
            dict()['nonexisting_key']
        except:
            logger.exception("exception log")

    def test_changing_log_levels(self):
        configure_root_logger(log_level=logging.WARNING)
        logger = logging.getLogger(__name__)
        logger.debug("should not show")
        logger.info("should not show")
        logger.warning("should show")
        logger.error("should show")

    def test_logging_with_tracing(self):
        configure_root_logger()
        logger = logging.getLogger(__name__)
        tracing.start_task('danf-splitter')
        logger.info("splitter")
        tracing.start_task('split-subprocess')
        logger.info("splitter subprocess")
        tracing.end_task()
        tracing.end_task()


if __name__ == '__main__':
    unittest.main()
