import logging
import os
from dataclasses import dataclass
from hashlib import md5
from pathlib import Path
from typing import (
    Generator,
    List,
    Set,
    Union,
    Optional,
    AnyStr,
)

import requests
from DANFUtils.constants import ROOT_FILE_GUID
from dataclasses_json import dataclass_json
from requests import Response

logger = logging.getLogger(__name__)


@dataclass
class Url:
    accessor_url: str
    creation_date: int
    entry_created_by: str
    url_status: str
    last_changed: int


@dataclass_json
@dataclass
class File:
    file_guid: str
    file_size: int
    file_type: str
    parent_guid: str
    tags: List[str]
    urls: List[Url]


class DataCatalogConnector:

    def __init__(self, metadata_api_catalog_url: str, search_api_catalog_url: str, dry_run: bool = False) -> None:
        """Create data catalog instance.

        :param metadata_api_catalog_url: Url of the metadata api.
        :param search_api_catalog_url: Url of the search api.
        :param dry_run: If set, no PUT or POST will be sent.
        """
        self.metadata_api_catalog_url = self._sanitize_url(metadata_api_catalog_url)
        self.search_api_catalog_url = self._sanitize_url(search_api_catalog_url)
        self.dry_run = dry_run

    @staticmethod
    def _sanitize_url(url: str) -> str:
        return url[:-1] if url.endswith('/') else url

    def exists(self, file_guid: str) -> bool:
        """Check if an entry exists in the filecatalog with the specified file_guid or not."""
        response = requests.get(f"{self.search_api_catalog_url}/file/?file_guid={file_guid}", verify=False)
        self.check_single_response_code(response, 200)
        logger.info("Request successful.")
        data = response.json()['results']
        if not data:
            return False
        assert len(data) == 1, "Multiple resources found with the same file_guid!"
        assert data[0]["file_guid"] == file_guid, "Returned document is not matching the required file_guid"
        return True

    def add_file(self, file_guid: str, file_size: int, file_type: str) -> None:
        """ Add a new file to the file catalog. Will raise an assertion error if the response from the file catalog
        is not 201.

        :param file_guid: Unique guid of the file.
        :param file_size: File size in bytes of the file.
        :param file_type: File type of the file.
        :return: None
        """
        logger.info(f"Adding file {file_guid}, file_size {file_size}, file_type {file_type}...")
        payload = {
            "parent_guid": str(ROOT_FILE_GUID),
            "file_type": file_type,
            "file_size": file_size,
            "tags": []
        }
        if not self.dry_run:
            response = requests.put(f"{self.metadata_api_catalog_url}/file/{file_guid}",
                                    json=payload,
                                    verify=False)
            self.check_single_response_code(response, 201)
        else:
            logger.info(f"No request sent since dry run is set.")
        logger.info(f"Adding completed.")

    def add_accessor_url(self, file_guid: str, accessor_url: str, creation_date: int,
                         fail_on_http_conflict: bool = True) -> None:
        """ Adds a new accessor url to an existing file.

        :param file_guid: Unique guid of the file.
        :param accessor_url: Url in fully qualified format. I.e. starting with "hdfs://
        :param creation_date: Timestamp in ms.
        :param fail_on_http_conflict: Whether an AssertionError should be thrown if the api response code is 409.
        :return: None
        """
        logger.info(f"Adding to file {file_guid}, accessor_url {accessor_url}, creation_date {creation_date}...")
        payload = {
            "accessor_url": accessor_url,
            "creation_date": creation_date,
            "entry_created_by": "DANF",
            "url_status": "Available"
        }
        if not self.dry_run:
            response = requests.post(f"{self.metadata_api_catalog_url}/file/{file_guid}/urls",
                                     json=payload,
                                     verify=False)
            accepted_response_codes = {200} if fail_on_http_conflict else {200, 409}
            self.check_response_code(response, accepted_response_codes)
        else:
            logger.info(f"No request sent since dry run is set.")
        logger.info(f"Adding completed.")

    @staticmethod
    def check_response_code(response: Response, accepted_codes: Set[int]):
        if response.status_code not in accepted_codes:
            raise AssertionError(f"Expected any response code of {accepted_codes}, but got "
                                 f"response with code {response.status_code}: {response}")

    @staticmethod
    def check_single_response_code(response: Response, accepted_code: int):
        DataCatalogConnector.check_response_code(response, {accepted_code})


def convert_to_fully_qualified_link(accessor_url: str) -> str:
    accessor_url = accessor_url.replace('/mnt/hdfs/hdfs', 'hdfs://abt', 1)
    accessor_url = accessor_url.replace('/mapr/', 'hdfs://', 1)
    return accessor_url


def convert_timestamp_from_s_to_ms(creation_time: Union[float, str, int]) -> int:
    try:
        timestamp = int(float(creation_time) * 1000)
        # Data Catalog rejects timestamps less than 946684800001
        return max(946684800001, timestamp)
    except ValueError:
        return 946684800001


def get_last_mb(file: Path) -> Optional[AnyStr]:
    try:
        with open(file, 'rb') as fp:
            fp.seek(-1 * min(file.stat().st_size, 1024 * 1024), os.SEEK_END)
            chunk = fp.read()
        return chunk
    except OSError as e:
        print(f"Error accessing file {file}: {str(e)}")
        return None


def calculate_md5_hash(file: Path) -> str:
    chunk = get_last_mb(file)

    if chunk is None:
        md5_hash = "ERROR"
    else:
        md5_hash = md5(chunk).hexdigest()

    return md5_hash
