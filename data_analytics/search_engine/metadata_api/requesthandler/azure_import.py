__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.constants import constants
from DANFUtils.exceptions import DANFBaseException
from DANFUtils.tracing import tracing
from DANFUtils.utils import validate_json

from handlers.auth import auth
from handlers.import_handler import ImportHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


logger = logging.getLogger(__name__)


class ImportRequestHandler(BaseRequestHandler):

    def initialize(self):
        self.import_handler = ImportHandler()
        self.schema_post = super().load_json_schema("metadata_schema_post")

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('import/post',
                                      context_headers=self.request.headers)
        logger.info("Request: {}".format(self.request))

        try:
            metadata = validate_json(self.request.body.decode('utf-8'), self.schema_post)
        except DANFBaseException as e:
            error_message = 'Bad request: ' + str(e)
            self._handle_error(error_message, constants["REQ_STATUS_CODE_BAD_REQ"])
            return

        try:
            res = self.import_handler.upsert_metadata(metadata)
        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            tracing.end_task(success=False)
            logger.exception(error_message)
            utils.build_response(self,
                                 utils.build_error_from_message(error_message),
                                 None,
                                 constants["REQ_STATUS_CODE_SERVER_ERROR"])

            return
        
        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
        return

    def _handle_error(self, error_msg, status_code):
        logger.exception(error_msg)
        tracing.end_task(success=False)
        utils.build_response(self, utils.build_error_from_message(error_msg),
                             None, status_code)
