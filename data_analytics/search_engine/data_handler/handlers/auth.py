"""Data Handler REST API auth handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import time
from collections import OrderedDict
from functools import wraps
from typing import List

import requests
from authlib.jose import jwt
from authlib.jose.errors import BadSignatureError, InvalidClaimError, \
    DecodeError
from requests.auth import HTTPBasicAuth

from config.config_handler import config_dict
from model.exceptions import DANFApiError

logger = logging.getLogger(__name__)


class Auth:
    def __init__(self):
        self._AUTH_METHOD = config_dict.get_value('AUTH_METHOD')
        self._AUTH_DOMAIN = config_dict.get_value('AUTH_DOMAIN')
        self._AUTH_AUDIENCE = config_dict.get_value('AUTH_AUDIENCE')
        self._CLIENT_ID = config_dict.get_value('AUTH_CLIENT_ID')
        self._CLIENT_INTROSPECTION_ID = config_dict.get_value('AUTH_CLIENT_INTROSPECTION_ID')
        self._CLIENT_INTROSPECTION_SECRET = config_dict.get_value('AUTH_CLIENT_INTROSPECTION_SECRET')
        self._INTROSPECTION_URL = config_dict.get_value('AUTH_INTROSPECTION_URL')
        self._JWKS_URL = config_dict.get_value('AUTH_JWKS_URL')
        self._jwks = (self._get_jwks()
                      if self._AUTH_METHOD == "auth0"
                      else None)
        self._token_cache = TokenCache()

    def requires_scope(self, required_scopes: List[str]):
        """
        TornadoWeb request handler decorator to check oauth2 access tokens

        :param required_scopes: list of required oauth2 scopes
        :except DANFAPIError: is thrown when access is denied
        """

        def decorator(f):
            @wraps(f)
            def wrapper(*args, **kwds):
                request_handler = args[0]

                if self._AUTH_METHOD in ["auth0", "gas"]:
                    self._check_auth_scopes(request_handler, required_scopes)

                return f(*args, **kwds)

            return wrapper

        return decorator

    def _check_auth_scopes(self, request_handler, required_scopes: List[str]):
        """
        Check oauth2 access tokens

        :param required_scopes: list of required oauth2 scopes
        :except DANFAPIError: is thrown when access is denied
        """
        try:
            logger.info("Checking Authorization scopes: {}".format(
                required_scopes))

            access_token = self._get_token_auth_header(
                request_handler.request)
            # do not log tokens
            # logger.debug("Access token: {}".format(access_token))

            if self._AUTH_METHOD == "auth0":
                claims = self._validate_auth0_token(access_token, required_scopes)
            elif self._AUTH_METHOD == "gas":
                claims = self._validate_gas_token(access_token)
            else:
                raise AuthError("Invalid auth method", None, [], [])

            logger.debug("Claims: {}".format(claims))

            sub = claims.get("sub", None)
            provided_scopes = claims.get("scope", [])

            if claims['exp'] < time.time():
                raise AuthError("insufficient permissions", sub,
                                provided_scopes, required_scopes)

            for scope in required_scopes:
                if scope not in provided_scopes:
                    raise AuthError("insufficient permissions",
                                    sub, provided_scopes,
                                    required_scopes)

        except AuthError as e:
            request_handler.set_header(
                "WWW-Authenticate",
                "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\""
            )
            raise DANFApiError(401, "Access denied: " + str(e)) from e

    def _get_jwks(self):
        try:
            response = requests.get(self._JWKS_URL)
        except requests.exceptions.SSLError:
            raise AuthError("Error: Could not validate SSL certificate", None,
                            [], [])
        else:
            if not response:
                raise AuthError("Error: Could not get JWKS", None, [], [])
            return response.json()

    @staticmethod
    def _get_token_auth_header(request):
        auth_header = request.headers.get("Authorization", None)

        if not auth_header:
            raise AuthError("Authorization header is expected", None, [], [])

        parts = auth_header.split()

        if parts[0].lower() != "bearer":
            raise AuthError("Invalid Authorization header", None, [], [])
        if len(parts) == 1:
            raise AuthError("Token not found", None, [], [])
        if len(parts) > 2:
            raise AuthError("Authorization header must be Bearer token", None,
                            [], [])

        token = parts[1]
        return token

    def _validate_auth0_token(self, access_token, required_scopes: list):
        claims_options = {
            "aud": {
                "essential": True,
                "values": [self._AUTH_AUDIENCE]
            },
            "iss": {
                "essential": True,
                "values": [self._AUTH_DOMAIN]
            },
            "scope": {
                "essential": True,
                "value": required_scopes
            }
        }

        try:
            claims = jwt.decode(access_token,
                                self._jwks,
                                claims_options=claims_options)
        except DecodeError:
            raise AuthError("INVALID Token", None, [], [])
        except BadSignatureError:
            raise AuthError("INVALID Token Signature", None, [], [])
        except InvalidClaimError:
            raise AuthError("INVALID Claim", None, [], [])
        else:
            return claims

    def _validate_gas_token(self, access_token):
        if self._token_cache.get(access_token, {}).get('exp', 0) > time.time():
            logger.debug("Valid cached token found.")
            return self._token_cache.get(access_token)
        else:
            logger.debug("Valid cached token not found. Introspecting token.")
            response = self._introspect_token(access_token)
            # if the token is valid cache it
            if response.get('active', False) and response.get('client_id', '') == self._CLIENT_ID:
                response['scope'] = ['openid']
                self._token_cache.set(access_token, response)
                return response
            else:
                raise AuthError("INVALID Token", None, [], [])

    def _introspect_token(self, token):
        data = {"token": token}
        r = requests.post(self._INTROSPECTION_URL, data=data,
                          auth=HTTPBasicAuth(self._CLIENT_INTROSPECTION_ID, self._CLIENT_INTROSPECTION_SECRET))
        return r.json()


class TokenCache:
    """
    Simple in-memory FIFO token cache (not thread safe)
    """

    def __init__(self, max_size=100):
        self._cache = OrderedDict()
        self._max_size = max_size

    def set(self, key, value):
        if len(self._cache) >= self._max_size:
            self._cache.popitem(last=False)
        self._cache[key] = value

    def get(self, key, default=None):
        return self._cache.get(key, default)


class AuthError(Exception):
    def __init__(self, message, sub, provided_scopes: list,
                 required_scopes: list):
        super().__init__(message)
        self.message = message
        self.sub = sub
        self.provided_scopes = provided_scopes
        self.required_scopes = required_scopes

    @property
    def identity(self):
        if self.sub:
            return self.sub
        else:
            return "unknown"

    def __str__(self):
        return "Access denied for sub {}. {}. Provided scopes: {} but requires {}".format(
            self.identity, self.message, self.provided_scopes,
            self.required_scopes)


auth = Auth()
