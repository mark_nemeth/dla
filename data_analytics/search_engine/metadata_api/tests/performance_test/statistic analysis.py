import json
import matplotlib.pyplot as plt
import numpy as np

with open('./test_time_windows_size_1000.json') as f:
    data = json.load(f)
times=data["times"]

ar = [i*1000 for i in range(1,151)]
plt.plot(ar, times, '-')
plt.xlabel('size of time window')
plt.ylabel('time(ms)')
plt.show()


with open('./test_req_list_size_50.json') as f:
    data2 = json.load(f)
times2=data2["times"]

ar2 = np.arange(50)
plt.plot(ar2, times2, '--')
plt.xlabel('numbers of request')
plt.ylabel('time(ms)')
plt.show()

