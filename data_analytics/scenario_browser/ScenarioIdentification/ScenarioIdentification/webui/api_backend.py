import flask
from flask_restx import Namespace, Resource, reqparse
import numpy as np
import pandas as pd

from backend import filter_geom_features, filter_lsq_features, filter_turn_ind, \
    filter_left_turn, filter_right_turn, filter_lane_change, filter_steering_angle, \
    filter_dyn_obj_intersections, filter_map_objects

from data import ego_tracks_all_files, geom_features_all_files

api = Namespace("Scenarios")

rt_parser = reqparse.RequestParser()
rt_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
@api.route('/api/right_turns')
@api.expect(rt_parser)
class RightTurns(Resource):
    def get(self):
        args = rt_parser.parse_args()
        df = filter_right_turn(geom_features_all_files)
        return df.head(args['num']).to_json(orient='split')

lt_parser = reqparse.RequestParser()
lt_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
@api.route('/api/left_turns')
@api.expect(lt_parser)
class LeftTurns(Resource):
    def get(self):
        args = lt_parser.parse_args()
        df = filter_left_turn(geom_features_all_files)
        return df.head(args['num']).to_json(orient='split')

lc_parser = reqparse.RequestParser()
lc_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
@api.route('/api/lane_change')
@api.expect(lc_parser)
class LaneChanges(Resource):
    def get(self):
        args = lc_parser.parse_args()
        df = filter_lane_change(geom_features_all_files)
        return df.head(args['num']).to_json(orient='split')

    
ti_parser = reqparse.RequestParser()
ti_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
ti_parser.add_argument('ind', type=int, help='Turn indicator value (0-3)', required=True)
@api.route('/api/turn_inds')
@api.expect(ti_parser)
class TurnInd(Resource):
    def get(self):
        args = ti_parser.parse_args()
        turnind = args['ind']
        num = args['num']
        if turnind not in range(4):
            flask.abort(404)
        turn_ind_filtered = geom_features_all_files[geom_features_all_files['Turn_Ind']==turnind].copy()
        output_df = pd.DataFrame({
            'Filename':turn_ind_filtered['Filename'],
            'T': turn_ind_filtered['T'],
            'Lat':turn_ind_filtered['Lat'],
            'Lon':turn_ind_filtered['Lon'],
            'Turn_Ind':turn_ind_filtered['Turn_Ind']
        })
        return output_df.head(num).to_json(orient='split')

    
sa_parser = reqparse.RequestParser()
sa_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
sa_parser.add_argument('cutoff', type=float, help='Cutoff for Steering Angle', required=True)
@api.route('/api/steer_angle')
@api.expect(sa_parser)
class SteerAngle(Resource):
    def get(self):
        args = sa_parser.parse_args()
        sa = args['cutoff']
        num = args['num']
        if sa < 0:
            flask.abort(404)
        else:
            df = filter_steering_angle(geom_features_all_files,steer_cutoff=sa)
            return df.head(num).to_json(orient='split')
    
# do_parser = reqparse.RequestParser()
# do_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
# do_parser.add_argument('label', type=int, help='Label of Dynamic Object', required=True)
# @api.route('/api/dyn_obj_label')
# @api.expect(do_parser)
# class DynamicObject(Resource):
#     def get(self):
#         args = do_parser.parse_args()
#         label = args['label']
#         num = args['num']
#         head = flask.request.args.get('num', default=5, type=int)
#         df = filter_dyn_objects(dyn_object_selected=label)
#         return df.head(num).to_json(orient='split')

lsq_parser = reqparse.RequestParser()
lsq_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
lsq_parser.add_argument('cutoff', type=float, help='Cutoff for trajectory curvature', required=True)
@api.route('/api/traj_lsq')
@api.expect(lsq_parser)
class TrajLSQ(Resource):
    def get(self):
        args = lsq_parser.parse_args()
        lsqcutoff = args['cutoff']
        num = args['num']
        if lsqcutoff < 0:
            flask.abort(404)
        else:
            df = filter_lsq_features(geom_features_all_files, coef2_lsq_cutoff=lsqcutoff)
            return df.head(num).to_json(orient='split')

poly_parser = reqparse.RequestParser()
poly_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
poly_parser.add_argument('cutoff', type=float, help='Cutoff for trajectory curvature', required=True)
@api.route('/api/traj_poly')
@api.expect(poly_parser)
class TrajPoly(Resource):
    def get(self):
        args = poly_parser.parse_args()
        polycutoff = args['cutoff']
        num = args['num']
        if polycutoff < 0:
            flask.abort(404)
        else:
            df = filter_geom_features(geom_features_all_files, coef2_cutoff=polycutoff)
            return df.head(num).to_json(orient='split')
        
dop_parser = reqparse.RequestParser()
dop_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
dop_parser.add_argument('dyn_obj_selected', type=str, help='type of dynamic object', required=True)
dop_parser.add_argument('prox_dyn_map_obj', type=str, help='comparison map object', default=None)
dop_parser.add_argument('prox_dyn_obj_type', type=str, help='comparison object within or not within threshold', required=True)
dop_parser.add_argument('prox_dyn_cutoff', type=float, help='cutoff distance for comparison', default=50)
@api.route('/api/dyn_obj_prox')
@api.expect(dop_parser)
class DynObjProx(Resource):
    def get(self):
        args = dop_parser.parse_args()
        dyn_obj_int = filter_dyn_obj_intersections(args.dyn_obj_selected, 
                                                   args.prox_dyn_map_obj, 
                                                   args.prox_dyn_obj_type,
                                                   args.prox_dyn_cutoff)  
        return dyn_obj_int.head(args.num).to_json(orient='split')

mop_parser = reqparse.RequestParser()
mop_parser.add_argument('num', type=int, help='Number of rows to output', default=5)
mop_parser.add_argument('map_obj', type=str, help='type of map object', required=True)
mop_parser.add_argument('comp_map_obj', type=str, help='comparison map object', action='append', default=[])
mop_parser.add_argument('prox_type', type=str, help='comparison object within or not within threshold', required=True)
mop_parser.add_argument('prox_cutoff', type=float, help='cutoff distance for comparison', default=50)
@api.route('/api/map_obj_prox')
@api.expect(mop_parser)
class MapObjProx(Resource):
    def get(self):
        args = mop_parser.parse_args()
        map_objects_filtered = filter_map_objects(args.map_obj,
                                                  args.comp_map_obj,
                                                  args.prox_type,
                                                  args.prox_cutoff)
        return map_objects_filtered.head(args.num).to_json(orient='split')