import React from 'react';
import {defaultEmptyValue} from '../../../Table';
import {isObjectValid} from '../../../../utils/helperFunctions';

export default function TopicEntry(props) {

  if (!isObjectValid(props.topics)) return <span>{defaultEmptyValue}</span>;

  const formattedTopicNumber = props.topics.length + ' Topics';

  return (
    <span>{formattedTopicNumber}</span>
  )
}