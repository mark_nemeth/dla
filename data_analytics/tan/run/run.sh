substring=alwayson

for file in /mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/*/*.bag

do
    echo "$file"

    filename=$file

    if [ "${filename/$substring}" = "$filename" ] ; then
      echo "${substring} is not in $filename, skipping"
      #python find_disengagement.py $file
    else
      echo "${substring} was found in $filename, execute"
      python Find_Disengagement.py $file
    fi
done

