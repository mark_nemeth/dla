"""Regression tests for vote search route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from copy import deepcopy

import requests
from DANFUtils.utils import generate_deterministic_guid

from .testutils import get_testdb_connector

base_url = 'http://localhost:8889'
search_vote_url = base_url + '/search/vote'


test_vote_document = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "bagfile_guid": generate_deterministic_guid(),
    "extracted_event_guid": generate_deterministic_guid(),
    "account_name": "workstream1",
    "quota_document_guid": "tbr",
    "start_ts": 1577971500002,
    "end_ts": 1577971500004,
    "quota": 20,
    "is_test_data": 1
}

test_vote_document_without_eeg = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "bagfile_guid": generate_deterministic_guid(),
    "account_name": 'workpackage2',
    "quota_document_guid": 'tbr',
    "start_ts": 1577971500002,
    "end_ts": 1577971500005,
    "quota": 30,
    "is_test_data": 1
}



def prepare_db():
    with get_testdb_connector() as con:
        con.db['bva_votes'].delete_many({'is_test_data': 1})


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['bva_votes'].delete_many({'is_test_data': 1})


class TestSearchVoteGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_guid(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'guid': test_vote_document['guid'],
            'limit': 1,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document], res.json()['results'])

    def test_search_by_bagfile_guid(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'bagfile_guid': test_vote_document['bagfile_guid'],
            'limit': 1,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document], res.json()['results'])

    def test_search_by_extracted_event_guid(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'extracted_event_guid': test_vote_document['extracted_event_guid'],
            'limit': 1,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document], res.json()['results'])

    def test_search_by_account_name(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'account_name': test_vote_document['account_name'],
            'limit': 2,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document], res.json()['results'])

    def test_search_by_account_names(self):
        test_document = deepcopy(test_vote_document)
        test_document_2 = deepcopy(test_vote_document_without_eeg)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)
            con.db['bva_votes'].insert_one(test_document_2)

        query_data = {
            'account_name': [test_vote_document['account_name'], test_vote_document_without_eeg['account_name']],
            'limit': 2,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document, test_vote_document_without_eeg], res.json()['results'])

    def test_search_by_quota_document_guid(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'quota_document_guid': test_vote_document['quota_document_guid'],
            'limit': 1,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document], res.json()['results'])

    def test_search_by_start_ts_gte(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'start_ts[gte]': 1563262908000,
            'limit': 1,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document], res.json()['results'])

    def test_search_by_end_ts_gte(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'end_ts[gte]': 1563262908000,
            'limit': 1,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document], res.json()['results'])

    def test_by_quota_get(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'quota[gte]': 20,
            'limit': 1,
            'offset': 0,
            'sortby': 'guid'
        }

        res = requests.get(url=search_vote_url, params=query_data)

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([test_vote_document], res.json()['results'])

    def test_search_by_start_gte_lte(self):
        test_document = deepcopy(test_vote_document)

        with get_testdb_connector() as con:
            con.db['bva_votes'].insert_one(test_document)

        query_data = {
            'start_ts[gte]': 1577971500000,
            'start_ts[lte]': 1577971500005,
        }
        res = requests.get(url=search_vote_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by start gte and lte  failed')
        self.assertCountEqual([test_vote_document], res_json,
                              'Bagfile search by start gte and lte failed')

if __name__ == '__main__':
    unittest.main()