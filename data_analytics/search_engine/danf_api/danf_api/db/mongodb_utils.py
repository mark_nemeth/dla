"""Db related utilities."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

from motor.motor_asyncio import AsyncIOMotorClient

from .mongodb import db
from ..core.config import DB_URL

_log = logging.getLogger(__name__)


async def connect_to_mongo():
    _log.info("Connecting to DB...")
    db.client = AsyncIOMotorClient(DB_URL)
    await db.client.server_info()  # trigger a connection attempt, this will raise an error if the connection is not ok
    _log.info("DB connected")


async def close_mongo_connection():
    _log.info("Closing DB connection...")
    db.client.close()
    _log.info("DB connection closed！")
