import configHandlerSingleton from '../../configHandler';

export default () => {
  return function secured(req, res, next) {
    // Session is not required while in development#
    if (configHandlerSingleton.uiIsInDevMode()) return next();
    // Check if session exist and continue
    if (req.user) return next();
    // Return unauthorized
    req.session.returnTo = '/';
    res.status(401).send()
  };
};