import React from 'react';
import Button from 'react-bootstrap/Button';
import LabelContainer from '../Utils/LabelContainer/LabelContainer';
import './ActionBar.scss'

export function ActionBar(props) {

  function onDeleteClick() {
    props.deleteSelectedBagfiles();
  }

  function onDownloadClick() {
    props.downloadSelectedBagfiles();
  }

  return (
    <LabelContainer variant="card-outline" label="Bulk-Actions">
      <Button disabled className="action-button" onClick={onDownloadClick}>Download</Button>
      <Button disabled className="action-button" onClick={onDeleteClick}>Delete</Button>
    </LabelContainer>
  );
}