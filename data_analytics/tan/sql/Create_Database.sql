-- Database: rca

-- DROP DATABASE rca;

CREATE DATABASE rca
  WITH OWNER = postgres
       ENCODING = 'LATIN1'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US'
       LC_CTYPE = 'en_US'
       CONNECTION LIMIT = -1;

-- Table: rca_bagfilewithdropouts

-- DROP TABLE rca_bagfilewithdropouts;

CREATE TABLE rca_bagfilewithdropouts
(
  id serial NOT NULL,
  filename character varying(1000) NOT NULL,
  vehicle character varying(10) NOT NULL,
  dropout_count bigint,
  disengagement_count bigint,
  CONSTRAINT rca_bagfilewithdropouts_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rca_bagfilewithdropouts
  OWNER TO postgres;

-- Table: rca_disengagements

-- DROP TABLE rca_disengagements;

CREATE TABLE rca_disengagements
(
  id serial NOT NULL,
  "timestamp" character varying(1000) NOT NULL,
  type integer,
  details character varying(100),
  bagfile_id integer NOT NULL,
  distance double precision,
  CONSTRAINT rca_disengagements_pkey PRIMARY KEY (id),
  CONSTRAINT rca_disengagements_bagfile_id_c6d8052c_fk_rca_bagfi FOREIGN KEY (bagfile_id)
      REFERENCES rca_bagfilewithdropouts (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rca_disengagements
  OWNER TO postgres;

-- Index: rca_disengagements_bagfile_id_c6d8052c

-- DROP INDEX rca_disengagements_bagfile_id_c6d8052c;

CREATE INDEX rca_disengagements_bagfile_id_c6d8052c
  ON rca_disengagements
  USING btree
  (bagfile_id);

