"""Unittest for child_bagfile_search_handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import unittest
from handlers.feature_search_handler import FeatureSearchHandler

bagfile_guid = '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'


class TestFeatureSearchHandler(unittest.TestCase):
    def test_create_feature_query(self):
        fh = FeatureSearchHandler()

        query = {
            'feat.vel[gte]': [0],
            'feat.acc[lte]': [100],
            'feat.eng_state[]': [11],
            'limit': [2],
            'sortby': ['-start_index'],
            "bagfile_guid": [bagfile_guid]
        }
        expected = {
            'query': {
                'bagfile_guid': '233985c0-5f9d-4e46-9c7c-56fe58bce5ed',
                'features': {
                    '$elemMatch': {
                        'vel': {
                            '$gte': 0.0
                        },
                        'acc': {
                            '$lte': 100.0
                        },
                        'eng_state': 11.0
                    }
                }
            },
            'limit': 2,
            'offset': 0,
            'sortby': [('start_index', -1)]
        }
        res = fh.create_search_kwargs(query)
        self.assertEqual(expected, res, msg="Query wrong for 4 items!")


if __name__ == '__main__':
    unittest.main()
