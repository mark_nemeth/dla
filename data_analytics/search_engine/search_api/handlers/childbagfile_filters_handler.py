"""Handler class for filters"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from handlers.db_data_handler import DBDataHandler

class ChildBagFileFiltersHandler:

    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def get_filters(self):
        childbagfile_duration_min_max = self.db_data_handler.get_childbagfile_duration_min_max()
        locations = self.db_data_handler.get_all_location()
        childbagfile_size_min_max = self.db_data_handler.get_childbagfile_size_min_max()
        topics = self.db_data_handler.get_all_topics()
        vehicle_ids = self.db_data_handler.get_all_vins()
        filters = {
            "duration_min_max": childbagfile_duration_min_max,
            "locations": locations,
            "size_min_max": childbagfile_size_min_max,
            "topics": topics,
            "vehicle_ids": vehicle_ids
        }
        return filters
