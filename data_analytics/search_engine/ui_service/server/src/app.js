import express from 'express';
import path from 'path';

import corsLoader from './loaders/corsLoader';
import expressLoader from './loaders/expressLoader';
import sessionLoader from './loaders/sessionLoader';
import oAuthLoader from './loaders/oAuthLoader';

import clientRoutes from './routes/client';
import apiRoutes from './routes/api';

// Init express app with all loaders
const app = express();
expressLoader(app);
corsLoader(app);
sessionLoader(app);
oAuthLoader(app);

// Routes
app.use('/', clientRoutes);
app.use('/api/v1/', apiRoutes);

// Static file serve
app.use(express.static('public'));
app.use(express.static(path.join(__dirname, '../clients/metadata')));

export default app;
