import React from 'react'
import ReactJson from 'react-json-view'
import Container from "react-bootstrap/Container";
import './JsonView.scss'

export function JsonView(props) {
  return (
      <Container fluid className="json-detail">
        <ReactJson src={props.json}
                   displayDataTypes={false}
                   enableClipboard={false}
                   displayObjectSize={false}
                   name={props.rootNodeName}
                   collapsed={1}
                   iconStyle="triangle"
                   sortKeys={true}
        />
      </Container>
  )
}
