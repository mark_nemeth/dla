import { createAction } from '@reduxjs/toolkit';
import configHandler from '../../config/configHandler';
import axios from 'axios';
import XLSX from 'xlsx';
import strings from '../../assets/string/default';
import { insertString } from '../../assets/string/stringHelper';

// Date
export const selectReportingDate = createAction('reporting/selectDate');
export const selectReportingLocation = createAction('reporting/selectLocation');
export const selectReportType = createAction('reporting/selectReportType');
export const setTripFilterSelection = createAction('reporting/setTripFilterSelection');
export const setTripFilterState = createAction('setTripFilterState');
export const prepareReportLoading = createAction('reporting/prepareReportLoading');
export const finalizeReportLoading = createAction('reporting/finalizeReportLoading');

//endregion

export function createReport() {
  return (dispatch, getState) => {
    dispatch(prepareReportLoading());
    loadReportKpis(dispatch, getState().reporting);
    loadReportTablesSimple(dispatch, getState().reporting);
    loadReportTablesAggregation(dispatch, getState().reporting);
  };
}

// Load KPIs
function loadReportKpis(dispatch, reportingState) {
  return axios.request(configHandler.getReportingKpiAPI(collectFilters(reportingState)))
    .then(res => {
      if (res.data.results.kpis.length > 0) {
        dispatch(fetchReportKpisSuccess({ kpis: res.data.results.kpis }));
        dispatch(finalizeReportLoading());
      } else {
        handleError(dispatch, "KPI result is empty", fetchReportKpisEmpty(insertString(strings.no_report_data_found)));
      }
    })
    .catch(err => {
      handleError(dispatch, err, fetchReportKpisError(insertString(strings.connection_error)));
    });
}
export const fetchReportKpisSuccess = createAction('reporting/fetchReportKpisSuccess');
export const fetchReportKpisError = createAction('filter/fetchReportKpisError');
export const fetchReportKpisEmpty = createAction('filter/fetchReportKpisEmpty');

// Load Simple Tables
function loadReportTablesSimple(dispatch, reportingState) {
  return axios.request(configHandler.getReportingTablesSimpleAPI(collectFilters(reportingState)))
    .then(res => {
      if (res.data.results.tables.length > 0) {
        dispatch(fetchReportTablesSimpleSuccess({ tables: res.data.results.tables }));
        dispatch(finalizeReportLoading());
      } else {
        handleError(dispatch, "Table result is empty", fetchReportTablesSimpleEmpty(insertString(strings.no_report_data_found)));
      }
    })
    .catch(err => {
      handleError(dispatch, err, fetchReportTablesSimpleError(insertString(strings.connection_error)));
    });
}
export const fetchReportTablesSimpleSuccess = createAction('report/fetchReportTableSimpleSuccess');
export const fetchReportTablesSimpleError = createAction('filter/fetchReportTablesSimpleError');
export const fetchReportTablesSimpleEmpty = createAction('filter/fetchReportTablesSimpleEmpty');

// Load Aggregation Tables
function loadReportTablesAggregation(dispatch, reportingState) {
  return axios.request(configHandler.getReportingTablesAggregationAPI(collectFilters(reportingState)))
    .then(res => {
      if (res.data.results.tables.length > 0) {
        dispatch(fetchReportTablesAggregationSuccess({ tables: res.data.results.tables }));
        dispatch(finalizeReportLoading());
      } else {
        handleError(dispatch, "Table result is empty", fetchReportTablesAggregationEmpty(insertString(strings.no_report_data_found)));
      }
    })
    .catch(err => {
      handleError(dispatch, err, fetchReportTablesAggregationError(insertString(strings.connection_error)));
    });
}

export const fetchReportTablesAggregationSuccess = createAction('report/fetchReportTablesAggregationSuccess');
export const fetchReportTablesAggregationError = createAction('filter/fetchReportTablesAggregationError');
export const fetchReportTablesAggregationEmpty = createAction('filter/fetchReportTablesAggregationEmpty');

function collectFilters(reportingState) {
  return Object.assign({ ...reportingState.globalFilters }, ...reportingState.tripFilters.map(item => ({ [item.id]: item.selected })));
}

// Export report to csv
export function exportReport() {
  return (dispatch, getState) => {
    var reportType = getState().reporting.reportTypes[getState().reporting.globalFilters.report_type].name;
    var location = getState().reporting.locations[getState().reporting.globalFilters.location].name;
    var wb = XLSX.utils.book_new();

    // Add overview sheet (aggregation tables + kpis)
    var overviewTable = [[],
    ["", reportType + " Report Overview"],
    [],
    ["", "Location", location]];
    getState().reporting.data.kpis.forEach((kpi, idx, arr) => {
      overviewTable.push(["", kpi.label, kpi.value]);
    });
    overviewTable.push([], []);

    getState().reporting.data.aggregationTables.forEach((table, idx, arr) => {
      overviewTable.push(["", table.caption], ["", ...table.colLabels]);
      table.values.forEach((row, idx, arr) => {
        var outRow = [""];
        for (let key in row) {
          outRow.push(row[key]);
        }
        overviewTable.push(outRow);
      });
      overviewTable.push([], []);
    });
    var wsOverviewTable = XLSX.utils.aoa_to_sheet(overviewTable);
    XLSX.utils.book_append_sheet(wb, wsOverviewTable, "Overview");

    // Add details sheet (simple tables)
    var detailTable = [[], ["", reportType + " Report Details"], []];
    getState().reporting.data.simpleTables.forEach((table, idx, arr) => {
      detailTable.push(["", ...table.colLabels]);
      table.values.forEach((row, idx, arr) => {
        var outRow = [""];
        for (let key in row) {
          outRow.push(row[key]);
        }
        detailTable.push(outRow);
      });
      table.valuesHidden.forEach((row, idx, arr) => {
        var outRow = [""];
        for (let key in row) {
          outRow.push(row[key]);
        }
        detailTable.push(outRow);
      });
      detailTable.push([]);
    });

    var wsDetailTable = XLSX.utils.aoa_to_sheet(detailTable);
    XLSX.utils.book_append_sheet(wb, wsDetailTable, "Details");

    XLSX.writeFile(wb, 'report.xlsx');
  }
}

function handleError(dispatch, err, errorTask) {
  console.log(err);
  dispatch(errorTask);
  dispatch(finalizeReportLoading());
}