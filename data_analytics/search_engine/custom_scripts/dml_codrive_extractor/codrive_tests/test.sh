#!/bin/bash

# This file builds and runs the ATPDML Co-Passenger-Annotated-Event extractor 
# on the local filesystem and abstracts away the DANF APIs by using 
# a simple HTTPS echo docker image (mendhak/http-https-echo
echo "Usage: ./build-and-test.sh <bagfile> <guid>"

# Set environment variables for docker build & run
here=`dirname $(readlink -f $0)`
bagfile_on_host=${1:-$here/20190806_194026_DML_PER_Route_4_BW_cv1_2_events.bag}
bagfile_guid=${2:-fb59ecdb-72bb-4388-81b8-9b64e721678a}
bagfile_path=/input/$(basename -- "$bagfile_on_host")
metadata_api_base_url=http://localhost:8080/
search_api_base_url=http://localhost:8080/
extractor_name=codrive
extractor_version=1.0

mkdir -p output
rm -f $here/output/extraction_request.json

docker run \
     --network host --rm -t \
     -v "$bagfile_on_host:$bagfile_path" \
     -v "$here/output:/output" \
     -v "$here/logs:/usr/local/lib/python3.6/dist-packages/DANFUtils/logging/logs" \
     -e BAGFILE_GUID=$bagfile_guid \
     -e BAGFILE_PATH=$bagfile_path \
     -e METADATA_API_BASE_URL=$metadata_api_base_url \
     -e SEARCH_API_BASE_URL=$search_api_base_url \
     -e HTTP_PROXY=$HTTP_PROXY \
     -e HTTPS_PROXY=$HTTPS_PROXY \
     -e EXTRACTOR_VERSION=$extractor_version \
     -e OFFLINE_MODE=True \
     -e OFFLINE_OUTPUT_PATH="/output/" \
     -e APPINSIGHTS_INSTRUMENTATIONKEY="APPINSIGHTS_DISABLE_KEY" \
     -e BAGFILE_RESPONSIBLE="BAGFILE_SKIP_RESPONSIBLE" \
     -e CO_DRIVER_EVENT="/events" \
     -e EXTRACTOR_NAME=$extractor_name \
     $extractor_name:$extractor_version

cmp --silent $here/output/extraction_request.json $here/extraction_request.json \
      && echo "Extractor sent out correct request. Test passed." \
      || echo "Extractor sent out wrong request. Test failed."
exit $?