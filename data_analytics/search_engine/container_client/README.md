# Container Client
### Version: see setup.py

## First steps:
1. Press 'Connect to feed' button on this page: https://daimler.visualstudio.com/Athena/_packaging?_a=feed&feed=ATTDATA
2. Select 'Python' from the left column in newly opened window.
3. In section 'Get packages with pip' press 'Generate Python credentials' and copy the value of index-url (it looks like: https://ATTDATA:******@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/ )
4. Define environment variable and set a value taken from the previous step.

```console
export index_url_with_access_from_azure_devops=https://ATTDATA:PASSWORD-TAKEN-FROM-AZURE-DEVOPS-ARTIFACTS-CONNECT-TO-FEED-BUTTON@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```

## Prerequisites
Install setuptools and wheel.
```python
pip install --upgrade setuptools wheel
```

## Set environment variables
```console
export METADATA_API_BASE_URL=https://localhost:8443/
export SEARCH_API_BASE_URL=https://localhost:8445/
```

Refer to the example config/config_dev.init for the complete list of config environment variables

Note that Container Client uses the config/config.init file as default config values
but environment variables override these defaults.

## Installation
```python
pip install . --extra-index-url=$index_url_with_access_from_azure_devops
```

## Usage
See tests/regressiontest/db_connector_test.py

# Authentication

DANF APIs are using Daimler GAS-OIDC for user authentication.
Client requests must provide a valid Oauth 2.0 bearer token
authorization header to access endpoints protected by the auth handler.

Client Credentials Flow has been implemented in DANF ContainerClient
to automatically authenticate clients via *CLIENT_CRED_ID and *CLIENT_CRED_SECRET
environment variables. See config_dev.init example for relevant AUTH config parameters.
In production clusters these parameters are initialized from k8s secrets. 

Further information about Daimler GAS-OIDC is available on
https://team.sp.wp.corpintra.net/sites/05389/GAS-OIDC/SitePages/DaimlerHome.aspx