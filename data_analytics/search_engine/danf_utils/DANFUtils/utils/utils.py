"""Utils"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import random as rd
import uuid
import json
from datetime import datetime, timezone
from pathlib import Path
from DANFUtils.logging import logger
import jsonschema

from DANFUtils.exceptions import JSONParseError, JSONValidationError


def generate_guid(unique_url=None):
    """
    Generates a GUID (e.g. 233985c0-5f9d-4e46-9c7c-56fe58bce5ed).
    :param unique_url: Optional unique string that is used to generate a deterministic GUID.
    :return: The GUID string.
    """
    if unique_url:
        return str(uuid.uuid3(uuid.NAMESPACE_URL, unique_url))
    else:
        return str(uuid.uuid4())


def generate_bag_guid(bag_link: str) -> str:
    """
    Generates a deterministic GUID for a bag (bagfile or "childbagfile").
    :param bag_link: The original ingestion link. Unique for all bags.
    :return: The GUID string.
    """
    return generate_guid(f'https://dla.bosch.com/bag_link/{bag_link}')


def generate_deterministic_guid():
    """
    Generate a deterministic GUID based on pre-defined rd.seed() (FOR TEST PURPOSES ONLY!)
    :return: The GUID string
    """
    return str(uuid.UUID(bytes=bytes(rd.getrandbits(8) for _ in range(16)), version=4))


def validate_json(json_string, schema):
    """
    Parse the given json_string and validate it against the given schema
    :param json_string: JSON String
    :param schema: A valid JSON schema
    :return: A dictionary holding the parsed json
    """
    # Parsing
    try:
        data = json.loads(json_string)
    except ValueError as e:
        raise JSONParseError("DANFUtils", 'Unable to parse JSON: ' + str(e))

    # Validation
    try:
        jsonschema.validate(data, schema)
    except jsonschema.ValidationError as e:
        raise JSONValidationError("DANFUtils", 'JSON validation failed: ' + str(e.message))

    except jsonschema.SchemaError as e:
        raise JSONValidationError("DANFUtils", 'JSON validation failed: ' + str(e.message))

    return data


def get_timestamp() -> int:
    """Returns the current time as unix timestamp in millisecond resolution.
    This is the format that is most commonly used in DANF internally and
    should be used to represent a point in time unless there are good reasons
    to use a different representation.

    :return: current time as unix timestamp in milliseconds
    """
    return int(datetime.now(tz=timezone.utc).timestamp() * 1000)


def generate_file_guid(file_name: str) -> str:
    """Generates a unique file guid in uuid format used in the file catalog. For the root guid see ROOT_FILE_GUID in
    DANFUtils constants.

    :param file_name: Name of file.
    :return: The deterministic guid.
    """
    url = f'https://dla.bosch.com/file/{file_name}'
    return str(uuid.uuid3(uuid.NAMESPACE_URL, url))


def extract_file_guid(full_file_path):
    """
    Extracts the file guid based on the file name and file size.
    :param full_file_path: Full path of the file.
    :return: The deterministic guid of the file.
    """
    path = Path(full_file_path)
    file_name = path.name
    logger.debug(f"Generating file guid for {file_name}.")
    file_guid = generate_file_guid(file_name)
    return file_guid
