import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import httpMocks from 'node-mocks-http'
import DataHandlerApi from './dataHandlerApi';
import configHandlerSingleton from '../configHandler';
import storeProvider from '../models/storeProvider';

const mock = new MockAdapter(axios);

describe('Routes for DataHandler API', () => {

  const req = httpMocks.createRequest({
    body: {
      file_paths: [
        '/mapr/624.mbc.us/data/input/rd/athena/08_robotaxi/plog/byName/v222-5215/2018/12/11/20181211_133439_alwayson.bag',
      ]
    }
  });

  configHandlerSingleton.getReprocessingAPIConfig = jest.fn(() => {
    return {url: '/files/reprocess'}
  });

  storeProvider.getUserTokenDataAsync = jest.fn(() => {
    return Promise.resolve({
      token_type: 'Bearer',
      access_token: 'access-token-1337'
    });
  });

  const mockDataHandlerApiRequest = mock.onGet('/files/reprocess');

  it('call routes for reprocessing a bagfile and get a result', () => {
    const responseData = {
      results: [
        {
          file_path: '/mapr/624.mbc.us/data/input/rd/athena/08_robotaxi/plog/byName/v222-5215/2018/12/11/20181211_133439_alwayson.bag',
          bagfile_guid: 'da3f6136-8ad7-49eb-bf4a-f8577f280601',
          result: 'SUCCESSFUL'
        }
      ]
    };
    mockDataHandlerApiRequest.reply(200, responseData);

    return DataHandlerApi.triggerReprocessFromAPI(req).then((response) => {
      expect(response.data).toStrictEqual(responseData);
    })
  });

});