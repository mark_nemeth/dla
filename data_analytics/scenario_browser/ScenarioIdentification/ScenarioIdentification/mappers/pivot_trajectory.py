import numpy as np
import pandas as pd


if __name__=='__main__':
    in_file='../Data/geom_features_all_files.csv'
    out_file='../Data/geom_features_all_files_timetraj_pivot.csv'
    time_bin=10. #in Seconds
    res=1
    resample_window=f'{res}s'
    relevant_columns=['Accel_X', 'Accel_Y', 'Lat', 'Lon', 'Steer_Angle', 'Turn', 'Turn_Ind',
           'Turn_Index', 'Turn_Type', 'Velocity_X', 'Yaw_Rate', 'coef0_lstsq',
           'coef0_polyfit', 'coef1_polyfit', 'coef2_lstsq', 'coef2_polyfit',
           'd2y_dx2', 'd2y_dx2_sm']


    #=======================
    #read csv
    #=======================
    df=pd.read_csv(in_file)
    set_files=list(set(df.Filename))


    #=======================
    #resample and pivot
    #=======================

    d_res_list=[]

    for i in set_files:
        dtemp=df[df.Filename==i].copy()
        dtemp['t_delta']=dtemp['T']-dtemp['T'].min()
        t_delta_max=dtemp.t_delta.max()
        dtemp['T']=pd.to_timedelta(dtemp['T']-dtemp['T'].min(),'s')
        dtemp.index=dtemp['T']
        dtemp=dtemp.resample(resample_window).mean().interpolate().ffill().bfill()
        dtemp['time_id']=(dtemp['t_delta'].astype('float64')/time_bin).astype(int)
        dtemp['time_step']=(dtemp['t_delta'].astype('int')%int(time_bin)).astype('str')
        d_pivot=dtemp.pivot_table(index='time_id',columns='time_step',values=relevant_columns)
        d_pivot.columns=['_'.join(col).strip() for col in d_pivot.columns.values]
        d_pivot=d_pivot.reset_index()
        d_pivot['Filename']=i
        d_res_list.append(d_pivot)
    d_res=pd.concat(d_res_list)
    d_res=d_res.reset_index()
    d_res.drop(columns=['index'],inplace=True)


    #=======================
    #save csv
    #=======================
    print(f"Saving pivot file to:{out_file}")
    d_res.to_csv(out_file)