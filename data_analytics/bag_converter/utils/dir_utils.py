__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import subprocess

from config import ExceptionHandler
from config import config_handler as configurator
from config import logging_handler as logger


def check_mapr():
    mapr_dir_env = configurator.get_value('mapr_dir_env')
    commands = [
        "timeout", configurator.get_value('dla_conv_timeout_app'), "ls", mapr_dir_env
    ]

    if configurator.get_value('log_level_env') == 'debug':
        logger.debug(f'send commands to shell: {commands}')

    proc = subprocess.Popen(commands, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = proc.communicate()
    output = output.decode("utf-8")
    returncode = proc.returncode

    if configurator.get_value('log_level_env') == 'debug':
        logger.debug(f'results of dla_converter: {output}')

    if returncode != 0:
        raise ExceptionHandler(code='mapr_not_found_err', debug_msg=f"mapr dir set to: {mapr_dir_env}")
