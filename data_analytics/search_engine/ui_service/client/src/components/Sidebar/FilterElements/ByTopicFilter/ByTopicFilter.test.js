import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByTopicFilter} from './ByTopicFilter';

describe('ByTopicFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      topicList: [
        '/pc_type',
        '/sensor/axis_camera/cam_left/compressed',
        '/diagnostics_toplevel_state',
        '/diagnostics'
      ],
      selectedTopicList: []
    };
    const wrapper = shallow(<ByTopicFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should renders topic list and selected topic list', () => {
    const props = {
      topicList: [
        '/pc_type',
        '/sensor/axis_camera/cam_left/compressed',
        '/diagnostics_toplevel_state',
        '/diagnostics'
      ],
      selectedTopicList: ['/diagnostics', '/pc_type'],
      filterByTopics: jest.fn()
    };
    const wrapper = mount(<ByTopicFilter {...props} />);
    const entries = wrapper.find('tr');
    expect(entries.length).toBe(2);
  });

  it('should show suggestion list when searchbox clicked', () => {
    const props = {
      topicList: [
        '/pc_type',
        '/sensor/axis_camera/cam_left/compressed',
        '/diagnostics_toplevel_state',
        '/diagnostics'
      ],
      selectedTopicList: ['/diagnostics'],
      filterByTopics: jest.fn(),
      setCurrentTopicSearchQuery: jest.fn()
    };
    const wrapper = mount(<ByTopicFilter {...props} />);
    const searchbar = wrapper.find('input');
    expect(wrapper.find('.suggestion-item').length).toBe(0);
    expect(wrapper.find('.suggestion-item-active').length).toBe(0);
    searchbar.simulate('click');
    expect(wrapper.find('.suggestion-item').length).toBe(3);
    expect(wrapper.find('.suggestion-item-active').length).toBe(1);
    expect(wrapper.find('.suggestion-item').length + wrapper.find('.suggestion-item-active').length).toBe(4);
  });

  it('should call action when topic is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      topicList: [
        '/pc_type',
        '/sensor/axis_camera/cam_left/compressed',
        '/diagnostics_toplevel_state',
        '/diagnostics'
      ],
      selectedTopicList: [],
      filterByTopics: reduxAction,
      setCurrentTopicSearchQuery: jest.fn()
    };
    const wrapper = mount(<ByTopicFilter {...props} />);
    const  searchbar = wrapper.find('input');
    searchbar.simulate('click');
    const suggestionItems = wrapper.find('.suggestion-item');
    suggestionItems.first().find('input').simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

  it('should call action when selected topic is removed', () => {
    const reduxAction = jest.fn();
    const props = {
      topicList: [
        '/pc_type',
        '/sensor/axis_camera/cam_left/compressed',
        '/diagnostics_toplevel_state',
        '/diagnostics'
      ],
      selectedTopicList: ['/pc_type'],
      filterByTopics: reduxAction,
    };
    const wrapper = mount(<ByTopicFilter {...props} />);
    wrapper.find('.select-topic-item').first().find('.icon-subtract').simulate('click');
    expect(reduxAction).toHaveBeenCalled();
  });
});
