#!/bin/sh
#This script gets the top values of the system and prints it to the output
TT=`date +%j%H%M%S`
TIME=1

function logtop
{
	echo "top values every  9 x $TIME seconds"

	while true; do

	PTOP=$(top -b -d3 -n3 | grep Cpu)
	echo $(echo $PTOP | awk -F % '{ print $4 }')

	PTOP=$(top -b -d3 -n3 | grep "KiB Mem" )
	echo $(echo $PTOP | awk -F KiB '{ print $4 }')

	sleep $TIME

	done

}
logtop > ptop_$TT.log











