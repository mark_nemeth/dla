"""General models."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import NamedTuple, Optional, List, TypeVar, Type

from pydantic import BaseModel, Field

T = TypeVar("T", bound="OpValue")


class OpValue(NamedTuple):
    op: str
    value: str

    @classmethod
    def __get_validators__(cls):
        """https://pydantic-docs.helpmanual.io/usage/types/#classes-with-__get_validators__"""
        yield cls.from_rhs_colon

    @classmethod
    def from_rhs_colon(cls: Type[T], s: str) -> T:
        parts = s.split(":", maxsplit=1)
        return cls(value=parts.pop(), op=parts.pop() if parts else "")


FilterParams = TypeVar("FilterParams", bound=BaseModel)


class BaseQueryParams(BaseModel):
    filter_params: FilterParams
    # circumventing the builtin "fields" attribute of BaseModel
    fields_: Optional[List[OpValue]] = Field(alias="fields")
    order_by: List[OpValue]
    limit: int = 20
    offset: int = 0


QueryParams = TypeVar("QueryParams", bound=BaseQueryParams)
