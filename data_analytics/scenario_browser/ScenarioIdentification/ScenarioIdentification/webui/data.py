import numpy as np
import pandas as pd

## load data
ego_tracks_all_files = pd.read_csv("../Data/ego_tracks_all_files.csv", dtype=object)
geom_features_all_files = pd.read_csv("../Data/geom_features_w_lc.csv")
tracks_df_intersections = pd.read_csv("../Data/tracks_intersection_dyn_obj_ego_w_relative.csv") #TODO: tracks would come back later
dyn_intersections = pd.read_csv('../Data/dynamic_objects_intersections_w_mo_small.csv')
map_objects = pd.read_csv('../Data/map_objects.csv')
## end of load data

# dyn object list:
dyn_obj_list = pd.unique(dyn_intersections['Label']).tolist()
map_obj_list = pd.unique(map_objects['object_type']).tolist()

# pivot file
pivot_file = pd.read_csv('../Data/geom_features_all_files_timetraj_pivot.csv')
dyn_pivot = pd.read_csv('../Data/dyn_features_pivot.csv')
pivot_proximity = pd.read_csv('../Data/proximity_features_ego_pivot.csv')