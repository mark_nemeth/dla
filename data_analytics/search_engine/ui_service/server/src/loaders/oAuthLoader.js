import passport from 'passport/lib';
import Auth0Strategy from 'passport-auth0';
import HttpsProxyAgent from 'https-proxy-agent';
import jwt from 'jsonwebtoken';
import {custom, Issuer, Strategy} from 'openid-client';
import tunnel from 'tunnel';
import configHandlerSingleton from '../configHandler';
import storeProvider from '../models/storeProvider';
import {isObjectValid} from '../utils/helperFunctions';
import getLogger from '../utils/logging';

const logger = getLogger(__filename)
const oAuthScope = 'openid email profile';
export const DAIMLER_STRATEGY_ID = 'oidc';
export const AUTH0_STRATEGY_ID = 'auth0';

// Check if cloud variable for development is set
export function getOAuthStrategyId() {
  return configHandlerSingleton.useDaimlerLogin() ? DAIMLER_STRATEGY_ID : AUTH0_STRATEGY_ID;
}

export default (app) => {

  const strategy = getOAuthStrategyId() === DAIMLER_STRATEGY_ID ? useDaimlerStrategy() : useAuth0Strategy();
  passport.use(getOAuthStrategyId(), strategy);

  // You can use this section to keep a smaller payload
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });
  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  app.use(passport.initialize());
  app.use(passport.session());
  logger.info('OAUTH: ' + getOAuthStrategyId() + ' enabled');

  return app;
}

function useAuth0Strategy() {
  const strategy = new Auth0Strategy({
      domain: configHandlerSingleton.getAuthDomain(),
      clientID: configHandlerSingleton.getAuthClientID(),
      clientSecret: configHandlerSingleton.getAuthClientSecret(),
      callbackURL: configHandlerSingleton.getAuthCallbackURL(),
      scope: oAuthScope
    },
    function (accessToken, refreshToken, extraParams, profile, done) {
      // console.log('Auth0Strategy: '); console.log(accessToken); console.log(refreshToken); console.log(extraParams); console.log(profile);
      storeProvider.saveUserTokenData(profile.id, extraParams);
      storeProvider.saveUserData(profile.id, profile);
      return done(null, profile);
    }
  );

  // Enable daimler proxy support for login in dev mode
  if (!configHandlerSingleton.isNodeEnvInProduction() && configHandlerSingleton.hasProxy()) {
    const agent = new HttpsProxyAgent(configHandlerSingleton.getProxyUrl());
    strategy._oauth2.setAgent(agent);
  }

  return strategy;
}

function useDaimlerStrategy() {

  const daimlerIssuer = new Issuer({
    issuer: configHandlerSingleton.getAuthDomain(),
    authorization_endpoint: configHandlerSingleton.getAuthDomain() + configHandlerSingleton.getAuthAuthEndpoint(),
    token_endpoint: configHandlerSingleton.getAuthDomain() + configHandlerSingleton.getAuthTokenEndpoint(),
    userinfo_endpoint: configHandlerSingleton.getAuthDomain() + configHandlerSingleton.getAuthUserEndpoint(),
    jwks_uri: configHandlerSingleton.getAuthDomain() + configHandlerSingleton.getAuthJWKSEndpoint()
  });

  const client = new daimlerIssuer.Client({
    client_id: configHandlerSingleton.getAuthClientID(),
    client_secret: configHandlerSingleton.getAuthClientSecret(),
    redirect_uris: [configHandlerSingleton.getAuthCallbackURL()],
  });

  const params = {
    scope: oAuthScope
  };

  // Enable daimler proxy support for login in dev mode and change timeout for long going login request for dev and prod
  if (!configHandlerSingleton.isNodeEnvInProduction() && configHandlerSingleton.hasProxy()) {
    custom.setHttpOptionsDefaults({
      agent: tunnel.httpsOverHttp({
        proxy: configHandlerSingleton.proxy
      }),
      timeout: 15000
    });
  } else {
    custom.setHttpOptionsDefaults({
      timeout: 15000
    });
  }

  return new Strategy({client, params},
    (tokenset, done) => {
      // console.log('DaimlerStrategy: '); console.log(tokenset); console.log(jwt.decode(tokenset.id_token));
      // ID Token verification
      if (!validateIdToken(tokenset.id_token)) return done(null, false, { message: 'Security error: ID Token verification was not successful' });
      const userData = jwt.decode(tokenset.id_token);
      if (userData.hasOwnProperty('jti')) userData.id = getOAuthStrategyId() + '|' + userData.jti;
      storeProvider.saveUserTokenData(userData.id, tokenset);
      storeProvider.saveUserData(userData.id, userData);
      return done(null, userData);
    }
  );

  function validateIdToken(idToken) {
    const userData = jwt.decode(idToken);
    if (!isObjectValid(userData)) return false;
    if (userData.iss !== configHandlerSingleton.getAuthDomain()) return false;
    if (userData.aud !== configHandlerSingleton.getAuthClientID()) return false;
    if (userData.exp >= Date.now()) return false;
    return true;
  }
}