# -*- coding: utf-8 -*-
__copyright__ = '''
COPYRIGHT: (c) 2017-2018 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

"""Module provides a set of classes which allow to define the system state."""


class SysState:
    # AD System state is unknown: SEM is not yet initialized
    # (=default).
    kStateUnknown = 0
    # AD System is initializing
    kStateInit = 1
    # AD System is running
    kStateRun = 2
    # AD System is terminating
    kStateTerminate = 3
    # AD System is in an error state/degraded state (note: not yet implemented)
    kStateError = 4


class SysOpMode:
    # AD System opmode is unknown: SEM is not yet initialized
    # (=default).
    kOpUnknown = 0
    # AD System is initializing SADCs
    kOpSADCWakeup = 1
    # AD System is initializing PADCs
    kOpPADCWakeup = 2
    # AD System is calculating pose and horizon
    kOpReadyToDrive = 3
    # AD System is Autonomous Driving
    kOpAutonomousDriving = 4
    # AD System is in mode Remote Assistance request
    kOpRemoteAssistanceRequest = 5
    # AD System is in mode high-level Remote Assistance
    kOpRemoteAssistanceHigh = 6
    # AD System is in mode mid-level Remote Assistance
    kOpRemoteAssistanceMid = 7
    # AD System is in mode low-level Remote Assistance
    kOpRemoteAssistanceLow = 8
    # AD System is in mode Remote Assistance communication
    kOpRemoteAssistanceCom = 9
    # AD System is in mode Remote Assistance completed
    kOpRemoteAssistanceComplete = 10
    # AD System is in Safety Driver mode
    kOpSafetyDriver = 11
    # AD System is in Shutting Down mode
    kOpShutDown = 12
    # AD System is in Degraded mode
    kOpDegraded = 13


class SysDegradation:
    # AD System degradation is unknown: SEM is not yet initialized
    # (=default).
    kDegUnknown = 0
    # AD System not yet ready (added @03.2018 for engagement handling)
    kDegNotEngagementReady = 1
    # AD System has no degradation
    kDegNone = 2
    # AD System is in Remote Assistance: The AD system requires
    # remote assistance in order to continue operation
    kDegRemoteAssistance = 3
    # AD System is in Emergency Stop
    kDegEmergencyStop = 4
    # AD System is in degradation level 1
    kDegLevel1 = 5
    # AD System is in degradation level 2
    kDegLevel2 = 6


class ADEngagementState:
    # unkown
    unknown = -1
    # initializing
    initializing = 0
    # ready to engage
    ready = 1
    # engaged. System is in the autonomous mode
    engaged = 2
    # system error
    error = 3
