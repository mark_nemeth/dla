"""Document Collection Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import json
from pymongo import UpdateOne
from pymongo.errors import BulkWriteError

from DANFUtils.utils import validate_json
from DANFUtils.logging import logger
from DANFUtils.exceptions import DANFBaseException
from DBConnector.exceptions import IndexOverwriteException, BulkOperationException
from DBConnector.utils import calculate_request_size, MAXIMUM_REQUEST_BATCH_SIZE


class DocumentCollectionHandler:
    """
    DocumentCollectionHandler
    """

    # Document index
    DOCUMENTS_COLLECTION_NAME = 'documents'
    DOCUMENTS_INDEX_NAME = 'documents_guid_index'
    DOCUMENTS_INDEX_FIELD_GUID = 'guid'

    def __init__(self, db):
        self.db = db

        col = self.db[self.DOCUMENTS_COLLECTION_NAME]
        col.create_index(self.DOCUMENTS_INDEX_FIELD_GUID,
                         unique=True,
                         name=self.DOCUMENTS_INDEX_NAME)
        try:
            base_path = os.path.abspath(os.path.dirname(__file__))
            schema_file = f"{base_path}/../schema/document_query.schema"

            with open(schema_file) as f:
                schema_query = f.read()
        except (IOError, OSError) as e:
            error_message = 'Could not read document query schema'
            logger.exception(error_message)
            return
        try:
            self.schema_query = json.loads(schema_query)
        except ValueError as e:
            error_message = 'Bad schema: unable to parse document query schema'
            logger.exception(error_message)

    def add(self, documents):
        assert isinstance(
            documents, list), 'Data inserted into db has to be a list of dicts'
        assert all(
            isinstance(x, dict) for x in
            documents), 'Data inserted into db has to be a list of dicts'
        assert all('guid' in x for x in
                   documents), 'Documents inserted into db must have guid key'
        assert all('type' in x for x in
                   documents), 'Documents inserted into db must have type key'
        col = self.db[self.DOCUMENTS_COLLECTION_NAME]

        try:
            num_docs = len(documents)
            request_size = calculate_request_size(documents)
            logger.debug(
                f"Adding {num_docs} documents to collection \"{self.DOCUMENTS_COLLECTION_NAME}\" (Request size: {request_size:.2f} kb)"
            )
            col.insert_many(documents)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

        for x in documents:
            del x['_id']

    def get(self, guids):
        col = self.db[self.DOCUMENTS_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        request = {self.DOCUMENTS_INDEX_FIELD_GUID: {"$in": guids}}
        request_size = calculate_request_size(request)
        logger.debug("Retrieving " + str(len(guids)) +
                     " documents from collection \"" +
                     self.DOCUMENTS_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {'_id': 0})

    def search(self, query=None, limit=100, offset=0, sortby="_id"):
        col = self.db[self.DOCUMENTS_COLLECTION_NAME]
        assert isinstance(query, dict), 'Expected dict query'
        assert isinstance(limit, int), 'Expected int limit'
        assert isinstance(offset, int), 'Expected int offset'
        assert isinstance(sortby, (str, list)), 'Expected str or list sortby'
        try:
            request = validate_json(json.dumps(query), self.schema_query)
        except DANFBaseException as e:
            error_message = 'Bad request: ' + str(e)
            logger.exception(error_message)
            return None
        request_size = calculate_request_size(request)
        logger.debug("Searching documents in collection \"" +
                     self.DOCUMENTS_COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return col.find(request, {
            '_id': 0
        }).sort(sortby).skip(offset).limit(limit)

    def update(self, guids, document_data, create_if_missing=False):
        col = self.db[self.DOCUMENTS_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        assert isinstance(document_data,
                          list), 'Expected a list of documents dicts.'
        assert all(
            isinstance(x, dict)
            for x in document_data), 'Expected a list of documents dicts.'
        assert all(
            'type' in x for x in
            document_data), 'Documents inserted into db must have type key'
        assert isinstance(create_if_missing,
                          bool), 'create_if_missing has to be of type bool'
        assert len(guids) == len(
            document_data
        ), 'List guids and list document_data should be of same length'
        for d in document_data:
            if self.DOCUMENTS_INDEX_FIELD_GUID in d.keys():
                msg = 'Overwriting index fields is not permitted!'
                logger.error(msg)
                raise IndexOverwriteException(msg)

        requests = []
        for g, d in zip(guids, document_data):
            # Add GUID to dict and create update request
            requests.append(
                UpdateOne({self.DOCUMENTS_INDEX_FIELD_GUID: g},
                          {"$set": {
                              **d, self.DOCUMENTS_INDEX_FIELD_GUID: g
                          }},
                          upsert=create_if_missing))
        try:
            logger.debug("Updating  " + str(len(guids)) +
                         " documents in collection \"" +
                         self.DOCUMENTS_COLLECTION_NAME + "\"")
            col.bulk_write(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

    def delete(self, guids):
        col = self.db[self.DOCUMENTS_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'

        num_docs = len(guids)
        if num_docs > 0:
            request = {'guid': {"$in": guids}}
            request_size = calculate_request_size(request)
            logger.debug("Deleting  " + str(num_docs) +
                         " documents from collection \"" +
                         self.DOCUMENTS_COLLECTION_NAME +
                         "\" (Request size: {:.2f}".format(request_size) +
                         " kb)")
            if num_docs <= MAXIMUM_REQUEST_BATCH_SIZE:
                col.delete_many(request)
            else:
                # batch delete
                logger.debug(
                    "Info: Number of delete operations exceeds maximum for single request."
                    + "Performing document deletion in batches of size " +
                    str(MAXIMUM_REQUEST_BATCH_SIZE))
                i = 0
                while i < num_docs:
                    start_id = i
                    i += MAXIMUM_REQUEST_BATCH_SIZE
                    end_id = i
                    if end_id >= num_docs:
                        end_id = num_docs
                    logger.debug("Info: Deleting batch objects " +
                                 str(end_id) + " of " + str(num_docs))
                    request = {'guid': {"$in": guids[start_id:end_id]}}
                    col.delete_many(request)

    def push_to_arrays_in_documents(self, guids, array_dicts, create_if_missing=False):
        col = self.db[self.DOCUMENTS_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
               for x in guids), 'Expected a list of guid strings.'
        assert isinstance(array_dicts,
                       list), 'Expected a list of documents dicts.'
        assert all(
            isinstance(x, dict)
            for x in array_dicts), 'Expected a list of documents dicts.'
        assert isinstance(create_if_missing,
                          bool), 'create_if_missing has to be of type bool'
        assert len(guids) == len(
            array_dicts
        ), 'List guids and list document_data should be of same length'
        requests = []

        for g, d in zip(guids, array_dicts):
            # Add GUID to dict and create update request
            requests.append(
                UpdateOne({self.DOCUMENTS_INDEX_FIELD_GUID: g},
                          {"$push": d},
                          upsert=create_if_missing))
        try:
            logger.debug("Updating  " + str(len(guids)) +
                         " documents in collection \"" +
                         self.DOCUMENTS_COLLECTION_NAME + "\"")
            col.bulk_write(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

    def pop_from_array_in_documents(self, guids, array_dicts):
        col = self.db[self.DOCUMENTS_COLLECTION_NAME]
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        assert isinstance(array_dicts,
                          list), 'Expected a list of documents dicts.'
        assert all(
            isinstance(x, dict)
            for x in array_dicts), 'Expected a list of documents dicts.'
        assert len(guids) == len(
            array_dicts
        ), 'List guids and list document_data should be of same length'
        requests = []

        for g, d in zip(guids, array_dicts):
            # Add GUID to dict and create update request
            requests.append(
                UpdateOne({self.DOCUMENTS_INDEX_FIELD_GUID: g},
                          {"$pop": d}))
        try:
            logger.debug("Updating  " + str(len(guids)) +
                         " documents in collection \"" +
                         self.DOCUMENTS_COLLECTION_NAME + "\"")
            col.bulk_write(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)
