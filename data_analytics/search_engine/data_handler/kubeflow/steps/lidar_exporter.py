__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from kfp import dsl
from kubernetes.client.models import V1EnvVar

from config.config_handler import config_dict
from kubeflow.steps.common import (VM_LABEL_ATTRIBUTE, RETRY_NUM,
                                   PIPELINE_POD_LABEL,
                                   BAGFILE_POD_LABEL, FILE_NAME_POD_LABEL,
                                   sanitize_label, DANF_ENV, TESTING_POD_LABEL,
                                   extract_tag_from_docker_img,
                                   GPU_RESOURCE_TITLE, GPU_VM_TITLE,
                                   attach_blobfuse_volume)
from kubeflow.steps.tracing_sidecar import TracingSidecar


class LidarExporterOp(dsl.ContainerOp):

    NAME = "lidar-exporter"
    DOCKER_IMG = config_dict.get_value(NAME)
    INPUT_DATA_BASE_PATH = "/input"
    EXPORT_DATA_OUTPUT_PATH = "/output"

    @classmethod
    def create(cls,
               bagfile_guid,
               flow_run_guid,
               bagfile_name,
               mount_id,
               storage_account,
               input_container,
               output_container,
               test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param bagfile_name: of the bagfile to process
        :param mount_id: used to generate unique mount names
        :param storage_account: storage acc. for input and output
        :param input_container: name of the blob storage container where
            the childbagfiles are located
        :param output_container: name of the blob storage container where
            exported data should be written to
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the lidar exporter operation
        """
        version = extract_tag_from_docker_img(cls.DOCKER_IMG)

        step = LidarExporterOp() \
            .set_retry(RETRY_NUM) \
            .add_pod_label(name=PIPELINE_POD_LABEL, value=sanitize_label(cls.NAME)) \
            .add_pod_label(name=BAGFILE_POD_LABEL, value=sanitize_label(bagfile_guid)) \
            .add_pod_label(name=FILE_NAME_POD_LABEL, value=sanitize_label(bagfile_name)) \
            .add_sidecar(TracingSidecar.create('main', 'exporter-lidar', version, bagfile_guid, flow_run_guid))

        if test_mode:
            step.add_pod_label(name=TESTING_POD_LABEL, value='true')

        step.add_env_variable(V1EnvVar(name="DANF_ENV", value=DANF_ENV))

        step.add_resource_request(GPU_RESOURCE_TITLE, "1") \
            .add_resource_limit(GPU_RESOURCE_TITLE, "1") \
            .add_node_selector_constraint(VM_LABEL_ATTRIBUTE, GPU_VM_TITLE)

        attach_blobfuse_volume(step,
                               cls.INPUT_DATA_BASE_PATH,
                               f"export-lidar-mount-in-{mount_id}",
                               storage_account,
                               input_container)
        attach_blobfuse_volume(step,
                               cls.EXPORT_DATA_OUTPUT_PATH,
                               f"export-lidar-mount-out-{mount_id}",
                               storage_account,
                               output_container,
                               False,
                               0)
        return step

    def __init__(self):
        super().__init__(
            name=self.NAME,
            image=self.DOCKER_IMG
        )
