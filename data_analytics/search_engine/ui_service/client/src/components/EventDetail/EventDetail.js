import React, {useState} from 'react';
import ScrollList from '../Utils/ScrollList/ScrollList';
import Form from 'react-bootstrap/Form';
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-json';
import './theme-danf'
import {timestampConverter} from '../../utils/valueConverter';
import {isObjectValid} from '../../utils/helperFunctions';
import './EventDetail.scss'

export default function EventDetail(props) {

  let eventKeys = isObjectValid(props.events) ? Object.keys(props.events) : [];

  const [selectedEventKey, setSelectedEventKey] = useState(eventKeys[0]);

  if (!isObjectValid(props.events) || eventKeys.length <= 0)
    return <div className="event-detail-empty-container">
      <div className="event-detail-empty-placeholder">There are no events for this file</div>
    </div>;

  const eventListItemComponent = (eventKey) => (
    <Form.Check
      inline type="radio" className="event-detail-event-list-item"
      id={eventKey} label={eventKey + " " + timestampConverter(props.events[eventKey].ts)} value={eventKey}
      checked={eventKey === selectedEventKey}
      onChange={() => setSelectedEventKey(eventKey)}
    />
  );

  return (
    <div className="event-detail-component">
      <div className="event-detail-event-list">
        <ScrollList
          variant="event-detail-event-list-style"
          itemVariant="borderless-item"
          bordered={false}
          listHeight={'100%'}
          showLoading={false}
          itemList={eventKeys}
          listItemComponent={eventListItemComponent}
        />
      </div>
      <div className="event-detail-divider">
        <div className="icon-arrow-right-1"/>
      </div>

      <div className="event-detail-json-container">
        <AceEditor
          mode="json"
          theme="danf"
          height="100%"
          width="100%"
          fontSize={14}
          readOnly={true}
          showPrintMargin={false}
          placeholder="Please select a event in the list to see the details"
          className="event-detail-json-ace-editor"
          value={JSON.stringify(props.events[selectedEventKey], null, 2)}
        />
      </div>
    </div>
  );
}