__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.constants import constants
from DANFUtils.exceptions import DANFBaseException
from DANFUtils.tracing import tracing
from DANFUtils.utils import validate_json

from handlers.auth import auth
from handlers.bagfile_flag_handler import BagFilesFlagHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


logger = logging.getLogger(__name__)


class BagfileFlagRequestHandler(BaseRequestHandler):

    def initialize(self):
        self.schema_guid = super().load_json_schema("schema_guid")
        self.schema_post = super().load_json_schema("schema_flag")
        self.flag_handler = BagFilesFlagHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'POST, DELETE')

    @auth.requires_entitlements(["openid"])
    def post(self, bagfile_guid):
        tracing.start_task_in_context('bagfiles/flag/post', context_headers=self.request.headers)
        logger.info("Request" + str(self.request))

        try:
            validate_json('{"guid":"' + str(bagfile_guid) + '"}', self.schema_guid)
            data = validate_json(self.request.body.decode('utf-8'),
                                 self.schema_post)
        except DANFBaseException as e:
            error_message = 'Bad request: ' + str(e)
            self._handle_error(error_message, constants["REQ_STATUS_CODE_BAD_REQ"])
            return

        flag_type = data.get("flag_type", None)

        try:
            res = self.flag_handler.add_flag(bagfile_guid, flag_type)
        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            self._handle_error(error_message, constants["REQ_STATUS_CODE_SERVER_ERROR"])
            return

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
        return

    @auth.requires_entitlements(["openid"])
    def delete(self):
        tracing.start_task_in_context('bagfiles/flag/delete', context_headers=self.request.headers)
        logger.info("Request" + str(self.request))

        try:
            flag_type = self.get_query_argument("flag_type", None)
            res = self.flag_handler.delete_flag(flag_type)
        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            self._handle_error(error_message, constants["REQ_STATUS_CODE_SERVER_ERROR"])
            return

        tracing.end_task()
        utils.build_response(response=self,
                            err=None,
                            results=res,
                            status_code=constants["REQ_STATUS_CODE_OK"])
        return

    def _handle_error(self, error_msg, status_code):
        logger.exception(error_msg)
        tracing.end_task(success=False)
        utils.build_response(self,
                             utils.build_error_from_message(error_msg),
                             None, status_code)
