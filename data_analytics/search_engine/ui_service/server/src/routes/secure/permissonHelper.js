import userWhitelist from '../../../config/admin_user_whitelist';
import storeProvider from '../../models/storeProvider';
import {DAIMLER_STRATEGY_ID, getOAuthStrategyId} from '../../loaders/oAuthLoader';
import configHandlerSingleton from "../../configHandler";

export function isUserPermitted(currentUserID) {
  return storeProvider.getUserDataAsync(currentUserID).then(userData => {
    if (configHandlerSingleton.uiIsInDevMode()) return true;
    for (let adminUser of userWhitelist.adminUsers) {
      const userMail = getOAuthStrategyId() === DAIMLER_STRATEGY_ID ? userData.email : userData.displayName;
      if (adminUser === userMail) return true;
    }
    return false;
  })
}