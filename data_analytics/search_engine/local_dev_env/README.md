# DANF Local Dev Environment
## Containers:
* MongoDB - localhost:27017
* Mongo Express - http://localhost:8081
* Metadata API - http://localhost:8887
* Search API - http://localhost:8889
* Redis Server - localhost:6379
* DANF UI - https://localhost:3001

## TODO:
- Add data handler and flow containers

## Requirements
- Setup docker and artifactory (K8s is not needed): https://athena.daimler.com/confluence/display/ATROB/How+to+deploy+DANF+on+premise
- Install docker-compose: https://docs.docker.com/compose/install/
- Make sure you don't already run a local mongodb or redis instance

## Start dev environment
To pull/start all containers at once run 
```
docker-compose up
```

(Optional: For better monitoring you might want to start the containers in individual terminals:)
```
e.g.
docker-compose up mongo
```

## Import test data to mongo db
There are 2 alternatives to import test-data
### Import test-data using MongoExpress-UI
...
### Import test-data using _init_db_-script
You can use the python script in examples/init_db.py. \
The script requires a local install of _DANF DBConnector_ \
You can run the script without parameters. Then it will install a minimum set of sample data. \
You can also do a mass-generation of bags and childbags. There are two parameters to influence this: \
_bag_count=50_ \
_include_child_bags=true_ \
This will create 50 bag-files. Each bag-file has 0..9 child-bags depending on the result of bag-index % 10. \
Please note that max_bags is limitted to 999. \
Notice that childbags are created by default. If you do not want to have them specify _include_child_bags=false_


## Run your code that uses the environment
* If you run your code locally without docker you can reference the containers of the local environment by localhost:"PORT". 
* If you run your code locally in a docker container you can reference the containers of the local environment by "NAME":"PORT". Also ensure that 
    - you add each "NAME" to the no_proxy environment variable in your container on run e.g.
        ```
        -e no_proxy=metadata_api,search_api
        ```
    - you attach your docker container on run to the docker network of the local dev environment ("test_net")
        ```
        --network=test_net
        ```
    - Example: 
        ```
        docker run -v /lhome/rbaldau/data/rca/stripped_bags:/input -e no_proxy=metadata_api,search_api -e DANF_ENV=local -e METADATA_API_BASE_URL=http://metadata_api:8887/ -e SEARCH_API_BASE_URL=http://search_api:8889/ -e BAGFILE_GUID=39a758db-7f24-4350-ae7a-d16259ecbe95 -e CHILD_BAGFILE_DIR="/input" -e APPINSIGHTS_INSTRUMENTATIONKEY=APPINSIGHTS_DISABLE_KEY -e FLOW_RUN_GUID=39a758db-7f24-4350-ae7a-d16259ecbe88 --network=test_net athena.daimler.com/dlaext_docker/dev/rca-extractor:test07
        ```

You can find the "NAME" and "PORT" values for the individual services in the "docker-compose.yml".

Use mongo-express to inspect/modify the local mongodb.

## Stop dev environment
```
docker-compose down
```
