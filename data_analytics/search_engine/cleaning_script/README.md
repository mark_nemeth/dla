# Cleaning script
### Version: see setup.py

Executes various optional cleanup tasks. Should be run as final step of a kubeflow flow.

## First steps
1. create `Personal Access Tokens` in AzureDevOps page
2. copy the token you created.
3. add to the general url like this(it looks like: https://YOUR_TOKEN@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/ )

```console
export index_url_with_access=https://<YOUR_KEY>@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```

## Build and push docker image:

```console
registry=dlaregistrydev.azurecr.io
docker build -t dlaregistrydev.azurecr.io/cleaning_script:<version> . --network=host --no-cache --build-arg https_proxy=https://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access
docker push ${registry}/cleaning_script:${version}
```

### Run in Docker:
 
Build docker image:
```console
$ docker build -t dlaregistrydev.azurecr.io/cleaning_script:<version> . --network=host --no-cache --build-arg https_proxy=https://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access
$ docker push dlaregistrydev.azurecr.io/cleaning_script:${version}
```

## Usage

The script supports execution of multiple optional tasks. Tasks are activated via command line arguments.
The base scripts as well as the individual tasks depend on environment variables for input.
Those are listed in the Environment variables section.


### Run script locally:
```console
# export required env variables
export ...

# display help
python cleanup/index.py --help

# execute delete-queue-message and set-sync-flag tasks
python cleanup/index.py --delete-queue-message --set-sync-flag
```


### Run docker image

```console
# display help
docker run --network=host -it [-e <env variables>] ${image}

# execute delete-queue-message and set-sync-flag tasks
docker run --network=host -it [-e <env variables>] ${image} --delete-queue-message --set-sync-flag
```


### Tasks

- delete-queue-message: Triggers deletion of a message from the azure storage queue via data handler
- set-sync-flag: Marks a bagfile with a sync flag
- update-processing-state: Sets the processing_state of a bagfile to the appropriate terminal state
- update-flow-run: Updates the flow run document associated with the cleanup step to indicate that the run is terminated


### Environment variables

Describes the required and optional env variables for the base script and individual tasks.

|Task|Parameter                  |Required   |Description|
|---|---|---|---|
|base script            |APPINSIGHTS_INSTRUMENTATIONKEY |yes    |see DANFTracingClient documentation|
|base script            |DANF_TRACING_TRACE_ID          |no     |see DANFTracingClient documentation|
|base script            |DANF_TRACING_SPAN_ID           |no     |see DANFTracingClient documentation|
|base script            |DANF_TRACING_TRACE_OPTIONS     |no     |see DANFTracingClient documentation|
|delete-queue-message   |EVENT_ID                       |yes    |event id of message to delete|
|delete-queue-message   |POP_RECEIPT                    |yes    |pop receipt of message to delete|
|set-sync-flag          |BAGFILE_GUID                   |yes    |bagfile for which sync flag should be set|
|update-processing-state|BAGFILE_GUID                   |yes    |bagfile for which state should be updated|
|update-flow-run        |FLOW_RUN_GUID                  |yes    |guid of flow run to update|
|update-flow-run        |POD_NAME                       |no     |kubernetes name of this pod|
|update-flow-run        |WORKFLOW_ID                    |no     |kubeflow workflow id of the current flow|
