import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import 'react-input-range/lib/css/index.css';
import './ByEngagementStateFilter.scss';


export function ByEngagementStateFilter(props) {

  function onEngagementStateSelect() {
    props.filterByEngagementState(props.selected ? 0 : 2);
  }

  function resetRangeSelection() {
    props.filterByEngagementState(0);
  }

  const active = props.selected;

  return (
    <CollapseContainer label="Engagement State"
                       active={active}
                       resetCallback={resetRangeSelection}
    >
      <div className="engagement-state-container">
        <label>
            <input type='checkbox' value={active}
             onChange={event => onEngagementStateSelect()}
             checked={active}/>
                &nbsp; system was engaged
        </label>
      </div>
    </CollapseContainer>
  );
}