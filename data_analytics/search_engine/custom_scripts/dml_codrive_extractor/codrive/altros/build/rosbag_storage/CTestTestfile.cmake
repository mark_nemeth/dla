# CMake generated Testfile for 
# Source directory: /app/altros/src/rosbag_storage
# Build directory: /app/altros/build/rosbag_storage
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_rosbag_storage_gtest_test_aes_encryptor "/app/altros/build/rosbag_storage/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/app/altros/build/rosbag_storage/test_results/rosbag_storage/gtest-test_aes_encryptor.xml" "--working-dir" "/app/altros/src/rosbag_storage/test" "--return-code" "/app/altros/devel/.private/rosbag_storage/lib/rosbag_storage/test_aes_encryptor --gtest_output=xml:/app/altros/build/rosbag_storage/test_results/rosbag_storage/gtest-test_aes_encryptor.xml")
subdirs("gtest")
