import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import {statusConverter} from '../../../../utils/valueConverter'
import './ByStatusFilter.scss';

export function ByStatusFilter(props) {

  const info = {
    'NOT_PROCESSED': 'File should be processed, but processing was not started yet',
    'BEING_PROCESSED': 'File is currently being processed',
    'SKIPPED': 'File will not be processed',
    'PROCESSING_DEACTIVATED': 'File will not be processed, because processing was manually deactivated',
    'FAILED': 'File processing was failed',
    'SUCCESSFUL': 'File was processed successfully'
  };
  const statusOptions = Object.entries(info);

  function onStatusSelect(status) {
    if (props.selectedStatus.includes(status)) props.filterByStatus(props.selectedStatus.filter(t => t !== status));
    else props.filterByStatus(props.selectedStatus.concat(status));
  }

  const listItemComponent = (item) => (
    <OverlayTrigger placement="right" delay={{show: 400, hide: 0}} outOfBoundaries overlay={
      <Tooltip id={item[0]} className="status-list-tooltip" outOfBoundaries>{item[1]}</Tooltip>
    }>
      <label className="status-list-item">
        <input type="checkbox" value={item[0]}
               onChange={event => onStatusSelect(event.target.value)}
               checked={props.selectedStatus.includes(item[0])}/>
        &nbsp; {statusConverter(item[0])}
      </label>
    </OverlayTrigger>
  );

  const subLabel = props.selectedStatus.length + ' selected';
  const active = props.selectedStatus.length > 0;

  function resetStatusList() {
    props.filterByStatus([]);
  }

  return (
    <CollapseContainer label={active > 0 ? "Status: " + subLabel : "Status"}
                       active={active}
                       variant="list"
                       resetCallback={resetStatusList}
    >
      <ScrollList
        itemList={statusOptions}
        listItemComponent={listItemComponent}
      />
    </CollapseContainer>
  );
}