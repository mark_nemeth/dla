
# coding: utf-8

# In[51]:


import rosbag
import rospy
import psycopg2
import pandas as pd
import numpy as np
import rosbag_pandas


# 1. Get data from DB and convert to pandas dataframe

# In[52]:


conn = psycopg2.connect(
    "host=localhost dbname=rca user=postgres password=postgres")
cur = conn.cursor()

cur.execute("SELECT * FROM rca_bagfilewithdropouts")
table_bagfile = cur.fetchall()

cur.execute("SELECT * FROM rca_disengagements")
table_disengagements = cur.fetchall()


# In[53]:


df_bagfile = pd.DataFrame(np.array(table_bagfile), columns=[
                          "bagfile_id", "name", "vehicle", "count_dropout", "count_disengagement"])
df_disengagements = pd.DataFrame(np.array(table_disengagements), columns=[
                                 "id", "timestamp", "cause", "detail", "bagfile_id", "distance"])

df_merged = pd.merge(df_bagfile, df_disengagements, on='bagfile_id')
# df_merged.head(5).style


# In[54]:


df_alwayson_with_disengagement = df_merged[
    df_merged["name"].str.contains("alwayson")]
# df_alwayson_with_disengagement.head(5).style


# In[55]:


selected_file = "/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/sv05/20180416_131854alwayson.bag"

# topics_all = bagfile.get_type_and_topic_info()[1].keys()
# display(topics_all)


# 2. Define functions for parsing topics

# In[56]:


def MessageToPandasVertical(bagfile, dis_timestamp, topic, variable, timestamp, value, df_dataset):
    d = {'bagfile': [str(bagfile)],
         'dis_timestamp': [str(dis_timestamp)],
         'topic': [str(topic)],
         'variable': [str(variable)],
         'timestamp': [str(timestamp)],
         'value': [str(value)]
         }
    df = pd.DataFrame(data=d)
    df_dataset = df.append(df_dataset, ignore_index=True)
    return df_dataset


# In[57]:


def BagToPandasVertical(bagfile, timestamp_list, topic_list, df_dataset, window_start_sec, window_end_sec):

    bag = rosbag.Bag(bagfile)

    for timestamp in timestamp_list:

        ts_sec = int(timestamp) / 1000000000.

        msg_list = {topic: [] for topic in topic_list}

        for topic, msg, t in bag.read_messages(topics=topics, start_time=rospy.Time(secs=ts_sec + window_start_sec), end_time=rospy.Time(ts_sec + window_end_sec)):
            msg_list[topic].append((t, msg))

        current_topic = '/CalService/instance_0/EgoVehStateInterface'
        for (t, msg) in msg_list[current_topic]:
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "velocity_x", t, msg.velocity_x, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "acceleration_x", t, msg.acceleration_x, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "acceleration_y", t, msg.acceleration_y, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "yaw_rate", t, msg.yaw_rate, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "vehicle_steer_angle", t, msg.vehicle_steer_angle, df_dataset)

        current_topic = '/CalService/instance_0/GPSInfoInterface'
        for (t, msg) in msg_list[current_topic]:
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "longitude", t, msg.longitude, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "latitude", t, msg.latitude, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "altitude", t, msg.altitude, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "heading", t, msg.heading, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "speed", t, msg.speed, df_dataset)
            df_dataset = MessageToPandasVertical(
                bagfile, timestamp, current_topic, "climb", t, msg.climb, df_dataset)

        current_topic = '/rosout_agg'
        for (t, msg) in msg_list[current_topic]:
            if msg.level > 4:
                df_dataset = MessageToPandasVertical(
                    bagfile, timestamp, current_topic, "level", t, msg.level, df_dataset)
                df_dataset = MessageToPandasVertical(
                    bagfile, timestamp, current_topic, "name", t, msg.name, df_dataset)
                df_dataset = MessageToPandasVertical(
                    bagfile, timestamp, current_topic, "msg", t, msg.msg, df_dataset)
                df_dataset = MessageToPandasVertical(
                    bagfile, timestamp, current_topic, "function", t, msg.function, df_dataset)
                df_dataset = MessageToPandasVertical(
                    bagfile, timestamp, current_topic, "topics", t, msg.topics, df_dataset)
                df_dataset = MessageToPandasVertical(
                    bagfile, timestamp, current_topic, "file", t, msg.topics, df_dataset)
    bag.close()

    return df_dataset


# In[58]:


# Define relevant topics

topics = [
    #'/PlanningTrajectoryInterface/instance_0/PlanningTrajectoryInterface',
    #'/CalService/instance_0/DsmInterface',
    '/rosout_agg',
    #'/fusion/fused_object_list',
    #'/LocalizationService/instance_0/LocDataInterface',
    #'/ShmComService/instance_0/ShmComInterface',
    '/CalService/instance_0/GPSInfoInterface',
    '/CalService/instance_0/EgoVehStateInterface'
    #'/MapsService/instance_0/GlobalMissionExecutionInterface',
    #'/CalService/instance_0/TimeOffsetInterface'
]


# 3. Produce Disengagement and Engagement Dataset

# 3.1. Disengagement Dataset

# In[59]:


# all relevant files (files with disengagements)

files_with_disengagements = df_alwayson_with_disengagement.name.unique()
len(files_with_disengagements)


# Write vertical dataframe: one row for each message and each variable

# In[60]:


df_disengagements_list_vertical = pd.DataFrame(columns=[])

for i in files_with_disengagements:

    if not str(i) == str(selected_file):
    # if "sv05" in str(i):
        continue
    print(i)

    timestamps_list = df_alwayson_with_disengagement[
        df_alwayson_with_disengagement["name"] == str(i)].timestamp

    df_dataset = pd.DataFrame(columns=[])

    df_disengagements_list_i = BagToPandasVertical(
        i, timestamps_list, topics, df_dataset, -5, 2)

    df_disengagements_list_vertical = df_disengagements_list_i.append(
        df_disengagements_list_vertical, ignore_index=True)

    df_disengagements_list_vertical.to_pickle("./disengement_list_test.pkl")


# In[ ]:


# df_disengagements_list_vertical.to_pickle("./disengement_list_vertical_test.pkl")


# In[ ]:


# df_disengagements_list_vertical.style


# In[ ]:


# pd.read_pickle("./disengement_list_vertical_sv05.pkl")


# 3.2. Engagement Dataset

# In[ ]:


# df_engagement_list = pd.read_pickle("./window.pkl")
# files_with_engagements = df_engagement_list.file.unique()


# In[ ]:

'''
df_engagements_list_vertical = pd.DataFrame(columns=[])

path = "/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/byVehicleID/hks03/"

for i in files_with_engagements:

    #if "sv05" in str(i):
    #    continue

    df_current_list = df_engagement_list[df_engagement_list["file"]== str(i)]

    df_current_list['timestamp_s_start'] = df_current_list['timestamp_s_start'].astype(str)
    df_current_list['timestamp_ns_start'] = df_current_list['timestamp_ns_start'].astype(str)

    timestamps_list = df_current_list[['timestamp_s_start', 'timestamp_ns_start']].apply(lambda x: ''.join(x), axis=1)

    i = path + str(i)
    print(i)

    df_dataset = pd.DataFrame(columns=[])

    df_engagements_list_i  = BagToPandasVertical(i, timestamps_list, topics, df_dataset, 0, 7)

    df_engagements_list_vertical = df_engagements_list_i.append(df_engagements_list_vertical, ignore_index=True)

    df_engagements_list_vertical.to_pickle("./engement_list_tmp_4.pkl")
'''

# In[ ]:


# df_engagements_list_vertical.to_pickle("./engagement_list_sv05.pkl")
