"""Regression tests for search bagfile route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import random as rd
import unittest
import os

import numpy as np
import requests
from DANFUtils.utils import generate_deterministic_guid

os.environ["DANF_ENV"] = "local"
from .testutils import get_testdb_connector

base_url = 'http://localhost:8889'
search_bag_url = base_url + '/search/bagfile'

bag_1 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "current_link": "testdata",
    "file_name": "testdata1.bag",
    "size": 21231,
    "num_messages": 4444,
    "start": 1563262908000,
    "end": 1563262911001,
    "duration": 3001,
    "vehicle_id_num": "V-123-234",
    "topics": ["topic2"],
    "drive_types": ["CID"]
}

bag_2 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "current_link": "testdata",
    "file_name": "testdata2.bag",
    "size": 42231,
    "num_messages": 4777,
    "start": 1563262925000,
    "end": 156326290833001,
    "duration": 8001,
    "vehicle_id_num": "V-123-777",
    "topics": ["topic1", "topic2", "topic3", "topic4"],
    "drive_types": ["CID", "CTD"]
}

bag_3 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "current_link": "testdata",
    "file_name": "testdata3.bag",
    "size": 33333,
    "num_messages": 5555,
    "start": 1563262942000,
    "end": 1563262948001,
    "duration": 6001,
    "vehicle_id_num": "V-123-777",
    "topics": ["topic2", "topic3"],
    "processing_state": "TEST_STATE",
    "drive_types": ["CTD"]
}

bag_4 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "file_name": "testdata4.bag",
    "size": 33333,
    "num_messages": 8888,
    "start": 1563262955000,
    "end": 1563262956001,
    "duration": 1001,
    "vehicle_id_num": "V-123-777",
    "topics": []
}


bag_5 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "current_link": "testdata",
    "file_name": "testdata5.bag",
    "size": 12546,
    "num_messages": 123456,
    "start": 1563262958000,
    "end": 1563262956001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}

bag_6 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "test/d/ata",
    "current_link": "test/b/ata",
    "file_name": "testdata5.bag",
    "size": 12546,
    "num_messages": 123456,
    "start": 1563262958000,
    "end": 1563262956001,
    #"duration": 1001, not defined to test search on undefined behavior
    "vehicle_id_num": "V-123-788",
    "file_type": 'UNKNOWN'
}



def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': 'testdata'})
        con.add_bagfiles([bag_1, bag_2, bag_3, bag_4, bag_5, bag_6])
    rd.seed(0)
    np.random.seed(0)


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': 'testdata'})
        con.db['bagfiles'].delete_many({'link': 'test/d/ata'})


class TestSearchBagfilesGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_guid(self):
        query_data = {
            'guid': bag_1['guid'],
            'limit': 1,
            'offset': 0,
            'sortby': 'guid'
        }
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'Bagfile search by guid failed')
        self.assertCountEqual([bag_1], res_json,
                              'Bagfile search by guid failed')

    def test_search_by_file_type_exist(self):
        query_data = {'file_type': 'UNKNOWN', 'file_path': 'TESTDATA'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file type failed')
        self.assertCountEqual([bag_5], res_json,
                              'Bagfile search by file type failed')

    def test_search_by_file_type_notexits(self):
        query_data = {'file_type': 'ORIGINAL', 'file_path': 'TESTDATA'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file type failed')
        self.assertNotIn([bag_5], res_json,
                              'Bagfile search by file type failed')

    def test_search_by_current_link(self):
        query_data = {'current_link': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_5], res_json,
                              'Bagfile search by file path failed')

    def test_search_by_any_link(self):
        query_data = {'any_link': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4, bag_5], res_json,
                              'Bagfile search by file path failed')

    def test_search_by_any_link_only_one(self):
        query_data = {'any_link': 'test/d/ata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.maxDiff = None
        self.assertCountEqual([bag_6], res_json,
                              'Bagfile search by file path failed')

    def test_search_by_file_path(self):
        query_data = {'file_path': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4, bag_5], res_json,
                              'Bagfile search by file path failed')

    def test_non_case_sensitive_search_by_file_path(self):
        query_data = {'file_path': 'TESTDATA'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4, bag_5], res_json,
                              'Bagfile search by file path failed')

    def test_search_by_file_name(self):
        query_data = {'file_name': 'data2.bag'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file name failed')
        self.assertCountEqual([bag_2], res_json,
                              'Bagfile search by file name failed')

    def test_non_case_sensitive_search_by_file_name(self):
        query_data = {'file_name': 'DATA2.bag'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file name failed')
        self.assertCountEqual([bag_2], res_json,
                              'Bagfile search by file name failed')

    def test_search_by_processing_state(self):
        query_data = {'processing_state': 'TEST_STATE'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by processing_state failed')
        self.assertCountEqual([bag_3], res_json,
                              'Bagfile search by processing_state failed')

    def test_search_should_include_undefined_and_defined_values_in_sort(self):
        query_data = {'sortby': 'duration'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile sortby processing_state failed')
        self.assertEqual(6, len(res_json),
                         'Not all bagfiles included in sort response')
        # bag 5 does not define duration
        self.assertDictEqual(bag_5, res_json[0],
                             'Undefined values do not appear at the beginning of sort query result')

    def test_search_by_vehicle_id_num(self):
        query_data = {'vehicle_id_num': 'V-123-777'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by vehicle_id_num failed')
        self.assertCountEqual([bag_2, bag_3, bag_4], res_json,
                              'Bagfile search by vehicle_id_num failed')

    def test_search_by_size_gte(self):
        query_data = {'size[gte]': 33333, 'file_path': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by size gte failed')
        self.assertCountEqual([bag_2, bag_3, bag_4], res_json,
                              'Bagfile search by size gte failed')

    def test_search_by_size_gte_lte(self):
        query_data = {
            'size[gte]': 21500,
            'size[lte]': 33334,
            'file_path': 'testdata'
        }
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by size gte and lte failed')
        self.assertCountEqual([bag_3, bag_4], res_json,
                              'Bagfile search by size gte and lte failed')

    def test_search_by_start_lte(self):
        query_data = {'start[lte]': 1563262925000, 'file_path': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by start lte failed')
        self.assertCountEqual([bag_1, bag_2], res_json,
                              'Bagfile search by start lte failed')

    def test_search_by_start_gte_lte(self):
        query_data = {
            'start[gte]': 1563262908000,
            'start[lte]': 1563262925000,
            'file_path': 'testdata'
        }
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by start gte and lte  failed')
        self.assertCountEqual([bag_1, bag_2], res_json,
                              'Bagfile search by start gte and lte failed')

    def test_search_by_duration_gte(self):
        query_data = {'duration[gte]': 6000, 'file_path': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by duration gte failed')
        self.assertCountEqual([bag_2, bag_3], res_json,
                              'Bagfile search by duration gte failed')

    def test_search_limit_sort(self):
        query_data = {'file_path': 'testdata', 'limit': 2, 'sortby': 'start'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        total = res.json()['total']

        self.assertEqual(5, total, 'Bagfile search limit sort result count failed.')

        self.assertEqual(200, res.status_code,
                         'Bagfile search limit sort failed')
        self.assertCountEqual([bag_1, bag_2], res_json,
                              'Bagfile search limit sort failed')

    def test_search_sort_by_size(self):
        query_data = {'file_path': 'testdata', 'sortby': '-size'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search sort by size failed')
        self.assertCountEqual([bag_2], [res_json[0]],
                              'Bagfile search sort by size failed')

    def test_search_invalid_limit(self):
        query_data = {
            'file_path': 'testdata',
            'limit': 'invalid',
            'sortby': 'start'
        }
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4, bag_5], res_json,
                              'Bagfile search by file path failed')

    def test_search_offset_descending_sort(self):
        query_data = {'file_path': 'testdata', 'offset': 1, 'sortby': '-start'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4], res_json,
                              'Bagfile search by file path failed')

    def test_search_by_empty_data(self):
        query_data = {}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by empty data failed')
        self.assertGreaterEqual(len(res_json), 4,
                                'Bagfile search by empty data failed')

    def test_search_by_multiple_vehicle_id_num(self):
        query_data = {"vehicle_id_num": ["V-123-234", "V-123-777"], 'file_path': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(
            200, res.status_code,
            'Bagfile search by multiple vehicle_id_num GET request failed')
        self.assertCountEqual(
            [bag_1, bag_2, bag_3, bag_4], res_json,
            'Bagfile search using multiple vehicle_id_num GET request failed')

    def test_search_topics_by_get_header(self):
        query_data = {"topics": ["topic2", "topic3"]}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(
            200, res.status_code,
            'Bagfile search for topics by get (header) request failed')

        # only one topic of both is in the document
        self.assertNotIn(
            bag_1, res_json,
            'Bagfile search using topics by get (header) request failed since bag_1 is in query result'
        )

        # more than both searched topics are in the document
        self.assertIn(
            bag_2, res_json,
            'Bagfile search using topics by get (header) request failed since bag_2 is not in query result'
        )

        # exactly both topics are in the document
        self.assertIn(
            bag_3, res_json,
            'Bagfile search using topics by get (header) request failed since bag_3 is not in query result'
        )

        # no topic is in the document
        self.assertNotIn(
            bag_4, res_json,
            'Bagfile search using topics by get (header) request failed since bag_4 is in query result'
        )
        cleanup_db()

    def test_search_topics_by_get_url(self):
        search_bag_url_query_topics = search_bag_url + "?topics=topic2&topics=topic3"
        res = requests.get(url=search_bag_url_query_topics)
        res_json = res.json()['results']

        self.assertEqual(
            200, res.status_code,
            'Bagfile search for topics by get (url) request failed')

        # only one topic of both is in the document
        self.assertNotIn(
            bag_1, res_json,
            'Bagfile search using topics by get (url) request failed since bag_1 is in query result'
        )

        # more than both searched topics are in the document
        self.assertIn(
            bag_2, res_json,
            'Bagfile search using topics by get (url) request failed since bag_2 is not in query result'
        )

        # exactly both topics are in the document
        self.assertIn(
            bag_3, res_json,
            'Bagfile search using topics by get (url) request failed since bag_3 is not in query result'
        )

        # no topic is in the document
        self.assertNotIn(
            bag_4, res_json,
            'Bagfile search using topics by (url) get request failed since bag_4 is in query result'
        )

    def test_search_by_single_drive_type(self):
        # Search by drive_type "CID"
        query_data = {'drive_types': ['CID'], 'link': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code, 'Bagfile search by Drive type failed')
        self.assertCountEqual([bag_1, bag_2], res_json,
                              'Bagfile search by Drive type failed')

        # Search for drive_type "CTD"
        query_data = {'drive_types': ['CTD']}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code, 'Bagfile search by Drive type failed')
        self.assertCountEqual([bag_2, bag_3], res_json,
                              'Bagfile search by Drive type failed')

    def test_search_by_multiple_drive_type_with_existingkw(self):
        # Search by both drive_types
        query_data = {'drive_types': ['CID', 'CTD'], 'link': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code, 'Bagfile search by Drive type failed')
        self.assertCountEqual([bag_1, bag_2, bag_3], res_json,
                              'Bagfile search by Drive type failed')

    def test_search_by_multiple_drive_type_with_existing_and_non_kw01(self):
        # Search with non-existent drive_type
        query_data = {'drive_types': ['CID', 'XYZ'], 'link': 'testdata'}
        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code, 'Bagfile search by Drive type failed')
        self.assertCountEqual([bag_1, bag_2], res_json,
                              'Bagfile search by Drive type failed')

    def test_search_by_multiple_drive_type_with_existing_and_non_kw02(self):
        query_data = {'drive_types': ['XYZ', 'CTD'], 'link': 'testdata'}

        res = requests.get(url=search_bag_url, params=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code, 'Bagfile search by Drive type failed')
        self.assertCountEqual([bag_2, bag_3], res_json,
                              'Bagfile search by Drive type failed')


class TestSearchBagfilesPOST(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_guid(self):
        query_data = {"guid": bag_1['guid']}
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using guid failed')
        self.assertCountEqual([bag_1], res_json,
                              'Bagfile search using guid failed')

    def test_search_by_invalid_guid(self):
        query_data = {"parent_guid": '4bed6407-a01d-424c-b031-21a4b618b55x'}
        res = requests.post(url=search_bag_url, json=query_data)

        self.assertEqual(
            400, res.status_code,
            'Bagfile search using invalid parent guid not detected')

    def test_search_by_file_path(self):
        query_data = {'link': 'testdata'}
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4, bag_5], res_json,
                              'Bagfile search using file path failed')

    def test_regex_search_by_file_path(self):
        query_data = {'link': {'$regex': 'testdata'}, 'file_path': 'testdata'}
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4, bag_5], res_json,
                              'Bagfile search using file path failed')

    def test_non_case_sensitive_regex_search_by_file_path(self):
        query_data = {'file_path': 'TESTDATA'}
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using file path failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4, bag_5], res_json,
                              'Bagfile search using file path failed')

    def test_search_by_file_name(self):
        query_data = {
            '$or': [{
                'file_name': 'testdata1.bag'
            }, {
                'file_name': 'testdata3.bag'
            }]
        }
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using file name failed')
        self.assertCountEqual([bag_1, bag_3], res_json,
                              'Bagfile search using file name failed')

    def test_regex_search_by_file_name(self):
        query_data = {
            '$or': [{
                'file_name': {
                    '$regex': 'data1.bag'
                }
            }, {
                'file_name': {
                    '$regex': 'data3.bag'
                }
            }]
        }
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using file name failed')
        self.assertCountEqual([bag_1, bag_3], res_json,
                              'Bagfile search using file name failed')

    def test_search_by_vehicle_id_num(self):
        query_data = {'vehicle_id_num': 'V-123-777'}
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using vehicle_id_num failed')
        self.assertCountEqual([bag_2, bag_3, bag_4], res_json,
                              'Bagfile search using vehicle_id_num failed')

    def test_search_by_multiple_vehicle_id_num(self):
        query_data = {
            '$or': [{
                'vehicle_id_num': 'V-123-234'
            }, {
                'vehicle_id_num': 'V-123-777'
            }],
            'file_path': 'testdata'
        }
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using vehicle_id_num failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4], res_json,
                              'Bagfile search using vehicle_id_num failed')

    def test_search_by_multiple_vehicle_id_num_in(self):
        query_data = {'vehicle_id_num': ['V-123-234', 'V-123-777'], 'file_path': 'testdata'}
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using vehicle_id_num failed')
        self.assertCountEqual([bag_1, bag_2, bag_3, bag_4], res_json,
                              'Bagfile search using vehicle_id_num failed')

    def test_search_gte(self):
        query_data = {
            'link': {
                '$regex': 'testdata'
            },
            'size': {
                '$gte': 30000
            },
            'duration': {
                '$gte': 7000
            }
        }
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using gte failed')
        self.assertCountEqual([bag_2], res_json,
                              'Bagfile search using gte failed')

    def test_search_size_gte_lte(self):
        query_data = {
            'link': {
                '$regex': 'testdata'
            },
            '$and': [{
                'size': {
                    '$gte': 21500
                }
            }, {
                'size': {
                    '$lte': 33334
                }
            }],
        }
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using gte and lte failed')
        self.assertCountEqual([bag_3, bag_4], res_json,
                              'Bagfile search using gte and lte failed')

    def test_search_lte(self):
        query_data = {
            'link': {
                '$regex': 'testdata'
            },
            'start': {
                '$lte': 1563262925000
            }
        }
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using gte failed')
        self.assertCountEqual([bag_1, bag_2], res_json,
                              'Bagfile search using gte failed')

    def test_search_or(self):
        query_data = {
            "$or": [{
                "guid": bag_1["guid"]
            }, {
                "guid": bag_2["guid"]
            }, {
                "guid": bag_3["guid"]
            }]
        }
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using or failed')
        self.assertCountEqual([bag_1, bag_2, bag_3], res_json,
                              'Bagfile search using or failed')

    def test_search_by_empty_data(self):
        query_data = {}
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code,
                         'Bagfile search using empty data failed')

        self.assertGreaterEqual(len(res_json), 4,
                                'Bagfile search using empty data failed')

    def test_search_topics_by_post(self):
        query_data = {"topics": ["topic2", "topic3"]}
        res = requests.post(url=search_bag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search for topics by post request failed')

        # only one topic of both is in the document
        self.assertNotIn(
            bag_1, res_json,
            'Bagfile search using topics by post request failed since bag_1 is in query result'
        )

        # more than both searched topics are in the document
        self.assertIn(
            bag_2, res_json,
            'Bagfile search using topics by post request failed since bag_2 is not in query result'
        )

        # exactly both topics are in the document
        self.assertIn(
            bag_3, res_json,
            'Bagfile search using topics by post request failed since bag_3 is not in query result'
        )

        # no topic is in the document
        self.assertNotIn(
            bag_4, res_json,
            'Bagfile search using topics post get request failed since bag_4 is in query result'
        )
