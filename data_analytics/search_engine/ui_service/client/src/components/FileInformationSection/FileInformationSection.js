import React from 'react';
import Card from 'react-bootstrap/Card';
import ScrollList from '../Utils/ScrollList/ScrollList';
import NameEntry from '../AttributeFormatter/Name/NameEntry';
import LabelContainer from '../Utils/LabelContainer/LabelContainer';
import SizeEntry from '../AttributeFormatter/Size/SizeEntry';
import PathEntry from '../AttributeFormatter/Path/PathEntry';
import DateTimeEntry from '../AttributeFormatter/DateTime/DateTimeEntry';
import LabelEntry from '../AttributeFormatter/Label/LabelEntry';
import TopicListEntry from '../AttributeFormatter/Topic/TopicList/TopicListEntry';
import MileDistanceEntry from '../AttributeFormatter/MileDistance/MileDistanceEntry';
import ProcessStatus from '../ProcessStatus/ProcessStatus';
import DurationEntry from '../AttributeFormatter/Duration/DurationEntry';
import {FileTypes, getFileTypeLabel} from '../Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import featureToggle from '../../config/featureToggleHelper';
import {copyToClipboard} from '../../utils/copyToClipboard';
import {isObjectValid} from '../../utils/helperFunctions';
import './FileInformationSection.scss';

const emptyPlaceholder = '-';

export function FileInformationSection(props) {

  function copyPathAction() {
    copyToClipboard(pathToLinkConvertor(props.displayedLink),
      () => props.showError({message: 'Link was copied to clipboard', type: 'primary'}),
      () => props.showError({message: 'Can not copy path to clipboard', type: 'error'}));
  }

  const childBagfileListItem = (childbagfile) => (
    <div className={'childbagfile-list-item ' +
    (childbagfile.guid === props.selectedChildbagfileGuid ? 'childbagfile-list-item-active' : '')}>
      <NameEntry guid={childbagfile.guid} fileType={FileTypes.CHILDBAGFILE} fileName={childbagfile.file_name}/>
    </div>
  );

  function pathToLinkConvertor(path){
    let filePath = path.replace(/^\W+/,'');
    let filePathParts = filePath.split('/');
    let storageAccount = filePathParts[0];
    let container = filePathParts[1];
    let blob  = filePathParts.slice(2).join('/');
    let link =  "https://"+storageAccount+".blob.core.windows.net/"+container+'/'+blob;
    return link
  }

  return (
    <Card className="file-information-section">
      <span className="file-information-section-label">
        {getFileTypeLabel(props.fileType)} Information
      </span>
      {props.overviewFile && <>
        <LabelContainer label="Name" variant="card-style-filled">
          <NameEntry guid={props.overviewFile.guid} fileName={props.overviewFile.file_name} withoutLink disableTooltip/>
        </LabelContainer>
        {props.fileType !== FileTypes.DRIVE &&
        <LabelContainer label="Blob Link" actionText="Copy" variant="card-style-filled"
                        actions={[{name: 'Copy', event: copyPathAction}]}>
          <PathEntry filePath={pathToLinkConvertor(props.displayedLink)}/>
        </LabelContainer>}
        <div className="information-row">
          <LabelContainer label="VID" variant="fifty-percent-card card-style-filled">
            <span>{props.overviewFile.vehicle_id_num || emptyPlaceholder}</span>
          </LabelContainer>
          <LabelContainer label="Origin" variant="fifty-percent-card card-style-filled">
            <span>{props.overviewFile.origin || emptyPlaceholder}</span>
          </LabelContainer>
        </div>
        <div className="information-row">
          <LabelContainer label="Start time" variant="seventy-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.overviewFile.start}/>
          </LabelContainer>
          {props.fileType !== FileTypes.DRIVE &&
          <LabelContainer label="Size" variant="thirty-percent-card card-style-filled">
            <SizeEntry size={props.overviewFile.size}/>
          </LabelContainer>}
        </div>
        <LabelContainer label="Duration" variant="fourths-card card-style-filled">
          <DurationEntry duration={props.overviewFile.duration}/>
        </LabelContainer>
        {props.overviewFile.file_type === "ALWAYSON" && props.overviewFile.custom_attributes && props.overviewFile.custom_attributes.feature_extraction
        && props.overviewFile.custom_attributes.feature_extraction.version &&
        [
          <div className="information-row" key="TOTAL_ENGAGED_DISTANCE">

            <LabelContainer label="Total engaged distance" variant="thirty-percent-card card-style-filled">
              <MileDistanceEntry
                distance_in_meter={props.overviewFile.custom_attributes.feature_extraction.total_engaged_distance}/>
            </LabelContainer>
            <LabelContainer label="Total engaged distance on public roads"
                            variant="fifty-percent-card card-style-filled">
              <MileDistanceEntry
                distance_in_meter={props.overviewFile.custom_attributes.feature_extraction.engaged_distance_in_public}/>
            </LabelContainer>
          </div>,
          <div className="information-row" key="ERROR_DISENGAGEMENTS">
            <LabelContainer label="Error disengagements" variant="thirty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.feature_extraction.number_of_error_disengagements_total}/>
            </LabelContainer>
            <LabelContainer label="Error disengagements on public roads" variant="fifty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.feature_extraction.number_of_public_error_disengagements_total}/>
            </LabelContainer>
          </div>,
          <div className="information-row" key="MANUAL_UNPLANNED_DISENGAGEMENTS">
            <LabelContainer label="Manual unplanned disengagements" variant="thirty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.feature_extraction.number_of_manual_unplanned_disengagements_total}/>
            </LabelContainer>
            <LabelContainer label="Manual unplanned disengagements on public roads"
                            variant="fifty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.feature_extraction.number_of_public_manual_unplanned_disengagements_total}/>
            </LabelContainer>
          </div>,
          <div className="information-row" key="MANUAL_PLANNED_DISENGAGEMENTS">
            <LabelContainer label="Manual planned disengagements" variant="thirty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.feature_extraction.number_of_planned_disengagements_total}/>
            </LabelContainer>
            <LabelContainer label="Manual planned disengagements on public roads"
                            variant="fifty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.feature_extraction.number_of_public_planned_disengagements_total}/>
            </LabelContainer>
          </div>
        ]
        }
        {props.overviewFile.file_type === "ALWAYSON" && props.overviewFile.custom_attributes && props.overviewFile.custom_attributes.disengagement_report
        && props.overviewFile.custom_attributes.disengagement_report.schema_version && props.overviewFile.custom_attributes.disengagement_report.schema_version.startsWith("1.") &&
        !props.overviewFile.custom_attributes.feature_extraction.version &&
        [
          <div className="information-row">
            <LabelContainer label="Manual unplanned and error disengagements"
                            variant="fifty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.disengagement_report.number_of_error_disengagements}/>
            </LabelContainer>
            <LabelContainer label="Total engaged distance on private and public roads"
                            variant="fifty-percent-card card-style-filled">
              <MileDistanceEntry
                distance_in_meter={props.overviewFile.custom_attributes.disengagement_report.total_engaged_distance}/>
            </LabelContainer>
          </div>,
          <div className="information-row">
            <LabelContainer label="Manual unplanned and error disengagements on public roads"
                            variant="fifty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.disengagement_report.number_of_public_error_disengagements}/>
            </LabelContainer>
            <LabelContainer label="Total engaged distance on public roads"
                            variant="fifty-percent-card card-style-filled">
              <MileDistanceEntry
                distance_in_meter={props.overviewFile.custom_attributes.disengagement_report.engaged_distance_in_public}/>
            </LabelContainer>
          </div>
        ]
        }
        {props.overviewFile.file_type === "ALWAYSON" &&
        props.overviewFile.custom_attributes &&
        props.overviewFile.custom_attributes.disengagement_report &&
        props.overviewFile.custom_attributes.disengagement_report.schema_version &&
        props.overviewFile.custom_attributes.disengagement_report.schema_version.startsWith("2.") &&
        props.overviewFile.custom_attributes.feature_extraction &&
        !props.overviewFile.custom_attributes.feature_extraction.version &&
        [
          <div className="information-row" key="TOTAL_ENGAGED_DISTANCE">

            <LabelContainer label="Total engaged distance" variant="thirty-percent-card card-style-filled">
              <MileDistanceEntry
                distance_in_meter={props.overviewFile.custom_attributes.disengagement_report.total_engaged_distance}/>
            </LabelContainer>
            <LabelContainer label="Total engaged distance on public roads"
                            variant="fifty-percent-card card-style-filled">
              <MileDistanceEntry
                distance_in_meter={props.overviewFile.custom_attributes.disengagement_report.engaged_distance_in_public}/>
            </LabelContainer>
          </div>,
          <div className="information-row" key="ERROR_DISENGAGEMENTS">
            <LabelContainer label="Error disengagements" variant="thirty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.disengagement_report.number_of_error_disengagements}/>
            </LabelContainer>
            <LabelContainer label="Error disengagements on public roads" variant="fifty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.disengagement_report.number_of_public_error_disengagements}/>
            </LabelContainer>
          </div>,
          <div className="information-row" key="MANUAL_UNPLANNED_DISENGAGEMENTS">
            <LabelContainer label="Manual unplanned disengagements" variant="thirty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.disengagement_report.number_of_manual_unplanned_disengagements}/>
            </LabelContainer>
            <LabelContainer label="Manual unplanned disengagements on public roads"
                            variant="fifty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.disengagement_report.number_of_public_manual_unplanned_disengagements}/>
            </LabelContainer>
          </div>,
          <div className="information-row" key="MANUAL_PLANNED_DISENGAGEMENTS">
            <LabelContainer label="Manual planned disengagements" variant="thirty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.disengagement_report.number_of_total_disengagements -
                props.overviewFile.custom_attributes.disengagement_report.number_of_manual_unplanned_disengagements -
                props.overviewFile.custom_attributes.disengagement_report.number_of_error_disengagements}/>
            </LabelContainer>
            <LabelContainer label="Manual planned disengagements on public roads"
                            variant="fifty-percent-card card-style-filled">
              <LabelEntry
                value={props.overviewFile.custom_attributes.disengagement_report.number_of_public_total_disengagements -
                props.overviewFile.custom_attributes.disengagement_report.number_of_public_manual_unplanned_disengagements -
                props.overviewFile.custom_attributes.disengagement_report.number_of_public_error_disengagements}/>
            </LabelContainer>
          </div>
        ]
        }
        {props.fileType === FileTypes.CHILDBAGFILE && props.overviewFile.custom_attributes && props.overviewFile.custom_attributes.rca &&
        <LabelContainer label="Root Cause Analysis" variant="fourths-card card-style-filled">
          <LabelEntry value={props.overviewFile.custom_attributes.rca.rca_short_text} variant="card-style-filled"/>
        </LabelContainer>
        }
        {featureToggle.showTagInformation &&
        <LabelContainer label="Tags" variant="card-style-filled">
          <span>PLACEHOLDER TAGS</span>
        </LabelContainer>}
        {props.fileType === FileTypes.CHILDBAGFILE &&
        <LabelContainer label="Parent Original File" variant="card-style-filled">
          <NameEntry guid={props.parent.guid} fileType={FileTypes.ORIGINALFILE} fileName={props.parent.file_name}/>
        </LabelContainer>
        }
        {(props.fileType === FileTypes.ORIGINALFILE || props.fileType === FileTypes.ALWAYSONFILE) && <>
          <LabelContainer label="Status" variant="card-style-filled-list">
            <ProcessStatus state={isObjectValid(props.flowRuns) ? props.flowRuns : props.overviewFile.state}/>
          </LabelContainer>
          <LabelContainer label={`${props.childbagfiles.length} snippets available`}
                          variant="card-style-filled-list">
            <ScrollList
                showLoading={false}
                listHeight={props.childbagfiles.length === 0 ? '28px' : (props.childbagfiles.length < 5 ? '150px' : '200px')}
                itemList={props.childbagfiles}
                listItemComponent={childBagfileListItem}
                itemVariant={"childbagfile-list-item-container"}
            />
          </LabelContainer>
        </>}
        {props.fileType !== FileTypes.DRIVE &&
        <LabelContainer label="Topics" variant="card-style-filled-list">
          <TopicListEntry topics={props.overviewFile.topics}/>
        </LabelContainer>}
      </>}
    </Card>
  )
}