export const mapStyleSimple = `
sources:
  omv:
    type: OMV
    max_zoom: 17
    min_display_zoom: 1
layers:
  water_areas:
    data: { source: omv, layer: water }
    draw:
      polygons:
        order: 1
        color: [0.055, 0.604, 0.914, 1.00]
`
