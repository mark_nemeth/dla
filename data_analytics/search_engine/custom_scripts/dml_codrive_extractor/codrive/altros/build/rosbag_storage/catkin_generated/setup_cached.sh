#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/app/altros/devel/.private/rosbag_storage:$CMAKE_PREFIX_PATH"
export PWD="/app/altros/build/rosbag_storage"
export ROSLISP_PACKAGE_DIRECTORIES="/app/altros/devel/.private/rosbag_storage/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/app/altros/src/rosbag_storage:$ROS_PACKAGE_PATH"