__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import hashlib
import json
import logging
import os
import subprocess
from pathlib import Path

import numpy as np
from ContainerClient.container_client import ContainerClient
from DANFUtils.constants import ROOT_FILE_GUID
from DANFUtils.exceptions import RESTException
from DANFUtils.utils import utils as danfutils

from config.constants import *
from extractors.utils import set_origin, timestamp2ms, get_storage_account
from model.bagfile import BagfilePatch

logger = logging.getLogger(__name__)


class MetadataManager:

    def __init__(self):
        self.client = ContainerClient()
        self.bagfile_metadata = None
        self.bagfile_valid = True

    def check_if_bagfile_is_valid(self, file_path, guid):
        """Check if the bagfile is valid. If it is unindexed, set indexed the
        indexed field in the bagfile metadata to false and create an .unindexed
        file. If the bagfile check fails due to another error (e.g. corrupted,
        header, invalid path), log the error and raise an ValueError.
        """
        process = subprocess.run(
            ["dla_converter/dla_converter", "--check-bag-file", file_path],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)

        if process.returncode == 0:
            return

        # bagfile check returned non 0 status code
        logger.error(f"stderr from 'dla_converter --check-bag-file': "
                     f"{process.stderr}")
        self.bagfile_valid = False

        if "Error: Invalid index position" in str(process.stderr):
            logger.warning("Bagfile is unindexed. Exiting execution.")
            # Update 'indexed' field in the DB
            self.client.update_bagfile(guid, {"indexed": False})

        raise ValueError("Invalid Bagfile! Exiting execution with FAILED!")

    def load_bagfile_data(self, bagfile_path, guid):
        """Reads the metadata of the bagfile located at the specified path
        and generates and stores a BagfilePatch object from it.

        :param bagfile_path: path to the bagfile
        """
        link = bagfile_path
        if guid:
            metadata_from_api = self.get_bagfile_metadata_from_api(guid)
            logger.info(f"The metadata of bagfile from api is {metadata_from_api}")
            link = metadata_from_api.get('link', bagfile_path)
        storage_account = get_storage_account(link)

        bagfile_metadata = self._read_bagfile_metadata(bagfile_path)
        logger.info(f"The metadata of bagfile from bag_file_path is {bagfile_metadata}")
        self.bagfile_metadata = BagfilePatch(
            size=bagfile_metadata.get("size", 0),
            num_messages=bagfile_metadata.get("message_count", 0),
            start=timestamp2ms(bagfile_metadata.get("start", 0)),
            end=timestamp2ms(bagfile_metadata.get("end", 0)),
            duration=int(bagfile_metadata.get("duration", 0)/1e6),
            vehicle_id_num=bagfile_metadata.get("vehicle_id", "unknown"),
            topics=bagfile_metadata.get("topics", []),
            origin=set_origin(storage_account)
        )

    def update_bagfile_metadata(self, guid):
        """Updates the bagfile with the given guid with the internally stored
        BagfilePatch object.

        :param guid: guid of the bagfile to update
        """
        logger.info(f"Updating metadata for bagfile {guid} "
                    f"with data {self.bagfile_metadata.dict()}")
        self.client.update_bagfile(guid, self.bagfile_metadata.dict())
        logger.info(f"Successfully updated metadata of bagfile {guid}")

    def get_bagfile_metadata_from_api(self, guid):
        """Retrieves and returns the metadata of the bagfile with the given
        guid as dict.

        :param guid: guid of the bagfile to retrieve
        :return: bagfile metadata as dict
        """
        bagfile_metadata = self.client.get_bagfile(guid)
        logger.info("Bagfile metadata could be fetched from the API.")
        return bagfile_metadata

    def submit_state_updates(self,
                             step,
                             bagfile_guid,
                             flow_run_guid,
                             status,
                             started_at,
                             message=None):
        """Updates metadata in the flow/bagfile state APIs with information
        about the execution of this step.

        :param step: Name of the step that is currently executed
        :param bagfile_guid: bagfile that is processed
        :param flow_run_guid: id of currently executed flow
        :param status: SUCCESSFUL or FAILED to indicate whether this processing
        :param started_at: start time of the processing step
        :param message: optional message to attach
        """
        run_update, state_update = self._build_state_updates(
            step, status, started_at, message)

        self.client.add_flow_run_step_run(flow_run_guid,
                                          step,
                                          run_update)

        # update legacy state API
        self.client.add_bagfile_state_event(bagfile_guid,
                                            state_update)

    @staticmethod
    def _build_state_updates(
            state_name, status, started_at, message=None):
        map_status_to_legacy_status = {
            'SUCCESSFUL': 'SUCCESS',
            'FAILED': 'FAILED'
        }

        now = danfutils.get_timestamp()
        report = {}
        if message:
            report['message'] = message

        flow_update = {
            'started_at': started_at,
            'completed_at': now,
            'status': status,
            'report': report
        }
        state_update = {
            'name': state_name,
            'version': VERSION,
            'time': now,
            'status': map_status_to_legacy_status[status],
            'report': report
        }
        return flow_update, state_update

    def submit_extracted_metadata(self, guid, extracted_events):
        """Submit extracted events of all extractors to the metadata API
        """
        if len(extracted_events) == 0:
            logger.info("No extraction request is inserted since the list of extracted events is empty!")
            return

        length_event_list = len(extracted_events)
        logger.info(f"length of the extracted event list: {length_event_list}")
        try:
            # split extraction request if the extracted list is longer
            # than MAX_LENGTH_EXTRACTED_EVENT_LIST
            if length_event_list > MAX_LENGTH_EXTRACTED_EVENT_LIST:
                num_batches = (length_event_list // MAX_LENGTH_EXTRACTED_EVENT_LIST) + 1
                logger.info(f"Number of extracted events is"
                            f"higher than {MAX_LENGTH_EXTRACTED_EVENT_LIST}, extraction "
                            f"request will be splited into {num_batches} pieces")
                splitted_event_list = np.array_split(extracted_events, num_batches)
                for counter, i in enumerate(splitted_event_list, 1):
                    self._submit_metadata_batch(i.tolist(), counter, num_batches)
            else:
                # fewer extracted events as limit, submit all at once
                self._submit_metadata_batch(extracted_events, 1, 1)

        except Exception as e:
            raise RuntimeError(f"Error submitting extracted metadata "
                               f"for bagfile {guid}") from e

    @staticmethod
    def _read_bagfile_metadata(file_path):
        """Reads and returns the info metadata of a bagfile using the
        dla_converter.

        :param file_path: bagfile location
        :return: info metadata as dict
        """
        try:
            subprocess.check_call([
                "dla_converter/dla_converter",
                "--export-info",
                file_path,
                "info.json"
            ])
            with open("info.json", "r") as f:
                bagfile_metadata = json.load(f)
            return bagfile_metadata
        except Exception as e:
            raise RuntimeError(f"Error opening bagfile using dla_converter. "
                               f"metadata cannot be fetched from the header! "
                               f"Error: {e}") from e

    def _submit_metadata_batch(self, extracted_events, counter, num_batches):
        """Submit a batch of extracted metadata of a single extractor

        :param extracted_events: a list of extracted events to submit
        :param counter: nr. of batch to submit
        :param num_batches: total number of batches
        :return: None
        """
        logger.info(f"[{counter}/{num_batches}] extraction_request = "
                    f"{self._truncate_string(extracted_events)}")
        guids = self.client.submit_metadata(
            [evt.dict() for evt in extracted_events])
        logger.info(f"[{counter}/{num_batches}] Extraction "
                    f"request is inserted into DB with guids = {guids}")

    @staticmethod
    def _truncate_string(string):
        full_msg = str(string)
        return full_msg \
            if len(full_msg) <= 5000 \
            else full_msg[:5000] + " ... ..."

    @staticmethod
    def _generate_file_in_output(file_name):
        if "OUTPUT_FOLDER" in os.environ:
            output_dir = Path(os.environ["OUTPUT_FOLDER"])
            if not output_dir.exists():
                logger.info(f"creating output directory {output_dir}")
                output_dir.mkdir(parents=True, exist_ok=True)
            Path(output_dir, file_name).touch()
            logger.info(f"wrote file {file_name} in output")
        else:
            logger.warning("missing env variable 'OUTPUT_FOLDER")

    def submit_bagfile_to_file_catalog(self, guid, full_output_file_path):
        try:
            path = Path(full_output_file_path)
            metadata = self.get_bagfile_metadata_from_api(guid)
            stat = path.stat()
            creation_time = int(stat.st_ctime * 1000)
            file_size = stat.st_size
            file_guid = danfutils.extract_file_guid(full_output_file_path)
            self.client.metadata_handler.add_file_to_catalog(file_guid, file_size, ".bag", str(ROOT_FILE_GUID))
            link = metadata["link"].replace("/mapr/", "hdfs://", 1)
            self.client.metadata_handler.add_accessor_url_to_catalog(file_guid, link, creation_time, "DANF")
        except (KeyError, OSError, RESTException):
            pass
