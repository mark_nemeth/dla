import dash
import flask
from flask_restx import Api
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from urllib.parse import urlparse, urlsplit, parse_qsl, parse_qs
import numpy as np
import pandas as pd
import logging

# mapbox_access_token = 'pk.eyJ1IjoiYWJoaXJ1cGtncCIsImEiOiJjazBwc3BpMzcwMTdsM2htbHJndTFtb204In0.sSm1F8rW1cm_0LNtGxIKrg'
# mapbox_access_token =  'pk.eyJ1IjoicGxvdGx5bWFwYm94IiwiYSI6ImNqdnBvNDMyaTAxYzkzeW5ubWdpZ2VjbmMifQ.TXcBE-xg9BFdV2ocecc_7g'
import plotly.graph_objects as go
import plotly.express as px

from backend import filter_geom_features, filter_lsq_features, filter_turn_ind, filter_dyn_obj_intersections, \
    filter_left_turn, filter_right_turn, filter_lane_change, filter_steering_angle, \
    filter_dyn_objects_rel_pos, filter_dyn_objects_rel_vel, filter_map_objects, filter_lane_changes_map, \
    filter_dyn_obj_tracks_from_trackid
from map import map_layout
from layout_browser import create_browser_layout
from layout_search import create_search_layout
from backend_search import search_traj, generate_table, search_dyn, search_proximity
from data import ego_tracks_all_files, geom_features_all_files, map_objects, dyn_intersections, tracks_df_intersections
from api_backend import api as scene_api

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = 'Athena'

# Get underlying flask app and initialize API
server = app.server
api = Api(server, doc='/docs',
          title='Scenario Identification API')
api.add_namespace(scene_api)

# Layout of Dash App
# app.layout = layout

app.layout = html.Div(
    [dcc.Location(id="url", refresh=True), html.Div(id="page-content")]
)
app.config.suppress_callback_exceptions = True

color_pallet = []
color_pallet = color_pallet + px.colors.qualitative.Prism+px.colors.qualitative.Safe+px.colors.qualitative.Bold

@app.callback(Output("page-content", "children"), 
              [
                  Input("url", "href"),
              ])
def display_page(href):
    url_res = urlparse(href)
    query = parse_qs(url_res.query)
    if url_res.path=='/search':
        return create_search_layout(app, query)
    if url_res.path=='/':
        return create_browser_layout()

@app.callback(
    Output("map-graph", "figure"),
    [
        Input('submit-button', 'n_clicks'),
        Input('submit-lsq', 'n_clicks'),
        Input('submit-sa', 'n_clicks'),
        Input('cb-ego', 'value'),
        Input('cb-curvature', 'value'),
        Input('cb-lsq', 'value'),
        Input('cb-turnind', 'value'),
#         Input('cb-cw', 'value'),
        Input('dyn-object-selector', 'value'),
        Input('proximity-dyn-obj-type-selector', 'value'),
        Input('proximity-dyn-obj-to-map-obj', 'value'),
        Input('map-object-selector', 'value'),
        Input('proximity-type-selector', 'value'),
        Input('comparitive-map-object-selector', 'value'),
        Input("cb-right-turn", "value"),
        Input("cb-left-turn", "value"),
        Input("cb-lane_change", "value"),
        Input("cb-lane_change-map", "value"),
        Input("cb-sa", "value"),
        Input("cb-heatmap", "value"),
        Input("cb-tracks", "value"),
        Input('map-graph', 'selectedData'),
    ],
    [
        State("curvature-cutoff", "value"),
        State("lsq-cutoff", "value"),
        State("sa-cutoff", "value"),
#         State('rp-cutoff-X', 'value'),
#         State('rp-cutoff-Y', 'value'),
#         State('rv-cutoff-X', 'value'),
#         State('rv-cutoff-Y', 'value'),
        State('prox-dyn-cutoff', 'value'),
        State('prox-cutoff', 'value'),
   ]
)
def update_graph(nclicks, nclicks_lsq,
                 nclicks_sa,
                 cb_ego,
                 cb_curvature, cb_lsq, cb_turnind,
#                  cb_cw,
                 dyn_obj_selected,
                 prox_dyn_obj_type,
                 prox_dyn_map_obj,
                 map_obj_selected,
                 prox_type_selected,
                 comp_map_obj_selected,
                 right_turn,
                 left_turn,
                 lane_change,
                 lane_change_map,
                 cb_sa,
                 cb_heatmap,
                 cb_tracks,
                 map_selected_data,
                 curvature_cutoff,
                 lsq_cutoff,
                 sa_cutoff,
#                  rp_cutoff_x,
#                  rp_cutoff_y,
#                  rv_cutoff_x,
#                  rv_cutoff_y,
                 prox_dyn_cutoff,
                 prox_cutoff,
):
    fig_data = []
    if cb_ego:
        fig_base_layer_data = go.Scattermapbox(
                lat=ego_tracks_all_files['Lat'],
                lon=ego_tracks_all_files['Lon'],
                text=list(zip(ego_tracks_all_files['Filename'], ego_tracks_all_files['T'])),
                mode='markers',
                marker=go.scattermapbox.Marker(
                    size=2,
                    color='blue',
                    ),
                hoverinfo="lat+lon+text",
                name="Base"
            )
        fig_data.append(fig_base_layer_data)
    # collect filtered data based on callback states
    if curvature_cutoff:
        geom_features_to_plot = filter_geom_features(geom_features_all_files, coef2_cutoff=float(curvature_cutoff))
        fig_geom_layer_data = go.Scattermapbox(
            lat=geom_features_to_plot['Lat'],
            lon=geom_features_to_plot['Lon'],
            text=geom_features_to_plot['turn_coef'], # list(zip(geom_features_to_plot['Filename'], geom_features_to_plot['T'])),
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=3,
                color='red',
                ),
            hoverinfo="text",
            name="Geom"
        )
        if cb_curvature:
            fig_data.append(fig_geom_layer_data)
        
        turn_ind_to_plot = filter_turn_ind(geom_features_all_files)
        fig_turnind_layer_data = go.Scattermapbox(
            lat=turn_ind_to_plot['Lat'],
            lon=turn_ind_to_plot['Lon'],
            text=list(zip(turn_ind_to_plot['Filename'], turn_ind_to_plot['T'])), # turn_ind_to_plot['Turn_Ind'], 
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=3,
                color='rgb(218,165,27)',
                ),
            hoverinfo="text",
            name="turn_ind"
        )
        if cb_turnind:
            fig_data.append(fig_turnind_layer_data)
        
    if lsq_cutoff:
        lsq_to_plot = filter_lsq_features(geom_features_all_files, coef2_lsq_cutoff=float(lsq_cutoff))
        fig_lsq_layer_data = go.Scattermapbox(
            lat=lsq_to_plot['Lat'],
            lon=lsq_to_plot['Lon'],
            text=list(zip(lsq_to_plot['Filename'], lsq_to_plot['T'])),#lsq_to_plot['turn_coef'] ,
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=3,
                color='purple',
                ),
            hoverinfo="text",
            name="lsq"
        )
        if cb_lsq:
            fig_data.append(fig_lsq_layer_data)
        
    if dyn_obj_selected:
        if prox_dyn_map_obj:
            dynamic_object_intersections = filter_dyn_obj_intersections(str(dyn_obj_selected), 
                                                                        str(prox_dyn_map_obj), 
                                                                        str(prox_dyn_obj_type),
                                                                        float(prox_dyn_cutoff))
        else:
            dynamic_object_intersections = filter_dyn_obj_intersections(str(dyn_obj_selected))
        fig_data_dyn_obj_layer = go.Scattermapbox(
            lat=dynamic_object_intersections['Y'],
            lon=dynamic_object_intersections['X'],
            text=list(zip(dynamic_object_intersections['Filename'], 
                          dynamic_object_intersections['T'],
                          dynamic_object_intersections['Track_id'])),
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=3,
                color='rgb(0,134,149)',
            ),
            hoverinfo='text',
            name='dynamicObject'
        )
        fig_data.append(fig_data_dyn_obj_layer)
        if cb_heatmap:
            fig_density_layer = go.Densitymapbox(
                lat=dynamic_object_intersections['Y'],
                lon=dynamic_object_intersections['X'],
                text='',
                opacity=0.5,
                radius=15,
                colorscale='Jet',
                hoverinfo='text',
                showscale=False,
                name='heatmap'
                )
            fig_data.append(fig_density_layer)
            
        if cb_tracks:
            if None not in (map_selected_data, dyn_obj_selected):
                num_points = len(map_selected_data['points'])
                if num_points < 20:
                    for dyn_point_selected in map_selected_data['points']:
                        dyn_point_track_id = dyn_point_selected['text'].split(',')[2].strip('\'( )\'')
                        dyn_point_fname = dyn_point_selected['text'].split(',')[0].strip('\'( )\'')
                        dyn_obj_selected_track = filter_dyn_obj_tracks_from_trackid(dyn_point_fname, 
                                                                                    int(float(dyn_point_track_id)))
                        fig_data_dyn_obj_tracks_layer = go.Scattermapbox(
                            lat=dyn_obj_selected_track['Y'],
                            lon=dyn_obj_selected_track['X'],
                            text=list(zip(dyn_obj_selected_track['Filename'], dyn_obj_selected_track['T'])),
                            mode='markers',
                            marker=go.scattermapbox.Marker(
                                size=3,
                                color='rgb(207,28,144)',
                            ),
                            hoverinfo='text',
                            name='dynamicObject'
                        )
                        fig_data.append(fig_data_dyn_obj_tracks_layer)
#        dynamic_object_tracks = filter_dyn_objects(dyn_obj_selected) #Removing tracks for now, tracks would become an on demand feature.
#         if rp_cutoff_x or rp_cutoff_y:
#             dynamic_object_tracks = filter_dyn_objects_rel_pos(dynamic_object_tracks,
#                                                                float(rp_cutoff_x), float(rp_cutoff_y))
#         if rv_cutoff_x or rv_cutoff_y:
#             dynamic_object_tracks = filter_dyn_objects_rel_vel(dynamic_object_tracks,
#                                                                float(rv_cutoff_x), float(rv_cutoff_y))
#         fig_data_dyn_obj_layer = go.Scattermapbox(
#             lat=dynamic_object_tracks['Y'],
#             lon=dynamic_object_tracks['X'],
#             text=list(zip(dynamic_object_tracks['Filename'], dynamic_object_tracks['T'])),
#             mode='markers',
#             marker=go.scattermapbox.Marker(
#                 size=3,
#                 color='rgb(0,134,149)',
#             ),
#             hoverinfo='text',
#             name='dynamicObject'
#         )
#         fig_data.append(fig_data_dyn_obj_layer)

    if map_obj_selected:
        mo_ct = 0
        for _map_obj in map_obj_selected:
            map_objects_filtered = filter_map_objects(_map_obj, comp_map_obj_selected, prox_type_selected, prox_cutoff)
            fig_data_map_obj_layer = go.Scattermapbox(
                lat=map_objects_filtered['Lat'],
                lon=map_objects_filtered['Lon'],
                text=list(map_objects_filtered['object_type']),
                mode='markers',
                marker=go.scattermapbox.Marker(
                    size=3,
                    color=color_pallet[mo_ct],
                ),
                hoverinfo='text',
                name=str(_map_obj)
            )
            fig_data.append(fig_data_map_obj_layer)
            mo_ct += 1
        
    if right_turn:
        right_turn_to_plot = filter_right_turn(geom_features_all_files)
        fig_rightturn_layer_data = go.Scattermapbox(
            lat=right_turn_to_plot['Lat'],
            lon=right_turn_to_plot['Lon'],
            text=list(zip(right_turn_to_plot['Filename'], right_turn_to_plot['T'])),
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=4,
                color='rgb(204,102,119)',
                ),
            hoverinfo="text",
            name="right_turn"
        )
        fig_data.append(fig_rightturn_layer_data)

    if left_turn:
        left_turn_to_plot = filter_left_turn(geom_features_all_files)
        fig_leftturn_layer_data = go.Scattermapbox(
            lat=left_turn_to_plot['Lat'],
            lon=left_turn_to_plot['Lon'],
            text=list(zip(left_turn_to_plot['Filename'], left_turn_to_plot['T'])),
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=3,
                color='rgb(57,105,172)',
                ),
            hoverinfo="text",
            name="left_turn"
        )
        fig_data.append(fig_leftturn_layer_data)

    if lane_change:
        lane_change_to_plot = filter_lane_change(geom_features_all_files)
        fig_lanechange_layer_data = go.Scattermapbox(
            lat=lane_change_to_plot['Lat'],
            lon=lane_change_to_plot['Lon'],
            text=list(zip(lane_change_to_plot['Filename'], lane_change_to_plot['T'])),
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=3,
                color='rgb(82,188,163)',
                ),
            hoverinfo="text",
            name="lane_change"
        )
        fig_data.append(fig_lanechange_layer_data)
        
    if lane_change_map:
        lane_change_map_to_plot = filter_lane_changes_map(geom_features_all_files)
        fig_lanechange_layer_data = go.Scattermapbox(
            lat=lane_change_map_to_plot['Lat'],
            lon=lane_change_map_to_plot['Lon'],
            text=list(zip(lane_change_map_to_plot['Filename'], lane_change_map_to_plot['T'])),
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=3,
                color='rgb(225,124,5)',
                ),
            hoverinfo="text",
            name="lane_change_map"
        )
        fig_data.append(fig_lanechange_layer_data)

    if sa_cutoff:
        sa_to_plot = filter_steering_angle(geom_features_all_files, steer_cutoff=float(sa_cutoff))
        fig_sa_layer_data = go.Scattermapbox(
            lat=sa_to_plot['Lat'],
            lon=sa_to_plot['Lon'],
            text=list(zip(sa_to_plot['Filename'], sa_to_plot['T'])), #sa_to_plot['Steer_Angle'],
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=3,
                color='rgb(160, 97, 119)',
                ),
            hoverinfo="text",
            name="steer_angle"
        )
        if cb_sa:
            fig_data.append(fig_sa_layer_data)

#     if cb_cw:
#         fig_data_crosswalk_layer = go.Scattermapbox(
#             lat=crosswalks_lon_lat['Lat'],
#             lon=crosswalks_lon_lat['Lon'],
#             text='CrossWalk',
#             mode='markers',
#             marker=go.scattermapbox.Marker(
#                 size=4,
#                 color='rgb(153, 153, 153)'
#             ),
#             hoverinfo='text',
#             name='CrossWalk'
#         )
#         fig_data.append(fig_data_crosswalk_layer)

    return go.Figure(
        data = fig_data,
        layout = map_layout
    )

@app.callback(
    [
        Output("selected-data", "children"),
        Output('search-fname', 'value'),
        Output('search-ts', 'value')
    ], [
        Input('map-graph', 'selectedData'),
    ]
)
def update_file_address(map_selected_data):
    if map_selected_data:
        num_points = len(map_selected_data['points'])
        file_and_time = map_selected_data['points'][num_points//2]['text'].split(',')
        fileName, timeStamp = [_.strip('\'( )\'') for _ in file_and_time[:2]] # only fname and ts
        strout = "File: {} || timestamp: {:.2f}".format(fileName, float(timeStamp))
    else:
        strout = None
        fileName = None
        timeStamp = None
    return strout, fileName, timeStamp

@app.callback(
    [
        Output('search-results', 'children'),
    ], [
        Input('search-button', 'n_clicks'),
    ], [
        State('file-selector', 'value'),
        State('timestamp-input', 'value'),
        State('ego-feature-selector', 'value'),
        State('map-feature-selector', 'value'),
        State('obj-feature-selector', 'value'),
    ]
)
def invoke_search(n_clicks, fname, ts, ego_features=None, map_features=None, obj_features=None):
    if n_clicks > 0:
        search_res = []
        search_res_df = pd.DataFrame()
        if ego_features:
            search_res_traj = search_traj(fname, int(int(ts)/10), relevant_cols = ego_features)
            search_res_df = search_res_traj
        if obj_features:
            search_res_obj = search_dyn(fname, int(int(ts)/10), relevant_cols=obj_features)
            search_res_df = search_res_df.append(search_res_obj)
        if map_features:
            search_res_map = search_proximity(fname, int(int(ts)/10), relevant_cols=map_features)
            search_res_df = search_res_df.append(search_res_map)
        return [generate_table(search_res_df)]
    else:
        return [html.Div()]
    
    

if __name__ == '__main__':
    app.run_server(host='0.0.0.0',debug=False)
    # for some weird reason, debug mode fails when I try to load a pandas data frame.
