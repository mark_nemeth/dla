"""Regression tests for cache size duration time route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from tests.regressiontests.testutils import get_testdb_connector

base_url = 'http://localhost:8887'
cache_bagfile_size_duration_time_url = base_url + '/cache/size-duration-time'


TEST_DATA_LINK = 'testdata'

raw_bagfile_1 = {
    'version': '1.0',
    'guid': generate_deterministic_guid(),
    'link': TEST_DATA_LINK,
    'size': 10000,
    'start': 1400000000000,
    'end': 1400000500000,
    'duration': 500000,
}

raw_bagfile_2 = {
    'version': '1.0',
    'guid': generate_deterministic_guid(),
    'link': TEST_DATA_LINK,
    'size': 25000,
    'start': 1400000000010,
    'end': 1400000000011,
    'duration': 1,
}


def prepare_db():
    with get_testdb_connector() as con:
        con.db['bagfiles'].delete_many({'link': TEST_DATA_LINK})
        con.db['documents'].delete_many({'type': {'$regex': '.*_cache'}})
        con.add_bagfiles([raw_bagfile_1, raw_bagfile_2])


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['bagfiles'].delete_many({'link': TEST_DATA_LINK})
        con.db['documents'].delete_many({'type': {'$regex': '.*_cache'}})


class TestBagfileCacheSizeDurationPOST(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()
    
    def test_post_cache_document_should_successfully_create(self):
        res = requests.post(url=cache_bagfile_size_duration_time_url)
        self.assertEqual(200, res.status_code, 'post request is failed')

    def test_post_cache_document_should_correctly_compute_size_cache(self):
        requests.post(url=cache_bagfile_size_duration_time_url)
        with get_testdb_connector() as con:
            cache = con.search_documents(query={'type': 'bagfile_size_cache'})[0]
        self.assertEqual(raw_bagfile_1['size'], cache['min'])
        self.assertEqual(raw_bagfile_2['size'], cache['max'])

    def test_post_cache_document_should_correctly_compute_duration_cache(self):
        requests.post(url=cache_bagfile_size_duration_time_url)
        with get_testdb_connector() as con:
            cache = con.search_documents(query={'type': 'bagfile_duration_cache'})[0]
        self.assertEqual(raw_bagfile_2['duration'], cache['min'])
        self.assertEqual(raw_bagfile_1['duration'], cache['max'])

    def test_post_cache_document_should_correctly_compute_time_cache(self):
        requests.post(url=cache_bagfile_size_duration_time_url)
        with get_testdb_connector() as con:
            cache = con.search_documents(query={'type': 'bagfile_time_cache'})[0]
        self.assertEqual(raw_bagfile_1['start'], cache['min'])
        self.assertEqual(raw_bagfile_2['start'], cache['max'])


if __name__ == '__main__':
    unittest.main()
