__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse
import contextlib
import os
import sys
import unittest
from io import StringIO

import coverage
from DANFUtils.logging import logger

#TODO DIRTY CODE, maybe find better possibility to make PYTHONPATH
tests_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
tests_path = os.path.abspath(os.path.join(tests_path, '..'))
sys.path.append(tests_path)  # add .../cleaning_script to path


# Globals
NAME_OF_TEST = "cluster_services/periodic_tasks"
logger.set_log_level('debug')


@contextlib.contextmanager
def stdout_redirect(where):
    sys.stdout = where
    try:
        yield where
    finally:
        sys.stdout = sys.__stdout__


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--cov',
                        action='store_true',
                        required=False,
                        help='Calculate coverage of executed tests.')
    parser.add_argument('--html',
                        action='store_true',
                        required=False,
                        help='Generate a html report for calculated coverage. (must be used in combination with --cov)'
                        )
    parser.add_argument('--txt',
                        action='store_true',
                        required=False,
                        help='Generate a txt report in folder unittests for calcualted covergae. (must be used in combination with --cov)'
                        )

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()

    cov = coverage.Coverage()
    include = list([f'*/search_engine/{NAME_OF_TEST}/periodictasks/*'])
    exclude = list(['*/*_unittest.py', '*/config/*'])

    dir_path = os.path.dirname(os.path.realpath(__file__))
    suite = unittest.TestLoader().discover(str(dir_path),
                                           pattern="*_unittest.py")

    with stdout_redirect(StringIO()) as new_stdout:
        if args.cov:
            cov.start()

        print("RUNNING UNITTESTS FOR " + NAME_OF_TEST)
        print("--------------------------------------------------------------------------")
        result = unittest.TextTestRunner(sys.stdout, verbosity=2).run(suite)

        if args.cov:
            cov.stop()
            cov.save()
            print("\nTEST COVERAGE FOR " + NAME_OF_TEST)
            print("----------------------------")
            cov.report(include=include, omit=exclude)

    new_stdout.seek(0)
    res = new_stdout.read()
    logger.info(res)

    if args.cov and args.html:
        cov.html_report(
            directory=os.path.dirname(__file__) + "/COVERAGE_HTML_REPORT",
            include=include,
            omit=exclude)

    if args.cov and args.txt:
        coverage_report = open(dir_path + '/COVERAGE_REPORT.txt', 'w')
        coverage_report.write(res)
        coverage_report.close()

    if not (result.wasSuccessful()):
        exit(1)
