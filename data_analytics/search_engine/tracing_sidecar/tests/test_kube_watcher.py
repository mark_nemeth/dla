from unittest import mock

from kubernetes.client import V1Pod, V1PodStatus, V1ContainerStatus, V1ContainerState

from TracingSidecar.kube_watcher import ContainerStateMachine, ContainerState

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from test.support import EnvironmentVarGuard

CONTAINER_NAME = 'container'


class TestContainerStateMachine(unittest.TestCase):

    def setUp(self):
        self.env = EnvironmentVarGuard()
        self.env.set('APPINSIGHTS_INSTRUMENTATIONKEY', 'APPINSIGHTS_DISABLE_KEY')

    def test_call_when_next_state_is_entered_should_invoke_callback(self):
        instance = ContainerStateMachine(CONTAINER_NAME)
        instance.state = ContainerState.WAITING
        mock_on_running = mock.Mock()
        instance.register_callback_on_state_change(ContainerState.RUNNING, mock_on_running)

        pod = self._create_pod_object(ContainerState.RUNNING)
        instance(pod)
        mock_on_running.assert_called_once()

    def test_call_when_gap_in_events_should_execute_callback_for_intermediate_states(self):
        instance = ContainerStateMachine(CONTAINER_NAME)
        mock_on_wait = mock.Mock()
        mock_on_running = mock.Mock()
        instance.register_callback_on_state_change(ContainerState.WAITING, mock_on_wait)
        instance.register_callback_on_state_change(ContainerState.RUNNING, mock_on_running)

        pod = self._create_pod_object(ContainerState.RUNNING)
        instance(pod)

        mock_on_wait.assert_called_once()
        mock_on_running.assert_called_once()

    def test_call_when_state_does_not_change_should_not_execute_callback(self):
        instance = ContainerStateMachine(CONTAINER_NAME)
        mock_callback = mock.Mock()
        instance.register_callback_on_state_change(ContainerState.RUNNING, mock_callback)

        pod1 = self._create_pod_object(ContainerState.RUNNING)
        instance(pod1)
        mock_callback.assert_called_once()

        pod2 = self._create_pod_object(ContainerState.RUNNING)
        instance(pod2)
        mock_callback.assert_called_once()  # second time no new call to mock

    def test_when_state_is_not_terminated_should_return_false(self):
        instance = ContainerStateMachine(CONTAINER_NAME)
        pod = self._create_pod_object(ContainerState.RUNNING)

        ret = instance(pod)

        self.assertFalse(ret)

    def test_when_state_is_terminated_should_return_true(self):
        instance = ContainerStateMachine(CONTAINER_NAME)
        pod = self._create_pod_object(ContainerState.TERMINATED)

        ret = instance(pod)

        self.assertTrue(ret)

    def _create_pod_object(self, container_state=ContainerState.WAITING):
        if container_state is ContainerState.WAITING:
            container_state = V1ContainerState(waiting='waiting')
        elif container_state is ContainerState.RUNNING:
            container_state = V1ContainerState(running='running')
        elif container_state is ContainerState.TERMINATED:
            container_state = V1ContainerState(terminated='terminated')

        container_status = V1ContainerStatus(name=CONTAINER_NAME,
                                             state=container_state,
                                             image='image',
                                             image_id='image_id',
                                             ready='ready',
                                             restart_count=0)

        pod_status = V1PodStatus(container_statuses=[container_status])

        pod = V1Pod(status=pod_status)
        return pod
