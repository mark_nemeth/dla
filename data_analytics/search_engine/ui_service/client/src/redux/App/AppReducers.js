import {createReducer} from '@reduxjs/toolkit';
import * as actions from './AppActions';

export const AppReducer = createReducer({}, {
  [actions.showError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload.message;
    state.errorMessageType = action.payload.type;
  },
  [actions.hideError]: (state, action) => {
    state.error = false;
    state.errorMessageType = null;
  },
  [actions.fetchUserDataSuccess]: (state, action) => {
    state.user = action.payload;
  },
  [actions.fetchUserDataError]: (state, action) => {
    state.user = null;
  },
  [actions.fetchVersionsSuccess]: (state, action) => {
    state.versions = action.payload
  },
  [actions.fetchVersionsError]: (state, action) => {
    state.versions = {
      uiVersion: 'unknown',
      danfVersion: 'unknown'
    }
  },
  [actions.fetchFeatureTogglesSuccess]: (state, action) => {
    state.featureToggles = action.payload
  },
  [actions.fetchFeatureTogglesError]: (state, action) => {
    state.featureToggles = {
      isDriveViewActive: false,
      isIngestDashboardActive: false
    }
  }
});