__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

SPLITTER_NAME = "splitter"
STRIPPER_NAME = "stripper"

VERSION = "0.3.57"
DANF_VERSION_TAG = "WILL_BE_TAKEN_FROM_GIT_TAG"

TOPIC_PROFILES_MOUNT_PATH = "/topic_profiles/bva_stripping_profiles.json"

STRIPPER_PROFILES = [
    "debug_io",
    "planning_io",
    "planning_io_no_camera",
    "fusion_io",
    "fusion_io_no_camera",
    "cid_bageval",
    "localization_and_map_creation_io",
    "localization_and_map_creation_io_no_camera",
    "sem_io",
    "radar_input_eval",
    "hmi_aduino_input",
    "hmi_visu_input",
    "phoenix_io",
]
STRIPPER_PROFILES_APPLY_TIME_CORRECTION = [
    "debug_io",
    "fusion_io",
    "fusion_io_no_camera",
]

OUTPUT_PREFIX = ""

PROFILE_JSON_PATH = "config/bva_stripping_profiles.json"

FILENAME_STR_LOGBOOK = "_EXTRACTEDBY_LogbookIssueInterface_at_"

FILENAME_STR_GUEST_LOGBOOK = "_EXTRACTEDBY_GuestLogbookIssueInterface_at_"

PROFILE_EXPIRATION_DAYS = {
    "planning_io_no_camera": 42,
    "fusion_io_no_camera": 42,
    "localization_and_map_creation_io_no_camera": 42,
    "planning_io": 42,
    "fusion_io": 42,
    "localization_and_map_creation_io": 42,
    "hmi_aduino_input": 42,
    "hmi_visu_input": 42,
}

APPLY_TAGS = {
    "transfer-eu_to_us_allowed": [
        "planning_io_no_camera",
        "fusion_io_no_camera",
        "localization_and_map_creation_io_no_camera",
    ]
}
