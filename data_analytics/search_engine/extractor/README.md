# Extractor

## Description
An Extractor is the component in DANF which extracts useful metadata from bagfiles (or other data sources in the future) and insert those metadata into the Cosmos DB using the Container Client. It is dockerized and  launched by the Data Handler in the Kubernetes cluster (see also DANF - High Level Component Architecture for more details).

In order to add metadata of your own interest to the database, a new Extractor can be created and added to the DANF pipeline. 

The extractor is highly customizable, there is basically no limitation on how to retrieve metadata from the original data source, but it must contain the following 3 features in order to be integrated into DANF:

  - The extracted metadata must be transformed into certain JSON format according to the DB Entity Model
  - It utilizes the DANF's **Container Client** to send POST request to the metadata API
  - It must be dockerized and accept certain environment variables as input, and an input folder containing bagfiles must be mounted

## Prerequisites
* The extractor uses the rosbag API to parse .bag files. Before installing the rosbag package, make sure that ROS is installed and the following line is added to the .bashrc:
```console
source /opt/ros/melodic/setup.bash
```
* MapR cluster has been mounted / bagfile is available
```console
sudo it4ad-loopbacknfs-client start
```

* Install dependencies
```console
python3 -m pip install . --extra-index-url=$EXTRA_INDEX_URL
```
As for how to get the `$EXTRA_INDEX_URL`, please refer to the following section: `How to get EXTRA_INDEX_URL` depending if you have access to Athena Artifactory or AzureDevOps

#### Install dependencies from Athena Artifactory:
1. Login to https://athena.daimler.com/artifactory/webapp/#/home
2. Click our docker repo "dla_docker" and input your password, and then it generates the auth
2. copy the token you created.
3. add to the general url like this(it looks like: https://YOUR_TOKEN@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/ )

```console
export EXTRA_INDEX_URL=https://<YOUR_KEY>@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```
#### Install dependencies from Azure DevOps:
1. create `Personal Access Tokens` in AzureDevOps page
2. copy the token you created.
3. add to the general url like this(it looks like: https://YOUR_TOKEN@pkgs.dev.azure.com/daimler/_packaging/ATTDATA/pypi/simple/ )

```console
export EXTRA_INDEX_URL=https://your_token@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```

## Usage
### Run locally:
#### Ensure that Metadata API is available
 - dev: "METADATA_API_BASE_URL":"http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/",
 - prod: "METADATA_API_BASE_URL":"http://danf-metadata-api-prod.westeurope.cloudapp.azure.com:8887/",

#### Run the script

1.Set environment variables BAGFILE_PATH and BAGFILE_GUID, e.g.:
```console
export BAGFILE_PATH=/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A411952/2019/05/27/20190527_150227_5214_104-12_CTD_Mission-3_3_ORIGINAL_STRIPPED_cid_bageval.bag
export BAGFILE_GUID=509ede01-1071-415a-a63f-31669fb8b984
```
2.Disable Tracing and ContainerClient Authorization for local test:
```console
export APPINSIGHTS_INSTRUMENTATIONKEY=APPINSIGHTS_DISABLE_KEY
export AUTH=none
```

3.Run the extractor:
```console
python entry.py --update_metadata --extractors disengagement silent_testing gps --add_state_event
```

### Run in Docker:
Build docker image:
```console
docker build -t dlaregistrydev.azurecr.io/extractor:<version> . --network=host --no-cache --build-arg https_proxy=https://localhost:3128 --build-arg EXTRA_INDEX_URL=$index_url_with_access
```
then run it:
```console
docker run -it -v /mapr/738.mbc.de/data/input/rd/athena/08_robotaxi:/input -e BAGFILE_PATH=/input/davos/WDF44781313121789/2019/04/08/20190408_145104_alwayson.bag -e BAGFILE_GUID=509ede01-1071-415a-a63f-31669fb8b984 dlaregistry.azurecr.io/extractor:${latest_version}
```
* with the option -v the /mapr folder is mapped to the /input folder inside the docker
* with BAGFILE_PATH the environment variables are set

If the docker is run locally, Metadata API and SSH tunnel must be set up, as described in the "Run locally" section, and the environment variables SEARCH_API_BASE_URL METADATA_API_BASE_URL must be set to the ip address of the local machine, e.g.:
```console
docker run -it -v /mapr/738.mbc.de/data/input/rd/athena/08_robotaxi:/input -e BAGFILE_PATH=/input/plog/WDD2221591A411952/2019/05/27/20190527_150227_5214_104-12_CTD_Mission-3_3_ORIGINAL_STRIPPED_cid_bageval.bag -e BAGFILE_GUID=509ede01-1071-415a-a63f-31669fb8b984 -e APPINSIGHTS_INSTRUMENTATIONKEY=APPINSIGHTS_DISABLE_KEY --network host dlaregistrydev.azurecr.io/extractor:${latest_version} --update_metadata --extractors disengagement silent_testing gps --add_state_event
```

## Reference
more information about version, usage and related components pls seee
1. [DANF' HOW TO](https://athena.daimler.com/confluence/display/ATROB/DANF+How-To%27s)
2. [How to add your own Extractor to DANF](https://athena.daimler.com/confluence/display/ATROB/How+to+add+your+own+Extractor+to+DANF)
3. [DANF components and version](https://athena.daimler.com/confluence/display/ATROB/DANF+release+notes)

