# -*- coding: utf-8 -*-
__copyright__ = '''
COPYRIGHT: (c) 2017-2018 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

"""Module provides a set of function to perform windowing operations on a pyspark dataframe."""

from pyspark.sql.types import StringType, BooleanType, LongType, ArrayType
from pyspark.sql.functions import udf, col, desc, explode
import pyspark.sql.functions as F
from pyspark.sql.window import Window
import datetime

from dlalib.definitions import ADEngagementState


def is_in_windows_udf(windows):
    """Return function object for filtering rows based on time windows.

    Function object allows to filter all rows of a dataframe. Condition: timestamp
    of row is in specified windows and filenames match. The returned function object f(t, f)
    expects timestamp t and filename f as arguments.

    Note: There is a faster (and redundant) version of this function available (is_in_windows).

    Args:
       windows -- list of windows [(filename1, start1, stop1), (filename2, start2, stop2), ...]

    Return:
        function object
    """

    def _is_in_windows(t, f, windows):
        for filename, ts_l, ts_h in windows:
            if (f == filename) and (ts_l <= t <= ts_h):
                return True
            return False

    return udf(lambda t, f: _is_in_windows(t, f, windows), BooleanType())


def is_in_windows(df, windows):
    """Filter rows within specified time windows and with a matching filename.

    Args:
        df -- pyspark dataframe
        windows -- list of windows [(filename1, start1, stop1), (filename2, start2, stop2), ...]

    Return:
        Pyspark dataframe
    """
    for idx, (f, ts_l, ts_h) in enumerate(windows):
        if idx == 0:
            result = df.where(
                (col("ts") <= ts_h) &
                (col("ts") >= ts_l)
            )
        else:
            result = result.union(
                df.where(
                    (col("ts") <= ts_h) &
                    (col("ts") >= ts_l)
                )
            )
    return result.distinct()


def label_windows_udf(windows, label_format='list'):
    """Return function object for labeling rows based on time windows.

    Function object allows to label all rows of a dataframe. Condition: timestamp
    of row is in specified windows and filenames match. The returned function object f(t, f)
    expects timestamp t and filename f as arguments. The window are labeled sequentially
    with integers (0,1,2, ...).

    Args:
        windows -- list of windows [(filename1, start1, stop1), (filename2, start2, stop2), ...]
        label_format -- Specifies if the labels are organized in a list of int64 ('list') or
                        in a comma-separated string ('string')

    Returns:
        function object
    """
    if label_format not in ("list", "string"):
        raise InvalidArgument

    if label_format == 'list':
        def _label_windows(t, f, windows):
            label = []
            for idx, (filename, ts_l, ts_h) in enumerate(windows):
                if (f == filename) and (ts_l <= t <= ts_h):
                    label.append(idx)
            return label

        return udf(lambda t, f: _label_windows(t, f, windows), ArrayType(LongType()))
    else:
        def _label_windows(t, f, windows):
            label = ""
            for idx, (filename, ts_l, ts_h) in enumerate(windows):
                if (f == filename) and (ts_l <= t <= ts_h):
                    label += str(idx) + ","
            return label

        return udf(lambda t, f: _label_windows(t, f, windows), StringType())


def explode_window_labels(df, label_column):
    """Explode a list of int64 window labels stored in the specified label_column into separate rows.

    Args:
        df -- pyspark dataframe with a column matching the value of label_column,
              containing a list of int64 window labels
        label_column -- A string representing the name of the column containing a list of window labels

    Returns:
        pyspark dataframe with exploded window labels
    """
    return df.withColumn(label_column, explode(df[label_column]))


def create_windows(filenames, ts, window_length_ms, direction="symmetrical"):
    """Return list of windows for filtering.

    Args:
        filenames     -- list of filenames
        ts            -- list of reference timestamps
        window_length_ms -- length of window in ms
        direction     -- specifies if reference timestamp is at beginning (right),
                         end (left) or in center of window (symmetrical)

    Returns:
        list of windows [(filename1, start1, stop1), (filename2, start2, stop2), ...]
    """
    # TODO add time span after reference timestamps to parameter list (flexibility)
    if direction not in ("symmetrical", "right", "left"):
        raise InvalidArgument
    if len(filenames) != len(ts):
        raise InvalidArgument

    window_length = datetime.timedelta(milliseconds=window_length_ms)
    windows = []
    for filename, t in zip(filenames, ts):
        if direction == "symmetrical":
            window_length = window_length / 2
            ts_l = t - window_length
            ts_h = t + window_length
        elif direction == "right":
            ts_l = t
            ts_h = t + window_length
        elif direction == "left":
            ts_l = t - window_length
            ts_h = t
        windows.append((filename, ts_l, ts_h))
    return windows


def engagements_ego(df):
    """Return pyspark dataframes with information about timestamps of engagements.

    Args:
        df -- pyspark dataframe with required columns:
           ts: timestamps of instance
           ad_engagement_state: engagement state of cal (levels: 0, 1, 2, 3)
           filename: filename of instance

    Returns:
        pyspark dataframe with columns:
            ts : timestamps of engagements
            filename : filename of corresponding instance  
    """
    if not all((ele in df.columns) for ele in ("ts", "ad_engagement_state", "filename")):
        raise MissingColumn
    df_engagements = rows_for_value_change(df, "ad_engagement_state", [ADEngagementState.initializing, \
                                                                       ADEngagementState.ready,
                                                                       ADEngagementState.error],
                                           [ADEngagementState.engaged])
    df_engagements = df_engagements.select(col("ts"), col("filename"))
    return df_engagements


def disengagments_ego(df):
    """Return pyspark dataframe with information about timestamps of disengagments.

    Args:
        df -- pyspark dataframe with required columns:
            ts : timestamps of instance
            ad_engagement_state : engagement state of cal (levels: 0, 1, 2, 3)
            filename : filename of instance

    Returns:
        pyspark dataframe with columns:
            ts : timestamps of disengagements
            filename : filename of corresponding instance  
            cause : reason for disengagment (ad_engagement_state after disengagement)
    """
    if not all((ele in df.columns) for ele in ("ts", "ad_engagement_state", "filename")):
        raise MissingColumn
    df_disengagements = rows_for_value_change(df, "ad_engagement_state", [ADEngagementState.engaged], \
                                              [ADEngagementState.initializing, ADEngagementState.ready,
                                               ADEngagementState.error])
    df_disengagements = df_disengagements.select(col("ts"), col("ad_engagement_state").alias("cause"), col("filename"))
    return df_disengagements


def rows_for_topic_value(df, column_name, values, ts_file_only=False):
    """Return all rows of given data frame for which parameter column_name matches values.
    
    Args:
        df -- spark dataframe
        column_name -- relevant column name
        values -- list of values
        ts_file_only -- specify if all columns or just timestamps are returned

    Returns:
        Pyspark dataframe
    """
    return df.where(col(column_name).isin(values)).select(col("ts"), col("filename")) if ts_file_only \
        else df.where(col(column_name).isin(values))


def rows_for_value_change(df, column_name, values_init, values_final, ts_file_only=False):
    """Return all rows of given data frame for which value of column_name changes from values_init to values_final.

    Args:
         df -- spark dataframe
         column_name -- relevant column name
         values_init -- list of initial values
         values_final -- list of final values
         ts_file_only -- return timestamp and filename only

    Returns:
        Pyspark dataframe
    """
    window = Window.partitionBy("filename").orderBy("ts")
    df = df.withColumn("prev", F.lag(col(column_name)).over(window))
    df = (df.where(col("prev").isin(values_init))
        .where(col(column_name).isin(values_final))
        .drop("prev"))
    return df.select(col("ts"), col("filename")) if ts_file_only \
        else df


def extract_windows_from_intervals(intervals, window_length_ms, overlap_ms, skip_last_ms=0, reverse_extraction=False):
    """Extract overlapping windows from list of intervals.

    Args:
        intervals -- list of intervals [(filename1, start1, end1), (filename2, start2, end2), ...]
        window_length_ms -- windows length in ms
        overlap_ms -- overlap in in ms
        skip_last_ms -- defines time span at the end of the interval which is skipped
        reverse_extraction -- Perform the window extraction reversed (from end to start timestamp)

    Returns:
        List of overlapping windows [(filename1, start1, end1), (filename2, start2, end2), ...]
    """
    windows = []
    for inv in intervals:
        windows.extend(
            extract_windows_from_interval(inv, window_length_ms, overlap_ms, skip_last_ms, reverse_extraction))
    return windows


def extract_windows_from_interval(interval, window_length_ms, overlap_ms, skip_last_ms=0, reverse_extraction=False,
                                  add_interval_to_output=False):
    """Extract sliding windows from the given time interval with the specified overlap.

    Args:
        interval -- tuple(filename, start, end)
        window_length -- length of the windows that should be extracted in ms
        overlap -- overlap between the windows in ms
        skip_last_ms -- defines how many ms should be ignored at the end of the interval
        reverse_extraction -- Perform the window extraction reversed (from end to start timestamp)
        add_interval_to_output -- add interval timestamps to resulting window tuples

    Returns:
        list of timestamp tuples (filename, start, end) representing the found windows
    """
    if overlap_ms > window_length_ms:
        raise InvalidWindowLength
    f, w_start_orig, w_end_orig = interval
    # convert timestamps
    w_len = datetime.timedelta(milliseconds=window_length_ms)
    w_overlap = datetime.timedelta(milliseconds=overlap_ms)
    w_skip_last_ms = datetime.timedelta(milliseconds=skip_last_ms)
    # extract matching windows
    w_timestamps = []
    w_start = w_start_orig
    w_end = w_end_orig - w_skip_last_ms
    if reverse_extraction:
        while w_end - w_len >= w_start:
            if add_interval_to_output:
                w_timestamps.append((w_start_orig, w_end_orig - w_skip_last_ms, f, w_end - w_len, w_end))
            else:
                w_timestamps.append((f, w_end - w_len, w_end))
            w_end = w_end - w_len + w_overlap
    else:
        while w_start + w_len <= w_end:
            if add_interval_to_output:
                w_timestamps.append((w_start_orig, w_end_orig - w_skip_last_ms, f, w_start, w_start + w_len))
            else:
                w_timestamps.append((f, w_start, w_start + w_len))
            w_start = w_start + w_len - w_overlap
    return w_timestamps


def _delete_windows(windows, f):
    """Delete all windows for which the filename does not match f."""
    return [w for w in windows if w[0] != f]


def _last_row(df, column_name, filename, values):
    """Return row with latest timestamp for given filename and machting values. """
    return (df.where(col(column_name).isin(values))
        .where(col("filename") == filename)
        .sort(desc("ts"))
        .limit(1))


def extract_intervals(df, column_name, values_outside_before, values_inside, values_outside_after):
    """Extract list of time interval defined by transitions of column column_name from values_outside_before to values_inside to values_outside_after.

    Assumptions:
        - number of leading and falling edge transitions are equal
        - in case the last message of a recording is within a window this last message is assumed to be the end of the window
        - The start of an interval is indicated by a transition of column column_name from values_outside_before to values_inside
        - The end of an interval is indicated by a transition of column column_name from values_inside to values_outside_after

    Args:
        df -- pyspark dataframe
        column_name -- relevant column name for filtering
        values_outside_before -- list of values which are outside and before the interval
        values_inside -- list of values which are inside the interval
        values_outside_after -- list of values which are outside and after the interval

    Returns:
        List of intervals [(filename1, start1, end1), (filename2, start2, end2), ...]
    """
    if not all((ele in df.columns) for ele in ("ts", "filename", column_name)):
        raise MissingColumn
    rising_edge = rows_for_value_change(df, column_name, values_outside_before, values_inside,
                                        ts_file_only=True).toPandas()
    falling_edge = rows_for_value_change(df, column_name, values_inside, values_outside_after,
                                         ts_file_only=True).toPandas()
    filenames = (rising_edge.sort_values("filename")["filename"].unique())
    windows = []
    for f in filenames:
        rising_edge_sub = rising_edge[rising_edge["filename"] == f].sort_values("ts")
        falling_edge_sub = falling_edge[falling_edge["filename"] == f].sort_values("ts")
        try:
            # consider special case: falling edge of last window of a file is missing
            if len(rising_edge_sub) == len(falling_edge_sub) + 1:
                last_row = _last_row(df, column_name, f, values_inside)
                falling_edge_sub = falling_edge_sub.append(last_row.toPandas())
            # for each rising edge there should be a falling edge
            if len(rising_edge_sub) != len(falling_edge_sub):
                raise InvalidIntervals
            for ts_l, ts_h in zip(rising_edge_sub["ts"], falling_edge_sub["ts"]):
                if ts_l >= ts_h:
                    windows = _delete_windows(windows, f)
                    raise InvalidIntervals
                windows.append((f, ts_l.to_pydatetime(), ts_h.to_pydatetime()))
        except InvalidIntervals:
            print("Structure of intervals of file {} is invalid.".format(f))
    return windows


class Error(Exception):
    """Base-class for all exception raised by this module."""


class InvalidIntervals(Error):
    """There was a problem with the provided rising and falling edges defining the intervals."""


class InvalidWindowLength(Error):
    """Length of window is invalid."""


class MissingColumn(Error):
    """Required column in dataframe is missingMiss."""


class InvalidArgument(Error):
    """Parameter of a function has an invalid value."""
