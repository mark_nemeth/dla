"""Unittest for bagfiles handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from collections import namedtuple
from test.support import EnvironmentVarGuard
from unittest.mock import MagicMock

from periodictasks import index
from periodictasks.tasks import registry


Args = namedtuple(
    'Args',
    'tasks'
)


class TestIndex(unittest.TestCase):

    def setUp(self):
        self.env = EnvironmentVarGuard()
        self.env.set('APPINSIGHTS_INSTRUMENTATIONKEY', 'APPINSIGHTS_DISABLE_KEY')
        for key in registry:
            registry[key] = MagicMock(return_value=None)

    def test_should_only_execute_specified_tasks(self):
        first_task = list(registry.keys())[0]
        args = Args(tasks=[first_task])

        index.main(args)

        registry[first_task].assert_called_once()
        for task in registry:
            if not task == first_task:
                registry[task].assert_not_called()

    def test_should_continue_processing_if_task_fails(self):
        all_tasks = list(registry.keys())
        registry[all_tasks[0]].side_effect = RuntimeError

        args = Args(tasks=all_tasks)

        index.main(args)

        for task in registry:
            registry[task].assert_called_once()

    def test_should_ignore_unknown_tasks(self):
        args = Args(tasks=['foo'])
        try:
            index.main(args)
        except Exception as e:
            self.fail(f"Unexpected exception {e}")


if __name__ == '__main__':
    unittest.main()
