__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import datetime
import io
import os
from typing import Optional

import fastapi as fa
import fastapi.responses as fa_resp
from API.bagfile_fns import bag2slice_fn
from API.image_fns import bag2img_fn
from API.video_fns import bag2vid_fn
from API.json_fns import info_fn
from config import ExceptionHandler
from config import code_handler as coder
from config import config_handler as configurator
from config import logging_handler as logger
from API.request_models import bag2slice_request, bag2img_request, bag2vid_request

converter = fa.FastAPI(title='Bag Converter API')


@converter.get(path='/info', tags=['get', 'info'],
               description=coder.get_value('info_fn_disc')
               )
async def get_info(bagfile: str):
    return fa_resp.JSONResponse(info_fn(bagfile))


@converter.get(path='/bag2slice', tags=['get', 'slice'],
               description=coder.get_value('bag2slice_fn_disc')
               )
async def get_bag2slice(
        bagfile: str,
        topic: str = None,
        profile: str = None,
        start_relative: int = 0,
        end_relative: int = 2000,
        start_time: int = None,
        end_time: int = None,
        apply_time_correct: bool = False
):
    bagfile_slice = bag2slice_fn(
        bagfile=bagfile,
        topic=topic,
        profile=profile,
        start_relative=start_relative,
        end_relative=end_relative,
        start_time=start_time,
        end_time=end_time,
        apply_time_correct=apply_time_correct
    )
    logger.info(f'bagfile slice size: {os.stat(bagfile_slice.name).st_size}')

    filename = datetime.datetime.now().strftime('bagslice_%Y%m%d-%H%M%S.bag')
    file_like = open(bagfile_slice.name, mode="rb")
    return fa_resp.StreamingResponse(content=file_like, media_type="file/bag",
                                         headers={'Content-Disposition': 'inline; filename="%s"' % (filename,)})


@converter.post(path='/bag2slice', tags=['post', 'slice'],
               description=coder.get_value('bag2slice_fn_disc')
               )
async def post_bag2slice(request: bag2slice_request):
    bagfile_slice = bag2slice_fn(
        bagfile=request.bagfile,
        topic=request.topic,
        profile=request.profile,
        start_relative=request.start_relative,
        end_relative=request.end_relative,
        start_time=request.start_time,
        end_time=request.end_time,
        apply_time_correct=request.apply_time_correct
    )
    logger.info(f'bagfile slice size: {os.stat(bagfile_slice.name).st_size}')

    filename = datetime.datetime.now().strftime('bagslice_%Y%m%d-%H%M%S.bag')
    file_like = open(bagfile_slice.name, mode="rb")
    return fa_resp.StreamingResponse(content=file_like, media_type="file/bag",
                                         headers={'Content-Disposition': 'inline; filename="%s"' % (filename,)})


@converter.post(path='/bag2img', tags=['post', 'image'],
               description=coder.get_value('bag2img_fn_disc')
               )
async def post_bag2img(request: bag2img_request):
    image = bag2img_fn(bagfile=request.bagfile,
                        topic=request.topic,
                        real_time=request.real_time,
                        relative_time=request.relative_time,
                        overlay=request.overlay,
                        overlay_id=request.overlay_id,
                        apply_time_correct=request.apply_time_correct)

    image_buf = io.BytesIO()
    image.save(image_buf, 'jpeg', quality=request.compression_quality)
    image_buf.seek(0)  # important here!

    if request.compression_quality == 0:
        return fa_resp.StreamingResponse(content=image_buf, media_type="image/bmp",
                                         headers={'Content-Disposition': 'inline; filename="%s.bmp"' % (topic,)})

    return fa_resp.StreamingResponse(content=image_buf, media_type="image/jpeg",
                                     headers={'Content-Disposition': 'inline; filename="%s.jpg"' % (topic,)})


@converter.get(path='/bag2img', tags=['get', 'image'],
               description=coder.get_value('bag2img_fn_disc')
               )
async def get_bag2img(
        bagfile: str,
        topic: str,
        real_time: Optional[int] = None,
        relative_time: Optional[int] = None,
        overlay: Optional[bool] = False,
        overlay_id: Optional[int] = -1,
        compression_quality: Optional[int] = configurator.get_value('jpeg_compression_quality_app'),
        apply_time_correct: Optional[bool] = False
):
    image = bag2img_fn(bagfile=bagfile,
                        topic=topic,
                        real_time=real_time,
                        relative_time=relative_time,
                        overlay=overlay,
                        overlay_id=overlay_id,
                        apply_time_correct=apply_time_correct)

    image_buf = io.BytesIO()
    image.save(image_buf, 'jpeg', quality=compression_quality)
    image_buf.seek(0)  # important here!

    if compression_quality == 0:
        return fa_resp.StreamingResponse(content=image_buf, media_type="image/bmp",
                                         headers={'Content-Disposition': 'inline; filename="%s.bmp"' % (topic,)})

    return fa_resp.StreamingResponse(content=image_buf, media_type="image/jpeg",
                                     headers={'Content-Disposition': 'inline; filename="%s.jpg"' % (topic,)})



@converter.get(path='/bag2vid', tags=['get', 'video'],
               description=coder.get_value('bag2vid_fn_disc')
               )
async def get_bag2vid(
        bagfile: str,
        topic: str,
        start_time: Optional[int] = None,
        end_time: Optional[int] = None,
        start_relative: Optional[int] = None,
        end_relative: Optional[int] = None,
        overlay: Optional[bool] = False,
        overlay_id: Optional[int] = -1,
        compression_quality: Optional[int] = configurator.get_value('jpeg_compression_quality_app'),
        apply_time_correct: Optional[bool] = False
):
    vid_file = bag2vid_fn(bagfile=bagfile,
                          topic=topic,
                          start_time=start_time,
                          start_relative=start_relative,
                          end_time=end_time,
                          end_relative=end_relative,
                          overlay=overlay,
                          overlay_id=overlay_id,
                          apply_time_correct=apply_time_correct)

    logger.info(f'bagfile slice size: {os.stat(vid_file.name).st_size}')

    filename = datetime.datetime.now().strftime('vid_%Y%m%d-%H%M%S.mp4')
    file_like = open(vid_file.name, mode="rb")
    return fa_resp.StreamingResponse(content=file_like, media_type="video/mp4",
                                     headers={'Content-Disposition': 'inline; filename="%s"' % (filename,)})


@converter.post(path='/bag2vid', tags=['post', 'video'],
               description=coder.get_value('bag2vid_fn_disc')
               )
async def post_bag2vid(request: bag2vid_request):
    vid_file = bag2vid_fn(bagfile=request.bagfile,
                          topic=request.topic,
                          start_time=request.start_time,
                          start_relative=request.start_relative,
                          end_time=request.end_time,
                          end_relative=request.end_relative,
                          overlay=request.overlay,
                          overlay_id=request.overlay_id,
                          apply_time_correct=request.apply_time_correct)

    logger.info(f'bagfile slice size: {os.stat(vid_file.name).st_size}')

    filename = datetime.datetime.now().strftime('vid_%Y%m%d-%H%M%S.mp4')
    file_like = open(vid_file.name, mode="rb")
    return fa_resp.StreamingResponse(content=file_like, media_type="video/mp4",
                                     headers={'Content-Disposition': 'inline; filename="%s"' % (filename,)})


@converter.exception_handler(ExceptionHandler)
async def handle_error(request: fa.Request, exc: ExceptionHandler):
    """ Called on any occurrence of ExceptionHandler """
    msg = {'status': 'error',
           'code': exc.code,
           'message': exc.msg}
    return fa_resp.JSONResponse(msg)
