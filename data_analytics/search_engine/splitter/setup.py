"""Setup file"""
from setuptools import setup
from config.constants import VERSION, SPLITTER_NAME

setup(
    name=SPLITTER_NAME,
    version=VERSION,
    install_requires=[
        'DANFUtils>=0.2.29',
        'ContainerClient>=0.6.102',
        'pytz',
        'psutil==5.7.3'
    ]
)
