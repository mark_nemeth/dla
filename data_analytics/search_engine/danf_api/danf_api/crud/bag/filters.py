"""Bag related filter definitions."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import List

from ..base import EQ_OP_MAP, INTERVAL_OP_MAP, REGEX_OP_MAP
from ..utils import convert_to_milli_timestamp
from ...core.errors import RHSColonOperatorError, RHSColonValueError
from ...models.bag import BagFilterParams
from ...models.base import OpValue
from ...models.tag import TagFilterParams


def construct_bag_filter(filter_params: BagFilterParams) -> dict:
    """Construct db filter object from the api filter object.

    This is the main filter constructor for bags calling the sub-constructors for each field.

    Args:
        filter_params: api filter object

    Returns:
        db filter object
    """
    filter_list = [globals()[f"_construct_{field}_filter"](filters) for field, filters in filter_params if filters]
    return {"$and": filter_list} if filter_list else {}


def _construct_guid_filter(guid_filters: List[OpValue]) -> dict:
    operator_mapping = EQ_OP_MAP
    try:
        [operator_mapping[f.op.lower()] for f in guid_filters]
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the guid filter", valid_op=list(operator_mapping))
    return {"guid": {"$in": [f.value for f in guid_filters]}}


def _construct_start_filter(start_filters: List[OpValue]) -> dict:
    operator_mapping = {**EQ_OP_MAP, **INTERVAL_OP_MAP}
    try:
        return {"start": {operator_mapping[f.op.lower()]: convert_to_milli_timestamp(f.value) for f in start_filters}}
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the start filter", valid_op=list(operator_mapping))
    except ValueError as e:
        raise RHSColonValueError(value=e.args[0], context="the start filter", valid_value=": int timestamp or ISO date")


def _construct_end_filter(end_filters: List[OpValue]) -> dict:
    operator_mapping = {**EQ_OP_MAP, **INTERVAL_OP_MAP}
    try:
        return {"end": {operator_mapping[f.op.lower()]: convert_to_milli_timestamp(f.value) for f in end_filters}}
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the end filter", valid_op=list(operator_mapping))
    except ValueError as e:
        raise RHSColonValueError(value=e.args[0], context="the end filter", valid_value=": int timestamp or ISO date")


def _construct_duration_filter(duration_filters: List[OpValue]) -> dict:
    operator_mapping = {**EQ_OP_MAP, **INTERVAL_OP_MAP}
    try:
        return {"duration": {operator_mapping[f.op.lower()]: int(f.value) for f in duration_filters}}
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the duration filter", valid_op=list(operator_mapping))
    except ValueError as e:
        raise RHSColonValueError(value=e.args[0], context="the duration filter", valid_value="an integer")


def _construct_vehicle_id_num_filter(vehicle_id_num_filters: List[OpValue]) -> dict:
    operator_mapping = EQ_OP_MAP
    try:
        [operator_mapping[f.op.lower()] for f in vehicle_id_num_filters]
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the vehicle_id_num filter", valid_op=list(operator_mapping))
    return {"vehicle_id_num": {"$in": [f.value for f in vehicle_id_num_filters]}}


def _construct_topics_filter(topics_filters: List[OpValue]) -> dict:
    operator_mapping = EQ_OP_MAP
    try:
        [operator_mapping[f.op.lower()] for f in topics_filters]
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the topics filter", valid_op=list(operator_mapping))
    return {"topics": {"$all": [f.value for f in topics_filters]}}


def _construct_size_filter(size_filters: List[OpValue]) -> dict:
    operator_mapping = {**EQ_OP_MAP, **INTERVAL_OP_MAP}
    try:
        return {"size": {operator_mapping[f.op.lower()]: int(f.value) for f in size_filters}}
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the size filter", valid_op=list(operator_mapping))
    except ValueError as e:
        raise RHSColonValueError(value=e.args[0], context="the size filter", valid_value="an integer")


def _construct_num_messages_filter(num_messages_filters: List[OpValue]) -> dict:
    operator_mapping = {**EQ_OP_MAP, **INTERVAL_OP_MAP}
    try:
        return {"num_messages": {operator_mapping[f.op.lower()]: int(f.value) for f in num_messages_filters}}
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the num_messages filter", valid_op=list(operator_mapping))
    except ValueError as e:
        raise RHSColonValueError(value=e.args[0], context="the num_messages filter", valid_value="an integer")


def _construct_drive_types_filter(drive_types_filters: List[OpValue]) -> dict:
    operator_mapping = EQ_OP_MAP
    try:
        [operator_mapping[f.op.lower()] for f in drive_types_filters]
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the drive_types filter", valid_op=list(operator_mapping))
    return {"drive_types": {"$in": [f.value for f in drive_types_filters]}}


def _construct_file_name_filter(file_name_filters: List[OpValue]) -> dict:
    operator_mapping = {**EQ_OP_MAP, **REGEX_OP_MAP}
    try:
        return {"$or": [{"file_name": {operator_mapping[f.op.lower()]: f.value}} for f in file_name_filters]}
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the file_name filter", valid_op=list(operator_mapping))


def _construct_origin_filter(origin_filters: List[OpValue]) -> dict:
    operator_mapping = EQ_OP_MAP
    try:
        [operator_mapping[f.op.lower()] for f in origin_filters]
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the origin filter", valid_op=list(operator_mapping))
    return {"origin": {"$in": [f.value for f in origin_filters]}}


def _construct_tag_filter(tag_filters: List[TagFilterParams]) -> dict:
    operator_mapping = {**EQ_OP_MAP, **REGEX_OP_MAP}
    try:
        return {
            "$and": [
                {
                    "tags": {
                        "$elemMatch": {
                            "namespace": f.namespace,
                            "value": {operator_mapping[f.value.op.lower()]: f.value.value},
                        }
                    }
                }
                for f in tag_filters
            ]
        }
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the tag filter", valid_op=list(operator_mapping))


def _construct_no_tag_filter(no_tag_filters: List[TagFilterParams]) -> dict:
    operator_mapping = {**EQ_OP_MAP, **REGEX_OP_MAP}
    try:
        return {
            "$and": [
                {
                    "tags": {
                        "$not": {
                            "$elemMatch": {
                                "namespace": f.namespace,
                                "value": {operator_mapping[f.value.op.lower()]: f.value.value},
                            }
                        }
                    }
                }
                for f in no_tag_filters
            ]
        }
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="the no_tag filter", valid_op=list(operator_mapping))
