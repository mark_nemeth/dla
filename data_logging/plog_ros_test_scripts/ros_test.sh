
#!/bin/bash
#set -e

REMOTE=(phoenix licam1) #(192.168.77.25)
LOCAL=mtdc    #(192.168.77.100)

ROSMASTER=phoenix # davos #localhost or cmtcde7u055 set in .baschrc for REMOTES, no elegant way to do this in script yet

DURATION=120          # Seconds
BANDWIDTH=1000000000  # Per client - only for SYNTHETIC_DATA
RATE_PER_CLIENT=$BANDWIDTH/(64*1024)
PACKET_SIZE=128000     # only for SYNTHETIC_DATA
MULTIPLE_TX=true     # Default: false, this activate multiple nodes. Adjustable with NUMBER_TX
NUMBER_TX=1

WRITE_TO_FILE=true
RECORD_TOOL="/home/pid1083/workspace/dla_repo/data_logging/ros/melodic/devel/lib/athena_rosbag/record"
SETUP_BASH_REC=/home/pid1083/workspace/dla_repo/data_logging/ros/melodic/devel/setup.bash
RECORD_PATH=~/sequences/
buffer_size=4095
chunk_size=102400

calling_dir=~/workspace/dla_repo/data_logging/plog_ros_test_scripts

PLAY_BAG=false
BAG_LOCATION=~/workspace/20180906_095536_CID_drive_lidar2_ORIGINAL.bag # make sure file is on REMOTE
# Get file from
# /mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121789/2018/09/06/20180906_095536_CID_drive_lidar2_ORIGINAL.bag
######################################################
######################################################
#                                                    #
#               SOME ERROR CHECKING                  #
#                                                    #
######################################################
if $PLAY_BAG; then
  for (( x=0; x<${#REMOTE[@]}; x++)); do
    if ssh -x ${REMOTE[$x]} "[ ! -f $BAG_LOCATION ]"; then
      echo ERROR - The bagfile $BAG_LOCATION you want to replay was not found on ${REMOTE[$x]}.
      exit 1
    fi
  done
fi
######################################################

echo "_______________________________________________"
echo "ROS Master should already be running on $ROSMASTER"

sleep 3

echo "_______________________________________________"
for (( x=0; x<${#REMOTE[@]}; x++)); do
        if $MULTIPLE_TX; then
          # ~~~~ Start mulitple transmitters per remote ~~~
          counter=1
          while [ $counter -le $NUMBER_TX ]; do
            ssh -x ${REMOTE[$x]} "
            export ROS_HOSTNAME=${REMOTE[$x]}
            source ~/workspace/dla_repo/data_logging/ros/melodic/devel/setup.bash
	          rosrun dla tx -r $RATE_PER_CLIENT
            " &
            sleep 2
            echo "Client $x$counter started on ${REMOTE[$x]}"
            ((counter++))
            sleep 1
          done
        elif $PLAY_BAG; then

            ssh -x ${REMOTE[$x]} "
            export ROS_HOSTNAME=${REMOTE[$x]}
            source /opt/ros/melodic/setup.bash
            rosbag play -q --clock $BAG_LOCATION & > /dev/null
            " &
            # sourcing here is fine bcs package won't change for now
        else
          # ~~~~ Start single transmitter per remote ~~~
            ssh -x ${REMOTE[$x]} "
            export ROS_HOSTNAME=${REMOTE[$x]}
	          source ~/workspace/dla_repo/data_logging/ros/melodic/devel/setup.bash
	          rosrun dla tx -r $RATE_PER_CLIENT
            " &
        fi

done


export ROS_HOSTNAME=${LOCAL}
if $WRITE_TO_FILE; then
    echo "_______________________________________________"
    echo "${LOCAL} : Subscribing to all topics and write to file."
    cd $RECORD_PATH

    source $SETUP_BASH_REC
    echo "Using following recorder:"
    which $RECORD_TOOL
    $RECORD_TOOL -a -o $x --duration=$DURATION -b $buffer_size --chunksize=$chunk_size  >> $calling_dir/athena_rosbag_record_$x.log &

    wait_id=$!
else
        cd $RECORD_PATH
        echo "_______________________________________________"
        echo "${LOCAL[$x]} : Subscribing to all topics."
        echo "Wait $DURATION seconds and do nothing else."
        source ~/workspace/dla_repo/data_logging/ros/melodic/devel/setup.bash
        rosrun dla rx >> $calling_dir/rx.log &

        sleep 1
fi

rosnode list -a >> $calling_dir/rosnode_list.log
rostopic list -v >> $calling_dir/rostopic_list.log

# wait for last process to finish
if $WRITE_TO_FILE; then
    wait $wait_id
    echo "_______________________________________________"
    echo "Recording done."
    sleep 4
    pkill --signal 2 record
else
    sleep $DURATION
    echo "_______________________________________________"
    echo "Done."
fi

## cleanup and kill ros Master, suppose all clients run on same machine
echo "Killing Playback."
    for (( x=0; x<${#REMOTE[@]}; x++)); do
      if $PLAY_BAG; then
        echo "Killing rosbag play on remote..."
        ssh -x ${REMOTE[$x]} "killall -s 2 play"
      else
        echo "Killing tx on remote..."
        ssh -x ${REMOTE[$x]} "killall -s 2 tx"
      fi
    done

if ! $WRITE_TO_FILE; then
    pkill --signal 2 rx
fi

sleep 2


wait $!
