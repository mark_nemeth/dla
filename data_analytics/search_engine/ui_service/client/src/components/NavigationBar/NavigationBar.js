import React from 'react';
import Button from 'react-bootstrap/Button';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import {isStringValid} from '../../utils/helperFunctions';
import {fileNameConverter} from '../../utils/valueConverter';
import './NavigationBar.scss'

function NavigationBar(props) {

  const fileName = (!isStringValid(props.currentFilePath)) ? ''
      : fileNameConverter(props.currentFilePath.substring(props.currentFilePath.lastIndexOf("/") + 1, props.currentFilePath.length), props.fileType);

  const showBackButton = false;

  function navigateHome() {
    props.navigate.push('/')
  }

  function navigateBack() {
    props.navigate.goBack();
  }

  return (
    <div className="navigation-bar">
      {showBackButton && <Button onClick={navigateBack} className="navigation-back-button">
        <div className="icon-arrow-left-1"/>
        <div>Back</div>
      </Button>}
      <Breadcrumb className="navigation-breadcrumb">
        <Breadcrumb.Item onClick={navigateHome}>Home</Breadcrumb.Item>
        <Breadcrumb.Item active>{fileName}</Breadcrumb.Item>
      </Breadcrumb>
    </div>
  );
}

export default NavigationBar