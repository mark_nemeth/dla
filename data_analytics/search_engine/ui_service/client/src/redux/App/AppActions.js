import { createAction } from '@reduxjs/toolkit';
import axios from 'axios';
import configHandler from '../../config/configHandler';
import { isObjectValid } from '../../utils/helperFunctions';

//region error message handling
export const showError = createAction('error/showError');
export const hideError = createAction('error/hideError');
//endregion

//region user data
export const fetchUserDataSuccess = createAction('user/fetchUserDataSuccess');
export const fetchUserDataError = createAction('user/fetchUserDataError');

export function fetchUserData() {
  return dispatch => {
    return axios.request(configHandler.getUserDataAPI())
      .then(res => {
        if (isObjectValid(res.data)) {
          dispatch(fetchUserDataSuccess(res.data));
        } else dispatch(fetchUserDataError());
      })
      .catch(err => {
        console.log(err);
        dispatch(fetchUserDataError());
      });
  }
}
//endregion

//region version
export const fetchVersionsSuccess = createAction('app/fetchVersionsSuccess');
export const fetchVersionsError = createAction('app/fetchVersionsError');

export function fetchDanfVersionInformation() {
  return dispatch => {
    return axios.request(configHandler.getVersionAPI())
      .then(res => {
        if (isObjectValid(res.data)) {
          dispatch(fetchVersionsSuccess(res.data));
        } else {
          dispatch(fetchVersionsError());
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(fetchVersionsError());
      });
  }
}
//endregion

//region version
export const fetchFeatureTogglesSuccess = createAction('app/fetchFeatureTogglesSuccess');
export const fetchFeatureTogglesError = createAction('app/fetchFeatureTogglesError');

export function fetchFeatureToggles() {
  return dispatch => {
    return axios.request(configHandler.getFeatureToggleAPI())
        .then(res => {
          if (isObjectValid(res.data)) {
            dispatch(fetchFeatureTogglesSuccess(res.data));
          } else {
            dispatch(fetchFeatureTogglesError());
          }
        })
        .catch(err => {
          console.log(err);
          dispatch(fetchFeatureTogglesError());
        });
  }
}
//endregion
