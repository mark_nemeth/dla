__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import Type, List

from pydantic import BaseModel

from config.config_handler import config_dict
from kubeflow.flows.common import BaseFlow
from kubeflow.flows.danf_az_original_processing_flow import DanfAzOriginalProcessingFlow
from kubeflow.flows.danf_az_alwayson_processing_flow import DanfAzAlwaysonFlow
from kubeflow.flows.danf_az_snippet_flow import DanfAzSnippetFlow
from kubeflow.flows.danf_azure_processing_flow import DanfAzureProcessingFlow
from kubeflow.flows.danf_alwayson_processing_flow import DanfAlwaysonFlow
from kubeflow.flows.danf_original_processing_flow import DanfOriginalProcessingFlow
from kubeflow.steps.atpftst_extractor import AtpftstExtractorOp
from kubeflow.steps.atpdml_extractor import AtpdmlExtractorOp
from kubeflow.steps.atpdml_poi_extractor import AtpdmlPoiExtractorOp
from kubeflow.steps.atbeval_extractor import AtbevalExtractorOp
from kubeflow.steps.common import RETRY_NUM
from kubeflow.steps.cv1_exporter import Cv1ExporterOp
from kubeflow.steps.danf_extractor import ExtractorOp, DanfExtractorInitOp, \
    DanfExtractorOp
from kubeflow.steps.lidar_exporter import LidarExporterOp
from kubeflow.steps.splitter import SplitterOp, AzureSplitterOp
from kubeflow.steps.stripper import StripperOp
from kubeflow.steps.file_transfer import FileTransferOp
from kubeflow.steps.rca_extractor import RcaExtractorOp
from kubeflow.steps.disengagement_report import DisengagementReportOp
from kubeflow.steps.reindexer import ReindexerOp
from kubeflow.steps.feature_extractor import FeatureExtractorOp
from model.exceptions import EventProcessingError

# TODO: Refactor - The steps per flow and their configuration should be taken dynamically from Flow API
# First level: flow name -> flow-config-dict
# flow-config-dict: step name -> step-config-dict
# step-config-dict: details of step
flow_run_configuration = {

    DanfAzOriginalProcessingFlow.NAME: {
        DanfExtractorInitOp.NAME: {
            'image': config_dict.get_value(DanfExtractorInitOp.NAME),
            'max_retries': RETRY_NUM,
            'is_essential': True,
        },
        DanfExtractorOp.NAME: {
            'image': config_dict.get_value(DanfExtractorOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        },
        StripperOp.NAME: {
            'image': config_dict.get_value(StripperOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        },
        AzureSplitterOp.NAME: {
            'image': config_dict.get_value(AzureSplitterOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        }
    },

    DanfOriginalProcessingFlow.NAME: {
        DanfExtractorInitOp.NAME: {
            'image': config_dict.get_value(DanfExtractorInitOp.NAME),
            'max_retries': RETRY_NUM,
            'is_essential': True,
        },
        ReindexerOp.NAME: {
            'image': config_dict.get_value(ReindexerOp.NAME),
            'max_retries': RETRY_NUM,
            'is_essential': False,
        },
        DanfExtractorOp.NAME: {
            'image': config_dict.get_value(DanfExtractorOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        },
        AtpftstExtractorOp.NAME: {
            'image': config_dict.get_value(AtpftstExtractorOp.NAME),
            'is_essential': False,
            'max_retries': RETRY_NUM,
        },
        AtpdmlExtractorOp.NAME: {
            'image': config_dict.get_value(AtpdmlExtractorOp.NAME),
            'is_essential': False,
            'max_retries': RETRY_NUM,
        },
        AtpdmlPoiExtractorOp.NAME: {
            'image': config_dict.get_value(AtpdmlPoiExtractorOp.NAME),
            'is_essential': False,
            'max_retries': RETRY_NUM,
        },
        AtbevalExtractorOp.NAME: {
            'image': config_dict.get_value(AtbevalExtractorOp.NAME),
            'is_essential': False,
            'max_retries': RETRY_NUM,
        },
        SplitterOp.NAME: {
            'image': config_dict.get_value(SplitterOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        },
        StripperOp.NAME: {
            'image': config_dict.get_value(StripperOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        },
        RcaExtractorOp.NAME: {
            'image': config_dict.get_value(RcaExtractorOp.NAME),
            'is_essential': False,
            'max_retries': RETRY_NUM,
        }
    },

    DanfAzureProcessingFlow.NAME: {
        ExtractorOp.NAME: {
            'image': config_dict.get_value(ExtractorOp.NAME),
            'max_retries': RETRY_NUM,
            'is_essential': True,
        },
        SplitterOp.NAME: {
            'image': config_dict.get_value(SplitterOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        },
        LidarExporterOp.NAME: {
            'image': config_dict.get_value(LidarExporterOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        },
        Cv1ExporterOp.NAME: {
            'image': config_dict.get_value(Cv1ExporterOp.NAME),
            'is_essential': True,
            'max_retries': RETRY_NUM,
        }
    },

    DanfAzSnippetFlow.NAME: {
        Cv1ExporterOp.NAME: {
            'image': config_dict.get_value(Cv1ExporterOp.NAME),
            'max_retries': RETRY_NUM,
            'is_essential': True,
        }
    },

    DanfAzAlwaysonFlow.NAME: {
        DanfExtractorInitOp.NAME: {
            'image': config_dict.get_value(DanfExtractorInitOp.NAME),
            'max_retries': RETRY_NUM,
            'is_essential': True,
        },
        FeatureExtractorOp.NAME: {
            'image': config_dict.get_value(FeatureExtractorOp.NAME),
            'max_retries': RETRY_NUM,
            'is_essential': True,
        }
    },

    DanfAlwaysonFlow.NAME: {
        DanfExtractorInitOp.NAME: {
            'image': config_dict.get_value(DanfExtractorInitOp.NAME),
            'max_retries': RETRY_NUM,
            'is_essential': True,
        }
    }
}


class FlowRunMetadata(BaseModel):
    class Reference(BaseModel):
        type: str = 'bagfile_guid'
        value: str

    class Step(BaseModel):
        name: str
        image: str
        tag: str
        max_retries: int
        is_essential: bool

    reference: Reference
    flow_name: str
    flow_version: str
    steps: List[Step]


def build_flow_run_metadata(file: dict,
                            flow: Type[BaseFlow]) -> FlowRunMetadata:
    """Creates and returns a flow run metadata object of the specified
    flow for the given file.

    :param file: Bagfile or childbagfile metadata for which flow run should be
        built
    :param flow: Flow which should be executed by the flow run to be built
    :return: flow run metadata
    """
    flow_run_config = flow_run_configuration.get(flow.NAME)
    if not flow_run_config:
        raise ValueError(f"Missing configuration for flow {flow.NAME}")

    steps = [_build_step(step, step_config) for step, step_config in flow_run_config.items()]

    return FlowRunMetadata(
        reference=_build_reference(file),
        flow_name=flow.NAME,
        flow_version=flow.VERSION,
        steps=steps
    )


def _build_reference(file: dict) -> FlowRunMetadata.Reference:
    # Decide whether file is a bagfile or childbagfile based on the presence
    # of parent_guid attribute
    ref_type = 'childbagfile_guid' if 'parent_guid' in file else 'bagfile_guid'

    return FlowRunMetadata.Reference(type=ref_type, value=file['guid'])


def _build_step(step_name, step_config):
    step_img = step_config.get('image')
    max_retries = step_config.get('max_retries')
    is_essential = step_config.get('is_essential')

    if None in (step_img, max_retries, is_essential):
        raise ValueError(f"Missing configuration for step {step_name}. "
                         f"config = {step_config}")

    img_and_tag = step_img.rsplit(':', 1)
    return FlowRunMetadata.Step(
        name=step_name,
        image=img_and_tag[0],
        tag=img_and_tag[1],
        max_retries=max_retries,
        is_essential=is_essential
    )
