__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DBConnector import DBConnector

from config.config_handler import config_dict


test_cert = '../../.cert/metadata-api-cert.pem'


def get_testdb_connector(allow_remote: bool = False):
    """
    :param allow_remote: Allow connection to remote DB (default False)
    :return: DBConnector wired to the test DB
    """
    host = config_dict.get_value('db_hostname')
    port = config_dict.get_value('db_port')
    db_name = config_dict.get_value('db_name')
    user = config_dict.get_value('db_user')
    key = config_dict.get_value('db_key')
    protocol = config_dict.get_value('db_protocol')
    ssl = str(config_dict.get_value('db_ssl')).lower() == 'true'

    if not allow_remote and host not in ('localhost', '127.0.0.1'):
        raise ValueError(f"Waring: Trying to connect ot remote DB ({host}) "
                         f"during testing. To override this check, "
                         f"specify 'allow_remote=true'")

    return DBConnector(host, port, db_name, user, key, protocol, ssl)
