"""Unittest for child_bagfile_search_handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import unittest
from unittest.mock import patch, MagicMock
from DANFUtils.utils import generate_deterministic_guid
from handlers.child_bagfile_search_handler import ChildBagfileSearchHandler

bagfile_guid = '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'

child_1 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "parent_guid": bagfile_guid,
    "start": 1558962320000,
    "end": 1558962372000,
    "parent_index_start": 169,
    "parent_index_end": 220,
    "metadata": {
        'ext1_4': {
            'stereo_cnn': [{
                'bounding_box_labels': {
                    '28': 2
                }
            }]
        }
    },
    "bva": 7,
    "is_test_data": 1
}


class TestChildBagfileSearchHandler(unittest.TestCase):
    def test_create_search_query(self):
        chbh = ChildBagfileSearchHandler()

        ###Check for proper query generation 4 items####################
        query = {
            'bva_min': 5,
            'file_path': 'path',
            'parent_guid': bagfile_guid,
            'cv1_classifier': 28
        }
        res = {
            '$and': [{
                'bva': {
                    '$gte': 5
                }
            }, {
                'link': {
                    '$options': 'i',
                    '$regex': 'path'
                }
            }, {
                'parent_guid': '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'
            }, {
                'metadata.ext1_4.stereo_cnn.bounding_box_labels.28': {
                    '$exists': True
                }
            }]
        }
        self.assertEqual(chbh.create_search_query(query),
                         res,
                         msg="Query wrong for 4 items!")
        ###Check for proper query generation 1 item####################
        query = {'parent_guid': bagfile_guid}
        res = {'parent_guid': '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'}
        self.assertEqual(chbh.create_search_query(query),
                         res,
                         msg="Query wrong for 1 item!")
        ###Check for empty query generation############################
        query = {}
        res = {}
        self.assertEqual(chbh.create_search_query(query),
                         res,
                         msg="Query wrong for empty dict!")

    @patch('handlers.child_bagfile_search_handler.DBDataHandler')
    def test_perform_search(self, mocked_DBDataHandler):
        ######Mock DBDataHandler ##############################
        mm = MagicMock()
        mm.search_child_bagfiles.return_value = child_1
        mocked_DBDataHandler.return_value = mm
        #######################################################
        chbh = ChildBagfileSearchHandler()
        ###Check for return####################################
        query = {'parent_guid': '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'}
        self.assertEqual(
            chbh.perform_search(**query),
            child_1,
            msg="Wrong child bagfile during perform_search returned!")


if __name__ == '__main__':
    unittest.main()
