__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from collections import namedtuple

NewBagfileEvent = namedtuple('NewBagfileEvent',
                             'file_name, file_path, container_name, storage_account, msg_id, msg_pop_receipt')


NewOnPremBagfileEvent = namedtuple('NewOnPremBagfileEvent',
                                   'file_name, file_path')

NewDrivesJson = namedtuple('NewDrivesJson',
                             'file_name, file_path, container_name, storage_account, msg_id, msg_pop_receipt')

NewOnPremDrivesJson = namedtuple('NewOnPremDrivesJson',
                                 'file_name, file_path, cluster')