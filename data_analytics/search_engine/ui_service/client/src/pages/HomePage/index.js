import {connect} from 'react-redux';
import {HomePage} from './HomePage';
import {
  refresh,
  fetchOriginalFileFilters,
  fetchChildBagfileFilters,
  fetchDriveFilters,
  fetchAlwaysonFileFilters,
  exportResults
} from '../../redux/Home/HomeActions';
import {
  fetchUserData,
  fetchFeatureToggles
} from '../../redux/App/AppActions';

function mapStateToProps(state, ownProps) {
  const currentFiles = state.currentFiles || [];
  const error = state.error || false;
  const errorMessage = state.errorMessage || '';
  const showLoading = state.showLoading || false;
  const total = state.total || 0;
  const isDriveViewActive = state.featureToggles.isDriveViewActive;

  return {currentFiles, error, errorMessage, showLoading, total, isDriveViewActive};
}

export default connect(
    mapStateToProps,
    {
      refresh,
      fetchOriginalFileFilters,
      fetchAlwaysonFileFilters,
      fetchChildBagfileFilters,
      fetchDriveFilters,
      fetchUserData,
      exportResults,
      fetchFeatureToggles
    }
)(HomePage);