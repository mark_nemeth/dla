"""Setup file"""
from setuptools import setup, find_packages
from DANFUtils.config.constants import VERSION

setup(
    name='DANFUtils',
    version=VERSION,
    packages=find_packages(exclude=["tests"]),
    package_data={'': ['*/config/*.init']},
    long_description=open('README.md', 'r').read(),
    long_description_content_type="text/markdown",
    test_suite="tests",
    install_requires=[
        'jsonschema>=3.0',
        'python-json-logger>=0.1,<0.2',
        'opencensus>=0.7,<1.0',
        'opencensus-ext-azure>=1.0,<2.0',
        'opencensus-ext-logging>=0.1,<1.0'
    ]
)
