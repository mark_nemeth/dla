"""Setup file"""
from setuptools import setup
from config.constants import VERSION

setup(
    name='cache_refresher',
    version=VERSION,
    install_requires=[
        'requests>=2.22.0',
        'ContainerClient>=0.6.102',
        'DANFUtils>=0.2.29'
    ]
)

