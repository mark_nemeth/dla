__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from pathlib import PurePath, Path
from typing import Optional

from kfp import dsl
from kubernetes.client import V1LocalObjectReference

from config.config_handler import config_dict
from kubeflow.flows.common import BaseFlow, get_mount_id
from kubeflow.steps.atpftst_extractor import AtpftstExtractorOp
from kubeflow.steps.cleanup import CleanupOp
from kubeflow.steps.danf_extractor import DanfExtractorInitOp
from kubeflow.steps.disengagement_report import DisengagementReportOp
from kubeflow.steps.file_transfer import FileTransferOp
from kubeflow.steps.feature_extractor import FeatureExtractorOp

logger = logging.getLogger(__name__)


class DanfAlwaysonFlow(BaseFlow):

    NAME = "danf-alwayson-processing-flow-v1"
    VERSION = "1.0"
    DESCRIPTION = "on prem flow for processing ALWAYSON bagfiles"
    IMAGE_PULL_SECRET = "dla-docker-pull-secret"

    @classmethod
    def create(cls,
               bagfile: dict,
               flow_run_guid: str,
               test_mode: bool,
               output_path_override: Optional[str]):
        return cls._build_pipeline(
            bagfile['guid'],
            flow_run_guid,
            bagfile['link'],
            output_path_override,
            test_mode)

    @classmethod
    def _build_pipeline(cls,
                        bagfile_guid,
                        flow_run_guid,
                        file_path,
                        output_path_override,
                        test_mode):
        @dsl.pipeline(
            name=cls.NAME,
            description=cls.DESCRIPTION
        )
        def processing_pipeline():
            # initialize params
            mapr_pvc_name = cls.get_config().get('mapr_pvc_name')
            mapr_user_id = cls.get_config().get('mapr_user_id')
            path_to_bagfile_in_mapr = PurePath(file_path)
            file_name = path_to_bagfile_in_mapr.name
            cluster = PurePath(*path_to_bagfile_in_mapr.parts[0:3])
            # transform path s.t. it can be used for mounting directory in pvc,
            # e.g. /mapr/624.mbc.us/usa/data/input/.../plog/file.bag
            # => data/input/.../plog
            path_to_bagfile_dir_in_mapr = (path_to_bagfile_in_mapr
                                           .relative_to(cluster)
                                           .parent)
            output_path = cls.get_output_path(
                output_path_override,
                path_to_bagfile_dir_in_mapr)

            vin = cls.extract_vin(path_to_bagfile_dir_in_mapr)
            run_feature_extraction_step = config_dict.get_value('run_feature_extraction', 'disabled')
            run_migrate_step = config_dict.get_value('run_migrate_step', 'enabled')

            # TODO: pvc volume mount subpaths are currently not used since
            # they don't work with the mapr NFS mount. They are still passed
            # as parameters in case we switch back to the mapr csi mounts

            cleanup_step = CleanupOp.create_for_on_prem(
                tasks=['set-sync-flag', 'update-flow-run'],
                bagfile_guid=bagfile_guid,
                flow_run_guid=flow_run_guid,
                bagfile_name=file_name,
                test_mode=test_mode
            )

            with dsl.ExitHandler(cleanup_step):
                # create metadata for each bagfile in the DB
                init_step = DanfExtractorInitOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=f"{DanfExtractorInitOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                    output_container=f"{DanfExtractorInitOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                    mount_id=get_mount_id(),
                    vin=vin,
                    test_mode=test_mode
                )

                # Disabled redundant disengagement step
                # extractor_step = DisengagementReportOp.create(
                #     bagfile_guid=bagfile_guid,
                #     flow_run_guid=flow_run_guid,
                #     input_file_path=f"{DisengagementReportOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                #     output_folder=f"{DisengagementReportOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                #     pvc_name=mapr_pvc_name,
                #     vin=vin,
                #     test_mode=test_mode
                # ).after(init_step)

                ready_to_transfer = init_step
                if run_feature_extraction_step == 'enabled':
                    # process the bagfile and extract events for enrichment of the metadata and not for the childbagfile creation
                    feature_extractor_step = FeatureExtractorOp.create(
                        bagfile_guid=bagfile_guid,
                        flow_run_guid=flow_run_guid,
                        input_file_path=f"{DisengagementReportOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                        output_folder=f"{DisengagementReportOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                        pvc_name=mapr_pvc_name,
                        vin=vin,
                        test_mode=test_mode
                    ).after(init_step)
                    ready_to_transfer = feature_extractor_step

                if run_migrate_step == 'enabled':
                    # copy from ring buffer zone (will be removed after 2 weeks) to input zone - only on-prem relevant, in azure maybe not needed?
                    transfer_step = FileTransferOp.create_file_transfer(
                        bagfile_guid=bagfile_guid,
                        flow_run_guid=flow_run_guid,
                        input_file_path=f"{FileTransferOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                        input_mount_sub_path=str(path_to_bagfile_dir_in_mapr),
                        childbagfiles_output_dir=f"{FileTransferOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                        output_mount_sub_path=output_path,
                        childbagfile_link_template=f"{cluster}/{output_path}" + "/{}",
                        pvc_name=mapr_pvc_name,
                        user_id=mapr_user_id,
                        vin=vin,
                        test_mode=test_mode
                    ).after(ready_to_transfer)

            pipeline_conf = dsl.get_pipeline_conf()
            pipeline_conf.set_image_pull_secrets([
                V1LocalObjectReference(cls.IMAGE_PULL_SECRET)
            ])

        return DanfAlwaysonFlow(processing_pipeline)

    @classmethod
    def get_output_path(cls,
                        output_path_override,
                        path_to_bagfile_dir_in_mapr):

        if output_path_override:
            logger.debug(f"output path overridden by {output_path_override}")
            return output_path_override

        try:
            new_path = list(path_to_bagfile_dir_in_mapr.parts)
            if "buffer" in new_path:
                index = new_path.index("buffer")
                new_path[index] = "input"
            elif "buffer_test" in new_path:
                index = new_path.index("buffer_test")
                new_path[index] = "input_test"
            path_to_bagfile_dir_in_mapr = Path(*new_path)
        except ValueError:
            logger.debug("Buffer not found in input path...")
        return str(path_to_bagfile_dir_in_mapr)

    @classmethod
    def extract_vin(cls, path_to_bagfile_dir_in_mapr):
        # get vehicle VIN
        segments = list(path_to_bagfile_dir_in_mapr.parts)
        try:
            vin_index = segments.index('plog') + 1
            return segments[vin_index]
        except ValueError:
            logger.warning(f"path {path_to_bagfile_dir_in_mapr} does not "
                           f"contain 'plog'. "
                           f"Vehicle whitelisting will be ignored")
            return "unknown"
