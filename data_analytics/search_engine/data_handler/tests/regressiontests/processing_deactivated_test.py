"""Regressiontests for bagfile routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import os
import unittest
from container_client.ContainerClient import ContainerClient
from handlers.bagfile_event_processor import BagfileEventProcessor
from model.models import NewOnPremBagfileEvent


class TestProcessingDeactivated(unittest.TestCase):
    def setUp(self):
        self.bagfile_event_processor = BagfileEventProcessor()
        self.container_client = ContainerClient()
        self.guids = []

    def tearDown(self):
        for guid in self.guids:
            self.container_client.delete_bagfile(str(guid))

    def test_process_with_deactivated_state(self):
        os.environ["processing_activated"] = "False"
        event = NewOnPremBagfileEvent(
            file_name="20190809.bag",
            file_path=
            "/default/containers/2019-08-19/20190809_112110_hks-small-loop1_ORIGINAL_test_001.bag"
        )
        guid = self.bagfile_event_processor.create_new_on_prem_bagfile(
            event)
        self.guids.append(guid)
        res = self.container_client.search_bagfiles({"guid": str(guid)})
        self.assertEqual(res[0]["processing_state"], "PROCESSING_DEACTIVATED")

    def test_process_with_activated_state_and_no_original(self):
        os.environ["processing_activated"] = "True"
        event = NewOnPremBagfileEvent(
            file_name="20190809.bag",
            file_path=
            "/default/containers/2019-08-19/20190809.bag"
        )
        guid = self.bagfile_event_processor.create_new_on_prem_bagfile(
            event)
        self.guids.append(guid)
        res = self.container_client.search_bagfiles({"guid": str(guid)})
        self.assertEqual(res[0]["processing_state"], "SKIPPED")

    def test_process_with_activated_state_and_original(self):
        os.environ["processing_activated"] = "True"
        event = NewOnPremBagfileEvent(
            file_name="20190809_ORIGINAL.bag",
            file_path=
            "/default/containers/2019-08-19/20190809_ORIGINAL.bagg"
        )
        guid = self.bagfile_event_processor.create_new_on_prem_bagfile(
            event)
        self.guids.append(guid)
        res = self.container_client.search_bagfiles({"guid": str(guid)})
        self.assertEqual(res[0]["processing_state"], "NOT_PROCESSED")

if __name__ == '__main__':
    unittest.main()
