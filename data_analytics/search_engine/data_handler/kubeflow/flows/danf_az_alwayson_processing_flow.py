import logging
from pathlib import PurePath, Path
from typing import Optional

from kfp import dsl

from config.config_handler import config_dict
from kubeflow.flows.common import get_mount_id, BaseFlow
from kubeflow.steps.cleanup import CleanupOp
from kubeflow.steps.danf_extractor import DanfExtractorInitOp, DanfExtractorOp
from kubeflow.steps.feature_extractor import FeatureExtractorOp
from model.paths import BlobStoragePath
from proxies.blob_storage import BlobStorageHandler

logger = logging.getLogger(__name__)


class DanfAzAlwaysonFlow(BaseFlow):

    NAME = "danf-az-alwayson-processing-flow"
    VERSION = "1.0"
    DESCRIPTION = "Azure flow for processing ALWAYSON bagfiles"
    IMAGE_PULL_SECRET = "dla-docker-pull-secret"
    OUTPUT_CONTAINER_NAME = "output"

    @classmethod
    def create(cls,
               bagfile: dict,
               flow_run_guid: str,
               input_location: BlobStoragePath,
               test_mode: bool):
        BlobStorageHandler()._ensure_container_exists(storage_account=input_location.storage_account,
                                                      container_name=cls.OUTPUT_CONTAINER_NAME)

        return cls._build_pipeline(
            bagfile['guid'],
            flow_run_guid,
            input_location,
            test_mode)

    @classmethod
    def _build_pipeline(cls,
                        bagfile_guid,
                        flow_run_guid,
                        input_location,
                        test_mode):
        @dsl.pipeline(
            name=cls.NAME,
            description=cls.DESCRIPTION
        )
        def processing_pipeline():
            logger.info(f"Processing DanfAzAlwaysonFlow for bagfile_guid {bagfile_guid}")
            mount_id = get_mount_id()

            cleanup_step = CleanupOp.create_for_azure(
                tasks=['set-sync-flag', 'update-flow-run'],
                bagfile_guid=bagfile_guid,
                flow_run_guid=flow_run_guid,
                bagfile_name=input_location.file,
                test_mode=test_mode
            )

            with dsl.ExitHandler(cleanup_step):
                # create metadata for each bagfile in the DB
                init_step = DanfExtractorInitOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=input_location,
                    output_container=cls.OUTPUT_CONTAINER_NAME,
                    mount_id=mount_id,
                    test_mode=test_mode
                )

                feature_extractor_step = FeatureExtractorOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=input_location,
                    output_container=cls.OUTPUT_CONTAINER_NAME,
                    mount_id=mount_id,
                    test_mode=test_mode
                ).after(init_step)

        return DanfAzAlwaysonFlow(processing_pipeline)

