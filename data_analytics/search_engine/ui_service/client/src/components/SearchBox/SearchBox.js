import React from 'react';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {isStringValid} from '../../utils/helperFunctions';
import './SearchBox.scss'

export function SearchBox(props) {

  function onSearchInputChange(event) {
    props.setCurrentSearchQuery(event.target.value);
    // Load new files when user delete the complete search input
    if (!isStringValid(event.target.value)) props.filterBySearchQuery();

  }

  function onSearchButtonClick() {
    if (isStringValid(props.searchQuery)) props.filterBySearchQuery(props.searchQuery);
    else props.filterBySearchQuery();
  }

  return (
      <div className="search-bar">
        <InputGroup>
          <FormControl
              placeholder={props.hint}
              aria-label="Search query"
              aria-describedby="basic-addon2"
              value={props.searchQuery}
              onChange={onSearchInputChange}
              onKeyDown={event => {
                if (event.key === 'Enter') onSearchButtonClick()
              }}
          />
          <InputGroup.Append>
            <Button onClick={onSearchButtonClick}>Search</Button>
          </InputGroup.Append>
        </InputGroup>
      </div>
  )
}