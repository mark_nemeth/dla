from bagfile import BagFile
from enum import IntEnum
import os
import datetime


class SysState():
    # AD System state is unknown: SEM is not yet initialized
    # (=default).
    kStateUnknown = 0
    # AD System is initializing
    kStateInit = 1
    # AD System is running
    kStateRun = 2
    # AD System is terminating
    kStateTerminate = 3
    # AD System is in an error state/degraded state (note: not yet implemented)
    kStateError = 4


class SysOpMode():
    # AD System opmode is unknown: SEM is not yet initialized
    # (=default).
    kOpUnknown = 0
    # AD System is initializing SADCs
    kOpSADCWakeup = 1
    # AD System is initializing PADCs
    kOpPADCWakeup = 2
    # AD System is calculating pose and horizon
    kOpReadyToDrive = 3
    # AD System is Autonomous Driving
    kOpAutonomousDriving = 4
    # AD System is in mode Remote Assistance request
    kOpRemoteAssistanceRequest = 5
    # AD System is in mode high-level Remote Assistance
    kOpRemoteAssistanceHigh = 6
    # AD System is in mode mid-level Remote Assistance
    kOpRemoteAssistanceMid = 7
    # AD System is in mode low-level Remote Assistance
    kOpRemoteAssistanceLow = 8
    # AD System is in mode Remote Assistance communication
    kOpRemoteAssistanceCom = 9
    # AD System is in mode Remote Assistance completed
    kOpRemoteAssistanceComplete = 10
    # AD System is in Safety Driver mode
    kOpSafetyDriver = 11
    # AD System is in Shutting Down mode
    kOpShutDown = 12
    # AD System is in Degraded mode
    kOpDegraded = 13


class SysDegradation():
    # AD System degradation is unknown: SEM is not yet initialized
    # (=default).
    kDegUnknown = 0
    # AD System not yet ready (added @03.2018 for engagement handling)
    kDegNotEngagementReady = 1
    # AD System has no degradation
    kDegNone = 2
    # AD System is in Remote Assistance: The AD system requires
    # remote assistance in order to continue operation
    kDegRemoteAssistance = 3
    # AD System is in Emergency Stop
    kDegEmergencyStop = 4
    # AD System is in degradation level 1
    kDegLevel1 = 5
    # AD System is in degradation level 2
    kDegLevel2 = 6


class ADEngagementState():
    # unkown
    unknown = -1
    # initializing
    initializing = 0
    # ready to engage
    ready = 1
    # engaged. System is in the autonomous mode
    engaged = 2
    # system error 
    error = 3


class DisengagementStatistics(object):
    def __init__(self, path):
        self.df_dis = None

        # use try except to check for topic (new error class?: missing topic)
        # even if log missing look at file nonetheless
        self.relevant_topics = {
            'ego_state': '/CalService/instance_0/EgoVehStateInterface',
            'log_request': '/LoggingLogRequestService/instance_0/LogRequestInterface',
            'sem_state': '/SemSystemStatusService/instance_0/SemSystemStatusInterface'
        }
        self.bag = BagFile(path)
        self.path = path
        self.get_topic_data()

    def get_topic_data(self):
        if (not self.bag.topic_in_bagfile(self.relevant_topics['ego_state'])) and \
                (not self.bag.topic_in_bagfile(self.relevant_topics['sem_state'])):
            raise ValueError('Neither topic {EGO} nor topic {SEM} are in bagfile.'.format(
                EGO=self.relevant_topics['ego_state'],
                SEM=self.relevant_topics['sem_state']))

        if self.bag.topic_in_bagfile(self.relevant_topics['ego_state']):
            self.df_ego = self.bag.topic_to_pandas(self.relevant_topics['ego_state'])
        if self.bag.topic_in_bagfile(self.relevant_topics['log_request']):
            self.df_log = self.bag.topic_to_pandas(self.relevant_topics['log_request'])
        if self.bag.topic_in_bagfile(self.relevant_topics['sem_state']):
            self.df_sem = self.bag.topic_to_pandas(self.relevant_topics['sem_state'])

    def ego(self):
        """Return data frame with timestamps of all disengagements and ad_engagement_state.

        Definition of disengagement: ad_engagement_state transitions from 2 to any other state (0, 1, 3).
        Timestamp and ad_engagement_state refer to the first event after the actual disengagement.
        """
        return BagFile.get_timestamps_for_topic_value_change(self.df_ego, 'ad_engagement_state',
                                                             [ADEngagementState.engaged],
                                                             [ADEngagementState.unknown, ADEngagementState.initializing,
                                                              ADEngagementState.ready, ADEngagementState.error])

    def log(self):
        if not hasattr(self, 'df_log'):
            return None
        return self.df_log[['ts', 'cause', 'source', 'details']]

    def sem(self):
        df = self.df_sem
        df['ad_engagement_state'] = df.apply(lambda row:
                                             DisengagementStatistics.toADEngagementState(
                                                 row['eSystemStatus.eSystemState'],
                                                 row['eSystemStatus.eDegradationLevel'],
                                                 row['eSystemStatus.eOperationMode']), axis=1)
        disengagements = BagFile.get_timestamps_for_topic_value(df, 'ad_engagement_state', ADEngagementState.engaged,
                                                                ['eSystemStatus.eDegradationLevel',
                                                                 'eSystemStatus.eOperationMode',
                                                                 'eSystemStatus.eSystemState'])
        return disengagements

    def disengagements_ego(self):
        df = self.ego()
        if df is None:
            return None
        log = self.log()
        if log is not None:
            df = BagFile.join_data_frames(df, log, tolerance='0.02s')
        df['filename'] = os.path.basename(self.path)
        return df

    def disengagements_sem(self):
        sem = self.sem()
        if sem is None:
            return None
        sem['filename'] = os.path.basename(self.path)
        return sem

    def get_engaged_windows(self,window_length_ms=10000, window_overlap_ms=5000,skip_last_ms=10000):
        """Extract all windows in which the ad_engagement_state is constantly engaged.

            Args:
            window_length -- length of the windows that should be extracted in ms
            overlap -- overlap between the windows in ms
            skip_last_ms -- defines how many ms should be ignored at the end of an engaged interval

            Returns:
                timestamp tuple (start,end)
        """
        # extract engage timestamps
        eng_start = BagFile.get_timestamps_for_topic_value_change(self.df_ego,
                                                                  'ad_engagement_state',
                                                                  [ADEngagementState.unknown,
                                                                   ADEngagementState.initializing,
                                                                   ADEngagementState.ready, ADEngagementState.error],
                                                                  [ADEngagementState.engaged])
        # extract disengage timestamps
        eng_stop = BagFile.get_timestamps_for_topic_value_change(self.df_ego,
                                                                 'ad_engagement_state', [ADEngagementState.engaged],
                                                                 [ADEngagementState.unknown,
                                                                  ADEngagementState.initializing,
                                                                  ADEngagementState.ready, ADEngagementState.error])

        # create engaged intervals
        engaged_intervals = eng_start['ts'].apply(self.match_disengagement_timestamps, args=(eng_stop,))
        engaged_window_timestamps = []
        # extract window timestamps
        for interval in engaged_intervals:
            engaged_window_timestamps.extend(
                BagFile.extract_windows_from_interval(interval, window_length_ms, window_overlap_ms, skip_last_ms))

        # assign window labels to rows (discard all other rows)
        return BagFile.assign_window_labels(self.df_ego,engaged_window_timestamps).copy()


    def get_disengaged_windows(self,window_length_ms=10000):
        """Extract windows before disengagements.

            Args:
            window_length -- length of the windows that should be extracted in ms
            Returns:
                timestamp tuple (start,end)
        """
        # extract disengage timestamps
        eng_stop = BagFile.get_timestamps_for_topic_value_change(self.df_ego,
                                                                 'ad_engagement_state', [ADEngagementState.engaged],
                                                                 [ADEngagementState.unknown,
                                                                  ADEngagementState.initializing,
                                                                  ADEngagementState.ready, ADEngagementState.error])
            # create disengagement window timestamps
        disengaged_window_timestamps = [(x - datetime.timedelta(milliseconds=window_length_ms), x) for x in
                                        eng_stop['ts']]
        # assign window labels to rows (discard all other rows)
        return BagFile.assign_window_labels(self.df_ego, disengaged_window_timestamps).copy()

    @staticmethod
    def toADEngagementState(eSystemState, eDegradation, eOperationMode):
        """Convert SEM state to Ego-Vehicle state.
    
        SEM state is defined by (composite state):
         - SysState
         - SysDegradation
         - SysOpMode
        Ego-Vehicle state is defined by:
         - ADEngagementState
        """
        if eSystemState == SysState.kStateInit and \
                eDegradation == SysDegradation.kDegNotEngagementReady and \
                eOperationMode in (SysOpMode.kOpSADCWakeup, SysOpMode.kOpPADCWakeup, SysOpMode.kOpReadyToDrive):
            return ADEngagementState.initializing
        if eSystemState == SysState.kStateRun:
            if eDegradation == SysDegradation.kDegNone:
                if eOperationMode == SysOpMode.kOpSafetyDriver:
                    return ADEngagementState.ready
                elif eOperationMode == SysOpMode.kOpAutonomousDriving:
                    return ADEngagementState.engaged
            if eDegradation in (SysDegradation.kDegNotEngagementReady, SysDegradation.kDegEmergencyStop) and \
                    eOperationMode == SysOpMode.kOpSafetyDriver:
                return ADEngagementState.error
        if eDegradation == SysDegradation.kDegUnknown:
            return ADEngagementState.unknown
        raise ValueError("State not defined "
                         "eSystemState={state} eDegradation={deg} eOperationMode={op}.".format(
            state=eSystemState, deg=eDegradation, op=eOperationMode))

    @staticmethod
    def match_disengagement_timestamps(ts_start, df_stop):
        """Find the first matching timestamp in a list of timestamps.

            Args:
            ts_start -- start timestamp
            df_stop -- pandas series of timestamps

            Returns:
                timestamp tuple (start,end)
        """
        matches = df_stop[df_stop['ts'].gt(ts_start)]
        if matches.empty:
            return None
        return ts_start, matches['ts'].iloc[0]