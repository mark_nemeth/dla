__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.utils import generate_guid

from handlers.db_data_handler import DB_Data_Handler
from model.flows import Flow, CreateFlowRequest


class FlowHandler:
    """Implements the Flow API business logic with model.flows.Flow objects
    at its core. A Flow represents a conceptual data processing pipeline (i.e.
    kubeflow pipeline). Flows are designed to be immutable."""

    def __init__(self):
        self.db_handler = DB_Data_Handler()

    def create_flow(self, req: CreateFlowRequest) -> Flow:
        """Creates and persists a new flow instance.

        :param req: Input used to create flow
        :return: The created flow instance
        """
        flow = Flow(
            **req.dict(),
            guid=generate_guid()
        )
        self.db_handler.add_flows([flow])
        return flow

    def get_flow(self, guid: str) -> Flow:
        """Retrieves and returns the flow identified by the given guid

        :param guid: guid of the flow to retrieve
        :return: the flow
        """
        flows = self.db_handler.get_flows([guid])
        return flows[0]
