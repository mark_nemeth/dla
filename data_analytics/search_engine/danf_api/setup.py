"""Package setup."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import sys
from pathlib import Path

from setuptools import setup, find_packages

import danf_api

MIN_PY_VERSION = (3, 8)
if sys.version_info < MIN_PY_VERSION:
    sys.exit(f"ERROR: Minimum required python version is {MIN_PY_VERSION}")

PACKAGE_ROOT = Path(__file__).parent
p = PACKAGE_ROOT / "requirements.txt"
with p.open() as f:
    requirements = f.read().splitlines()

setup(
    name=danf_api.__name__,
    version=danf_api.__version__,
    packages=find_packages(),
    test_suite="tests",
    install_requires=requirements,
)
