export function updatePrivTripFilterState(state, reportType, location) {
    // determines whether the ign_priv filter should be enabled/selected
    resetFilters(state);
    if (reportType === 'rp') {
        // rp report requires public and private roads
        state.reporting.tripFilters[3].selected = false;
        state.reporting.tripFilters[3].enabled = false;
    } else {
        if (!['any', 'us', 'ger'].includes(location)) {
            // locations for which ign_priv cannot be changed by the user
            state.reporting.tripFilters[3].enabled = false;
            if (location !== 'sjpilot') {
                state.reporting.tripFilters[3].selected = false;
            }
        }
    }
}

export function resetFilters(state) {
    for (let i = 0; i < state.reporting.tripFilters.length; i++) {
        state.reporting.tripFilters[i].selected = true;
        state.reporting.tripFilters[i].enabled = true;
    }
}