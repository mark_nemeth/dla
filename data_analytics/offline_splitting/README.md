# Offline Splitting 0.0.1


## Prerequisites
Build the offline_extractor docker image
```console
docker build --network host -t offline_extractor:latest offline_extractor/
```

Build the tmt docker image located in https://athena.daimler.com/bitbucket/projects/ATTDATA/repos/dla_repo/browse/data_logging/


## Offline splitting wrapper script
This script will traverse the provided input folder recursively and for each bagfile:
 - extract relevant events (e.g. disengagements)
 - strip a fixed length window arround each event
 - store the stripped data as a new bagfile in the provided output folder.
  
```console
python split_bagfiles.py -i /lhome/rbaldau/data/input -o /lhome/rbaldau/data/output
```


