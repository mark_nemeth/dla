#!/bin/sh
#This script writes the bandwith measured  to a file
#it checks te nic satistics recievec bytes evere seconds and approximates the
# bandwith in bytes / second
T=$1
INTERFACE=$2
FILENAME="bandwidthethtool_${INTERFACE}_$T.log"


old=$(ethtool -S $INTERFACE | grep rx_bytes: | cut -d":" -f2  || sed 's/ //g');
while $(sleep 1);
do
now=$(ethtool -S $INTERFACE | grep rx_bytes: | cut -d":" -f2  || sed 's/ //g')	;
echo $((($now-$old))) >> $FILENAME;
old=$now;
done
