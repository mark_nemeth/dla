import os
import unittest

from ContainerClient.container_client import ContainerClient
from DANFUtils.utils import generate_bag_guid
from handlers.file_reprocessing import FileReprocessingHandler
from proxies.metadata_api import MetadataApiProxy
from proxies.search_api import SearchApiProxy

RAW_CHILDBAG_PATH = 'childbag/test/1/20200311_213450_qg3_smoke_test_one_button_start_0_ORIGINAL.bag'
RAW_CHILDBAG_PATH_2 = 'childbag/test/2/another.bag'
REALISTIC_TEST_PATH = '/mapr/624.mbc.us/data/input/rd/athena/08_robotaxi/plog/byName/v222-5469/2020/03/11/silent_testing_snippets/childbag_processing_test.bag'
TEST_GUID = 'c52adb2f-bcbe-4554-815b-d3573fd77b14'


class ChildbagfileProcessingUnittest(unittest.TestCase):
    def setUp(self):
        # Requirement - we should run metadata and search API locally
        os.environ['METADATA_API_BASE_URL'] = 'http://localhost:8887/'
        os.environ['SEARCH_API_BASE_URL'] = 'http://localhost:8889/'
        os.environ['DANF_ENV'] = 'local'
        self.clear_db()

    def tearDown(self):
        self.clear_db()

    @staticmethod
    def clear_db():
        search_proxy = SearchApiProxy()
        metadata_proxy = MetadataApiProxy()
        for path in [RAW_CHILDBAG_PATH, RAW_CHILDBAG_PATH_2, REALISTIC_TEST_PATH]:
            guid = generate_bag_guid(path)
            childbag = search_proxy.get_childbagfile(guid)
            soft_deleted_childbag = search_proxy.get_childbagfile(guid, is_deleted=True)
            if not childbag and not soft_deleted_childbag:
                continue
            metadata_proxy.delete_childbagfile(guid, hard_delete=True)

    def test_childbagfile_soft_deletion(self):
        metadata_proxy = MetadataApiProxy()
        search_proxy = SearchApiProxy()

        guid = self.insert_test_childbagfile_to_db(metadata_proxy)
        childbag = search_proxy.get_childbagfile(guid)
        deleted_childbag = dict(childbag)
        deleted_childbag['is_deleted'] = True

        # Soft delete
        metadata_proxy.delete_childbagfile(guid=guid)
        actual_deleted_childbag = search_proxy.get_childbagfile(guid, is_deleted=True)

        self.assertDictEqual(actual_deleted_childbag, deleted_childbag)

    def test_childbagfile_cascading_soft_deletion(self):
        metadata_proxy = MetadataApiProxy()
        search_proxy = SearchApiProxy()
        cc = ContainerClient()

        bag_guid = metadata_proxy.upsert_bagfile(
            file_guid=TEST_GUID,
            file_name='name',
            file_path=RAW_CHILDBAG_PATH_2,
            file_type='ALWAYSON',
            processing_state='SUCCESSFUL',
            drive_types=[],
            processing_priority=1
        )
        metadata_proxy = MetadataApiProxy()
        child_guid = self.insert_test_childbagfile_to_db(metadata_proxy, RAW_CHILDBAG_PATH)

        cc.update_bagfile(bag_guid, {'childbagfiles': [child_guid]})
        cc = ContainerClient()
        cc.update_childbagfile(child_guid, {'parent_guid': bag_guid})

        metadata_proxy.delete_bagfile_and_orphans(bag_guid)
        self.assertIsNotNone(search_proxy.get_bagfile(bag_guid, is_deleted=True))
        self.assertIsNotNone(search_proxy.get_childbagfile(child_guid, is_deleted=True))

    def test_update_retriggered_childbagfile(self):
        metadata_proxy = MetadataApiProxy()
        search_proxy = SearchApiProxy()

        # Insert
        guid = self.insert_test_childbagfile_to_db(metadata_proxy, REALISTIC_TEST_PATH)

        # Soft delete
        cc = ContainerClient()
        cc.update_childbagfile(guid, {'is_deleted': True})
        flagged_childbag = search_proxy.get_childbagfile(guid, is_deleted=True)
        assert flagged_childbag is not None

        # Create / Update
        metadata_proxy = MetadataApiProxy()
        metadata_proxy.upsert_childbagfile(
            file_name='updated.bag',
            link=REALISTIC_TEST_PATH,
            processing_state='BEING_PROCESSED',
        )
        expected = dict(flagged_childbag)
        expected['is_deleted'] = False
        expected['file_name'] = 'updated.bag'
        expected['processing_state'] = 'BEING_PROCESSED'
        updated_childbag = search_proxy.get_childbagfile(guid)
        self.maxDiff = None
        self.assertDictEqual(updated_childbag, expected)

    @staticmethod
    def insert_test_childbagfile_to_db(metadata_proxy, path=RAW_CHILDBAG_PATH):
        childbag_guid = metadata_proxy.upsert_childbagfile(
            file_name='20200311_213450_qg3_smoke_test_one_button_start_0_ORIGINAL.bag',
            link=path,
            processing_state='SUCCESSFUL',
        )
        return childbag_guid
