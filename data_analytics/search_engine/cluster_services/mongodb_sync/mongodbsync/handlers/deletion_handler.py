__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from ContainerClient import ContainerClient
from DANFUtils.logging import logger
from DANFUtils.exceptions import RESTException
from DANFUtils.constants import *
from ..config.config_handler import config_dict
import itertools


class DeletionHandler:

    def __init__(self):
        self.on_prem_client = ContainerClient()
        self.azure_client = ContainerClient()
        self.azure_client.metadata_handler.BAGFILE_URL = config_dict.get_value(
            "AZURE_METADATA_BAGFILE_URL")

    def sync_deleted_bagfiles_to_azure(self):
        deleted_bagfiles = self.on_prem_client.get_bagfile_guids_to_delete()
        logger.info(f"Received Bagfile list: {deleted_bagfiles} to delete on Azure")
        for guid in itertools.islice(deleted_bagfiles, 0, config_dict.get_value("delete_batch_size")):
            self._delete_bagfile_metadata_in_azure(guid)
            self._remove_flag(guid)

    def _delete_bagfile_metadata_in_azure(self, guid):
        try:
            self.azure_client.delete_bagfile(guid, ["childbagfiles", "extractor_requests"])
            logger.info(f"Deleted bagfile with GUID: {guid} on Azure")
        except RESTException as e:
            if e.status_code != REQ_STATUS_CODE_NOT_FOUND:
                raise RuntimeError(f"Bagfile with GUID: {guid}, could not be deleted in Azure.") from e
            logger.warning(f"404 error. Bagfile with GUID: {guid} was not found, could not be deleted in Azure.")

    def _remove_flag(self, guid):
        # Failing while removing the delete flag as well as deleting the actual data has to end up
        # in an exception to not getting out of sync.
        try:
            self.on_prem_client.remove_delete_flag()
        except RESTException as e:
            raise RuntimeError(f"Delete flag could not be removed for bagfile guid {guid}") from e
