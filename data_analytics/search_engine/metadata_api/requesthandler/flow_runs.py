"""Metadata REST API flow runs route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from handlers.auth import auth
from handlers.flow_runs import FlowRunHandler
from handlers.utils import utils
from model.flow_runs import CreateFlowRunRequest, FlowRunStepRun, \
    UpdateFlowRunRequest
from requesthandler.base_request_handler import BaseRequestHandler


class FlowRunsRequestHandler(BaseRequestHandler):

    def initialize(self):
        self.flow_run_handler = FlowRunHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('flow-runs/post',
                                      context_headers=self.request.headers)

        create_flow_req = self.deserialize(self.request.body,
                                           CreateFlowRunRequest)

        new_flow_run = self.flow_run_handler.create_flow_run(create_flow_req)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=[new_flow_run.dict()],
            status_code=constants['REQ_STATUS_CODE_RESOURCE_CREATED'],
            location=f"/flow-runs/{new_flow_run.guid}")


class FlowRunRequestHandler(BaseRequestHandler):

    _schema_guid = BaseRequestHandler.load_json_schema("guid.schema")

    def initialize(self):
        self.flow_run_handler = FlowRunHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET, PATCH')

    @auth.requires_entitlements(["openid"])
    def get(self, guid):
        tracing.start_task_in_context('flow-runs/{guid}/get',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        flow_run = self.flow_run_handler.get_flow_run(guid)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=[flow_run.dict()],
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def patch(self, guid):
        tracing.start_task_in_context('flow-runs/{guid}/patch',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        update_req = self.deserialize(self.request.body,
                                      UpdateFlowRunRequest)

        self.flow_run_handler.update_flow_run(guid, update_req)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=None,
                             status_code=constants["REQ_STATUS_CODE_OK"])


class FlowRunStepRunsRequestHandler(BaseRequestHandler):

    _schema_guid = BaseRequestHandler.load_json_schema("guid.schema")

    def initialize(self):
        self.flow_run_handler = FlowRunHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self, guid, name):
        tracing.start_task_in_context(
            'flow-runs/{guid}/steps/{name}/runs/post',
            context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        step_run = self.deserialize(self.request.body,
                                    FlowRunStepRun)

        self.flow_run_handler.add_flow_run_step_run(guid, name, step_run)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=None,
                             status_code=constants['REQ_STATUS_CODE_RESOURCE_CREATED'],
                             location=f"/flow-runs/{guid}")
