# DANF - Kubernetes Garbage Collector (kubegc)

The DANF - Kubernetes Garbage Collector removes every pod and workflow which did exit successfully. This is necessary since Kubernetes does not delete finished resources on its own to free up space again.

The basic logic is to periodically poll the kubeAPI using Kubernetes CronJob to check for the amount of succeeded pods / workflows and delete them as soon as they reached a resource specific threshold set in the configuration.

> Current version: 0.1.4, please replace the ${version} below

## How to deploy the Kubernetes Garbage Collector in Kubernetes

### Manually

1. build docker image:

    ```console
    docker build -t dlaregistry.azurecr.io/kubegc:${version} . --network=host --no-cache
    ```

2. push to the Azure Registry

    ```console
    az acr login --name dlaregistry docker push dlaregistry.azurecr.io/kubegc:${version}
    ```

3. connecting to aks and applying to Kubernetes

    ```console
    az aks get-credentials --resource-group ATTDATA --name ${dla-kubecluster-aks-dev dla-kubecluster-aks-prod}
    kubectl apply -f kubernetes/
    ```

4. start the dashboard to monitor

    ```console
    az aks browse --resource-group ATTDATA --name ${dla-kubecluster-aks-dev, dla-kubecluster-aks-prod}
    ```
