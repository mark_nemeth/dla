__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import pymongo
from locust import Locust


class PyMongoLocust(Locust):
    """Custom locust base user that can be used for performance
    testing of DBClient. The attribute self.client refers to a
    a DBConnected enabled for performance measurements.
    """

    port = None
    user = None
    key = None
    db_name = 'metadata'
    protocol = 'mongodb'

    def __init__(self, *args, **kwargs):
        super(PyMongoLocust, self).__init__(*args, **kwargs)
        self.uri = f"{self.protocol}://{self.user}:{self.key}@{self.host}:{self.port}/?ssl=true"
        self.mongo_client = pymongo.MongoClient(self.uri)
        self.client = self.mongo_client[self.db_name]
