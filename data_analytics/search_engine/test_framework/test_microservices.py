from apscheduler.schedulers.background import BackgroundScheduler

from scenario import *

logging.basicConfig(level=logging.INFO)
from multiprocessing import Process
from config.constants import DANF_VERSION_TAG


class TestMicroServices:

    def __init__(self):
        self.processes = []

    def long_running_process(self, tag):
        getattr(Scenario(), tag)(tag)

    def job(self):
        tags = config_dict.get_value('tags')
        scenarios = Scenario()
        for tag in tags:

            if tag == 'insert_bagfile':
                scenarios.insert_bagfile(tag)

            elif tag == 'extraction_request':
                scenarios.extraction_request(tag)

            elif tag == 'search_request':
                scenarios.search_request(tag)

            elif tag == 'test_full_flow_on_prem':
                p = Process(target=self.long_running_process, args=('test_full_flow_on_prem',))
                p.start()
                self.processes.append(p)

            elif tag == 'test_full_flow_on_cloud':
                p = Process(target=self.long_running_process, args=('test_full_flow_on_cloud',))
                p.start()
                self.processes.append(p)
            x = '___' * 50
            scenarios.result += x + '\n'

        logging.info(scenarios.result)
        logging.info('sending the notification to mattermost')
        webhook_url = config_dict.get_value("webhook_url")
        logging.info("webhook url: {}".format(webhook_url))
        requests.post(webhook_url, json={'text': scenarios.result})

        for p in self.processes:
            p.join()


if __name__ == '__main__':
    freq_seconds = int(sys.argv[1])
    logging.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    logging.info('starting the scheduler')
    test_case = TestMicroServices()
    scheduler = BackgroundScheduler()
    scheduler.configure()
    scheduler.add_job(test_case.job, 'interval', seconds=freq_seconds)

    # Copied from https://stackoverflow.com/revisions/47569535/1
    # Under CC-BY-3.0 License - START
    scheduler.start()
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    try:
        # This is here to simulate application activity (which keeps the main thread alive).
        while True:
            time.sleep(5)
    except (KeyboardInterrupt, SystemExit):
        # Not strictly necessary if daemonic mode is enabled but should be done if possible
        scheduler.shutdown()
    # Under CC-BY-3.0 License - END
