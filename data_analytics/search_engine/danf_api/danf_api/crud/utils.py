"""Various general utilities for the crud operations."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import datetime as dt


def convert_to_milli_timestamp(value: str) -> int:
    """Convert the input to a milliseconds timestamp.

    Args:
        value: value to be converted, should be a string representing an int OR a date-time in ISO format

    Returns:
        the input value converted to milliseconds timestamp

    Raises:
        ValueError: if the input could not be converted because it doesn't match the required format
    """
    try:
        return int(float(value))
    except ValueError:
        pass
    d = dt.datetime.fromisoformat(value)
    if d.tzinfo is None:
        d = d.replace(tzinfo=dt.timezone.utc)
    return int(d.timestamp() * 1000)
