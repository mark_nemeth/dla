__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import logging
import traceback

import DANFUtils.utils
import tornado.web
from DANFUtils.constants import constants
from DANFUtils.exceptions import JSONValidationError
from DANFUtils.tracing import tracing
from pydantic import ValidationError, BaseModel, parse_obj_as

from config.config_handler import config_dict
from handlers.accounts import AccountHandler
from handlers.utils import utils
from model.exceptions import DANFApiError, RequestNotValidError
from model.users import User

logger = logging.getLogger(__name__)

debug = 'debug' == config_dict.get_value('log_level')


class BaseRequestHandler(tornado.web.RequestHandler):
    """
    Base request handler to introduce a common set of default values /
    methods / functionality which can be overridden by the specific
    request handlers
    """

    _account_handler = AccountHandler()

    def prepare(self):
        """
        Method to perform common initialization regardless of the
        request method.

        :return: None
        """
        logger.info(f"Incoming request: {self.request}, "
                    f"body={self.request.body.decode('utf-8')}")

    def set_default_headers(self):
        """
        Method to configure default headers for responses

        :return: None
        """
        self.set_header("access-control-allow-origin", "*")
        self.set_header(
            "Access-Control-Allow-Headers",
            "x-requested-with,access-control-allow-origin,authorization,content-type"
        )

    def options(self, *args):
        utils.build_response(response=self,
                             err=None,
                             results=None,
                             status_code=constants["REQ_STATUS_CODE_OK"])

    def get_current_user(self):
        """
        Returns a representation of the user that is performing the current
        request.

        This method will be called to populate the current_user property.
        It is only executed when the property is accessed and the result is
        cached. For more details, see definition in the super class.
        """
        account_key = self.request.headers.get('X-account-key')
        account = (self._account_handler.get_by_key(account_key)
                   if account_key
                   else None)
        return User(account=account)

    def log_exception(self, typ, value, tb):
        """
        Automatic logging of uncaught exceptions

        :return None
        """
        prefix = (f"API Error {value.status_code}"
                  if isinstance(value, DANFApiError)
                  else "Uncaught exception")
        logger.exception(f"{prefix} ({typ.__name__}): {value}")

    def write_error(self, status_code, **kwargs):
        """
        Automatic generation of error response for uncaught exceptions.

        :return: None
        """
        exc_info = kwargs.get('exc_info')

        if exc_info and hasattr(exc_info[1], 'status_code'):
            status_code = exc_info[1].status_code

        error_json = {
            'code': status_code,
            'reason': self._reason
        }

        tracing_attributes = dict()

        if exc_info:
            error_json['error_message'] = str(exc_info[1])

            trace = "".join(traceback.format_exception(*kwargs['exc_info']))
            tracing_attributes['fatalError'] = trace

            if debug:
                error_json['traceback'] = trace

        tracing.end_all_tasks(attributes=tracing_attributes)
        utils.build_response(self,
                             err=error_json,
                             status_code=status_code)

    @staticmethod
    def load_json_schema(schema_name):
        """Loads and parses the specified json schema.
        First tries to retrieve schema location from config_dict with
        schema_name as key.
        If no such config value exists, searches for schema with file name
        schema_name in the default schema location.

        :param schema_name: config_dict key pointing to schema location or
            schema file name
        :return: dict representing the schema
        """
        schema_location = config_dict.get_value(schema_name,
                                                f"schema/{schema_name}")
        # load schema
        try:
            with open(schema_location) as f:
                schema = f.read()
        except (IOError, OSError):
            logger.exception(f"Could not read ${schema_name} schema")
            raise

        # parse schema
        try:
            return json.loads(schema)
        except ValueError:
            logger.exception(f"Bad schema: "
                             f"unable to parse ${schema_name} schema")
            raise

    @staticmethod
    def validate_json(json_string, schema):
        """Validates the specified json_string against the specified schema
        and deserializes it to a dict.

        :param json_string: JSON string to validate and deserialize
        :param schema: JSON schema to validate against
        :return: dict holding the parsed deserialized
        :raises
            RequestNotValidError: if json_string does not conform to schema
        """
        try:
            return DANFUtils.utils.validate_json(json_string, schema)
        except JSONValidationError as e:
            raise RequestNotValidError(str(e)) from e

    @staticmethod
    def deserialize(json_string, target_cls):
        """Deserializes the specified json_string to an instance of the
        specified target_cls.

        :param json_string: JSON string to deserialize
        :param target_cls: target class json_string should be deserialized to.
            Can be: a) a pydantic model extending BaseModel (e.g. MyModel) or
            b) a List type specialized on a pydantic model extending BaseModel
            (e.g. List[BaseModel]
        :return: instance of target_cls
        :raises
            RequestNotValidError: if json_string does not conform to schema
                of the target_cls
        """
        if isinstance(target_cls, BaseModel):
            try:
                return target_cls.parse_raw(json_string)
            except ValidationError as e:
                raise RequestNotValidError(str(e)) from e
        else:
            try:
                obj = json.loads(json_string)
                return parse_obj_as(target_cls, obj)
            except ValidationError as e:
                raise RequestNotValidError(str(e)) from e
