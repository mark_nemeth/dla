import React from 'react';
import './Table.scss';
import { Card, Row, Col } from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import TableField from '../TableField/TableField';


export function Table(props) {

  //region Table style
  const tableRowStripe = (row, rowIndex) => {
    const style = {};
    if (rowIndex % 2 !== 0) style.backgroundColor = '#fff';
    else style.backgroundColor = 'rgba(230, 230, 230, .25)';
    return style;
  };

  const formatColumns = (colLabels) => {
    return colLabels.map((colLabel, idx, arr) => {
      return {
        dataField: idx.toString(),
        text: colLabel,
        classes: 'table-name-column',
        formatter: (cell, row) => {
          return <TableField value={cell} />
        },
        headerStyle: () => {
          return {}
        }
      }
    })
  };

  const NoDataIndication = () => {
    return (
      <></>
    );
  };

  return (
    <div className="reporting-table-container">
      <Row className='mb-12'>
        <Col md={12} lg={12} xl={12} className="col-xxl-12 col-xxxl-12 col-xxxxl-12">
          <Card className='reporting-table-card'>
            <Card.Body>
              <Row className="table-row-style">
                <span className="table-caption">{props.caption}</span>
              </Row>
              <Row className="table-row-style table-responsive">
                <BootstrapTable
                  bootstrap4
                  // keyField='0'
                  keyField='0-1-2-3'
                  data={props.values}
                  columns={formatColumns(props.colLabels)}
                  noDataIndication={() => <NoDataIndication />}
                  condensed
                  striped
                  headerClasses="table-header-style"
                  rowClasses="table-row-style"
                  rowStyle={tableRowStripe}
                />
              </Row>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}