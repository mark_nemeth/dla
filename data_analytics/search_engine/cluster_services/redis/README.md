# DANF - Kubernetes Redis OnPrem Setup

This Kubernetes Deployment will deploy a single instance of a Redis cache to the cluster.

## Prerequisites
* Secrets to successfully run Redis in Kubernetes 
   ```
   apiVersion: v1
   kind: Secret
   metadata:
     name: redis-ui-credentials
   type: Opaque
   stringData:
     REDIS_KEY: <insert your redis key>
   ```
   
## Setup

1. Connect to the `cluster` and `namespace where you want to deploy the Redis`
2. Create the secret `redis-ui-credentials` as already mentioned in the prerequisites.
3. Create the `service` and the `deployment`
   ```
   k apply -f redis-ui-deployment.yml
   k apply -f redis-ui-service.yml
   ```
   