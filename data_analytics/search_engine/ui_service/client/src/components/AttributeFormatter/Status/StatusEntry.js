import React from 'react';
import {defaultEmptyValue} from '../../Table';
import {statusConverter} from '../../../utils/valueConverter';
import {isStringValid} from '../../../utils/helperFunctions';
import './StatusEntry.scss'

export default function StatusEntry(props) {

  if (!isStringValid(props.status)) return <span>{defaultEmptyValue}</span>;

  const formattedStatus = statusConverter(props.status);

  return (
    <span className="state-entry">{formattedStatus}</span>
  )
}