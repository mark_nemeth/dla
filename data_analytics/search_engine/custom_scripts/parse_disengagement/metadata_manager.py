__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from ContainerClient import ContainerClient
from DANFUtils.utils import utils as danfutils
from config.constants import VERSION, EXTRACTOR_NAME


class MetadataManager:

    def __init__(self):
        self.client = ContainerClient()

    def submit_state_updates(self,
                             bagfile_guid,
                             flow_run_guid,
                             status,
                             started_at,
                             message=None):
        run_update, state_update = self._build_state_updates(
            status, started_at, message)

        self.client.add_flow_run_step_run(flow_run_guid,
                                          'danf-disengagement-report',
                                          run_update)

        # update legacy state API
        self.client.add_bagfile_state_event(bagfile_guid,
                                            state_update)

    def _build_state_updates(self, status, started_at, message=None):
        map_status_to_legacy_status = {
            'SUCCESSFUL': 'SUCCESS',
            'FAILED': 'FAILED'
        }

        now = danfutils.get_timestamp()
        report = {}
        if message:
            report['message'] = message

        flow_update = {
            'started_at': started_at,
            'completed_at': now,
            'status': status,
            'report': report
        }
        state_update = {
            'name': EXTRACTOR_NAME,
            'version': VERSION,
            'time': now,
            'status': map_status_to_legacy_status[status],
            'report': report
        }
        return flow_update, state_update
