__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import re
from typing import List
from config.config_handler import config_dict


def determine_drive_types(file_name: str) -> List[str]:
    """
    Determines the drive type (e.g. CID, CTD) of a bagfile based on the
    file name.

    :param file_name: Of the bagfile to classify
    :return: List of drive types
    """
    drive_type_patterns = config_dict.get_value('file_name_to_drive_type')
    drive_types = []
    for pattern in drive_type_patterns:
        if re.search(pattern, file_name):
            drive_type = drive_type_patterns[pattern]
            drive_types.append(drive_type)

    return drive_types