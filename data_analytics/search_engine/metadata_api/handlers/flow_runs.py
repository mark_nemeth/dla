__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List

from DANFUtils.utils import utils as danfutils

from handlers.db_data_handler import DB_Data_Handler
from model.exceptions import DANFApiError
from handlers.flows import FlowHandler
from model.flow_runs import (CreateFlowRunRequest, FlowRun, FlowRunStep,
                             FlowRunStepRun, UpdateFlowRunRequest)


class FlowRunHandler:
    """Implements the Flow Run API business logic with model.flow_runs.FlowRun
    objects at its core. A Flow Run represents a specific execution of a
    model.flows.Flow."""

    def __init__(self):
        self.db_handler = DB_Data_Handler()
        self.flow_handler = FlowHandler()

    def create_flow_run(self, req: CreateFlowRunRequest) -> FlowRun:
        """Creates and persists a new flow run instance. The new run will
        be in status IN_PROGRESS representing a currently executed flow.

        :param req: Input used to create flow
        :return: The created flow run instance
        """
        run = FlowRun(
            **req.dict(exclude={'steps'}),
            guid=danfutils.generate_guid(),
            started_at=danfutils.get_timestamp(),
            status='IN_PROGRESS',
            steps=self._generate_steps_list(req.steps)
        )

        self.db_handler.add_flow_runs([run])
        return run

    def update_flow_run(self,
                        flow_run_guid: str,
                        req: UpdateFlowRunRequest) -> None:
        """Updates a existing flow run instance. Only very limited updates are
        allowed.

        Setting the completed_at field to a different value than the initial
        None will transfer a IN_PROGRESS run to a completed run. Terminal
        status values of the run and its individual steps are automatically
        computed and updated.

        :param flow_run_guid: identifies the flow run that should be updated
        :param req: fields that should be updated
        """
        field_updates = req.dict(exclude_unset=True)

        # if completed_at is set, complete flow and determine final status
        if req.completed_at:
            flow = self.get_flow_run(flow_run_guid)
            self._complete_flow_run(flow)
            field_updates['status'] = flow.status
            for i, step in enumerate(flow.steps):
                field_updates[f'steps.{i}.status'] = step.status

        self.db_handler.update_flow_runs([flow_run_guid], [field_updates])

    def get_flow_run(self, guid: str) -> FlowRun:
        """Retrieves and returns the flow run identified by the given guid

        :param guid: guid of the flow run to retrieve
        :return: the flow run
        """
        flows = self.db_handler.get_flow_runs([guid])
        return flows[0]

    def add_flow_run_step_run(self,
                              flow_run_guid,
                              step_name,
                              step_run: FlowRunStepRun) -> None:
        """Adds a run of a individual step to the flow run object and persists
        the change.

        :param flow_run_guid: identifies the flow run to update
        :param step_name: identifies the step within the flow run to update
        :param step_run: data of the step run that should be added
        """
        step_index = self._get_index_of_step(flow_run_guid, step_name)
        self.db_handler.add_flow_run_step_run(
            flow_run_guid, step_index, step_run)

    @staticmethod
    def _complete_flow_run(flow_run: FlowRun):
        """Completes a IN_PROGRESS flow run. For this, all terminal status
        values (of the run and of individual steps) are computed and set.

        :raises
            DANFApiError: if run is already completed or computation of
                terminal status fails
        """
        if flow_run.status not in ('IN_PROGRESS', 'PENDING'):
            raise DANFApiError(409, f"could not complete flow run {flow_run}: "
                                    f"flow run already in terminal status")

        # compute terminal status of each step
        for step in flow_run.steps:
            if not step.runs:
                # no step run submitted (failure state)
                step.status = 'NOT_EXECUTED'
            elif any([run.status == 'SUCCESSFUL' for run in step.runs]):
                # if at least one step run was successful, step is successful
                step.status = 'SUCCESSFUL'
            elif all([run.status == 'SKIPPED' for run in step.runs]):
                # if step was skipped on purpose, step is successful
                step.status = 'SKIPPED'
            elif all([run.status == 'FAILED' for run in step.runs]):
                # if all step runs failed, step failed
                step.status = 'FAILED'
            else:
                raise DANFApiError(
                    500,
                    f"could not determine terminal status for step {step}"
                    f" of flow run {flow_run.guid}: unexpected "
                    f"combination of run statuses")

        # Compute terminal status of flow run based on status of steps.
        # FAILED or NOT_EXECUTED steps will fail the flow run. Only essential
        # steps are considered (steps without is_essential property are
        # considered as well for backwards compatibility).
        if any([
            step.status in ('FAILED', 'NOT_EXECUTED')
            for step in flow_run.steps
            if step.is_essential or step.is_essential is None
        ]):
            flow_run.status = 'FAILED'
        else:
            flow_run.status = 'SUCCESSFUL'

    @staticmethod
    def _generate_steps_list(
            req_steps: List[CreateFlowRunRequest.Step]) -> List[FlowRunStep]:
        steps = []
        for req_step in req_steps:
            steps.append(FlowRunStep(
                **req_step.dict(),
                status='PENDING')
            )
        return steps

    def _get_index_of_step(self, flow_run_guid, step_name):
        run = self.get_flow_run(flow_run_guid)

        step_index = -1
        for i, step in enumerate(run.steps):
            if step.name == step_name:
                step_index = i

        if step_index < 0:
            raise DANFApiError(
                404, f"flow run {run} has no step step '{step_name}'")

        return step_index


