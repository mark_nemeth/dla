#include <algorithm>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <sys/errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/resource.h>
#include <signal.h>
#include "common.h"
#include "aiostream.h"
#include "hps.h"

using namespace std;

#define RECV_BLOCK_SIZE		(1 * 1024 * 1024)
#define MIN_RECV_SIZE		(32 * 1024)
#define CORE_MANAGEMENT		(0)

#define AFFINITY_NOT_SET	(-2)
#define AFFINITY_OTHER		(-1)

struct hpTcpServerT
{
	hps_type type;
	const char *out;
	int port;
	int sock;
	pthread_t thread;
};

struct clientCtxT;

static volatile int clientId = 0;
static volatile int connections = 0;

struct clientCtxT
{
	int id;
	int sock;
	int core; // RX queue core
	int affinity;
	bool shutdown;
	hpStreamT *s;
	pthread_t thread;

	clientCtxT(int clientId, int clientSocket, const char *out)
	{
		int e;

		id = clientId;
		sock = clientSocket;
		affinity = AFFINITY_NOT_SET;
		shutdown = false;

		s = hps_open(out);
    	if (s == NULL)
    	{
    		perror("AIO stream failed to open file");
    		throw exception();
    	}

        // Get receive CPU
        socklen_t cpuSize = sizeof(core);
        if (getsockopt(clientSocket, SOL_SOCKET, SO_INCOMING_CPU, &core, &cpuSize))
        {
        	perror("Failed to get SO_INCOMING_CPU");
    		throw exception();
        }
        printf("Receive core: %d\n", core);

//	    int size = 0;
//	    socklen_t len = sizeof(int);
//	    // Get receive buffer size
//	    e = getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &size, &len);
//	    // Set receive buffer size
//	    int newSize = 1 * 1024 * 1024;
//	    e = setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &newSize, (socklen_t)sizeof(int));
//	    if (e != 0) {
//	    	perror("Failed to increase socket receive buffer");
//	    }
//	    // Get receive buffer size
//	    e = getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &newSize, &len);
//		printf("Receive buffer size: %d --> %d\n", size, newSize);

        // Set SO_RCVLOWAT
        int low = MIN_RECV_SIZE;
        socklen_t lowSize = sizeof(low);
        if (setsockopt(clientSocket, SOL_SOCKET, SO_RCVLOWAT, &low, lowSize))
        {
        	perror("Failed to set SO_RCVLOWAT");
    		throw exception();
        }

        connections++;

		pthread_create(&thread, NULL, handle, this);
	}

	~clientCtxT()
	{
		shutdown = true;
		pthread_join(thread, NULL);

	    // Close socket
		if (sock != -1)
		{
		    close(sock);
		    sock = -1;
		}

	    // Close stream
	    if (s != NULL)
	    {
	    	hps_close(s);
	    	s = NULL;
	    }

        connections--;
	}

#if CORE_MANAGEMENT
	void AutoAdjustAffinity()
	{
		if (connections <= 1)
		{
			// Only one connection
			if (affinity != AFFINITY_OTHER)
			{
				// Set thread affinity to any core except the RX queue core
				cpu_set_t cpuset;
				CPU_ZERO(&cpuset);
				int cores = sysconf(_SC_NPROCESSORS_ONLN);
				for (int c = 0; c < cores; c++)
					if (core != c)
						CPU_SET(c, &cpuset);
				pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset);
				printf("%d: RX core %d -> all cores except %d\n", id, core, core);
				affinity = AFFINITY_OTHER;
			}
		}
		else
		{
			// Multiple connections
			if (affinity != core)
			{
				// Set thread affinity to RX queue core
				cpu_set_t cpuset;
				CPU_ZERO(&cpuset);
				CPU_SET(core, &cpuset);
				pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset);
				printf("%d: RX core %d -> core %d\n", id, core, core);
				affinity = core;
			}
		}
	}
#endif

	/*
	 * This handler receives and stores the data.
	 */
	void record()
	{
	    while (!shutdown)
	    {
#if CORE_MANAGEMENT
	    	AutoAdjustAffinity();
#endif

			// Get as much space as is available in the buffer
			void *buffer;
			int space = hps_write_begin(s, &buffer, RECV_BLOCK_SIZE);
			if (space < 0)
			{
				perror("hps_write_begin() failed");
				break;
			}
//			else if (space <= MIN_RECV_SIZE)
//			{
//				usleep(1000);
//				continue;
//			}

			// Receive data
			int n = recv(sock, buffer, space, 0);
			if (0 < n)
			{
				__sync_add_and_fetch(&statistics.rxBytes, n);
				hps_write_end(s, n);
			}
			else if (n == 0)
			{
				printf("Client has shutdown the connection\n");
				break;
			}
			else if (errno == EWOULDBLOCK)
			{
				// No data
			}
			else
			{
				perror("recv() failed");
				break;
			}
	    }
	}

	static void *handle(void *arg)
	{
		clientCtxT* client = (clientCtxT*)arg;
		client->record();
		delete client;
		return NULL;
	}
};

static void *server(void *arg)
{
	hpTcpServerT *server = (hpTcpServerT*)arg;

	printf("TCP server listening on port %d\n", server->port);

	// Setup server socket
	server->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (server->sock == -1)
    {
    	perror("Failed to create handler socket");
    	return NULL;
    }

    // Bind & listen
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(server->port);
    if (bind(server->sock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)))
    {
		perror("Failed to bind socket");
		return NULL;
    }
	if (listen(server->sock, 10))
	{
		perror("Failed to listen socket");
		return NULL;
	}

	while (1)
	{
		// Accept client
		struct sockaddr_in clientAddr;
		int addrLen = sizeof(clientAddr);
		int clientSocket = accept(server->sock, (struct sockaddr *)&clientAddr, (socklen_t*)&addrLen);
		if (clientSocket < 0)
		{
			if (errno != EBADF)
				perror("Failed to accept client");
			break;
		}

		printf("%d: accepted client %s\n", clientId, inet_ntoa(clientAddr.sin_addr));

		// Start client handler
		clientCtxT *client = new clientCtxT(clientId, clientSocket, server->out);

		clientId++;
	}

    return NULL;
}

hpTcpServerT* hps_tcp_start_server(const char *name, int port, const char *out)
{
	hpTcpServerT *s = new hpTcpServerT();

	s->type = HPS_TCP;
	s->out = strdup(out);

	s->port = port;
	pthread_create(&s->thread, NULL, server, s);

    return s;
}

int hps_tcp_close_server(hpTcpServerT *server)
{
    void *rc;

    close(server->sock);

    pthread_kill(server->thread, SIGTERM);
    pthread_join(server->thread, &rc);

    delete server;

    return 0;
}
