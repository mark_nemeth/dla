import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../Table';
import MileDistanceEntry from './MileDistanceEntry';

describe('Bagfile Table MileDistanceEntry', () => {

  it('should format distance in meters to distance in miles with two decimal places', () => {
    const formattedResult = '7.46 Miles';
    const props = {distance_in_meter: 12000};
    const wrapper = shallow(<MileDistanceEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should create a disabled button when distance in meters does not exists', () => {
    const props = {duration: null};
    const wrapper = shallow(<MileDistanceEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});
