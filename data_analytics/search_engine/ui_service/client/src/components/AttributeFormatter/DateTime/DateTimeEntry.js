import React from 'react';
import {defaultEmptyValue} from '../../Table';
import {timestampConverter} from '../../../utils/valueConverter';
import {isNumberValid} from '../../../utils/helperFunctions';

export default function DateTimeEntry(props) {

  if (!isNumberValid(props.timestamp)) return <span>{defaultEmptyValue}</span>;

  const formattedDateTime = timestampConverter(props.timestamp);

  return (
    <span>{formattedDateTime}</span>
  )
}