__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from test.support import EnvironmentVarGuard
from unittest.mock import Mock

from kubeflow.flow_creation import FlowManager
from model.paths import BlobStoragePath


FAKE_FLOW_RUN = {'guid': '11111111-1111-1111-1111-111111111111'}


class TestBlobStoragePath(unittest.TestCase):

    def setUp(self):
        self.env = EnvironmentVarGuard()
        self.env.set('APPINSIGHTS_INSTRUMENTATIONKEY', 'APPINSIGHTS_DISABLE_KEY')
        self.flow_manager = FlowManager()
        self.mock_kf_client = Mock()
        self.mock_metadata_client = Mock()
        self.flow_manager.client = self.mock_kf_client
        self.flow_manager.metadata_api = self.mock_metadata_client
        self.mock_metadata_client.create_flow_run.return_value = FAKE_FLOW_RUN

    def test_launch_dml_codrive_processing_flow_should_launch_valid_flow(self):
        self.flow_manager.launch_dml_codrive_processing_flow(
            {
                'guid': '00000000-0000-0000-0000-000000000000',
                'link': '/dlastorage1sadev/2019-12-24/test_ORIGINAL.bag'
            },
            False)
        self.mock_kf_client.run_pipeline.assert_called_once()

    def test_launch_danf_azure_processing_flow_should_launch_valid_flow(self):
        self.flow_manager.launch_dml_codrive_processing_flow(
            {
                'guid': '00000000-0000-0000-0000-000000000000',
                'link': '/dlastorage1sadev/2019-12-24/test_ORIGINAL.bag'
            },
            False)
        self.mock_kf_client.run_pipeline.assert_called_once()

    def test_launch_danf_az_snippet_flow_should_launch_valid_flow_on_container(self):
        self.flow_manager.launch_danf_az_snippet_flow(
            {
                'guid': '00000000-0000-0000-0000-000000000000',
                'link': '/dlastorage1sadev/2019-12-24/test_ORIGINAL.bag'
            },
            BlobStoragePath("/storage/container/file.bag"),
            False)
        self.mock_kf_client.run_pipeline.assert_called_once()


if __name__ == '__main__':
    unittest.main()
