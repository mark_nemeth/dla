import {connect} from 'react-redux';
import {filterByVelocity, setCurrentVelocityRange} from '../../../../redux/Home/HomeActions';
import {ByVelocityFilter, STATIC_LIMITS} from './ByVelocityFilter';

function mapStateToProps(state, ownProps) {
  const limits = state.filterOptions.bagfileVelocityLimits ?
    {
      min: state.filterOptions.bagfileVelocityLimits[0],
      max: state.filterOptions.bagfileVelocityLimits[1]
    }
    : STATIC_LIMITS;
  
  const velocity = state.currentVelocityRange || {
    'min': limits.min,
    'max': limits.max
  };
  const steps = 0.1;
  return {velocity, limits, steps};
}

export default connect(
  mapStateToProps,
  {setCurrentVelocityRange, filterByVelocity}
)(ByVelocityFilter)