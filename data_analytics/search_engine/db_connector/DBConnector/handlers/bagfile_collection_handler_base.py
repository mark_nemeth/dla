"""Bagfile Collection Base Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


class BagfileCollectionBaseHandler:

    INDEX_FIELD_GUID = 'guid'
    INDEX_FIELD_START = 'start'

    def __init__(self, db, collection_name, guid_index_name, start_index_name):
        self.db = db
        self.col = self.db[collection_name]
        self.col.create_index(self.INDEX_FIELD_GUID,
                              unique=True,
                              name=guid_index_name)
        self.col.create_index(self.INDEX_FIELD_START,
                              unique=False,
                              name=start_index_name)
