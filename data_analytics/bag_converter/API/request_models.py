
__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from pydantic import BaseModel
from typing import Optional, List
from config import config_handler as configurator

class bag2img_request(BaseModel):
    bagfile: str
    topic: str
    real_time: Optional[int] = None
    relative_time: Optional[int] = None
    overlay: Optional[bool] = False
    overlay_id: Optional[int] = -1
    compression_quality: Optional[int] = configurator.get_value('jpeg_compression_quality_app')
    apply_time_correct: Optional[bool] = False


class bag2vid_request(BaseModel):
    bagfile: str
    topic: str
    start_time: Optional[int] = None
    end_time: Optional[int] = None
    start_relative: Optional[int] = None
    end_relative: Optional[int] = None
    overlay: Optional[bool] = False
    overlay_id: Optional[int] = -1
    compression_quality: Optional[int] = configurator.get_value('jpeg_compression_quality_app')
    apply_time_correct: Optional[bool] = False


class bag2slice_request(BaseModel):
    bagfile: str
    topic: str = None
    profile: str = None
    start_relative: int = 0
    end_relative: int = 2000
    start_time: int = None
    end_time: int = None
    apply_time_correct: bool = False


