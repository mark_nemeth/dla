import {connect} from 'react-redux';
import {IngestPage} from './IngestPage';
import {fetchUserData} from "../../redux/App/AppActions";
import {refresh} from "../../redux/Ingest/IngestActions";

function mapStateToProps(state, ownProps) {
  const latestIngests = state.latestIngests || [];
  const selectedCar = state.selectedCar || [];
  const error = state.error || false;
  const errorMessage = state.errorMessage || '';
  const showLoading = state.showLoading || false;

  return {latestIngests, selectedCar, error, errorMessage, showLoading};
}

export default connect(
    mapStateToProps,
    {
      refresh,
      fetchUserData
    }
)(IngestPage);
