__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from proxies.kubernetes import KubernetesApiProxy

logger = logging.getLogger(__name__)

kube_api = KubernetesApiProxy()

if __name__ == '__main__':
    logger.info("abount to update cluster with custom storage definition")
    kube_api.update_cluster_with_custom_storage_definition()
    logger.info("done...")
