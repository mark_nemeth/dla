"""Tag related operations."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import re
from uuid import UUID

from motor.motor_asyncio import AsyncIOMotorCollection

from ..core.errors import RHSColonOperatorError
from ..models.base import OpValue
from ..models.tag import TagFilterParams, Tag, TagQueryParams, ManyTags


async def tag_resource(collection: AsyncIOMotorCollection, guid: UUID, tag: Tag, delete: bool = False) -> None:
    """Add or remove a tag.

    Args:
        collection: mongodb collection
        guid: id of the resource to tag / untag
        tag: the tag to be added / removed
        delete: remove the tag instead of adding

    Raises:
        ValueError: if the tag to be removed was not found for an existing resource
        KeyError: if no resource was found for the given id
    """
    result = await collection.update_one(
        filter={"guid": str(guid)},
        update={"$pull" if delete else "$addToSet": {"tags": tag.dict()}},
    )
    if result.matched_count == 1:
        if delete and result.modified_count == 0:
            raise ValueError(f"No tag: ({tag}) was found for resource: {guid}")
        return

    raise KeyError(f"No resource was found with guid: {guid}")


async def find_tags(collection: AsyncIOMotorCollection, query_params: TagQueryParams) -> ManyTags:
    """Query tags.

    Args:
        collection: mongodb collection
        query_params: object holding the query settings

    Returns:
        object holding the matching tags and some extra information about the query as a whole
    """
    # Note: the /if t/ condition at the end is required to filter out None values that appear due to this bug:
    # https://jira.mongodb.org/browse/SERVER-14832
    tags = (Tag(**t) for t in await collection.distinct("tags") if t)

    # filter
    # +performance hack: compile the regex only once here
    value_filter = query_params.filter_params.value
    if value_filter and value_filter.op == "re":
        query_params.filter_params.value = OpValue(op="re", value=re.compile(value_filter.value))
    matching_tags = [t for t in tags if _tag_is_matching(tag=t, filter_params=query_params.filter_params)]

    # sort
    if query_params.order_by:
        matching_tags.sort(key=lambda tag: (tag.namespace, tag.value))

    # paginate
    end = None if query_params.limit == 0 else query_params.offset + query_params.limit
    queried_tags = matching_tags[query_params.offset:end]

    return ManyTags(tags=queried_tags, total_count=len(matching_tags))


def _tag_is_matching(tag: Tag, filter_params: TagFilterParams) -> bool:
    if filter_params.namespace and tag.namespace != filter_params.namespace:
        return False

    if filter_params.value:
        if filter_params.value.op in ["", "eq"]:
            return filter_params.value.value == tag.value
        if filter_params.value.op == "re":
            return bool(filter_params.value.value.match(tag.value))
        raise RHSColonOperatorError(op=filter_params.value.op, context="tag filter", valid_op=["", "eq", "re"])

    return True
