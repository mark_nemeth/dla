__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from locust import TaskSet, task, HttpLocust

from utils.test_data import create_bagfile, TestData, delete_resources
from config.config_handler import config_dict


metadata_base_url = config_dict.get_value('METADATA_API_BASE_URL')
search_base_url = config_dict.get_value('SEARCH_API_BASE_URL')


class ApiClientBehavior(TaskSet):

    def perform_extract_and_get_snippets(self, incidents, incident_duration):
        bagfile_guid = create_bagfile()
        extract_payload = TestData.extraction_payload(bagfile_guid, incidents, incident_duration)
        self.client.post(f"{metadata_base_url}extractorrequest/",
                         json=extract_payload,
                         name=f"extractor {incidents}x {incident_duration}s",
                         timeout=3600)

        snippet_payload = {'bagfile_guid': bagfile_guid, 'bva_limit': 3}
        self.client.post(f"{metadata_base_url}snippet/bva/",
                         json=snippet_payload,
                         name=f"snippets {incidents}x {incident_duration}s",
                         timeout=3600)

        delete_resources(bagfile_guid)

    # 5 incidents
    @task
    def task_5_incidents_3_seconds(self):
        self.perform_extract_and_get_snippets(5, 3)

    @task
    def task_5_incidents_10_seconds(self):
        self.perform_extract_and_get_snippets(5, 10)

    @task
    def task_5_incidents_30_seconds(self):
        self.perform_extract_and_get_snippets(5, 30)

    @task
    def task_5_incidents_60_seconds(self):
        self.perform_extract_and_get_snippets(5, 60)

    @task
    def task_5_incidents_150_seconds(self):
        self.perform_extract_and_get_snippets(5, 150)

    @task
    def task_5_incidents_300_seconds(self):
        self.perform_extract_and_get_snippets(5, 300)

    # 10 incidents
    @task
    def task_10_incidents_3_seconds(self):
        self.perform_extract_and_get_snippets(10, 3)

    @task
    def task_10_incidents_10_seconds(self):
        self.perform_extract_and_get_snippets(10, 10)

    @task
    def task_10_incidents_30_seconds(self):
        self.perform_extract_and_get_snippets(10, 30)

    @task
    def task_10_incidents_60_seconds(self):
        self.perform_extract_and_get_snippets(10, 60)

    @task
    def task_10_incidents_150_seconds(self):
        self.perform_extract_and_get_snippets(10, 150)

    @task
    def task_10_incidents_300_seconds(self):
        self.perform_extract_and_get_snippets(10, 300)

    # 20 incidents
    @task
    def task_20_incidents_3_seconds(self):
        self.perform_extract_and_get_snippets(20, 3)

    @task
    def task_20_incidents_10_seconds(self):
        self.perform_extract_and_get_snippets(20, 10)

    @task
    def task_20_incidents_30_seconds(self):
        self.perform_extract_and_get_snippets(20, 30)

    @task
    def task_20_incidents_60_seconds(self):
        self.perform_extract_and_get_snippets(20, 60)

    @task
    def task_20_incidents_150_seconds(self):
        self.perform_extract_and_get_snippets(20, 150)

    @task
    def task_20_incidents_300_seconds(self):
        self.perform_extract_and_get_snippets(20, 300)

    # 50 incidents
    @task
    def task_50_incidents_3_seconds(self):
        self.perform_extract_and_get_snippets(50, 3)

    @task
    def task_50_incidents_10_seconds(self):
        self.perform_extract_and_get_snippets(50, 10)

    @task
    def task_50_incidents_30_seconds(self):
        self.perform_extract_and_get_snippets(50, 30)

    @task
    def task_50_incidents_60_seconds(self):
        self.perform_extract_and_get_snippets(50, 60)

    @task
    def task_50_incidents_150_seconds(self):
        self.perform_extract_and_get_snippets(50, 150)

    @task
    def task_50_incidents_300_seconds(self):
        self.perform_extract_and_get_snippets(50, 300)

    # 100 incidents
    @task
    def task_100_incidents_3_seconds(self):
        self.perform_extract_and_get_snippets(100, 3)

    @task
    def task_100_incidents_10_seconds(self):
        self.perform_extract_and_get_snippets(100, 10)

    @task
    def task_100_incidents_30_seconds(self):
        self.perform_extract_and_get_snippets(100, 30)

    @task
    def task_100_incidents_60_seconds(self):
        self.perform_extract_and_get_snippets(100, 60)

    @task
    def task_100_incidents_150_seconds(self):
        self.perform_extract_and_get_snippets(100, 150)

    @task
    def task_100_incidents_300_seconds(self):
        self.perform_extract_and_get_snippets(100, 300)

    # 200 incidents
    @task
    def task_200_incidents_3_seconds(self):
        self.perform_extract_and_get_snippets(200, 3)

    @task
    def task_200_incidents_10_seconds(self):
        self.perform_extract_and_get_snippets(200, 10)

    @task
    def task_200_incidents_30_seconds(self):
        self.perform_extract_and_get_snippets(200, 30)

    @task
    def task_200_incidents_60_seconds(self):
        self.perform_extract_and_get_snippets(200, 60)

    @task
    def task_200_incidents_150_seconds(self):
        self.perform_extract_and_get_snippets(200, 150)

    @task
    def task_200_incidents_300_seconds(self):
        self.perform_extract_and_get_snippets(200, 300)

    # 400 incidents
    @task
    def task_400_incidents_3_seconds(self):
        self.perform_extract_and_get_snippets(400, 3)

    @task
    def task_400_incidents_10_seconds(self):
        self.perform_extract_and_get_snippets(400, 10)

    @task
    def task_400_incidents_30_seconds(self):
        self.perform_extract_and_get_snippets(400, 30)

    @task
    def task_400_incidents_60_seconds(self):
        self.perform_extract_and_get_snippets(400, 60)

    @task
    def task_400_incidents_150_seconds(self):
        self.perform_extract_and_get_snippets(400, 150)

    @task
    def task_400_incidents_300_seconds(self):
        self.perform_extract_and_get_snippets(400, 300)


class ApiUser(HttpLocust):
    task_set = ApiClientBehavior
    host = ''
