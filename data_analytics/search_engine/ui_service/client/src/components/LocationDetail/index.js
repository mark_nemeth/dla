import { connect } from 'react-redux';
import { LocationDetail } from './LocationDetail';
import { showError } from '../../redux/App/AppActions';
import { requestFileTransfer } from '../../redux/FileTransfer/FileTransferActions';
import { getCurrentTransferRequestStatus } from '../../redux/FileTransfer/FileTransferUtils';
import { FileTypes } from "../Sidebar/FilterElements/ByFileFilter/ByFileFilter";
import { isObjectValid } from "../../utils/helperFunctions";
import { fetchFileByBagPath } from '../../redux/Overview/OverviewActions'
import './LocationDetail.scss';

function mapStateToProps(state, ownProps) {
  const file = isObjectValid(state.overview.file) ? state.overview.file : null;

  let locationTransferRequests = {};
  if (file) {
    file.locations.forEach(location => {
      locationTransferRequests[location.cluster.id] = getCurrentTransferRequestStatus(state, file.fileGuid, location.cluster.id);
    });
  }

  const locationFetchingOngoing = state.httpRequests.fetchFileByBagPathRequest.ongoing;

  const fileType = ownProps.fileType;
  const overviewFile = (fileType === FileTypes.ORIGINALFILE || fileType === FileTypes.ALWAYSONFILE)
    ? (isObjectValid(state.overview.bagfile)) ? state.overview.bagfile : null
    : (isObjectValid(state.overview.childbagfiles)) ? state.overview.childbagfiles[0] : null;
  return { file, locationTransferRequests, fileType, overviewFile, locationFetchingOngoing };
}

export default connect(
  mapStateToProps,
  { showError, requestFileTransfer, fetchFileByBagPath }
)(LocationDetail);