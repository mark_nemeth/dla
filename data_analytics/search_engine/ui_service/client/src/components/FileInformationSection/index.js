import {connect} from 'react-redux';
import {FileInformationSection} from './FileInformationSection';
import {showError} from '../../redux/App/AppActions';
import {highlightSelectedChildBagfile} from '../../redux/Overview/OverviewActions';
import {FileTypes} from '../Sidebar/FilterElements/ByFileFilter/ByFileFilter';

function mapStateToProps(state, ownProps) {
  const fileType = ownProps.fileType;
  const bagfile = state.overview.bagfile || null;
  const childbagfiles = state.overview.childbagfiles || [];
  const selectedChildbagfileGuid = state.overview.selectedChildbagfileGuid || null;
  const overviewFile = (fileType === FileTypes.ORIGINALFILE || fileType === FileTypes.ALWAYSONFILE || fileType === FileTypes.DRIVE) ? bagfile : childbagfiles[0];
  const displayedLink = overviewFile.current_link || overviewFile.link;
  const flowRuns = state.overview.flowRuns || [];
  return {fileType, overviewFile, parent: bagfile, childbagfiles, displayedLink, selectedChildbagfileGuid, flowRuns};
}

export default connect(
  mapStateToProps,
  {highlightSelectedChildBagfile, showError}
)(FileInformationSection);