import storeProvider from '../../models/storeProvider';
import {isUserPermitted} from '../secure/permissonHelper';
import {isObjectValid} from '../../utils/helperFunctions';
import configHandlerSingleton from '../../configHandler';
import {DAIMLER_STRATEGY_ID, getOAuthStrategyId} from '../../loaders/oAuthLoader';
import getLogger from '../../utils/logging';

const logger = getLogger(__filename)

class UserHandler {

  constructor() {
    this._defaultUser = {name: null, admin: false};
  }

  async getUserInformation(req) {
    if (configHandlerSingleton.uiIsInDevMode()) {
      logger.info("UI is in devmode. Returning test user with admin rights.");
      return {name: 'DANFTestUser', admin: true};
    }
    
    if (!isObjectValid(req.user)) {
      logger.error("Request object does not have valid user data. Returning default user information.");
      return this._defaultUser;
    }
    
    const userData = await storeProvider.getUserDataAsync(req.user);

    if (!isObjectValid(userData)) {
      logger.warning('No user information received. Returning default user.');
      return this._defaultUser;
    }

    const username = getOAuthStrategyId() === DAIMLER_STRATEGY_ID ? userData.sub : userData.nickname;
    const userIsAdmin = await isUserPermitted(req.user);
    const userInformation = {name: username, admin: userIsAdmin}

    logger.debug(`Return user information: name: ${userInformation.name} isAdmin: ${userInformation.admin}`);
    return userInformation;
  }
}

export default new UserHandler();