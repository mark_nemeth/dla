import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByVIDFilter} from "./ByVIDFilter";
import ScrollList from "../../../Utils/ScrollList/ScrollList";

describe('ByVIDFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      vehicleIDList: ['VID-1', 'VID-2', 'VID-3'],
      selectedVidList: ['VID-2']
    };
    const wrapper = shallow(<ByVIDFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should renders vid list with selected column', () => {
    const reduxAction = jest.fn();
    const props = {
      vehicleIDList: ['VID-1', 'VID-2', 'VID-3'],
      selectedVidList: ['VID-2'],
      fetchAllVehicleIds: reduxAction,
      filterByVIDs: reduxAction
    };
    const wrapper = mount(<ByVIDFilter {...props} />);
    const vidList = wrapper.find(ScrollList);
    const entries = vidList.find('tr');
    expect(entries.length).toBe(3);
    expect(entries.at(0).find('input').props().checked).toBe(false);
    expect(entries.at(1).find('input').props().checked).toBe(true);
    expect(entries.at(2).find('input').props().checked).toBe(false);
  });


  it('should call action when vid is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      vehicleIDList: ['VID-1', 'VID-2', 'VID-3'],
      selectedVidList: ['VID-2'],
      fetchAllVehicleIds: jest.fn(),
      filterByVIDs: reduxAction
    };
    const wrapper = mount(<ByVIDFilter {...props} />);
    const input = wrapper.find('tr').at(1).find('input');
    input.simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

});
