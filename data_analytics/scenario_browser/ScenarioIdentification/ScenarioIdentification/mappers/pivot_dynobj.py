import numpy as np
import pandas as pd


if __name__=='__main__':
    in_file='../Data/tracks_intersection_dyn_obj_ego_w_relative.csv'
    out_file='../Data/dyn_features_pivot.csv'
    time_bin=10. #in Seconds
    res=1
    resample_window=f'{res}s'


    #=======================
    #read csv
    #=======================
    df=pd.read_csv(in_file)
    set_files=list(set(df.Filename))

    # for each timewindow: do a simple count of each type of dynamic objects intersected.
    #=======================
    #resample and pivot
    #=======================

    feat_dyn_list = []
    for fname in set_files:
        dtemp = df[df['Filename']==fname][['Filename', 'Track_id', 'T', 'Label',]]
        dtemp['t_delta']=dtemp['T']-dtemp['T'].min()
        dtemp['time_id']=(dtemp['t_delta'].astype('float64')/time_bin).astype(int)
        dtemp['Track_id'] = dtemp['Track_id'].astype(int)
        dtemp_pivot = dtemp.pivot_table(values = 'Track_id', index=['Filename', 'time_id'], columns=['Label'], aggfunc=lambda _: len(np.unique(_)))
        dtemp_pivot.reset_index(inplace=True)
        dtemp_pivot['time_id'] = pd.to_datetime(dtemp_pivot['time_id'], unit='s')
        dtemp_pivot.index = dtemp_pivot['time_id']
        dtemp_pivot = dtemp_pivot.resample('1s').nearest()
        dtemp_pivot['time_id'] = dtemp_pivot['time_id'].dt.second
        dtemp_pivot.reset_index(inplace=True, drop=True)
        dtemp_pivot=dtemp_pivot.fillna(0)
        feat_dyn_list.append(dtemp_pivot)

    feat_dyn = pd.concat(feat_dyn_list, sort=False)
    feat_dyn = feat_dyn.fillna(0)

    #=======================
    #save csv
    #=======================
    print(f"Saving pivot file to:{out_file}")
    feat_dyn.to_csv(out_file)