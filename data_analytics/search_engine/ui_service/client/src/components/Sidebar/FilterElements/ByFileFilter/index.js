import {connect} from 'react-redux';
import {filterByFileType} from '../../../../redux/Home/HomeActions';
import {ByFileFilter, FileTypes} from './ByFileFilter';
import {fetchFeatureToggles} from "../../../../redux/App/AppActions";

function mapStateToProps(state, ownProps) {
  const fileType = state.fileType || FileTypes.ORIGINALFILE;
  const isDriveViewActive = state.featureToggles.isDriveViewActive;

  return {fileType, isDriveViewActive};
}

export default connect(
  mapStateToProps,
  {
    filterByFileType,
    fetchFeatureToggles
  }
)(ByFileFilter);