import datetime
import logging
import pathlib
from typing import List, Set, Tuple, Union

import pandas as pd
import urllib3
from collector.data_catalog_connector import DataCatalogConnector, File, Url
from DANFUtils.utils.utils import generate_file_guid
from joblib import delayed, Parallel

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
logging.basicConfig(format='%(asctime)s:\t%(name)s:\t%(levelname)s:\t%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def collect(input_path: str,
            metadata_catalog_url: str,
            search_catalog_url: str,
            concurrent_requests: int = 1,
            file_type: str = ".bag",
            dry_run: bool = False,
            history_size: int = 3,
            update_buffer_in_hours: int = 12) -> None:
    """ Parses multiple csv files containing file metadata. Then, generates unique guids based on filename(This
    information is contained in the csv files). Then, the files are grouped based on the guids. Last,
    the file information is added to the data catalog.

    :param input_path: Path to csv file that will be scanned and added to the data catalog.
    :param metadata_catalog_url: Url leading to the data catalog. More specific to the metadata api part.
    :param search_catalog_url: Url leading to the data catalog. More specific to the search api part.
    :param concurrent_requests: Number of concurrent_requests that are sent to data catalog.
    :param file_type: File type that is added to the file catalog.
    :param dry_run: If set, no requests will be sent to the data catalog.
    :param history_size: Number of previous runs that are saved.
    :param update_buffer_in_hours: Minimum number of hours before an accessor_url can be updated.
    :return: None
    """

    root = pathlib.Path(input_path)
    all_files = list(root.glob("*.csv"))
    scanned_folder_files = list(root.glob("*.folder"))
    scanned_folders = extract_scanned_folder(scanned_folder_files)
    file_type = file_type.strip()

    if not all_files:
        logger.info("Nothing found to add. Exiting...")
        return

    assert len(all_files) == len(scanned_folder_files), "Expected matching count for .csv and .folder files."

    df = read_csv_files(all_files)
    df = filter_invalid_entries(df)
    df = filter_by_file_ending(df, file_type)
    df = filter_duplicate_full_paths(df)

    if len(df) > 0:
        bags = generate_guids(df)
        bags_grouped = group_by_guid(bags)
    else:
        bags_grouped = pd.DataFrame(columns=["FullPath", "FileName", "CreationTime", "FileSize"])

    data_catalog_connector = DataCatalogConnector(logger, metadata_catalog_url, search_catalog_url, dry_run=dry_run)

    update_cutoff_timestamp = convert_timestamp_from_s_to_ms(
        (datetime.datetime.now() - datetime.timedelta(hours=update_buffer_in_hours)).timestamp())

    prune_data_catalog(bags_grouped,
                       data_catalog_connector,
                       scanned_folders,
                       concurrent_requests,
                       file_type,
                       update_cutoff_timestamp)

    suffix = datetime.datetime.now().strftime("%y%m%d-%H%M%S")
    logger.info('Storing added files for now...')
    for path in all_files + scanned_folder_files:
        path.rename(str(path) + f".{suffix}.added")

    logger.info(f"Running housekeeping... Only keeping files from last {history_size} runs...")
    housekeeping(root=root, history_size=history_size)
    logger.info(f"Housekeeping completed")

    logger.info('Completed data catalog update!')


def read_csv_files(all_files: List[pathlib.Path]) -> pd.DataFrame:
    df_from_each_file = (read_csv(file) for file in all_files)
    df = pd.concat(df_from_each_file, ignore_index=False)
    logger.info(f'All files parsed.')
    return df


def read_csv(file: pathlib.Path) -> pd.DataFrame:
    logger.info(f'Parsing file "{file}"...')
    return pd.read_csv(file)


def filter_invalid_entries(df: pd.DataFrame) -> pd.DataFrame:
    logger.info(f'Filtering invalid entries...')
    df = df[df['MD5(lastMB)'] != "ERROR"]
    logger.info(f'Filtered invalid entries.')
    return df


def filter_by_file_ending(df: pd.DataFrame, ending: str) -> pd.DataFrame:
    logger.info(f'Filtering by file endings (Only keeping "{ending}")...')
    bags = df[df['FileName'].map(lambda x: x.endswith(ending))]
    logger.info(f'Filtered by file endings (Only keeping "{ending}").')
    return bags


def filter_duplicate_full_paths(df: pd.DataFrame) -> pd.DataFrame:
    logger.info(f'Filtering duplicate full paths...')
    filtered = df.drop_duplicates(subset="FullPath")
    logger.info(f'Filtered duplicate full paths.')
    return filtered


def generate_guids(df: pd.DataFrame) -> pd.DataFrame:
    logger.info(f'Generating guids...')
    df['guid'] = df.apply(lambda x: generate_file_guid(x['FileName']), axis=1)
    logger.info(f'Generated guids.')
    return df


def group_by_guid(df: pd.DataFrame) -> pd.DataFrame:
    logger.info(f'Grouping by guid...')
    df_grouped = df.groupby('guid').agg({
        "FullPath": list,
        "FileName": lambda x: x.iloc[0],
        "CreationTime": list,
        "FileSize": lambda x: x.iloc[0]
    })
    logger.info(f'Grouped by guid.')
    return df_grouped


def prune_data_catalog(df: pd.DataFrame,
                       data_catalog_connector: DataCatalogConnector,
                       scanned_folders: List[str],
                       concurrent_requests: int,
                       file_type: str,
                       cutoff_timestamp_in_ms: int) -> None:
    scanned_folders = set(convert_to_fully_qualified_link(folder) for folder in scanned_folders)
    to_drop = []
    logger.info(f"CSVs contain info for {len(df)} files.")
    logger.info(f"Scanned folders are {scanned_folders}.")
    for file in data_catalog_connector.iterate_catalog(chunk_size=1000):
        if file.file_type != file_type:
            continue

        add_to_drop, creation_times, full_paths = get_file_from_df(df, file)

        present_urls = [convert_to_fully_qualified_link(url) for url in full_paths]
        skip = []
        for url_id, url in enumerate(file.urls):
            if not in_scanned_folders(scanned_folders, url):
                continue

            if url.last_changed > cutoff_timestamp_in_ms:
                continue

            try:
                skip.append(present_urls.index(url.accessor_url))
                update_url_status(data_catalog_connector, file, url_id, "Available")
            except ValueError:
                update_url_status(data_catalog_connector, file, url_id, "Unavailable")

        add_missing_urls(data_catalog_connector, file, creation_times, present_urls, skip)

        if add_to_drop:
            to_drop.append(file.file_guid)
    df.drop(to_drop, inplace=True)
    logger.info(f"Remaining file count is: {len(df)}.")

    logger.info("Adding remaining files...")
    Parallel(n_jobs=concurrent_requests, verbose=1)(
        delayed(add_file_to_data_catalog)(data_catalog_connector, str(file_guid), row, file_type) for file_guid, row in
        df.iterrows())
    logger.info("Remaining files added.")


def get_file_from_df(df: pd.DataFrame, file: File) -> Tuple[bool, List[float], List[str]]:
    try:
        return True, df.at[file.file_guid, 'CreationTime'], df.at[file.file_guid, 'FullPath']
    except KeyError:
        return False, [], []


def in_scanned_folders(scanned_folders: Set[str], url: Url) -> bool:
    return any([url.accessor_url.startswith(scanned_folder) for scanned_folder in scanned_folders])


def add_missing_urls(data_catalog_connector: DataCatalogConnector,
                     file: File, creation_times: List[float],
                     present_urls: List[str],
                     skip: List[int]) -> None:
    for index, url in enumerate(present_urls):
        if index in skip:
            continue

        creation_time = creation_times[index]
        data_catalog_connector.add_accessor_url(file_guid=file.file_guid,
                                                accessor_url=url,
                                                creation_date=convert_timestamp_from_s_to_ms(creation_time))


def update_url_status(data_catalog_connector: DataCatalogConnector, file: File, url_id: int, new_status: str):
    if file.urls[url_id].url_status != new_status:
        data_catalog_connector.update_url_status(file.file_guid, url_id, new_status)


def add_file_to_data_catalog(data_catalog_connector: DataCatalogConnector,
                             file_guid: str,
                             row: pd.Series,
                             file_type: str) -> None:
    data_catalog_connector.add_file(
        file_guid,
        row.get("FileSize"),
        file_type
    )

    for index, accessor_url in enumerate(row.get("FullPath")):
        data_catalog_connector.add_accessor_url(
            file_guid,
            convert_to_fully_qualified_link(accessor_url),
            convert_timestamp_from_s_to_ms(row.get('CreationTime')[index])
        )


def convert_to_fully_qualified_link(accessor_url: str) -> str:
    accessor_url = accessor_url.replace('/mnt/hdfs/hdfs', 'hdfs://abt', 1)
    accessor_url = accessor_url.replace('/mapr/', 'hdfs://', 1)
    return accessor_url


def convert_timestamp_from_s_to_ms(creation_time: Union[float, str, int]) -> int:
    try:
        timestamp = int(float(creation_time) * 1000)
        # Data Catalog rejects timestamps less than 946684800001
        return max(946684800001, timestamp)
    except ValueError:
        return 946684800001


def delete_added_csv_files(paths: List[pathlib.Path]) -> None:
    for path in paths:
        path.unlink()


def extract_scanned_folder(paths: List[pathlib.Path]) -> List[str]:
    folders = []
    for path in paths:
        with open(path) as fp:
            folders += fp.readlines()
    return [folder for folder in folders if folder]


def housekeeping(root: pathlib.Path, history_size: int) -> None:
    suffix_len = len("000000-000000.added")
    files = list(root.rglob("**/*.??????-??????.added"))
    suffixes = sorted(set([file.name[-suffix_len:] for file in files]))
    suffixes_to_delete = suffixes[:-history_size] if history_size > 0 else suffixes

    for file in files:
        if file.name[-suffix_len:] in suffixes_to_delete:
            file.unlink()
