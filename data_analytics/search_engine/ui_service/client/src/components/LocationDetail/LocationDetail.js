import React, {useState, useEffect} from 'react';
import Card from 'react-bootstrap/Card';
import StatusEntry from '../AttributeFormatter/Status/StatusEntry';
import LabelEntry from '../AttributeFormatter/Label/LabelEntry';
import LabelContainer from '../Utils/LabelContainer/LabelContainer';
import PathEntry from '../AttributeFormatter/Path/PathEntry';
import SecondTimeEntry from "../AttributeFormatter/SecondTime/SecondTimeEntry";
import ScrollList from '../Utils/ScrollList/ScrollList';
import {Countdown, CountdownStates} from '../../utils/refresh/Countdown';
import {Container, Row, Col, ProgressBar, Button, Badge} from 'react-bootstrap';
import Spinner from 'react-bootstrap/Spinner';
import {FileTypes} from "../Sidebar/FilterElements/ByFileFilter/ByFileFilter";
import Modal from "react-bootstrap/Modal";
import './LocationDetail.scss';


function activateLocationRefresh(file) {
  let activateRefresh = false;
  if (!file) {
    return false;
  }

  const locations = file.locations;
  if (!locations) {
    return false;
  }

  locations.forEach(location => {
    if (location.transfer && (location.transfer.status === "STARTED" || location.transfer.status === "WAITING")) {
      activateRefresh = true;
    }
  })
  return activateRefresh;
}

export function LocationDetail(props) {

  const refreshIntervalInSeconds = 10;

  const [modalShow, setModalShow] = useState(false);
  const [transferLocationId, setTransferLocationId] = useState("");

  const [locationRefreshCountdown, setLocationRefreshCountdown] = useState(null);
  const [locationRefreshIsActive, setLocationRefreshIsActive] = useState(false);
  const [nextLocationRefreshIn, setNextLocationRefreshIn] = useState(refreshIntervalInSeconds);

  const scrollListPathItem = (path) => {
    return <PathEntry filePath={path}/>
  };

  function countdownCompletedListener() {
    let bagPath = props.overviewFile.hasOwnProperty("current_link") ? props.overviewFile.current_link : props.overviewFile.link;
    props.fetchFileByBagPath(bagPath);
  }

  if (activateLocationRefresh(props.file)) {
    if (!locationRefreshIsActive) {
      setLocationRefreshIsActive(true);
    }

    if (!locationRefreshCountdown) {
      const locationRefreshCountdownInstance = new Countdown(refreshIntervalInSeconds);
      locationRefreshCountdownInstance.addCountdownEventListener(setNextLocationRefreshIn);
      locationRefreshCountdownInstance.addCountdownCompletedEventListener(countdownCompletedListener);
      setLocationRefreshCountdown(locationRefreshCountdownInstance);
    }

    if (locationRefreshCountdown && locationRefreshCountdown.state === CountdownStates.IDLE && !props.locationFetchingOngoing) {
      locationRefreshCountdown.startNewCountdown();
    }

  } else {
    if (locationRefreshIsActive) {
      setLocationRefreshIsActive(false);
    }
    if (locationRefreshCountdown && locationRefreshCountdown.state === CountdownStates.STARTED) {
      locationRefreshCountdown.clearCountdown();
      setNextLocationRefreshIn(refreshIntervalInSeconds);
    }
  }

  if (props.locationFetchingOngoing && locationRefreshCountdown && locationRefreshCountdown.state === CountdownStates.STARTED) {
    locationRefreshCountdown.clearCountdown();
  }

  useEffect(() => {

    return () => {
      if (locationRefreshCountdown) {
        locationRefreshCountdown.clearCountdown();
      }
    }
  }, [locationRefreshCountdown]);

  const locationCards = props.file && props.file.locations && props.file.locations.map((location) => {

    function onTransferButtonClick() {
      if (props.file && props.file.fileGuid &&
          location && location.cluster && location.cluster.id) {
        if ((props.fileType === FileTypes.ORIGINALFILE || props.fileType === FileTypes.ALWAYSONFILE) &&
            (props.overviewFile.file_type === "ORIGINAL" || props.overviewFile.size > 200000000000)) { // file size > 200GB
          setTransferLocationId(location.cluster.id);
          setModalShow(true)
        } else {
          props.requestFileTransfer(props.file.fileGuid, location.cluster.id);
        }
      }
    }

    function getTransferRequestButton(text, clusterId) {
      if (props.overviewFile.tags.includes("transfer-eu_to_us_allowed") || clusterId !== "SV")
        return <Col lg={2}>
          {props.locationTransferRequests[location.cluster.id].ongoing === false &&
          <Button disabled={props.transferRequestOngoing} variant="info" onClick={() => onTransferButtonClick()}>
            {text}
          </Button>}
          {props.locationTransferRequests[location.cluster.id].ongoing === true &&
          <Spinner animation="border" variant="info"/>}
        </Col>
      else
        return <Col className="ml-3">
          Transfer not allowed due to SHREMS regulations.
        </Col>
    }

    return (
        <Row className='mb-4' key={location.cluster.id}>
          <Col>
            <Card className='location-card'>
              <Card.Body>
                <Row className='justify-content-between'>
                  <Col md={12} lg={6} xl={4} className="col-xxl-3 col-xxxl-3 col-xxxxl-2">
                    <LabelContainer label="Cluster Name" variant="card-style-filled">
                      <LabelEntry value={location.cluster.name}/>
                    </LabelContainer>
                  </Col>
                  <Col md={12} lg={6} xl={4} className="col-xxl-3 col-xxxl-3 col-xxxxl-2">
                    <LabelContainer label="Status" variant="card-style-filled">
                      <StatusEntry status={location.status}/>
                    </LabelContainer>
                  </Col>
                </Row>
                {
                  location.paths && location.paths.length > 0 &&
                  <Row className='pt-2'>
                    <Col>
                      <LabelContainer label="Paths" variant="card-style-filled">
                        <ScrollList
                            showLoading={false}
                            listHeight={'100%'}
                            striped={false}
                            itemList={location.paths}
                            listItemComponent={scrollListPathItem}
                            itemVariant={"childbagfile-list-item-container"}
                        />
                      </LabelContainer>
                    </Col>
                  </Row>
                }
                {
                  location.transfer && location.transfer.status === 'WAITING' &&
                  <Row className='pt-2'>
                    <Col lg={2} md={10} col={12} xl={2} className="col-xxl-1">
                      <LabelContainer label="Queue Position" variant="card-style-filled">
                        <LabelEntry value={location.transfer.queuePosition}/>
                      </LabelContainer>
                    </Col>
                  </Row>
                }
                {
                  location.transfer && location.transfer.status === 'STARTED' &&
                  <Row className='pt-2'>
                    <Col>
                      <LabelContainer label="Progress" variant="card-style-filled-padding-left-right">
                        <div className="progress-bar-wrapper">
                          <ProgressBar variant="info" now={location.transfer.progress}
                                       label={`${(location.transfer.progress).toFixed(2)}%`}/>
                        </div>
                      </LabelContainer>
                    </Col>
                    <Col md={12} lg={6} xl={4} className="col-xxl-3 col-xxxl-3 col-xxxxl-2">
                      <LabelContainer label="Estimated Time of Arrival" variant="card-style-filled">
                        <SecondTimeEntry time_in_seconds={location.transfer.etaSeconds}/>
                      </LabelContainer>
                    </Col>
                  </Row>
                }
                {
                  location.status === "Unavailable" &&
                  <Row className='pt-2'>
                    {getTransferRequestButton("Request Transfer", location.cluster.id)}
                  </Row>
                }
                {
                  location.status === "Error" &&
                  <React.Fragment>
                    <Row className='pt-2'>
                      <Col>
                        <LabelContainer label="Error Message" variant="card-style-filled">
                          <LabelEntry value={location.transfer.errorMessage}/>
                        </LabelContainer>
                      </Col>
                    </Row>
                    <Row className='pt-2'>
                      {getTransferRequestButton("Retrigger Transfer", location.cluster.id)}
                    </Row>
                  </React.Fragment>
                }
              </Card.Body>
            </Card>
          </Col>
          <Modal className="confirm-transfer-modal" show={modalShow} onHide={() => setModalShow(false)} size="lg"
                 aria-labelledby="contained-modal-title-vcenter" centered scrollable>
            <div onClick={() => setModalShow(false)}>
              <div className="modal-icon-close icon-close"/>
            </div>
            <Modal.Header>
              <Modal.Title className="modal-title" id="contained-modal-title-vcenter">Warning</Modal.Title>
            </Modal.Header>
            <Modal.Body>{props.overviewFile.file_type === "ORIGINAL" ?
                "You are trying to trigger a transfer of an ORIGINAL file. Such files are usually very large and the transfer can take a long time. As DANF creates smaller stripped and splitted snippets of ORIGINAL files based on events and topic profiles, please consider to transfer such a snippet which fits your need." :
                "You are trying to trigger a transfer of a file larger than 200 GB. Please consider to transfer a snippet that also fits your need."}
            </Modal.Body>
            <Modal.Body>{props.overviewFile.file_type === "ORIGINAL" ?
                "Do you really want to start the transfer of the ORIGINAL file?" :
                "Do you really want to start the transfer?"}</Modal.Body>
            <div className="button-container">
              <Button variant="info"
                      onClick={() => {
                        props.requestFileTransfer(props.file.fileGuid, transferLocationId);
                        setModalShow(false)
                      }}>Yes </Button>
              <Button variant="info" onClick={() => setModalShow(false)}>No</Button>
            </div>
          </Modal>
        </Row>

    )
  });


  return (
      <Container fluid className='location-detail p-5'>
        {
          (!props.file && props.locationFetchingOngoing) &&
          <Row className="h-100 justify-content-center align-items-center">
            <Spinner animation="border" variant="info"/>
          </Row>
        }

        {
          (props.file && props.file.locations) &&
          <Row className='mb-1'>
            <Col lg={2}>
              {
                (locationRefreshIsActive && locationRefreshCountdown.state === CountdownStates.STARTED) &&
                <Badge variant="info" className="span-12">Refresh active. Next
                  in {nextLocationRefreshIn} Second(s)</Badge>
              }
              {
                (locationRefreshIsActive && locationRefreshCountdown.state === CountdownStates.IDLE && props.locationFetchingOngoing) &&
                <Badge variant="info" className="flex-grow-1">Refresh active. Loading Data ...</Badge>
              }
              {
                !locationRefreshIsActive &&
                <Badge variant="info">Refresh inactive</Badge>
              }
            </Col>
          </Row>
        }
        {
          (props.file && props.file.locations && props.file.locations.length > 0) &&
          locationCards
        }
        {
          (!props.file && !props.locationFetchingOngoing) &&
          <div>File unavailable on all locations.</div>
        }
      </Container>
  );
}