import React, {useEffect, useState} from 'react';
import {Spinner} from 'react-bootstrap';
import IngestSupportBox from '../../components/Ingest/IngestSupportBox/IngestSupportBox';
import IngestList from '../../components/Ingest/IngestList/IngestList';
import IngestHistory from '../../components/Ingest/IngestHistory';
import {Countdown, CountdownStates} from "../../utils/refresh/Countdown";
import './IngestPage.scss';

export function IngestPage(props) {

  const refreshIntervalInSeconds = 10;

  const [selectedVehicle, setSelectedVehicle] = useState();

  const [ingestRefreshCountdown, setIngestRefreshCountdown] = useState(null);
  const [nextIngestRefreshIn, setNextIngestRefreshIn] = useState(refreshIntervalInSeconds);

  function countdownCompletedListener() {
    props.refresh();
  }

  if (!ingestRefreshCountdown) {
    const ingestRefreshCountdownInstance = new Countdown(refreshIntervalInSeconds);
    ingestRefreshCountdownInstance.addCountdownEventListener(setNextIngestRefreshIn);
    ingestRefreshCountdownInstance.addCountdownCompletedEventListener(countdownCompletedListener);
    setIngestRefreshCountdown(ingestRefreshCountdownInstance);
  }

  if (ingestRefreshCountdown && ingestRefreshCountdown.state === CountdownStates.IDLE) {
    ingestRefreshCountdown.startNewCountdown();
  }

  useEffect(() => {
    return () => {
      if (ingestRefreshCountdown) {
        ingestRefreshCountdown.clearCountdown();
      }
    }
  }, [ingestRefreshCountdown]);

  useEffect(() => {
    document.title = 'DANF - Ingest';
    props.refresh();
  }, []);

  const showLoading = !props.latestIngests;

  return (
      <div className="ingest-page">
        {
          showLoading ?
              (
                  <div className="loading-spinner-container">
                    <Spinner animation="border" variant="info"/>
                  </div>
              ) :
              (
                  <div className="ingest-page-content">
                    <div className="ingest-page-content-sidebar">
                      {
                        (ingestRefreshCountdown && ingestRefreshCountdown.state === CountdownStates.STARTED) &&
                        <div className="ingest-page-countdown pl-1">
                          {"Next refresh in " + nextIngestRefreshIn + " second(s)."}
                        </div>
                      }
                      <IngestList ingests={props.latestIngests} onVehicleSelected={setSelectedVehicle}/>
                    </div>
                    <div className="ingest-page-content-detail">
                      {
                        selectedVehicle &&
                        <IngestHistory selectedVehicle={selectedVehicle}
                                       nextIngestRefreshIn={nextIngestRefreshIn}/>
                      }
                      {
                        !selectedVehicle &&
                        <div className="mx-2 ingest-history-title">
                          <div className="mt-2 fifty-percent-card">
                            { props.latestIngests.length > 0 ? "Please select a vehicle.": "There are no ingests available."}
                          </div>
                          <div className="fifty-percent-card">
                            <IngestSupportBox/>
                          </div>
                        </div>
                      }
                    </div>
                  </div>
              )
        }
      </div>
  );
}
