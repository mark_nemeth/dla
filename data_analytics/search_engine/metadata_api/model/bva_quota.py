__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from pydantic import BaseModel
from model.types import Guid, UnixTimestampMilliseconds
from typing import Dict, Optional


class QuotaUsage(BaseModel):
    total: int
    used: int = 0


class QuotaDocument(BaseModel):
    version: str = "1.0"
    guid: Guid
    active: bool
    started_at: UnixTimestampMilliseconds
    ended_at: Optional[UnixTimestampMilliseconds]
    quota_usage_per_account: Dict[str, QuotaUsage]

    def get_usage_of_account(self, account_name) -> Optional[QuotaUsage]:
        return self.quota_usage_per_account.get(account_name)


class QuotaResponse(BaseModel):
    started_at: UnixTimestampMilliseconds
    ended_at: Optional[UnixTimestampMilliseconds]
    total: int
    used: int = 0


class QuotaPatchRequest(BaseModel):
    active: Optional[bool]
    started_at: Optional[UnixTimestampMilliseconds]
    ended_at: Optional[UnixTimestampMilliseconds]