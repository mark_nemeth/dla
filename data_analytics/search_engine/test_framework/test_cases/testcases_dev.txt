[insert_bagfile]
env = development
api = post_bagfile
request_type = raw
value = {"version": "1.0", "link": "XXXXXX", "file_name":"test_file", "file_type":"test_filetype", "tags":["training", "test"], "size": 21231, "num_messages": 4444, "start": 1475272800000, "end": 1475272800001, "duration": 752728, "vehicle_id_num": "V-123-234", "extractor": "ext1", "state":[]}

[retrieve_bagfile]
env = development
api = get_bagfile
request_type = url
value = 3bcc1954-5259-451d-8b40-ddc9f9e8b547


[insert_sequence]
env = development
api = post_sequence
request_type = raw
value = {"bagfile": {"version": "1.0", "link": "XXXXXX", "file_name":"test_file", "file_type":"test_filetype", "tags":["training", "test"], "size": 21231, "num_messages": 4444, "start": 1475272800000, "end": 1475272800001, "duration": 752728, "vehicle_id_num": "V-123-234", "extractor": "ext1", "state":[]},"sequence":{"version": "1.0","index": "0" ,"start": 1475272800000,"end": 1475272800000,"type": "basic","extractor": "ext1"}}


[search_request]
env = development
api = post_search_request
request_type = raw
value = {"bagfile": {"version": "1.0","link": "XXXXXX", "file_name":"test_file", "file_type":"test_filetype", "tags":["training", "test"], "size": 21231,"num_messages": 4444,"start": 1557847244000,"end": 1557847249700,"duration": 752728,"vehicle_id_num": "V-123-234","extractor": "ext1", "state":[]},"extraction":{"version": "1.0","bagfile_guid": "","extractor_id": "ext1","metadata_list": [{"start": 1557847244200,"end": 1557847244800,"metadata": {"key1": "value1"},"bva":8},{"start": 1557847247000,"end": 1557847249000,"metadata": {"key2": "value2"},"bva": 7}]},"search_request":{"bagfile_guid": "","bva_limit": 5}}


[extraction_request]
env = development
api = post_extractionrequest
request_type = raw
value = {"bagfile": {"version": "0.1", "link": "XXXXXX", "file_name":"test_file", "file_type":"test_filetype", "tags":["training", "test"], "size": 21231, "num_messages": 4444, "start": 1557847244000, "end": 1557847249700, "duration": 752728, "vehicle_id_num": "V-123-234", "extractor": "ext1", "state":[]},"extraction":{"version": "1.0","bagfile_guid": "","extractor_id": "ext1","metadata_list": [{"start": 1557847244200,"end": 1557847244800,"metadata": {"key1": "value1"},"bva":8},{"start": 1557847247000,"end": 1557847249000,"metadata": {"key2": "value2"},"bva": 7}]}}

[test_full_flow_on_cloud]
env = development
value = {"path": "/dlastorage1sadev/2019-07-30/20190722_155134_deepmap-im-v108-mission9014_ORIGINAL_training_000.bag", "output_directory": "tmp/danf/testframework/", "steps":["extractor", "splitter", "exporter-cv1","export-lidar","cleanup"]}