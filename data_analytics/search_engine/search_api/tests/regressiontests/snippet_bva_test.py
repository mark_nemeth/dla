"""Regressiontests for snippet bva route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import random as rd
import unittest

import numpy as np
import requests
from DANFUtils.utils import generate_deterministic_guid

from config.config_handler import config_dict
from .testutils import get_testdb_connector

disable_auth = True
token_url = 'https://dev-joa-e89f.eu.auth0.com/oauth/token'
token_payload = {
    "client_id": "6zoivUDEKA2wgnsauHWlwalYFua6Dm0h",
    "client_secret":
    "DORKkHZbvQtmTw1fzcurcs66jZ6SyPWbfYwGJDVjX5XrnGSFMvvY2Fw8Y6vBLon_",
    "audience": "https://metadata.api/",
    "grant_type": "client_credentials",
    "scope": "read:sequences"
}

bagfile_guid = generate_deterministic_guid()

raw_data_bagfile = {
    "version": "1.0",
    "guid": bagfile_guid,
    "bva":5,
    "link": "XXXXXX",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1"
}


def generate_test_sequence(index):
    return {
        "guid": generate_deterministic_guid(),
        "bagfile_guid": bagfile_guid,
        "index": index,
        "start": 1475272800000 + index * 1000,
        "end": 1475272800000 + index * 1000 + 999,
        "type": "basic",
        "extractors": {
            "ext1": {
                "bva": int(np.random.randint(1, 11)),
            },
            "ext1_3": {
                "bva": 0,
                "metadata": {
                    "gps": [{
                        "longitude": 9.1 + index * 0.001,
                        "latitude": 48.7 + index * 0.001,
                        "ts": 1475272800 * index
                    }]
                }
            },
            "ext2": {
                "bva": int(np.random.randint(1, 11))
            },
            "ext3": {
                "bva": int(np.random.randint(1, 11))
            }
        }
    }


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'guid': bagfile_guid})
        con.db['sequences'].delete_many({'bagfile_guid': bagfile_guid})
        con.db['child_bagfiles'].delete_many({'parent_guid': bagfile_guid})
    rd.seed(0)
    np.random.seed(0)


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'guid': bagfile_guid})
        con.db['sequences'].delete_many({'bagfile_guid': bagfile_guid})
        con.db['child_bagfiles'].delete_many({'parent_guid': bagfile_guid})


def get_token(scope=""):
    if disable_auth:
        return None
    token_payload['scope'] = scope
    payload = json.dumps(token_payload)
    headers = {"content-type": "application/json"}
    r = requests.post(token_url, payload, headers=headers)
    return r.json().get('access_token')


def auth_header(scope=""):
    token = get_token(scope)
    if token:
        return {"authorization": "Bearer " + token}
    else:
        return {}


def getCert():
    try:
        cert = config_dict.get_value("testing_ssl_cert", "")
        return cert
    except (IOError, OSError) as e:
        print(e)
        return


base_url = 'http://localhost:8889'
bva_snippet_url = base_url + '/snippet/bva/'
https_url = 'https://localhost:8445/snippet/bva/'

cert = getCert()


class TestSnippetBVAPOST(unittest.TestCase):
    def test_snippet_bva_connected_sequences_POST(self):
        prepare_db()
        sequences = [generate_test_sequence(x) for x in range(0, 10)]
        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences(sequences)
        query_data = {
            'bagfile_guid': bagfile_guid,
            'bva_limit': 7,
            'gps_subsampling_ratio': 2,
            'generate_child_bagfiles': False
        }

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(200, res.status_code,
                         'Snippet BVA post failed for connected sequences')

        # expected response
        res_json = res.json()['results']

        expected_values = [(1475272801000, 1475272801999, 1, 1, {}),
                           (1475272803000, 1475272807999, 3, 7, {
                               "type":
                               "LineString",
                               "coordinates": [[9.103, 48.703],
                                               [9.105, 48.705000000000005],
                                               [9.107, 48.707]]
                           })]

        actual_values = [(x['start'], x['end'], x['parent_index_start'],
                          x['parent_index_end'], x['gps_track'])
                         for x in res_json]
        self.assertCountEqual(
            expected_values, actual_values,
            'Snippet BVA post failed for connected sequences')
        self.assertEqual(bagfile_guid, res_json[1]['parent_guid'],
                         'Snippet BVA post failed for connected sequences')

        cleanup_db()

    def test_snippet_bva_connected_sequences_extractors_POST(self):
        prepare_db()
        sequences = [generate_test_sequence(x) for x in range(0, 10)]
        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences(sequences)
        query_data = {
            'bagfile_guid': bagfile_guid,
            'bva_limit': 7,
            'gps_subsampling_ratio': 2,
            'extractors': ['ext2'],
            'generate_child_bagfiles': False
        }

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(200, res.status_code,
                         'Snippet BVA post failed for connected sequences')

        # expected response
        res_json = res.json()['results']

        expected_values = [(1475272801000, 1475272801999, 1, 1, {}),
                           (1475272803000, 1475272805999, 3, 5, {
                               "type":
                               "LineString",
                               "coordinates": [[9.103, 48.703],
                                               [9.105, 48.705000000000005]]
                           }), (1475272807000, 1475272807999, 7, 7, {})]

        actual_values = [(x['start'], x['end'], x['parent_index_start'],
                          x['parent_index_end'], x['gps_track'])
                         for x in res_json]
        self.assertCountEqual(
            expected_values, actual_values,
            'Snippet BVA post failed for connected sequences')
        self.assertEqual(bagfile_guid, res_json[1]['parent_guid'],
                         'Snippet BVA post failed for connected sequences')

        cleanup_db()

    def test_snippet_bva_disconnected_sequences_POST(self):
        prepare_db()
        sequences = [
            generate_test_sequence(x)
            for x in [13, 14, 15, 16, 19, 30, 31, 32, 58, 59]
        ]
        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences(sequences)
        query_data = {'bagfile_guid': bagfile_guid, 'bva_limit': 4}

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(200, res.status_code,
                         'Snippet BVA post failed for disconnected sequences')

        res_json = res.json()['results']

        expected_values = [(1475272813000, 1475272816999, 13, 16),
                           (1475272819000, 1475272819999, 19, 19),
                           (1475272830000, 1475272832999, 30, 32),
                           (1475272858000, 1475272859999, 58, 59)]
        actual_values = [(x['start'], x['end'], x['parent_index_start'],
                          x['parent_index_end']) for x in res_json]
        self.assertCountEqual(
            expected_values, actual_values,
            'Snippet BVA post failed for disconnected sequences')
        cleanup_db()

    def test_snippet_bva_disconnected_sequences_extractors_POST(self):
        prepare_db()
        sequences = [
            generate_test_sequence(x)
            for x in [13, 14, 15, 16, 19, 30, 31, 32, 58, 59]
        ]
        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences(sequences)
        query_data = {
            'bagfile_guid': bagfile_guid,
            'bva_limit': 4,
            'extractors': ['ext2']
        }

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(200, res.status_code,
                         'Snippet BVA post failed for disconnected sequences')

        res_json = res.json()['results']

        expected_values = [(1475272814000, 1475272816999, 14, 16),
                           (1475272819000, 1475272819999, 19, 19),
                           (1475272830000, 1475272830999, 30, 30),
                           (1475272832000, 1475272832999, 32, 32),
                           (1475272858000, 1475272859999, 58, 59)]
        actual_values = [(x['start'], x['end'], x['parent_index_start'],
                          x['parent_index_end']) for x in res_json]
        self.assertCountEqual(
            expected_values, actual_values,
            'Snippet BVA post failed for disconnected sequences')
        cleanup_db()

    def test_snippet_bva_limit_is_zero_POST(self):
        prepare_db()
        sequences = [
            generate_test_sequence(x)
            for x in [13, 14, 15, 16, 19, 30, 31, 32, 58, 59]
        ]
        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences(sequences)
        query_data = {'bagfile_guid': bagfile_guid, 'bva_limit': 0}

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(200, res.status_code,
                         'Snippet BVA post failed for bva_limit=0')

        res_json = res.json()['results']

        expected_values = [(1475272813000, 1475272816999, 13, 16),
                           (1475272819000, 1475272819999, 19, 19),
                           (1475272830000, 1475272832999, 30, 32),
                           (1475272858000, 1475272859999, 58, 59)]
        actual_values = [(x['start'], x['end'], x['parent_index_start'],
                          x['parent_index_end']) for x in res_json]
        self.assertCountEqual(expected_values, actual_values,
                              'Snippet BVA post failed for bva_limit=0')
        cleanup_db()

    def test_snippet_bva_no_sequences_POST(self):
        prepare_db()
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        query_data = {'bagfile_guid': raw_data_bagfile['guid'], 'bva_limit': 5}

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(200, res.status_code,
                         'Snippet BVA post test failed for missing sequences')

        res_json = res.json()['results']
        self.assertEqual([], res_json,
                         'Snippet BVA post test failed for missing sequences')
        cleanup_db()

    def test_snippet_bva_one_sequence_POST(self):
        prepare_db()
        sequences = [{
            "bagfile_guid": bagfile_guid,
            "index": 0,
            "version": "0.3.6",
            "extractors": {
                "ext1_3": {
                    "bva": 0,
                    "metadata": {
                        "gps": [{
                            "latitude": 47.9198452215455,
                            "longitude": 8.71771894618182,
                            "ts": 1558533895
                        }]
                    }
                }
            },
            "start": 1558533895000,
            "end": 1558533896000,
            "guid": generate_deterministic_guid()
        }]
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
            con.add_sequences(sequences)
        query_data = {'bagfile_guid': bagfile_guid, 'bva_limit': 5}

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(200, res.status_code,
                         'Snippet BVA post test failed for one sequence')

        res_json = res.json()['results']
        self.assertEqual([], res_json,
                         'Snippet BVA post test failed for one sequence')
        cleanup_db()

    def test_snippet_bva_no_sequences_no_bagfile_POST(self):
        prepare_db()
        query_data = {'bagfile_guid': bagfile_guid, 'bva_limit': 5}

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertNotEqual(
            500, res.status_code,
            'Snippet BVA post test failed for missing sequences and bagfile')
        cleanup_db()

    def test_snippet_bva_metadata_aggregation_POST(self):
        prepare_db()
        sequences = [generate_test_sequence(x) for x in [13, 14, 16]]

        sequences[0]['extractors']['ext1']['metadata'] = {
            'disengagement': [{
                "error msg": "LogRequest topic unavailable",
                "type": "Unknown",
                "ts": 1558962227411
            }, {
                "error msg": "LogRequest topic unavailable",
                "type": "Unknown",
                "ts": 1558962227422
            }],
            'silent_testing': [{
                "type": "Unknown",
                "ts": 1558962227444
            }]
        }
        sequences[1]['extractors']['ext1']['metadata'] = {
            'disengagement': [{
                "error msg": "LogRequest topic unavailable",
                "type": "Unknown",
                "ts": 1558962227422
            }]
        }

        sequences[1]['extractors']['ext2']['metadata'] = {
            'dummymetadata': [{
                "msg": "Dummy",
                "ts": 1558962227466
            }]
        }

        sequences[2]['extractors']['ext1']['metadata'] = {
            'silent_testing': [{
                "type": "Unknown",
                "ts": 1558962227444
            }, {
                "type": "Unknown",
                "ts": 1558962229555
            }]
        }

        del sequences[0]['extractors']['ext1_3']
        del sequences[1]['extractors']['ext1_3']
        del sequences[2]['extractors']['ext1_3']

        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences(sequences)
        query_data = {'bagfile_guid': bagfile_guid, 'bva_limit': 5}

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(
            200, res.status_code,
            'Snippet BVA post test failed for sequence aggregation')

        res_json = res.json()['results']

        self.assertEqual(
            10, res_json[0]['bva_max'],
            'Snippet BVA post test failed for sequence aggregation. Aggregated BVA value is not correct.'
        )
        self.assertEqual(
            6, res_json[1]['bva_max'],
            'Snippet BVA post test failed for sequence aggregation. Aggregated BVA value is not correct.'
        )

        expected_metadata_child_bag_1 = {
            'ext1': {
                'disengagement': [{
                    'error msg': 'LogRequest topic unavailable',
                    'type': 'Unknown',
                    'ts': 1558962227411
                }, {
                    'error msg': 'LogRequest topic unavailable',
                    'type': 'Unknown',
                    'ts': 1558962227422
                }],
                'silent_testing': [{
                    'type': 'Unknown',
                    'ts': 1558962227444
                }]
            },
            'ext2': {
                'dummymetadata': [{
                    'msg': 'Dummy',
                    'ts': 1558962227466
                }]
            },
            'ext3': {}
        }
        self.assertDictEqual(
            expected_metadata_child_bag_1, res_json[0]['metadata'],
            'Snippet BVA post test failed for sequence aggregation. Metadata not aggregated correctly.'
        )

        expected_metadata_child_bag_2 = {
            'ext1': {
                'silent_testing': [{
                    'type': 'Unknown',
                    'ts': 1558962227444
                }, {
                    'type': 'Unknown',
                    'ts': 1558962229555
                }]
            },
            'ext2': {},
            'ext3': {}
        }
        self.assertDictEqual(
            expected_metadata_child_bag_2, res_json[1]['metadata'],
            'Snippet BVA post test failed for sequence aggregation. Metadata not aggregated correctly.'
        )

        cleanup_db()

    def test_snippet_bva_metadata_aggregation_extractors_POST(self):
        prepare_db()
        sequences = [generate_test_sequence(x) for x in [13, 14, 16]]

        sequences[0]['extractors']['ext1']['metadata'] = {
            'disengagement': [{
                "error msg": "LogRequest topic unavailable",
                "type": "Unknown",
                "ts": 1558962227411
            }, {
                "error msg": "LogRequest topic unavailable",
                "type": "Unknown",
                "ts": 1558962227422
            }],
            'silent_testing': [{
                "type": "Unknown",
                "ts": 1558962227444
            }]
        }
        sequences[1]['extractors']['ext1']['metadata'] = {
            'disengagement': [{
                "error msg": "LogRequest topic unavailable",
                "type": "Unknown",
                "ts": 1558962227422
            }]
        }

        sequences[1]['extractors']['ext2']['metadata'] = {
            'dummymetadata': [{
                "msg": "Dummy",
                "ts": 1558962227466
            }]
        }

        sequences[2]['extractors']['ext1']['metadata'] = {
            'silent_testing': [{
                "type": "Unknown",
                "ts": 1558962227444
            }, {
                "type": "Unknown",
                "ts": 1558962229555
            }]
        }

        del sequences[0]['extractors']['ext1_3']
        del sequences[1]['extractors']['ext1_3']
        del sequences[2]['extractors']['ext1_3']

        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences(sequences)
        query_data = {
            'bagfile_guid': bagfile_guid,
            'bva_limit': 5,
            'extractors': ['ext1']
        }

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(
            200, res.status_code,
            'Snippet BVA post test failed for sequence aggregation')

        res_json = res.json()['results']

        self.assertEqual(
            6, res_json[0]['bva_max'],
            'Snippet BVA post test failed for sequence aggregation. Aggregated BVA value is not correct.'
        )

        expected_metadata_child_bag_1 = {
            'ext1': {
                'disengagement': [{
                    'error msg': 'LogRequest topic unavailable',
                    'type': 'Unknown',
                    'ts': 1558962227411
                }, {
                    'error msg': 'LogRequest topic unavailable',
                    'type': 'Unknown',
                    'ts': 1558962227422
                }],
                'silent_testing': [{
                    'type': 'Unknown',
                    'ts': 1558962227444
                }]
            }
        }

        self.assertDictEqual(
            expected_metadata_child_bag_1, res_json[0]['metadata'],
            'Snippet BVA post test failed for sequence aggregation. Metadata not aggregated correctly.'
        )

        cleanup_db()

    def test_snippet_bva_metadata_aggregation_without_ts_POST(self):
        prepare_db()
        sequences = [generate_test_sequence(x) for x in [13, 14, 16]]

        sequences[0]['extractors']['ext1']['metadata'] = {
            'disengagement': [{
                "error msg": "LogRequest topic unavailable",
                "type": "Unknown"
            }]
        }
        sequences[1]['extractors']['ext1']['metadata'] = {
            'disengagement': [{
                "error msg": "LogRequest topic unavailable",
                "type": "Unknown"
            }]
        }

        sequences[1]['extractors']['ext2']['metadata'] = {
            'dummymetadata': [{
                "msg": "Dummy"
            }]
        }

        sequences[2]['extractors']['ext1']['metadata'] = {
            'silent_testing': [{
                "type": "Unknown"
            }, {
                "type": "Unknown"
            }]
        }

        connector = get_testdb_connector()
        with connector as con:
            con.add_sequences(sequences)
        query_data = {'bagfile_guid': bagfile_guid, 'bva_limit': 5}

        headers = auth_header('read:snippets')
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)

        self.assertEqual(
            200, res.status_code,
            'Snippet BVA post test failed for sequence aggregation')

        res_json = res.json()['results']

        self.assertEqual(
            10, res_json[0]['bva_max'],
            'Snippet BVA post test failed for sequence aggregation. Aggregated BVA value is not correct.'
        )
        self.assertEqual(
            6, res_json[1]['bva_max'],
            'Snippet BVA post test failed for sequence aggregation. Aggregated BVA value is not correct.'
        )

        cleanup_db()

    def test_snippet_bva_POST_missing_token(self):
        if disable_auth:
            return
        query_data = {
            'bagfile_guid': '233985c0-5f9d-4e46-9c7c-56fe58bce5ed',
            'bva_limit': 5
        }
        headers = {}
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)
        self.assertEqual(401, res.status_code,
                         'Successful access without token')

    def test_snippet_bva_POST_invalid_token(self):
        if disable_auth:
            return
        query_data = {
            'bagfile_guid': '233985c0-5f9d-4e46-9c7c-56fe58bce5ed',
            'bva_limit': 5
        }
        headers = auth_header('read:snippets')
        headers['authorization'] += 'x'
        res = requests.post(url=bva_snippet_url,
                            json=query_data,
                            headers=headers)
        self.assertEqual(401, res.status_code,
                         'Successful access without token')

    def test_snippet_bva_OPTIONS(self):
        res = requests.options(url=bva_snippet_url)
        self.assertEqual(200, res.status_code,
                         'Fetching Snippet BVA route options failed')
        expected_result = 'OPTIONS, POST'
        self.assertEqual(expected_result,
                         res.headers['Access-Control-Allow-Methods'],
                         'Fetching snippet bva route options failed')

    def test_snippet_bva_HTTPS(self):
        res = requests.options(url=https_url, verify=cert)
        self.assertEqual(200, res.status_code,
                         'HTTPS Fetching Snippet BVA route options failed')
        expected_result = 'OPTIONS, POST'
        self.assertEqual(expected_result,
                         res.headers['Access-Control-Allow-Methods'],
                         'Fetching snippet bva route options failed')


if __name__ == '__main__':
    unittest.main()
