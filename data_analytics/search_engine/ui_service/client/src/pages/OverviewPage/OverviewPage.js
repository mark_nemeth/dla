import React, { useEffect, useState } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import NavigationBar from '../../components/NavigationBar/NavigationBar';
import FileInformationSection from '../../components/FileInformationSection';
import LocationDetail from '../../components/LocationDetail';
import EventDetail from '../../components/EventDetail/EventDetail';
import ErrorBanner from '../../components/ErrorBanner';
import featureToggle from '../../config/featureToggleHelper';
import { FileTypes, getFileTypeLabel } from '../../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import { isObjectValid } from '../../utils/helperFunctions';
import DriveInformationSection from "../../components/DriveInformationSection";
import DriveDetail from "../../components/DriveDetail";
import JsonView from "../../components/JsonView";
import MapDetail from "../../components/MapDetail";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faAngleDoubleLeft, faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';
import './OverviewPage.scss';

export function OverviewPage(props) {

  useEffect(() => {
    let typeName = getFileTypeLabel(props.fileType);
    document.title = 'DANF - ' + typeName + ' Overview';
  }, [props.match.params.guid]);

  useEffect(() => {
    props.fetchFileByGuid(props.match.params.guid, props.fileType)
      .then((action) => {
        if (props.fileType === FileTypes.ORIGINALFILE || props.fileType === FileTypes.ALWAYSONFILE) {
          let bagfile = action.payload.bagfile;
          let bagPath = bagfile.hasOwnProperty("current_link") ? bagfile.current_link : bagfile.link;
          props.fetchFileByBagPath(bagPath, props.fileType)
        } else {
          action.payload.childbagfiles && action.payload.childbagfiles.forEach(childBag => {
            if (childBag.guid === props.match.params.guid) {
              props.fetchFileByBagPath(childBag.link, props.fileType)
            }
          });
        }
      }
      );
    if (props.fileType === FileTypes.ORIGINALFILE || props.fileType === FileTypes.ALWAYSONFILE) props.loadFlowRunsFromApi(props.match.params.guid);
  }, [props.match.params.guid]);

  const showLoading = !isObjectValid(props.overviewFile) || props.overviewFile.guid !== props.match.params.guid;
  const [isOpen, setIsOpen] = useState(false);
  const toggleSidebar = () => setIsOpen(!isOpen);

  return (
    <div className="overview-page">
      <div className="overview-page-action-bar">
        <NavigationBar navigate={props.history}
          currentFilePath={!isObjectValid(props.overviewFile) ? '' : (props.fileType === FileTypes.DRIVE ? props.overviewFile.file_path : props.overviewFile.link)}
          fileType={props.fileType} />
      </div>
      {showLoading ? (
        <div className="loading-spinner-container">
          <Spinner animation="border" variant="info" />
        </div>
      ) : (
          <div>
            <button className="toggle-button" onClick={toggleSidebar} title={props.fileType + " " + "information"}>

              {isOpen === false ?
                <FontAwesomeIcon className="toggle-side-info" icon={faAngleDoubleLeft} />
                :
                <FontAwesomeIcon className="toggle-side-info" icon={faAngleDoubleRight} />
              }
              <b> Information</b>
            </button>
            <div className="overview-page-content">
              {
                isOpen === false &&
                <div className="overview-page-content-info">
                  {props.fileType !== FileTypes.DRIVE &&
                    <FileInformationSection fileType={props.fileType} />
                  }
                  {props.fileType === FileTypes.DRIVE &&
                    <DriveInformationSection fileType={props.fileType} drive={props.overviewFile} />
                  }
                </div>
              }

              <div className="overview-page-content-detail">
                <Tabs defaultActiveKey={props.fileType === FileTypes.DRIVE ? "timeline" : "locations"} mountOnEnter={true}>
                  {props.fileType === FileTypes.DRIVE &&
                    <Tab eventKey="timeline" title="Timeline">
                      <div className="overview-page-content-detail-tab">
                        <DriveDetail />
                      </div>
                    </Tab>
                  }
                  {props.fileType === FileTypes.DRIVE &&
                    <Tab eventKey="json" title="JSON View">
                      <div className="overview-page-content-detail-tab">
                        <JsonView rootNodeName="drive" json={props.overviewFile} />
                      </div>
                    </Tab>
                  }
                  {props.fileType !== FileTypes.DRIVE &&
                    <Tab eventKey="locations" title="Locations">
                      <div className="overview-page-content-detail-tab">
                        <LocationDetail fileType={props.fileType} />
                      </div>
                    </Tab>}
                  {featureToggle.showLeafLetMap &&
                    props.fileType !== FileTypes.DRIVE &&
                    props.fileType === FileTypes.ALWAYSONFILE &&
                    <Tab eventKey="map" title="Map">
                      <MapDetail guid={props.match.params.guid} />
                    </Tab>
                  }
                  {props.fileType === FileTypes.CHILDBAGFILE &&
                    <Tab eventKey="event" title="Events">
                      <EventDetail events={props.overviewFile.metadata} />
                    </Tab>
                  }
                </Tabs>
              </div>
            </div>
          </div>
        )}
      <ErrorBanner />
    </div>
  );
}