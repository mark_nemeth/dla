from unittest import TestCase
from unittest.mock import patch, MagicMock, create_autospec

from fastapi.testclient import TestClient

from app.main import app
from app.models.bag import Bagfile, Childbagfile, BagFilterParams

client = TestClient(app)

child_1 = {
    "version": "1.0",
    "guid": "39a758db-7f24-4350-ae7a-d16259ecbe01",
    "parent_guid": "39a758db-7f24-4350-ae7a-d16259ecbe95",
    "link": "testdata",
    "file_name": "20200130_152349_cid_guest_drive_part2_0_EXTRACTEDBY_LogbookIssueInterface_at_20200130_152926.bag",
    "start": 1558962320000,
    "end": 1558962372000,
    "parent_index_start": 169,
    "parent_index_end": 220,
    "size": 32768,
    "vehicle_id_num": "V-123-234",
    "metadata": {
        "logbook": ""
    },
    "bva": 7,
    "is_test_data": 1
}

def test_get_bag():
    """
    - Given: the original get_bag function
    - When: client.get gets the path (which is used in the original
    function's router decorator)
    - Then: the status code get back with 200 which means okay and json
    text Hello word message
    :return:
    """
    response = client.get("/api/v2/metadata/project/danf/resource_type/bags/foo")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}


@patch('app.api.v2.endpoints.bag.get_database', autospec=True)
@patch('app.api.v2.endpoints.bag.get_bags_with_filters', autospec=True)
def test_get_bags(mock_get_bags_with_filters: MagicMock, mock_get_database: MagicMock):
    bag_1 = create_autospec(Bagfile)
    bag_2 = create_autospec(Childbagfile)
    bag_3 = create_autospec(Childbagfile)
    # mock_get_bags_with_filters.return_value = ([bag_1, bag_2, bag_3], 3)
    mock_get_bags_with_filters.return_value = ([child_1, child_1, child_1], 3)
    response = client.get("/api/v2/metadata/project/danf/resource_type/bags")
    mock_get_bags_with_filters.assert_called_once_with(mock_get_database.return_value, BagFilterParams(limit=50))
    assert response.status_code == 200
    TestCase().assertDictEqual({'bags': [{}, {}, {}], 'bags_count': 3}, response.json())

