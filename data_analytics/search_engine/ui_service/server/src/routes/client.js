import express from 'express';
import path from 'path';

import secured from './secure/secureRoute'
import loginHandler from './handler/loginHandler'

const router = express.Router();

/** Serve **/

/* Home Page. */
router.get('/', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/* Original Bagfile Page */
router.get('/originalfile/*', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/*AlwaysOn Bagfile Page */
router.get('/alwaysonfile/*', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/* Child Bagfile Page */
router.get('/childbagfile/*', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/* Drive Page */
router.get('/drive/*', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/* Support Page */
router.get('/support', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/* Reporting Page */
router.get('/reporting', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/* Announcements Page */
router.get('/announcements', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/* Ingests Page */
router.get('/ingest', secured(), function (req, res) {
  res.sendFile(path.join(__dirname, '../../clients/metadata/index.html'));
});

/** Login **/

// Perform the login, after login Auth0 will redirect to callback
router.get('/login', loginHandler.callLogin(), function (req, res) {
  res.redirect('/');
});

// Perform the final stage of authentication and redirect to previously requested URL or '/user'
router.get('/callback', function (req, res, next) {
  loginHandler.callAuthentication(req, res, next);
});

// Perform session logout and redirect to homepage
router.get('/logout', (req, res) => {
  loginHandler.callLogout(req, res);
});

export default router;