import React from 'react';
import Form from 'react-bootstrap/Form';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import {isObjectValid} from '../../../../utils/helperFunctions';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import './ByDurationFilter.scss';

export const STATIC_LIMITS = {
  ORIGINALFILE: {
    min: 0,
    max: 1500 * 60000
  },
  ALWAYSONFILE: {
    min: 0,
    max: 1500 * 60000
  },
  CHILD_BAGFILE: {
    min: 0,
    max: 5 * 60000
  },
  DRIVE: {
    min: 0,
    max: 1500 * 60000
  }
};

export function ByDurationFilter(props) {

  function toMillisec(value) {
    return {
      min: value.min * 60000,
      max: value.max * 60000
    }
  }

  function onRangeSelection(value) {
    value = toMillisec(value);
    if (isObjectValid(value)) props.filterByDuration(value);
    else props.filterByDuration({min: null, max: null})
  }

  function resetRangeSelection() {
    props.setCurrentDurationRange(props.limits);
    props.filterByDuration({min: null, max: null})
  }

  const subLabel = props.duration.min + ' - ' + (props.steps < 0.5 ? props.duration.max.toFixed(2) : props.duration.max) + ' m';
  const active = props.duration.min !== props.limits.min || props.duration.max !== props.limits.max;

  return (
    <CollapseContainer label={active ? "Duration :" + subLabel : "Duration"}
                       active={active}
                       resetCallback={resetRangeSelection}
    >
      <div className="track-container">
        <Form>
          <InputRange
            className="input-range"
            draggableTrack
            step={props.steps}
            maxValue={props.limits.max}
            minValue={props.limits.min}
            onChange={value => props.setCurrentDurationRange(value)}
            onChangeComplete={onRangeSelection}
            value={props.duration}
          />
        </Form>
      </div>
    </CollapseContainer>
  );
}