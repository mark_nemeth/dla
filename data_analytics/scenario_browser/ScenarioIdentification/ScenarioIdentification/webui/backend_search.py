"""
Backend functions for similarity search
"""
import numpy as np
import pandas as pd
import sklearn.neighbors as nei
import dash_table
from data import pivot_file, dyn_pivot, pivot_proximity

def search_traj(fname, ts, relevant_cols, n_nearest=6, leaf_size=2, dist_metric='euclidean'):
    '''
    Creates the distance tree and returns the nearest neighbors for trajectory feature
    '''
    pivot_keep = pivot_file[(pivot_file['Filename']==fname) & (pivot_file['time_id']==ts)]
    keep_cols = [col for col in pivot_file.columns if any(x in col for x in relevant_cols)]
    pivot_file_keep=pivot_file[keep_cols]
    tree_timetraj=nei.KDTree(pivot_file_keep,leaf_size=leaf_size,metric=dist_metric)
    
    X = pivot_keep[keep_cols]
    dist, ind = tree_timetraj.query(X, k=n_nearest)
    search_out = pivot_file.iloc[ind.tolist()[0]][['Filename', 'time_id']]
    search_out['time_id'] = search_out['time_id']*10
    search_out['Distance'] = dist.tolist()[0]
    search_out.rename(columns={"time_id":"TimeStamp"}, inplace=True)
    return search_out

def search_dyn(fname, ts, relevant_cols, n_nearest=6, leaf_size=2, dist_metric='euclidean'):
    dyn_keep = dyn_pivot[(dyn_pivot['Filename']==fname) & (dyn_pivot['time_id']==ts)]
    keep_cols = [col for col in dyn_pivot.columns if any(x in col for x in relevant_cols)]
    pivot_file_keep=dyn_pivot[keep_cols]
    tree_dyn=nei.KDTree(pivot_file_keep,leaf_size=leaf_size,metric=dist_metric)
    X = dyn_pivot[keep_cols]
    dist, ind = tree_dyn.query(X, k=n_nearest)
    search_out = dyn_pivot.iloc[ind.tolist()[0]][['Filename', 'time_id']]
    search_out['time_id'] = search_out['time_id']*10
    search_out['Distance'] = dist.tolist()[0]
    search_out.rename(columns={"time_id":"TimeStamp"}, inplace=True)
    return search_out

def search_proximity(fname, ts, relevant_cols, n_nearest=6, leaf_size=2, dist_metric='euclidean'):
    '''
    Creates the distance tree and returns the nearest neighbors for proximity feature.
    relevant cols should be from ['CROSSWALK', 'PARKINGSPACE']
    '''
    pivot_keep = pivot_proximity[(pivot_proximity['Filename']==fname) & (pivot_proximity['time_id']==ts)]
    keep_cols = [col for col in pivot_proximity.columns if any(x in col for x in relevant_cols)]
    pivot_proximity_keep=pivot_proximity[keep_cols]
    tree_timetraj=nei.KDTree(pivot_proximity_keep,leaf_size=leaf_size,metric=dist_metric)
    
    X = pivot_keep[keep_cols]
    dist, ind = tree_timetraj.query(X, k=n_nearest)
    search_out = pivot_proximity.iloc[ind.tolist()[0]][['Filename', 'time_id']]
    search_out['time_id'] = search_out['time_id']*10
    search_out['Distance'] = dist.tolist()[0]
    search_out.rename(columns={"time_id":"TimeStamp"}, inplace=True)
    return search_out

def generate_table(dataframe, max_rows=10):
    return dash_table.DataTable(
        id='table',
        columns=[{"name": i, "id": i} for i in dataframe.columns],
        data=dataframe.to_dict('records'),
        style_as_list_view=False,
        style_cell_conditional=[
            {'if': {'column_id': 'FileName'},
             'width': '40%'},
            {'if': {'column_id': 'TimeStamp'},
             'width': '10%'},
        ],
        style_header={
            'backgroundColor': 'rgb(30, 30, 30)',
            'fontWeight': 'bold'
                     },
        style_cell={
            'backgroundColor': 'rgb(50, 50, 50)',
            'color': 'white',
            'padding': '5px'
            }
        )