import unittest
from unittest.mock import Mock, patch

from handlers.cache_distinct_values_handler import CacheDistinctValuesHandler


class TestTimestampOutOfRangeException(unittest.TestCase):
    def test_update_cache_should_make_correct_metadata_api_call(self):
        handler = CacheDistinctValuesHandler("col", "f")
        handler.db_data_handler.get_distinct_field_values = Mock(return_value=["v1", "null", "  ", "v2", ""])
        handler.db_data_handler.search_documents = Mock(
            return_value=[{"guid": "a guid", "values": [], "type": "col_f_cache"}])
        with patch.object(handler.db_data_handler, 'update_documents') as mock:
            handler.update_cache()
            expected_documents = [{'type': "col_f_cache", 'values': ["v1", "v2"]}]
            mock.assert_called_once_with(["a guid"], expected_documents, True)


if __name__ == '__main__':
    unittest.main()
