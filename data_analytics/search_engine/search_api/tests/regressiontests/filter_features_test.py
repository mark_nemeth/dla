"""Unittests for DBConnector"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
os.environ["DANF_ENV"] = "local"

import unittest
from datetime import timedelta

import requests
from DANFUtils.logging import logger

# increase log level for tests
from .testutils import get_testdb_connector
from .utils import generate_feature_buckets

logger.set_log_level('debug')

base_url = 'http://localhost:8889'
filter_features_url = base_url + '/filter/features'

bag = {
    "guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "link": "/test/test/test/test/test/test/test",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "version": "1.0",
    "is_test_data": 1
}
LEN_IN_S = 3600
feat_bucket_docs = generate_feature_buckets(bag["guid"],
                                            timedelta(0, LEN_IN_S))


class TestFeatureSearchChildBagfilesGET(unittest.TestCase):
    def setUp(self):
        self.connector = get_testdb_connector()
        self.cleanup_db()
        # add
        self.connector.add_bagfiles([bag])
        self.connector.add_feature_buckets(feat_bucket_docs)

    def tearDown(self):
        self.cleanup_db()

    def cleanup_db(self):
        with self.connector as con:
            con.db['bagfiles'].delete_many({"is_test_data": 1})
            con.db['feature_buckets'].delete_many({"is_test_data": 1})

    def test_filter_features(self):
        query_data = {'bagfile_guid': bag["guid"], "features": "acc,vel"}
        res = requests.get(url=filter_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'Feature filtering failed')
        self.assertEqual(LEN_IN_S - 1, len(res_json["ts"]),
                         'Feature filtering failed')
        self.assertCountEqual(["acc", "vel", "bagfile_guid", "ts"],
                              res_json.keys(), 'Feature filtering failed')

    def test_filter_features_empty(self):
        query_data = {}
        res = requests.get(url=filter_features_url, params=query_data)
        self.assertEqual(500, res.status_code,
                         'Feature should have been failing, but didnot')

    def test_filter_features_no_features(self):
        query_data = {'bagfile_guid': bag["guid"]}
        res = requests.get(url=filter_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'Feature filtering failed')
        self.assertCountEqual(["bagfile_guid", "ts"], res_json.keys(),
                              'Feature filtering failed')

    def test_filter_features_invalid_features(self):
        query_data = {
            'bagfile_guid': bag["guid"],
            "features": "acc,vel,test,dummy"
        }
        res = requests.get(url=filter_features_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'Feature filtering failed')
        self.assertEqual(LEN_IN_S - 1, len(res_json["ts"]),
                         'Feature filtering failed')
        self.assertCountEqual(["acc", "vel", "bagfile_guid", "ts"],
                              res_json.keys(), 'Feature filtering failed')
