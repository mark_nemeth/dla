export const CountdownStates = Object.freeze({
    STARTED: 1,
    IDLE: 3
});


export class Countdown {

    constructor(countdownTimeInSeconds) {

        this.interval = 1 * 1000;

        this.countdownTimeInSeconds = countdownTimeInSeconds;
        this.secondsPast = 0;
        this.secondsUntilFinished = this.countdownTimeInSeconds;

        this.countdownEventListeners = [];
        this.countdownCompletedListeners = [];

        this.timeout = null;
        this.state = CountdownStates.IDLE;
    }

    addCountdownEventListener(listener) {
        this.countdownEventListeners.push(listener);
    }

    notifyCoundDownEventListeners() {
        this.countdownEventListeners.forEach(listener => listener(this.secondsUntilFinished));
    }

    addCountdownCompletedEventListener(listener) {
        this.countdownCompletedListeners.push(listener);
    }

    notifyCountDownCompletedEventListeners() {
        this.countdownCompletedListeners.forEach(listener => listener(this.state));
    }

    startNewCountdown() {
        if (this.state === CountdownStates.STARTED) {
            return;
        }

        this.state = CountdownStates.STARTED;
        this.secondsPast = 0;
        this.secondsUntilFinished = this.countdownTimeInSeconds;
        this.notifyCoundDownEventListeners();
        this.scheduleNextTick();
    }

    restartCountdown(){
        this.clearCountdown();
        this.startNewCountdown();
    }

    tick() {
        this.timeout = null;
        this.secondsPast++;
        this.secondsUntilFinished = this.countdownTimeInSeconds - this.secondsPast;

        if (this.secondsUntilFinished === 0) {
            this.clearCountdown();
            this.notifyCountDownCompletedEventListeners();
            return;
        }

        this.notifyCoundDownEventListeners();
        this.scheduleNextTick();
    }

    scheduleNextTick() {
        if (this.timeout) {
            return;
        }
        this.timeout = setTimeout(() => { this.tick() }, this.interval);
    }

    clearCountdown() {
        clearTimeout(this.timeout);
        this.timeout = null;
        this.secondsPast = 0;
        this.secondsUntilFinished = this.countdownTimeInSeconds;
        this.state = CountdownStates.IDLE;
    }
}