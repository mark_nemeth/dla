import { connect } from 'react-redux';
import { ReportingPage } from './ReportingPage';
import { createReport, exportReport, setTripFilterSelection, setTripFilterState, selectReportingLocation, selectReportType, selectReportingDate } from '../../redux/Reporting/ReportingActions';

function mapStateToProps(state, ownProps) {
  const showReport = state.reporting.showReport || false;
  const enableExport = state.reporting.enableExport || false;
  const error = state.error || false;
  const errorMessage = state.errorMessage || '';
  const showLoading = state.showLoading || false;
  const locations = state.reporting.locations;
  const selectedLocation = state.reporting.globalFilters.location;
  const reportTypes = state.reporting.reportTypes;
  const selectedReportType = state.reporting.globalFilters.report_type;
  const selectedStartDate = state.reporting.globalFilters.start;
  const selectedEndDate = state.reporting.globalFilters.end;
  const tripFilters = state.reporting.tripFilters;
  //const tripFiltersVisible = [true, true, true, selectedReportType!=='rp'];
  const kpis = state.reporting.data.kpis;
  const simpleTables = state.reporting.data.simpleTables;
  const aggregationTables = state.reporting.data.aggregationTables;
  return { showReport, enableExport, error, errorMessage, showLoading, selectedLocation, locations, selectedReportType, reportTypes, selectedStartDate, selectedEndDate, tripFilters, kpis, simpleTables, aggregationTables };
}

export default connect(
  mapStateToProps,
  { createReport, exportReport, setTripFilterSelection, setTripFilterState, selectReportingLocation, selectReportType, selectReportingDate }
)(ReportingPage);
