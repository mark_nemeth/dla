"""data_handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import requests
import json

from ContainerClient.config.config_handler import config_dict
from ContainerClient.handlers.auth_handler import create_session
from ContainerClient.handlers.base_request_handler import BaseRequestHandler, HttpMethod
from DANFUtils.constants import *
from DANFUtils.exceptions import RESTException
from DANFUtils.logging import logger


class DataHandler(BaseRequestHandler):
    """
    Data Handler class
    """

    def __init__(self):
        super().__init__()

        self.BASE_URL = config_dict.get_value('DATA_HANDLER_BASE_URL')
        self.HANDLER_URL = self.BASE_URL + "delete_record"
        self.SIMULATE_FILE_URL = self.BASE_URL + "testing/simulate-new-file"
        self.FILE_CHECK_URL = self.BASE_URL + "testing/file-check/"

    def _init_session(self):
        self.AUTH = config_dict.get_value('AUTH')
        if self.AUTH == "auth0":
            token_url = config_dict.get_value('AUTH_TOKEN_URL')
            client_credentials_id = config_dict.get_value('AUTH_DATAHANDLER_CLIENT_CRED_ID')
            client_credentials_secret = config_dict.get_value('AUTH_DATAHANDLER_CLIENT_CRED_SECRET')
            audience = config_dict.get_value('AUTH_DATAHANDLER_AUDIENCE')
            if token_url and client_credentials_id and client_credentials_secret and audience:
                self.session = create_session(token_url=token_url,
                                              client_credentials_id=client_credentials_id,
                                              client_credentials_secret=client_credentials_secret,
                                              audience=audience)
        if 'session' not in self.__dict__:
            self.session = requests.Session()

        self.set_retries()

    def delete_event_record(self, event_id, pop_receipt):
        # validate input data
        assert isinstance(event_id, str), 'event_id has to be of type string.'
        assert isinstance(pop_receipt, str), 'pop_receipt has to be of type string'

        data_dict = {'event_id': event_id,
                     'pop_receipt': pop_receipt}

        res = self.send_request(HttpMethod.DELETE, url=self.HANDLER_URL, params=data_dict)

        if res.status_code == REQ_STATUS_CODE_OK:
            logger.info("successfully deleted the event message from storage queue")
            return res.status_code
        else:
            # handle errors
            raise RESTException(self.COMPONENT, HttpMethod.DELETE.value, self.HANDLER_URL, res.status_code, res.text)

    def simulate_new_file(self, path, output_dir):

        # validate input data
        assert isinstance(path, str), 'path has to be of type string.'
        assert isinstance(output_dir, str), 'output directory has to be of type string'

        data_dict = {'path': path,
                     'output_directory': output_dir}

        res = self.send_request(HttpMethod.POST,
                                url=self.SIMULATE_FILE_URL, params=data_dict)

        if res.status_code != REQ_STATUS_CODE_RESOURCE_CREATED:
            raise RESTException(self.COMPONENT,
                                HttpMethod.POST.value,
                                self.SIMULATE_FILE_URL,
                                res.status_code,
                                res.text)
        data = json.loads(res.text)['results'][0]
        return data, res

    def file_check(self, paths, delete_files):
        # validate input data
        assert isinstance(paths, list), 'path has to be of type list'
        assert isinstance(delete_files, bool), 'output directory has to be of type string'

        data_dict = {'paths': paths,
                     'delete_files': delete_files}

        res = self.send_request(HttpMethod.POST,
                                url=self.SIMULATE_FILE_URL, params=data_dict)

        if res.status_code != REQ_STATUS_CODE_RESOURCE_CREATED:
            raise RESTException(self.COMPONENT,
                                HttpMethod.POST.value,
                                self.FILE_CHECK_URL,
                                res.status_code,
                                res.text)
        data = json.loads(res.text)['results'][0]
        return data, res
