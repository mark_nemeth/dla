#include <stdio.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/duration.h>
#include <chrono>
#include <regex>

using namespace std;
using namespace chrono;

typedef struct statS
{
	long long count = 0;
	uint32_t minSize = 0xFFFFFFFF;
	uint32_t maxSize = 0;
	long long size = 0;
	ros::Time start = ros::TIME_MAX;
	ros::Time end = ros::TIME_MIN;
	long long MPS; // Messages per second
	long long BPS; // Bytes per second
	long long maxBPS; // Max bytes per second
} statT;

typedef struct dbEntryS
{
	string topicName;
	string bagFileName;
	ros::Time bagFileTime = ros::TIME_MIN;
	statT stat;
	string classification;
	string whitelist;
	string comment;

} dbEntryT;

void saveSCV(string filePath, map<string, dbEntryT> db)
{
	ofstream dbFile(filePath);

	dbFile << "topic" << ";";
	dbFile << "bag file" << ";";
	dbFile << "bag file time" << ";";
	dbFile << "min msg size" << ";";
	dbFile << "max msg size" << ";";
	dbFile << "message rate [msgs/s]" << ";";
	dbFile << "data rate [B/s]" << ";";
	dbFile << "max data rate [B/s]" << ";";
	dbFile << "classification" << ";";
	dbFile << "whitelist" << ";";
	dbFile << "comment";
	dbFile << endl;

	for (auto& kv : db)
	{
		dbEntryT& e = kv.second;

		dbFile << e.topicName << ";";
		dbFile << e.bagFileName << ";";
		if (!e.bagFileName.empty())
		{
//			time_t secsSinceEpoch = e.bagFileTime.sec;
//			dbFile << put_time(localtime(&secsSinceEpoch), "%F %R") << ";";
			dbFile << e.bagFileTime.sec << ";";
			dbFile << e.stat.minSize << ";";
			dbFile << e.stat.maxSize << ";";
			dbFile << e.stat.MPS << ";";
			dbFile << e.stat.BPS << ";";
			dbFile << e.stat.maxBPS << ";";
		}
		else
		{
			dbFile << ";";
			dbFile << ";";
			dbFile << ";";
			dbFile << ";";
			dbFile << ";";
			dbFile << ";";
		}
		dbFile << e.classification << ";";
		dbFile << e.whitelist << ";";
		dbFile << e.comment;
		dbFile << endl;
	}

	cout << "Saved " << filePath << endl;
}

int main(int argc, char **argv)
{
	map<string, dbEntryT> db;

	// Read .csv
	try
	{
		string filePath("db.csv");
		cout << "Reading " << filePath << endl;

		ifstream file(filePath);
		string line;
		getline(file, line);
		while (getline(file, line))
		{
			vector<string> cols;
			stringstream ss(line);
			string s;
			while (getline(ss, s, ';'))
				cols.push_back(s);

			dbEntryT& e = db[cols[0]];
			e.topicName = cols[0];
			e.bagFileName = cols[1];
			if (!e.bagFileName.empty())
			{
				e.bagFileTime = ros::Time(stoi(cols[2]));
				e.stat.minSize = stoi(cols[3]);
				e.stat.maxSize = stoi(cols[4]);
				e.stat.MPS = stoi(cols[5]);
				e.stat.BPS = stoi(cols[6]);
				e.stat.maxBPS = stoi(cols[7]);
			}
			e.classification = cols[8];
			e.whitelist = cols[9];
			e.comment = cols[10];
		}
	}
	catch (exception& ex)
	{
		cout << ex.what() << endl;
	}

	// Read whitelist.config
	for (int i = 1; i < argc; i++)
	{
		if (regex_match(argv[i], regex(".+\\.config")))
		{
			// Clear whitelist flag
			for (auto& kv : db)
			{
				dbEntryT& e = kv.second;
				e.whitelist.clear();
			}

			string filePath(argv[i]);
			cout << "Reading " << filePath << endl;

			ifstream file(filePath);
			string topicName;
			while (getline(file, topicName))
			{
				dbEntryT& e = db[topicName];
				e.topicName = topicName;
				e.whitelist = "x";
			}
		}
	}

	// Iterate .bag files
	for (int i = 1; i < argc; i++)
	{
		if (!regex_match(argv[i], regex(".+\\.bag")))
			continue;

		string bagFilePath(argv[i]);
		string bagFileName(basename(bagFilePath.c_str()));

		long long fileSize;
		{
		    std::ifstream in(bagFilePath, std::ifstream::ate | std::ifstream::binary);
		    fileSize = in.tellg();
		}

		cout << "Scanning " << bagFilePath << " size: " << (fileSize / 1024 / 1024) << "MB";
		flush(cout);

		rosbag::Bag bag;
		try
		{
			bag.open(bagFilePath);
		}
		catch (exception& ex)
		{
			cout << " Error: " << ex.what() << endl;
			continue;
		}
		rosbag::View view(bag);

		map<string, statT> bagStats;

		ros::Time maxTime = view.getBeginTime();
		maxTime.sec += 30;
		long long maxSize = 5 * 1024llu * 1024llu * 1024llu;

		// Scan all topics
		long long messageCount = 0;
		long long processedSize = 0;
		auto t1 = high_resolution_clock::now();
		for (rosbag::MessageInstance const m: view)
		{
			statT& stat = bagStats[m.getTopic()];

			stat.count++;
			stat.minSize = min(stat.minSize, m.size());
			stat.maxSize = max(stat.maxSize, m.size());
			stat.size += m.size();

			stat.start = min(stat.start, m.getTime());
			stat.end = max(stat.end, m.getTime());

			processedSize += m.size();
			messageCount++;

			if (maxTime < m.getTime() || maxSize < processedSize)
				break;
		}
		auto t2 = high_resolution_clock::now();

		auto dt = chrono::duration_cast<seconds>(t2 - t1).count();
		auto MBPS = 0 < dt ? processedSize / dt / 1024 / 1024 : 0;
		cout << " messages: " << messageCount << " t: " << dt << "s Throughput: " << MBPS << "MB/s " << endl;

		// Output statistics
		for (auto& kv : bagStats)
		{
			auto topicName = kv.first;
			statT& stat = kv.second;

			double dt = (stat.end - stat.start).toSec();
			stat.MPS = dt == 0 ? 0 : stat.count / dt;
			stat.BPS = dt == 0 ? 0 : stat.size / dt;
		}

		// Integrate
		for (auto& kv : bagStats)
		{
			auto topicName = kv.first;
			statT& stat = kv.second;

			dbEntryT& e = db[topicName];
			if (e.bagFileTime < view.getBeginTime())
			{
				e.topicName = topicName;
				e.bagFileTime = view.getBeginTime();
				e.bagFileName = bagFileName;
				long long maxBPS = max(stat.BPS, e.stat.maxBPS);
				e.stat = stat;
				e.stat.maxBPS = maxBPS;
			}
		}

		bag.close();
	}

	// Save CSV
	saveSCV("db.csv", db);

	return 0;
}

