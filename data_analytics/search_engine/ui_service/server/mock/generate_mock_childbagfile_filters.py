import random
import uuid
import json
import os

OUTPUT_FILENAME = "childbagfile_filters.json"
VERSION = "0.1"

childbagfile_filters_info = {
  "duration_min_max": [
      3612,
      2130000
  ],
  "locations": [
      "DE"
  ],
  "size_min_max": [
      123456,
      200021615933
  ],
  "topics": [
      "/licam2_DaddyROSGateway/mrc_lut_rectificationMRCRL1ParameterConfig/parameter_descriptions",
      "/maps/git/runtime_diff",
      "/licam2_DaddyROSGateway/CalibLidarCommunicatorLidarVeloBumperFLParameterConfig/parameter_descriptions",
      "/licam1_DaddyROSGateway/PoleDetectionLidarVeloBumperFRParameterConfig/parameter_descriptions",
      "/introspection/loc",
      "/vulnerable_road_users/PedestrianSkeletonService/MRCFL1/PedestrianSkeletonEvent",
      "/lidar/Stixel/LidarVeloRoofRL/StixelMap",
      "/licam2_DaddyROSGateway/SemanticsGieLidarVeloRoofRLParameterConfig/parameter_updates",
      "/radar/RadarARS4xxNearRangeService/47/ARS4xxTargetListNearInterface",
      "/sem/SemClientService/PreprocessingArs_RadarMmrFL_NW/SemClientInterface",
      "/licam1_DaddyROSGateway/DynamicObjectDetectionLidarVeloRoofRRParameterConfig/parameter_descriptions",
      "/mops_lidar_stixel/introspection_data/LidarVeloRoofFR",
      "/diagnostics",
      "/CAN/Infrastructure/ADCo_Sig"
  ],
  "vehicle_ids": [
      "lab02",
      "v222-5253",
      "v222-5215",
      "V-123-234",
      "unknown",
      "v222-5214",
      "v222-5252",
      "v222-5251",
      "v222-5217"
        ]
  }
result = {"results": childbagfile_filters_info}

outpath = os.path.join("public/mockdata")
if not os.path.exists(outpath):
    os.makedirs(outpath)

outfilePath = os.path.join(outpath, OUTPUT_FILENAME)
with open(outfilePath, 'w') as outfile:
    json.dump(result, outfile, indent=4)

print("Mock json file created: " + outfilePath)
