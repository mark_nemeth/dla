"""Handler class for crawler"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from handlers.db_data_handler import DBDataHandler


class CrawlerHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def fetch_bagfiles(self):
        query = {'is_deleted': {'$ne': True}}
        bagfiles, total = self.db_data_handler.search_bagfiles(
            query=query, limit=0,
            projection={'current_link': 1})
        return [bagfile.get('current_link', '') for bagfile in bagfiles]
