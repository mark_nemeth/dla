import pandas as pd
import shapely
from shapely.ops import transform
from pyproj import Transformer, Proj
import geopandas as gpd
import psycopg2


class MapObjectExtractor:
    '''Extracts map object geometries from postgres database'''

    def __init__(self, 
                 dbname='mapsdb', #postgres
                 user='postgres', 
                 host='localhost', 
                 pw='postgres', #1234
                 to_csv=False, 
                 path='../Data/map_objects',
                 prox_epsg=3310):
        self.dbname = dbname
        self.user = user
        self.host = host
        self.password = pw
        self.path = path
        self.epsg = prox_epsg
        self.db_rows = self.db_connect()
        self.mapobjects_df = self.convert_to_df()
        self.mapobjects_gdf = self.convert_to_gdf()
        self.mapgdf_filtered = self.filter_objs()
        self.get_latlon()
        self.project_gdf()
        for i in MapObjectSelected:
            self.mapgdf_filtered[MapObjectType[i]+'_DIST'] = self.get_proximities(MapObjectType[i])
        if to_csv:
            self.save_gdf_csv()

    def db_connect(self):
        dbstr ="dbname='%s' user='%s' host='%s' password='%s'" % (self.dbname, self.user, self.host, self.password)
        try:
            conn = psycopg2.connect(dbstr)
            #conn = psycopg2.connect("dbname='mapsdb' user='postgres' host='localhost' password='postgres'")
        except:
            print("database connection failed!")

        cur = conn.cursor()
        cur.execute("""SELECT objid , type , ST_AsText(geom) FROM mapobjects""") #traffic_light
        return cur.fetchall()

    def convert_to_df(self):
        mapobjects_objid = []
        mapobjects_type = []
        mapobjects_geom = []
        for objid, objtype, objgeom in self.db_rows:
            mapobjects_objid.append(objid)
            mapobjects_type.append(objtype)
            mapobjects_geom.append(objgeom)

        mapobjects_df = pd.DataFrame({
            'id':mapobjects_objid,
            'type':mapobjects_type,
            'geom':mapobjects_geom
        })
        return mapobjects_df

    def convert_to_gdf(self):
        geom_list = []
        for idx, row in self.mapobjects_df.iterrows():
            if row['geom']:
                geom_list.append(shapely.wkt.loads(str(row['geom'])))
            else:
                geom_list.append(None)

        mapobjects_gdf = gpd.GeoDataFrame(self.mapobjects_df, geometry=geom_list, crs={'init':'epsg:4326'})
        mapobjects_gdf['centroid'] = mapobjects_gdf['geometry'].centroid
        mapobjects_gdf['object_type'] = mapobjects_gdf['type'].apply(lambda _: MapObjectType[_])
        mapobjects_gdf.drop(columns=['geom'], inplace=True)
        return mapobjects_gdf

    def filter_objs(self):
        mapobjects_gdf_filtered = self.mapobjects_gdf[(self.mapobjects_gdf['type'].isin(MapObjectSelected))].copy()
        return mapobjects_gdf_filtered

    def get_latlon(self):
        self.mapgdf_filtered['Lat'] = self.mapgdf_filtered['centroid'].apply(get_lat)
        self.mapgdf_filtered['Lon'] = self.mapgdf_filtered['centroid'].apply(get_lon)

    def save_gdf_csv(self):
        #Drop columns other than types, lat, and lon since gdf can't be reloaded from csv
        df_out = self.mapgdf_filtered.copy()
        df_out.drop(columns=['geometry','centroid'],inplace=True)
        df_out.to_csv(self.path+'.csv')

    def project_gdf(self):
        #EPSG 3310 (default) works for CA.  Different projections needed for different areas
        #This should be easily possible through simple lat/lon filtering
        self.mapgdf_filtered.to_crs(epsg=self.epsg, inplace=True)
        
    def get_proximities(self, obj_type):
        # Computes the minimum distance from a path to a single object type
        # path is a shapely linestring with epsg 4326 (standard lat/lon floats)
        # obj_type is the string matching the map object type

        temp_gdf = self.mapgdf_filtered[self.mapgdf_filtered['object_type'] == obj_type].copy()
        obj_locs = shapely.geometry.MultiPoint(temp_gdf.centroid.tolist())
        return self.mapgdf_filtered.distance(obj_locs)

def get_lon(point):
    return point.coords.xy[0][0]

def get_lat(point):
    return point.coords.xy[1][0]

MapObjectType = {
    0:'UNKNOWN',
    1:'STOPLINE',
    2:'TREE',
    3:'STREETLAMP',
    4:'BARRIER',
    5:'PARKINGSPACE',
    6:'CROSSWALK',
    7:'ISLAND',
    8:'POLE',
    9:'ROADMARK',
    10:'OBSTACLE',
    11:'GUARDRAIL',
    12:'FENCE',
    13:'ARROW_STRAIGHT_LEFT',
    14:'ARROW_STRAIGHT_RIGHT',
    15:'ARROW_STRAIGHT',
    16:'ARROW_LEFT',
    17:'ARROW_RIGHT',
    18:'TRAFFICLIGHT',
    19:'TRAFFICSIGN',
    20:'ARROW_MERGE_LEFT',
    21:'ARROW_MERGE_RIGHT',
    22:'VEGETATION',
    23:'RAILWAY',
    24:'KEEP_CLEAR',
    25:'BUILDING',
    26:'ARROW_STRAIGHT_LEFT_RIGHT',
    27:'ARROW_LEFT_RIGHT',
    28:'TRAFFICLIGHT_BULB',
    29:'CROSSWALK_START',
    30:'CROSSWALK_END',
    31:'KEEP_CLEAR_START',
    32:'KEEP_CLEAR_END',
    33:'LEARNED_VIRTUAL_LINE',
    34:'WALL',
    35:'CROSSWALK_STRIPE',
    36:'DELINEATOR',
    37:'HYDRANT',
    38:'VIRTUAL_WAIT_LINE_BEGIN',
    39:'VIRTUAL_WAIT_LINE_END',
    40:'UNKNOWN_CONTROLLER',
    41:'YIELD_LINE',
    42:'VIRTUAL_STOPLINE',
    43:'VIRTUAL_CROSSWALK_START',
    44:'VIRTUAL_CROSSWALK_END',
    45:'VIRTUAL_KEEP_CLEAR_START',
    46:'VIRTUAL_KEEP_CLEAR_END',
    47:'VIRTUAL_YIELD_LINE',
    48:'VIRTUAL_REFERENCE_LINE',
    49:'VIRTUAL_CROSSWALK',
    50:'PEDESTRIAN_YIELD_LINE',
    51:'VIRTUAL_PEDESTRIAN_YIELD_LINE',
    52:'ARROW_UTURN',
    100:'PLOCL_CRR_POINT',
    101:'PLOCL_CRR_STRAIGHT',
    102:'PLOCL_LANE_MARKING',
    103:'PLOCL_POLE',
    104:'PLOCL_TRAFFIC_LIGHT',
    105:'PLOCL_TRAFFIC_SIGN',
}

MapObjectSelected = [1, 5, 6, 18, 19, 23]

if __name__ == "__main__":
    map_extractor = MapObjectExtractor(to_csv=True)