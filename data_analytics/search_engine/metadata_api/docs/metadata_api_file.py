"""
Data Loop Catalog Service prototype 0.2.6
OpenAPI specifications of file endpoints to be implemented in Metadata API and Search API

$ pip install fastapi
$ pip install uvicorn
$ uvicorn metadata_api_file:app --reload

http://127.0.0.1:8000/docs or http://127.0.0.1:8000/redoc


TODO items to be implemented after MVP:
 - Support paging (skip/limit) and sorting in get("/file/") endpoint
 - OAuth2 / GAS authentication & authorization
 - Introduce additional filters (file_size, entry_created_by, parent)
 - Additional metadata (drive_id, project, commit_hash, custom...)
"""
import re
from datetime import datetime
from typing import List, Set
from uuid import NAMESPACE_URL, uuid3

from fastapi import FastAPI, HTTPException, Query, Response, status
from pydantic import BaseModel, Field, UUID3

example_regex_url = "/baseinfraabtraw03sadev/20210504/*\\.bag"
example_original_url = "/baseinfraabtraw03sadev/20210504/20200528_135052_alwayson.bag"
example_destination_url = "/baseinfraabtraw03sadev/20210504/20200528_135052_alwayson.bag"
example_file = {
    "file_guid": uuid3(NAMESPACE_URL, example_original_url),
    "file_type": "rosbag",
    "file_size": 70078524328,
    "urls": [
        {
            "accessor_url": example_original_url,
            "creation_date": 1234567890000,
            "entry_created_by": "Ingest: <IngestID>: <DriveID>/<IngestSessionID>",
            "url_status": "Moved",
            "last_changed": 1234567890000
        },
        {
            "accessor_url": example_destination_url,
            "creation_date": 1234567890000,
            "entry_created_by": "TransferAPI: 1234",
            "url_status": "Available",
            "last_changed": 1234567890000
        },
    ],
}

root_uuid = uuid3(NAMESPACE_URL, "https://halfdome.bosch.com/")


class FileUrlInCreate(BaseModel):
    accessor_url: str = Field(..., example=example_file["urls"][0]["accessor_url"])
    creation_date: int = Field(..., example=example_file["urls"][0]["creation_date"])
    entry_created_by: str = Field(..., example=example_file["urls"][0]["entry_created_by"])
    url_status: str = Field("Available", example="TransferAPI: Moved")


class FileUrl(FileUrlInCreate):
    last_changed: int = 1234567890000


class FileInCreate(BaseModel):
    parent_guid: UUID3 = Field(root_uuid, example=uuid3(NAMESPACE_URL, str(root_uuid)))
    file_type: str = Field("data", example=example_file["file_type"])
    file_size: int = Field(..., example=example_file["file_size"])
    tags: Set[str] = Field([], example=example_file["tags"])


class File(FileInCreate):
    file_guid: UUID3 = Field(..., example=example_file["file_guid"])
    urls: List[FileUrl] = Field([], example=example_file["urls"])


class FileFilterParams(BaseModel):
    file_type: str = Field(None, example=example_file["file_type"])
    url: str = Field(None, example=example_file["urls"][0]["accessor_url"])
    tags: Set[str] = Field(None, example=example_file["tags"])
    url_status: str = Field("Available", example=example_file["urls"][0]["accessor_url"])
    regex_url: str = Field(None, example=example_regex_url)


class ManyFilesInResponse(BaseModel):
    results: List[File] = Field(..., example=[example_file])
    total: int = Field(..., example=1)


class ResultsFile(BaseModel):
    results: File


app = FastAPI(
    title="Data Loop Catalog Service prototype",
    description="OpenAPI specifications of file endpoints to be implemented in Metadata API and Search API",
    version="0.2.6"
)

files = {
    example_file["file_guid"]: File(**example_file)
}


# Files

# Implemented in Metadata API
@app.put("/file/{file_guid}", tags=["files"])
def put_file(file_guid: UUID3, file: FileInCreate):
    """
    Create (PUT / Upsert) a file catalog item with all the information:

    - **file_guid**: uuid3(NAMESPACE_URL, <original_url>)
    - **parent_guid**: (optional) file_guid of parent/source file
    - **file_type**: (optional) str of file type. Defaults to "data"
    - **file_size**: int size of file in bytes
    - **tags**: (optional) Set[str] unique tag strings for this item
    \f
    :param file_guid: uuid3 of file catalog item
    :param file: file catalog item
    """
    new_file = File(file_guid=file_guid, parent_guid=file.parent_guid, file_type=file.file_type,
                    file_size=file.file_size, tags=file.tags)

    files[file_guid] = new_file

    return Response(status_code=status.HTTP_204_NO_CONTENT)


# Implemented in Search API
@app.get("/file/", response_model=ManyFilesInResponse, tags=["files"])
def read_files(
        file_type: str = Query(None, title="File Type Query Filter"),
        url: str = Query(None, title="File Access URL Query Filter"),
        tags: List[str] = Query(None, title="File Tags Query Filter"),
        url_status: str = Query("Available", title="File and URL Status Query Filter"),
        regex_url: str = Query(None, title="File Access URL Regex Query Filter")
):
    """
    List (GET / Query) matching file catalog items:

    - **file_type**: (optional) str file type filter
    - **url**: (optional) str URL filter matching files with substrings in any urls.accessor_url
    - **tags**: (optional) Set[str] unique tag filters matching files with all tags
    - **url_status**: (optional) str URL status filter matching files and urls with urls.status
                  **defaults to "Available"**
                  use "ALL" to list all files and URLs
    - **regex_url**: (optional) str URL filter matching via regex in any urls.accessor_url
    \f
    :param file_type: file type filter
    :param url: URL filter matching files with substrings in any urls.accessor_url
    :param tags: unique tag filters matching files with all tags
    :param url_status: URL status filter matching files and urls with urls.status
    :param regex_url: URL filter matching via regex in any urls.accessor_url
    """
    filters = FileFilterParams(file_type=file_type, url=url, tags=tags, url_status=url_status, regex_url=regex_url)
    matching_files = []
    for file in files.values():
        # skip files that do not match
        if filters.file_type is not None and filters.file_type != file.file_type:
            continue
        file_urls = [url.accessor_url for url in file.urls]
        if filters.url is not None and not any(filters.url in s for s in file_urls):
            continue
        if filters.tags is not None and not all(tag in file.tags for tag in filters.tags):
            continue
        if filters.url_status != "ALL" and not any(url.url_status == url_status for url in file.urls):
            print(f"any(url.url_status == {url_status} for url in {file.urls})")
            continue
        if filters.regex_url is not None \
                and all(re.search(pattern=filters.regex_url, string=file_url) is None for file_url in file_urls):
            continue

        # filter URLs in files to return only those where status matches ("Available" file URLs only by default)
        filtered_file = file.copy(deep=True)
        if filters.url_status != "ALL":
            filtered_file.urls = [url for url in filtered_file.urls if url.url_status == url_status]
        matching_files.append(filtered_file)

    return ManyFilesInResponse(results=matching_files, total=len(matching_files))


# Implemented in Metadata API
@app.get("/file/{file_guid}", response_model=ResultsFile, tags=["files"])
def read_file(file_guid: UUID3, url_status: str = Query("Available", title="File Access URL status Query Filter")):
    """
    Fetch (GET / Query) file catalog items by file_guid:

    - **file_guid**: uuid3
    - **url_status**: (optional) str URL status filter matching files and urls with urls.status
                  **defaults to "Available"**
                  use "ALL" to get all URLs
    \f
    :param file_guid: uuid3(NAMESPACE_URL, <original_url>)
    :param url_status: URL status filter matching files and urls with urls.status
    """
    if file_guid not in files:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"File '{file_guid}' not found",
        )

    file = files.get(file_guid, {})
    # filter URLs to return only those where status matches ("Available" file URLs only by default)
    filtered_file = file.copy(deep=True)
    if url_status != "ALL":
        filtered_file.urls = [url for url in filtered_file.urls if url.url_status == url_status]

    return ResultsFile(results=filtered_file)


# Implemented in Metadata API
@app.post("/file/{file_guid}/urls/", tags=["files"])
def create_file_url(file_guid: UUID3, file_url: FileUrlInCreate, response: Response):
    """
    Add new URL to file

    - **accessor_url**: accessor URL of file instance
    - **creation_date**: creation date of file instance
    - **entry_created_by**: e.g. "TransferAPI: RequestID"
    \f
    :param file_guid: uuid3(NAMESPACE_URL, <original_url>)
    :param file_url: FileUrl
    :param response: FastAPI Response
    """
    if file_guid not in files:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"File '{file_guid}' not found",
        )

    url_id = len(files[file_guid].urls)
    new_file_url = FileUrl(url_id=url_id, accessor_url=file_url.accessor_url, creation_date=file_url.creation_date,
                           entry_created_by=file_url.entry_created_by, url_status=file_url.url_status,
                           last_changed=file_url.creation_date)
    files[file_guid].urls.append(new_file_url)
    # file = files[file_guid]
    response.headers["Location"] = f"/file/{file_guid}/urls/{url_id}/"

    return Response(status_code=status.HTTP_201_CREATED,
                    headers={"Location": f"/file/{file_guid}/urls/{url_id}/"})


# Implemented in Metadata API
@app.put("/file/{file_guid}/urls/{url_id}/status/", tags=["files"])
def update_file_url_status(file_guid: UUID3, url_id: int, url_status: str):
    """
    Update file URL status

    - **url_status**: str new status of file URL
    \f
    :param file_guid: uuid3(NAMESPACE_URL, <original_url>)
    :param url_id: int id of URL
    :param url_status: str new status
    """
    if file_guid not in files:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"File '{file_guid}' not found",
        )
    if url_id >= len(files[file_guid].urls) or url_id < 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"File URL '{file_guid}/{url_id}' not found",
        )
    files[file_guid].urls[url_id].url_status = url_status
    files[file_guid].urls[url_id].last_changed = datetime.now().timestamp() * 1000  # DANF timestamps are in ms

    return Response(status_code=status.HTTP_204_NO_CONTENT)


# Not part of the MVP since file metadata shall not be deleted
# @app.delete("/file/{file_guid}", status_code=status.HTTP_204_NO_CONTENT, tags=["files"])
# def delete_file(file_guid: UUID3):
#     files.pop(file_guid, None)
#     return


# This endpoint shall not be implemented in Metadata API - it's here just for convenience
@app.get("/jsonschema/file", tags=["files"])
def file_schema():
    return File.schema()
