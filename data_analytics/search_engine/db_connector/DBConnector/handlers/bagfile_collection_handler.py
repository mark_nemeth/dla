"""Bagfile Collection Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os

from DANFUtils.exceptions import DANFBaseException
from DANFUtils.logging import logger
from DANFUtils.utils import validate_json
from DBConnector.exceptions import IndexOverwriteException, BulkOperationException, InvalidQueryException, \
    InvalidConfigurationException
from DBConnector.handlers.bagfile_collection_handler_base import BagfileCollectionBaseHandler
from DBConnector.utils import calculate_request_size, MAXIMUM_REQUEST_BATCH_SIZE
from pymongo import UpdateOne
from pymongo.errors import BulkWriteError


class BagfileCollectionHandler(BagfileCollectionBaseHandler):
    """
    BagfileCollectionHandler
    """

    # Bagfile index
    COLLECTION_NAME = 'bagfiles'
    GUID_INDEX_NAME = 'bagfiles_guid_index'
    START_INDEX_NAME = 'bagfiles_start_index'
    INDEX_FIELD_GUID = 'guid'

    def __init__(self, db):
        BagfileCollectionBaseHandler.__init__(self, db, self.COLLECTION_NAME,
                                              self.GUID_INDEX_NAME,
                                              self.START_INDEX_NAME)
        base_path = os.path.abspath(os.path.dirname(__file__))
        query_schema_file = f"{base_path}/../schema/bagfile_query.schema"
        self.schema_query = self._load_json_schema(query_schema_file)
        query_projection_file = f"{base_path}/../schema/bagfile_projection.schema"
        self.schema_projection = self._load_json_schema(query_projection_file)

    def add(self, bagfiles):
        assert isinstance(
            bagfiles, list), 'Data inserted into db has to be a list of dicts'
        assert all(isinstance(x, dict) for x in
                   bagfiles), 'Data inserted into db has to be a list of dicts'

        try:
            num_docs = len(bagfiles)
            request_size = calculate_request_size(bagfiles)
            logger.debug(
                f"Adding {num_docs} documents to collection \"{self.COLLECTION_NAME}\" (Request size: {request_size:.2f} kb)"
            )
            self.col.insert_many(bagfiles)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

        for x in bagfiles:
            del x['_id']

    def get(self, guids):
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        request = {self.INDEX_FIELD_GUID: {"$in": guids}}
        request_size = calculate_request_size(request)
        logger.debug("Retrieving " + str(len(guids)) +
                     " documents from collection \"" + self.COLLECTION_NAME +
                     "\" (Request size: {:.2f}".format(request_size) + " kb)")
        return self.col.find(request, {'_id': 0})

    def get_distinct_field_values(self, field: str):
        return self.col.distinct(field)

    def get_max_field_value(self, field: str):
        try:
            return list(self.col.aggregate([{"$group": {"_id": None, "max": {"$max": "$" + field}}}]))[0].get("max",
                                                                                                              None)
        except IndexError:
            return None

    def get_min_field_value(self, field: str):
        try:
            return list(self.col.aggregate([{"$group": {"_id": None, "min": {"$min": "$" + field}}}]))[0].get("min",
                                                                                                              None)
        except IndexError:
            return None

    def search(self,
               query=None,
               limit=100,
               offset=0,
               sortby="_id",
               projection=None):
        assert isinstance(query, dict), 'Expected dict query'
        assert isinstance(limit, int), 'Expected int limit'
        assert isinstance(offset, int), 'Expected int offset'
        assert isinstance(sortby, (str, list)), 'Expected str or list sortby'
        if projection is not None:
            assert isinstance(projection, dict), 'Expected dict projection'
        else:
            projection = {}
        try:
            request = validate_json(json.dumps(query), self.schema_query)
        except DANFBaseException as e:
            error_message = f'Bad request: invalid search query {query} - Error: {e}'
            logger.exception(error_message)
            raise InvalidQueryException(error_message)
        try:
            proj = validate_json(json.dumps(projection),
                                 self.schema_projection)
        except DANFBaseException as e:
            error_message = f'Bad request: invalid search projection {projection} - Error: {e}'
            logger.exception(error_message)
            raise InvalidQueryException(error_message)
        request_size = calculate_request_size(request)
        logger.debug(
            f"Searching documents in collection \""
            f"{self.COLLECTION_NAME} \" (Request size: {request_size:.2f} kb)")
        proj['_id'] = 0
        cursor = self.col.find(request, proj).sort(sortby)
        if offset > 0:
            cursor = cursor.skip(offset)
        if limit > 0:
            cursor = cursor.limit(limit)
        return cursor

    def update(self, guids, bagfile_data, create_if_missing=False):
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'
        assert isinstance(bagfile_data,
                          list), 'Expected a list of bagfiles dicts.'
        assert all(isinstance(x, dict)
                   for x in bagfile_data), 'Expected a list of bagfiles dicts.'
        assert isinstance(create_if_missing,
                          bool), 'create_if_missing has to be of type bool'
        assert len(guids) == len(
            bagfile_data
        ), 'List guids and list bagfile_data should be of same length'
        for d in bagfile_data:
            if self.INDEX_FIELD_GUID in d.keys():
                msg = 'Overwriting index fields is not permitted!'
                logger.error(msg)
                raise IndexOverwriteException(msg)

        requests = []
        for g, d in zip(guids, bagfile_data):
            # Add GUID to dict and create update request
            requests.append(
                UpdateOne({self.INDEX_FIELD_GUID: g},
                          {"$set": {
                              **d, self.INDEX_FIELD_GUID: g
                          }},
                          upsert=create_if_missing))
        try:
            logger.debug("Updating  " + str(len(guids)) +
                         " documents in collection \"" + self.COLLECTION_NAME +
                         "\"")
            self.col.bulk_write(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

    def delete(self, guids):
        assert isinstance(guids, list), 'Expected a list of guid strings.'
        assert all(isinstance(x, str)
                   for x in guids), 'Expected a list of guid strings.'

        num_docs = len(guids)
        if num_docs > 0:
            request = {'guid': {"$in": guids}}
            request_size = calculate_request_size(request)
            logger.debug("Deleting  " + str(num_docs) +
                         " documents from collection \"" +
                         self.COLLECTION_NAME +
                         "\" (Request size: {:.2f}".format(request_size) +
                         " kb)")
            if num_docs <= MAXIMUM_REQUEST_BATCH_SIZE:
                self.col.delete_many(request)
            else:
                # batch delete
                logger.debug(
                    "Info: Number of delete operations exceeds maximum for single request."
                    + "Performing document deletion in batches of size " +
                    str(MAXIMUM_REQUEST_BATCH_SIZE))
                i = 0
                while i < num_docs:
                    start_id = i
                    i += MAXIMUM_REQUEST_BATCH_SIZE
                    end_id = i
                    if end_id >= num_docs:
                        end_id = num_docs
                    logger.debug("Info: Deleting batch objects " +
                                 str(end_id) + " of " + str(num_docs))
                    request = {'guid': {"$in": guids[start_id:end_id]}}
                    self.col.delete_many(request)

    def update_field(self, bag_guids, data_list, tag, append=False):
        create_if_missing = False

        assert isinstance(bag_guids,
                          list), 'Expected a list of guid strings. (bag_guids)'
        assert all(
            isinstance(x, str)
            for x in bag_guids), 'Expected a list of guid strings. (bag_guids)'
        assert len(bag_guids) == len(
            data_list
        ), 'List bag_guids and child_guids should be of same length'
        assert isinstance(tag, str), 'Expected a string for tag.'
        assert isinstance(append, bool), 'Expected a string for tag.'

        requests = []
        if append == True:
            cmd = "$push"
        else:
            cmd = "$set"

        for g, d in zip(bag_guids, data_list):
            requests.append(
                UpdateOne({self.INDEX_FIELD_GUID: g}, {cmd: {
                    tag: d
                }},
                          upsert=create_if_missing))
        try:
            logger.debug("Updating  " + str(len(bag_guids)) +
                         " documents in collection \"" + self.COLLECTION_NAME +
                         "\"")
            return self.col.bulk_write(requests)
        except BulkWriteError as e:
            msg = 'An error occured while performing a bulk operation (bulk_write). Details:\n' + '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            logger.exception(msg)
            raise BulkOperationException(msg)

    @staticmethod
    def _load_json_schema(schema_file):
        try:
            with open(schema_file) as f:
                schema = f.read()
        except (IOError, OSError) as e:
            error_message = f'Could not read ${schema_file} schema - Error: {e}'
            logger.exception(error_message)
            raise InvalidConfigurationException(error_message)

        try:
            return json.loads(schema)
        except ValueError as e:
            error_message = f'Bad schema: unable to parse ${schema_file} schema - Error {e}'
            logger.exception(error_message)
            raise InvalidConfigurationException(error_message)
