import React from 'react';
import Card from 'react-bootstrap/Card';
import './LabelContainer.scss'

function LabelContainer(props) {

  return (
    <Card className={props.variant + " label-container"}>
      <div className="label-container-header">
        <label className="label-container-header-name">
          {props.label}
        </label>
        <span className="label-container-header-actions">
        {props.actions && props.actions.map(action => {
          return (
            <label key={action.name} className="label-container-header-action-item" onClick={action.event}>
              {action.name}
            </label>
          )
        })}
        </span>
      </div>
      <div className="label-container-content">
        {props.children}
      </div>
    </Card>
  );
}

export default LabelContainer;