__copyright__ = """COPYRIGHT: (c) 2017-2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

# endpoint_value:db_value
REPORTING_LOCATIONS = {
    'any': 'Any',
    'us': 'US',
    'ger': 'DE',
    'renningen': 'Renningen',
    'immendingen': 'Immendingen',
    'sjpilot': 'SJ pilot'
}

REPORT_TYPE_DMV = "dmv"
REPORT_TYPE_RP = "rp"

MIN_DRIVE_DISTANCE = 1.5  # meters
MIN_DRIVE_DURATION = 1  # seconds
MILES_IN_KM = 0.621371
