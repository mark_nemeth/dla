"""Unittests for Feature Buckets"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

from ContainerClient.container_client import ContainerClient
from DANFUtils.logging import logger
from DANFUtils.utils import generate_deterministic_guid

import os
os.environ["DANF_ENV"] = "local"
from tests.regressiontests.testutils import get_testdb_connector

logger.set_log_level('debug')

bagfile_guid = generate_deterministic_guid()
feature_bucket_data_post = {
    "version":
    "1.0",
    "bagfile_guid":
    bagfile_guid,
    "start":
    1563262908000,
    "end":
    1563262911001,
    "start_index":
    60,
    "end_index":
    119,
    "tags": ["tag01", "tag02"],
    "features": [{
        "ts": 1563262908000,
        "vel": 0.5,
        "acc": 0.3,
        "eng_state": 11
    }, {
        "ts": 1563262908000,
        "vel": 0.6,
        "acc": 0.2,
        "eng_state": 5
    }],
    "is_test_data":
    1
}

feature_bucket_data_patch = {
    "version":
    "2.0",
    "bagfile_guid":
    bagfile_guid,
    "start":
    1563262908099,
    "end":
    156326291199,
    "start_index":
    70,
    "end_index":
    125,
    "tags": ["tag101", "tag102"],
    "features": [{
        "ts": 1563262908099,
        "vel": 0.8,
        "acc": 0.2,
        "eng_state": 10
    }, {
        "ts": 1593262908099,
        "vel": 0.6,
        "acc": 0.2,
        "eng_state": 5
    }],
    "is_test_data":
    1
}


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['feature_buckets'].delete_many({'metadata.is_test_data': 1})


class TestFeatureBucketMetaApi(unittest.TestCase):
    def setUp(self):
        cleanup_db()

    def tearDown(self):
        cleanup_db()

    def test_create_feature_bucket(self):
        client = ContainerClient()
        guid = client.create_feature_bucket(feature_bucket_data_post)
        self.assertEqual(201, client._last_response.status_code,
                         'feature bucket creation failed')
        self.assertRegex(
            guid,
            '([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid GUID has been returned')

    def test_update_feature_bucket(self):
        client = ContainerClient()
        guid = client.create_feature_bucket(feature_bucket_data_post)
        client.update_feature_bucket(guid, feature_bucket_data_patch)
        self.assertEqual(200, client._last_response.status_code,
                         'feature bucket update failed')


if __name__ == '__main__':
    unittest.main()
