__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config import code_handler as coder
from dla_converter import dla_conv
from utils.dir_utils import check_mapr
from utils.text_format import text2list_format


def help_fn():
    disc_msgs = {k: text2list_format(v) for k,v in coder.find('fn_disc').items()}
    dla_converter_msg = dla_conv.get_help()
    return {"Available Routes": disc_msgs,
            "dla_converter help": text2list_format(dla_converter_msg)}


def info_fn(bagfile: str):
    check_mapr()
    return dla_conv.get_info(bagfile)
