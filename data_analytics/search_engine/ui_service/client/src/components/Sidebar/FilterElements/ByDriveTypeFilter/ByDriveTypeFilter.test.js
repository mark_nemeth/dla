import React from 'react';
import {shallow, mount} from 'enzyme/build';
import {ByDriveTypeFilter} from './ByDriveTypeFilter';
import ScrollList from '../../../Utils/ScrollList/ScrollList';

describe('ByDriveTypeFilter component', () => {

  it('should render without crashing', () => {
    const props = {
      driveList: new Map([
        ['CID', 'CID'],
        ['CTD', 'CTD']]),
      selectedDriveTypeList: ['CID']
    };
    const wrapper = shallow(<ByDriveTypeFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should render Drive Type list with selected value', () => {
    const reduxAction = jest.fn();
    const props = {
      driveList: new Map([
        ['CID', 'CID'],
        ['CTD', 'CTD']]),
      selectedDriveTypeList: ['CTD'],
      filterByDriveType: reduxAction
    };
    const wrapper = mount(<ByDriveTypeFilter {...props} />);
    const driveList = wrapper.find(ScrollList);
    const entries = driveList.find('tr');
    expect(entries.length).toBe(2);
    expect(entries.at(0).find('input').props().checked).toBe(false);
    expect(entries.at(1).find('input').props().checked).toBe(true);
  });


  it('should call action when Drive Type is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      driveList: new Map([
        ['CID', 'CID'],
        ['CTD', 'CTD']]),
      selectedDriveTypeList: ['CID'],
      filterByDriveType: reduxAction
    };
    const wrapper = mount(<ByDriveTypeFilter {...props} />);
    const input = wrapper.find('tr').at(1).find('input');
    input.simulate('change', { target: { checked: true } });
    expect(reduxAction).toHaveBeenCalled();
  });

});
