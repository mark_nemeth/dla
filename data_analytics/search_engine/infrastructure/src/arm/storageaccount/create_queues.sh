storageAccountName=$(jq -r '.resourceName.value' <<< $STORAGEDEPLOYMENTOUTPUT)

# get agent public IP
ipAddress=$(curl ipinfo.io/ip)

# add firewall exception for agent
az storage account network-rule add \
    --account-name $storageAccountName \
    --ip-address $ipAddress

# create the storage queue
az storage queue create \
    --name danf-events-queue-$ENVIRONMENT \
    --account-name $storageAccountName

# remove agent firewall exception again    
az storage account network-rule remove \
    --account-name $storageAccountName \
    --ip-address $ipAddress
