from unittest.mock import patch
import os


def fake_load_dotenv(_):
    """
    - Given: the defined parameters with default values in the config
    file's if statement
    - When: using mock patch in the fake_load_dotenv to check the load_dotenv
    - Then: the patch modifies the load_dotenv with the fake's os.environ
    parameters
    :param _:
    :return:
    """
    os.environ['log_level'] = 'warning'
    os.environ['MAX_CONNECTIONS_COUNT'] = '7'
    os.environ['MIN_CONNECTIONS_COUNT'] = '1'
    os.environ['FAST_API_TITLE'] = 'danf'
    os.environ['ALLOWED_HOSTS'] = 'boo'
    os.environ['MONGO_DB'] = 'meta'
    os.environ['MONGODB_URL'] = ''


with patch.dict(os.environ, {'DANF_ENV': 'foo'}), \
     patch('dotenv.load_dotenv', fake_load_dotenv):
    from app.core import config

print(config.LOG_LEVEL, config.MAX_CONNECTIONS_COUNT, config.MIN_CONNECTIONS_COUNT,
      config.FAST_API_TITLE, config.ALLOWED_HOSTS, config.MONGO_DB, config.MONGODB_URL)

