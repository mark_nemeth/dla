import numpy as np
import pandas as pd
import rosbag
from extractors.intersections_feature_extractor import IntersectionsFeatureExtractor
from extractors.geometric_feature_extractor import GeometricFeatureExtractor

data_dir = '/tmp/' #TODO: change this to your bag directory

# read list of filenames
with open('filelist', 'r') as f:
    filename_list = f.readlines()

filename_list = [_.rstrip() for _ in filename_list]

geom_features_dict = dict()

for filename in filename_list:
    print(filename)
    geom_extractor = GeometricFeatureExtractor(data_dir+filename)
    geom_features_file = geom_extractor.ego_track_geom_features
    ego_track_file = geom_extractor.ego_track
    results_geom_features_file = {
        'Ego_track': ego_track_file,
        'Geom_features': geom_features_file
    }
    geom_features_dict[filename] = results_geom_features_file

ego_tracks_all_files = pd.DataFrame({
    'Filename': [],
    'Lat': [],
    'Lon': [],
    'T': []
})

geom_features_all_files = pd.DataFrame({
    'Filename': [],
    'Lon': [],
     'Lat': [],
     'T': [],
     'Turn_Ind': [],
     'Velocity_X': [],
     'Accel_X': [],
     'Accel_Y': [],
     'Yaw_Rate': [],
     'Steer_Angle': [],
     'd2y_dx2': [],
     'd2y_dx2_sm': [],
     'coef0_polyfit': [],
     'coef1_polyfit': [],
     'coef2_polyfit': [],
     'coef0_lstsq': [],
     'coef2_lstsq': [],
     'Turn': [],
     'Turn_Index': [],
     'Turn_Type': []
})

for filename in filename_list:
    geom_features = geom_features_dict[filename]
    ego_track_file=geom_features['Ego_track'].copy()
    ego_track_file['Filename'] = filename
    geom_features_file = geom_features['Geom_features'].copy()
    geom_features_file['Filename'] = filename
    ego_tracks_all_files = ego_tracks_all_files.append(ego_track_file)
    geom_features_all_files = geom_features_all_files.append(geom_features_file)

ego_tracks_all_files.to_csv('./Data/ego_tracks_all_files.csv', index=False)
geom_features_all_files.to_csv('./Data/geom_features_all_files.csv', index=False)

# run intersection feature extractor

results_dict = {}

for filename in filename_list:
    print(filename)
    bag = rosbag.Bag(data_dir+filename)
    scene_dyn_obj_crossing = IntersectionsFeatureExtractor(bag)
    results, scalars, tags = scene_dyn_obj_crossing.run()
    results_dict[filename] = results

tracks_df_intersections = pd.DataFrame({
    'T':[],
    'X':[],
    'Y':[],
    'Track_id':[],
    'Label_id':[],
    'Label':[],
    'Filename':[],
    'T_delta':[],
    'Lon_intersect':[],
    'Lat_intersect':[]
})

for filename in filename_list:
    results = results_dict[filename]
    for idx, row in results['intersections'][results['intersections']['T_delta'] < np.Inf].iterrows():
        subject_track_intersection_df = results['track_dict'][row['track_id']].get_track_gps()
        subject_track_intersection_df['Track_id'] = row['track_id']
        subject_track_intersection_df['Label_id'] = row['label_id']
        subject_track_intersection_df['Label'] = row['label']
        subject_track_intersection_df['Filename'] = filename
        subject_track_intersection_df['T_delta'] = row['T_delta']
        subject_track_intersection_df['Lon_intersect'] = row['lon_intersect']
        subject_track_intersection_df['Lat_intersect'] = row['lat_intersect']
        tracks_df_intersections = tracks_df_intersections.append(subject_track_intersection_df)

tracks_df_intersections.to_csv('Data/tracks_intersection_dyn_obj_ego_full.csv', index=False)
