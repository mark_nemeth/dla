import {connect} from 'react-redux';
import {filterByJerk, setCurrentJerkRange} from '../../../../redux/Home/HomeActions';
import {ByJerkFilter, STATIC_LIMITS} from './ByJerkFilter';

function mapStateToProps(state, ownProps) {
  const limits = state.filterOptions.bagfileJerkLimits ?
    {
      min: state.filterOptions.bagfileJerkLimits[0],
      max: state.filterOptions.bagfileJerkLimits[1]
    }
    : STATIC_LIMITS;
  
  const jerk = state.currentJerkRange || {
    'min': limits.min,
    'max': limits.max
  };
  const steps = 0.05;
  return {jerk, limits, steps};
}

export default connect(
  mapStateToProps,
  {setCurrentJerkRange, filterByJerk}
)(ByJerkFilter)