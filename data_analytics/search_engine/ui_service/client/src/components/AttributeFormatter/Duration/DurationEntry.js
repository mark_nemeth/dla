import React from 'react';
import {defaultEmptyValue} from '../../Table';
import {durationConverter} from '../../../utils/valueConverter';
import {isNumberValid} from '../../../utils/helperFunctions';

export default function DurationEntry(props) {

  if (!isNumberValid(props.duration)) return <span>{defaultEmptyValue}</span>;

  const formattedDuration = durationConverter(props.duration);

  return (
    <span>{formattedDuration}</span>
  )
}