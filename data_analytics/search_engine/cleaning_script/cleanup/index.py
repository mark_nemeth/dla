__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse
import logging

from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.logging import configure_root_logger
from DANFUtils.tracing import tracing

from config.constants import DANF_VERSION_TAG

from cleanup.tasks.delete_queue_message import DeleteQueueMessageTask
from cleanup.tasks.set_sync_flag import SetSyncFlagTask
from cleanup.tasks.update_flow_run import UpdateFlowRunTask
from cleanup.tasks.update_processing_state import UpdateProcessingStateTask

logger = logging.getLogger(__name__)


def _get_commandline_args():
    parser = argparse.ArgumentParser(description='Execute final cleanup tasks '
                                                 'after flow completion')
    parser.add_argument('--delete-queue-message',
                        action='store_true',
                        required=False,
                        help='Specify to execute delete queue message task')
    parser.add_argument('--set-sync-flag',
                        action='store_true',
                        required=False,
                        help='Specify to execute set sync flag task')
    parser.add_argument('--update-processing-state',
                        action='store_true',
                        required=False,
                        help='Specify to execute update processing state task')
    parser.add_argument('--update-flow-run',
                        action='store_true',
                        required=False,
                        help='Specify to execute update flow run task')
    return parser.parse_args()


def delete_queue_message():
    delete_task = DeleteQueueMessageTask()
    delete_task.execute()


def set_sync_flag():
    sync_task = SetSyncFlagTask()
    sync_task.execute()


def update_processing_state():
    update_task = UpdateProcessingStateTask()
    update_task.execute()


def update_flow_run():
    update_run_task = UpdateFlowRunTask()
    update_run_task.execute()


@tracing.trace_task('cleanup')
def main(args):
    if args.delete_queue_message:
        delete_queue_message()

    if args.set_sync_flag:
        set_sync_flag()

    if args.update_processing_state:
        update_processing_state()

    if args.update_flow_run:
        update_flow_run()


if __name__ == '__main__':
    configure_root_logger(
        log_level='INFO',
        enable_azure_log_aggregation=danf_env().platform != Platform.LOCAL,
    )
    logger.setLevel('INFO')
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    logger.info("################################")
    logger.info("######## cleanup script ########")
    logger.info("################################")

    cli_args = _get_commandline_args()
    main(cli_args)

