"""Unittest for bagfiles handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

from kubeflow.steps.common import sanitize_label


class TestSanitizeLabel(unittest.TestCase):

    def test_sanitize_label_when_legal_label_should_not_truncate(self):
        raw_label = "2020-file_file1.bag"
        res = sanitize_label(raw_label)
        self.assertEqual(res, raw_label)

    def test_sanitize_label_when_label_to_long_should_truncate_to_63_chars(self):
        raw_label = 'x' * 65
        res = sanitize_label(raw_label)
        self.assertEqual(len(res), 63)

    def test_sanitize_label_when_sanitized_label_ends_with_non_alphanumeric_char_should_remove_it(self):
        raw_label = ('x' * 62) + '-' + 'x'
        res = sanitize_label(raw_label)
        self.assertEqual(res, 'x' * 62)

    def test_sanitize_label_when_special_characters_are_included(self):
        raw_label ="xxxx-xxxx.xxxx)xxxxx+x"
        res = sanitize_label(raw_label)
        self.assertEqual(res, "xxxx-xxxx.xxxx_xxxxx_x")

if __name__ == '__main__':
    unittest.main()
