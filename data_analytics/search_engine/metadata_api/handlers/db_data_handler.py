"""Handler class for db"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List, Dict, Any

from DANFUtils.exceptions import GUIDNotFoundException
from DANFUtils.logging import logger
from DBConnector.db_connector import DBConnector
from DBConnector.exceptions import InvalidQueryException
from model.accounts import Account
from model.bva_quota import QuotaDocument
from model.bva_vote import VoteDocument
from model.extractor_event import ExtractedEvent
from model.file_catalog import File, FileCatalogUpdateRequest, FileCatalogUrlStatusUpdateRequest
from model.flow_runs import FlowRun, FlowRunStepRun
from model.flows import Flow

from config.config_handler import config_dict
from model.exceptions import GuidNotFoundError, DANFApiError, RequestNotValidError, InvariantViolationError


class DB_Data_Handler:
    def __init__(self):
        ssl_enabled = config_dict.get_value("db_ssl", "True") == "True"

        self.connector = DBConnector(
            hostname=config_dict.get_value("db_hostname"),
            port=config_dict.get_value("db_port"),
            db_name=config_dict.get_value("db_name"),
            user=config_dict.get_value("db_user"),
            key=config_dict.get_value("db_key"),
            protocol=config_dict.get_value("db_protocol"),
            ssl=ssl_enabled)

    ###########################################################################
    #### Sequence operations ##################################################
    ###########################################################################
    def add_sequences(self, sequence_data):
        bagfile_guids = [x['bagfile_guid'] for x in sequence_data]
        with self.connector as db:
            if self.bagfiles_exist(self.connector, bagfile_guids):
                db.add_sequences(sequence_data)
            else:
                raise GUIDNotFoundException(
                    'MetadataAPI', bagfile_guids,
                    'Sequence(s) could not be added to db: bagfile_guid(s) not found in db'
                )

    def get_sequences(self, guids):
        with self.connector as db:
            sequences = list(db.get_sequences(guids))
        if len(sequences) == len(guids) and all(
                [x['guid'] in guids for x in sequences]):
            return sequences
        else:
            raise GUIDNotFoundException(
                'MetadataAPI', guids,
                'Sequence(s) could not be retrieved: sequence guid(s) not found in db'
            )

    def update_sequences(self, guids, sequence_data):
        with self.connector as db:
            if self.sequences_exist(self.connector, guids):
                db.update_sequences(guids, sequence_data)
            else:
                raise GUIDNotFoundException(
                    'MetadataAPI', guids,
                    'Sequence(s) could not be updated: sequence_guid(s) not found in db'
                )

    def update_sequences_by_index(self, bagfile_guid_index_tuples,
                                  sequence_data):
        bagfile_guids = list(set([x for x, y in bagfile_guid_index_tuples]))
        with self.connector as db:
            if self.bagfiles_exist(self.connector, bagfile_guids):
                db.update_sequences_by_index(bagfile_guid_index_tuples,
                                             sequence_data, upsert=True)
            else:
                raise GUIDNotFoundException(
                    'MetadataAPI', bagfile_guids,
                    'Sequence(s) could not be updated: bagfile_guid(s) not found in db'
                )

    def sequences_exist(self, db_connector, guids):
        guids = list(set(guids))
        sequences = db_connector.get_sequences(guids)
        return sequences.count() == len(guids) and all(
            [x['guid'] in guids for x in sequences])

    def delete_sequences(self, guids):
        with self.connector as db:
            if self.sequences_exist(self.connector, guids):
                db.delete_sequences(guids)
            else:
                raise GUIDNotFoundException(
                    'MetadataAPI', guids,
                    'Sequence(s) could not be deleted: sequence_guid(s) not found in db'
                )

    ###########################################################################
    #### Bagfile operations ###################################################
    ###########################################################################
    def add_bagfiles(self, bagfile_data):
        with self.connector as db:
            db.add_bagfiles(bagfile_data)

    def get_bagfiles(self, guids):
        with self.connector as db:
            bagfiles = list(db.get_bagfiles(guids))
        if len(bagfiles) == len(guids) and all(
                [x['guid'] in guids for x in bagfiles]):
            return bagfiles
        else:
            raise GuidNotFoundError(guids, 'bagfile')

    def update_bagfiles(self, guids, bagfile_data):
        with self.connector as db:
            if self.bagfiles_exist(self.connector, guids):
                db.update_bagfiles(guids, bagfile_data)
            else:
                raise GuidNotFoundError(guids, 'bagfile')

    def update_childbagfiles(self, guids, childbagfile_data):
        with self.connector as db:
            if self.child_bagfiles_exist(self.connector, guids):
                db.update_child_bagfiles(guids, childbagfile_data)
            else:
                raise GuidNotFoundError(guids, 'childbagfile')

    def delete_bagfiles(self, guids, cascade_to_list=None, hard_delete=False):
        with self.connector as db:
            if not self.bagfiles_exist(self.connector, guids):
                raise GuidNotFoundError(guids, 'bagfile')

            sequences = list(db.get_sequences_by_bagfile_guid(guids))
            sequence_guids = [seq['guid'] for seq in sequences]
            self.delete_sequences(sequence_guids)

            if hard_delete:
                db.delete_bagfiles(guids)
            else:
                db.update_bagfiles(guids, [{'is_deleted': True}] * len(guids))

            if cascade_to_list and "childbagfiles" in cascade_to_list:
                childbagfiles = list(
                    db.get_child_bagfiles_by_bagfile_guid(guids))
                if childbagfiles:
                    childbagfile_guids = [cbag['guid'] for cbag in childbagfiles]
                    self.delete_child_bagfiles(childbagfile_guids, hard_delete)

            if cascade_to_list and "extractor_requests" in cascade_to_list:
                extracor_requests = list(
                    db.get_extractor_requests_by_bagfile_guid(guids))
                extracor_request_guids = [
                    extr['guid'] for extr in extracor_requests
                ]
                self.delete_extractor_requests(extracor_request_guids)

    def bagfiles_exist(self, db_connector, guids):
        guids = list(set(guids))
        bagfiles = db_connector.get_bagfiles(guids)
        return bagfiles.count() == len(guids) and all(
            [x['guid'] in guids for x in bagfiles])

    def _dictify_bagfiles(self, bagfiles):
        """
        :param bagfiles:
        :return:
        """
        return list(map(lambda f: {"file_path": f.file_path}, bagfiles))

    def search_bagfiles(self, query=None, limit=100, offset=0, sortby='guid'):
        with self.connector as db:
            cursor = db.search_bagfiles(query=query,
                                        limit=limit,
                                        offset=offset,
                                        sortby=sortby)
            return [x for x in cursor]

    ###########################################################################
    #### Child bagfile operations #############################################
    ###########################################################################
    def add_child_bagfiles(self, child_bagfile_data):
        with self.connector as db:
            db.add_child_bagfiles(child_bagfile_data)

    def get_child_bagfiles(self, guids):
        with self.connector as db:
            child_bagfiles = list(db.get_child_bagfiles(guids))
        if len(child_bagfiles) == len(guids) and all(
                [x['guid'] in guids for x in child_bagfiles]):
            return child_bagfiles
        else:
            raise GuidNotFoundError(guids, 'childbagfile')

    def update_child_bagfiles(self, guids, child_bagfile_data):
        with self.connector as db:
            if self.child_bagfiles_exist(self.connector, guids):
                db.update_child_bagfiles(guids, child_bagfile_data)
            else:
                raise GuidNotFoundError(guids, 'childbagfile')

    def delete_child_bagfiles(self, guids, hard_delete=False):
        with self.connector as db:
            if not self.child_bagfiles_exist(self.connector, guids):
                raise GuidNotFoundError(guids, 'childbagfile')

            if hard_delete:
                db.delete_child_bagfiles(guids)
            else:
                db.update_child_bagfiles(guids, [{'is_deleted': True}] * len(guids))

    def child_bagfiles_exist(self, db_connector, guids):
        guids = list(set(guids))
        child_bagfiles = db_connector.get_child_bagfiles(guids)
        return child_bagfiles.count() == len(guids) and all(
            [x['guid'] in guids for x in child_bagfiles])

    def search_childbagfiles(self,
                             query=None,
                             limit=100,
                             offset=0,
                             sortby='guid'):
        with self.connector as db:
            cursor = db.search_child_bagfiles(query=query,
                                              limit=limit,
                                              offset=offset,
                                              sortby=sortby)
        return list(cursor)

    ###########################################################################
    #### feature buckets request operations ###################################
    ###########################################################################
    def add_feature_buckets(self, feature_bucket_data):
        with self.connector as db:
            db.add_feature_buckets(feature_bucket_data)

    def get_feature_buckets(self, guids):
        with self.connector as db:
            feature_buckets = list(db.get_feature_buckets(guids))
        if len(feature_buckets) == len(guids) and all(
                [x['guid'] in guids for x in feature_buckets]):
            return feature_buckets
        else:
            raise GuidNotFoundError(guids, 'feature_buckets')

    def upsert_single_feature_bucket(self, guid: str,
                                     feature_bucket_data: dict):
        with self.connector as db:
            if self.feature_buckets_exist(self.connector, [guid]):
                if "guid" in feature_bucket_data:
                    del feature_bucket_data["guid"]
                db.update_feature_buckets([guid], [feature_bucket_data])
            else:
                feature_bucket_data["guid"] = guid
                db.add_feature_buckets([feature_bucket_data])

    def update_feature_buckets(self, guids, feature_bucket_data):
        with self.connector as db:
            if self.feature_buckets_exist(self.connector, guids):
                db.update_feature_buckets(guids, feature_bucket_data)
            else:
                raise GuidNotFoundError(guids, 'feature_bucket')

    def feature_buckets_exist(self, db_connector, guids: list):
        guids = list(set(guids))
        feature_buckets = db_connector.get_feature_buckets(guids)
        return feature_buckets.count() == len(guids) and all(
            [x['guid'] in guids for x in feature_buckets])

    def delete_feature_buckets(self, guids):
        with self.connector as db:
            if not self.feature_buckets_exist(self.connector, guids):
                raise GuidNotFoundError(guids, 'feature_bucket')
            db.delete_feature_buckets(guids)

    ###########################################################################
    #### Drive request operations #############################################
    ###########################################################################
    def add_drives(self, drive_data):
        with self.connector as db:
            db.add_drives(drive_data)

    def get_drives(self, guids):
        with self.connector as db:
            drives = list(db.get_drives(guids))
        if len(drives) == len(guids) and all(
                [x['guid'] in guids for x in drives]):
            return drives
        else:
            raise GuidNotFoundError(guids, 'drive')

    def update_drives(self, guids, drive_data):
        with self.connector as db:
            if self.drives_exist(self.connector, guids):
                db.update_drives(guids, drive_data)
            else:
                raise GuidNotFoundError(guids, 'drive')

    def drives_exist(self, db_connector, guids):
        guids = list(set(guids))
        drives = db_connector.get_drives(guids)
        return drives.count() == len(guids) and all(
            [x['guid'] in guids for x in drives])

    def delete_drives(self, guids, cascade_to_list=None, hard_delete=False):
        with self.connector as db:
            if not self.drives_exist(self.connector, guids):
                raise GuidNotFoundError(guids, 'drive')

            if cascade_to_list and 'bagfiles' in cascade_to_list:
                drives = list(db.get_drives(guids))
                bagfile_lists = [drive['bagfiles_list'] for drive in drives]
                bagfile_guids = [
                    guid for sublist in bagfile_lists for guid in sublist
                ]
                for bagfile_guid in bagfile_guids:
                    try:
                        self.delete_bagfiles([bagfile_guid], cascade_to_list, hard_delete)
                    except GuidNotFoundError as e:
                        logger.warning(f"Bagfile not found with guid: {bagfile_guid}, {e}.")

            if hard_delete:
                db.delete_drives(guids)
            else:
                db.update_drives(guids, [{'is_deleted': True}] * len(guids))

    def get_distinct_field_values(self, collection: str, field: str):
        with self.connector as db:
            return db.get_distinct_field_values(collection, field)

    def get_max_field_value(self, collection: str, field: str):
        with self.connector as db:
            return db.get_max_field_value(collection, field)

    def get_min_field_value(self, collection: str, field: str):
        with self.connector as db:
            return db.get_min_field_value(collection, field)

    ###########################################################################
    #### Extractor request operations #########################################
    ###########################################################################
    def add_extractor_requests_legacy(self, extractor_request_data):
        bagfile_guids = [x['bagfile_guid'] for x in extractor_request_data]
        with self.connector as db:
            if self.bagfiles_exist(self.connector, bagfile_guids):
                db.add_extractor_requests(extractor_request_data)
            else:
                raise GUIDNotFoundException(
                    'MetadataAPI', bagfile_guids,
                    'Extractor Request(s) could not be added to db: bagfile_guid(s) not found in db'
                )

    def upsert_extractor_requests(
            self, guids: List[str],
            extractor_request_data: List[ExtractedEvent]):
        bagfile_guids = []
        extractor_requests_as_dicts = []
        for x in extractor_request_data:
            bagfile_guids.append(x.bagfile_guid)
            ext_dict = x.dict()
            if "guid" in ext_dict:
                ext_dict.pop("guid")
            extractor_requests_as_dicts.append(ext_dict)
        with self.connector as db:
            if self.bagfiles_exist(self.connector, bagfile_guids):
                db.update_extractor_requests(guids,
                                             extractor_requests_as_dicts, True)
            else:
                raise GUIDNotFoundException(
                    'MetadataAPI', bagfile_guids,
                    'Extractor Request(s) could not be added to db: bagfile_guid(s) not found in db'
                )

    def add_extractor_requests(self,
                               extractor_request_data: List[ExtractedEvent]):
        bagfile_guids = [x.bagfile_guid for x in extractor_request_data]
        extractor_requests_as_dicts = [
            x.dict() for x in extractor_request_data
        ]
        with self.connector as db:
            if self.bagfiles_exist(self.connector, bagfile_guids):
                db.add_extractor_requests(extractor_requests_as_dicts)
            else:
                raise GUIDNotFoundException(
                    'MetadataAPI', bagfile_guids,
                    'Extractor Request(s) could not be added to db: bagfile_guid(s) not found in db'
                )

    def extractor_request_exist(self, db_connector, guids):
        guids = list(set(guids))
        extractor_requests = db_connector.get_extractor_requests(guids)
        return extractor_requests.count() == len(guids) and all(
            [x['guid'] in guids for x in extractor_requests])

    def delete_extractor_requests(self, guids):
        with self.connector as db:
            if self.extractor_request_exist(self.connector, guids):
                db.delete_extractor_requests(guids)
            else:
                raise GUIDNotFoundException(
                    'MetadataAPI', guids,
                    'Extractor Request(s) could not be deleted from db: guid(s) not found in db'
                )

    ###########################################################################
    #### Document run operations ##############################################
    ###########################################################################
    def add_documents(self, documents):
        with self.connector as db:
            return db.add_documents(documents)

    def update_documents(self, guids, document_data, create_if_missing=False):
        with self.connector as db:
            return db.update_documents(guids=guids,
                                       document_data=document_data,
                                       create_if_missing=create_if_missing)

    def push_to_array_in_documents(self,
                                   guids,
                                   array_dicts,
                                   create_if_missing=False):
        with self.connector as db:
            db.push_to_arrays_in_documents(guids, array_dicts,
                                           create_if_missing)

    def pop_from_array_in_documents(self, guids, array_dicts):
        with self.connector as db:
            db.pop_from_arrays_in_documents(guids, array_dicts)

    def search_documents(self, query=None, limit=100, offset=0, sortby='guid'):
        with self.connector as db:
            cursor = db.search_documents(query=query,
                                         limit=limit,
                                         offset=offset,
                                         sortby=sortby)
        return [x for x in cursor]

    def get_documents(self, guids):
        with self.connector as db:
            cursor = db.get_documents(guids)
            documents = [x for x in cursor]
            if len(documents) == len(guids) and all(
                    [x['guid'] in guids for x in documents]):
                return documents
            else:
                raise GUIDNotFoundException(
                    'SearchAPI', guids,
                    'Cached document(s) could not be retrieved: guid(s) not found in db'
                )

    def delete_documents(self, guids):
        with self.connector as db:
            return db.delete_documents(guids)

    def update_field(self, bag_guids, values, field, append=False):
        with self.connector as db:
            return db.update_tag(bag_guids, values, field, append)

    ###########################################################################
    #### File Catalog operations ######################################################
    ###########################################################################
    def add_catalog(self, catalog: List[Dict]) -> None:
        with self.connector as db:
            any_exist = self.catalog_exist(
                db, [entry['file_guid'] for entry in catalog])
            if any_exist:
                raise RequestNotValidError(
                    "File guid already present in catalog.")
            db.add_catalog(catalog)

    def get_catalog_all(self, file_guid: List[str]) -> List[File]:
        with self.connector as db:
            catalogs_as_dict = list(db.get_catalog_all(file_guid))
        catalogs = [File.parse_obj(c) for c in catalogs_as_dict]
        for catalog in catalogs:
            seen_urls = set()
            unique_urls = []
            for url in catalog.urls:
                if (url.accessor_url in seen_urls):
                    continue
                unique_urls.append(url)
                seen_urls.add(url.accessor_url)
            catalog.urls = unique_urls
        if len(catalogs) != len(file_guid) or not all(
                [catalog.file_guid in file_guid for catalog in catalogs]):
            raise GuidNotFoundError(file_guid, 'catalog')
        return catalogs

    def get_catalog(self, file_guid: str, url_status: str) -> List[File]:
        with self.connector as db:
            catalogs_as_dict = list(db.get_catalog(file_guid, url_status))
        catalogs = [File.parse_obj(c) for c in catalogs_as_dict]
        if len(catalogs) != 1:
            raise GuidNotFoundError(file_guid, 'catalog')
        return catalogs

    def update_catalog(self, catalog_data: FileCatalogUpdateRequest):
        with self.connector as db:
            guids = catalog_data.file_guid
            if self.catalog_exist(self.connector, [guids]):
                existing_urls = self.get_catalog_all([guids])[0]
                for url in existing_urls.urls:
                    # if url already exists, dont add a duplicate
                    if url.accessor_url == catalog_data.file_url.accessor_url:
                        raise InvariantViolationError('accessor url already exists')
                db.update_catalog([guids], [{
                    "urls": catalog_data.file_url.dict()
                }], create_if_missing=False)
            else:
                raise GuidNotFoundError(guids, 'catalog')

    def update_catalog_url_status(
            self, catalog_data: FileCatalogUrlStatusUpdateRequest):
        with self.connector as db:
            guids = catalog_data.file_guid
            if self.catalog_exist(self.connector, [guids]):
                try:
                    db.update_catalog_url_status(guids,
                                                 catalog_data.url_id,
                                                 catalog_data.url_status,
                                                 catalog_data.last_changed,
                                                 create_if_missing=False)
                except InvalidQueryException as e:
                    raise RequestNotValidError(str(e))
            else:
                raise GuidNotFoundError(guids, 'catalog')

    def search_catalog(self, query=None, limit=100, offset=0, sortby='guid'):
        with self.connector as db:
            cursor = db.search_catalog(query=query,
                                       limit=limit,
                                       offset=offset,
                                       sortby=sortby)
            return [x for x in cursor]

    def delete_catalog(self, guid: str) -> int:
        with self.connector as db:
            return db.delete_catalog(guid)

    def catalog_exist(self, db_connector, guids):
        # catalogs = db_connector.get_catalog(guids)
        catalogs = db_connector.get_catalog_all(guids)
        return catalogs.count() == len(guids) and all(
            [x['file_guid'] in guids for x in catalogs])

    ###########################################################################
    #### Flow operations ######################################################
    ###########################################################################
    def add_flows(self, flows: List[Flow]) -> None:
        flows_as_dicts = [f.dict() for f in flows]
        with self.connector as db:
            db.add_flows(flows_as_dicts)

    def get_flows(self, guids: List[str]) -> List[Flow]:
        with self.connector as db:
            flows_as_dicts = list(db.get_flows(guids))
        flows = [Flow.parse_obj(f) for f in flows_as_dicts]
        if len(flows) != len(guids) or not all(
                [flow.guid in guids for flow in flows]):
            raise GuidNotFoundError(guids, 'flow')
        return flows

    ###########################################################################
    #### Flow run operations ##################################################
    ###########################################################################
    def add_flow_runs(self, flow_runs: List[FlowRun]) -> None:
        runs_as_dicts = [r.dict() for r in flow_runs]
        with self.connector as db:
            db.add_flow_runs(runs_as_dicts)

    def update_flow_runs(self, guids: List[str],
                         field_updates: List[Dict[str, str]]) -> None:
        with self.connector as db:
            db.update_flow_runs(guids, field_updates)

    def add_flow_run_step_run(self, flow_run_guid: str, step_index: int,
                              step_run: FlowRunStepRun) -> None:
        run_as_dict = step_run.dict()
        with self.connector as db:
            db.add_flow_run_step_run(flow_run_guid, step_index, run_as_dict)

    def get_flow_runs(self, guids: List[str]) -> List[FlowRun]:
        with self.connector as db:
            runs_as_dicts = db.get_flow_runs(guids)
        flow_runs = [FlowRun.parse_obj(r) for r in runs_as_dicts]
        if len(flow_runs) != len(guids) or not all(
                [r.guid in guids for r in flow_runs]):
            raise GuidNotFoundError(guids, 'flow_run')
        return flow_runs

    ###########################################################################
    #### BVA operations #######################################################
    ###########################################################################
    def add_quota(self, new_voting_quota: QuotaDocument):
        voting_quota_as_dict = new_voting_quota.dict()
        with self.connector as db:
            db.add_quota(voting_quota_as_dict)

    def get_quotas(self, active) -> List[QuotaDocument]:
        with self.connector as db:
            quotas_as_dict = db.get_quotas(active)
        quotas = [QuotaDocument.parse_obj(quota) for quota in quotas_as_dict]
        if not quotas:
            raise DANFApiError(404, 'Quota documents could not be found')
        return quotas

    def get_votes(self, active_voting_id) -> List[VoteDocument]:
        with self.connector as db:
            votes_as_dict = db.get_votes(active_voting_id)
        votes = [VoteDocument.parse_obj(votes) for votes in votes_as_dict]
        if not votes:
            raise DANFApiError(404, 'Vote documents could not be found')
        return votes

    def update_quota(self, filter_query, update_request):
        with self.connector as db:
            quota_as_dict = db.find_and_update_quota(filter_query,
                                                     update_request)

        return (QuotaDocument.parse_obj(quota_as_dict)
                if quota_as_dict else None)

    def add_vote(self, vote: VoteDocument):
        vote_as_dict = vote.dict()
        with self.connector as db:
            db.add_vote(vote_as_dict)

    def get_vote_by_guid(self, guid):
        with self.connector as db:
            vote_as_dict = db.get_vote_by_guid(guid)
        vote = VoteDocument.parse_obj(vote_as_dict)
        if not vote:
            raise DANFApiError(404, 'Vote document could not be found')
        return vote

    def delete_vote_by_guid(self, guid):
        with self.connector as db:
            return db.delete_vote_by_guid(guid)

    def update_vote(self, filter_query, update_request):
        with self.connector as db:
            vote_as_dict = db.find_and_update_vote(filter_query,
                                                   update_request)
        return (VoteDocument.parse_obj(vote_as_dict) if vote_as_dict else None)

    ###########################################################################
    #### Account operations ################################################
    ###########################################################################

    def add_account(self, account: Account) -> None:
        with self.connector as db:
            db.add_accounts([account.dict()])

    def get_all_accounts(self) -> List[Account]:
        with self.connector as db:
            as_dicts = db.search_accounts(query={}, limit=0)
        accounts = [Account.parse_obj(account) for account in as_dicts]
        return accounts

    def get_account_by_guid(self, guid: str) -> Account:
        with self.connector as db:
            as_dicts = db.search_accounts(query={'guid': guid}, limit=1)
        accounts = [Account.parse_obj(account) for account in as_dicts]
        if not accounts:
            raise GuidNotFoundError(guid, 'account')
        return accounts[0]

    def get_account_by_key_hash(self, key_hash) -> Account:
        with self.connector as db:
            as_dicts = db.search_accounts(query={'account_key_hash': key_hash},
                                          limit=1)
        accounts = [Account.parse_obj(account) for account in as_dicts]
        if not accounts:
            raise GuidNotFoundError(key_hash,
                                    'account')  # TODO: correct exception
        return accounts[0]

    def update_account(self, guid: str, field_updates: Dict[str, Any]) -> None:
        with self.connector as db:
            db.update_accounts([guid], [field_updates])
