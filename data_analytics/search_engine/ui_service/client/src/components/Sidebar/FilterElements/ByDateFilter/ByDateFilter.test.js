import React from 'react';
import {shallow} from "enzyme/build";
import {ByDateFilter} from "./ByDateFilter";
import DatePicker from "react-datepicker";


describe('ByDateFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      startdate: 'Mon Jan 01 2018 00:00:00 GMT+0530 (India Standard Time)',
      enddate: 'Wed Sep 04 2019 19:25:15 GMT+0530 (India Standard Time)'
    };
    const wrapper = shallow(<ByDateFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should call action when dates are selected', () => {
    const reduxAction = jest.fn();
    const props = {
      size_max: '',
      size_min: '',
      filterByDate: reduxAction
    };
    // const wrapper = shallow(<ByDateFilter {...props} />);
    // wrapper.find(DatePicker).simulate('onChange')
    // expect(reduxAction).toHaveBeenCalled();
    const wrapper = shallow(<ByDateFilter {...props} />);
    const datePicker =  wrapper.find(DatePicker).first();
    datePicker.simulate('change', { target: { selected: 'Mon Jan 01 2018 00:00:00 GMT+0530 (India Standard Time)' } });
    datePicker.simulate('onChange');
    datePicker.simulate('onSelect');
    const datePicker2 = wrapper.find(DatePicker).at(1);
    datePicker2.simulate('change', { target: { selected: 'Wed Sep 04 2019 19:25:15 GMT+0530 (India Standard Time)' } });
    datePicker2.simulate('onChange');
    datePicker2.simulate('onSelect');
    // expect(reduxAction).toHaveBeenCalled();
  })

});