import dash
import flask
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from data import dyn_obj_list, map_obj_list

def create_browser_layout():
    layout = html.Div(
        children=[
            html.Div(
                className="row",
                children=[
                    # Column for user controls
                    html.Div(
                        className="three columns div-user-controls",
                        children=[
                            html.H1("Athena Scenario Browser"),
                            html.H2(
                                """Dynamic object actions"""
                            ),
                            html.Div(
                                className="row",
                                children=[
                                    html.Div(
                                        className="div-for-dropdown",
                                        children=[
                                            # Dropdown to select times
                                            dcc.Dropdown(
                                                id="dyn-object-selector",
                                                options=[
                                                    {
                                                        "label": str(obj_label),
                                                        "value": str(obj_label),
                                                    }
                                                    for obj_label in dyn_obj_list
                                                ],
                                                multi=False,
                                                placeholder="Select a Dynamic Object",
                                            )
                                        ],
                                    ),
                                ],
                            ),
                            html.Div(id='hidden',children=[],style={'display': 'none'}),
                            html.Div(
                                className="div-for-dropdown",
                                children=[
                                    html.Div(
                                        children=[
#                                             html.Label('Relative Position of dynamic object (X,Y)'),
#                                             dcc.Input(id="rp-cutoff-X",value='10', type='text'),
#                                             dcc.Input(id="rp-cutoff-Y",value='30', type='text'),
#                                             html.Label('Relative Velocity of dynamic object (X,Y)'),
#                                             dcc.Input(id="rv-cutoff-X",value='5', type='text'),
#                                             dcc.Input(id="rv-cutoff-Y",value='25', type='text'),
                                            html.Label('Relative distance to nearest map objects (meters)'),
                                            dcc.Dropdown(
                                                id="proximity-dyn-obj-type-selector",
                                                options=[
                                                    {
                                                        "label": str(prox_dyn_type),
                                                        "value": str(prox_dyn_type),
                                                    }
                                                    for prox_dyn_type in ['Within', 'Not Within']
                                                ],
                                                multi=False,
                                                placeholder="Select a Proximity Type",
                                            ),
                                            dcc.Input(id="prox-dyn-cutoff",value='50', type='text'),
                                            html.Label('Comparison Map Object'),
                                            dcc.Dropdown(
                                                id="proximity-dyn-obj-to-map-obj",
                                                options=[
                                                    {
                                                        "label": str(obj_type),
                                                        "value": str(obj_type),
                                                    }
                                                    for obj_type in ['CROSSWALK', 'PARKINGSPACE']
                                                ],
                                                multi=False,
                                                placeholder="Select a Relative Map Object",
                                            ),
                                            html.Div(className='div-for-checklist',
                                                     children=[
                                                         html.Div(className='column',
                                                                  children=[
                                                                     dcc.Checklist(id='cb-heatmap',options=[{'label':'HeatMap', 'value':1}],
                                                                      value=[], style={'display':'inline-block'}),
                                                         ]),
                                                         html.Div(className='column',
                                                                  children=[
                                                                     dcc.Checklist(id='cb-tracks',options=[{'label':'Tracks', 'value':1}],
                                                                      value=[], style={'display':'inline-block'}),
                                                         ]),
                                                     ]),
                                            
                                        ]
                                    )
                                ]
                            ),
                            html.Div(
                                className="row",
                                children=[
                                    html.Div(
                                        className="div-for-dropdown",
                                        children=[
                                            html.H2("Map Objects"),
                                            # Dropdown to select map objects
                                            dcc.Dropdown(
                                                id="map-object-selector",
                                                options=[
                                                    {
                                                        "label": str(obj_type),
                                                        "value": str(obj_type),
                                                    }
                                                    for obj_type in map_obj_list
                                                ],
                                                multi=True,
                                                placeholder="Select a Map Object",
                                            ),
                                            html.Label('Relative distance to other map objects (meters)'),
                                            dcc.Dropdown(
                                                id="proximity-type-selector",
                                                options=[
                                                    {
                                                        "label": str(prox_type),
                                                        "value": str(prox_type),
                                                    }
                                                    for prox_type in ['Within', 'Not Within']
                                                ],
                                                multi=False,
                                                placeholder="Select a Proximity Type",
                                            ),
                                            dcc.Input(id="prox-cutoff",value='50', type='text'),
                                            html.Label('Comparison Map Object'),
                                            dcc.Dropdown(
                                                id="comparitive-map-object-selector",
                                                options=[
                                                    {
                                                        "label": str(obj_type),
                                                        "value": str(obj_type),
                                                    }
                                                    for obj_type in map_obj_list
                                                ],
                                                multi=True,
                                                placeholder="Select a Relative Map Object",
                                            ),
                                            
                                            
                                        ],
                                    ),
                                ],
                            ),
                            html.Div(className='div-for-dropdown',
                                     children = [html.H2("Ego vehicle maneuvers"),
                                                ]
                                    ),
                            html.Div(
                                className="div-for-checklist",
                                children=[
                                    html.Div(className='column',
                                             children=[
                                                 dcc.Checklist(id='cb-ego',options=[{'label':'Plot Ego', 'value':1}], 
                                                 value=[]),
                                             ],
                                            ),
                                    html.Div(className='column',
                                             children=[
                                                 dcc.Checklist(id='cb-turnind',options=[{'label':'TurnInd', 'value':1}], 
                                                 value=[]),
                                             ],
                                            ),
                                    html.Div(className='column',
                                             children = [
                                                 dcc.Checklist(id='cb-right-turn',options=[{'label':'Right Turns', 'value':1}],
                                                      value=[], style={'display':'inline-block'}),
                                             ],
                                    ),
                                    html.Div(className='column',
                                             children = [
                                                 dcc.Checklist(id='cb-left-turn',options=[{'label':'Left Turns', 'value':1}],
                                                      value=[], style={'display':'inline-block'}),
                                             ],
                                    ),
                                ],
                            ),
                            html.Div(
                                className="div-for-checklist",
                                children=[
                                    html.Div(className='column',
                                        children=[
                                            dcc.Checklist(id='cb-lane_change',options=[{'label':'Lane Changes', 'value':1}],
                                                      value=[], style={'display':'inline-block'})
                                        ]
                                    ),
                                    html.Div(className='column',
                                        children=[
                                            dcc.Checklist(id='cb-lane_change-map',options=[{'label':'Lane Changes (Map)', 'value':1}],
                                                      value=[], style={'display':'inline-block'})
                                        ]
                                    )
                                ]
                            ),
                            html.Div(
                                className="div-for-dropdown",
                                children=[
                                    html.Div(
                                        children=[
                                            html.Label('Curvature cutoff'),
                                            dcc.Input(id="curvature-cutoff",value='700', type='text'),
                                            html.Button(id='submit-button', n_clicks=0, children='Submit'),
                                            dcc.Checklist(id='cb-curvature',options=[{'label':'show', 'value':1}], 
                                                          value=[], style={'display':'inline-block'}),
                                        ]
                                    )
                                ]
                            ),
                            html.Div(
                                className="div-for-dropdown",
                                children=[
                                    html.Div(
                                        children=[
                                            html.Label('Curvature cutoff (lsq)'),
                                            dcc.Input(id="lsq-cutoff",value='0.1', type='text'),
                                            html.Button(id='submit-lsq', n_clicks=0, children='Submit'),
                                            dcc.Checklist(id='cb-lsq',options=[{'label':'show', 'value':1}], 
                                                          value=[], style={'display':'inline-block'}),
                                        ]
                                    )
                                ]
                            ),
                            html.Div(
                                className="div-for-dropdown",
                                children=[
                                    html.Div(
                                        children=[
                                            html.Label('Steering Angle'),
                                            dcc.Input(id="sa-cutoff",value='0.1', type='text'),
                                            html.Button(id='submit-sa', n_clicks=0, children='Submit'),
                                            dcc.Checklist(id='cb-sa',options=[{'label':'show', 'value':1}],
                                                          value=[], style={'display':'inline-block'}),
                                        ]
                                    )
                                ]
                            ),
                            html.Hr(),
                            html.P(id="selected-data"),
                            html.Form(id='search-form', 
                                      children=[
                                          dcc.Input(
                                              id='search-fname',
                                              name='fname',
                                              type='hidden',
                                              value='File1.bag'
                                          ),
                                          dcc.Input(
                                              id='search-ts',
                                              name='ts',
                                              type='hidden',
                                              value=1235
                                          ),
                                          html.Button(
                                              id='search-button',
                                              n_clicks=0, 
                                              children='Search',
                                              formAction='/search?'
                                          )
                                      ],
                                      method='GET'
                                     ),
                            html.H2(children=[html.A('API', href='/docs')],
                                   style={
                                       'textAlign': 'center'
                                       }
                                   )
                        ],
                    ),
                    # Column for app graphs and plots
                    html.Div(
                        className="nine columns div-for-charts bg-grey",
                        children=[
                            dcc.Graph(id="map-graph"),
                        ],
                    ),
                ],
            )
        ]
    )
    return layout