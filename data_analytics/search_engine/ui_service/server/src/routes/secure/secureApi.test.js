import httpMocks from 'node-mocks-http'
import secureApiMiddleware from "./secureApi";

const secured = secureApiMiddleware();

describe('Call secured api middleware', () => {

  it('should call next callback directly when in devMode', () => {
    const req = httpMocks.createRequest({
      headers: {origin: 'http://localhost:3000'},
      session: {}
    });
    const res = httpMocks.createResponse();
    const nextCallback = jest.fn();

    secured(req, res, nextCallback);

    expect(nextCallback).toHaveBeenCalled();
  });

  it('should call next callback when user session exist', () => {
    const req = httpMocks.createRequest({
      user: {
        name: "DanfUser"
      },
      session: {}
    });
    const res = httpMocks.createResponse();
    const nextCallback = jest.fn();

    secured(req, res, nextCallback);

    expect(nextCallback).toHaveBeenCalled();
  });

  it('should return 401 unauthorized when user session is not provided', () => {
    const req = httpMocks.createRequest({session: {}});
    const res = httpMocks.createResponse();
    const nextCallback = jest.fn();

    secured(req, res, nextCallback);

    expect(req.session.returnTo).toBe('/');
    expect(res.statusCode).toBe(401);
  });

});