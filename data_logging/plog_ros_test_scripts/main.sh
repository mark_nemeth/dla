#!/bin/bash

./statistics/kill_monitoring.sh

TT=`date +%j%H%M%S`
INTERFACES=(eth_log_1)
echo " -------- start test sequence "

mkdir -p run

# ~~~~ Start statistics for logfiles ~~~~ #
sleep 2
for IFACE in "${INTERFACES[@]}"
do
	./statistics/bwm-ng.sh "$TT" "${IFACE}" &

	./statistics/print_bandwidth_ethtool.sh "$TT" "${IFACE}" &

	./statistics/print_bandwidth_system.sh "$TT" "${IFACE}" &

	./statistics/nic_statistic.sh "$TT" "${IFACE}"
done

./statistics/iostat.sh &

./statistics/load.sh &

./statistics/ptop.sh &




# ~~~~ Test Type  ~~~~ #
./ros_test.sh


# ~~~~ Test Type  ~~~~ #
sleep 2

./statistics/kill_monitoring.sh

for IFACE in "${INTERFACES[@]}"
do
	./statistics/nic_statistic.sh "$TT" "${IFACE}"

	./statistics/collect_nic_stats.sh "$TT" "${IFACE}"
done

ls -lah ~/sequences/. > written_files.log

sleep 1

mv *.log run
echo '# ~~~~ Test was run with the configuration and code below ~~~~ #' | cat - ros_test.sh > run/ros_test.cfg

# We do not want to rebuild the FS in the car. Uncomment for performance tests in the lab.
#sleep 20
#time sudo taskset -c 0 ./sysconfig/rebuildFS.sh $HOST

# Start analyse batch script
# Analyse Runs Script : "-G -s 20 -e 3550" cuts the first 20 seconds
# and stops after 3550 seconds -G prints gigabyte -g prints gigabit
for IFACE in "${INTERFACES[@]}"
do
./analyse/analyse_runs.sh $(pwd) "-g" ${IFACE}
done

results_folder=results/$(date +%F)
mkdir -p $results_folder
commit_hash=$(git rev-parse --short HEAD)
mv run $results_folder/results_commit_${commit_hash}_$(date +%F-%H%M)
