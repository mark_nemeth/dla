import React, {useEffect, useState} from 'react';
import {FormControl, ProgressBar} from 'react-bootstrap';
import ScrollList from '../../Utils/ScrollList/ScrollList';
import CollapseContainer from '../../Utils/CollapsContainer/CollapseContainer';
import {secondsToTimeConverter, statusConverter, timestampToTimeConverter} from '../../../utils/valueConverter';
import './IngestList.scss';

export default function IngestList(props) {
  const [deIngestList, setDeIngestList] = useState([]);
  const [usIngestList, setUsIngestList] = useState([]);
  const [searchQuery, setSearchQuery] = useState("")

  useEffect(() => {
    updateLists();
  }, [props.ingests, searchQuery]);

  function onVehicleSelected(carName) {
    if (props.onVehicleSelected !== undefined) {
      props.onVehicleSelected(carName)
    }
  }

  function getStatusString(ingest) {
    if (ingest.status === 'WAITING') {
      return "Waiting";
    }
    return statusConverter(ingest.status, true) + " " + timestampToTimeConverter(ingest.endTime, false);
  }

  const ingestItem = (item) => (
      <div onClick={() => onVehicleSelected(item.carName)}>
        <div className="ingest-item-title">
          <div className="fifty-percent-card">{item.carName}</div>
          {
            item.status === "STARTED" &&
            <div className="fifty-percent-card text-right">
              {secondsToTimeConverter(item.etaSeconds)}
            </div>
          }
        </div>
        {
          item.status === "STARTED" ?
              (
                  <div className="progress-bar-wrapper">
                    <ProgressBar now={item.progress}
                                 variant="info"
                                 label={`${(item.progress).toFixed(2)}%`}
                    />
                  </div>
              ) :
              (
                  <div>
                    {getStatusString(item)}
                  </div>
              )
        }
      </div>
  );

  function sortIngestsByStatus(list) {
    const ordering = {}; // map for efficient lookup of sortIndex
    const order = ["ERROR", "STARTED", "COMPLETED"];
    for (let i = 0; i < order.length; i++)
      ordering[order[i]] = i;

    return list.sort(function (a, b) {
      return (ordering[a.status] - ordering[b.status]) || (b.endTime - a.endTime);
    });
  }

  function compareIngestsStartTime(i1, i2) {
    return i2.startTime - i1.startTime;
  }

  function updateLists() {
    const filteredIngests = (props.ingests.filter(ingest => ingest.carName.toLowerCase().includes(searchQuery)));
    setDeIngestList(sortIngestsByStatus(Object.entries(groupArrayByKey(filteredIngests.filter(ingest => ingest.ingestCluster !== 'SV'),
        'carName')).map(([key, value]) => value.sort(compareIngestsStartTime)[0])));
    setUsIngestList(sortIngestsByStatus(Object.entries(groupArrayByKey(filteredIngests.filter(ingest => ingest.ingestCluster === 'SV'),
        'carName')).map(([key, value]) => value.sort(compareIngestsStartTime)[0])));
  }

  function onSearchVehicles(event) {
    setSearchQuery(event.target.value.toLowerCase());
    updateLists();
  }

  function groupArrayByKey(array, key) {
    return array
        .reduce((hash, obj) => {
          if (obj[key] === undefined) return hash;
          return Object.assign(hash, {[obj[key]]: (hash[obj[key]] || []).concat(obj)})
        }, {})
  }

  const accordionItems = [
    {
      header: "DE",
      list: deIngestList
    },
    {
      header: "US",
      list: usIngestList
    }
  ];

  return (
      <div className="ingest-list-section">
        <div>
          <FormControl placeholder="Search vehicles..."
                       aria-label="Search query"
                       aria-describedby="basic-addon2"
                       onChange={onSearchVehicles}
                       onClick={onSearchVehicles}
          />
          <div>
            {
              accordionItems.map((item) => (
                      <CollapseContainer label={item.header}
                                         active={false}
                                         variant="list"
                                         preexpanded={true}
                                         key={item.header}>
                        <ScrollList {...props}
                                    showLoading={false}
                                    itemList={item.list}
                                    listItemComponent={ingestItem}
                                    listHeight="auto"
                        />
                      </CollapseContainer>
                  )
              )
            }
          </div>
        </div>
      </div>
  )
}