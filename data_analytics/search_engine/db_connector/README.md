# DB Connector

## Prerequisites
Install setuptools and wheel.
```python
pip install --upgrade setuptools wheel
```


## Create .whl file manually (optional)
```python
python3 setup.py sdist bdist_wheel
```

## Installation
```python
pip install --upgrade dist/DBConnector-0.4.29-py3-none-any.whl 
```

## Usage
```python
# Usage example
connector = DBConnector(hostname, port, db_name, user, key, protocol, ssl)

# Usage option 1) (initialization with context manager)
# preferred if multiple operations are performed
with connector as db:
    sequences = list(db.get_sequences_by_bagfile_guid([bagfile_guid_list]))

# Usage option 2) (lazy initialization without context manager)
# simpler than option 1) but first call might be slower because of the initialization
sequences = list(connector.get_sequences_by_bagfile_guid([bagfile_guid_list]))
```
See more examples in tests/regressiontests/db_connector_test.py
