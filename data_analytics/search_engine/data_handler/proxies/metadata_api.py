"""Interface to the DANF metadata API"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from datetime import datetime, timezone
from typing import List

from ContainerClient.container_client import ContainerClient

logger = logging.getLogger(__name__)


class MetadataApiProxy:
    BAGFILE_DATA_VERSION = '0.1'
    CHILDBAGFILE_DATA_VERSION = '0.1'

    def __init__(self):
        self._client = ContainerClient()

    def upsert_bagfile(self,
                       file_path,
                       file_name,
                       file_type,
                       drive_types,
                       processing_state,
                       processing_priority):
        bagfile_data = {
            "link": file_path,
            "current_link": file_path,
            "version": self.BAGFILE_DATA_VERSION,
            "file_name": file_name,
            "file_type": file_type,
            "tags": [],  # do not add tags here
            "drive_types": drive_types,
            "processing_state": processing_state,
            "processing_priority": processing_priority,
        }
        try:
            bagfile_guid = self._client.upsert_bagfile(bagfile_data)

            logger.info(f"Created metadata for bagfile {bagfile_guid} "
                        f"with payload {bagfile_data}")
            return bagfile_guid
        except Exception as e:
            logger.exception(f"Bagfile creation failed: {e}")

    def upsert_drive(self, drives_json: dict):
        drive_guid = self._client.upsert_drive(drives_json)
        logger.info(f"Created metadata for drives file {drives_json} "
                    f"with payload {drives_json}")
        return drive_guid

    def upsert_childbagfile(self, file_name, link, processing_state):
        childbagfile_data = {
            "version": self.CHILDBAGFILE_DATA_VERSION,
            "parent_guid": "00000000-0000-0000-0000-000000000000",
            "start": 0,
            "end": 1,
            "file_name": file_name,
            "link": link,
            "size": 0,
            "topics": [],
            "num_messages": 0,
            "vehicle_id_num": "unknown",
            "processing_state": processing_state,
        }
        childbagfile_guid = self._client.upsert_childbagfile(childbagfile_data)

        logger.info(f"Created metadata for childbagfile {childbagfile_guid} "
                    f"with payload {childbagfile_data}")
        return childbagfile_guid

    def add_bagfile_state_skip_events(self, guid, names, message=None):
        # unix timestamp in ms resolution
        now = int(datetime.now(tz=timezone.utc).timestamp() * 1000)

        for name in names:
            event = {
                'name': name,
                'version': 'unspecified',
                'time': now,
                'status': 'SKIPPED',
                'report': {}
            }
            if message:
                event['report']['message'] = message
            logger.info(f"Adding SKIP state event to bagfile {guid} "
                        f"for step {name}")
            self._client.add_bagfile_state_event(guid, event)

    def update_bagfile_processing_state(self, guid, state):
        self._client.update_bagfile(guid, {"processing_state": state})

    def update_drive_processing_state(self, guid, state):
        self._client.update_drive(guid, {"processing_state": state})

    def update_bagfile_soft_deletion_state(self, guid: str, is_deleted: bool):
        self._client.update_bagfile(guid, {"is_deleted": is_deleted})

    def update_bagfile_drive_guid(self, bagfile_guid, drive_guid):
        self._client.update_bagfile(bagfile_guid, {"drive_guid": drive_guid})

    def update_bagfile_drive_information(self, bagfile_guid: str, drive_guid: str, drive_types: List[str]) -> None:
        update_dict = {"drive_guid": drive_guid,
                       "drive_types": drive_types}
        self._client.update_bagfile(bagfile_guid, update_dict)

    def update_childbagfile_processing_state(self, guid, state):
        self._client.update_childbagfile(guid, {"processing_state": state})

    def get_bagfile(self, guid):
        return self._client.get_bagfile(guid)

    def mark_bagfile_for_testing(self, guid):
        self._client.update_bagfile(guid, {"testing": True})

    def delete_bagfile_and_orphans(self, guid: str, hard_delete: bool = False):
        self._client.delete_bagfile(guid,
                                    ["childbagfiles", "extractor_requests"],
                                    hard_delete)

    def delete_childbagfile(self, guid: str, hard_delete: bool = False):
        self._client.delete_childbagfile(guid, hard_delete)

    def delete_drivefile(self, guid: str, hard_delete: bool = False):
        self._client.delete_drivefile(guid, ['bagfiles'], hard_delete)

    def get_childbagfiles_from_bagfile(self, bagfile_guid):
        bagfile = self._client.get_bagfile(bagfile_guid,
                                           whitelist=['childbagfiles'])
        return bagfile.get('childbagfiles', [])

    def create_flow_run(self, flow_run_data):
        return self._client.create_flow_run(flow_run_data)

    def set_flow_run_wf_id(self, flow_run_guid, wf_id):
        self._client.update_flow_run(flow_run_guid,
                                     {'kubeflow_workflow_id': wf_id})
