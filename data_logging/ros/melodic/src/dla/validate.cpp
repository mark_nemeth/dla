#include <stdio.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/duration.h>

using namespace std;

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		printf("Usage: validate <bag_file>\n");
		return 0;
	}

	printf("Checking %s\n", argv[1]);

	rosbag::Bag bag;
	bag.open(argv[1]);
	rosbag::View view(bag);

	// Iterate all messages
	long long messageCount = 0;
	for (rosbag::MessageInstance const m: view)
	{
		if (m.getTopic() != "/test_array")
			continue;

		if (m.size() != 1024 * 1024)
		{
			printf("Invalid message size %d\n", m.size());
			return -1;
		}

		messageCount++;
	}

	bag.close();

	printf("Validated %lld messages\n", messageCount);

	return 0;
}
