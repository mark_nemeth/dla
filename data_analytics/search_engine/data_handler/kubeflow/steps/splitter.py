__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import (ENV_SPAN_ID, ENV_TRACE_ID,
                                       ENV_TRACE_OPTIONS)
from config.config_handler import config_dict
from kfp import dsl
from kubeflow.steps.common import (VM_LABEL_ATTRIBUTE, RETRY_NUM,
                                   PIPELINE_POD_LABEL,
                                   BAGFILE_POD_LABEL, FILE_NAME_POD_LABEL,
                                   sanitize_label, METADATA_API_BASE_URL,
                                   SEARCH_API_BASE_URL,
                                   APPINSIGHTS_KEY,
                                   DANF_ENV, AUTH, AUTH_TOKEN_URL,
                                   CPU_RESOURCE_TITLE,
                                   MEMORY_RESOURCE_TITLE, TESTING_POD_LABEL,
                                   VIN_POD_LABEL,
                                   extract_file_name_from_path, HTTP_PROXY,
                                   HTTPS_PROXY, NO_PROXY,
                                   AUTH_METADATA_AUDIENCE,
                                   AUTH_METADATA_CLIENT_CRED_ID,
                                   AUTH_METADATA_CLIENT_CRED_SECRET,
                                   get_pvc_volume, AUTH_SEARCH_AUDIENCE,
                                   AUTH_SEARCH_CLIENT_CRED_ID,
                                   AUTH_SEARCH_CLIENT_CRED_SECRET,
                                   attach_blobfuse_volume,
                                   CUSTOM_NODE_RESOURCE_TITLE,
                                   CUSTOM_STORAGE_VM_TITLE,
                                   AUTH_DATAHANDLER_CLIENT_CRED_SECRET,
                                   AUTH_DATAHANDLER_CLIENT_CRED_ID,
                                   AUTH_DATAHANDLER_AUDIENCE, BaseStep)
from kubernetes.client import V1SecurityContext, V1VolumeMount
from kubernetes.client.models import V1EnvVar

logger = logging.getLogger(__name__)


class AzureSplitterOp(BaseStep):
    INPUT_MOUNT_BASE_PATH = "/data"
    OUTPUT_MOUNT_BASE_PATH = "/outputs"
    NAME = "danf-splitter"
    DOCKER_IMG = config_dict.get_value(NAME)
    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "1Gi"
    MEMORY_LIMIT = "3Gi"
    DLA_CONVERTER = "True"
    SILENT_TESTING_SUBFOLDER = "silent_testing_snippets"

    @classmethod
    def create_azure_splitter(cls,
                              bagfile_guid,
                              flow_run_guid,
                              input_location,
                              output_container,
                              mount_id,
                              test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_location: path to the bagfile in the storage account
        :param output_container: location of the blob storage container where the data should be written to
        :param mount_id: used to generate unique mount names
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the Stripper operation
        """
        # Output should be in the same storage account as the input, but in a container inside the storageaccount called "output"
        # e.g.
        # input in: baseinfraabtraw01sadev/20200220/20200220_134051_wdd2221591a470493_cid_rc124_m1_sm_open_loop_0_original_test.bag
        # output in: baseinfraabtfltr01sadev/20200220/...

        output_location = input_location
        output_location.storage_account.replace("raw", "fltr")

        logger.info(
            f"SplitterOP created for bagfile_guid {bagfile_guid}, "
            f"input_location {input_location.storage_account}, "
            f"output_location {output_location.storage_account} "
            f"and output_container {output_location.container}")

        # derive a 'virtual' sub directory within output container from the input container name
        output_container_sub_path = output_location.container

        childbagfile_link_template = f"{output_location.storage_account}/{output_location.container}"

        step = AzureSplitterOp(
            flow_run_guid,
            bagfile_guid,
            input_location.file,
            vin='undefined'
        )

        if test_mode:
            step.enable_test_mode()

        step.set_image_pull_policy('Always') \
            .enable_tracing_integration() \
            .add_kubernetes_resources_for_pod(cls.CPU_REQUEST,
                                              cls.MEMORY_REQUEST, cls.CPU_LIMIT,
                                              cls.MEMORY_LIMIT) \
            .connect_search_api() \
            .connect_metadata_api() \
            .enable_proxy() \
            .enable_tracing_integration() \
            .add_authentication_env_variables()

        step.add_env_variable(V1EnvVar(name="MODE", value="splitter")) \
            .add_env_variable(V1EnvVar(name="BAGFILE_PATH",
                                       value=f"{cls.INPUT_MOUNT_BASE_PATH}/{input_location.file}")) \
            .add_env_variable(
            V1EnvVar(name="OUTPUT_FOLDER", value=cls.OUTPUT_MOUNT_BASE_PATH)) \
            .add_env_variable(V1EnvVar(name="CHILDBAGFILE_LINK_TEMPLATE",
                                       value=childbagfile_link_template)) \
            .add_env_variable(
            V1EnvVar(name="DLA_CONVERTER", value=cls.DLA_CONVERTER))

        attach_blobfuse_volume(step,
                               cls.INPUT_MOUNT_BASE_PATH,
                               f"splitting-mount-input-{mount_id}",
                               input_location.storage_account,
                               input_location.container)
        attach_blobfuse_volume(step,
                               cls.OUTPUT_MOUNT_BASE_PATH,
                               f"splitting-mount-output-{mount_id}",
                               output_location.storage_account,
                               output_location.container,
                               False,
                               0)
        return step


class SplitterOp(dsl.ContainerOp):
    INPUT_MOUNT_BASE_PATH = "/data"
    OUTPUT_MOUNT_BASE_PATH = "/outputs"
    NAME = "danf-splitter"
    DOCKER_IMG = config_dict.get_value(NAME)
    CPU_REQUEST = "300m"
    CPU_LIMIT = "1000m"
    MEMORY_REQUEST = "1Gi"
    MEMORY_LIMIT = "3Gi"
    DLA_CONVERTER = "True"
    SILENT_TESTING_SUBFOLDER = "silent_testing_snippets"

    @classmethod
    def create_azure_splitter(cls,
                              bagfile_guid,
                              flow_run_guid,
                              mount_id,
                              input_storage_account,
                              input_container,
                              input_file_path,
                              output_storage_account,
                              output_container,
                              childbagfiles_output_dir,
                              test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param mount_id: used to generate unique mount names
        :param input_storage_account: storage acc. where bagfile is located
        :param input_container: name of the blob storage container where
            the bagfile is located
        :param input_file_path: path to the bagfile in the pod
        :param output_storage_account: storage acc. where childbagfiles
            should be written to
        :param output_container: blob storage container where childbagfiles
            should be written to
        :param childbagfiles_output_dir: directory in the pod to which dummy
            files should be written
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the Splitter operation configured for
            azure
        """
        childbagfile_link_template = f"{output_storage_account}/{output_container}" + "/{}"

        step = cls._create_base_instance(bagfile_guid,
                                         flow_run_guid,
                                         input_file_path,
                                         childbagfile_link_template,
                                         childbagfiles_output_dir,
                                         test_mode)

        step.add_resource_request(CUSTOM_NODE_RESOURCE_TITLE, "2") \
            .add_resource_limit(CUSTOM_NODE_RESOURCE_TITLE, "2") \
            .add_node_selector_constraint(VM_LABEL_ATTRIBUTE,
                                          CUSTOM_STORAGE_VM_TITLE)

        attach_blobfuse_volume(step,
                               cls.INPUT_MOUNT_BASE_PATH,
                               f"splitting-mount-input-{mount_id}",
                               input_storage_account,
                               input_container)
        attach_blobfuse_volume(step,
                               cls.OUTPUT_MOUNT_BASE_PATH,
                               f"splitting-mount-output-{mount_id}",
                               output_storage_account,
                               output_container,
                               False,
                               0)
        return step

    @classmethod
    def create_sv_splitter(cls,
                           bagfile_guid,
                           flow_run_guid,
                           input_file_path,
                           input_mount_sub_path,
                           childbagfiles_output_dir,
                           output_mount_sub_path,
                           childbagfile_link_template,
                           pvc_name,
                           user_id,
                           vin,
                           test_mode):
        """
        :param bagfile_guid: of the bagfile to process
        :param flow_run_guid: id of the flow run in which this step is executed
        :param input_file_path: path to the bagfile in the pod
        :param input_mount_sub_path: path to mount within input volume
        :param childbagfiles_output_dir: directory in the pod to which dummy
            files should be written
        :param output_mount_sub_path: path to mount within output volume
        :param childbagfile_link_template: template to expand when generating
            the link property for childbagfiles
        :param pvc_name: name of the pvc to mount
        :param user_id: step will be executed with this user id
        :param vin: of the vehicle from which the processed bagfile originates
        :param test_mode: bool to indicate if step should be run in test mode
        :return: a new instance of the Splitter operation configured for
            the on prem sunnyvale cluster
        """
        step = cls._create_base_instance(bagfile_guid,
                                         flow_run_guid,
                                         input_file_path,
                                         childbagfile_link_template,
                                         childbagfiles_output_dir,
                                         test_mode)

        step.set_security_context(V1SecurityContext(run_as_user=user_id))

        step.add_pod_label(name=VIN_POD_LABEL, value=sanitize_label(vin))

        step.add_env_variable(V1EnvVar(name="DISABLE_OVERLAP", value="True")) \
            .add_env_variable(V1EnvVar(name="SILENT_TESTING_SUBFOLDER",
                                       value=cls.SILENT_TESTING_SUBFOLDER)) \
            .add_env_variable(V1EnvVar(name="APPINSIGHTS_INSTRUMENTATIONKEY",
                                       value=APPINSIGHTS_KEY)) \
            .add_env_variable(V1EnvVar(name="HTTP_PROXY", value=HTTP_PROXY)) \
            .add_env_variable(V1EnvVar(name="HTTPS_PROXY", value=HTTPS_PROXY)) \
            .add_env_variable(V1EnvVar(name="no_proxy", value=NO_PROXY))

        volume_name = "mapr-mount"
        step.add_volume(get_pvc_volume(volume_name, pvc_name)) \
            .add_volume_mount(V1VolumeMount(
            mount_path=cls.INPUT_MOUNT_BASE_PATH,
            name=volume_name)) \
            .add_volume_mount(V1VolumeMount(
            mount_path=cls.OUTPUT_MOUNT_BASE_PATH,
            name=volume_name))
        return step

    @classmethod
    def _create_base_instance(cls,
                              bagfile_guid,
                              flow_run_guid,
                              input_file_path,
                              childbagfile_link_template,
                              childbagfiles_output_dir,
                              test_mode):
        step = SplitterOp() \
            .set_retry(RETRY_NUM) \
            .add_pod_label(name=PIPELINE_POD_LABEL,
                           value=sanitize_label(cls.NAME)) \
            .add_pod_label(name=BAGFILE_POD_LABEL,
                           value=sanitize_label(bagfile_guid)) \
            .add_pod_label(name=FILE_NAME_POD_LABEL, value=sanitize_label(
            extract_file_name_from_path(input_file_path)))

        if test_mode:
            step.add_pod_label(name=TESTING_POD_LABEL, value='true')

        step.add_env_variable(
            V1EnvVar(name="BAGFILE_PATH", value=input_file_path)) \
            .add_env_variable(V1EnvVar(name="BAGFILE_GUID", value=bagfile_guid)) \
            .add_env_variable(
            V1EnvVar(name="FLOW_RUN_GUID", value=flow_run_guid)) \
            .add_env_variable(
            V1EnvVar(name="SEARCH_API_BASE_URL", value=SEARCH_API_BASE_URL)) \
            .add_env_variable(
            V1EnvVar(name="METADATA_API_BASE_URL", value=METADATA_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="APPINSIGHTS_INSTRUMENTATIONKEY",
                                       value=APPINSIGHTS_KEY)) \
            .add_env_variable(V1EnvVar(name="DANF_ENV", value=DANF_ENV)) \
            .add_env_variable(
            V1EnvVar(name="OUTPUT_FOLDER", value=childbagfiles_output_dir)) \
            .add_env_variable(V1EnvVar(name="CHILDBAGFILE_LINK_TEMPLATE",
                                       value=childbagfile_link_template)) \
            .add_env_variable(
            V1EnvVar(name="DLA_CONVERTER", value=cls.DLA_CONVERTER)) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_ID,
                                       value=tracing.get_propagation_vars().get(
                                           ENV_TRACE_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_SPAN_ID,
                                       value=tracing.get_propagation_vars().get(
                                           ENV_SPAN_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_OPTIONS,
                                       value=tracing.get_propagation_vars().get(
                                           ENV_TRACE_OPTIONS))) \
            .add_env_variable(V1EnvVar(name="AUTH", value=AUTH)) \
            .add_env_variable(
            V1EnvVar(name="AUTH_TOKEN_URL", value=AUTH_TOKEN_URL)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_AUDIENCE",
                                       value=AUTH_DATAHANDLER_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_ID",
                                       value=AUTH_DATAHANDLER_CLIENT_CRED_ID)) \
            .add_env_variable(
            V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_SECRET",
                     value=AUTH_DATAHANDLER_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_AUDIENCE",
                                       value=AUTH_METADATA_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_ID",
                                       value=AUTH_METADATA_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_SECRET",
                                       value=AUTH_METADATA_CLIENT_CRED_SECRET)) \
            .add_env_variable(
            V1EnvVar(name="AUTH_SEARCH_AUDIENCE", value=AUTH_SEARCH_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_ID",
                                       value=AUTH_SEARCH_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_SECRET",
                                       value=AUTH_SEARCH_CLIENT_CRED_SECRET))

        step.add_resource_request(CPU_RESOURCE_TITLE, cls.CPU_REQUEST) \
            .add_resource_request(MEMORY_RESOURCE_TITLE, cls.MEMORY_REQUEST) \
            .add_resource_limit(CPU_RESOURCE_TITLE, cls.CPU_LIMIT) \
            .add_resource_limit(MEMORY_RESOURCE_TITLE, cls.MEMORY_LIMIT)

        return step

    def __init__(self):
        super().__init__(
            name=self.NAME,
            image=self.DOCKER_IMG
        )
