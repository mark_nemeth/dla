"""Setup file"""

from setuptools import setup, find_packages
from ContainerClient.config.constants import VERSION

setup(name='ContainerClient',
      version=VERSION,
      packages=find_packages(exclude=["tests"]),
      package_data={'': ['config/*.init']},
      long_description=open('README.md', 'r').read(),
      long_description_content_type="text/markdown",
      test_suite="tests",
      install_requires=[
          'requests>=2.22.0', 'oauthlib>=3.1.0', 'requests-oauthlib>=1.3.0',
          'DANFUtils>=0.2.29'
      ])
