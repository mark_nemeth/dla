import React from 'react';
import ErrorPage from "./ErrorPage";
import {shallow} from "enzyme/build";

describe('ErrorPage component', () => {

  it('should renders without crashing', () => {
    const wrapper = shallow(<ErrorPage />);
    expect(wrapper.exists()).toBe(true);
  });

});