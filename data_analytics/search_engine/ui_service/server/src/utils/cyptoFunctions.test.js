import crypto from 'crypto';
import {decryptStringData, encryptStringData} from './cyptoFunctions';

describe('Redis encryption', () => {

  it('encrypt and decrypt redis data', () => {
    const key = Buffer.from(crypto.randomBytes(32), 'utf8');
    const inputData = {
      access_token: 'ABC',
      id_token: 123
    };
    const inputString = JSON.stringify(inputData);
    const encryptData = encryptStringData(inputString, key);
    const decyptData = decryptStringData(encryptData, key);
    const outputData = JSON.parse(decyptData);
    console.log('%s === %s', inputData, outputData);
    expect(outputData).toStrictEqual(inputData);
  });

  it('erro while decrypt redis data with wrong pw', () => {
    let key = Buffer.from(crypto.randomBytes(32), 'utf8');
    const inputData = {
      access_token: 'ABC',
      id_token: 123
    };
    const inputString = JSON.stringify(inputData);
    const encryptData = encryptStringData(inputString, key);
    key = Buffer.from(crypto.randomBytes(32), 'utf8');
    const decyptData = decryptStringData(encryptData, key);
    const outputData = JSON.parse(decyptData);
    console.log('%s !== %s', inputData, outputData);
    expect(outputData).not.toStrictEqual(inputData);
  });

});