__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from pathlib import PurePath, Path
from typing import Optional

from kfp import dsl
from kubernetes.client import V1LocalObjectReference

from DANFUtils.danf_environment import danf_env, Platform

from kubeflow.flows.common import BaseFlow, get_mount_id
from kubeflow.steps.atpftst_extractor import AtpftstExtractorOp
from kubeflow.steps.atpdml_extractor import AtpdmlExtractorOp
from kubeflow.steps.atpdml_poi_extractor import AtpdmlPoiExtractorOp
from kubeflow.steps.atbeval_extractor import AtbevalExtractorOp
from kubeflow.steps.cleanup import CleanupOp
from kubeflow.steps.danf_extractor import DanfExtractorInitOp, DanfExtractorOp
from kubeflow.steps.splitter import SplitterOp
from kubeflow.steps.stripper import StripperOp
from kubeflow.steps.rca_extractor import RcaExtractorOp

logger = logging.getLogger(__name__)


class DanfOriginalProcessingFlow(BaseFlow):

    NAME = "danf-original-processing-flow-v1"
    VERSION = "1.0"
    DESCRIPTION = "on prem flow for processing ORIGINAL bagfiles"
    IMAGE_PULL_SECRET = "dla-docker-pull-secret"

    @classmethod
    def create(cls,
               bagfile: dict,
               flow_run_guid: str,
               test_mode: bool,
               output_path_override: Optional[str]):
        return cls._build_pipeline(
            bagfile['guid'],
            flow_run_guid,
            bagfile['link'],
            output_path_override,
            test_mode)

    @classmethod
    def _build_pipeline(cls,
                        bagfile_guid,
                        flow_run_guid,
                        file_path,
                        output_path_override,
                        test_mode):
        @dsl.pipeline(
            name=cls.NAME,
            description=cls.DESCRIPTION
        )
        def processing_pipeline():
            # initialize params
            mapr_pvc_name = cls.get_config().get('mapr_pvc_name')
            mapr_user_id = cls.get_config().get('mapr_user_id')
            path_to_bagfile_in_mapr = PurePath(file_path)
            file_name = path_to_bagfile_in_mapr.name
            cluster = PurePath(*path_to_bagfile_in_mapr.parts[0:3])
            # transform path s.t. it can be used for mounting directory in pvc,
            # e.g. /mapr/624.mbc.us/usa/data/input/.../plog/file.bag
            # => data/input/.../plog
            path_to_bagfile_dir_in_mapr = (path_to_bagfile_in_mapr
                                           .relative_to(cluster)
                                           .parent)
            output_path = cls.get_output_path(
                output_path_override,
                path_to_bagfile_dir_in_mapr)

            vin = cls.extract_vin(path_to_bagfile_dir_in_mapr)
            env = danf_env()

            # TODO: pvc volume mount subpaths are currently not used since
            # they don't work with the mapr NFS mount. They are still passed
            # as parameters in case we switch back to the mapr csi mounts

            cleanup_step = CleanupOp.create_for_on_prem(
                tasks=['set-sync-flag', 'update-flow-run'],
                bagfile_guid=bagfile_guid,
                flow_run_guid=flow_run_guid,
                bagfile_name=file_name,
                test_mode=test_mode
            )

            with dsl.ExitHandler(cleanup_step):
                init_step = DanfExtractorInitOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=f"{DanfExtractorInitOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                    output_container=f"{DanfExtractorInitOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                    mount_id=get_mount_id(),
                    test_mode=test_mode
                )

                # strip the bagfiles by topic into childbagfiles
                stripper_step = StripperOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=str(path_to_bagfile_dir_in_mapr),
                    output_container=output_path,
                    mount_id=get_mount_id(),
                    test_mode=test_mode
                ).after(init_step)

                if cls.get_config().get('enable_atbeval_extractor'):
                    # The input of the atbeval_extractor is the planning_IO stripped file but we have no guarantee
                    # that the stripper_step above actually created one neither do we know how it will be named
                    # (which is defined in a lot of configs elsewhere).
                    # For now we assume it succeeded and manipulate the filename to the form the Stripper class from
                    # the Stripper container would output,
                    # see ca. data_analytics/search_engine/splitter/index.py:277 -> Stripper._get_file_name
                    # TODO: modify Stripper to report stripped files back to kubeflow as something like
                    #   file_outputs={"stripped_files": "/tmp/stripped_files"}
                    #   and check planning_io stripped file using "with dsl.Condition(...)"
                    file_name_base = ".".join(file_name.split(".")[:-1])  # file_name is guaranteed to have no preceeding path
                    planning_io_stripped_file = str(
                        Path(AtbevalExtractorOp.INPUT_MOUNT_BASE_PATH)  # "/input"
                        / output_path  # "data/input/.../plog"
                        / Path(file_name_base.replace("ORIGINAL", "STRIPPED") + "_planning_io.bag"))
                    atbeval_extractor_step = AtbevalExtractorOp.create(
                        bagfile_guid=bagfile_guid,
                        flow_run_guid=flow_run_guid,
                        input_file_path=planning_io_stripped_file,
                        pvc_name=mapr_pvc_name,
                        user_id=mapr_user_id,
                        vin=vin,
                        test_mode=test_mode
                    ).after(stripper_step)

                # FTS (Functional Testing Team) - process the original bagfiles and find the events and save them in the DB
                if env.platform == Platform.ON_PREM:
                    atpftst_extractor_step = AtpftstExtractorOp.create(
                        bagfile_guid=bagfile_guid,
                        flow_run_guid=flow_run_guid,
                        input_file_path=f"{AtpftstExtractorOp.INPUT_MOUNT_BASE_PATH}/{file_name}",
                        input_mount_sub_path=str(path_to_bagfile_dir_in_mapr),
                        pvc_name='kubeflow-dla-danf-csi',  # mounted via csi, due to missing posix groups
                        user_id=mapr_user_id,
                        vin=vin,
                        test_mode=test_mode
                    ).after(init_step)

                # extract events for DANF
                extractor_step = DanfExtractorOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_location=f"{DanfExtractorOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                    output_container=f"{DanfExtractorOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                    mount_id=get_mount_id(),
                    test_mode=test_mode
                ).after(init_step)

                # extract POI for DML
                atpdml_poi_extractor_step = AtpdmlPoiExtractorOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_file_path=f"{DanfExtractorOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                    output_folder=f"{DanfExtractorOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                    pvc_name=mapr_pvc_name,
                    vin=vin,
                    test_mode=test_mode
                ).after(extractor_step)

                # extract events for DML
                atpdml_extractor_step = AtpdmlExtractorOp.create(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_file_path=f"{DanfExtractorOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                    output_folder=f"{DanfExtractorOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                    pvc_name=mapr_pvc_name,
                    vin=vin,
                    test_mode=test_mode
                ).after(atpdml_poi_extractor_step)

                # split the bagfile into childbagfile by time
                splitter_step = SplitterOp.create_sv_splitter(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_file_path=f"{SplitterOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                    input_mount_sub_path=str(path_to_bagfile_dir_in_mapr),
                    childbagfiles_output_dir=f"{SplitterOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                    output_mount_sub_path=output_path,
                    childbagfile_link_template=f"{cluster}/{output_path}" + "/{}",
                    pvc_name=mapr_pvc_name,
                    user_id=mapr_user_id,
                    vin=vin,
                    test_mode=test_mode
                ).after(atpdml_extractor_step)

                # RCA (root cause analysis) - analysis
                rca_step = RcaExtractorOp.create_rca_extractor(
                    bagfile_guid=bagfile_guid,
                    flow_run_guid=flow_run_guid,
                    input_file_path=f"{RcaExtractorOp.INPUT_MOUNT_BASE_PATH}/{path_to_bagfile_dir_in_mapr}/{file_name}",
                    input_mount_sub_path=str(path_to_bagfile_dir_in_mapr),
                    childbagfiles_output_dir=f"{RcaExtractorOp.OUTPUT_MOUNT_BASE_PATH}/{output_path}",
                    output_mount_sub_path=output_path,
                    childbagfile_link_template=f"{cluster}/{output_path}" + "/{}",
                    pvc_name=mapr_pvc_name,
                    user_id=mapr_user_id,
                    vin=vin,
                    test_mode=test_mode
                ).after(splitter_step)
                logger.info(f'Created: RcaExtractorOp')

            logger.info("Getting pipeline configuration")
            pipeline_conf = dsl.get_pipeline_conf()
            logger.info("Setting image pull secrets")
            pipeline_conf.set_image_pull_secrets([
                V1LocalObjectReference(cls.IMAGE_PULL_SECRET)
            ])
            logger.info("DanfOriginalProcessingFlow._build_pipeline(...) finished!")

        return DanfOriginalProcessingFlow(processing_pipeline)

    @classmethod
    def get_output_path(cls,
                        output_path_override,
                        path_to_bagfile_dir_in_mapr):

        if output_path_override:
            logger.debug(f"output path overridden by {output_path_override}")
            return output_path_override

        try:
            new_path = list(path_to_bagfile_dir_in_mapr.parts)
            if "buffer" in new_path:
                index = new_path.index("buffer")
                new_path[index] = "input"
            elif "buffer_test" in new_path:
                index = new_path.index("buffer_test")
                new_path[index] = "input_test"
            path_to_bagfile_dir_in_mapr = Path(*new_path)
        except ValueError:
            logger.debug("Buffer not found in input path...")
        return str(path_to_bagfile_dir_in_mapr)

    @classmethod
    def extract_vin(cls, path_to_bagfile_dir_in_mapr):
        # get vehicle VIN
        segments = list(path_to_bagfile_dir_in_mapr.parts)
        try:
            vin_index = segments.index('plog') + 1
            return segments[vin_index]
        except ValueError:
            logger.warning(f"path {path_to_bagfile_dir_in_mapr} does not "
                           f"contain 'plog'. "
                           f"Vehicle whitelisting will be ignored")
            return "unknown"
