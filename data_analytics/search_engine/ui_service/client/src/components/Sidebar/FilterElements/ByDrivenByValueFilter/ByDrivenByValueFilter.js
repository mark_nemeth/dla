import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import './ByDrivenByValueFilter.scss'

export function ByDrivenByValueFilter(props) {

  function onDrivenByValueSelect(drivenByValue) {
    if (props.selectedDrivenByValueList.includes(drivenByValue)) props.filterByDrivenByValues(props.selectedDrivenByValueList.filter(t => t !== drivenByValue));
    else props.filterByDrivenByValues(props.selectedDrivenByValueList.concat(drivenByValue));
  }

  function resetDrivenByValueList() {
    props.filterByDrivenByValues([]);
  }

  const showLoading = props.drivenByValueList === undefined || props.drivenByValueList.length <= 0;
  const subLabel = props.selectedDrivenByValueList.length + ' selected';
  const active = props.selectedDrivenByValueList.length > 0;

  const listItemComponent = (item) => (
      <label className="driven-by-value-list-item">
        <input type="checkbox" value={item}
               onChange={event => onDrivenByValueSelect(event.target.value)}
               checked={props.selectedDrivenByValueList.includes(item)}/>
        &nbsp; {item}
      </label>
  );

  return (
      <CollapseContainer label={active > 0 ? "Driven by: " + subLabel : "Driven by"}
                         active={active}
                         variant="list"
                         resetCallback={resetDrivenByValueList}
      >
        <ScrollList
            showLoading={showLoading}
            itemList={props.drivenByValueList}
            listItemComponent={listItemComponent}
        />
      </CollapseContainer>
  );
}