__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import re
from abc import abstractmethod
from pathlib import Path

from DANFUtils.logging import logger
from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import ENV_TRACE_ID, ENV_SPAN_ID, \
    ENV_TRACE_OPTIONS
from kfp import dsl
from kubernetes import client as k8s_client
from kubernetes.client import (V1PersistentVolumeClaimVolumeSource, V1EnvVar,
                               V1HostPathVolumeSource, V1ConfigMapVolumeSource)

from config.config_handler import config_dict

logger.set_log_level("debug")

# main constants
METADATA_API_BASE_URL = config_dict.get_value("metadata_api_base_url")
SEARCH_API_BASE_URL = config_dict.get_value("search_api_base_url")
DATA_HANDLER_BASE_URL = config_dict.get_value("data_handler_base_url")

PIPELINE_POD_LABEL = "danf_processing_flow"
BAGFILE_POD_LABEL = "danf_bagfile"
INPUT_POD_LABEL = "danf_input_dir"
OUTPUT_POD_LABEL = "danf_output_dir"
VIN_POD_LABEL = "danf_vin"
FILE_NAME_POD_LABEL = "danf_filename"
TESTING_POD_LABEL = "testing"

CUSTOM_NODE_RESOURCE_TITLE = "daimler.com/athena-custom-storage"
VM_LABEL_ATTRIBUTE = "beta.kubernetes.io/instance-type"
CUSTOM_STORAGE_VM_TITLE = "Standard_L8s"
GPU_VM_TITLE = "Standard_ND6s"
SERVICE_VM_TITLE = "Standard_D4s_v3"

MOUNT_DATA_BASE_PATH = "/data"
MOUNT_OUTPUT_DATA_BASE_PATH = "/outputs"

GPU_RESOURCE_TITLE = "nvidia.com/gpu"
CPU_RESOURCE_TITLE = "cpu"
MEMORY_RESOURCE_TITLE = "memory"

RETRY_NUM = 0

APPINSIGHTS_KEY = os.environ.get('APPINSIGHTS_INSTRUMENTATIONKEY')

HTTP_PROXY = os.environ.get('HTTP_PROXY')
HTTPS_PROXY = os.environ.get('HTTPS_PROXY')
NO_PROXY = os.environ.get('no_proxy')

SONDER_PROXY = config_dict.get_value("SONDER_PROXY")

DANF_ENV = os.environ.get('DANF_ENV')

AUTH = config_dict.get_value('AUTH')
AUTH_TOKEN_URL = config_dict.get_value('AUTH_TOKEN_URL')

AUTH_METADATA_AUDIENCE = config_dict.get_value('AUTH_METADATA_AUDIENCE')
AUTH_METADATA_CLIENT_CRED_ID = config_dict.get_value('AUTH_METADATA_CLIENT_CRED_ID')
AUTH_METADATA_CLIENT_CRED_SECRET = config_dict.get_value('AUTH_METADATA_CLIENT_CRED_SECRET')

AUTH_SEARCH_AUDIENCE = config_dict.get_value('AUTH_SEARCH_AUDIENCE')
AUTH_SEARCH_CLIENT_CRED_ID = config_dict.get_value('AUTH_SEARCH_CLIENT_CRED_ID')
AUTH_SEARCH_CLIENT_CRED_SECRET = config_dict.get_value('AUTH_SEARCH_CLIENT_CRED_SECRET')

AUTH_DATAHANDLER_AUDIENCE = config_dict.get_value('AUTH_DATAHANDLER_AUDIENCE')
AUTH_DATAHANDLER_CLIENT_CRED_ID = config_dict.get_value('AUTH_DATAHANDLER_CLIENT_CRED_ID')
AUTH_DATAHANDLER_CLIENT_CRED_SECRET = config_dict.get_value('AUTH_DATAHANDLER_CLIENT_CRED_SECRET')


class BaseStep(dsl.ContainerOp):
    """Base class for all steps that should be executed as part of a DANF
    flow. Provides common functionality. Extends kubeflow dsl.ContainerOp."""

    @property
    @classmethod
    @abstractmethod
    def NAME(cls) -> str:
        """Name of the step"""
        raise NotImplementedError

    @property
    @classmethod
    @abstractmethod
    def DOCKER_IMG(cls) -> str:
        """Docker image to use for this step"""
        raise NotImplementedError

    def __init__(self,
                 flow_run_guid: str,
                 bagfile_guid: str,
                 bagfile_name: str,
                 vin: str,
                 **container_op_kwargs):
        """Creates a new instance with configuration common to all steps.

        :param flow_run_guid: id of the flow run in which this step is executed
        :param bagfile_guid: of the bagfile to process
        :param bagfile_name: of the bagfile to process
        :param vin: of the vehicle from which the processed bagfile originates
        :param container_op_kwargs: optional kwargs that will be passed to
            the dsl.ContainerOp constructor
        """
        super().__init__(
            name=self.NAME,
            image=self.DOCKER_IMG,
            **container_op_kwargs
        )

        self.set_retry(RETRY_NUM) \
            .add_env_variable(V1EnvVar(name="APPINSIGHTS_INSTRUMENTATIONKEY", value=APPINSIGHTS_KEY)) \
            .add_env_variable(V1EnvVar(name="DANF_ENV", value=DANF_ENV)) \
            .add_env_variable(V1EnvVar(name="BAGFILE_GUID", value=bagfile_guid)) \
            .add_env_variable(V1EnvVar(name="FLOW_RUN_GUID", value=flow_run_guid)) \
            .add_pod_label(name=PIPELINE_POD_LABEL, value=sanitize_label(self.NAME)) \
            .add_pod_label(name=BAGFILE_POD_LABEL, value=sanitize_label(bagfile_guid)) \
            .add_pod_label(name=VIN_POD_LABEL, value=sanitize_label(vin)) \
            .add_pod_label(name=FILE_NAME_POD_LABEL, value=sanitize_label(bagfile_name))

    def enable_tracing_integration(self):
        """Set environment variables relevant for integration with the DANF
        tracing library in this step.
        """
        self.container \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_ID, value=tracing.get_propagation_vars().get(ENV_TRACE_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_SPAN_ID, value=tracing.get_propagation_vars().get(ENV_SPAN_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_OPTIONS, value=tracing.get_propagation_vars().get(ENV_TRACE_OPTIONS)))
        return self

    def connect_metadata_api(self):
        """Adds connection details for the metadata API to this step.
        Does not configure authentication and http proxy settings."""
        self.container \
            .add_env_variable(V1EnvVar(name="METADATA_API_BASE_URL", value=METADATA_API_BASE_URL))
        return self

    def connect_search_api(self):
        """Adds connection details for the search API to this step.
        Does not configure authentication and http proxy settings."""
        self.container \
            .add_env_variable(V1EnvVar(name="SEARCH_API_BASE_URL", value=SEARCH_API_BASE_URL))
        return self

    def enable_proxy(self):
        """Sets environment variables in this step for using the correct
        HTTP(S) proxy."""
        self.container \
            .add_env_variable(V1EnvVar(name="HTTP_PROXY", value=HTTP_PROXY)) \
            .add_env_variable(V1EnvVar(name="HTTPS_PROXY", value=HTTPS_PROXY)) \
            .add_env_variable(V1EnvVar(name="no_proxy", value=NO_PROXY))
        return self

    def enable_test_mode(self):
        """Indicates that this step is executed as part of a test flow run."""
        self.add_pod_label(name=TESTING_POD_LABEL, value='true')
        return self

    def add_authentication_env_variables(self):
        """Sets environment variables in this step, that are required for
        authZ/authN when communicating with internal APIs"""
        self.container \
            .add_env_variable(V1EnvVar(name="AUTH", value=AUTH)) \
            .add_env_variable(V1EnvVar(name="AUTH_TOKEN_URL", value=AUTH_TOKEN_URL)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_AUDIENCE", value=AUTH_DATAHANDLER_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_ID", value=AUTH_DATAHANDLER_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_SECRET", value=AUTH_DATAHANDLER_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_AUDIENCE", value=AUTH_METADATA_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_ID", value=AUTH_METADATA_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_SECRET", value=AUTH_METADATA_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_AUDIENCE", value=AUTH_SEARCH_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_ID", value=AUTH_SEARCH_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_SECRET", value=AUTH_SEARCH_CLIENT_CRED_SECRET))
        return self

    def add_input_and_output_labels(self, input_path: str, output_path: str):
        """Sets the pod labels for the input and output path of a step

        :param input_path: str
        :param output_path: str
        """
        self.add_pod_label(name=INPUT_POD_LABEL, value=sanitize_label(input_path)) \
            .add_pod_label(name=OUTPUT_POD_LABEL, value=sanitize_label(output_path))
        return self

    def add_kubernetes_resources_for_pod(self,
                                         cpu_request: str,
                                         memory_request: str,
                                         cpu_limit: str,
                                         memory_limit: str):
        """Sets the resource requests and limits for a step

        :param cpu_request: str
        :param memory_request: str
        :param cpu_limit: str
        :param memory_limit: str
        """
        self.container \
            .add_resource_request(CPU_RESOURCE_TITLE, cpu_request) \
            .add_resource_request(MEMORY_RESOURCE_TITLE, memory_request) \
            .add_resource_limit(CPU_RESOURCE_TITLE, cpu_limit) \
            .add_resource_limit(MEMORY_RESOURCE_TITLE, memory_limit)
        return self


def sanitize_label(label: str) -> str:
    """Truncate label to match kubernetes naming restrictions

    :param label: full label
    :return: truncated label
    """
    # max length is restricted to 63 characters
    label = label[:63]

    # label must not contain special_characters except of '-', '_' and '.'
    valid_label = re.sub(r"[^\w\-\.]", '_', label)

    # label must end with alphanumeric character
    while not re.search(r"[a-z0-9A-Z]$", valid_label):
        valid_label = valid_label[:-1]

    # label must start with alphanumerical character
    while not re.search(r"^[a-z0-9A-Z]", valid_label):
        valid_label = valid_label[1:]


    return valid_label


def extract_tag_from_docker_img(img: str):
    """Extracts the tag of the specified docker image string.

    :param img: name of the docker image, e.g. dockerhub.io/pg/postgres:latest
    :return: the tag of the image
    """
    tokens = img.split(':')
    return tokens[-1]


def extract_file_name_from_path(path: str) -> str:
    """Extracts file name from the specified path. The path can be a blob
    storage path, mapr cluster path or standard POSIX path

    :param path: path to extract file name from
    :return: file name as string
    """

    return str(Path(path).name)


def attach_blobfuse_volume(op,
                           mount_path,
                           mount_id,
                           storage_name,
                           container_name,
                           read_only=True,
                           file_cache_timeout=60,
                           mount_sub_path=None):
    local_obj_ref = k8s_client.V1LocalObjectReference(
        name=storage_name
    )

    flex_volume_obj = k8s_client.V1FlexVolumeSource(
        driver="azure/blobfuse",
        read_only=read_only,
        secret_ref=local_obj_ref,
        options={
            "container": container_name,
            "tmppath": "/mnt/blobfuse/{}".format(mount_id),
            "mountoptions":
                f"--file-cache-timeout-in-seconds={file_cache_timeout} "
                f"--use-https=true"
        }
    )

    v1_volume = k8s_client.V1Volume(
        name=mount_id,
        flex_volume=flex_volume_obj
    )

    v1_volume_mount = k8s_client.V1VolumeMount(
        mount_path=mount_path,
        sub_path=mount_sub_path,
        name=mount_id
    )

    op.add_volume(v1_volume)
    logger.debug(f"V1 volume (name={mount_id}, flex_volume={flex_volume_obj}, container_name:{container_name}) added.")
    op.add_volume_mount(v1_volume_mount)
    logger.debug(f"V1 volume mount (name={mount_id}, mount_path={v1_volume_mount}, sub_path={mount_sub_path}) added.")


def get_pvc_volume(volume_name, claim_name, read_only=False):
    return k8s_client.V1Volume(
        name=volume_name,
        persistent_volume_claim=V1PersistentVolumeClaimVolumeSource(
            claim_name=claim_name,
            read_only=read_only
        )
    )


def get_host_path_volume(volume_name, host_path, type='DirectoryOrCreate'):
    return k8s_client.V1Volume(
        name=volume_name,
        host_path=V1HostPathVolumeSource(
            path=host_path,
            type=type
        )
    )

def get_configmap_volume(volume_name, configmap_name):
    return k8s_client.V1Volume(
        name=volume_name,
        config_map=V1ConfigMapVolumeSource(
            name=configmap_name,
            optional=True)
    )