; Auto-generated. Do not edit!


(cl:in-package rosbag-msg)


;//! \htmlinclude RosbagRecorderState.msg.html

(cl:defclass <RosbagRecorderState> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (wall_time
    :reader wall_time
    :initarg :wall_time
    :type cl:real
    :initform 0)
   (recording_start_time
    :reader recording_start_time
    :initarg :recording_start_time
    :type cl:real
    :initform 0)
   (recording_duration
    :reader recording_duration
    :initarg :recording_duration
    :type cl:real
    :initform 0)
   (recording
    :reader recording
    :initarg :recording
    :type cl:boolean
    :initform cl:nil)
   (timeshift_enabled
    :reader timeshift_enabled
    :initarg :timeshift_enabled
    :type cl:boolean
    :initform cl:nil)
   (disk_capacity
    :reader disk_capacity
    :initarg :disk_capacity
    :type cl:integer
    :initform 0)
   (disk_available
    :reader disk_available
    :initarg :disk_available
    :type cl:integer
    :initform 0)
   (recorder_buffer_used
    :reader recorder_buffer_used
    :initarg :recorder_buffer_used
    :type cl:integer
    :initform 0)
   (recorder_buffer_capacity
    :reader recorder_buffer_capacity
    :initarg :recorder_buffer_capacity
    :type cl:integer
    :initform 0)
   (droppedMessagesSinceStart
    :reader droppedMessagesSinceStart
    :initarg :droppedMessagesSinceStart
    :type cl:integer
    :initform 0))
)

(cl:defclass RosbagRecorderState (<RosbagRecorderState>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RosbagRecorderState>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RosbagRecorderState)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name rosbag-msg:<RosbagRecorderState> is deprecated: use rosbag-msg:RosbagRecorderState instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:header-val is deprecated.  Use rosbag-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'wall_time-val :lambda-list '(m))
(cl:defmethod wall_time-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:wall_time-val is deprecated.  Use rosbag-msg:wall_time instead.")
  (wall_time m))

(cl:ensure-generic-function 'recording_start_time-val :lambda-list '(m))
(cl:defmethod recording_start_time-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:recording_start_time-val is deprecated.  Use rosbag-msg:recording_start_time instead.")
  (recording_start_time m))

(cl:ensure-generic-function 'recording_duration-val :lambda-list '(m))
(cl:defmethod recording_duration-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:recording_duration-val is deprecated.  Use rosbag-msg:recording_duration instead.")
  (recording_duration m))

(cl:ensure-generic-function 'recording-val :lambda-list '(m))
(cl:defmethod recording-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:recording-val is deprecated.  Use rosbag-msg:recording instead.")
  (recording m))

(cl:ensure-generic-function 'timeshift_enabled-val :lambda-list '(m))
(cl:defmethod timeshift_enabled-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:timeshift_enabled-val is deprecated.  Use rosbag-msg:timeshift_enabled instead.")
  (timeshift_enabled m))

(cl:ensure-generic-function 'disk_capacity-val :lambda-list '(m))
(cl:defmethod disk_capacity-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:disk_capacity-val is deprecated.  Use rosbag-msg:disk_capacity instead.")
  (disk_capacity m))

(cl:ensure-generic-function 'disk_available-val :lambda-list '(m))
(cl:defmethod disk_available-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:disk_available-val is deprecated.  Use rosbag-msg:disk_available instead.")
  (disk_available m))

(cl:ensure-generic-function 'recorder_buffer_used-val :lambda-list '(m))
(cl:defmethod recorder_buffer_used-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:recorder_buffer_used-val is deprecated.  Use rosbag-msg:recorder_buffer_used instead.")
  (recorder_buffer_used m))

(cl:ensure-generic-function 'recorder_buffer_capacity-val :lambda-list '(m))
(cl:defmethod recorder_buffer_capacity-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:recorder_buffer_capacity-val is deprecated.  Use rosbag-msg:recorder_buffer_capacity instead.")
  (recorder_buffer_capacity m))

(cl:ensure-generic-function 'droppedMessagesSinceStart-val :lambda-list '(m))
(cl:defmethod droppedMessagesSinceStart-val ((m <RosbagRecorderState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbag-msg:droppedMessagesSinceStart-val is deprecated.  Use rosbag-msg:droppedMessagesSinceStart instead.")
  (droppedMessagesSinceStart m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RosbagRecorderState>) ostream)
  "Serializes a message object of type '<RosbagRecorderState>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'wall_time)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'wall_time) (cl:floor (cl:slot-value msg 'wall_time)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'recording_start_time)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'recording_start_time) (cl:floor (cl:slot-value msg 'recording_start_time)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'recording_duration)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'recording_duration) (cl:floor (cl:slot-value msg 'recording_duration)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'recording) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'timeshift_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'disk_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'disk_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'disk_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'disk_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'disk_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'disk_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'disk_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'disk_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'disk_available)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'disk_available)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'disk_available)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'disk_available)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'disk_available)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'disk_available)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'disk_available)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'disk_available)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'recorder_buffer_used)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'recorder_buffer_used)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'recorder_buffer_used)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'recorder_buffer_used)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'recorder_buffer_used)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'recorder_buffer_used)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'recorder_buffer_used)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'recorder_buffer_used)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'recorder_buffer_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'recorder_buffer_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'recorder_buffer_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'recorder_buffer_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'recorder_buffer_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'recorder_buffer_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'recorder_buffer_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'recorder_buffer_capacity)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'droppedMessagesSinceStart)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'droppedMessagesSinceStart)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'droppedMessagesSinceStart)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'droppedMessagesSinceStart)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'droppedMessagesSinceStart)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'droppedMessagesSinceStart)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'droppedMessagesSinceStart)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'droppedMessagesSinceStart)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RosbagRecorderState>) istream)
  "Deserializes a message object of type '<RosbagRecorderState>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'wall_time) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'recording_start_time) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'recording_duration) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:setf (cl:slot-value msg 'recording) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'timeshift_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'disk_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'disk_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'disk_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'disk_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'disk_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'disk_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'disk_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'disk_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'disk_available)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'disk_available)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'disk_available)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'disk_available)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'disk_available)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'disk_available)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'disk_available)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'disk_available)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'recorder_buffer_used)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'recorder_buffer_used)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'recorder_buffer_used)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'recorder_buffer_used)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'recorder_buffer_used)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'recorder_buffer_used)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'recorder_buffer_used)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'recorder_buffer_used)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'recorder_buffer_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'recorder_buffer_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'recorder_buffer_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'recorder_buffer_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'recorder_buffer_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'recorder_buffer_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'recorder_buffer_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'recorder_buffer_capacity)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'droppedMessagesSinceStart)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'droppedMessagesSinceStart)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'droppedMessagesSinceStart)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'droppedMessagesSinceStart)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'droppedMessagesSinceStart)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'droppedMessagesSinceStart)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'droppedMessagesSinceStart)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'droppedMessagesSinceStart)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RosbagRecorderState>)))
  "Returns string type for a message object of type '<RosbagRecorderState>"
  "rosbag/RosbagRecorderState")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RosbagRecorderState)))
  "Returns string type for a message object of type 'RosbagRecorderState"
  "rosbag/RosbagRecorderState")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RosbagRecorderState>)))
  "Returns md5sum for a message object of type '<RosbagRecorderState>"
  "d5e30b7ec94cbb4dcb4898b09b7dc723")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RosbagRecorderState)))
  "Returns md5sum for a message object of type 'RosbagRecorderState"
  "d5e30b7ec94cbb4dcb4898b09b7dc723")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RosbagRecorderState>)))
  "Returns full string definition for message of type '<RosbagRecorderState>"
  (cl:format cl:nil "Header header~%time wall_time~%~%time recording_start_time~%duration recording_duration~%~%bool recording~%bool timeshift_enabled~%~%uint64 disk_capacity~%uint64 disk_available~%~%# the same buffer is used in timeshift mode as in normal recording mode~%uint64 recorder_buffer_used~%uint64 recorder_buffer_capacity ~%~%uint64 droppedMessagesSinceStart~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RosbagRecorderState)))
  "Returns full string definition for message of type 'RosbagRecorderState"
  (cl:format cl:nil "Header header~%time wall_time~%~%time recording_start_time~%duration recording_duration~%~%bool recording~%bool timeshift_enabled~%~%uint64 disk_capacity~%uint64 disk_available~%~%# the same buffer is used in timeshift mode as in normal recording mode~%uint64 recorder_buffer_used~%uint64 recorder_buffer_capacity ~%~%uint64 droppedMessagesSinceStart~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RosbagRecorderState>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     8
     8
     8
     1
     1
     8
     8
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RosbagRecorderState>))
  "Converts a ROS message object to a list"
  (cl:list 'RosbagRecorderState
    (cl:cons ':header (header msg))
    (cl:cons ':wall_time (wall_time msg))
    (cl:cons ':recording_start_time (recording_start_time msg))
    (cl:cons ':recording_duration (recording_duration msg))
    (cl:cons ':recording (recording msg))
    (cl:cons ':timeshift_enabled (timeshift_enabled msg))
    (cl:cons ':disk_capacity (disk_capacity msg))
    (cl:cons ':disk_available (disk_available msg))
    (cl:cons ':recorder_buffer_used (recorder_buffer_used msg))
    (cl:cons ':recorder_buffer_capacity (recorder_buffer_capacity msg))
    (cl:cons ':droppedMessagesSinceStart (droppedMessagesSinceStart msg))
))
