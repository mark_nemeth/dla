"""Regressiontests for bagfile routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import random as rd
import unittest
from urllib.parse import urlencode

import requests
from DANFUtils.utils import generate_deterministic_guid

from tests.regressiontests.testutils import get_testdb_connector


# testdata
# can change the localhost into external ip:
BAGFILE_GET_FAILED = 'Bagfile GET failed'
EVENTS_POST_FAILED = 'Bagfile state events POST failed'
INVALID_GUID_NOT_DETECTED = 'Invalid GUID not detected'
base_url = 'http://localhost:8887'
bagfile_url = base_url + '/bagfile/'
sequence_url = base_url + '/sequence/'
state_url_template = bagfile_url + '{}/state'
custom_attributes_url_template = bagfile_url + '{}/custom_attributes'

https_url = 'https://localhost:8443/bagfile/'

rd.seed("test")

raw_data_bagfile = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "link": "testdata",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1",
    "file_name": "test.bag",
    "file_type": "ORIGINAL",
    "sequences": [generate_deterministic_guid(), generate_deterministic_guid()],
    "childbagfiles": [generate_deterministic_guid(), generate_deterministic_guid()],
    "tags": ["tag", "tag2"],
    "processing_priority": 1
}
raw_data_sequence = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "bagfile_guid": raw_data_bagfile['guid'],
    "start": 1475272800000,
    "end": 1475272800000,
    "type": "testdata",
    "index": 1,
    "extractor": "ext1"
}

custom_attributes = {
    "vehicle_type": "S101",
    "weather_type": "sunny"
}

valid_state_event = {
    "name": "splitter",
    "version": "v1",
    "time": 1475272800000,
    "status": "SUCCESS",
    "report": {
        "message": "All good"
    }

}
invalid_state_event = {"name": "splitter"}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
        con.db['sequences'].delete_many({'type': raw_data_sequence['type']})


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['bagfiles'].delete_many({'link': raw_data_bagfile['link']})
        con.db['sequences'].delete_many({'type': raw_data_sequence['type']})


class TestBagfilePOST(unittest.TestCase):
    def test_bagfile_POST(self):
        prepare_db()
        data = raw_data_bagfile.copy()
        del data['guid']
        res = requests.post(url=bagfile_url, json=data)
        self.assertEqual(201, res.status_code, 'Bagfile post failed')
        self.assertRegex(
            res.headers['Location'],
            '/bagfile/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')
        cleanup_db()

    def test_bagfile_POST_missing_attributes(self):
        prepare_db()
        data = raw_data_bagfile.copy()
        del data['guid']
        del data['link']
        del data['version']

        res = requests.post(url=bagfile_url, json=data)
        self.assertEqual(400, res.status_code,
                         'Missing attributes not detected')
        cleanup_db()


class TestBagfileGET(unittest.TestCase):
    def test_bagfile_GET(self):
        prepare_db()
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        data = raw_data_bagfile.copy()
        del data["sequences"]
        del data["childbagfiles"]
        res = requests.get(url=bagfile_url + raw_data_bagfile['guid'])
        self.assertDictEqual(data,
                             res.json()['results'][0], 'Get by guid failed!')
        cleanup_db()

    def test_bagfile_GET_invalid(self):
        prepare_db()
        res = requests.get(url=bagfile_url +
                               '12341234-1234-1234-1234-123412341235')
        self.assertEqual(404, res.status_code, '%s' % INVALID_GUID_NOT_DETECTED)
        cleanup_db()

    def test_bagfile_GET_and_query_with_multiple_whitelist(self):
        prepare_db()
        whitelist = ["sequences", "childbagfiles"]
        whitelist_query = "?" + urlencode([("whitelist", x) for x in whitelist])
        connector = get_testdb_connector()
        raw_bag = raw_data_bagfile.copy()

        with connector as con:
            con.add_bagfiles([raw_bag])

        res = requests.get(url=bagfile_url + raw_bag['guid'] + whitelist_query)
        self.assertCountEqual(raw_bag, res.json()['results'][0],
                              'Get bagfile --> Whitelisting with multiple attributes didnt work!')
        cleanup_db()

    def test_bagfile_GET_and_query_with_single_whitelist(self):
        prepare_db()
        whitelist = ["sequences"]
        whitelist_query = "?" + urlencode([("whitelist", x) for x in whitelist])

        connector = get_testdb_connector()
        raw_bag = raw_data_bagfile.copy()

        with connector as con:
            con.add_bagfiles([raw_bag])
        del raw_bag['childbagfiles']
        res = requests.get(url=bagfile_url + raw_bag['guid'] + whitelist_query)
        self.assertCountEqual(raw_bag,
                              res.json()['results'][0],
                              'Get bagfile --> Whitelisting with single attribute didnt work!')
        cleanup_db()

    def test_bagfile_GET_and_query_with_invalid_whitelist(self):
        prepare_db()
        whitelist = ["invalid_tag", "inv", "123"]
        whitelist_query = "?" + urlencode([("whitelist", x) for x in whitelist])

        connector = get_testdb_connector()
        raw_bag = raw_data_bagfile.copy()

        with connector as con:
            con.add_bagfiles([raw_bag])
        del raw_bag['childbagfiles']
        del raw_bag['sequences']
        res = requests.get(url=bagfile_url + raw_bag['guid'] + whitelist_query)
        self.assertCountEqual(raw_bag,
                              res.json()['results'][0], 'Get bagfile --> wrong query leads to wrong received bagfile!')
        cleanup_db()


class TestBagfilesPATCH(unittest.TestCase):
    def test_bagfile_PATCH(self):
        prepare_db()
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        data = raw_data_bagfile.copy()
        del data['guid']
        data['vehicle_id_num'] = "V-9999-999"
        res = requests.patch(url=bagfile_url + raw_data_bagfile['guid'],
                             json=data)
        self.assertEqual(200, res.status_code, 'Bagfile patch failed')
        cleanup_db()

    def test_bagfile_PATCH_unknown_guid_should_return_404(self):
        prepare_db()
        data = raw_data_bagfile.copy()
        del data['guid']
        data['vehicle_id_num'] = "V-9999-999"

        res = requests.patch(url=bagfile_url +
                                 '12341234-1234-1234-1234-123412341235',
                             json=data)
        self.assertEqual(404, res.status_code, INVALID_GUID_NOT_DETECTED)
        cleanup_db()


class TestBagfileDELETE(unittest.TestCase):
    def test_bagfile_DELETE(self):
        prepare_db()
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
            con.add_sequences([raw_data_sequence])
        res = requests.delete(url=bagfile_url + raw_data_bagfile['guid'])
        self.assertEqual(200, res.status_code, 'Bagfile delete failed')
        res = requests.get(url=sequence_url + raw_data_sequence['guid'])
        self.assertEqual(404, res.status_code, 'Cascade Sequence not deleted')
        cleanup_db()

    def test_bagfile_GET_unknown_guid_should_return_404(self):
        prepare_db()
        res = requests.delete(url=bagfile_url +
                                  '12341234-1234-1234-1234-123412341235')
        self.assertEqual(404, res.status_code, 'Invalid GUID not detected')
        cleanup_db()


class TestBagfileOPTIONS(unittest.TestCase):
    def test_bagfile_OPTIONS(self):
        prepare_db()
        res = requests.options(url=bagfile_url)
        self.assertEqual(200, res.status_code,
                         'Fetching Bagfile route options failed')
        expected_result = 'OPTIONS, POST'
        self.assertEqual(expected_result,
                         res.headers['Access-Control-Allow-Methods'],
                         'Fetching Bagfile route options failed')
        cleanup_db()


class TestBagfilesStateEventsPOST(unittest.TestCase):
    def setUp(self):
        prepare_db()
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        self.guid = raw_data_bagfile['guid']

    def tearDown(self):
        cleanup_db()

    def test_POST_state_when_no_events_should_create_array_with_new_event(
            self):
        res_post = requests.post(url=state_url_template.format(self.guid),
                                 json=valid_state_event)
        self.assertEqual(200, res_post.status_code,
                         '%s' % EVENTS_POST_FAILED)

        res_get = requests.get(url=bagfile_url + self.guid)
        self.assertEqual(200, res_get.status_code, '%s' % BAGFILE_GET_FAILED)
        res_bagfile = res_get.json()['results'][0]

        self.assertEqual(1, len(res_bagfile['state']),
                         'Wrong number of events')

    def test_POST_state_when_existing_events_should_append_new_event(self):
        # add first event
        res_post = requests.post(url=state_url_template.format(self.guid),
                                 json=valid_state_event)
        self.assertEqual(200, res_post.status_code,
                         EVENTS_POST_FAILED)

        # add second event
        res_post = requests.post(url=state_url_template.format(self.guid),
                                 json=valid_state_event)
        self.assertEqual(200, res_post.status_code,
                         EVENTS_POST_FAILED)

        res_get = requests.get(url=bagfile_url + self.guid)
        self.assertEqual(200, res_get.status_code, BAGFILE_GET_FAILED)
        res_bagfile = res_get.json()['results'][0]

        self.assertEqual(2, len(res_bagfile['state']),
                         'Wrong number of events')

    def test_POST_state_when_invalid_body_should_return_400(self):
        res = requests.post(url=state_url_template.format(self.guid),
                            json=invalid_state_event)
        self.assertEqual(400, res.status_code, 'Invalid body not detected')

    def test_POST_state_when_unknown_bagfile_guid_should_return_404(self):
        res = requests.post(url=state_url_template.format(
            "00000000-0000-0000-0000-000000000000"),
            json=valid_state_event)
        self.assertEqual(404, res.status_code, 'Missing guid not detected')


class TestBagfilesCustomAttributesPOST(unittest.TestCase):
    def setUp(self):
        prepare_db()
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        self.guid = raw_data_bagfile['guid']

    def tearDown(self):
        cleanup_db()

    @unittest.skip("test-case is broken")
    def test_custom_attributes_PATCH(self):
        res = requests.patch(url=custom_attributes_url_template.format(self.guid),
                             json=custom_attributes)
        self.assertEqual(200, res.status_code, 'Bagfile custom attributes post failed')
        res = requests.get(url=bagfile_url + self.guid)
        self.assertEqual(200, res.status_code, 'Bagfile GET failed')
        data = raw_data_bagfile.copy()
        del data["sequences"]
        del data["childbagfiles"]
        data['custom_attributes'] = {"common_values": custom_attributes}
        self.assertDictEqual(data,
                             res.json()['results'][0],
                             'Bagfile custom attributes post failed')


if __name__ == '__main__':
    unittest.main()
