import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from "../../../Utils/ScrollList/ScrollList";
import './ByEventFilter.scss'

export function ByEventFilter(props) {

  function onEventSelect(event) {
    const value = props.eventList.get(event);
    if (props.selectedEventList.includes(value)) props.filterByEvents(props.selectedEventList.filter(t => t !== value));
    else props.filterByEvents(props.selectedEventList.concat(value));
  }

  function isEventSelected(item) {
    const eventValue = props.eventList.get(item);
    return props.selectedEventList.includes(eventValue);
  }

  function resetEventSelection() {
    props.filterByEvents([]);
  }

  const showLoading = props.eventList === undefined || props.eventList.length <= 0;
  const subLabel = props.selectedEventList.length + ' selected';
  const active = props.selectedEventList.length > 0;

  const listItemComponent = (item) => (
    <label className="event-list-item">
      <input type="checkbox" value={item}
             onChange={event => onEventSelect(event.target.value)}
             checked={isEventSelected(item)}/>
      &nbsp; {item}
    </label>
  );

  return (
      <CollapseContainer label={active > 0 ? "Event: " + subLabel : "Event"}
                         active={active}
                         variant="list"
                         resetCallback={resetEventSelection}
      >
      <ScrollList
        showLoading={showLoading}
        itemList={Array.from(props.eventList.keys())}
        listItemComponent={listItemComponent}
      />
    </CollapseContainer>
  );
}