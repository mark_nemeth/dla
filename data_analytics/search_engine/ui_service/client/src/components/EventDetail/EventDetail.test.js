import React from 'react';
import {shallow} from 'enzyme/build';
import EventDetail from './EventDetail';

describe('EventDetail tab component', () => {
  const props = {
    events: {
      annotation: {
        title: "Annotation",
        image: [],
        is_critical_error: false,
        annotation_event_type: "Test",
        annotation_event_timestamp: "1574853147000",
        ts: 1574853147000
      },
      disengagement: {
        title: "Disengagement",
        image: [],
        is_critical_error: true,
        disengagement_event_type: "Crash",
        disengagement_event_timestamp: "1535653147000",
        ts: 1535653147000
      }
    }
  };

  it('should renders and show all elements and two entries in the list', () => {
    const wrapper = shallow(<EventDetail {...props}/>);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('ScrollList').exists()).toBe(true);
    expect(wrapper.find('ScrollList').shallow().find('tr')).toHaveLength(2);
    expect(wrapper.find('.icon-arrow-right-1').exists()).toBe(true);
    expect(wrapper.find('ReactAce').exists()).toBe(true);
  });

  it('should renders an empty placeholder text', () => {
    const wrapper = shallow(<EventDetail {...undefined}/>);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('.event-detail-empty-container').exists()).toBe(true);
    expect(wrapper.find('.event-detail-empty-placeholder').exists()).toBe(true);
  });

  it('should change input when list item is selected', () => {
    const wrapper = shallow(<EventDetail {...props}/>);
    const currentEventDetailContent = wrapper.find('ReactAce').props().value;
    expect(currentEventDetailContent).toBe(JSON.stringify(props.events.annotation, null, 2));

    const secondListEntry = wrapper.find('ScrollList').shallow().find('tr').at(1).find('FormCheck');
    secondListEntry.simulate('change', { target: { checked: true } });
    const newEventDetailContent = wrapper.find('ReactAce').props().value;
    expect(currentEventDetailContent).not.toBe(newEventDetailContent);
  });

});