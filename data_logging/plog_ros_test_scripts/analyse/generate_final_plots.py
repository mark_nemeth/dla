#!/usr/bin/env python
# This script analyses iostat output.



import re
import os
import sys
import getopt
import matplotlib.pyplot as plt
import pickle
list_d = []
time =[]
tstart=0;
inputfile = ''
crop_end= False
crop_start= False
dimension = 'MB/s'
device="md127"
value= "wMB/s"
export = False

try:
	    options, remainder = getopt.getopt(
		sys.argv[1:],
		'i:s:e:mMgGd:v:p',
		['input=',
		 'starttime=',
		 'endtime='
		 ])
except getopt.GetoptError as err:
	    print('ERROR:', err)
	    sys.exit(1)



for opt, arg in options:
	    if opt in ('-i', '--input'):
		inputfile = arg
	    elif opt in ('-s', '--starttime'):
		tstart = int(arg)
		crop_start = True
	    elif opt in ('-e', '--endtime'):
		tend = int(arg)
		crop_end= True
	    elif opt == "-m":
            	dimension = 'Mb/s'
	    elif opt == "-M":
            	dimension = 'MB/s'
	    elif opt == "-g":
            	dimension = 'Gb/s'
	    elif opt == "-G":
            	dimension = 'GB/s'
	    elif opt == "-d":
		device = arg
		print device
	    elif opt == "-v":
		value=arg
		print value
	    elif opt == "-p":
 		export= True




time_path=os.getcwd()+"/"+"time"+".list"
iostat_path=os.getcwd()+"/"+"iostat"+".list"
eth_log_1_path=os.getcwd()+"/"+"eth_log_1"+".list"
eth_log_2_path=os.getcwd()+"/"+"eth_log_2"+".list"
time=pickle.load(open(time_path, 'rb'))
iostat=pickle.load(open(iostat_path, 'rb'))
eth_log_1=pickle.load(open(eth_log_1_path, 'rb'))
eth_log_2=pickle.load(open(eth_log_2_path, 'rb'))

#plot the output
fig = plt.figure()
ax = fig.add_subplot(111)
line1,  = ax.plot(time,iostat, label="Iostat output")
line2, =  ax.plot(eth_log_1,label="eth_log_1")
line3, =  ax.plot(eth_log_2,label="eth_log_2")
fig.suptitle('Combined output', fontsize=14, fontweight='bold')
plt.xlabel('time in s')
plt.xlim(0,time[-1])
plt.ylabel(dimension)
plt.legend(loc='best', fontsize=12)
if export== False:
	plt.show()
if export == True:
	fig.savefig(os.getcwd()+"/combined.png", bbox_inches='tight')

