import BaseApi from './BaseApi';
import configHandlerSingleton from '../configHandler';
import {isObjectValid} from '../utils/helperFunctions';

export default class SearchApi {

  static async getBagfilesFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getBagfileAPIConfig();
      config.headers = authHeader;
      config.qs = {
        ...this.getNewestElements(req.query)
      };

      return BaseApi.doRequest(config);
    });
  }

  static getFilterFeaturesFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getFilterFeaturesAPIConfig();
      config.headers = authHeader;
      config.qs = {
        ...this.getNewestElements(req.query)
      };
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static getChildBagfilesFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getChildBagfileAPIConfig();
      config.headers = authHeader;
      config.qs = {
        ...this.getNewestElements(req.query)
      };
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static getDrivesFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getDriveAPIConfig();
      config.headers = authHeader;
      config.qs = {
        ...this.getNewestElements(req.query)
      };
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static getFiltersFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getFiltersAPIConfig();
      config.headers = authHeader;
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static getChildBagfileFiltersFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getChildBagfileFiltersAPIConfig();
      config.headers = authHeader;
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static getDriveFiltersFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getDriveFiltersAPIConfig();
      config.headers = authHeader;
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static async getReportingKPIsFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getReportingKPIConfig();
      config.headers = authHeader;
      config.qs = req.query;
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static async getReportingTablesSimpleFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getReportingTablesSimpleConfig();
      config.headers = authHeader;
      config.qs = req.query;
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static async getReportingTablesAggregationFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getReportingTablesAggregationConfig();
      config.headers = authHeader;
      config.qs = req.query;
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  static getFlowRunsFromAPI(req) {
    return BaseApi.getAuthHeader(req).then((authHeader) => {
      const config = configHandlerSingleton.getFlowrunAPIConfig();
      config.headers = authHeader;
      config.qs = req.query;
      try {
        return BaseApi.doRequest(config);
      } catch (error) {
        throw error;
      }
    });
  }

  // Sort result for newest bagfiles if user has not entered another sort criteria
  static getNewestElements(query) {
    if (!isObjectValid(query)) return query;
    if (query.hasOwnProperty('sortby')) return query;
    query.sortby = '-start';
    return query;
  }
}