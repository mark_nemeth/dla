# DLA Repository
This is the repository for DLA team.


### About this repository
This repo is separate from Athena, therefore anything pushed here is primarily used for DLA internal purposes. Nevertheless, the repository access is not limited to DLA team members.
- How should we use it?
This is used to to share your code within DLA or to work on your code while being able to track changes.
Typical use scenario:
- You have an idea to analyze the data, so you first create a new branch using the BitBucket web interface.
    You can start working on that branch locally by:
    ```shell
    cd ~/workspace/dla_repo
    git checkout master
    git pull
    git checkout [name-of-new-branch]
    ```
- At some point, when the code is more mature, you might want to share it with others; for this you'll need to create a PR within BitBucket, so that it can be merged to master.
 Since this is NOT the Athena repo, we should expect faster and more "tolerant" code reviews (see more below about [PR conduct](#pr-conduct)) .


## Code Style
There are no strict code guidelines for this repo. That being said, complying to PEP8 guidelines helps readability and is generally a good dev skill to have; not to mention that PEP8 compliance is now enforced for Athena. If you want to check that your code if formatted according to PEP8 guidelines, there are many tools you can use. The simplest is [flake8](http://flake8.pycqa.org/en/latest/), a lightweight python package used fore code style guide.
From the shell, install it:
```shell
python -m pip install flake8
```
Run it on a new python script:
```
flake8 script.py
```

## PR conduct
This is a development repo, so expect faster/shallower code reviews when comparing it to a PR in Athena. However, as code reviews are useful to find bugs and have other developers suggest better ways to solve problems, more thorough reviews might be desired at times.
hdre is a suggested guideline for creating/reviewing PRs:
- Using the development branch you created before, create a PR from the BitBucket UI.
- Add all reviewers you think might have an interest in what your code does.
- Name a "Main Reviewer" (eg: "Main reviewer: @sonHeung").
- (optional) Specify a criticality "level", as:
  -- level 0: fast review (eg, this is to let other people know about your recent code (also "FYI"), but you don't expect reviewers to spend much time on it.
  -- level 1: medium
  -- level 2: thorough review (suggestions are welcomed)
- If you are a reviewer, please review in a timely fashion.
- For any PR that is level 2, please don't approve without checking/testing the code.

## What to commit
To keep the size of the repository as small as possible, please take care of what you commit. Please doe not commit binaries, measurement files (e.g. .bag) or large (serialized) data sets which are the result of your analysis scripts.


## Use this repo
Before cloning this repo, it is highly encouraged that you build Athena following the instructions detailed [here](https://athena.daimler.com/bitbucket/projects/ATROB/repos/athena/browse/README.md?useDefaultHandler=true). While this is not strictly necessary, several steps in that tutorial (eg installing ROS, getting certificates and so on) are required to have full access to resources.

Clone this repository on the same level as the athena one.
Eg, for most setups:
```shell
$ cd ~/workspace
$ git clone https://[YOUR_BITBUCKET_ID]@athena.daimler.com/bitbucket/scm/attdata/dla_repo.git
```

## Repo's structure
```
dla_repo
│   README.md
│
└───data_analytics
│   │
│   └───common
│   │   │   ...
│   │
│   └───tools
│   │   └───analyzePassFail
│   │   │   ...
│   │
│   └───team
│       └───ashmele
│       └───cosents
│       └───kolbfab
│       └───rbaldau
│       └───tanwang
│       └───vanjoh
│       └───walzchr
│       │   ...
│
└───data_logging
│   └───ros_test
│   │   ...
│
└───data_engineering
        │   ...
```

