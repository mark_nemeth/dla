"""Main Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import logging

from config.config_handler import config_dict
from handlers.bagfile_event_processor import get_cloud_event_processor
from handlers.utils import utils
from proxies.storage_queue import EventListener
from model.models import NewBagfileEvent

logger = logging.getLogger(__name__)

NEW_BLOB_EVENT_TYPE = 'Microsoft.Storage.BlobCreated'


class StorageQueueIngestor:
    """The main handler aims to automate the process of input data"""

    def __init__(self):
        self.queue_name = config_dict.get_value("queue_name")
        self.event_agent = EventListener(self.queue_name)
        self.bagfile_event_processor = get_cloud_event_processor()
        self.queue_message_timeout = int(config_dict.get_value('queue_message_timeout'))

    def process_new_files(self):
        """Fetch new events from the receiver and analyse the data
        :return:
            trigger the some functions
        Attention:
            Better to set the timeout >= 3*(the DEFAULT_PERIODIC_FUNCTION_DELAY_IN_SEC)
        """
        message = self._get_next_new_file_message()
        if not message:
            # no (valid) message available. nothing to do for now
            return

        # check for required attributes
        file_name = message.get("file_name")
        container_name = message.get("container")
        storage_account = message.get("storage_account")
        file_path = message.get("file_path")

        if (not file_name) or (not container_name) or (not storage_account):
            logger.error(f"input values "
                         f"[file_name, container_name, storage_account] "
                         f"are not available! payload: {message}")
            return

        # Make sure that we ingested from right storage accounts:
        if not utils.validate_sa(storage_account):
            logger.info(f"The input storage account {storage_account} is invalid")
            self.event_agent.delete_event(
                message.get('id'),
                message.get('pop_receipt'))
            return

        event = NewBagfileEvent(file_name=file_name,
                                file_path=file_path,
                                container_name=container_name,
                                storage_account=storage_account,
                                msg_id=message.get('id'),
                                msg_pop_receipt=message.get('pop_receipt'))

        self.bagfile_event_processor.process_new_file_event(event)

    def _get_next_new_file_message(self):
        messages = self.event_agent.fetch_events(timeout=self.queue_message_timeout)
        if not messages:
            logger.info("No events has been found, waiting for next session to try again")
            return None

        message = messages[0]

        # parse event
        payload = self._parse_queue_msg(message)
        if payload is None:
            logger.error(f"failed to parse msg from message queue! message: {message}")
            return None

        # validate event type
        event_type = payload.get("event_type")
        if event_type != NEW_BLOB_EVENT_TYPE:
            logger.warning(f"read message with unexpected event type {event_type}: {message}")
            if self.event_agent.delete_event(message.id, message.pop_receipt):
                logger.info(f"successfully deleted message with unexpected event type: {message}")
                return None

        return payload

    def _parse_queue_msg(self, message):
        try:
            json_content = json.loads(message.content)
            subjects = json_content['subject'].split('/')
            topic = json_content['topic'].split('/')
        except Exception:
            logger.exception(f"fail to parse the content in event message: {message}")
            return None

        if len(subjects) < 5 or subjects[3] != "containers":
            logger.info('The parameters are insufficient: {}'.format(json_content.get('subject')))
            return None

        storage_account = topic[-1]
        container_name = subjects[4]
        file_name = subjects[-1]
        file_path = json_content['topic'] + json_content['subject']
        event_time = json_content.get("eventTime", "")
        event_type = json_content['eventType']
        logger.info(f"the pop_receipt is: {message.pop_receipt}, and the event id is: {message.id}")

        return {
            "container": container_name,
            "file_name": file_name,
            "file_path": file_path,
            "event_time": event_time,
            "storage_account": storage_account,
            "event_type": event_type,
            "pop_receipt": message.pop_receipt,
            "id": message.id
        }
