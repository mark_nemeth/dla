__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


import unittest
from unittest import mock

from kubernetes.client import V1PodList, V1Pod, V1PodStatus

from kubegc.removers import PodRemover, WorkflowRemover


class TestKubernetesGarbageCollector(unittest.TestCase):
    NAMESPACE = "test_namespace"

    @mock.patch('kubegc.removers.PodRemover._init_kube_api')
    def test_should_not_delete_pods_when_under_threshold(self, _):
        podremover = PodRemover(self.NAMESPACE, 2)

        podremover.v1 = mock.Mock()
        podremover.v1.list_namespaced_pod.return_value = self._getPodList()
        podremover.remove_pods()

        podremover.v1.delete_collection_namespaced_pod.assert_not_called()

    @mock.patch('kubegc.removers.PodRemover._init_kube_api')
    def test_should_delete_pods_when_over_threshold(self, _):
        podremover = PodRemover(self.NAMESPACE, 0)

        podremover.v1 = mock.Mock()
        podremover.v1.list_namespaced_pod.return_value = self._getPodList()

        podremover.remove_pods()

        podremover.v1.delete_collection_namespaced_pod.assert_called_once()

    @mock.patch('kubegc.removers.WorkflowRemover._init_kube_extensions_api')
    def test_should_not_delete_worflows_when_under_threshold(self, _):
        workflowremover = WorkflowRemover(self.NAMESPACE, 2)

        workflowremover.custom_objects_api = mock.Mock()
        workflowremover.custom_objects_api.list_namespaced_custom_object.return_value = self._getWorkflowDict()

        workflowremover.remove_workflows()

        workflowremover.custom_objects_api.delete_namespaced_custom_object.assert_not_called()

    @mock.patch('kubegc.removers.WorkflowRemover._init_kube_extensions_api')
    def test_should_delete_worflows_when_over_threshold(self, _):
        workflowremover = WorkflowRemover(self.NAMESPACE, 0)

        workflowremover.custom_objects_api = mock.Mock()
        workflowremover.custom_objects_api.list_namespaced_custom_object.return_value = self._getWorkflowDict()

        workflowremover.remove_workflows()

        workflowremover.custom_objects_api.delete_namespaced_custom_object.assert_called_once()

    @mock.patch('kubegc.removers.WorkflowRemover._init_kube_extensions_api')
    def test_should_ignore_worflows_when_not_succeeded(self, _):
        workflowremover = WorkflowRemover(self.NAMESPACE, 0)

        workflowremover.custom_objects_api = mock.Mock()
        workflowremover.custom_objects_api.list_namespaced_custom_object.return_value = self._getRunning_WorkflowDict()

        workflowremover.remove_workflows()

        workflowremover.custom_objects_api.delete_namespaced_custom_object.assert_not_called()

    @mock.patch('kubegc.removers.WorkflowRemover._init_kube_extensions_api')
    def test_should_delete_workflows_if_failed_and_older_than_one_week(self, _):
        workflowremover = WorkflowRemover(self.NAMESPACE, 0)

        workflowremover.custom_objects_api = mock.Mock()
        workflowremover.custom_objects_api.list_namespaced_custom_object.return_value = self._getWorkflowDict(
            failed=True)

        workflowremover.remove_workflows()

        workflowremover.custom_objects_api.delete_namespaced_custom_object.assert_called_once()

    def _getPodList(self):
        pod_status = V1PodStatus(phase='Succeeded')
        pod = V1Pod(status=pod_status)
        podlist = V1PodList(items=[pod])
        return podlist

    def _getWorkflowDict(self, failed=False):
        phase = 'Error' if failed else 'Succeeded'
        return {'items': [
                     {
                         'status': {'finishedAt': '2019-07-08T13:16:25Z', 'phase': phase, 'startedAt': '2019-07-08T13:14:27Z'},
                         'metadata': {'name': 'testworkflow'}
                     }]}

    def _getRunning_WorkflowDict(self):
        return {'items': [
            {
                'status': {'finishedAt': '2019-07-08T13:16:25Z', 'phase': "Running", 'startedAt': '2019-07-08T13:14:27Z'},
                'metadata': {'name': 'testworkflow'}
            }]}
