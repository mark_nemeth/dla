;; Auto-generated. Do not edit!


(when (boundp 'rosbag::RosbagRecorderState)
  (if (not (find-package "ROSBAG"))
    (make-package "ROSBAG"))
  (shadow 'RosbagRecorderState (find-package "ROSBAG")))
(unless (find-package "ROSBAG::ROSBAGRECORDERSTATE")
  (make-package "ROSBAG::ROSBAGRECORDERSTATE"))

(in-package "ROS")
;;//! \htmlinclude RosbagRecorderState.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass rosbag::RosbagRecorderState
  :super ros::object
  :slots (_header _wall_time _recording_start_time _recording_duration _recording _timeshift_enabled _disk_capacity _disk_available _recorder_buffer_used _recorder_buffer_capacity _droppedMessagesSinceStart ))

(defmethod rosbag::RosbagRecorderState
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:wall_time __wall_time) (instance ros::time :init))
    ((:recording_start_time __recording_start_time) (instance ros::time :init))
    ((:recording_duration __recording_duration) (instance ros::time :init))
    ((:recording __recording) nil)
    ((:timeshift_enabled __timeshift_enabled) nil)
    ((:disk_capacity __disk_capacity) 0)
    ((:disk_available __disk_available) 0)
    ((:recorder_buffer_used __recorder_buffer_used) 0)
    ((:recorder_buffer_capacity __recorder_buffer_capacity) 0)
    ((:droppedMessagesSinceStart __droppedMessagesSinceStart) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _wall_time __wall_time)
   (setq _recording_start_time __recording_start_time)
   (setq _recording_duration __recording_duration)
   (setq _recording __recording)
   (setq _timeshift_enabled __timeshift_enabled)
   (setq _disk_capacity (round __disk_capacity))
   (setq _disk_available (round __disk_available))
   (setq _recorder_buffer_used (round __recorder_buffer_used))
   (setq _recorder_buffer_capacity (round __recorder_buffer_capacity))
   (setq _droppedMessagesSinceStart (round __droppedMessagesSinceStart))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:wall_time
   (&optional __wall_time)
   (if __wall_time (setq _wall_time __wall_time)) _wall_time)
  (:recording_start_time
   (&optional __recording_start_time)
   (if __recording_start_time (setq _recording_start_time __recording_start_time)) _recording_start_time)
  (:recording_duration
   (&optional __recording_duration)
   (if __recording_duration (setq _recording_duration __recording_duration)) _recording_duration)
  (:recording
   (&optional __recording)
   (if __recording (setq _recording __recording)) _recording)
  (:timeshift_enabled
   (&optional __timeshift_enabled)
   (if __timeshift_enabled (setq _timeshift_enabled __timeshift_enabled)) _timeshift_enabled)
  (:disk_capacity
   (&optional __disk_capacity)
   (if __disk_capacity (setq _disk_capacity __disk_capacity)) _disk_capacity)
  (:disk_available
   (&optional __disk_available)
   (if __disk_available (setq _disk_available __disk_available)) _disk_available)
  (:recorder_buffer_used
   (&optional __recorder_buffer_used)
   (if __recorder_buffer_used (setq _recorder_buffer_used __recorder_buffer_used)) _recorder_buffer_used)
  (:recorder_buffer_capacity
   (&optional __recorder_buffer_capacity)
   (if __recorder_buffer_capacity (setq _recorder_buffer_capacity __recorder_buffer_capacity)) _recorder_buffer_capacity)
  (:droppedMessagesSinceStart
   (&optional __droppedMessagesSinceStart)
   (if __droppedMessagesSinceStart (setq _droppedMessagesSinceStart __droppedMessagesSinceStart)) _droppedMessagesSinceStart)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; time _wall_time
    8
    ;; time _recording_start_time
    8
    ;; duration _recording_duration
    8
    ;; bool _recording
    1
    ;; bool _timeshift_enabled
    1
    ;; uint64 _disk_capacity
    8
    ;; uint64 _disk_available
    8
    ;; uint64 _recorder_buffer_used
    8
    ;; uint64 _recorder_buffer_capacity
    8
    ;; uint64 _droppedMessagesSinceStart
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; time _wall_time
       (write-long (send _wall_time :sec) s) (write-long (send _wall_time :nsec) s)
     ;; time _recording_start_time
       (write-long (send _recording_start_time :sec) s) (write-long (send _recording_start_time :nsec) s)
     ;; duration _recording_duration
       (write-long (send _recording_duration :sec) s) (write-long (send _recording_duration :nsec) s)
     ;; bool _recording
       (if _recording (write-byte -1 s) (write-byte 0 s))
     ;; bool _timeshift_enabled
       (if _timeshift_enabled (write-byte -1 s) (write-byte 0 s))
     ;; uint64 _disk_capacity
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _disk_capacity (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _disk_capacity) (= (length (_disk_capacity . bv)) 2)) ;; bignum
              (write-long (ash (elt (_disk_capacity . bv) 0) 0) s)
              (write-long (ash (elt (_disk_capacity . bv) 1) -1) s))
             ((and (class _disk_capacity) (= (length (_disk_capacity . bv)) 1)) ;; big1
              (write-long (elt (_disk_capacity . bv) 0) s)
              (write-long (if (>= _disk_capacity 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _disk_capacity s)(write-long (if (>= _disk_capacity 0) 0 #xffffffff) s)))
     ;; uint64 _disk_available
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _disk_available (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _disk_available) (= (length (_disk_available . bv)) 2)) ;; bignum
              (write-long (ash (elt (_disk_available . bv) 0) 0) s)
              (write-long (ash (elt (_disk_available . bv) 1) -1) s))
             ((and (class _disk_available) (= (length (_disk_available . bv)) 1)) ;; big1
              (write-long (elt (_disk_available . bv) 0) s)
              (write-long (if (>= _disk_available 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _disk_available s)(write-long (if (>= _disk_available 0) 0 #xffffffff) s)))
     ;; uint64 _recorder_buffer_used
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _recorder_buffer_used (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _recorder_buffer_used) (= (length (_recorder_buffer_used . bv)) 2)) ;; bignum
              (write-long (ash (elt (_recorder_buffer_used . bv) 0) 0) s)
              (write-long (ash (elt (_recorder_buffer_used . bv) 1) -1) s))
             ((and (class _recorder_buffer_used) (= (length (_recorder_buffer_used . bv)) 1)) ;; big1
              (write-long (elt (_recorder_buffer_used . bv) 0) s)
              (write-long (if (>= _recorder_buffer_used 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _recorder_buffer_used s)(write-long (if (>= _recorder_buffer_used 0) 0 #xffffffff) s)))
     ;; uint64 _recorder_buffer_capacity
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _recorder_buffer_capacity (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _recorder_buffer_capacity) (= (length (_recorder_buffer_capacity . bv)) 2)) ;; bignum
              (write-long (ash (elt (_recorder_buffer_capacity . bv) 0) 0) s)
              (write-long (ash (elt (_recorder_buffer_capacity . bv) 1) -1) s))
             ((and (class _recorder_buffer_capacity) (= (length (_recorder_buffer_capacity . bv)) 1)) ;; big1
              (write-long (elt (_recorder_buffer_capacity . bv) 0) s)
              (write-long (if (>= _recorder_buffer_capacity 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _recorder_buffer_capacity s)(write-long (if (>= _recorder_buffer_capacity 0) 0 #xffffffff) s)))
     ;; uint64 _droppedMessagesSinceStart
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _droppedMessagesSinceStart (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _droppedMessagesSinceStart) (= (length (_droppedMessagesSinceStart . bv)) 2)) ;; bignum
              (write-long (ash (elt (_droppedMessagesSinceStart . bv) 0) 0) s)
              (write-long (ash (elt (_droppedMessagesSinceStart . bv) 1) -1) s))
             ((and (class _droppedMessagesSinceStart) (= (length (_droppedMessagesSinceStart . bv)) 1)) ;; big1
              (write-long (elt (_droppedMessagesSinceStart . bv) 0) s)
              (write-long (if (>= _droppedMessagesSinceStart 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _droppedMessagesSinceStart s)(write-long (if (>= _droppedMessagesSinceStart 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; time _wall_time
     (send _wall_time :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _wall_time :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; time _recording_start_time
     (send _recording_start_time :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _recording_start_time :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; duration _recording_duration
     (send _recording_duration :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _recording_duration :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; bool _recording
     (setq _recording (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _timeshift_enabled
     (setq _timeshift_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; uint64 _disk_capacity
     
#+(or :alpha :irix6 :x86_64)
      (setf _disk_capacity (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _disk_capacity (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; uint64 _disk_available
     
#+(or :alpha :irix6 :x86_64)
      (setf _disk_available (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _disk_available (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; uint64 _recorder_buffer_used
     
#+(or :alpha :irix6 :x86_64)
      (setf _recorder_buffer_used (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _recorder_buffer_used (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; uint64 _recorder_buffer_capacity
     
#+(or :alpha :irix6 :x86_64)
      (setf _recorder_buffer_capacity (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _recorder_buffer_capacity (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; uint64 _droppedMessagesSinceStart
     
#+(or :alpha :irix6 :x86_64)
      (setf _droppedMessagesSinceStart (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _droppedMessagesSinceStart (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(setf (get rosbag::RosbagRecorderState :md5sum-) "d5e30b7ec94cbb4dcb4898b09b7dc723")
(setf (get rosbag::RosbagRecorderState :datatype-) "rosbag/RosbagRecorderState")
(setf (get rosbag::RosbagRecorderState :definition-)
      "Header header
time wall_time

time recording_start_time
duration recording_duration

bool recording
bool timeshift_enabled

uint64 disk_capacity
uint64 disk_available

# the same buffer is used in timeshift mode as in normal recording mode
uint64 recorder_buffer_used
uint64 recorder_buffer_capacity 

uint64 droppedMessagesSinceStart


================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :rosbag/RosbagRecorderState "d5e30b7ec94cbb4dcb4898b09b7dc723")


