import React from 'react';
import { defaultEmptyValue } from '../../Table';

export default function LabelEntry(props) {

    let defaultLabelValue = props.defaultValue;
    let labelValue = props.value;

    if (defaultLabelValue === undefined) {
        defaultLabelValue = defaultEmptyValue
    }

    if (labelValue === undefined || labelValue === "") {
      labelValue = defaultLabelValue;
    }

    if (labelValue instanceof Object){
        labelValue = defaultLabelValue;
    }

    return (
        <span>{labelValue}</span>
    )
}