import requests


bagfile_url = 'http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/bagfile/'
childbagfile_url = 'http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/childbagfile/'
extractorrequest_url = 'http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/extractorrequest/'
sequence_url = 'http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/sequence/'
childbagfile_search_url = 'http://danf-search-api-dev.westeurope.cloudapp.azure.com:8889/search/childbagfile'


class TestData:

    bagfile_payload = {
        "version": "1.0",
        "guid": "",
        "link": "XXXXXX",
        "size": 21231,
        "num_messages": 4444,
        "start": 1554800000000,
        "end": 1557848250000,
        "duration": 3048250000,
        "vehicle_id_num": "V-123-234",
        "extractor": "ext1"
    }

    @staticmethod
    def extraction_payload(bagfile_guid, incidents, incident_duration):
        """
        :param bagfile_guid:
        :param incidents: number of incidents, i.e. entries to the metadata
            list
        :param incident_duration: duration of a single incident in seconds
        :return:
        """

        time_between_incidents = 1000
        incident_duration_ms = incident_duration * 1000
        required_time = (incident_duration_ms + time_between_incidents) * incidents

        assert required_time < TestData.bagfile_payload['duration'],\
            'required time must not exceed duration of bagfile'

        p = {
            "version": "1.0",
            "bagfile_guid": bagfile_guid,
            "extractor_id": "ext1",
            "metadata_list": []
        }

        start = TestData.bagfile_payload['start']
        for i in range(incidents):
            end = start + incident_duration_ms
            metadata = {
                "start": start,
                "end": end,
                "metadata": {
                    "key1": "foo",
                    "about": "bar",
                    "registered": "2014-05-09T04:56:33 -02:00",
                    "tags": ["Lorem"]
                },
                "bva": 10
            }
            p['metadata_list'].append(metadata)
            start = end + time_between_incidents

        return p


def create_bagfile():
    res = requests.post(bagfile_url, json=TestData.bagfile_payload)
    uuid = res.headers['Location'].split('/')[2]
    return uuid


def delete_resources(bagfile_guid):
    # delete bagfile and associated sequences
    requests.delete(bagfile_url + bagfile_guid)

    # delete child bagfiles
    res = requests.get(childbagfile_search_url, params={'parent_guid': bagfile_guid})
    for child in res.json()['results']:
        requests.delete(childbagfile_url + child.get('guid'))

    # TODO: delete extraction request
