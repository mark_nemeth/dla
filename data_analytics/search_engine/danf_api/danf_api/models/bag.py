"""Bag related models."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from enum import Enum
from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel, conint

from .base import OpValue, BaseQueryParams
from .tag import Tag, TagFilterParams

DEFAULT_BAGFILE_PROJECTION = [
    "guid",
    "link",
    "tags",
    "file_name",
    "start",
    "end",
    "duration",
    "vehicle_id_num",
    "size",
    "num_messages",
    "drive_types",
    "file_guid",
    "drive_guid",
    "origin",
]
DEFAULT_CHILDBAGFILE_PROJECTION = [
    "guid",
    "link",
    "tags",
    "file_name",
    "start",
    "end",
    "duration",
    "vehicle_id_num",
    "size",
    "num_messages",
    "drive_types",
    "file_guid",
    "parent_guid",
    "origin",
]


class BagType(str, Enum):
    BAGFILE = "bagfile"
    CHILDBAGFILE = "childbagfile"


class Bag(BaseModel):
    bag_type: BagType
    guid: Optional[UUID]
    link: Optional[str]
    tags: Optional[List[Tag]]
    file_name: Optional[str]
    start: Optional[int]
    end: Optional[int]
    duration: Optional[int]
    topics: Optional[List[str]]
    vehicle_id_num: Optional[str]
    size: Optional[int]
    num_messages: Optional[int]
    drive_types: Optional[List[str]]
    origin: Optional[str]
    drive_guid: Optional[UUID]
    parent_guid: Optional[UUID]


class ManyBags(BaseModel):
    bags: List[Bag]
    total_count: conint(ge=0)


class BagFilterParams(BaseModel):
    guid: Optional[List[OpValue]]
    start: Optional[List[OpValue]]
    end: Optional[List[OpValue]]
    duration: Optional[List[OpValue]]
    vehicle_id_num: Optional[List[OpValue]]
    topics: Optional[List[OpValue]]
    size: Optional[List[OpValue]]
    num_messages: Optional[List[OpValue]]
    tags: Optional[List[OpValue]]
    drive_types: Optional[List[OpValue]]
    file_name: Optional[List[OpValue]]
    origin: Optional[List[OpValue]]
    tag: Optional[List[TagFilterParams]]
    no_tag: Optional[List[TagFilterParams]]


class BagQueryParams(BaseQueryParams):
    filter_params: BagFilterParams
    bag_type: Optional[BagType]
