from dataclasses import dataclass
from logging import Logger
from typing import (
    Generator,
    List,
)

import requests
from dataclasses_json import dataclass_json
from DANFUtils.constants import ROOT_FILE_GUID


@dataclass
class Url:
    accessor_url: str
    creation_date: int
    entry_created_by: str
    url_status: str
    last_changed: int


@dataclass_json
@dataclass
class File:
    file_guid: str
    file_size: int
    file_type: str
    parent_guid: str
    tags: List[str]
    urls: List[Url]


class DataCatalogConnector:

    def __init__(self, logger: Logger, metadata_api_catalog_url: str, search_api_catalog_url: str,
                 dry_run: bool = False) -> None:
        """ Create data catalog instance.

        :param logger: Instance of a logger, to write logs to.
        :param metadata_api_catalog_url: Url of the metadata api.
        :param search_api_catalog_url: Url of the search api.
        :param dry_run: If set, no PUT or POST will be sent.
        """
        self.logger = logger
        self.metadata_api_catalog_url = self._sanitize_url(metadata_api_catalog_url)
        self.search_api_catalog_url = self._sanitize_url(search_api_catalog_url)
        self.dry_run = dry_run

    @staticmethod
    def _sanitize_url(url: str) -> str:
        return url[:-1] if url.endswith('/') else url

    def iterate_catalog(self, chunk_size: int = 50) -> Generator[File, None, None]:
        """ A generator that iterates over the entire file catalog.

        :param chunk_size: How many files are queried from the file catalog at once. Files are still yielded one by one.
        :return: A File Object representing a single file.
        """
        offset = 0
        while True:
            self.logger.info("Requesting next data catalog entries...")
            response = requests.get(f"{self.search_api_catalog_url}/file/?limit={chunk_size}&offset={offset}",
                                    verify=False)
            assert response.status_code == 200
            self.logger.info("Request successful.")
            data = response.json()['results']

            if not data:
                self.logger.info("All data catalog entries queried.")
                return

            for entry in data:
                try:
                    yield File.from_dict(entry)
                except KeyError:
                    self.logger.warning(f"Could not serialize {entry}")

            offset += chunk_size

    def update_url_status(self, file_guid: str, url_id: int, new_status: str) -> None:
        """ Update the url status. Will raise an assertion error if the response from the file catalog is not 200.

        :param file_guid: Unique guid the url is attached to.
        :param url_id: The id of the url. This is the index in the urls field.
        :param new_status: New status.
        :return: None
        """
        self.logger.info(f"Updating file {file_guid}, url_id {url_id} to status {new_status}...")
        if not self.dry_run:
            response = requests.put(f"{self.metadata_api_catalog_url}/file/{file_guid}/urls/{url_id}/status",
                                    json={"url_status": new_status},
                                    verify=False)
            assert response.status_code == 200
        else:
            self.logger.info(f"No request sent since dry run is set.")
        self.logger.info(f"Update completed.")

    def add_file(self, file_guid: str, file_size: int, file_type: str) -> None:
        """ Add a new file to the file catalog. Will raise an assertion error if the response from the file catalog
        is not 201.

        :param file_guid: Unique guid of the file.
        :param file_size: File size in bytes of the file.
        :param file_type: File type of the file.
        :return: None
        """
        self.logger.info(f"Adding file {file_guid}, file_size {file_size}, file_type {file_type}...")
        payload = {
            "parent_guid": str(ROOT_FILE_GUID),
            "file_type": file_type,
            "file_size": file_size,
            "tags": []
        }
        if not self.dry_run:
            response = requests.put(f"{self.metadata_api_catalog_url}/file/{file_guid}",
                                    json=payload,
                                    verify=False)
            assert response.status_code == 201
        else:
            self.logger.info(f"No request sent since dry run is set.")
        self.logger.info(f"Adding completed.")

    def add_accessor_url(self, file_guid: str, accessor_url: str, creation_date: int) -> None:
        """ Adds a new accessor url to an existing file.

        :param file_guid: Unique guid of the file.
        :param accessor_url: Url in fully qualified format. I.e. starting with "hdfs://
        :param creation_date: Timestamp in ms.
        :return: None
        """
        self.logger.info(f"Adding to file {file_guid}, accessor_url {accessor_url}, creation_date {creation_date}...")
        payload = {
            "accessor_url": accessor_url,
            "creation_date": creation_date,
            "entry_created_by": "Data Crawler",
            "url_status": "Available"
        }
        if not self.dry_run:
            response = requests.post(f"{self.metadata_api_catalog_url}/file/{file_guid}/urls",
                                     json=payload,
                                     verify=False)
            assert response.status_code == 200
        else:
            self.logger.info(f"No request sent since dry run is set.")
        self.logger.info(f"Adding completed.")
