import numpy as np
import pandas as pd
import shapely
import geopandas as gpd
import geopy
from geopy import distance

tracks_df_intersections = pd.read_csv("../Data/tracks_intersection_dyn_obj_ego_w_relative.csv")
dynamic_obj_intersections = tracks_df_intersections.groupby(['Filename', 'Track_id']).first()
dynamic_obj_intersections.reset_index(inplace=True)

map_objects=pd.read_csv('../Data/map_objects.csv')

def calculate_distance_of_nearest_mapobj(dyn_obj_row):
    distance_from_point = map_objects_selected.apply(lambda _: geopy.distance.geodesic(
        distance.lonlat(*(dyn_obj_row['Lon_intersect'], dyn_obj_row['Lat_intersect'])),
        distance.lonlat(*(_['Lon'],_['Lat']))).meters, axis=1)
    return np.min(distance_from_point)


for mo_type in ['CROSSWALK', 'PARKINGSPACE']:
    map_objects_selected = map_objects[map_objects['object_type']==mo_type].copy()
    dynamic_obj_intersections['Proximity_'+mo_type] = dynamic_obj_intersections.apply(calculate_distance_of_nearest_mapobj, axis=1)
    
dynamic_obj_intersections.to_csv('../Data/dynamic_objects_intersections_w_mo_small.csv', index=False)