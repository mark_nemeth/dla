import {connect} from 'react-redux';
import {ActionBar} from './ActionBar';
import {deleteSelectedBagfiles, downloadSelectedBagfiles} from '../../redux/Home/HomeActions';

export default connect(
  null,
  {downloadSelectedBagfiles, deleteSelectedBagfiles}
)(ActionBar);