COPYRIGHT: (c) 2017 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this product as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.

--------------------------------------------------------------------

This product bundles "ROS communications-related packages", which is available under a "BSD 3-clause" License.
For details, see: LICENSES/ros_com-license.txt

This product bundles "Boost C++ Libraries", which is available under a "Boost Software License 1.0" License.
For details, see: LICENSES/boost-license.txt

This product uses "Bzip2", which is available under a "Bzip2" License.
For details, see: LICENSES/bzip2-license.txt

