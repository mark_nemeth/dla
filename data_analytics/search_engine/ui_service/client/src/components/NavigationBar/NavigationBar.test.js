import React from 'react';
import NavigationBar from './NavigationBar';
import {shallow} from 'enzyme/build';

describe('NavigationBar component', () => {

  it('should renders show correct file name', () => {
    const props = {
      currentFilePath: 'path1/path2/name.bag',
    };
    const wrapper = shallow(<NavigationBar {...props}/>);
    expect(wrapper.find('BreadcrumbItem').at(1).props().children).toBe('name');
  });

  it('should renders show empty file name', () => {
    const props = {};
    const wrapper = shallow(<NavigationBar {...props}/>);
    expect(wrapper.find('BreadcrumbItem').at(1).props().children).toBe('');
  });

  it('should navigate to Home when click on breadcrumb home link', () => {
    const goHomeAction = jest.fn();
    const props = {
      currentFilePath: 'path1/path2/name.bag',
      navigate: {
        push: goHomeAction
      }
    };
    const wrapper = shallow(<NavigationBar {...props}/>);
    wrapper.find('BreadcrumbItem').first().simulate('click');
    expect(goHomeAction).toHaveBeenCalledWith('/');
  });

  it('should to last Page when click on back button DISABLED ', () => {
    const goBackAction = jest.fn();
    const props = {
      currentFilePath: 'path1/path2/name.bag',
      navigate: {
        length: 3,
        goBack: goBackAction
      }
    };
    const wrapper = shallow(<NavigationBar {...props}/>);
    //wrapper.find('Button').first().simulate('click');
    expect(goBackAction).not.toHaveBeenCalled();
  });

  it('should hide back button when no history exists', () => {
    const goBackAction = jest.fn();
    const props = {
      currentFilePath: 'path1/path2/name.bag',
      navigate: {
        length: 1,
        goBack: goBackAction
      }
    };
    const wrapper = shallow(<NavigationBar {...props}/>);
    expect(wrapper.find('Button').first().exists()).toBe(false);
    expect(goBackAction).not.toHaveBeenCalled();
  });

});