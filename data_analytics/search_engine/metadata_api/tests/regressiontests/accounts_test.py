"""Regressiontests for account routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import random as rd
import unittest
from copy import deepcopy

import requests
from DANFUtils.utils import generate_deterministic_guid

from tests.regressiontests.testutils import get_testdb_connector


# urls
base_url = 'http://localhost:8887'
accounts_url = base_url + '/accounts'

rd.seed("test")

raw_data_account1 = {
    'guid': generate_deterministic_guid(),
    'name': "integration-test-ws-1",
    'key_hash': "keyhash1",
    'available_quota': 1000,
    'entitlements': ['READ.X', 'WRITE.Y']
}

raw_data_account2 = {
    'guid': generate_deterministic_guid(),
    'name': "integration-test-ws-2",
    'key_hash': "keyhash2",
    'available_quota': 2000,
    'entitlements': ['READ.X', 'WRITE.Y']
}

create_account_request = {
    'name': "integration-test-ws-create",
    'key': "key",
    'available_quota': 2000
}

update_account_request = {
    'key': "key",
    'available_quota': 500,
}


def prepare_db():
    with get_testdb_connector() as con:
        con.db['accounts'].delete_many(
            {'name': {'$regex': '^integration-test-ws.*'}})


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['accounts'].delete_many(
            {'name': {'$regex': '^integration-test-ws.*'}})


def persist_accounts(accounts):
    with get_testdb_connector() as con:
        con.add_accounts([deepcopy(acc) for acc in accounts])


class TestAccountsGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_accounts_GET_should_return_all_accounts(self):
        persist_accounts([raw_data_account1, raw_data_account2])

        res = requests.get(url=accounts_url)
        self.assertEqual(200, res.status_code, 'Fetching accounts failed')

        fetched_account_guids = [account['guid'] for account in res.json()['results']]
        persistend_account_guids = [account['guid'] for account in [raw_data_account1, raw_data_account2]]
        self.assertCountEqual(fetched_account_guids, persistend_account_guids)


class TestAccountPOST(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_accounts_POST_when_valid_account_should_create_successful(self):
        data = deepcopy(create_account_request)

        res = requests.post(url=accounts_url, json=data)
        self.assertEqual(201, res.status_code, 'Account creation failed')
        location = res.headers['Location']
        self.assertRegex(
            location,
            '/accounts/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_accounts_POST_when_invalid_input_parameter_should_return_400(self):
        data = deepcopy(create_account_request)
        data['available_quota'] = "foo"

        res = requests.post(url=accounts_url, json=data)
        self.assertEqual(400, res.status_code, 'Invalid quota not detected')

    def test_accounts_POST_when_required_parameter_is_missing_should_return_400(self):
        data = deepcopy(create_account_request)
        del data['available_quota']

        res = requests.post(url=accounts_url, json=data)
        self.assertEqual(400, res.status_code, 'Invalid quota not detected')


class TestAccountGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_GET_account_when_account_exists_should_return_it(self):
        persist_accounts([raw_data_account1, raw_data_account2])

        res = requests.get(url=accounts_url + f"/{raw_data_account1['guid']}")
        self.assertEqual(200, res.status_code, 'Fetching account failed')

        fetched_account = res.json()['results']
        self.assertEqual(1, len(fetched_account),
                         'Did not return expected number of results')
        self.assertEqual(raw_data_account1['guid'], fetched_account[0]['guid'],
                         'Guid of returned account does not match')

    def test_GET_account_should_not_return_hash_key(self):
        persist_accounts([raw_data_account1])

        res = requests.get(url=accounts_url + f"/{raw_data_account1['guid']}")
        self.assertEqual(200, res.status_code, 'Fetching account failed')

        self.assertIsNone(res.json()['results'][0].get('account_key_hash'),
                          'key hash present in response')

    def test_GET_account_when_guid_does_not_exist_should_return_404(self):
        res = requests.get(url=accounts_url + "/00000000-0000-0000-0000-000000000000")
        self.assertEqual(404, res.status_code, 'Unexpeced status code')


class TestAccountPATCH(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_accounts_PATCH_when_request_valid_should_update(self):
        persist_accounts([raw_data_account1])

        data = deepcopy(update_account_request)

        res = requests.patch(url=accounts_url + f"/{raw_data_account1['guid']}",
                             json=data)
        self.assertEqual(200, res.status_code, 'Updating account failed')

    def test_accounts_PATCH_when_invalid_input_parameter_should_return_400(self):
        persist_accounts([raw_data_account1])

        data = {'available_quota': 'foo'}

        res = requests.patch(url=accounts_url + f"/{raw_data_account1['guid']}",
                             json=data)
        self.assertEqual(400, res.status_code, 'Invalid update not detected')

    def test_accounts_PATCH_when_account_does_not_exist_should_return_404(self):
        data = deepcopy(update_account_request)

        res = requests.patch(url=accounts_url + "00000000-0000-0000-0000-000000000000",
                             json=data)
        self.assertEqual(404, res.status_code, 'Missing account not detected')


if __name__ == '__main__':
    unittest.main()
