"""Search Engine and Metadata REST API extractor request route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
from typing import List

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from handlers.auth import auth
from model.exceptions import DANFApiError
from handlers.extractor_requests import ExtractorRequestsHandler
from packaging.version import Version, parse
from handlers.utils import utils
from model.extractor_event import ExtractedEvent
from requesthandler.base_request_handler import BaseRequestHandler


class ExtractorRequestsRequestHandler(BaseRequestHandler):
    _schema_guid = BaseRequestHandler.load_json_schema(
        "guid.schema")
    _schema_post = BaseRequestHandler.load_json_schema(
        "extractor_request_post.schema")

    def initialize(self):
        self.extractor_request_handler = ExtractorRequestsHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST, DELETE')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('extractorrequest/post',
                                      context_headers=self.request.headers)

        raw_json = json.loads(self.request.body.decode('utf-8'))
        if isinstance(raw_json, dict):
            # legacy dict
            data = self.validate_json(self.request.body.decode('utf-8'),
                                      self._schema_post)
            event_version = parse(data.get('version', '0.0'))
            if event_version >= Version("1.0"):
                raise DANFApiError(400, "Version must be below 1.0 for "
                                        "request with legacy dict format")
            guids = self.extractor_request_handler.new_extractor_request_legacy(
                data)
        else:
            # new Model
            data = self.deserialize(self.request.body.decode('utf-8'),
                                    List[ExtractedEvent])
            guids = self.extractor_request_handler.new_extractor_request(
                data)

        # extracting the last guid it can be removed later, it is for two-way
        # compatibility between container client and metadata api
        guid = None if not guids else guids[-1]
        res = {
            'guids': guids,
            'message': 'Extractor request successfully added to db.'
        }
        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res,
            status_code=constants["REQ_STATUS_CODE_RESOURCE_CREATED"],
            location="/extractorrequest/" + guid)

    @auth.requires_entitlements(["openid"])
    def delete(self, guid):
        tracing.start_task_in_context('extractorrequest/delete',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        res = self.extractor_request_handler.delete_extractor_request_from_db(
            guid)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])
