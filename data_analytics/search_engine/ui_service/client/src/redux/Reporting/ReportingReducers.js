import { createReducer } from '@reduxjs/toolkit';
import * as actions from './ReportingActions';
import { updatePrivTripFilterState } from './ReportingUtils'

export const ReportingReducer = createReducer({}, {
  // Filter reducers
  [actions.selectReportingDate]: (state, action) => {
    state.reporting.globalFilters.start = action.payload.startDate;
    state.reporting.globalFilters.end = action.payload.endDate;
    state.reporting.enableExport = false;
  },
  [actions.selectReportingLocation]: (state, action) => {
    state.reporting.globalFilters.location = action.payload.location;
    // update tripfilters based on report type and location
    updatePrivTripFilterState(state, state.reporting.globalFilters.report_type, action.payload.location);
    state.reporting.enableExport = false;
  },
  [actions.selectReportType]: (state, action) => {
    state.reporting.globalFilters.report_type = action.payload.reportType;
    let reportTypeObj = state.reporting.reportTypes[action.payload.reportType];
    // update location with default location for report type + enable/disable supported locations
    state.reporting.globalFilters.location = reportTypeObj.defaultLocation;
    for (let loc in state.reporting.locations) {
      state.reporting.locations[loc].enabled = reportTypeObj.enabledLocations.includes(loc);
    }    
    // update tripfilters based on report type and location
    updatePrivTripFilterState(state, action.payload.reportType, reportTypeObj.defaultLocation);
    state.reporting.enableExport = false;
  },
  [actions.setTripFilterSelection]: (state, action) => {
    state.reporting.tripFilters[action.payload.idx].selected = action.payload.value;
    state.reporting.enableExport = false;
  },
  // KPI fetching reducers
  [actions.fetchReportKpisSuccess]: (state, action) => {
    state.reporting.data.kpis = action.payload.kpis;
    state.reporting.kpisLoadingState = 'success';
  },
  [actions.fetchReportKpisEmpty]: (state, action) => {
    state.reporting.data.kpis = [];
    state.errorMessage = action.payload;
    state.errorMessageType = 'warning';
    state.reporting.kpisLoadingState = 'success';
  },
  [actions.fetchReportKpisError]: (state, action) => {
    state.reporting.data.kpis = [];
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.reporting.kpisLoadingState = 'error';
  },

  // Table fetching reducers
  [actions.fetchReportTablesSimpleSuccess]: (state, action) => {
    action.payload.tables.forEach((table, idx, arr) => {
      // Build column labels
      let colLabels = [...table.col_labels];

      // Build values table (list of objects) including totals
      var valuesShown = [];
      var valuesHidden = [];
      for (var j = 0; j < table.values.length; j++) {
        let row_new = {};
        for (var i = 0; i < colLabels.length; i++) {
          // resolve reoccurring values
          if (table.values[j][i] in table.key_value_map) {
            row_new[i.toString()] = table.key_value_map[table.values[j][i]];
          }
          else {
            row_new[i.toString()] = table.values[j][i];
          }
        }
        if (j < 50) {
          // Show only first 50 rows
          valuesShown.push(row_new);
        } else {
          // Buffer hidden values for export
          valuesHidden.push(row_new);
        }
      };
      var caption = table.caption;
      if (valuesHidden.length > 0) {
        caption = caption + " (Showing only " + valuesShown.length + " entries, export data to view all " + (valuesShown.length + valuesHidden.length) + ")"
      }
      state.reporting.data.simpleTables.push({
        'caption': caption,
        'colLabels': colLabels,
        'values': valuesShown,
        'valuesHidden': valuesHidden
      });
    });
    state.reporting.simpleTablesState = 'success';
  },
  [actions.fetchReportTablesSimpleEmpty]: (state, action) => {
    state.reporting.data.simpleTables = [];
    state.errorMessage = action.payload;
    state.errorMessageType = 'warning';
    state.reporting.simpleTablesState = 'success';
  },
  [actions.fetchReportTablesSimpleError]: (state, action) => {
    state.reporting.data.simpleTables = [];
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.reporting.simpleTablesState = 'error';
  },

  [actions.fetchReportTablesAggregationSuccess]: (state, action) => {
    action.payload.tables.forEach((table, idx, arr) => {
      // Build column labels
      let colLabels = [...table.col_labels];
      colLabels.unshift("Total");
      colLabels.unshift(table.row_caption);

      // Build values table (list of objects) including totals
      var values = [];
      table.values.forEach((row, idx, arr) => {
        let row_new = {};
        row_new['0'] = table.row_labels[idx];
        row_new['1'] = table.row_total[idx];
        for (var i = 2; i < colLabels.length; i++) {
          row_new[i.toString()] = row[i - 2];
        }
        values.push(row_new);
      });
      let totalRow = { '0': 'Total', '1': table.total };
      table.col_total.forEach((v, idx, arr) => {
        totalRow[(idx + 2).toString()] = v;
      });
      values.push(totalRow);

      state.reporting.data.aggregationTables.push({
        'caption': table.caption,
        'colLabels': colLabels,
        'values': values
      });
    });
    state.reporting.aggregationTablesState = 'success';
  },
  [actions.fetchReportTablesAggregationEmpty]: (state, action) => {
    state.reporting.data.aggregationTables = [];
    state.errorMessage = action.payload;
    state.errorMessageType = 'warning';
    state.reporting.aggregationTablesState = 'success';
  },
  [actions.fetchReportTablesAggregationError]: (state, action) => {
    state.reporting.data.aggregationTables = [];
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.reporting.aggregationTablesState = 'error';
  },

  // other report reducers
  [actions.prepareReportLoading]: (state, action) => {
    state.reporting.showReport = false;
    state.error = false;
    state.showLoading = true;
    state.reporting.data.kpis = [];
    state.reporting.data.simpleTables = [];
    state.reporting.data.aggregationTables = [];
    state.reporting.kpisLoadingState = 'loading';
    state.reporting.simpleTablesState = 'loading';
    state.reporting.aggregationTablesState = 'loading';
  },

  [actions.finalizeReportLoading]: (state, action) => {
    if (state.reporting.kpisLoadingState === 'success' && state.reporting.simpleTablesState === 'success' && state.reporting.aggregationTablesState === 'success') {
      if (state.reporting.data.kpis.length === 0 && state.reporting.data.simpleTables.length === 0 && state.reporting.data.aggregationTables.length === 0) {
        state.error = true;
      } else {
        state.reporting.showReport = true;
        state.reporting.enableExport = true;
      }
    }
    if (state.reporting.kpisLoadingState !== 'loading' && state.reporting.simpleTablesState !== 'loading' && state.reporting.aggregationTablesState !== 'loading') {
      state.showLoading = false;
    }

  }
});