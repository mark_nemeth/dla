'''Tests for DBConnector Document Handler'''

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
import random as rd

from DANFUtils.logging import logger
from DANFUtils.utils import generate_deterministic_guid
from DBConnector import DBConnector
from DBConnector.exceptions import IndexOverwriteException

from . import testconfig

rd.seed('test1')

# increase log level for tests
logger.set_log_level('debug')

doc_1 = {
    'guid': generate_deterministic_guid(),
    'type': 'VINs',
    'version': '1.0',
    'vins': ['V1234-567', 'V1234-890'],
    'is_test_data': 1
}

doc_2 = {
    'guid':
    generate_deterministic_guid(),
    'type':
    'topics',
    'version':
    '1.0',
    'topics': [
        '/lidar/VelodyneMonitorDriver/LidarVeloRoofFL/VelodyneSensorStatus',
        '/planning/Trajectory2DArray', '/environment_model/dynamic_agents',
        '/sem/SemReporterService/0/SemStateInterface',
        '/planning/Planner/timing',
        '/stereo/CameraImage/StereoCameraFront/CameraImageRight',
        '/environment_model/ego_vehicle_state',
        '/environment_model/dsm_interface'
    ],
    'is_test_data':
    1
}

doc_3 = {
    'guid': generate_deterministic_guid(),
    'type': "sync_flag",
    'version': '1.0',
    'bagfile_guids': ["cac739c6-90bf-40f1-8e6c-aa0842926414", "9fd08e38-0af8-4648-99f3-16f283784b99"],
    'is_test_data': 1
}

def cleanup_db():
    connector = DBConnector(
        testconfig.db_hostname, testconfig.db_port, testconfig.db_name,
        testconfig.db_user, testconfig.db_key, testconfig.db_protocol)
    with connector as con:
        con.db['documents'].delete_many({'is_test_data': 1})


class TestDocumentIO(unittest.TestCase):

    def setUp(self):
        self.connector = DBConnector(
            testconfig.db_hostname, testconfig.db_port, testconfig.db_name,
            testconfig.db_user, testconfig.db_key, testconfig.db_protocol)

    def test_write_update_read_delete_single_document(self):
        cleanup_db()

        new_vins = ['1234-567', 'V1234-890', 'V9999-999']
        with self.connector as con:
            # write single document, read it back by guid
            con.add_documents([doc_1])
            res_document = con.get_documents([doc_1['guid']])[0]
            self.assertDictEqual(doc_1, res_document,
                                 'Write-read failed for single document!')

            # update single document, read it back by guid
            con.update_documents([doc_1['guid']], [{
                'type': 'VINs',
                'vins': new_vins,
                'is_test_data': 1
            }])
            res_document = con.get_documents([doc_1['guid']])[0]
            self.assertEqual(new_vins, res_document['vins'],
                             'Update failed for single document!')

            # Create-Update single document, read it back by guid
            con.update_documents(
                [doc_2['guid']],
                [{
                    'type': 'topics',
                    'topics': ['/environment_model/ego_vehicle_state'],
                    'is_test_data': 1
                }],
                create_if_missing=True)
            res_document = con.get_documents([doc_2['guid']])[0]
            self.assertEqual(['/environment_model/ego_vehicle_state'],
                             res_document['topics'],
                             'Create-Update failed for single document!')

            # delete single document by guid
            con.delete_documents([doc_1['guid']])
            document_count = con.get_documents([doc_1['guid']]).count()
            self.assertEqual(0, document_count,
                             'Delete failed for single document!')

        cleanup_db()

    def test_write_search_delete_multiple_documents(self):
        cleanup_db()

        with self.connector as con:
            # write and search multiple documents
            con.add_documents([doc_1, doc_2])
            query = {'$or': [{'guid': doc_1['guid']}, {'guid': doc_2['guid']}]}
            res_doc = list(
                con.search_documents(query,
                                     limit=2,
                                     offset=1,
                                     sortby=[('guid', -1)]))
            self.assertCountEqual([doc_1], res_doc,
                                  'Search failed for multiple documents!')

            # delete multiple documents by guid
            con.delete_documents([doc_1['guid'], doc_2['guid']])
            doc_count = con.get_documents([doc_1['guid'],
                                           doc_2['guid']]).count()
            self.assertEqual(0, doc_count,
                             'Delete failed for multiple documents!')

        cleanup_db()

    def test_write_search_documents_by_type(self):
        cleanup_db()

        with self.connector as con:
            # write and search multiple documents
            con.add_documents([doc_1, doc_2])
            query = {'type': 'VINs'}
            res_doc = list(con.search_documents(query, limit=1, offset=0))
            self.assertCountEqual([doc_1], res_doc,
                                  'Search by type failed for documents!')

        cleanup_db()

    def test_search_bad_query(self):
        with self.connector as con:
            query = {
                '$or': [{
                    'parent_guid': doc_1['guid']
                }, {
                    'guid': doc_2['guid']
                }]
            }
            res_doc_2 = con.search_documents(query, limit=5, offset=1)
            self.assertIsNone(
                res_doc_2,
                'Invalid search test failed for multiple documents!')

    def test_guid_overwrite(self):
        cleanup_db()

        new_vins = ['1234-567', 'V1234-890', 'V9999-999']
        with self.connector as con:
            con.add_documents([doc_1])
            with self.assertRaises(IndexOverwriteException) as context:
                con.update_documents([doc_1['guid']], [{
                    'type': 'VINs',
                    'vins': new_vins,
                    'guid': '12345'
                }])
        cleanup_db()

    def test_delete_sync_guid(self):

        with self.connector as con:
            con.add_documents([doc_3])
            con.pop_from_arrays_in_documents([doc_3['guid']], [{"bagfile_guids": -1}])

            guids = con.get_documents([doc_3['guid']])[0]['bagfile_guids']
            self.assertNotIn("cac739c6-90bf-40f1-8e6c-aa0842926414", guids)

        cleanup_db()

    def test_add_sync_guid(self):

        with self.connector as con:
            con.add_documents([doc_3])
            con.push_to_arrays_in_documents([doc_3['guid']], [{"bagfile_guids": ["ec0b9910-56ef-4fcf-bfa3-0cb9dd4e7071"]}])

            guids = con.get_documents([doc_3['guid']])[0]['bagfile_guids']
            self.assertIn("ec0b9910-56ef-4fcf-bfa3-0cb9dd4e7071", guids)

        cleanup_db()

if __name__ == '__main__':
    unittest.main()
