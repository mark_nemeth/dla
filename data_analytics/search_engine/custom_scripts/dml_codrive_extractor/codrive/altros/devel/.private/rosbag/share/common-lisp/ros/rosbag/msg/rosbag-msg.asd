
(cl:in-package :asdf)

(defsystem "rosbag-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "RosbagRecorderState" :depends-on ("_package_RosbagRecorderState"))
    (:file "_package_RosbagRecorderState" :depends-on ("_package"))
  ))