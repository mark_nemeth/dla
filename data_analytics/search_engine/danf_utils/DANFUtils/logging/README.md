# DANFUtils - logging

The DANFUtils logging module provides an opinionated configuration of the root logger of the python standard library logging module.

Calling the `configure_root_logger` function will add logging handlers and formatters to the
root logger according to our best practices. `configure_root_logger` also allows for some limited configuration
options.

In the application code, one can simply acquire logger instances from the standard library and use them
without attaching any handlers or formatters. This ensures consistent formatting and shipping
of all logs from DANF or third party components. Only in special cases, custom formatters/handlers should be utilized.

## Usage

### Hello World

```python
import logging
from DANFUtils.logging import configure_root_logger

configure_root_logger()
logger = logging.getLogger(__name__)
logger.info('Hello World')
```

### Log levels

The root logger and its handlers are by default logging at DEBUG. This can be changed via `configure_root_logger`.
Adapting handlers of the root logger individually is not supported.

```python
configure_root_logger(log_level=logging.ERROR)
```

Instead of adjusting the log level of the root logger, it often makes more sense to adjust log levels on the different
loggers in the application code. Using a hierarchical logger organization with `getLogger(__name__)` comes handy hereby.

```python
# root logger logs at debug
configure_root_logger(log_level=logging.DEBUG)

# in module A: Set log level for A to INFO
logger = logging.getLogger(__name__)  # logs with log level INFO
logger.setLevel(logging.INFO)

# in module A.B: Automatcally inherit log level INFO
logger = logging.getLogger(__name__)  # logs with log level INFO

# in module A.C: Override inherited log level with WARNING
logger = logging.getLogger(__name__)  # logs with log level WARNING
logger.setLevel(logging.WARNING)
```

### Structured logging

The default log formatting of the console log handler are plain strings. JSON logging to console
can be activated:

```python
configure_root_logger(log_structure=LogStructure.JSON)
```

### Logging to files

File logging is deactivated by default. To activate additional logging to a file,
pass the path to a log file to `configure_root_logger`.

```python
configure_root_logger(file_path='my_logfile.log')
```