import React from 'react';
import Header from './Header';
import {shallow} from 'enzyme/build';

describe('ActionBar component', () => {

  it('should renders and show links in correct order', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('NavLink').at(0).props().to).toBe('/');
    expect(wrapper.find('NavLink').at(1).props().to).toBe('/announcements');
    expect(wrapper.find('NavLink').at(2).props().to).toBe('/support');
    expect(wrapper.find('NavLink').at(3).props().href).toBe('/logout');
  });

  it('should render and display new badge if there is an announcement', () => {
    jest.mock('../../pages/Announcements/Announcements.json',
              () => {
                const returnVal = [{"title": "Test","body": "Test message","date": new Date()}];
                return returnVal;
              }, 
              {virtual: true});
    const wrapper = shallow(<Header />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('Badge').at(0));
  });
});