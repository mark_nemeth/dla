import React from 'react';
import Card from "react-bootstrap/Card";
import './DriveMetadataView.scss';

export function DriveMetadataView(props) {
  return (
      <Card className="drive-information-section">
        <span className="drive-information-section-label">
          For drive metadata see the information column on the left.
        </span>
      </Card>
  )
}