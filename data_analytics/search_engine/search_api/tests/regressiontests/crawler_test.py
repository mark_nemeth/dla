"""Regression tests for crawler bagfiles route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests

base_url = 'http://localhost:8889'
crawler_bagfiles_url = base_url + '/crawler/bagfiles'


class TestSearchBagfilesGET(unittest.TestCase):

    def test_list_bagfile_links(self):
        res = requests.get(url=crawler_bagfiles_url)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'Crawler bagfiles status code test failed')
        self.assertIsInstance(res_json, list, 'Crawler bagfiles result test failed')
