"""Handler class for drive search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

from handlers.db_data_handler import DBDataHandler

logger = logging.getLogger(__name__)


class DriveSearchHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def create_search_kwargs(self, query_args):
        search_kwargs = {
            'query': None,
            'limit': 50,
            'offset': 0,
            'sortby': 'start',
            'projection': None
        }
        query = {}
        range_query = []
        or_query = []
        projection = {}
        # By default we should return not deleted documents
        if 'is_deleted' not in query_args:
            query_args['is_deleted'] = ['false']

        for key, value in query_args.items():
            # Query filters
            if key in ['guid', 'file_path']:
                query[key] = value[0]
            elif key == 'is_deleted':
                query[key] = self.create_is_deleted_query(value[0])
            elif key in ['driven_by', 'driver', 'operator', 'hardware_release', 'drive_types']:
                query[key] = {'$in': value}
            elif key == 'vehicle_id_num':
                query['vehicle_id_num'] = self.create_vin_query(query_args[key])
            elif key == 'description':
                query["description"] = {"$regex": value[0]}
            elif key == "start[gte]":
                try:
                    range_query.append({'start': {"$gte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning(
                        'Invalid start[gte] query parameter in drive search - integer expected'
                    )
            elif key == 'start[lte]':
                try:
                    range_query.append({'start': {"$lte": abs(int(value[0]))}})
                except ValueError:
                    logger.warning('Invalid start[lte] query parameter in drive search - integer expected')
            # Projection
            elif key == 'projection':
                projection[value[0]] = 1
            # Pagination
            elif key == 'limit':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning(
                        'Invalid limit query parameter in drive search - integer expected'
                    )
            elif key == 'offset':
                try:
                    search_kwargs[key] = abs(int(value[0]))
                except ValueError:
                    logger.warning('Invalid offset query parameter in drive search - integer expected')
            # Sort
            elif key == 'sortby':
                # Ascending order
                sorting_keys = ['guid', 'description', 'start', 'duration', 'vehicle_id_num', 'driven_by', 'driver',
                                'operator']
                if value[0] in sorting_keys:
                    search_kwargs[key] = value[0]
                elif value[0] in ["+" + key for key in sorting_keys]:
                    search_kwargs[key] = value[0][1:]
                # Descending order
                elif value[0] in ["-" + key for key in sorting_keys]:
                    search_kwargs[key] = [(value[0][1:], -1)]

        if len(range_query) >= 1:
            query['$and'] = range_query

        if len(or_query) >= 1:
            query['$or'] = or_query

        search_kwargs['query'] = query
        search_kwargs['projection'] = projection
        return search_kwargs

    def create_search_query(self, query_fields):
        field_list = []
        for key, value in query_fields.items():
            if key == 'vehicle_id_num':
                field_list.append({key: self.create_vin_query(value)})
            else:
                field_list.append({key: value})
        if len(field_list) > 1:
            return {"$and": field_list}
        elif len(field_list) == 1:
            return field_list[0]
        return {}

    @staticmethod
    def create_vin_query(vins_field):
        """
        to search for a list of VINs, query must be changed including an "$in"
        (true, if any VINs from query are in a database entry)
        :param vins_field: dict element containing the key == vin
        :return:
        """
        if isinstance(vins_field, list):
            vins_field = {"$in": vins_field}
        return vins_field

    @staticmethod
    def create_is_deleted_query(is_deleted_field):
        """
        To either search for bagfiles that are not soft deleted (default)
        or to specifically search for the deleted ones.
        :param is_deleted_field: if 'false', then "$ne: true" returns bagfiles with is_deleted = false OR missing field
        :return:
        """
        if str.lower(is_deleted_field) == 'false':
            is_deleted_field = {"$ne": True}
        else:
            is_deleted_field = {"$eq": True}
        return is_deleted_field

    def perform_search(self, **search_kwargs):
        return self.db_data_handler.search_drives(**search_kwargs)
