import shutil
import os

output_path = os.path.join('public/mockdata/')

if not os.path.exists(output_path):
    os.makedirs(output_path)

print("Copy local on-prem dummy json mock data files to: " + output_path)
shutil.copy(os.path.join('mock/on-prem/bagfiles.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/childbagfiles.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/filters.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/childbagfile_filters.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/flowruns.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/drives.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/drive_filters.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/ingests.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/latest_ingests.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/reporting_kpis.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/reporting_tables_simple.json'), output_path)
shutil.copy(os.path.join('mock/on-prem/reporting_tables_agg.json'), output_path)