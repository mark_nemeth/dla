__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


from typing import List

from pydantic import BaseModel

from model.types import Guid


class FlowStep(BaseModel):
    name: str


class Flow(BaseModel):
    version: str = "1.0"
    guid: Guid
    name: str
    flow_version: str
    kubeflow_experiment_id: Guid
    steps: List[FlowStep] = []


class CreateFlowRequest(BaseModel):
    name: str
    flow_version: str
    kubeflow_experiment_id: Guid
    steps: List[FlowStep] = []
