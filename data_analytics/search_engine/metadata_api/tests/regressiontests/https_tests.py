"""Regressiontests for sequence routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests

https_url = 'https://localhost:8443/bagfile/'
cert = '.cert/metadata-api-cert.pem'


class TestHTTPS(unittest.TestCase):

    @unittest.skip("test-case is broken")
    def test_bagfile_options_HTTPS(self):
        res = requests.options(url=https_url,
                               verify=cert)
        self.assertEqual(200, res.status_code)


if __name__ == '__main__':
    unittest.main()
