from dlalib.executors import LocalExecutor

executor = LocalExecutor()
files = [
    "/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/20180821_130123_alwayson.bag",
    "/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/20181010_100218_alwayson.bag"]
topics = ['/CalService/instance_0/VehicleDataInterface']

df_dict = executor.get_DF(files, topics)
