import React from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import './ByFileFilter.scss';

export const FileTypes = Object.freeze({
  ORIGINALFILE: 'originalfile',
  ALWAYSONFILE: 'alwaysonfile',
  CHILDBAGFILE: 'childbagfile',
  DRIVE: 'drive'
});

export function getFileTypeLabel(fileType) {
  switch (fileType) {
    case FileTypes.ORIGINALFILE:
      return 'Original File';
    case FileTypes.ALWAYSONFILE:
      return 'Alwayson File';
    case FileTypes.CHILDBAGFILE:
      return 'Snippet';
    case FileTypes.DRIVE:
      return 'Drive';
    default:
      return 'Original File';
  }
}

export function ByFileFilter(props) {

  function onFileTypeSelect(type) {
    props.filterByFileType(type);
  }

  return (
    <ButtonGroup className="file-filter">
      {props.isDriveViewActive &&
      <Button
          className={props.fileType === FileTypes.DRIVE ? "file-filter-selected-button" : "file-filter-unselected-button" + ' btn-info'}
          onClick={() => onFileTypeSelect(FileTypes.DRIVE)}>
        Drives</Button>
      }
      <Button
        className={props.fileType === FileTypes.ORIGINALFILE ? "file-filter-selected-button" : "file-filter-unselected-button" + ' btn-info'}
        onClick={() => onFileTypeSelect(FileTypes.ORIGINALFILE)}>
        Original files</Button>
      <Button
          className={props.fileType === FileTypes.ALWAYSONFILE ? "file-filter-selected-button" : "file-filter-unselected-button" + ' btn-info'}
          onClick={() => onFileTypeSelect(FileTypes.ALWAYSONFILE)}>
        Alwayson files</Button>
      <Button
        className={props.fileType === FileTypes.CHILDBAGFILE ? "file-filter-selected-button" : "file-filter-unselected-button" + ' btn-info'}
        onClick={() => onFileTypeSelect(FileTypes.CHILDBAGFILE)}>
        Snippets</Button>
    </ButtonGroup>
  );
}