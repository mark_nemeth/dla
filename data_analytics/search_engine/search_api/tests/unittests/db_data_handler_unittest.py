"""Unittest for db_data_handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import unittest
from unittest.mock import patch, MagicMock

from DANFUtils.exceptions import GUIDNotFoundException
from DANFUtils.utils import generate_deterministic_guid

from handlers.db_data_handler import DBDataHandler

bagfile_guid = '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'
vehicle_id_num =  'V-123-234'
guid = generate_deterministic_guid()


def generate_test_sequence(index):
    return {
        "guid": guid,
        "bagfile_guid": bagfile_guid,
        "index": index,
        "start": 1475272800000 + index * 1000,
        "end": 1475272800000 + index * 1000 + 999,
        "type": "basic",
        "extractors": {
            "ext1": {
                "bva": 5,
            },
            "ext2": {
                "bva": 5,
            },
            "ext3": {
                "bva": 5
            }
        }
    }


guid1 = generate_deterministic_guid()
guid2 = generate_deterministic_guid()

raw_data_bagfile1 = {
    "version": "1.0",
    "guid": guid1,
    "link": "XXXXXX",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1"
}

raw_data_bagfile2 = {
    "version": "1.0",
    "guid": guid2,
    "link": "XXXXXX",
    "size": 21231,
    "num_messages": 4444,
    "start": 1475272800000,
    "end": 1475272800001,
    "duration": 752728,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1"
}

child_1 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "parent_guid": bagfile_guid,
    "start": 1558962320000,
    "end": 1558962372000,
    "parent_index_start": 169,
    "parent_index_end": 220,
    "metadata": {},
    "bva": 7,
    "is_test_data": 1
}


class TestDbDataHandler(unittest.TestCase):
    @patch('handlers.db_data_handler.DBConnector')
    def test_get_sequences_by_bagflile_guid(self, mocked_DBConnector):
        ######Mock DBConnector ##############################
        mm = MagicMock()
        mm.get_bagfiles.return_value = raw_data_bagfile1
        mm.get_sequences_by_bagfile_guid.return_value = [
            generate_test_sequence(2),
            generate_test_sequence(5)
        ]
        mm.add_child_bagfiles.return_value = "TEST"
        mm.search_child_bagfiles.return_value = "childbagfile"
        mocked_DBConnector.return_value.__enter__.return_value = mm  #DBDataHandler use a with statement so we need to assign __enter__.return to mm
        ########################################################
        #######Test#############################################
        dbh = DBDataHandler()
        #Test return
        seqs = [generate_test_sequence(2), generate_test_sequence(5)]
        mm.get_sequences_by_bagfile_guid.return_value = seqs
        self.assertEqual(dbh.get_sequences_by_bagfile_guid("test"), seqs)
        #Test Exception
        mm.get_sequences_by_bagfile_guid.return_value = ""
        self.assertEqual(len(dbh.get_sequences_by_bagfile_guid("test")), 0)

    @patch('handlers.db_data_handler.DBConnector')
    def test_get_bagfiles(self, mocked_DBConnector):
        ######Mock DBConnector ##############################
        mm = MagicMock()
        mm.get_bagfiles.return_value = list(
            [raw_data_bagfile1,
             raw_data_bagfile2])  # Mock DBConnector method get_bagfiles(guids)
        mocked_DBConnector.return_value.__enter__.return_value = mm  #DBDataHandler use a with statement so we need to assign __enter__.return to mm
        ########################################################
        dbh = DBDataHandler()
        #Test return of get_bagfiles
        list_of_guids = list([guid1, guid2])
        self.assertEqual(dbh.get_bagfiles(list_of_guids),
                         list([raw_data_bagfile1, raw_data_bagfile2]))

        #Test Exception, wrong guid
        list_of_guids = list([guid1 + "-dsds", guid2 + "-dffdf"])  #wrong guid
        with self.assertRaises(GUIDNotFoundException):
            dbh.get_bagfiles(list_of_guids)

        #Test Exception, guid cannot be found in dB
        mm.get_bagfiles.return_value = list([])  #empty response from db
        with self.assertRaises(GUIDNotFoundException):
            dbh.get_bagfiles(list_of_guids)
            
    @patch('handlers.db_data_handler.DBConnector')
    def test_search_bagfiles(self, mocked_DBConnector):
        ######Mock DBConnector ##############################
        mm = MagicMock()
        mm.search_bagfiles.return_value = raw_data_bagfile1  #return bagfile
        mocked_DBConnector.return_value.__enter__.return_value = mm
        ########################################################

        #Test return
        dbh = DBDataHandler()
        query_data = {"vehicle_id_num": vehicle_id_num}
        self.assertEqual(dbh.search_bagfiles(query_data), (raw_data_bagfile1,1))

    @patch('handlers.db_data_handler.DBConnector')
    def test_add_child_bagfiles(self, mocked_DBConnector):
        ######Mock DBConnector ##############################
        mm = MagicMock()
        mm.add_child_bagfiles.side_effect = Exception(
            'Writing child bagfile doesnt work...'
        )  #raise a exception when method is called
        mocked_DBConnector.return_value.__enter__.return_value = mm
        ########################################################

        #Test Exception... find more useful test cases...
        dbh = DBDataHandler()
        with self.assertRaises(Exception):
            dbh.add_child_bagfiles("childbag_data")

    @patch('handlers.db_data_handler.DBConnector')
    def test_search_child_bagfiles(self, mocked_DBConnector):
        ######Mock DBConnector ##############################
        mm = MagicMock()
        mm.search_child_bagfiles.return_value = child_1  #return childbag
        mocked_DBConnector.return_value.__enter__.return_value = mm
        ########################################################

        #Test return
        dbh = DBDataHandler()
        query_data = {"parent_guid": bagfile_guid}
        self.assertEqual(dbh.search_child_bagfiles(query_data), (child_1,1))


if __name__ == '__main__':

    unittest.main()
