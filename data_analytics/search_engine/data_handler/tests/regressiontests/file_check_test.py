"""Regressiontests for bagfile routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import datetime
import json
import logging
import os
import random
import unittest
from pathlib import Path

from azure.storage.blob.blockblobservice import BlockBlobService

from handlers.file_check_handler import FileCheckHandler

logger = logging.getLogger(__name__)

res_on_prem = [{
    'path': str(os.path.expanduser("~")) + "/test_regressiontest.bag",
    'exists': True,
    'size': 0,
    'timestamp': datetime.datetime(2019, 10, 22, 7, 12, 50, 24777),
    'location': 'prem'
}]

res_cloud_blob = {
    'path':
    '/dlastorage1sadev/blobServices/default/containers/9999-99-99/blobs/test_regressiontest.bag',
    'exists':
    True,
    'size':
    0,
    'timestamp':
    "",
    'blob_type':
    'BlockBlob',
    'location':
    'cloud'
}


class TestFileCheck(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        path = Path(__file__).parents[2]
        with open(f"{path}/config/config_dev.init", "r") as f:
            configs_obj = json.load(f)
        cls.block_blob_service = BlockBlobService(
            account_name=configs_obj.get("storage_queue_account"),
            account_key=configs_obj.get("access_key_dlastorage1sadev"))
        cls.random_date = f"9999-99-{random.randint(10, 99)}"
        cls.create_test_file(cls)
        cls.create_file_in_cloud(cls)

    @classmethod
    def tearDownClass(cls):
        cls.delete_test_file(cls)
        cls.delete_container_in_cloud(cls)

    def create_test_file(self):
        try:
            os.mknod(str(os.path.expanduser("~")) + "/test_regressiontest.bag")
        except FileExistsError:
            pass

    def delete_test_file(self):
        try:
            os.remove(
                str(os.path.expanduser("~")) + "/test_regressiontest.bag")
        except FileNotFoundError:
            pass

    def create_file_in_cloud(self):
        self.block_blob_service.create_container(self.random_date)
        self.block_blob_service.create_blob_from_path(
            self.random_date, "test_regressiontest.bag",
            str(os.path.expanduser("~")) + "/test_regressiontest.bag")

    def delete_container_in_cloud(self):
        while self.block_blob_service.exists(self.random_date):
            self.block_blob_service.delete_container(self.random_date)

    #######################################START OF TEST CASES##########################################################
    def test_file_check_on_prem_file_exists(self):
        os.environ["DANF_ENV"] = "sv_prod"
        self.file_check_handler = FileCheckHandler()
        logger.info("Start test test_file_check_on_prem_file_exists.")
        res = self.file_check_handler.check_if_file_exist(
            [str(os.path.expanduser("~")) + "/test_regressiontest.bag"])
        res[0]["timestamp"] = datetime.datetime(2019, 10, 22, 7, 12, 50, 24777)
        self.assertCountEqual(res, res_on_prem)

    def test_file_check_on_prem_file_not_exists(self):
        os.environ["DANF_ENV"] = "sv_prod"
        self.file_check_handler = FileCheckHandler()
        logger.info("Start test test_file_check_on_prem_file_not_exists.")
        self.delete_test_file()
        res = self.file_check_handler.check_if_file_exist(
            ["/mapr/624.mbc.us/data/input/file.bag"])
        res[0]["timestamp"] = datetime.datetime(2019, 10, 22, 7, 12, 50, 24777)
        res_on_prem[0]["exists"] = False
        res_on_prem[0]["size"] = None
        res_on_prem[0]["path"] = "/mapr/data/input/file.bag"
        self.assertCountEqual(res, res_on_prem)
        self.create_test_file()

    def test_file_check_in_cloud_file_exists(self):
        os.environ["DANF_ENV"] = "dev"
        self.file_check_handler = FileCheckHandler()
        logger.info("Start test test_file_check_in_cloud_file_exists.")
        res = self.file_check_handler.check_if_file_exist([
            f"/dlastorage1sadev/{self.random_date}/test_regressiontest.bag"
        ])
        res_cloud_blob["timestamp"] = res[0]["timestamp"]
        self.assertCountEqual(res_cloud_blob, res[0])

    def test_file_check_in_cloud_file_not_exists(self):
        os.environ["DANF_ENV"] = "dev"
        self.file_check_handler = FileCheckHandler()
        logger.info("Start test test_file_check_in_cloud_file_not_exists.")
        res = self.file_check_handler.check_if_file_exist([
            f"/dlastorage1sadev/blobServices/default/containers/{self.random_date}/blobs/test_regressiontest.bag"
        ])
        self.assertCountEqual(res_cloud_blob, res[0])


if __name__ == '__main__':
    unittest.main(warnings='ignore')