#!/bin/sh
#This script writes the bandwith measured  to a file
#it checks te nic satistics recievec bytes evere seconds and approximates the
# bandwith in bytes / second
T=$1
INTERFACE=$2
FILENAME="bandwidthsystem_${INTERFACE}_$T.log"

old="$(</sys/class/net/$INTERFACE/statistics/rx_bytes)";
while $(sleep 1);
do
now=$(</sys/class/net/$INTERFACE/statistics/rx_bytes);
echo $((($now-$old))) >> $FILENAME;
old=$now;
done
