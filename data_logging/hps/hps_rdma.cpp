#include <thread>
#include <mutex>
#include <atomic>
#include <rdma/rdma_verbs.h>
#include "common.h"
#include "hps_rdma_common.h"
#include "hps.h"

using namespace std;

struct hpRdmaStreamT
{
	hps_type type = HPS_RDMA;

	char *ip = NULL;
	int port;
	thread eventHandlerThread;
	thread sendDispatcherThread;
	mutex mtx;

	struct rdma_event_channel *ec = NULL;
	struct ibv_pd *pd = NULL;
	struct ibv_comp_channel *channel = NULL;
	struct ibv_cq *cq = NULL;
	thread cqPollThread;

	struct rdma_cm_id *id = NULL;

	// Local Queue Control Block
	struct queueControlBlock *LQCB = NULL;
	struct ibv_mr *LQCB_mr = NULL;
	char *buffer = NULL;
	volatile long long rLast = 0;
	volatile uint64_t cTimestamp = 0;
	struct ibv_mr *buffer_mr = NULL;

	struct message *msg = NULL;
	struct ibv_mr *msg_mr = NULL;

	remoteBufferInfo remoteBuffer;
	struct queueControlBlock *RQCB = NULL;
	uint64_t remoteDataBuffer = 0;

	atomic<int> txCount; // Number of sent packets
	atomic<int> cqPendingCount; // Number of pending (expected) CQ events
	volatile int ready = 0;

	hpRdmaStreamT(const char *ip, int port)
	{
		this->ip = strdup(ip);
		this->port = port;

		cqPendingCount = 0;

		// Connect
		TEST_Z(ec = rdma_create_event_channel());
		TEST_NZ(rdma_create_id(ec, &id, NULL, RDMA_PS_TCP));

		// Associate
		this->id = id;
		id->context = this;

		struct rdma_cm_event *event;

		// Resolve address
		{
			struct addrinfo *addr = NULL;
			char port_str[32];
			memset(port_str, 0, sizeof(port_str));
			sprintf(port_str, "%d", port);
			TEST_NZ(getaddrinfo(ip, port_str, NULL, &addr));
			TEST_NZ(rdma_resolve_addr(id, NULL, addr->ai_addr, RDMA_TIMEOUT_MS));
			freeaddrinfo(addr);
		}
		TEST_NZ(rdma_get_cm_event(ec, &event));
		if (event->event != RDMA_CM_EVENT_ADDR_RESOLVED)
		{
			printf("rdma_resolve_addr failed\n");
			rdma_ack_cm_event(event);
			throw exception();
		}
		rdma_ack_cm_event(event);

		// Build connection
		{
			ibv_device_attr dev_attr;
			TEST_E(ibv_query_device(id->verbs, &dev_attr));

			// Create CQ
			TEST_Z(pd = ibv_alloc_pd(id->verbs));
			TEST_Z(channel = ibv_create_comp_channel(id->verbs));
			TEST_Z(cq = ibv_create_cq(id->verbs, RDMA_MIN_CQE, NULL, channel, 0));
			TEST_NZ(ibv_req_notify_cq(cq, 0));

			// Create QP
			struct ibv_qp_init_attr qp_attr;
			memset(&qp_attr, 0, sizeof(qp_attr));
			qp_attr.send_cq = cq;
			qp_attr.recv_cq = cq;
			qp_attr.qp_type = IBV_QPT_RC;
			qp_attr.cap.max_send_wr = MIN(RDMA_CLIENT_MAX_SEND_WR, dev_attr.max_qp_wr);
			qp_attr.cap.max_recv_wr = MIN(RDMA_CLIENT_MAX_RECV_WR, dev_attr.max_qp_wr);
			qp_attr.cap.max_send_sge = MIN(1, dev_attr.max_sge);
			qp_attr.cap.max_recv_sge = MIN(1, dev_attr.max_sge);
			TEST_NZ(rdma_create_qp(id, pd, &qp_attr));

			// Allocate LQCB
			TEST_NZ(posix_memalign((void **)&LQCB, sysconf(_SC_PAGESIZE), sizeof(*LQCB)));
			memset(LQCB, 0, sizeof(*LQCB));
			TEST_Z(LQCB_mr = ibv_reg_mr(pd, LQCB, sizeof(*LQCB),
					IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_REMOTE_READ));

			// Allocate send buffer
			// TODO: fix this
			size_t bufferSize = RDMA_BUFFER_SIZE + 64 * 1024 * 1024;
			TEST_NZ(posix_memalign((void **)&buffer, sysconf(_SC_PAGESIZE), bufferSize));
			memset(buffer, 0, RDMA_BUFFER_SIZE);
			TEST_Z(buffer_mr = ibv_reg_mr(pd, buffer, RDMA_BUFFER_SIZE, IBV_ACCESS_LOCAL_WRITE));

			// Allocate message buffer
			TEST_NZ(posix_memalign((void **)&msg, sysconf(_SC_PAGESIZE), sizeof(*msg)));
			TEST_Z(msg_mr = ibv_reg_mr(pd, msg, sizeof(*msg), IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE));

			post_recv();

			// Start CQ poll thread
			cqPollThread = thread(&hpRdmaStreamT::poll_cq, this);
		}

		// Resolve route
		TEST_NZ(rdma_resolve_route(id, RDMA_TIMEOUT_MS));
		TEST_NZ(rdma_get_cm_event(ec, &event));
		if (event->event != RDMA_CM_EVENT_ROUTE_RESOLVED)
		{
			printf("rdma_resolve_route failed\n");
			rdma_ack_cm_event(event);
			throw exception();
		}
		rdma_ack_cm_event(event);

		// Connect
		struct rdma_conn_param cm_params;
		memset(&cm_params, 0, sizeof(cm_params));
		cm_params.initiator_depth = 1;
		cm_params.responder_resources = 1;
		cm_params.retry_count = 3; // 7 = infinite
		cm_params.rnr_retry_count = 3; // 7 = infinite
		TEST_NZ(rdma_connect(id, &cm_params));
		TEST_NZ(rdma_get_cm_event(ec, &event));
		if (event->event != RDMA_CM_EVENT_ESTABLISHED)
		{
			printf("rdma_connect failed\n");
			rdma_ack_cm_event(event);
			throw exception();
		}
		rdma_ack_cm_event(event);

		// Start event handler
		eventHandlerThread = thread(&hpRdmaStreamT::cmEventHandler, this);
		sendDispatcherThread = thread(&hpRdmaStreamT::sendDispatcher, this);
	}

	~hpRdmaStreamT()
	{
		size_t rem = LQCB->w - LQCB->r;
		if (0 < LQCB->r)
		{
			post_write(LQCB->r % RDMA_BUFFER_SIZE, rem, NULL);
			LQCB->r += rem;
		}

		post_w();

		cqPollThread.join();

		if (ec)
			rdma_destroy_event_channel(ec);

		if (ip)
			free(ip);
	}

	void cmEventHandler()
	{
		struct rdma_cm_event *event = NULL;
		while (rdma_get_cm_event(ec, &event) == 0)
		{
			struct rdma_cm_event e;
			memcpy(&e, event, sizeof(*event));
			rdma_ack_cm_event(event);

			if (e.event == RDMA_CM_EVENT_DISCONNECTED)
			{
				rdma_destroy_qp(e.id);
				rdma_destroy_id(e.id);
				break;
			}
			else
			{
				printf("Unknown CM event 0x%08X\n", e.event);
				break;
			}
		}
	}

	int post_recv()
	{
		struct ibv_recv_wr wr, *bad_wr = NULL;
		struct ibv_sge sge;

		wr.wr_id = (uintptr_t)id;
		wr.next = NULL;
		wr.sg_list = &sge;
		wr.num_sge = 1;

		sge.addr = (uintptr_t)msg;
		sge.length = sizeof(*msg);
		sge.lkey = msg_mr->lkey;

		return ibv_post_recv(id->qp, &wr, &bad_wr);
	}

	int post_write(uint64_t offset, uint32_t size, uint32_t *imm)
	{
		struct ibv_send_wr wr, *bad_wr = NULL;
		struct ibv_sge sge;

		memset(&wr, 0, sizeof(wr));

		wr.wr_id = (uintptr_t)offset;
		wr.opcode = imm ? IBV_WR_RDMA_WRITE_WITH_IMM : IBV_WR_RDMA_WRITE;
		if (99999999 < txCount)
		{
			wr.send_flags = IBV_SEND_SIGNALED; //IBV_SEND_SIGNALED;
			txCount = 0;
		}
		wr.imm_data = imm ? *imm : 0;
		wr.wr.rdma.remote_addr = remoteDataBuffer + offset;
		wr.wr.rdma.rkey = remoteBuffer.rkey;

		if (size)
		{
			wr.sg_list = &sge;
			wr.num_sge = 1;

			sge.addr = (uintptr_t)buffer + offset;
			sge.length = size;
			sge.lkey = buffer_mr->lkey;
		}

		int e = ibv_post_send(id->qp, &wr, &bad_wr);
		if ((wr.send_flags & IBV_SEND_SIGNALED) && (e == 0))
		{
			cqPendingCount++;
			txCount = 0;
		}
		else
			txCount++;
		return e;
	}

	int post_w()
	{
		struct ibv_send_wr wr, *bad_wr = NULL;
		struct ibv_sge sge;

		memset(&wr, 0, sizeof(wr));

		wr.wr_id = (uintptr_t)-1;
		wr.opcode = IBV_WR_RDMA_WRITE;
		wr.send_flags = IBV_SEND_SIGNALED; // IBV_SEND_INLINE | IBV_SEND_SIGNALED
		wr.imm_data = 0;
		wr.wr.rdma.remote_addr = remoteBuffer.addr + offsetof(queueControlBlock, w);
		wr.wr.rdma.rkey = remoteBuffer.rkey;

		wr.sg_list = &sge;
		wr.num_sge = 1;

		sge.addr = (uint64_t)&LQCB->r;
		sge.length = sizeof(long long);
		sge.lkey = LQCB_mr->lkey;

		int e = ibv_post_send(id->qp, &wr, &bad_wr);
		if ((wr.send_flags & IBV_SEND_SIGNALED) && (e == 0))
		{
			cqPendingCount++;
			txCount = 0;
		}
		else
			txCount++;
		return e;
	}

	int rdma_read_c()
	{
		struct ibv_send_wr wr, *bad_wr = NULL;
		struct ibv_sge sge;

		memset(&wr, 0, sizeof(wr));

		wr.wr_id = (uintptr_t)-1;
		wr.opcode = IBV_WR_RDMA_READ;
		wr.send_flags = IBV_SEND_SIGNALED; // IBV_SEND_INLINE | IBV_SEND_SIGNALED
		wr.imm_data = 0;
		wr.wr.rdma.remote_addr = (uint64_t)&RQCB->c;
		wr.wr.rdma.rkey = remoteBuffer.rkey;

		wr.sg_list = &sge;
		wr.num_sge = 1;

		sge.addr = (uint64_t)&LQCB->c;
		sge.length = sizeof(LQCB->c);
		sge.lkey = LQCB_mr->lkey;

		int e = ibv_post_send(id->qp, &wr, &bad_wr);
		if ((wr.send_flags & IBV_SEND_SIGNALED) && (e == 0))
		{
			cqPendingCount++;
			txCount = 0;
		}
		else
			txCount++;
		return e;
	}

	int write(void *data, size_t size)
	{
		while (!ready)
			usleep(10000);

		uint8_t* src = (uint8_t*)data;
		size_t rem = size;
		while (0 < rem)
		{
			long long w = LQCB->w;

			// Compute space
			size_t space = LQCB->c + RDMA_BUFFER_SIZE - w;

			size_t n = MIN(rem, space);
			n = MIN(n, RDMA_BUFFER_SIZE - (w % RDMA_BUFFER_SIZE));

			// Copy chunk into send buffer
			if (0 < n)
			{
				memcpy(buffer + (w % RDMA_BUFFER_SIZE), src, n);
				src += n;
				rem -= n;
				LQCB->w += n;
			}
			else
			{
				usleep(1000);
			}
		}

		return size - rem;
	}

	void sendDispatcher()
	{
		while (!ready)
			usleep(10000);

		while (1)
		{
			int cnt = 0;

			if (RDMA_MIN_CQE <= cqPendingCount)
			{
				usleep(1000);
				continue;
			}

			// When less than half of the buffer space left
			if ((RDMA_BUFFER_SIZE / 2) < (LQCB->w - LQCB->c))
			{
				// Receive c
				uint64_t t = getTimestampNs();
				if (RDMA_CLIENT_GET_C_TO_MS * 1000000ull < t - cTimestamp)
				{
					int e = rdma_read_c();
					if (e == 0)
					{
						cTimestamp = t;
						cnt++;
					}
					else
					{
						usleep(1000);
						continue;
					}
				}
			}

			// Send w
			if (RDMA_CLIENT_SR_THRESHOLD <= (LQCB->r - rLast))
			{
				int e = post_w();
				if (e == 0)
				{
					rLast = LQCB->r;
					cnt++;
				}
				else
				{
					usleep(1000);
					continue;
				}
			}

			if (64 <= cqPendingCount)
			{
				usleep(1000);
				continue;
			}

			// Write block
			if (RDMA_SEND_BLOCK_SIZE <= LQCB->w - LQCB->r)
			{
				int offset = LQCB->r % RDMA_BUFFER_SIZE;

				// Post block write
				int e = post_write(offset, RDMA_SEND_BLOCK_SIZE, NULL);
				if (e == 0)
				{
					LQCB->r += RDMA_SEND_BLOCK_SIZE;
					cnt++;
				}
				else
				{
					usleep(1000);
					continue;
				}
			}

			if (!cnt)
			{
				usleep(1000);
				fflush(stdout);
			}
		}
	}

	void on_completion(struct ibv_wc *wc)
	{
		if (wc->opcode & IBV_WC_RECV)
		{
			if (msg->id == MSG_MR)
			{
				memcpy(&remoteBuffer, &msg->data.mr, sizeof(remoteBuffer));
				RQCB = (struct queueControlBlock *)remoteBuffer.addr;
				remoteDataBuffer = remoteBuffer.addr + RDMA_QCB_SIZE;
				ready = 1;
				post_recv();
			}
			else if (msg->id == MSG_UPDATE_C)
			{
				LQCB->c = msg->data.update.c;
				post_recv();
			}
			else if (msg->id == MSG_DONE)
			{
				printf("Disconnecting...\n");
				rdma_disconnect(id);
				ready = 0;
			}
		}
		else if (wc->opcode == IBV_WC_RDMA_WRITE)
		{
			cqPendingCount--;
			if (wc->status != IBV_WC_SUCCESS)
			{
				printf("IBV_WC_RDMA_WRITE failed with (0x%08X)\n", wc->status);
			}
		}
		else if (wc->opcode == IBV_WC_RDMA_READ)
		{
			cqPendingCount--;
			if (wc->status != IBV_WC_SUCCESS)
			{
				printf("IBV_WC_RDMA_READ failed with (0x%08X)\n", wc->status);
			}
		}
		else
		{
			printf("Unexpected WC opcode 0x%08X\n", wc->opcode);
			throw exception();
		}
	}

	void poll_cq()
	{
		using namespace std;

		struct ibv_cq *ev_cq;
		void *ev_ctx;
		struct ibv_wc wc[RDMA_POLL_CQ_BATCH_SIZE];

		try
		{
			while (1)
			{
				TEST_NZ(ibv_get_cq_event(channel, &ev_cq, &ev_ctx));
				assert(ev_cq == cq);
				ibv_ack_cq_events(ev_cq, 1);
				TEST_NZ(ibv_req_notify_cq(ev_cq, 0));

				int n = ibv_poll_cq(ev_cq, RDMA_POLL_CQ_BATCH_SIZE, wc);
				if (n < 0)
				{
					printf("ibv_poll_cq failed (%d)\n", n);
					break;
				}
				for (int i = 0; i < n; i++)
				{
					on_completion(&wc[i]);
				}
			}
		}
		catch (exception&)
		{
		}
	}
};

/******************************************************************************
 * Public API
 *****************************************************************************/

hpRdmaStreamT *hps_rdma_open(const char *ip, int port)
{
	try
	{
		return new hpRdmaStreamT(ip, port);
	}
	catch (exception&)
	{
		return NULL;
	}
}

int hps_rdma_write_begin(hpRdmaStreamT *s, void **buffer, unsigned size)
{
	if (s == NULL || buffer == NULL || RDMA_BUFFER_SIZE < size)
		return -1;

	while (!s->ready)
		usleep(10000);

	long long space;
	while (1)
	{
		// Check free space
		space = s->LQCB->c + RDMA_BUFFER_SIZE - s->LQCB->w;
		if (size <= space)
			break;
		usleep(1000);
	}

	*buffer = (void*)((uintptr_t)s->buffer + (s->LQCB->w % RDMA_BUFFER_SIZE));

	return size ? size : space;
}

int hps_rdma_write_end(hpRdmaStreamT *s, unsigned size)
{
	if (s == NULL)
		return -1;

	while (!s->ready)
		usleep(10000);

	s->LQCB->w += size;

	statistics.writeBytes += size;

	return 0;
}

int hps_rdma_writev(hpRdmaStreamT *s, const struct iovec *iov, int count)
{
	size_t size = 0;

	for (int i = 0; i < count; i++)
	{
		size_t n = s->write(iov[i].iov_base, iov[i].iov_len);
		if (0 <= n)
		{
			size += n;
		}
		else
		{
			break;
		}
	}

	statistics.txBytes += size;

	return size;
}

int hps_rdma_close(hpRdmaStreamT *s)
{
	try
	{
		delete s;
		return 0;
	}
	catch (exception&)
	{
		return -1;
	}
}
