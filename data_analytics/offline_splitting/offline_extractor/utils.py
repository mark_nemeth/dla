"""Utils"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


def charlist2str(chrlist):
    """Converts list of integers into a string assuming the integers represent
    chars"""
    list_of_char = [chr(x) for x in chrlist]
    string = ''.join(list_of_char).rstrip('\0')
    return string


def rospytime2int(timestamp):
    """Converts rospy time to int (in ms)"""
    return int(str(timestamp)[0:13])


def timestamp2ms(timestamp):
    """Converts timestamp to milliseconds (int)"""
    return int(str(timestamp).replace(".", "")[0:13])


def timestamp2s(timestamp):
    """Converts timestamp to seconds (int)"""
    return int(str(timestamp).replace(".", "")[0:10])
