import React, { useEffect } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import { MapContainer, TileLayer, Popup, Polyline, CircleMarker } from 'react-leaflet'
import './MapDetail.scss'

const lineoptions = { color: 'red' }
const markeroptions = { color: 'blue', fillColor: 'red', fillOpacity: 1 }

function plotMap(points, guid) {
    let arr = points
    let sortedarr = [...arr].sort()
    let arrlen = points.length
    let outerBounds = [sortedarr[0], sortedarr[arrlen - 1]]
    if (arrlen > 0 && arr[0] !== 'Error' && arr[0] !== 'Null') {
        return (
            <MapContainer bounds={outerBounds} scrollWheelZoom={true}>

                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <CircleMarker center={arr[0]} radius={5} pathOptions={markeroptions}>
                    <Popup>
                        Start Position
                    </Popup>
                </CircleMarker>
                <CircleMarker center={arr[arrlen - 1]} radius={5} pathOptions={markeroptions}>
                    <Popup>
                        End Position
                  </Popup>
                </CircleMarker>
                <Polyline pathOptions={lineoptions} positions={arr} />
            </MapContainer>
        )

    }
    else if (arr[0] === 'Error') {
        return <div>Network Error Occured While Fetching GPS Data</div>
    }

    else if (arr[0] === 'Null') {
        return <div>No Gps data available for this '{guid}' Bag File</div>
    }
    else {
        return (
            <div className="loading-spinner-container">
                <Spinner animation="border" variant="info" />
            </div>)
    }

}

export function MapDetail(props) {
    useEffect(() => {
        props.getGPSdata(props.guid)
        return function cleanup() {
            props.cleanMapComponentData()
        };
    }, []);
    return (
        <div className="map-detail" id="map-id">
            {
                plotMap(props.points, props.guid)
            }

        </div>
    )

}

export default MapDetail