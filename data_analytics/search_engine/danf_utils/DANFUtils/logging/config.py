__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os
import time
from enum import Enum
from typing import Union, Optional

from opencensus.ext.azure.log_exporter import AzureLogHandler
from opencensus.trace import config_integration
from pythonjsonlogger import jsonlogger

from DANFUtils.tracing.tracing import APPINSIGHTS_DISABLE_KEY


class LogStructure(Enum):
    PLAIN_TEXT = 'PLAIN_TEXT'
    JSON = 'JSON'


class UTCFormatter(logging.Formatter):
    """Custom formatter that logs UTC time"""
    converter = time.gmtime


class UTCJsonFormatter(jsonlogger.JsonFormatter):
    """Custom json formatter that logs UTC time"""
    converter = time.gmtime


class TracingFilter(logging.Filter):
    """Replaces missing traceId in log record with None to avoid exceptions"""
    def filter(self, record):
        if not hasattr(record, 'traceId'):
            record.traceId = None
        return True


PLAIN_TEXT_FORMATTER = UTCFormatter(
    '%(levelname)s [%(asctime)s] [%(name)s] [%(traceId)s] - %(message)s')

JSON_FORMATTER = UTCJsonFormatter(
    '(levelname) (asctime) (traceId) (name) (message)')


# configure open census integration for logging in order to make trace and
# span ids available in log messages
config_integration.trace_integrations(['logging'])


def configure_root_logger(
        log_level: Union[str, int] = logging.INFO,
        log_structure: LogStructure = LogStructure.PLAIN_TEXT,
        file_path: Optional[str] = None,
        enable_azure_log_aggregation: bool = False):
    """Applies the DANF specific default configuration to the root logger
    of the python logging library. The default configuration always includes
    a console handler and may include additional handlers.

    In most cases, applications should just modify the log levels of their
    logger hierarchy and use the handlers and formatters that are attached to
    the root logger by this function. This will ensure that all logs are
    properly shipped and follow a common format.

    :param log_level: Log level to set for the root logger. Value must be an
        str or int and valid according to the specification of
        logging#setLevel() (default: INFO).
    :param log_structure: Defines the output format of the log messages.
        Supported values: PLAIN_TEXT (log plain string), JSON (log json
        objects).
    :param file_path: Path to the log file. File logging is activated only if
        this parameter is given.
    :param enable_azure_log_aggregation: If true, an additional handler will be
        attached that ships logs to azure application insights. Additionally,
        the APPINSIGHTS_INSTRUMENTATIONKEY env variable has to be set.
    :return:
    """
    root_logger = logging.getLogger()
    root_logger.handlers = []  # remove existing handlers
    root_logger.setLevel(log_level)

    # configure console handler
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.addFilter(TracingFilter())
    if log_structure == LogStructure.PLAIN_TEXT:
        cf = PLAIN_TEXT_FORMATTER
    elif log_structure == LogStructure.JSON:
        cf = JSON_FORMATTER
    else:
        raise ValueError(f"Unexpected log structure: {log_structure}")
    ch.setFormatter(cf)
    root_logger.addHandler(ch)

    # configure log aggregation in azure
    appinsights_key = os.environ.get('APPINSIGHTS_INSTRUMENTATIONKEY')
    if (enable_azure_log_aggregation
            and appinsights_key
            and appinsights_key != APPINSIGHTS_DISABLE_KEY):
        ah = AzureLogHandler(
            connection_string=f'InstrumentationKey={appinsights_key}')
        ah.setFormatter(logging.Formatter('%(message)s'))
        root_logger.addHandler(ah)

    # configure file handler if file path is given
    if file_path:
        fh = logging.FileHandler(file_path)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(PLAIN_TEXT_FORMATTER)
        root_logger.addHandler(fh)

    # configure log level for third party libraries

    # prevent endless log loops by not logging 'Transmission succeeded' for
    # logs that have been sent to Azure LogAnalytics
    logging.getLogger('opencensus.ext.azure.common').setLevel(logging.ERROR)
