__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from requests.auth import HTTPBasicAuth
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session


def create_session(token_url="", client_credentials_id="", client_credentials_secret="", audience=""):
    auth = HTTPBasicAuth(client_credentials_id, client_credentials_secret)
    client = BackendApplicationClient(client_id=client_credentials_id)
    oauth_session = OAuth2Session(client=client)
    oauth_session.fetch_token(token_url=token_url, auth=auth, audience=audience)
    return oauth_session
