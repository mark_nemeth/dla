__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from locust import TaskSet, task, HttpLocust


class ApiClientBehavior(TaskSet):
    @task
    def query_bagfile_by_guid(self):
        query = {'guid': 'e8156b0d-c2f1-4754-b557-678cd582451d'}
        self.client.get('/search/bagfile', params=query)

    @task
    def query_bagfile_by_file_path(self):
        query = {'file_path': 'xxx'}
        self.client.get('/search/bagfile', params=query)

    @task
    def query_bagfiles_unsorted(self):
        query = {}
        self.client.get('/search/bagfile', params=query)

    @task
    def query_bagfiles_sort_by_guid(self):
        query = {'sortby': 'guid'}
        self.client.get('/search/bagfile', params=query)

    @task
    def query_bagfiles_sort_by_start(self):
        query = {'sortby': 'start'}
        self.client.get('/search/bagfile', params=query)

    @task
    def query_bagfiles_sort_by_start_with_offset_and_large_limit(self):
        query = {
            'sortby': 'start',
            'offset': 500,
            'limit': 10000
        }
        self.client.get('/search/bagfile', params=query)

    @task
    def get_childbagfiles_by_parent_guid(self):
        query = {'parent_guid': 'e8156b0d-c2f1-4754-b557-678cd582451d'}
        self.client.get('/search/childbagfile', params=query)

    @task
    def get_childbagfiles_unsorted(self):
        query = {}
        self.client.get('/search/childbagfile', params=query)

    @task
    def get_childbagfiles_sorted_by_guid(self):
        query = {'sortby': 'guid'}
        self.client.get('/search/childbagfile', params=query)

    @task
    def query_childbagfiles_by_bva(self):
        query = {'bva_min': 5}
        self.client.post('/search/childbagfile', json=query)

    @task
    def query_childbagfiles_by_parent_guid(self):
        query = {'parent_guid': 'e8156b0d-c2f1-4754-b557-678cd582451d'}
        self.client.post('/search/childbagfile', json=query)

    @task
    def query_childbagfiles_by_file_path(self):
        query = {'file_path': 'xxx'}
        self.client.post('/search/childbagfile', json=query)


class ApiUser(HttpLocust):
    host = 'http://danf-search-api-dev.westeurope.cloudapp.azure.com:8889'
    task_set = ApiClientBehavior

