import React, { Component } from 'react';
import { mapStyleNight } from './MapStyles/mapStyleNight';
import { ColorScheme, ScaleTheme } from './Models/RouteModels';
import configHandler from '../../config/configHandler';
import './LeafLetMap.scss';

let LeafLetMap_var = window.H;

class LeafLetMap extends Component {
  constructor(props) {
    super(props);
    
    // set marker icons here in order to improve performance
    let svgMarkerDefault = `<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"><circle cx="10" cy="10" r="5" stroke="black" stroke-width="1" fill="${ColorScheme.colorEventDefault}" /></svg>`
    let svgMarkerHover = `<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"><circle cx="10" cy="10" r="5" stroke="black" stroke-width="1" fill="${ColorScheme.colorEventHover}" /></svg>`
    let svgMarkerSelected = `<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"><circle cx="10" cy="10" r="5" stroke="black" stroke-width="1" fill="${ColorScheme.colorEventSelected}" /></svg>`
    let svgMarkerBorder = `<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"><circle cx="10" cy="10" r="5" stroke="black" stroke-width="1" fill="black" /></svg>`

    // marker icon types
    this.markerIconDefault = new LeafLetMap_var.map.Icon(svgMarkerDefault, { anchor: new LeafLetMap_var.math.Point(10, 10) });
    this.markerIconHover = new LeafLetMap_var.map.Icon(svgMarkerHover, { anchor: new LeafLetMap_var.math.Point(10, 10) });
    this.markerIconSelected = new LeafLetMap_var.map.Icon(svgMarkerSelected, { anchor: new LeafLetMap_var.math.Point(10, 10) });
    this.markerIconBorder = new LeafLetMap_var.map.Icon(svgMarkerBorder, { anchor: new LeafLetMap_var.math.Point(10, 10) });

    this.selectedElement = null;
    this.allGroupsBb = null;
    this.routeColor = ColorScheme.colorChildBagfileDefault;

    this.state = {
      isLoaded: false,
      selectedElement: null,
      width: window.innerWidth,
      height: window.innerHeight,
    }
  }

  updateDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  };

  componentDidMount() {
    // create map with platform, layers, style,...

    this.platform = new LeafLetMap_var.service.Platform({
      apikey: configHandler.getLeafLetMapApiKey().ApiKey
    });

    this.defaultLayers = this.platform.createDefaultLayers();
    this.map = new LeafLetMap_var.Map(
      document.getElementById('leaflet-map-element'),
      this.defaultLayers.vector.normal.map, {
      center: {
        lat: this.props.lat,
        lng: this.props.lng,
      },
      zoom: this.props.zoom,
      pixelRatio: window.devicePixelRatio || 1,
    });

    this.map.addEventListener('tap', (evt) => {
      // reset old style to default
      if (this.selectedElement) {
        let selectedGeo = this.selectedElement.getGeometry();
        if (!selectedGeo.lat) {
          this.selectedElement.setStyle(this.selectedElement.getStyle().getCopy({ strokeColor: ColorScheme.colorChildBagfileDefault }));
        }
      }
      // this.props.onSelection(null); // TODO: implement redux state
      this.selectedElement = null;
    }, true);

    // set custom map style
    let baseLayer = this.map.getBaseLayer();
    let provider = baseLayer.getProvider();
    const nightStyle = new LeafLetMap_var.map.Style(mapStyleNight);
    provider.setStyle(nightStyle);

    window.addEventListener('resize', this.updateDimensions);

    this.ui = LeafLetMap_var.ui.UI.createDefault(this.map, this.defaultLayers);

    this.setState({ isLoaded: true }, this.setRoute(this.props.route));
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  componentDidUpdate(prevProps) {
    if (this.props.route.length > 0) {
      this.setRoute(this.props.route);
    }
  }

  setRoute(route) {
    // Note: I had to make the destruction so complicated... TODO: Find better solution
    while (true) {
      let mapObjects = this.map.getObjects();
      if (mapObjects.length < 1) {
        break;
      }
      this.map.removeObject(mapObjects[0]);
    }
    this.createRoute();
  }

  createRoute() {
    // create array of all groups, in order to center map to them
    let allGroups = [];

    if (!this.props.route) {
      return;
    }

    // create childBagfiles, add start and end marker for each childBagfile and the entire route
    let childBagfiles = this.props.route;
    // if (this.props.route && this.props.route instanceof RouteData) { } else { console.log('exiting'); return; }
    if (childBagfiles.length) {
      for (let idx = 0; idx < childBagfiles.length; idx++) {
        let childBagfile = childBagfiles[idx];
        let group = new LeafLetMap_var.map.Group();
        allGroups.push(group);
        let lineString = new LeafLetMap_var.geo.LineString();

        // add events
        group.addEventListener('tap', (evt) => {
          // reset old style to default
          if (this.selectedElement) {
            let selectedGeo = this.selectedElement.getGeometry();
            if (!selectedGeo.lat) {
              this.selectedElement.setStyle(this.selectedElement.getStyle().getCopy({ strokeColor: ColorScheme.colorChildBagfileDefault }));
            }
          }

          this.props.highlightSelectedChildBagfile(childBagfile.guid);
          this.props.highlightSelectedEvent([])
          // save new selection
          this.selectedElement = evt.target;
          this.centerMapAtObject(evt);
          this.props.onChildBagfileClick();

          // style selected element
          let geo = evt.target.getGeometry();
          if (!geo.lat) {
            evt.target.setStyle(evt.target.getStyle().getCopy({ strokeColor: ColorScheme.colorChildBagfileSelected }));
          }
        }, true);

        group.addEventListener('pointerenter', (evt) => {
          this.props.onChildBagfileHover();
          if (this.selectedElement !== evt.target) {
            let geo = evt.target.getGeometry();
            if (!geo.lat) {
              this.routeColor = evt.target.style.strokeColor
              evt.target.setStyle(evt.target.getStyle().getCopy({ strokeColor: ColorScheme.colorChildBagfileHover }));
            }
          }
        }, true);

        group.addEventListener('pointerleave', (evt) => {
          if (this.selectedElement !== evt.target) {
            let geo = evt.target.getGeometry();
            if (!geo.lat) {
              evt.target.setStyle(evt.target.getStyle().getCopy({ strokeColor: this.routeColor }));
            }
          }
        }, true);

      // create childBagfiles for different data structure
      if (childBagfile.gps_track) {
        for (let idx2 = 0; idx2 < childBagfile.gps_track.coordinates.length; idx2++) {
          let el = childBagfile.gps_track.coordinates[idx2];
          let lat = el[1];
          let lng = el[0];
          lineString.pushPoint({ lat: lat, lng: lng });
        }
      } else {
        for (let idx2 = 0; idx2 < childBagfile.metadata.ext1_3.gps.length; idx2++) {
          let el = childBagfile.metadata.ext1_3.gps[idx2];
          let lat = el.latitude;
          let lng = el.longitude;
          lineString.pushPoint({ lat: lat, lng: lng });
        }
      }

        let len = childBagfile.gps_track ? childBagfile.gps_track.coordinates.length: childBagfile.metadata.ext1_3.gps.length;
        if ((len !== null || len !== 'undefined') && len > 0) {
          // add lines to map
          let polyline;
          if ( (!this.props.selectedEvent || this.props.selectedEvent.length === 0) && (this.props.selectedChildbagfileGuid === childBagfile.guid)) {
            polyline = new LeafLetMap_var.map.Polyline(
              lineString, {
              style: {
                lineWidth: ScaleTheme.sizeXS,
                strokeColor: ColorScheme.colorChildBagfileSelected
              }
            })
          }
          else {
            polyline = new LeafLetMap_var.map.Polyline(
              lineString, {
              style: {
                lineWidth: ScaleTheme.sizeXS,
                strokeColor: ColorScheme.colorChildBagfileDefault
              }
            })
          }
          group.addObject(polyline);
          this.map.addObject(group);
        }
      }
    }
    
    // To get the childbagfile name from the event selected
    const getChildbagfileFromCoordinates = (coordinates) => {
      let childBagfiles = this.props.route;
      for (let idx=0; idx<childBagfiles.length; idx++) {
        let events = Object.entries(childBagfiles[idx].metadata);
        for (let idx2=0; idx2<events.length; idx2++) {
          let lat = Object.entries(events[idx2][1])[0][1][0].latitude;
          let lng = Object.entries(events[idx2][1])[0][1][0].longitude;
          if( coordinates[0] === lat && coordinates[1] === lng) {
            return childBagfiles[idx].file_name;
          }
        }
    }
    return null;
  }

    // create event markers
    let allEvents = this.props.route.getEventsPerChildBagfile();
    let isInvalidPoints = false; //Check for valid co-ordinates. Can be removed once the data structure is stable
    if (allEvents.length) {
        for (let events of allEvents) {
            let group = new LeafLetMap_var.map.Group();
            allGroups.push(group);

            // create group of markers and connecting lines and add to map, add event listener
            group.addEventListener('tap', (evt) => {
                if (this.selectedElement) {
                    let selectedGeo = this.selectedElement.getGeometry();
                    if (selectedGeo.lat) {
                        this.selectedElement.setIcon(this.markerIconDefault);
                    }
                }
                this.centerMapAtObject(evt);
                let geo = evt.target.getGeometry();
                this.props.highlightSelectedEvent([geo.lat,geo.lng]);
                const childBagfile = getChildbagfileFromCoordinates([geo.lat,geo.lng])
                if (childBagfile) this.props.highlightSelectedChildBagfile(childBagfile.guid);
                // save new selection
                this.selectedElement = evt.target;

                // style selected element
                if (geo.lat) {
                    evt.target.setIcon(this.markerIconSelected);
                }
            }, false);

            group.addEventListener('pointerenter', (evt) => {
                this.props.onEventHover();
                if (this.selectedElement !== evt.target) {
                    let geo = evt.target.getGeometry();
                    if (geo.lat) {
                        evt.target.setIcon(this.markerIconHover);
                    }
                }
            });

            group.addEventListener('pointerleave', (evt) => {
                if (this.selectedElement !== evt.target) {
                    let geo = evt.target.getGeometry();
                    if (geo.lat) {
                        evt.target.setIcon(this.markerIconDefault);
                    }
                }
            });
            // let svgMarkup = `<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"><circle cx="10" cy="10" r="5" stroke="black" stroke-width="1" fill="${markers.markerProperties.color}" /></svg>`
            // let icon = new LeafLetMap_var.map.Icon(svgMarkup, { anchor: new LeafLetMap_var.math.Point(10, 10) });
            
            let eventsList = Object.entries(events);
            for( let idx=0; idx<eventsList.length; idx++) {
              let lat,lng;
              let el = eventsList[idx];
              lat = Object.entries(el[1])[0][1][0].latitude;
              lng = Object.entries(el[1])[0][1][0].longitude;
              if(!lng) {
                isInvalidPoints = true
                console.log("No coordinates defined for the event")
                break;
              }
              let marker;
              if (this.props.selectedEvent && this.props.selectedEvent[0] === lat && this.props.selectedEvent[1] === lng) {
                marker = new LeafLetMap_var.map.Marker({ lat: lat, lng: lng }, { icon: this.markerIconSelected })
              } else {
                marker = new LeafLetMap_var.map.Marker({ lat: lat, lng: lng }, { icon: this.markerIconDefault });
              }
                marker.setData("Testdata for marker:" + lat + ", " + lng);
                group.addObject(marker);
            };
            if(isInvalidPoints) {
              console.log("No coordinates defined for the event")
              allGroups.pop();
              break;
            }
            this.map.addObject(group);
        }
    }
    this.centerMapAtRoute(allGroups);
  }

  centerMapAtObject(event) {
    let coord = this.map.screenToGeo(
      event.currentPointer.viewportX,
      event.currentPointer.viewportY
    );
    this.map.setCenter(coord, true);
  }

  centerMapAtRoute(allGroups) {
    // get bounding box of all groups and center map to common area
    let allBbs = [];
    allGroups.forEach((el) => allBbs.push(el.getBoundingBox()));
    let allGroupsBb = LeafLetMap_var.geo.Rect.coverRects(allBbs);
    // save allGroups bounding box for potential later use
    this.allGroupsBb = allGroupsBb;
    this.map.getViewModel().setLookAtData({
      bounds: allGroupsBb
    }, true);
    this.map.setZoom(17);
  }

  render() {
    return (
      <div id="leaflet-map-element" style={{ width: '99%', height: '89%' }} />
    );
  }
}

export default LeafLetMap;