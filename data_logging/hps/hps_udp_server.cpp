#include <thread>
#include <sys/errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "common.h"
#include "hps.h"

using namespace std;

typedef struct
{
	uint32_t size;
	uint32_t sequence;
	uint64_t timestamp;
} msgHeaderT;

struct hpUdpServerT
{
	hps_type type = HPS_UDP;
	const char *out = NULL;
	int port = DEFAULT_UDP_PORT;
	int sock = -1;
	hpStreamT *s = NULL;
	thread handlerThread;
	bool shutdownRequest = false;

	hpUdpServerT(const char *name, int port, const char *out)
	{
		int e;

		this->out = strdup(out);
		this->port = port;

		printf("UDP server listening on port %d\n", port);

		// Setup server socket
		sock = socket(AF_INET, SOCK_DGRAM, 0);
	    if (sock == -1)
	    {
	    	perror("Failed to create handler socket");
	    	throw exception();
	    }

	    // Bind & listen
	    struct sockaddr_in serverAddr;
	    serverAddr.sin_family = AF_INET;
	    serverAddr.sin_addr.s_addr = INADDR_ANY;
	    serverAddr.sin_port = htons(port);
	    if (bind(sock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)))
	    {
			perror("Failed to bind socket");
	    	throw exception();
	    }

	    uint32_t size = 0;
	    socklen_t len = sizeof(int);
	    // Get receive buffer size
	    e = getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &size, &len);
	    // Set receive buffer size
	    uint32_t newSize = 512 * 1024 * 1024;
	    e = setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &newSize, (socklen_t)sizeof(int));
	    if (e != 0) {
	    	perror("Failed to increase socket receive buffer");
	    }
	    // Get receive buffer size
	    e = getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &newSize, &len);
		printf("Receive buffer size: %d --> %d\n", size / 2, newSize / 2);

		s = hps_open(out);
    	if (s == NULL)
    	{
    		perror("AIO stream failed to open file");
    		throw exception();
    	}

		handlerThread = thread(&hpUdpServerT::run, this);
	}

	~hpUdpServerT()
	{
	    shutdownRequest = true;
	    handlerThread.join();

	    // Close socket
		if (sock != -1)
		    close(sock);

		// Close stream
	    if (s != NULL)
	    	hps_close(s);
	}

#if 1
	void run()
	{
		uint32_t lastSequence = -1;
    	// Number of messages to receive at once
		const int BURST = 32;
		const size_t requestBufferSize = BURST * MAX_UDP_SIZE;
		struct mmsghdr msgs[BURST];
		struct iovec iovecs[BURST];

	    while (1)
	    {
			// Acquire buffer
			void *buffer;
			int space = hps_write_begin(s, &buffer, requestBufferSize);
			if (space != requestBufferSize) {
				if (space < 0) {
					perror("hps_write_begin() failed");
					break;
				}
				else {
					pthread_yield();
					continue;
				}
			}

			// Receive data
			memset(msgs, 0, sizeof(msgs));
			for (int i = 0; i < BURST; i++) {
				iovecs[i].iov_base = (void*)((uintptr_t)buffer + i * MAX_UDP_SIZE);
				iovecs[i].iov_len = MAX_UDP_SIZE;
				msgs[i].msg_hdr.msg_iov = &iovecs[i];
				msgs[i].msg_hdr.msg_iovlen = 1;
			}

			// Receive up to BURST number of UDP messages
			int n = recvmmsg(sock, msgs, BURST, MSG_DONTWAIT, NULL);
			if (0 < n)
			{
				size_t rxBytes = 0;

				// Iterate messages
				for (int i = 0; i < n; i++) {
					// Cast message header
					msgHeaderT *header = (msgHeaderT*)msgs[i].msg_hdr.msg_iov[0].iov_base;

					// Check sequence number
					uint32_t lost = header->sequence - lastSequence - 1;
					if (0 < lost)
					{
						__sync_add_and_fetch(&statistics.rxLostMessages, lost);
						//printf("Error: lost %u packets\n", lost);
					}
					lastSequence = header->sequence;

					rxBytes += msgs[i].msg_len;
				}

				hps_write_end(s, rxBytes);

				// Update statistics
				__sync_add_and_fetch(&statistics.rxMessages, n);
				__sync_add_and_fetch(&statistics.rxBytes, rxBytes);
			}
			else
			{
				if (n == 0 || errno == EWOULDBLOCK) {
					// No data
					//pthread_yield();
					usleep(1000);
				}
				else {
					perror("recvmmsg() failed");
					break;
				}
			}
	    }
	}
#else
	void run()
	{
		uint32_t lastSequence = -1;
	    while (1)
	    {
			// Acquire buffer
			void *buffer;
			int space = hps_write_begin(s, &buffer, MAX_UDP_SIZE);
			if (space < 0)
			{
				perror("hps_write_begin() failed");
				break;
			}

			// Receive data
			int n = recv(sock, buffer, space, 0);
			if (0 < n)
			{
				msgHeaderT *header = (msgHeaderT*)buffer;
				uint64_t t = getTimestampNs();

				// Check sequence number
				uint32_t lost = header->sequence - lastSequence - 1;
				if (0 < lost)
				{
					__sync_add_and_fetch(&statistics.rxLostMessages, lost);
					//printf("Error: lost %u packets\n", lost);
				}
				lastSequence = header->sequence;

				hps_write_end(s, n);

				// Update statistics
				__sync_add_and_fetch(&statistics.rxMessages, 1);
				__sync_add_and_fetch(&statistics.rxBytes, n);
			}
			else if (n == 0)
			{
				printf("Client has shutdown the connection\n");
				break;
			}
			else if (errno == EWOULDBLOCK)
			{
				// No data
			}
			else
			{
				perror("recv() failed");
				break;
			}
	    }
	}
#endif
};

hpUdpServerT* hps_udp_start_server(const char *name, int port, const char *out)
{
	try
	{
		return new hpUdpServerT(name, port, out);
	}
	catch (exception&)
	{
		return NULL;
	}
}

int hps_udp_close_server(hpUdpServerT *server)
{
    delete server;

    return 0;
}
