import React from 'react';
import {ActionBar} from './ActionBar';
import {shallow} from 'enzyme/build';

describe('ActionBar component', () => {

  it('should renders and show links in correct order', () => {
    const wrapper = shallow(<ActionBar />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('Button').at(0).exists()).toBe(true);
    expect(wrapper.find('Button').at(1).exists()).toBe(true);
  });

});