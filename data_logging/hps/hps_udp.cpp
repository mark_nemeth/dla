#include <errno.h>
#include <string.h>
#include <malloc.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include "common.h"
#include "hps.h"

struct hpUdpStreamT
{
	hps_type type;
	int sock;
};

hpUdpStreamT *hps_udp_open(const char *ip, int port)
{
	hpUdpStreamT *s = (hpUdpStreamT*)malloc(sizeof(hpUdpStreamT));
	if (s == NULL)
		return NULL;
	memset(s, 0, sizeof(*s));

	s->type = HPS_UDP;

	// Connect
    printf("Connecting to UDP://%s:%d\n", ip, port);
	s->sock = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in server;
    server.sin_addr.s_addr = inet_addr(ip);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    int e = connect(s->sock, (struct sockaddr *)&server, sizeof(server));
    if (e != 0)
    {
    	perror("Failed to connect");
    	close(s->sock);
    	free(s);
    	return NULL;
    }

	int size = 0;
	socklen_t len = sizeof(int);
	// Get send buffer size
	e = getsockopt(s->sock, SOL_SOCKET, SO_SNDBUF, &size, &len);
	// Set send buffer size
	int newSize = 4 * 1024 * 1024;
	e = setsockopt(s->sock, SOL_SOCKET, SO_SNDBUF, &newSize, (socklen_t)sizeof(int));
	if (e != 0) {
		perror("Failed to increase socket receive buffer");
	}
	// Get send buffer size
	e = getsockopt(s->sock, SOL_SOCKET, SO_SNDBUF, &newSize, &len);
	printf("Send buffer size: %d --> %d\n", size, newSize);

	return s;
}

int hps_udp_writev(hpUdpStreamT *s, const struct iovec *iov, int count)
{
	size_t size = 0;
	for (int i = 0; i < count; i++) {
		size += iov[i].iov_len;
	}

	while (true) {
		struct msghdr msg;
		memset(&msg, 0, sizeof(msg));
		msg.msg_iov = (struct iovec *)iov;
		msg.msg_iovlen = count;
		int n = sendmsg(s->sock, &msg, MSG_DONTWAIT);
		//int n = writev(s->sock, iov, count);

		if (n == size) {
			statistics.txBytes += n;
			return n;
		}
		else {
			if ((n == -1) && (errno == EAGAIN)) {
				usleep(1000);
			}
			else {
				fprintf(stderr, "sendmsg() failed with %d (errno %d)\n", n, errno);
				return -1;
			}
		}
	}
}

int hps_udp_close(hpUdpStreamT *s)
{
	close(s->sock);
	free(s);
	return 0;
}
