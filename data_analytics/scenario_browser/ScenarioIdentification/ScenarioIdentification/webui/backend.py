import numpy as np
import pandas as pd
from data import ego_tracks_all_files, geom_features_all_files, map_objects, dyn_intersections, tracks_df_intersections
## filtering function for the data
def filter_geom_features(geom_features, coef2_cutoff=700):
    geom_features_filtered = geom_features[geom_features['coef2_polyfit'] >= coef2_cutoff].copy()
    output_df = pd.DataFrame({
        'Filename':geom_features_filtered['Filename'],
        'T': geom_features_filtered['T'],
        'Lat':geom_features_filtered['Lat'],
        'Lon':geom_features_filtered['Lon'],
        'turn_coef':geom_features_filtered['coef2_polyfit'],
        'turn_ind':geom_features_filtered['Turn_Ind']
    })
    return output_df

def filter_lsq_features(geom_features, coef2_lsq_cutoff=0):
    lsq_features_filtered = geom_features[abs(geom_features['coef2_lstsq']) >= coef2_lsq_cutoff].copy()
    output_df = pd.DataFrame({
        'Filename':lsq_features_filtered['Filename'],
        'T': lsq_features_filtered['T'],
        'Lat':lsq_features_filtered['Lat'],
        'Lon':lsq_features_filtered['Lon'],
        'turn_coef':lsq_features_filtered['coef2_lstsq'],
    })
    return output_df

def filter_turn_ind(geom_features):
    turn_ind_filtered = geom_features[geom_features['Turn_Ind']!=0].copy()
    output_df = pd.DataFrame({
        'Filename':turn_ind_filtered['Filename'],
        'T': turn_ind_filtered['T'],
        'Lat':turn_ind_filtered['Lat'],
        'Lon':turn_ind_filtered['Lon'],
        'Turn_Ind':turn_ind_filtered['Turn_Ind']
    })
    return output_df

# def filter_dyn_objects(dyn_object_selected=None):
#     if dyn_object_selected:
#         dyn_obj_tracks = tracks_df_intersections[tracks_df_intersections['Label']==str(dyn_object_selected)].copy()
#     else:
#         dyn_obj_tracks = pd.DataFrame({
#             'Filename':None,
#             'Label':None,
#             'Label_id':None,
#             'T':None,
#             'Track_id':None,
#             'X':None,
#             'Y':None
#         })
#     return dyn_obj_tracks

def filter_dyn_obj_intersections(dyn_object_selected=None, mo_type = None, proximity_type='Within', cutoff=0):
    if dyn_object_selected:
        if mo_type:
            if proximity_type == 'Within':
                dyn_obj_intersections = (dyn_intersections[dyn_intersections['Label']==str(dyn_object_selected)]
                      [dyn_intersections['Proximity_'+mo_type] <= cutoff]
                      [['Filename', 'Label', 'Label_id', 'T', 'Track_id', 'Lon_intersect', 'Lat_intersect']]
                      .rename(columns={'Lon_intersect':'X', 'Lat_intersect':'Y'}).copy())
            elif proximity_type == 'Not Within':
                dyn_obj_intersections = (dyn_intersections[dyn_intersections['Label']==str(dyn_object_selected)]
                      [dyn_intersections['Proximity_'+mo_type] > cutoff]
                      [['Filename', 'Label', 'Label_id', 'T', 'Track_id', 'Lon_intersect', 'Lat_intersect']]
                      .rename(columns={'Lon_intersect':'X', 'Lat_intersect':'Y'}).copy())
        else:
            dyn_obj_intersections = (dyn_intersections[dyn_intersections['Label']==str(dyn_object_selected)]
                              [['Filename', 'Label', 'Label_id', 'T', 'Track_id', 'Lon_intersect', 'Lat_intersect']]
                              .rename(columns={'Lon_intersect':'X', 'Lat_intersect':'Y'}).copy())
    else:
        dyn_obj_intersections = pd.DataFrame({
            'Filename':None,
            'Label':None,
            'Label_id':None,
            'T':None,
            'Track_id':None,
            'X':None,
            'Y':None
        })
    return dyn_obj_intersections

def filter_dyn_obj_tracks_from_trackid(dyn_fname=None, dyn_track_id=None):
    if dyn_track_id:
        dyn_obj_tracks = tracks_df_intersections[
            (tracks_df_intersections['Track_id']==dyn_track_id) & 
            (tracks_df_intersections['Filename']==dyn_fname)][
            ['Filename', 'Track_id','X', 'Y','T']].copy()
        return dyn_obj_tracks

def filter_dyn_objects_rel_pos(dyn_obj_tracks=None, dyn_obj_rel_x=0, dyn_obj_rel_y=0):
    if (dyn_obj_rel_y > 0) or (dyn_obj_rel_x > 0):
        dyn_obj_tracks = dyn_obj_tracks[((dyn_obj_tracks['Y_relative']<dyn_obj_rel_y)
                                             & (dyn_obj_tracks['Y_relative']>-dyn_obj_rel_y)
                                             & (dyn_obj_tracks['X_relative']>-dyn_obj_rel_x)
                                             & (dyn_obj_tracks['X_relative']<dyn_obj_rel_x))].copy()
    return dyn_obj_tracks


def filter_dyn_objects_rel_vel(dyn_obj_tracks=None, dyn_obj_v_x=0, dyn_obj_v_y=0):
    if (dyn_obj_v_y > 0) or (dyn_obj_v_x > 0):
        dyn_obj_tracks = dyn_obj_tracks[((dyn_obj_tracks['V_y']<dyn_obj_v_y)
                                             & (dyn_obj_tracks['V_y']>-dyn_obj_v_y)
                                             & (dyn_obj_tracks['V_x']>-dyn_obj_v_x)
                                             & (dyn_obj_tracks['V_x']<dyn_obj_v_x))].copy()
    return dyn_obj_tracks

def filter_right_turn(geom_features, sa_cutoff=.1, curvature_cutoff=700):
    right_turn_temp = geom_features[geom_features['Turn_Ind']==2].copy()
    right_turn_temp = right_turn_temp[right_turn_temp['Steer_Angle']<=-abs(sa_cutoff)].copy()
    right_turn_filtered = right_turn_temp[right_turn_temp['coef2_polyfit']>curvature_cutoff].copy()
    output_df = pd.DataFrame({
        'Filename':right_turn_filtered['Filename'],
        'T': right_turn_filtered['T'],
        'Lat':right_turn_filtered['Lat'],
        'Lon':right_turn_filtered['Lon'],
        'turn_ind':right_turn_filtered['Turn_Ind']
    })
    return output_df

def filter_left_turn(geom_features, sa_cutoff=.1, lsq_cutoff=0.08):
    left_turn_temp = geom_features[geom_features['Turn_Ind']==1].copy()
    left_turn_filtered = left_turn_temp[left_turn_temp['Steer_Angle']>=sa_cutoff].copy()
    #left_turn_filtered = left_turn_temp[abs(left_turn_temp['coef2_lstsq'])>lsq_cutoff].copy()
    output_df = pd.DataFrame({
        'Filename':left_turn_temp['Filename'],
        'T': left_turn_filtered['T'],
        'Lat':left_turn_filtered['Lat'],
        'Lon':left_turn_filtered['Lon'],
        'turn_ind':left_turn_filtered['Turn_Ind']
    })
    return output_df

def filter_lane_change(geom_features, sa_cutoff=.05):
    lane_change_temp = geom_features[geom_features['Turn_Ind']!=0].copy()
    lane_change_filtered = lane_change_temp[lane_change_temp['Steer_Angle']<=sa_cutoff].copy()
    output_df = pd.DataFrame({
        'Filename':lane_change_filtered['Filename'],
        'T': lane_change_filtered['T'],
        'Lat':lane_change_filtered['Lat'],
        'Lon':lane_change_filtered['Lon'],
        'turn_ind':lane_change_filtered['Turn_Ind']
    })
    return output_df

def filter_lane_changes_map(geom_features):
    lane_changes_map = geom_features[geom_features['Lane_Change'].notna()].copy()
    output_df = pd.DataFrame({
        'Filename':lane_changes_map['Filename'],
        'T': lane_changes_map['T'],
        'Lat':lane_changes_map['Lat'],
        'Lon':lane_changes_map['Lon'],
        'Lane_Change':lane_changes_map['Lane_Change']
    })
    return output_df

def filter_steering_angle(geom_features, steer_cutoff=0):
    geom_features_filtered = geom_features[abs(geom_features['Steer_Angle']) >= steer_cutoff].copy()
    output_df = pd.DataFrame({
        'Filename':geom_features_filtered['Filename'],
        'T': geom_features_filtered['T'],
        'Lat':geom_features_filtered['Lat'],
        'Lon':geom_features_filtered['Lon'],
        'Steer_Angle':geom_features_filtered['Steer_Angle'],
        'turn_ind':geom_features_filtered['Turn_Ind']
    })
    return output_df

def filter_map_objects(obj_type, comp_obj_list=[], prox_type=None, cutoff=50):
    cutoff = float(cutoff)
    map_objects_filtered = map_objects[map_objects['object_type'] == obj_type].copy()
    if prox_type == 'Within':
        for obj in comp_obj_list:
            map_objects_filtered = map_objects_filtered[map_objects_filtered[obj+'_DIST'] <= cutoff].copy()
    elif prox_type == 'Not Within':
        for obj in comp_obj_list:
            map_objects_filtered = map_objects_filtered[map_objects_filtered[obj+'_DIST'] >= cutoff].copy()
    output_df = pd.DataFrame({
        'object_type':map_objects_filtered['object_type'],
        'Lat':map_objects_filtered['Lat'],
        'Lon':map_objects_filtered['Lon']
    })
    return output_df