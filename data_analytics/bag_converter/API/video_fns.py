__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import shutil
import tempfile

import cv2
import numpy as np

from API.bagfile_fns import bag2slice_fn
from config import ExceptionHandler
from config import logging_handler as logger
from image_processing.image_processor import extract_images


def bag2vid_fn(
        bagfile: str,
        topic: str,
        start_time: int = None,
        end_time: int = None,
        start_relative: int = None,
        end_relative: int = None,
        overlay: bool = False,
        overlay_id: int = -1,
        apply_time_correct: bool = False
):
    """ Returns a video for the given duration. """

    if start_time is None and start_relative is None:
        raise ExceptionHandler('missing_relative_time_err')

    bagfile_slice = bag2slice_fn(bagfile=bagfile,
                                 topic=topic,
                                 start_relative=start_relative,
                                 end_relative=end_relative,
                                 start_time=start_time,
                                 end_time=end_time,
                                 apply_time_correct=apply_time_correct)

    images = extract_images(bagfile=bagfile_slice,
                            overlay=overlay,
                            overlay_id=overlay_id,
                            max_messages=-1,
                            )

    logger.debug(f'images count: {len(images)}, {len(images[0])}')
    # Calculate average framerate.
    first_frame = float('{}.{}'.format(images[0][1].secs, images[0][1].nsecs))
    last_frame = float('{}.{}'.format(images[-1][1].secs, images[-1][1].nsecs))
    logger.debug(f'first_frame: {first_frame}, last_frame:{last_frame}')

    duration = last_frame - first_frame
    fps = len(images) / duration

    width, height = images[0][0].size

    tmpfile = os.path.join(tempfile.mkdtemp(), 'out.mp4')
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    video = cv2.VideoWriter(filename=tmpfile, apiPreference=cv2.CAP_ANY, fourcc=fourcc, fps=fps, frameSize=(width, height))

    for image in images:
        video.write(cv2.cvtColor(np.array(image[0]), cv2.COLOR_RGB2BGR))

    cv2.destroyAllWindows()
    video.release()

    videofile = tempfile.NamedTemporaryFile()
    shutil.copyfile(tmpfile, videofile.name)
    shutil.rmtree(os.path.dirname(tmpfile))

    videofile.seek(0)
    return videofile



