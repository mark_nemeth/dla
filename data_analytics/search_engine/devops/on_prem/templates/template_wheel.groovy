node("${params.node}") {
 stage('Get repo...') {
    checkout scm
 }
 stage('Building image') {
    withCredentials([usernamePassword(credentialsId: 'tech_user_dla', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
    //Build wheel
    sh"""
    cd "$path_to_component"
    python3 setup.py sdist bdist_wheel
    """
    //Push to artifactory
    def server = Artifactory.newServer url: 'https://athena.daimler.com/artifactory/', username: "$USERNAME", password: "$PASSWORD"
    def uploadSpec = """{
    "files": [
    {
      "pattern": "${params.path_to_component}/dist/*${params.component}*.whl",
      "target": "${params.registry_target}"
    }
    ]
    }"""
    server.upload(uploadSpec)
    }
 }
}
