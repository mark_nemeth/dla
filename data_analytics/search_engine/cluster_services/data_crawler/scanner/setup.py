"""Setup file"""
from setuptools import find_packages, setup

from config.constants import VERSION

setup(
    name='scanner',
    version=VERSION,
    packages=find_packages(),
    test_suite="tests",
    install_requires=[
        'fire~=0.3.1'
    ]
)
