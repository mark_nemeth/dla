import {connect} from 'react-redux';
import {filterByAcceleration, setCurrentAccelerationRange} from '../../../../redux/Home/HomeActions';
import {ByAccelerationFilter, STATIC_LIMITS} from './ByAccelerationFilter';

function mapStateToProps(state, ownProps) {
  const limits = state.filterOptions.bagfileAccelerationLimits ?
    {
      min: state.filterOptions.bagfileAccelerationLimits[0],
      max: state.filterOptions.bagfileAccelerationLimits[1]
    }
    : STATIC_LIMITS;
  
  const acceleration = state.currentAccelerationRange || {
    'min': limits.min,
    'max': limits.max
  };
  const steps = 0.1;
  return {acceleration, limits, steps};
}

export default connect(
  mapStateToProps,
  {setCurrentAccelerationRange, filterByAcceleration}
)(ByAccelerationFilter)