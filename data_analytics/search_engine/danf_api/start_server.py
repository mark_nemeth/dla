"""Start the server."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import os

import uvicorn

import danf_api
from config.constants import DANF_VERSION_TAG
from danf_api.core.config import LOGLEVEL
from danf_api.core.logging import create_logger
from danf_api.main import app

_log = create_logger(name=danf_api.__name__, level=LOGLEVEL)

_log.info(f"Running service on version: {DANF_VERSION_TAG}")
uvicorn.run(app, host="0.0.0.0", port=int(os.getenv("SERVICE_PORT", 8449)))
