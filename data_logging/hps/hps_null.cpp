#include <string.h>
#include <malloc.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "hps.h"

#define DEFAULT_BUFFER_SIZE		(64 * 1024 * 1024)

struct hpNullStreamT
{
	hps_type type;
	void *buffer;
	int bufferSize;
};

hpNullStreamT *hps_null_open()
{
	hpNullStreamT *s = (hpNullStreamT*)malloc(sizeof(hpNullStreamT));
	if (s == NULL)
		return NULL;
	memset(s, 0, sizeof(*s));

	s->type = HPS_NULL;

	s->buffer = malloc(DEFAULT_BUFFER_SIZE);
	if (s->buffer == NULL)
	{
		free(s);
		return NULL;
	}
	s->bufferSize = DEFAULT_BUFFER_SIZE;

	return s;
}

int hps_null_write_begin(hpNullStreamT *s, void **buffer, unsigned size)
{
	if (size == 0)
	{
		*buffer = s->buffer;
		return s->bufferSize;
	}

	if (s->bufferSize < size)
	{
		void *p = realloc(s->buffer, size);
		if (p != NULL)
		{
			s->buffer = p;
			s->bufferSize = size;
		}
	}

	*buffer = s->buffer;

	return MIN(size, s->bufferSize);
}

int hps_null_write_end(hpNullStreamT *s, unsigned size)
{
	return 0;
}

int hps_null_writev(hpNullStreamT *s, const struct iovec *iov, int count)
{
	long long size = 0;

	for (int i = 0; i < count; i++)
		size += iov[i].iov_len;

	return size;
}

int hps_null_close(hpNullStreamT *s)
{
	if (s->buffer != NULL)
		free(s->buffer);
	memset(s, 0, sizeof(*s));

	free(s);

	return 0;
}
