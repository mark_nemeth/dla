import React from 'react';
import Form from 'react-bootstrap/Form';
import DatePicker from 'react-datepicker';
import { Portal } from 'react-overlays';
import { timestampToDateConverter } from '../../../utils/valueConverter';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import './DateSelector.scss';

// Defined limits are From June, 2019 to current date
export const LIMITS = {
  'min': new Date(2019, 5, 1),
  'max': new Date()
};

export function DateSelector(props) {

  const CalendarContainer = ({ children }) => {
    return (
      <Portal container={document.getElementById('calendar-portal')}>
        {children}
      </Portal>
    )
  };

  return (
    <Form className="date-wrapper">
      <div className="from-date-wrapper">
        <DatePicker
          placeholderText="Select start date"
          selected={timestampToDateConverter(props.start)}
          onChange={props.onStartDateSelection}
          minDate={LIMITS.min}
          maxDate={props.end}
          dateFormat="dd/MM/yyyy"
          popperContainer={CalendarContainer}
        />
      </div>
      <span className="date-divider">-</span>
      <div className="to-date-wrapper">
        <DatePicker
          placeholderText="Select end date"
          selected={timestampToDateConverter(props.end)}
          onChange={props.onEndDateSelection}
          minDate={props.start}
          maxDate={LIMITS.max}
          dateFormat="dd/MM/yyyy"
          popperContainer={CalendarContainer}
        />
      </div>
    </Form>
  );
}