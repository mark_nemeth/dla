node('SMTCA01551') {
 stage('Get repo...') {
    checkout scm
 }

 stage('Launch kubeconfig cli...') {
 try{
    withCredentials([usernamePassword(credentialsId: 'tech_user_dla', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
    withCredentials([usernamePassword(credentialsId: 'tech_user_dla_k8', usernameVariable: 'KUSERNAME', passwordVariable: 'KPASSWORD')]) {
    withCredentials([file(credentialsId: 'tech_user_cert', variable: 'cert')]) {
        //REPLACE ALL PLACEHOLDERS IN CONFIGMAP
        sh "cd data_analytics/search_engine/devops/on_prem/scripts/ && /bin/bash ./replace_placeholders.sh ${params.configmap_filepath}"
        //START KUBECTL DOCKER AND LOGIN
        sh"""
        docker login athena.daimler.com --username="$USERNAME" --password="$PASSWORD"
        docker pull athena.daimler.com/dla_docker/kubectl:latest
        docker logout athena.daimler.com
        docker tag athena.daimler.com/dla_docker/kubectl:latest dla-danf-kubectl
        docker run --rm -itd --name dla-danf-kubectl dla-danf-kubectl
        docker cp ${params.configmap_filepath} dla-danf-kubectl:/home/appuser/
        docker exec -t dla-danf-kubectl kubelogin --user oidc --username "$KUSERNAME" --password "$KPASSWORD" --insecure-skip-tls-verify
        """
        //Deploy or update configmap
        sh"""
        docker exec -t dla-danf-kubectl kubectl config use-context oidc@${params.loc}-${params.stage}
        docker exec -t dla-danf-kubectl kubectl create -f ${params.filename} -n ${params.namespace} || true
        docker exec dla-danf-kubectl /bin/sh -c 'kubectl replace -f ${params.filename} -n ${params.namespace}'
        """
        //LOGOUT
        sh'docker kill dla-danf-kubectl'
    }
    }
    }
    }
    catch (e) {
    sh"""
    docker kill dla-danf-kubectl
    docker logout athena.daimler.com
    """
    deleteDir()
    throw e
    }
    deleteDir()
 }
}