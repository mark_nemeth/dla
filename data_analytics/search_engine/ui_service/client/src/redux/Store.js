import {configureStore} from '@reduxjs/toolkit';
import reduceReducers from 'reduce-reducers';
import {AppReducer} from './App/AppReducers';
import {HomeReducer} from './Home/HomeReducers';
import {ReportingReducer} from './Reporting/ReportingReducers';
import {OverviewReducer} from './Overview/OverviewReducers';
import {FileTeansferReducer} from './FileTransfer/FileTransferReducers'
import {FileTypes} from '../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import {dateToTimestampConverter} from '../utils/valueConverter';
import {IngestReducer} from './Ingest/IngestReducers';

const initialState = {
  versions: {uiVersion: 'unknown', danfVersion: 'unknown'},
  fileType: FileTypes.CHILDBAGFILE,
  overview: {
    originalfiles: {},
    alwaysonfiles: {},
    childbagfiles: {},
    drives: {},
    file: {},
    flowRuns: {},
  },
  filters: {},
  reporting: {
    data: {kpis: [], aggregationTables: [], simpleTables: []},
    globalFilters: {
      report_type: 'dmv',
      location: 'us',
      start: dateToTimestampConverter(new Date(new Date() - 365 * 24 * 3600 * 1000).toLocaleString(), true),
      end: dateToTimestampConverter(new Date().toLocaleString(), true)
    },
    tripFilters: [
      {id: 'ign_dur', name: 'Ignore trips with duration < 1s', selected: true, enabled: true},
      {id: 'ign_dist', name: 'Ignore trips with distance < 1.5m ', selected: true, enabled: true},
      {id: 'ign_no_info', name: 'Ignore trips with no diseng. info', selected: true, enabled: true},
      {id: 'ign_priv', name: 'Ignore trips on private roads', selected: true, enabled: true}
    ],
    reportTypes: {
      dmv: {
        name: 'DMV',
        enabledLocations: ['any', 'us', 'ger', 'sjpilot', 'renningen', 'immendingen'],
        defaultLocation: 'us',
        enabled: true
      },
      rp: {
        name: 'Regierungspräsidium',
        enabledLocations: ['any', 'us', 'ger'],
        defaultLocation: 'ger',
        enabled: true
      }
    },
    locations: {
      any: {name: 'Any', enabled: true},
      ger: {name: 'Germany', enabled: true},
      us: {name: 'USA', enabled: true},
      renningen: {name: 'Renningen', enabled: true},
      immendingen: {name: 'Immendingen', enabled: true},
      sjpilot: {name: 'SJ Pilot Area', enabled: true}
    }
  },
  appliedFilters: {},
  filterOptions: {},
  httpRequests: {
    fetchFileByBagPathRequest: {
      ongoing: false
    },
    requestFileTransferRequests: {
      ongoing: []
    }
  },
  featureToggles: {
    isDriveViewActive: false,
    isIngestDashboardActive: false
  },
  points: []
};

const rootReducer = reduceReducers(
    initialState,
    AppReducer,
    HomeReducer,
    IngestReducer,
    OverviewReducer,
    FileTeansferReducer,
    ReportingReducer
);

export const store = configureStore({
  reducer: rootReducer
});