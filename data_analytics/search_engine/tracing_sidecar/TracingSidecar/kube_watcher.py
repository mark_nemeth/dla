"""Watching and processing kubernetes watch event"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from enum import IntEnum
from time import sleep
from typing import Callable, Optional, List, Any, Tuple

from DANFUtils.logging import logger
from kubernetes import client, config, watch
from kubernetes.client import V1Pod, V1ContainerStatus, CoreV1Api
from urllib3.exceptions import ProtocolError


class PodWatcher:
    """A pod watcher listens for kubernetes API pod change events of a
    specified target pod and forwards all events to a specified
    pod event handler.
    """

    def __init__(self,
                 pod_name: str,
                 namespace: str,
                 pod_event_handler: Callable[[V1Pod], bool]) -> None:
        """Create a new PodWatcher instance that listens for pod change
        events published by the kubernetes API and executes a specified
        pod event handler whenever the pod changes.

        :param pod_name: name of the pod to watch
        :param namespace: namespace of the pod to watch
        :param pod_event_handler: event handler that will be executed whenever
            the status of the watched pod changes. The event handler can stop
            the watching by returning True (bool terminate_watch).
        """

        self.pod_name = pod_name
        self.namespace = namespace
        self.pod_selector = "metadata.name=" + self.pod_name
        self.pod_event_handler = pod_event_handler
        self.v1 = self._init_kube_api_client()

    def start(self) -> None:
        """Start watching the target pod"""
        current_resource_version, pod = self._get_current_pod_state()
        if not pod:
            logger.error("Couldn't retrieve initial pod state, abort watching")
            return

        logger.debug(f"Retrieved initial state: {pod}")
        # publish current state as first change,
        # then start watching for follow up events
        terminate_watch = self.pod_event_handler(pod)
        if terminate_watch:
            logger.debug(
                "Received terminate signal from handler")
            return
        # initiate watch with the current resource version state,
        # so that only future events will be received
        self._event_loop(current_resource_version)

    def _init_kube_api_client(self) -> CoreV1Api:
        config.load_incluster_config()
        return client.CoreV1Api()

    def _get_current_pod_state(self,
                               max_retries: int = 6,
                               retry_interval: int = 30) -> Tuple[int, V1Pod]:
        """
        :param max_retries: max number of retries to get current state
        :param retry_interval: time between retries in seconds
        :return: tuple of current resource version and pod state
        """
        retries = 0
        while retries < max_retries:
            ret = self.v1.list_namespaced_pod(namespace=self.namespace,
                                              field_selector=self.pod_selector)
            matched_pods = [
                pod for pod in ret.items if pod.metadata.name == self.pod_name
            ]

            if len(matched_pods) == 1:
                # happy case: found exactly 1 matching pod
                return ret.metadata.resource_version, matched_pods[0]
            if len(matched_pods) > 1:
                # ambiguous target pod, abort
                logger.error(f"Expected one target pod but got {len(matched_pods)}"
                             f"for pod_selector={self.pod_selector}"
                             f"in namespace {self.namespace}")
                return None, None

            # No pod matched the selector. this could be due to misconstrued
            # pod name or if pod information is not available via API yet.
            # Due to the possibility of the latter case, we retry for a
            # limited amount of time
            retries += 1
            logger.error(f"No pod matched selector={self.pod_selector}"
                         f"in namespace {self.namespace}")
            sleep(retry_interval)

    def _event_loop(self, current_resource_version: int) -> None:
        w = watch.Watch()
        while True:
            try:
                # start watching kube API for pod changes
                for event in w.stream(
                        self.v1.list_namespaced_pod,
                        namespace=self.namespace,
                        field_selector=self.pod_selector,
                        resource_version=current_resource_version):
                    logger.debug(f"Received event: {event}")
                    pod = event['object']
                    current_resource_version = pod.metadata.resource_version
                    terminate_watch = self.pod_event_handler(pod)
                    if terminate_watch:
                        logger.debug(f"Received terminate signal from handler "
                                     f"for event {pod}")
                        return

            except ProtocolError as exp:
                # Connection to watch event stream is lost periodically. In
                # this case a ProtocolError is raised. If it happens,
                # simply reconnect to event stream starting at last seen
                # resource version
                logger.debug(f"Disconnected from kube API: {exp}")


class ContainerState(IntEnum):
    """Represents the different states a container within a pod can have
    (see also https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-states)
    """

    UNDEFINED = 1
    WAITING = 2
    RUNNING = 3
    TERMINATED = 4


class ContainerStateMachine:
    """Tracks the state of a specified target container within a pod. Consumes
    pod events published by the kubernetes API to update its internal state.
    """

    def __init__(self, target_container: str) -> None:
        """New instance to track the state of the specified target container.

        :param target_container: name of the container to track
        """
        self.target_container = target_container
        self.state = ContainerState.UNDEFINED
        self._callbacks = dict()

    def __call__(self, pod: V1Pod) -> bool:
        """Accepts pod change events, updates internal model of container
        state and executes callback functions if required.

        :param pod: Current pod object after change
        :return: flag to signal to the PodWatcher whether to continue watching
        """
        container_statuses = pod.status.container_statuses

        if not container_statuses:
            # No container status present, e.g. new container.
            # Continue watching pod
            return False

        matched_target_containers = [
            c for c in container_statuses if c.name == self.target_container
        ]
        if len(matched_target_containers) != 1:
            logger.error(f"expected one target container but found "
                         f"{len(matched_target_containers)}")
            return True

        container_status = matched_target_containers[0]
        return self._handle_new_status(container_status)

    def _handle_new_status(self,
                           container_status: V1ContainerStatus) -> bool:
        new_state, new_state_details = self._get_container_state(
            container_status)

        if new_state is self.state:
            # State did not change, no action required. Continue watching pod
            return False

        state_transitions = self._get_state_transitions(self.state, new_state)
        self._execute_callbacks(state_transitions, new_state,
                                new_state_details)

        self.state = new_state
        return self.state is ContainerState.TERMINATED

    def _execute_callbacks(self,
                           states: List[ContainerState],
                           new_state: ContainerState,
                           new_state_details: Any) -> None:
        for state in states:
            callback = self._callbacks.get(state)
            details = new_state_details if state is new_state else None
            if callback:
                callback(details)

    def register_callback_on_state_change(self,
                                          on_state: ContainerState,
                                          callback: Callable[
                                              [ContainerState, Any], None]
                                          ) -> None:
        """Registers callback that will be executed when the specified state
         is entered. The callback will also be executed for all intermediate
         states, e.g. when the current state is WAITING and the state is
         updated to TERMINATED, the callback for RUNNING is also executed.

        :param on_state: Entering this state will cause execution
        :param callback: function to execute
        """
        self._callbacks[on_state] = callback

    def _get_state_transitions(self, old_state: Optional[ContainerState],
                               new_state: ContainerState
                               ) -> List[ContainerState]:
        """Return list of ContainerStates that are newly entered when
        transitioning from old_state to new_state, e.g. when old_state
        is UNSPECIFIED and new_state is RUNNING, the result will be
        [WAITING, RUNNING].
        
        :param old_state: old container state. Will not be included
            in result
        :param new_state: new container state. Will be included
            in result
        :return: list of ContainerStates
        """
        return list(ContainerState)[old_state.value:new_state.value]

    def _get_container_state(self,
                             container_status: V1ContainerStatus
                             ) -> Tuple[ContainerState, Any]:
        """Extracts the ContainerState and container state details from
        the specified V1ContainerStatus object.
        """
        state = container_status.state
        if state.waiting:
            return ContainerState.WAITING, state.waiting
        if state.running:
            return ContainerState.RUNNING, state.running
        if state.terminated:
            return ContainerState.TERMINATED, state.terminated
        logger.warning(
            f"Received unknown container state from pod event: {state}")
