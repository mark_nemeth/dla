# Welcome to the Shared Infrastructure Repository
We will use this repos to store the [Azure Resource Management (ARM)](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-authoring-templates) Templates

The folder structure is aligned with the Athena`s team structure

- DLA
- DML
- etc.