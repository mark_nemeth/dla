import React, {useState} from 'react';
import FormControl from 'react-bootstrap/FormControl';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import NameEntry from '../NameEntry';
import './NameListEntry.scss'

export default function NameListEntry(props) {

  const [filteredNameList, setFilteredNameList] = useState(props.files);

  const listItem = (file) => (
      <div className={file.guid ? 'file-list-item-hover' : 'file-list-item'}>
        <NameEntry guid={file.guid} fileType={props.fileType} fileName={file.name} disableTooltip={true}/>
      </div>
  );

  function onSearchName(event) {
    const filteredNames = (props.files.filter(file => file.name.toLowerCase().includes(event.target.value.toLowerCase())));
    if (filteredNames.length === 0) filteredNames.push({name: 'No results found'});
    setFilteredNameList(filteredNames)
  }

  return (
      <div className="name-search-list">
        {
          props.enableSearchBar &&
          <FormControl placeholder="Search available files..."
                       aria-label="Search query"
                       aria-describedby="basic-addon2"
                       onChange={onSearchName}
                       onClick={onSearchName}
          />
        }
        <ScrollList
            showLoading={false}
            listHeight={props.files.length === 0 ? '28px' : (props.files.length < 5 ? '150px' : '200px')}
            itemList={filteredNameList}
            listItemComponent={listItem}
            itemVariant={"file-list-item-container"}
        />
      </div>
  )
}