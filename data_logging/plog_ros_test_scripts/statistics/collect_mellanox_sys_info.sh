#!/bin/sh

TT=`date +%j%H%M%S`

echo -e ""
echo -e "\033[35m collect temperatur informaton \033[0m"
sshpass -p admin ssh admin@192.168.253.1 cli \"show temperature\" > mellanox_status_$TT.log

echo $temp

echo -e ""
echo -e "\033[35m collect voltage informaton \033[0m"
sshpass -p admin ssh admin@192.168.253.1 cli \"show voltage\"  >> mellanox_status_$TT.log

echo -e ""
echo -e "\033[35m collect resources informaton \033[0m"
sshpass -p admin ssh admin@192.168.253.1 cli \"show resources\"  >> mellanox_status_$TT.log

echo -e ""
echo -e "\033[35m collect alarm informaton \033[0m"
sshpass -p admin ssh admin@192.168.253.1 cli \"show stats alarm\"  >> mellanox_status_$TT.log

echo -e ""
echo -e "\033[35m collect cpu informaton \033[0m"
sshpass -p admin ssh admin@192.168.253.1 cli \"show stats cpu\"  >> mellanox_status_$TT.log
