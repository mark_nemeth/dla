import axios from "axios";
import configHandlerSingleton from "../configHandler";
import MockAdapter from "axios-mock-adapter";
import SearchApi from './searchApi';
import storeProvider from '../models/storeProvider';

const mock = new MockAdapter(axios);

describe('Routes for API', () => {

  const req = {
    session: {
      passport: {
        user: 'USER-ID'
      }
    },
    query: {}
  };

  configHandlerSingleton.getBagfileAPIConfig = jest.fn(() => {
    return {url: '/search/bagfile'}
  });

  storeProvider.getUserTokenDataAsync = jest.fn(() => {
    return Promise.resolve({
      token_type: 'Bearer',
      access_token: 'access-token-1337'
    });
  });

  describe('Bagfile Route', () => {

    const url = '/search/bagfile';
    configHandlerSingleton.getBagfileAPIConfig = jest.fn(() => {
      return {url}
    });
    const mockSearchApiRequest = mock.onGet(url);

    it('call routes for bagfiles and get a valid result', () => {
      const responseData = [{
        "_id": "9217ea0ad7f44c2caa655c5110553779",
        "link": "/input/plog/WDD2221591A668818/2019/07/9/20190712_103708_test.bag",
        "size": 362172470,
        "num_messages": 5665488,
        "start": 1516299527033,
        "end": 1516301755313,
        "duration": 2228280,
        "vehicle_id_num": "v222-4567",
        "extractor": "ext3",
        "version": "0.1",
        "guid": "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4"
      }];
      mockSearchApiRequest.reply(200, responseData);

      return SearchApi.getBagfilesFromAPI(req).then((response) => {
        expect(response.data).toStrictEqual(responseData);
      })
    });

    it('call bagfiles should add sorting for newest data query parameter when no other sorting was selected', () => {
      mockSearchApiRequest.reply(200, {});
      return SearchApi.getBagfilesFromAPI(req).then((response) => {
        console.log(response);
        expect(response.config.params).toStrictEqual({sortby: '-start'});
      })
    });

    it('call bagfiles should not add sorting query parameter when user select another sorting criteria', () => {
      mockSearchApiRequest.reply(200, {});
      req.query.sortby = '+size';
      return SearchApi.getBagfilesFromAPI(req).then((response) => {
        console.log(response);
        expect(response.config.params).toStrictEqual({sortby: '+size'});
      })
    });
  });

  describe('Flow runs route', () => {

    const url = '/search/flow-runs';
    configHandlerSingleton.getFlowrunAPIConfig = jest.fn(() => {
      return {url}
    });
    const mockSearchApiRequest = mock.onGet(url);

    it('call routes for bagfiles and get a valid result', () => {
      const responseData = {
        "version": "1",
        "guid": "9de06cbd-a2b8-4d14-1337-c8a8da45cfe4",
        "kubeflow_workflow_name": "workflow1",
        "kubeflow_workflow_id": "1337",
        "started_at": 1581409906,
        "completed_at": 1581409999,
        "status": "SUCCESSFUL",  // IN_PROGRESS, SUCCESSFUL, FAILED
        "bagfile_guid": "9de06cbd-a2b8-4d14-9868-c8a8da45cfe4",
        "flow_guid": "9de06cbd-a2b8-4d14-1337-c8a8da45cfe4",
        "steps": [
          {
            "name": "extractor",        // stripper, extractor, ...
            "image": "extractor_v1",
            "tag": "my_tag",
            "max_retries": "3",
            "status": "SUCCESSFUL",    // IN_PROGRESS, SUCCESSFUL, FAILED, SKIPPED
            "runs": [
              {
                "started_at": 1581409906,
                "completed_at": 1581409999,
                "status": "FAILED",  // "SUCCESSFUL", "FAILED", "SKIPPED"
                "report": {
                  "message": "Permission denied opening file xxx"  // optional
                }
              },
              {
                "started_at": 1581409906,
                "completed_at": 1581409999,
                "status": "SUCCESSFUL",  // "SUCCESSFUL", "FAILED", "SKIPPED"
              }
            ]
          },
          {
            "name": "stripper",        // stripper, extractor, ...
            "image": "stripper_v1",
            "tag": "my_tag",
            "max_retries": "3",
            "status": "SUCCESSFUL",    // IN_PROGRESS, SUCCESSFUL, FAILED, SKIPPED
            "runs": [
              {
                "started_at": 1581409906,
                "completed_at": 1581409999,
                "status": "SUCCESSFUL",  // "SUCCESSFUL", "FAILED", "SKIPPED"
              }
            ]
          }
        ]
      };

      mockSearchApiRequest.reply(200, responseData);

      return SearchApi.getFlowRunsFromAPI(req).then((response) => {
        expect(response.data).toStrictEqual(responseData);
      })
    });

  });
});