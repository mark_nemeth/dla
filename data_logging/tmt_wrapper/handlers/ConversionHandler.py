"""Conversion Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
from handlers.AbstractHandler import AbstractHandler
from constants import *


class ConversionHandler(AbstractHandler):

    def __init__(self, config, tmt_path):
        super(ConversionHandler, self).__init__(config, SCHEMA_FILE_CONVERSION, tmt_path)

    def validate_config_fields(self):
        for f in self.config["dbc_files"]:
            assert os.path.isfile(os.path.abspath(f["path"])), "Description file {} not found!".format(f["path"])

    def build_cmd_list(self):
        cmd = [self.tmt_path]
        for f in self.config['input_file_paths']:
            cmd.extend(["--in", f])
        for f in self.config["dbc_files"]:
            cmd.extend(["--dbc", str(f["channel"]), f["path"]])
        cmd.extend(["--out", self.config['output_file_path']])
        return [cmd]
