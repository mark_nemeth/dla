"""Metadata REST API"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import ssl

import tornado.httpserver
import tornado.ioloop
import tornado.web
from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.logging import configure_root_logger

from config.config_handler import config_dict
from config.constants import DANF_VERSION_TAG
from requesthandler.accounts import AccountsRequestHandler, \
    AccountRequestHandler
from requesthandler.azure_import import ImportRequestHandler
from requesthandler.bagfile_custom_attributes import \
    BagfileCustomAttributesRequestHandler
from requesthandler.bagfile_flag import BagfileFlagRequestHandler
from requesthandler.bagfile_state import BagfileStateRequestHandler
from requesthandler.bagfiles import BagfilesRequestHandler, \
    BagfileRequestHandler
from requesthandler.bva_quota import BvaQuotaRequestHandler
from requesthandler.bva_vote import BvaVoteRequestHandler
from requesthandler.cache_bagfile_size_duration_time import \
    CacheBagFileSizeDurationRequestHandler
from requesthandler.cache_bagfile_vehicleids import CacheBagfileVehicleIDsRequestHandler
from requesthandler.cache_childbagfile_size_duration_time import \
    CacheChildBagFileSizeDurationRequestHandler
from requesthandler.cache_drive_duration_time import \
    CacheDriveDurationRequestHandler
from requesthandler.cache_driven_by_values import CacheDrivenByValueRequestHandler
from requesthandler.cache_drivers import CacheDriverRequestHandler
from requesthandler.cache_hardware_releases import CacheHardwareReleaseRequestHandler
from requesthandler.cache_operators import CacheOperatorRequestHandler
from requesthandler.cache_origins import CacheOriginRequestHandler
from requesthandler.cache_topics import CacheTopicsRequestHandler
from requesthandler.child_bagfiles import ChildbagfilesRequestHandler, ChildbagfileRequestHandler
from requesthandler.childbagfile_custom_attributes import \
    ChildBagfileCustomAttributesRequestHandler
from requesthandler.drives import DrivesRequestHandler, \
    DriveRequestHandler
from requesthandler.extractor_requests import ExtractorRequestsRequestHandler
from requesthandler.feature_buckets import FeatureBucketsRequestHandler, FeatureBucketRequestHandler
from requesthandler.file_catalog import FileCatalogRequestHandler, \
    FileCatalogUpdateRequestHandler, \
    FileCatalogUrlStatusRequestHandler
from requesthandler.flow_runs import FlowRunsRequestHandler, \
    FlowRunRequestHandler, FlowRunStepRunsRequestHandler
from requesthandler.flows import FlowsRequestHandler, FlowRequestHandler
from requesthandler.sequences import SequencesRequestHandler

logger = logging.getLogger(__name__)


# Handlers
def _make_app():
    return tornado.web.Application(
        [(r"/bagfile/?", BagfilesRequestHandler),
         (r"/bagfile/([0-9a-f\-]{36})/?", BagfileRequestHandler),
         (r"/bagfile/([0-9a-f\-]{36})/state/?", BagfileStateRequestHandler),
         (r"/bagfile/([0-9a-f\-]{36})/custom_attributes/?",
          BagfileCustomAttributesRequestHandler),
         (r"/bagfile/flag/?", BagfileFlagRequestHandler),
         (r"/bagfile/([0-9a-f\-]{36})/flag/?", BagfileFlagRequestHandler),
         (r"/drive/?", DrivesRequestHandler),
         (r"/drive/([0-9a-f\-]{36})/?", DriveRequestHandler),
         (r"/sequence/?", SequencesRequestHandler),
         (r"/sequence/([0-9a-f\-]{36})/?", SequencesRequestHandler),
         (r"/extractorrequest/?", ExtractorRequestsRequestHandler),
         (r"/extractorrequest/([0-9a-f\-]{36})/?",
          ExtractorRequestsRequestHandler),
         (r"/childbagfile/?", ChildbagfilesRequestHandler),
         (r"/childbagfile/([0-9a-f\-]{36})/?", ChildbagfileRequestHandler),
         (r"/featurebucket/?", FeatureBucketsRequestHandler),
         (r"/featurebucket/([0-9a-f\-]{36})/?", FeatureBucketRequestHandler),
         (r"/childbagfile/([0-9a-f\-]{36})/custom_attributes/?",
          ChildBagfileCustomAttributesRequestHandler),
         (r"/cache/vehicleids/?", CacheBagfileVehicleIDsRequestHandler),
         (r"/cache/topics/?", CacheTopicsRequestHandler),
         (r"/cache/size-duration-time/?",
          CacheBagFileSizeDurationRequestHandler),
         (r"/cache/childbagfile-size-duration-time/?",
          CacheChildBagFileSizeDurationRequestHandler),
         (r"/cache/drive-duration-time/?",
          CacheDriveDurationRequestHandler),
         (r"/cache/origins/?", CacheOriginRequestHandler),
         (r"/cache/drivers/?", CacheDriverRequestHandler),
         (r"/cache/operators/?", CacheOperatorRequestHandler),
         (r"/cache/hardware-releases/?", CacheHardwareReleaseRequestHandler),
         (r"/cache/driven-by-values/?", CacheDrivenByValueRequestHandler),
         (r"/import/?", ImportRequestHandler),
         (r"/file/([0-9a-f\-]{36})/urls/(\d+)/status/?",
          FileCatalogUrlStatusRequestHandler),
         (r"/file/([0-9a-f\-]{36})/urls/?", FileCatalogUpdateRequestHandler),
         (r"/file/([0-9a-f\-]{36})/?", FileCatalogRequestHandler),
         (r"/flows/?", FlowsRequestHandler),
         (r"/flows/([0-9a-f\-]{36})/?", FlowRequestHandler),
         (r"/flow-runs/?", FlowRunsRequestHandler),
         (r"/flow-runs/([0-9a-f\-]{36})/?", FlowRunRequestHandler),
         (r"/flow-runs/([0-9a-f\-]{36})/steps/([^/]+)/runs/?",
          FlowRunStepRunsRequestHandler),
         (r"/bva/quotas/?", BvaQuotaRequestHandler),
         (r"/bva/quotas/([0-9a-f\-]{36})/?", BvaQuotaRequestHandler),
         (r"/bva/vote/([0-9a-f\-]{36})/?", BvaVoteRequestHandler),
         (r"/bva/vote/?", BvaVoteRequestHandler),
         (r"/accounts/?", AccountsRequestHandler),
         (r"/accounts/([0-9a-f\-]{36})/?", AccountRequestHandler)],
        debug=config_dict.get_value('log_level').upper() == 'DEBUG')


def _main():
    log_level = config_dict.get_value('log_level', 'INFO').upper()
    configure_root_logger(
        log_level=log_level,
        file_path='/tmp/metadata.log',
        enable_azure_log_aggregation=danf_env().platform != Platform.LOCAL,
    )
    logger.setLevel(log_level)

    logger.info("#############################")
    logger.info("######## Metadata API #########")
    logger.info("#############################")
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    logger.info(f"DANF ENV: {config_dict.get_value('DANF_ENV')}")
    logger.info("starting server...")

    app = _make_app()

    app_https_port = config_dict.get_value('app_https_port', 8443)
    app_http_port = config_dict.get_value('app_http_port', 0)
    ssl_cert = config_dict.get_value('ssl_cert', None)
    ssl_key = config_dict.get_value('ssl_key', None)
    if app_http_port:
        app.listen(app_http_port)
        logger.info(f"app is listening on HTTP port: {app_http_port}...")
    if app_https_port:
        https_server = tornado.httpserver.HTTPServer(app,
                                                     ssl_options={
                                                         'certfile':
                                                         ssl_cert,
                                                         'keyfile':
                                                         ssl_key,
                                                         'ssl_version':
                                                         ssl.PROTOCOL_TLSv1_2
                                                     })
        https_server.listen(app_https_port)
        logger.info(f"app is listening on HTTPS port: {app_https_port} ...")

    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    _main()
