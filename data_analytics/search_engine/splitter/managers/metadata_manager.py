__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os
from subprocess import check_call
from typing import Optional
from pathlib import Path

from ContainerClient.container_client import ContainerClient
from DANFUtils.constants import ROOT_FILE_GUID
from DANFUtils.exceptions import RESTException
from DANFUtils.logging import logger
from DANFUtils.utils.utils import extract_file_guid


class MetadataManager(object):
    """Retrieve metadata from childbagfiles. Sources are:
    1. header information from the childbagfile itself: start, end,, size, topics, duration
    2. Get from the parent bagfile: vid, drive type
    3. Additional metadata from the snippets (for splitter): metadata, parent_index_start, parent_index_end
    4. Get version, parent_guid, link, filename from env
    """
    def __init__(self, parent_guid):
        self.client = ContainerClient()
        self.parent_guid = parent_guid
        self.parent_metadata = None

    def _get_childbagfile_header_metadata(self, output_path):
        """Open the header of the bagfile and get metadata using dla_converter
        """
        try:
            # read header with dla_converter and save as json
            check_call([
                "dla_converter/dla_converter",
                "--export-info",
                output_path,
                "info.json"
            ])

            # read and delete json
            with open("info.json", "r") as f:
                info_dict = json.load(f)
            os.remove("info.json")

            header_dict = {
                "start": self._timestamp2ms(info_dict.get("start", 0)),
                "end": self._timestamp2ms(info_dict.get('end', 0)),
                "duration": int(info_dict.get('duration', 0)/1e6),
                "topics": info_dict.get('topics', []),
                "size": info_dict.get('size', 0),
                "num_messages": info_dict.get('message_count', 0)
            }
            return header_dict

        except Exception:
            logger.exception(f"Error opening childbag file, "
                             f"metadata cannot be fetched from the header! file: {output_path}")
            return {"start": 0, "end": 0, "duration": 0, "topics": [], "size": 0, "num_messages": 0}

    def load_parent_bagfile_metadata(self):
        """Get metadata from the parent bagfile which should be copied to the child bagfile
        """
        try:
            self.parent_metadata = self.client.get_bagfile(self.parent_guid)
        except Exception:
            logger.exception(f"Error fetch parent bagfile metadata! parent_guid: {self.parent_guid}")
            self.parent_metadata = {}

    def check_if_parent_indexed(self):
        return self.parent_metadata.get("indexed")

    @staticmethod
    def _get_snippet_metadata(snippet):
        """For splitted files: get metadata from the snippet
        """
        if snippet:
            snippet_dict = {
                "metadata": snippet.get("metadata", {}),
                "parent_index_start": snippet.get("parent_index_start", 0),
                "parent_index_end": snippet.get("parent_index_end", 0)
            }
            return snippet_dict
        else:
            return {}

    def collect_metadata_to_submit(self, child_path, output_path, filename, snippet=None):
        """Collect metadata for bagfile from different sources
        """
        metadata_dict = {
            "version": "1.0",
            "parent_guid": self.parent_guid,
            "link": os.path.join(child_path, filename.split("/")[-1]),
            "file_name": filename.split("/")[-1]
        }
        parent_dict = {
            "vehicle_id_num": self.parent_metadata.get("vehicle_id_num", "unknown"),
            "tags": [],  # do not add tags here
            "drive_types": self.parent_metadata.get("drive_types", [])
        }
        metadata_dict.update(self._get_childbagfile_header_metadata(output_path))
        metadata_dict.update(parent_dict)
        metadata_dict.update(self._get_snippet_metadata(snippet))
        return metadata_dict

    def submit_metadata(self, child_metadata):
        """Submit metadata of each splitted child bagfile after generation
        """
        try:
            child_guid = self.client.upsert_childbagfile(child_metadata)
            logger.info(f"Childbagfile metadata is created in the DB. guid: {child_guid}")
            logger.info(f"Metadata of the childbagfile: {child_metadata}")
        except RESTException:
            logger.exception(f"Can not update metadata {child_metadata}")

    def update_bagfile_path(self, childbagfile_link_template):
        """Update the file path of the bagfile after the file is transfered to another folder
        """
        self.load_parent_bagfile_metadata()
        filename = self.parent_metadata.get("file_name")
        logger.info(f"Updating current path for bagfile {self.parent_guid}.")
        update_links_dict = {
            "current_link": childbagfile_link_template.format(filename)
        }
        self.client.update_bagfile(self.parent_guid, update_links_dict)

    @staticmethod
    def _timestamp2ms(timestamp):
        """Converts timestamp to milliseconds (int)"""
        return int(str(timestamp).replace(".", "")[0:13])

    def submit_child_bagfile_to_file_catalog(
            self, metadata, full_output_file_path, expiration_date=None, tags=None
    ) -> Optional[str]:
        try:
            path = Path(full_output_file_path)
            stat = path.stat()
            creation_time = int(stat.st_ctime * 1000)
            file_size = stat.st_size
            file_guid = extract_file_guid(full_output_file_path)

            self.client.metadata_handler.add_file_to_catalog(file_guid, file_size, ".bag", str(ROOT_FILE_GUID), tags)
            link = metadata["link"].replace("/mapr/", "hdfs://", 1)
            self.client.metadata_handler.add_accessor_url_to_catalog(file_guid, link, creation_time, "DANF",
                                                                     expiration_date)

        except (KeyError, OSError, RESTException):
            logger.exception(f"Error submitting childbagfile metadata to the file catalog.")
            return

        return file_guid

    def add_accessor_url_to_file_catalog(self, output_folder):
        self.load_parent_bagfile_metadata()
        file_name = self.parent_metadata.get("file_name")
        link = self.parent_metadata.get("current_link")
        try:
            full_output_file_path = Path(output_folder, file_name)
            stat = full_output_file_path.stat()
            creation_time = int(stat.st_ctime * 1000)
            file_guid = extract_file_guid(full_output_file_path)
            link = link.replace("/mapr/", "hdfs://", 1)
            self.client.metadata_handler.add_accessor_url_to_catalog(file_guid, link, creation_time, "DANF")

        except (KeyError, OSError, RESTException):
            pass
