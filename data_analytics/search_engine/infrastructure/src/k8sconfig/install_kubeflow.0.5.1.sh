#!/bin/bash

# Download AKS credentials
az aks get-credentials --name "dla-kubecluster-aks-$ENVIRONMENT" --resource-group $RESOURCEGROUP --admin

# install kubeflow with kfctl.sh instead of kfctl binary due to https://github.com/kubeflow/kubeflow/issues/4642#issuecomment-573428444
# requires ksonnet and kubeflow as dependencies

# download ksonnet binary, make it executable and add it to PATH
ks_distribution=ks_0.13.1_linux_amd64
wget https://github.com/ksonnet/ksonnet/releases/download/v0.13.1/${ks_distribution}.tar.gz
tar -xvf ${ks_distribution}.tar.gz
chmod +x ${ks_distribution}/ks
export PATH="$PATH:$(pwd)/${ks_distribution}"

# download kubectl binary, make it executable and add it to PATH (via ks dir)
wget https://storage.googleapis.com/kubernetes-release/release/v1.14.0/bin/linux/amd64/kubectl
chmod +x kubectl
mv kubeflow ${ks_distribution}/kubectl

# Download kubeflow 0.5.1 source code
wget https://github.com/kubeflow/kubeflow/archive/v0.5.1.tar.gz -O kfctl.tar.gz
tar -xvf kfctl.tar.gz
kfctlsh="$(pwd)/kubeflow-0.5.1/scripts/kfctl.sh"

# provision kubeflow instance
export KFAPP="dla-kubeflow"
${kfctlsh} init ${KFAPP}
cd ${KFAPP}
${kfctlsh} generate all
${kfctlsh} apply all