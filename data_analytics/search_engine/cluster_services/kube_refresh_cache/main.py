__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.logging import logger
from ContainerClient import ContainerClient
from DANFUtils.exceptions import RESTException
from config.constants import DANF_VERSION_TAG


def main():
    client = ContainerClient()

    try:
        client.cache_origins()
    except RESTException as e:
        logger.exception(
            f"Failed to cache post request for caching origins. Reason: {e}"
        )

    try:
        client.cache_bagfile_size_duration_time()
    except RESTException as e:
        logger.exception(
            f"Failed to cache post request for caching bagfile size_duration_time. "
            f"Reason: {e}"
        )

    try:
        client.cache_childbagfile_size_duration_time()
    except RESTException as e:
        logger.exception(
            f"Failed to cache post request for caching childbagfile size_duration_time. "
            f"Reason: {e}"
        )

    try:
        client.cache_topics()
    except RESTException as e:
        logger.exception(
            f"Failed to cache post request for caching topics. Reason: {e}"
        )

    try:
        client.cache_vehicleids()
    except RESTException as e:
        logger.exception(
            f"Failed to cache post request for caching vehicleids. Reason: {e}"
        )


if __name__ == '__main__':
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    main()
