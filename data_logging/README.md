# Folder structure:

- hps : experimental High Performance Streaming (HPS) library (CMake)
- ros : ROS packages (Catkin)
  - jade : ROS jade packages
    - roscar : ROSCAR 1.0 package
  - melodic : ROS melodic packages
    - roscar : ROSCAR 1.0 package
    - src : DLA packages
      - rosstat : ROS package for extracting .bag file data rates
      - dla : DLA utility package
        - scripts : pyhon scripts
          - calc_topic_bw.py
          - talker.py
          - listener.py
        - src
          - talker.cpp
          - listener.cpp
- plog_ros_test_scripts : Test script for the PLog
- tmt_wrapper: python wrapper for the tmt library
# Prerequisites:

 ROS create.sh script requires ROS package mk
  sudo apt-get install ros-melodic-mk

To create all workspaces and build packages run:
 
```console
./create.sh
```

Don't forget to 
```console
source ros/melodic/devel/setup.bash
```
when working with specific ROS catkin workspace.

To start e.g. ROS talker and listener:
```console
roscore
```

```console
rosrun dla talker.py
```

```console
rosrun dla listener.py
```

# tmt
### Build tmt docker container
```console
docker build --network host -t tmt:latest .
```
 
### Use tmt docker container for bag-splitting
```console
docker run -v /lhome/rbaldau/data/tmt:/input -v /lhome/rbaldau/data/out:/output -e TMT_MODE=splitting -e TMT_CONFIG=/input/config_split_window.json tmt:latest 
```

### Use tmt docker container for telemotive file conversion
```console
docker run -v /lhome/rbaldau/data/tmt:/input -v /lhome/rbaldau/data/out:/output -e TMT_MODE=conversion -e TMT_CONFIG=/input/config_conversion.json - tmt:latest
```

### Use tmt docker container for bag-splitting (with DANF extractor request)
```console
docker run -v /lhome/rbaldau/data/tmt:/input -v /lhome/rbaldau/data/out:/output -e TMT_MODE=danf-splitting -e TMT_CONFIG=/input/config_split_danf.json -e INPUT_PATH=/input/20190613_150252_TC_3-03-01-01_01_STRIPPED_cid_bageval.bag -e OUTPUT_PATH=/output/test.bag tmt:latest 
```