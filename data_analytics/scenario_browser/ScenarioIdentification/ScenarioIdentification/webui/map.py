import plotly.graph_objects as go

map_layout = go.Layout(
#     uirevision = False,
    autosize = True,
    margin=go.layout.Margin(l=0, r=35, t=0, b=0),
    showlegend = True,
    mapbox = dict(
        #accesstoken=mapbox_access_token,
        bearing=0,
        center=go.layout.mapbox.Center(
            lat=37.323579, 
            lon=-121.922051
        ),
        pitch=0,
        zoom=13.5,
        style='carto-positron'                
    ),
    updatemenus=[
        dict(
            buttons=(
                [
                    dict(
                        args=[
                            {
                                "mapbox.zoom": 13.5,
                                "mapbox.center.lon": "-121.931958", #37.323292, -121.931958
                                "mapbox.center.lat": "37.323292",
                                "mapbox.bearing": 0,
                                "mapbox.style": "carto-positron",
                            }
                        ],
                        label="Reset Zoom",
                        method="relayout",
                    )
                ]
            ),
            direction="left",
            pad={"r": 0, "t": 0, "b": 0, "l": 0},
            showactive=False,
            type="buttons",
            x=0.45,
            y=0.02,
            xanchor="left",
            yanchor="bottom",
            bgcolor="#323130",
            borderwidth=1,
            bordercolor="#6d6d6d",
            font=dict(color="#FFFFFF"),
        )
    ],            
)