__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""
import json
import math
import numpy as np
import pandas as pd
from pkgutil import get_data
from pyproj import Proj, transform
# from abstract_scenario import GeneralScenarioExtractor
# import eval_logging.glog as log
# from scenario_extractor.scen_extr_util import Status, has_required_topics

## TODO:delete ##
import pdb
import rosbag
import argparse
import logging
import pickle
logging.basicConfig(level=logging.DEBUG)
################

# class ScenarioDynamicObjectCrossing(GeneralScenarioExtractor): # TODO: changed inheritance
class IntersectionsFeatureExtractor:
    """Scenario Extractor description"""

    def __init__(self, bagfile): # TODO: changed init
        """
        :param bagfile: instance of rosbag.Bag(bagfile_path)
        :param bagfile_path: string, path of the bagfile
        :param info: dictionary containing bagfile metadata as well as
                    info coming from the interesting_event_extractor
        :param bagfile_topics: set of bagfile topics
        """
        # super(ScenarioDynamicObjectCrossing, self).__init__(
        #     bagfile, bagfile_path, info, bagfile_topics)

        self.relevant_topics = {
            'localization_topic': '/localization/LocalizationService/0/LocDataInterface',
            'dynamicWorld_topic': '/fusion/FusionDynamicWorldService/0/DynamicWorld',
        }

        self.bagfile = bagfile
        # Get bag start and end times
        self.bag_start = self.bagfile.get_start_time()
        self.bag_end = self.bagfile.get_end_time()
        self.check_name = "scenario_dynamic_object_crossing"

        self.results = {
            "scenario_name": "dyn_obj_crossing",
            # "status": Status.UNSET,
            "description": "",
            "log_msg": ""
        }

        self.scalars = None
        self.tags = None

    def _rect_inter_inner(self, x1, x2):
        """docstring please"""
        n1 = x1.shape[0]-1
        n2 = x2.shape[0]-1
        X1 = np.c_[x1[:-1], x1[1:]]
        X2 = np.c_[x2[:-1], x2[1:]]
        S1 = np.tile(X1.min(axis=1), (n2, 1)).T
        S2 = np.tile(X2.max(axis=1), (n1, 1))
        S3 = np.tile(X1.max(axis=1), (n2, 1)).T
        S4 = np.tile(X2.min(axis=1), (n1, 1))
        return S1, S2, S3, S4

    def _rectangle_intersection_(self, x1, y1, x2, y2):
        """docstring please"""
        S1, S2, S3, S4 = self._rect_inter_inner(x1, x2)
        S5, S6, S7, S8 = self._rect_inter_inner(y1, y2)

        C1 = np.less_equal(S1, S2)
        C2 = np.greater_equal(S3, S4)
        C3 = np.less_equal(S5, S6)
        C4 = np.greater_equal(S7, S8)

        ii, jj = np.nonzero(C1 & C2 & C3 & C4)
        return ii, jj

    def intersection(self, x1, y1, x2, y2):
        """
        INTERSECTIONS Intersections of curves.
        Computes the (x,y) locations where two curves intersect.  The curves
        can be broken with NaNs or have vertical segments.

        usage:
        x,y=intersection(x1,y1,x2,y2)

            Example:
            a, b = 1, 2
            phi = np.linspace(3, 10, 100)
            x1 = a*phi - b*np.sin(phi)
            y1 = a - b*np.cos(phi)

            x2=phi
            y2=np.sin(phi)+2
            x,y=intersection(x1,y1,x2,y2)

            plt.plot(x1,y1,c='r')
            plt.plot(x2,y2,c='g')
            plt.plot(x,y,'*k')
            plt.show()

        """
        ii, jj = self._rectangle_intersection_(x1, y1, x2, y2)
        n = len(ii)

        dxy1 = np.diff(np.c_[x1, y1], axis=0)
        dxy2 = np.diff(np.c_[x2, y2], axis=0)

        T = np.zeros((4, n))
        AA = np.zeros((4, 4, n))
        AA[0:2, 2, :] = -1
        AA[2:4, 3, :] = -1
        AA[0::2, 0, :] = dxy1[ii, :].T
        AA[1::2, 1, :] = dxy2[jj, :].T

        BB = np.zeros((4, n))
        BB[0, :] = -x1[ii].ravel()
        BB[1, :] = -x2[jj].ravel()
        BB[2, :] = -y1[ii].ravel()
        BB[3, :] = -y2[jj].ravel()

        for i in range(n):
            try:
                T[:, i] = np.linalg.solve(AA[:, :, i], BB[:, i])
            except ValueError:
                T[:, i] = np.NaN

        in_range = (T[0, :] >= 0) & (T[1, :] >= 0) & (T[0, :] <= 1) & (T[1, :] <= 1)

        xy0 = T[2:, in_range]
        xy0 = xy0.T
        return xy0[:, 0], xy0[:, 1]

    def get_time_of_crossing(self, trackSample, ptX, ptY):
        """
        Given the track pandas dataframe (track) and intersection point (ptX, ptY)
        find the time of crossing of the track at ptX and ptY
        """
        minIdx = np.argmin(np.array((trackSample.X - ptX)**2 + (trackSample.Y - ptY)**2))
        if minIdx < len(trackSample["T"]):
            return trackSample["T"][minIdx]
        else:
            return np.nan

    class ObjPos(object):
        """docstring please"""
        def __init__(self, x, y, z, x_ecef, y_ecef, z_ecef,v_x,v_y,acc_x,acc_y, t):
            self.x = x
            self.y = y
            self.z = z
            self.t = t
            self.x_ecef = x_ecef
            self.y_ecef = y_ecef
            self.z_ecef = z_ecef
            self.v_x = v_x
            self.v_y = v_y
            self.acc_x = acc_x
            self.acc_y = acc_y
            loc_gps = self.lon_lat_from_ecef_proj(x_ecef, y_ecef, z_ecef)
            self.lon = loc_gps[0]
            self.lat = loc_gps[1]

        ecef_model = Proj(proj='geocent', ellps='WGS84', datum='WGS84')
        lla_model = Proj(proj='latlong', ellps='WGS84', datum='WGS84', radians=False)
        def lon_lat_from_ecef_proj(self, x_ecef, y_ecef, z_ecef):
            '''Transform ecef to lon lat using pyproj, returns (lon, lat)'''
            lon, lat, alt = transform(self.ecef_model, self.lla_model, *(x_ecef, y_ecef, z_ecef))
            return lon, lat

    class ObjTrack(object):
        """docstring please"""
        def __init__(self, label_id, tracking_id, object_labels):
            self.label_id = label_id
            self.tracking_id = tracking_id
            self.label = object_labels[self.label_id]
            self.track = []
        # helper functions, currently assumes the track information is ordered. -7.250169826671481

        def get_first_appearance(self):
            return self.track[0].t

        def get_last_appearance(self):
            return self.track[-1].t

        def get_track_duration(self):
            return self.track[-1].t - self.track[0].t

        def get_track_relative(self):
            """Returns the relative track as a np.array object (x,y,t)"""
            x_track = []
            y_track = []
            t = []
            for pos in self.track:
                x_track.append(pos.x)
                y_track.append(pos.y)
                t.append(pos.t)

            return pd.DataFrame({'X': x_track, 'Y': y_track, 'T': t})

        def get_track_ecef(self):
            """Returns the earth coordinate track as a np.array object (x,y,t)"""
            x_track = []
            y_track = []
            t = []
            for pos in self.track:
                x_track.append(pos.x_ecef)
                y_track.append(pos.y_ecef)
                t.append(pos.t)
            return pd.DataFrame({'X': x_track, 'Y': y_track, 'T': t})

        def get_track_gps(self):
            """Returns the gps coordinate of track as a np.array object (x,y) or (lon, lat, t)"""
            x_track = []
            y_track = []
            t = []
            v_x = []
            v_y = []
            acc_x = []
            acc_y = []
            for pos in self.track:
                x_track.append(pos.lon)
                y_track.append(pos.lat)
                t.append(pos.t)
                v_x.append(pos.v_x)
                v_y.append(pos.v_y)
                acc_x.append(pos.acc_x)
                acc_y.append(pos.acc_y)
                
            return pd.DataFrame({'X': x_track, 'Y': y_track, 'T': t,
                                 'V_x': v_x, 'V_y': v_y,
                                 'Acc_x': acc_x, 'Acc_y': acc_y})

    def get_tracks_and_objects(self, object_labels):
        """docstring please"""
        location_record_started = False
        msg_count = 0

        # Initialize track dictionary
        # Get tracks for all dynamic objects
        track_dict = {}     # <tracking_id, ObjTrack>

        # Set track_dict with key = -1 for ego-vehicle
        track_dict[-1] = self.ObjTrack(
            label_id=40,  # egoVehicle has label id=40.
            tracking_id=None,
            object_labels=object_labels
        )

        for topic, message, timestamp in self.bagfile.read_messages(topics=self.relevant_topics.values()):
            msg_count += 1

            if topic == self.relevant_topics['localization_topic']:
                location_record_started = True
                ecef = np.matrix(message.loc_state[0].ecef_T_front_axle).reshape(4, 4)
                ego_pos = self.ObjPos(
                    ecef[0, 3]/ecef[3, 3], ecef[1, 3]/ecef[3, 3], ecef[2, 3]/ecef[3, 3],
                    ecef[0, 3]/ecef[3, 3], ecef[1, 3]/ecef[3, 3], ecef[2, 3]/ecef[3, 3],
                    0,0,0,0, # relative velocity and accelaration is zero for ego
                    timestamp.to_sec() - self.bag_start
                )
                track_dict[-1].track.append(ego_pos)   # track_dict with key=-1 indicate ego vehicle

            elif topic == self.relevant_topics['dynamicWorld_topic']:
                if location_record_started:
                    object_set_at_t = set()
                    for obj in message.object_list:
                        object_set_at_t.add(obj.top_labels[0].id)

                        # fill up track_dict
                        DyObjX, DyObjY, DyObjZ = (obj.state_estimate.obb[6], obj.state_estimate.obb[7],
                                                  obj.state_estimate.obb[8])
                        DyObjEcef = np.matmul(np.array(ecef), np.array([DyObjX, DyObjY, DyObjZ, 1]))

                        #relative object velocity and acceleration
                        Vel_ObjX, Vel_ObjY = (obj.state_estimate.vel[0], obj.state_estimate.vel[1])
                        Accel_ObjX, Accel_ObjY = (obj.state_estimate.accel[0], obj.state_estimate.accel[1])
                        
                        current_pos = self.ObjPos(
                            DyObjX, DyObjY, DyObjZ,
                            DyObjEcef[0], DyObjEcef[1], DyObjEcef[2],
                            Vel_ObjX,Vel_ObjY,
                            Accel_ObjX,Accel_ObjY,
                            message.timestamp - self.bag_start
                        )

                        if obj.id in track_dict:
                            track_dict[obj.id].track.append(current_pos)
                        else:
                            track_dict[obj.id] = self.ObjTrack(
                                label_id=obj.top_labels[0].id,
                                tracking_id=obj.id,
                                object_labels=object_labels
                            )
                            track_dict[obj.id].track.append(current_pos)
        return track_dict

    def get_intersections(self, track_dict):
        """docstring please"""
        # TODO: previously track_dict_tmp[-1] .. but where is track_dict_tmp
        ego_tmp = track_dict[-1].get_track_gps()
        track_id = []
        label_id = []
        label = []
        T_delta = []
        lon_intersect = []
        lat_intersect = []
        # create intersections data frame
        for sub_id in track_dict:
            if sub_id < 0:
                continue
            subject = track_dict[sub_id]
            track_subject = subject.get_track_gps()
            label_id_subject = subject.label_id
            label_subject = subject.label
            # pdb.set_trace()
            loc_intersection = (np.array([]), np.array([]))
            lon_intersect_, lat_intersect_ = None, None
            try:
                loc_intersection = self.intersection(ego_tmp.X, ego_tmp.Y, track_subject.X, track_subject.Y)
            except Exception as err:
                logging.info('Intersection failed due to {}'.format((sub_id, err)))
            if loc_intersection[0].size > 0:
                t_intersect_ego = self.get_time_of_crossing(ego_tmp, loc_intersection[0][0], loc_intersection[1][0])
                t_intersect_subject = self.get_time_of_crossing(track_subject, loc_intersection[0][0],
                                                             loc_intersection[1][0])
                t_delta = t_intersect_subject - t_intersect_ego
                lon_intersect_ = loc_intersection[0][0]
                lat_intersect_ = loc_intersection[1][0]
            else:
                t_delta = np.Inf
            track_id.append(sub_id)
            label_id.append(label_id_subject)
            label.append(label_subject)
            T_delta.append(t_delta)
            lon_intersect.append(lon_intersect_)
            lat_intersect.append(lat_intersect_)

        intersections_df = pd.DataFrame(
            {
                'track_id': track_id,
                'label_id': label_id,
                'label': label,
                'T_delta': T_delta,
                'lon_intersect':lon_intersect,
                'lat_intersect': lat_intersect
            }
        )

        intersections_df = intersections_df[['track_id', 'label_id', 'label', 'T_delta', 'lon_intersect', 'lat_intersect']]
        return intersections_df

    @staticmethod
    def load_hierarchy_file(label_file_name):
        """Load json containing object labels list"""
        try:
            with open(label_file_name, 'r') as label_file:
                label_hierarchy = json.load(label_file)
        except (IOError, json.JSONEncoder) as exception:
            logging.error('\nError while reading from {} file: {}'
                      .format(label_file_name, exception))
            return
        return label_hierarchy

    def run(self):

        logging.info('reading label hierarchy')
        label_file_name =  './LabelHierarchy_1.8.json'
        label_hierarchy = self.load_hierarchy_file(label_file_name)
        if not label_hierarchy:
            # self.results["status"] = Status.UNSET
            self.results["log_msg"] = ('\nMissing label hierarchy file: {}.'
                                       .format(label_file_name))
            return self.results, self.scalars, self.tags

        # Generate object labels dictionary
        object_labels = {}
        for obj in label_hierarchy['classes']:
            object_labels[obj['unique_id']] = obj['name']

        logging.info('executing get_tracks_and_objects')
        tracks_and_objects = self.get_tracks_and_objects(object_labels)
        logging.info('executing get_intersections')
        intersections_df = self.get_intersections(tracks_and_objects)
        self.results['track_dict'] = tracks_and_objects
        self.results['intersections'] = intersections_df

        self.results['log_msg'] = 'Extractor completed'

        return self.results, self.scalars, self.tags



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--bag', help = 'input bag file')
    args = parser.parse_args()
    logging.info('Reading bag file')
    bag = rosbag.Bag(args.bag)
    logging.info('Bag file reading complete')
    scene_dyn_obj_crossing = ScenarioDynamicObjectCrossing(bag)
    logging.info('Executing run function')
    results, scalars, tags = scene_dyn_obj_crossing.run()
    logging.info('run function complete')
    with open('./debug_dyn_obj_reslts.json', 'w') as jsonout:
        json.dump(results, jsonout, cls=NestedEncoder)
    logging.info('JSON output complete')
