"""Constants"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

# bagfile data
EXTRACTOR_ID = "ext1"

# extraction request data
EXTRACTOR_ID_DISENGAGEMENT = "ext1_1"
EXTRACTOR_TYPE = "offline"
EXTRACTOR_VERSION = "0.0.2"

# bva values
BVA_DISENGAGEMENT = 10

# window size of events
SECONDS_BEFORE_DISENGAGEMENT = 15
SECONDS_AFTER_DISENGAGEMENT = 15

# Maximum length of the metadata list in one extraction request
MAX_LENGTH_METADATA_LIST = 1000
