#!/bin/sh
#this script scanns the sub directorys for iostat load and bandwidth files and
#generates the png plots to the locations
#call: $ ./analyse_runs.sh /lhome/cschob/ploggit/scripts/20180201/ "-G -s 0 -e 5"
# the parameter string "-G -s 0 -e 5" sets the unit of Gigabyte oder gigabit
# the start time and the endtime for generation of the plots
PARAMSTRING=$2
INTERFACE=$3
DIR_NAME=$4
if [ -z "$DIR_NAME" ]
then
    DIR_NAME="run*"
fi
find $1 -maxdepth 1 -mindepth 1 -name $DIR_NAME -type d | while read -r dir

do
    cd ${dir}
    echo "cd in ${dir}"
    if $(ls | grep -q iostat*.log)
    then
      ~/workspace/dla_repo/data_logging/plog_ros_test_scripts/analyse/analyse_iostat.py -i "${dir}"/"$(ls | grep iostat*.log)" -p ${PARAMSTRING}
    else
      echo "no iostat log found"
    fi
    if $(ls | grep -q load*.log)
    then
      ~/workspace/dla_repo/data_logging/plog_ros_test_scripts/analyse/analyse_load.py -i "${dir}"/"$(ls | grep load*.log)" -p ${PARAMSTRING}
    else
      echo "no load log found"
    fi
        if $(ls | grep -q bwm_${INTERFACE}*.log)
        then
          ~/workspace/dla_repo/data_logging/plog_ros_test_scripts/analyse/analyse_bandwidth.py -i "${dir}"/"$(ls | grep bwm_${INTERFACE}*.log)" -p ${PARAMSTRING} -d ${INTERFACE}
        else
          echo "no bandwidth ${INTERFACE} monitor log found"
        fi
        if $(ls | grep -q bandwidthethtool_${INTERFACE}*.log)
        then
	  ~/workspace/dla_repo/data_logging/plog_ros_test_scripts/analyse/analyse_bandwidth_stats.py -i "${dir}"/"$(ls | grep bandwidthethtool_${INTERFACE}*.log)" -p ${PARAMSTRING} -d ${INTERFACE}
        else
          echo "no bandwidth ${INTERFACE} from system stats log found"
        fi
        if $(ls | grep -q bandwidthsystem_${INTERFACE}*.log)
        then
          ~/workspace/dla_repo/data_logging/plog_ros_test_scripts/analyse/analyse_bandwidth_stats.py -i "${dir}"/"$(ls | grep bandwidthsystem_${INTERFACE}*.log)" -p ${PARAMSTRING} -d ${INTERFACE}
        else
          echo "no bandwidth ${INTERFACE} from ethtool stats log found"
        fi
	#only call the script in the second call of analyse_runs.sh	
	if [ $INTERFACE = "eth_log_2" ]
	then
          ~/workspace/dla_repo/data_logging/plog_ros_test_scripts/analyse/generate_final_plots.py -i "${dir}"/ -p ${PARAMSTRING}
	fi
    cd ..
done
