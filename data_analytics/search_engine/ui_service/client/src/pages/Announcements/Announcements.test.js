import React from 'react';
import Announcements from './Announcements';
import {shallow} from 'enzyme/build';

describe('Announcements page', () => {

  it('should render without crashing', () => {
    const wrapper = shallow(<Announcements/>);
    expect(wrapper.exists()).toBe(true);
  });

});