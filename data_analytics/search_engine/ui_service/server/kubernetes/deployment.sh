if [ $# -eq 0 ]
  then
    echo "Version argument missing!"
  else
    docker_image_version=$1

    # build docker image
    docker build -t cmtcdeu58434236:32455/ui-microservice:$docker_image_version . --no-cache --network host

    # push docker image to registry
    docker push cmtcdeu58434236:32455/ui-microservice:$docker_image_version

    # replace template + create / update deployment on Kubernetes cluster
    sed "s/{docker_image_version}/$docker_image_version/g" kubernetes/ui_service.yaml | kubectl  apply -f -
fi
