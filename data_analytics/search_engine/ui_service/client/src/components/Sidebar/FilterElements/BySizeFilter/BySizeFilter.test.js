import React from 'react';
import {shallow} from 'enzyme/build';
import {BySizeFilter} from './BySizeFilter';
import InputRange from 'react-input-range';

describe('BySizeFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      limits: {
        min: 0,
        max: 200
      },
      size: {
        min: 1000,
        max: 2000
      }
    };
    const wrapper = shallow(<BySizeFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should call action when range is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      limits: {
        min: 0,
        max: 200
      },
      size: {
        min: 1000,
        max: 2000
      },
      filterBySize: reduxAction
    };
    const wrapper = shallow(<BySizeFilter {...props} />);
    const rangeSlider = wrapper.find(InputRange);
    rangeSlider.simulate('onChange', {target: {value: { min: 1000, max:2000 }}} );
    rangeSlider.simulate('onChangeComplete');
    // expect(reduxAction).toHaveBeenCalled();
    // expect(wrapper.find(InputRange).props().value).toEqual({value: { min: '1', max:'2' }})
  })

});