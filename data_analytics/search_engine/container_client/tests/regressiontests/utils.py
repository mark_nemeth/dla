"""Test utils"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import random as rd
import numpy as np

from DANFUtils.utils import generate_deterministic_guid

rd.seed(0)
np.random.seed(0)


# testdata
def generate_testdata():
    bagfile_data = {
        "link": "/path/to/bagfile.bag",
        "size": 21231,
        "num_messages": 4444,
        "start": 1557847224000,
        "end": 1557847287000,
        "duration": 752728,
        "vehicle_id_num": "V-123-234",
        "extractor": "ext1",
        "version": "0.1",
        "file_name": "testdata",
        "file_type": "testdata",
        "tags": ["testdata"]
    }

    bagfile_data_post = {
        "link": "/path/to/bagfile.bag",
        "version": "0.1",
        "file_name": "testdata",
        "file_type": "testdata",
        "tags": ["testdata"]
    }

    bagfile_data_patch = {
        "size": 21231,
        "num_messages": 4444,
        "start": 1557847224000,
        "end": 1557847287000,
        "duration": 752728,
        "vehicle_id_num": "V-123-234",
        "extractor": "ext1",
        "file_name": "testdata",
        "file_type": "testdata",
        "tags": ["testdata"]
    }

    metadata_wrapper = {
        "extractor_id": 'ext1',
        "version": '0.1',
        "metadata_list":
            [
                {
                    "start": 1557847224000,
                    "end": 1557847263000,
                    "metadata": {
                        'rain': 1,
                        'fingerprint': 'ABC123456'
                    },
                    "bva": 5
                }
            ]
    }

    return bagfile_data, bagfile_data_post, bagfile_data_patch, metadata_wrapper

def generate_childbagfile_testdata():
    childbagfile_data = {
        "version": '0.1',
        "parent_guid": "233985c0-5f9d-4e46-9c7c-56fe58bce5ed",
        "start": 1571386447692,
        "end": 1571386457692,
        "parent_index_start": 1,
        "parent_index_end": 10,
        "metadata": {
            'fallback': {'ts': 1571386457692}
        }
    }
    return childbagfile_data

def generate_state_testdata():
    return {
        "name": "",
        "version": "",
        "time": 1475272800000,
        "status": "SUCCESS",
        "report": {
            "message": "All good"
        }
    }

def generate_custom_attributes():
    return {
        "custom_attributes": {
            "number_of_disenagement": 5,
            "total_engaged_distance": 12000,
            "key1": "value1",
            "key2": "value2"
        }
    }

def generate_test_sequence(index):
    return {
        "guid": generate_deterministic_guid(),
        "bagfile_guid": "233985c0-5f9d-4e46-9c7c-56fe58bce5ed",
        "index": index,
        "start": 1475272800000 + index * 1000,
        "end": 1475272800000 + index * 1000 + 999,
        "type": "basic",
        "extractors": {
            "ext1": {
                "bva": int(np.random.randint(1, 11))
            },
            "ext2": {
                "bva": int(np.random.randint(1, 11))
            },
            "ext3": {
                "bva": int(np.random.randint(1, 11))
            }
        }
    }