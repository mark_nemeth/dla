import React from 'react';
import Form from 'react-bootstrap/Form';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import {isObjectValid} from '../../../../utils/helperFunctions';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import './ByAccelerationFilter.scss';

export const STATIC_LIMITS = {
  min: -10,
  max: 10
};

export function ByAccelerationFilter(props) {

  function onRangeSelection(value) {
    if (isObjectValid(value)) props.filterByAcceleration(value);
    else props.filterByAcceleration({min: null, max: null})
  }

  function resetRangeSelection() {
    props.setCurrentAccelerationRange(props.limits);
    props.filterByAcceleration({min: null, max: null})
  }

  const subLabel = props.acceleration.min.toFixed(1) + ' - ' + props.acceleration.max.toFixed(1) + ' m/s2';
  const active = props.acceleration.min !== props.limits.min || props.acceleration.max !== props.limits.max;

  return (
    <CollapseContainer label={active ? "Acceleration: " + subLabel : "Acceleration"}
                       active={active}
                       resetCallback={resetRangeSelection}
    >
      <div className="track-container">
        <Form>
          <InputRange
            className="input-range"
            draggableTrack
            step={props.steps}
            maxValue={props.limits.max}
            minValue={props.limits.min}
            onChange={value => props.setCurrentAccelerationRange(value)}
            onChangeComplete={onRangeSelection}
            value={props.acceleration}
          />
        </Form>
      </div>
    </CollapseContainer>
  );
}