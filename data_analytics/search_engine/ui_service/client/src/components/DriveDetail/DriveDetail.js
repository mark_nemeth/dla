import React from 'react';
import {Container} from "react-bootstrap";
import DriveTimelineChart from "./DriveTimelineChart";
import DriveItemView from "./DriveItemView";

export const DriveItemTypes = Object.freeze({
  DRIVE: 'Drive',
  MISSION: 'Mission',
  ENGAGEMENT: 'Engagement',
  SYSTEM_RUN: 'System run',
  ON_DEMAND_RECORDING: 'On demand recording',
  ALWAYS_ON_RECORDING: 'Always on recording',
  ANNOTATION: 'Annotation'
});

export function DriveDetail(props) {
  return (
      <Container fluid className='drive-timeline-detail'>
        <DriveTimelineChart drive={props.drive}/>
        {props.selectedDriveItem && props.selectedDriveItemType &&
        <DriveItemView item={props.selectedDriveItem}
                       itemType={props.selectedDriveItemType}/>
        }
      </Container>
  )
}