"""Silent Testing Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from config.constants import *
from extractors.utils import timestamp2ms, aracom_timestamp2ms
from extractors.base_interpreter import Interpreter

BVA_ST = 10
SECONDS_BEFORE_SILENT_TESTING_EVENT = 1.5
SECONDS_AFTER_SILENT_TESTING_EVENT = 1.5
# see athena/common/interfaces/fusion_interfaces/include/fusion_interfaces/mops_mrc/mrc_plugin_debug_interface.h
# so far this list is empty, as we are interested in all events
SILENT_TESTING_TRIGGER_TO_SKIP = []
# wait for that many seconds after an extraction, to prevent spamming
BREAK_AFTER_SILENT_TESTING_EVENT = 60 


class MrcSilentTestingInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.last_silent_testing_event_ts = None
        self.bva = BVA_ST

    def feed_new_msg(self, topic, msg, t):
        silent_testing = {}
        silent_testing["ts"] = aracom_timestamp2ms(msg.header.timestamp)

        objects = []
        for trigger_event in msg.data.silent_testing_events:
            object = {}

            # testtrigger number from test cataloque,
            object["testcase"] = trigger_event.trigger_reason

            # check if vru or vehicle event
            if trigger_event.vru_box_valid:
                # position: [top, left, bottom, right]
                object["position"] = [
                    trigger_event.vru_bounding_box.top_left[0],
                    trigger_event.vru_bounding_box.top_left[1],
                    trigger_event.vru_bounding_box.bottom_right[0],
                    trigger_event.vru_bounding_box.bottom_right[1]
                ]
                # top class label and score
                object["label"] = trigger_event.vru_bounding_box.box_label
                object["score"] = trigger_event.vru_bounding_box.likelihood

                # body body yaw orienation if present 
                if len(trigger_event.vru_bounding_box.body_yaw_mixture) > 0:
                    object["orientation"] = trigger_event.vru_bounding_box.body_yaw_mixture[0].mean

                    
            elif trigger_event.vehicle_box_valid:                
                # 3D vehicle boxes are represented by isometry and extent
                object['sensor_T_object'] = trigger_event.vehicle_bounding_box.sensor_T_object
                object['extent'] = trigger_event.vehicle_bounding_box.extent

                object['label'] = trigger_event.vehicle_bounding_box.semantic_class.id
                object['score'] = trigger_event.vehicle_bounding_box.semantic_class.confidence


            else:
                # Empty trigger event, should not happen.
                # But then again, we might want to add trigger reasons some day, which do not relate to a measurement
                pass


            # append the object if the testcase should not be skipped:
            if object["testcase"] not in SILENT_TESTING_TRIGGER_TO_SKIP:
                objects.append(object)

        silent_testing["objects"] = objects
        if len(silent_testing["objects"]) > 0:
            if self.last_silent_testing_event_ts is None \
                    or silent_testing["ts"] - self.last_silent_testing_event_ts > BREAK_AFTER_SILENT_TESTING_EVENT * 1e3:
                silent_testing = self.calc_start_end_ts_snippet(silent_testing)
                self.event_list.append(silent_testing)
                self.last_silent_testing_event_ts = silent_testing["ts"]

        return self.event_list

    @staticmethod
    def calc_start_end_ts_snippet(event):
        event["start_ts"] = int(event["ts"] - (SECONDS_BEFORE_SILENT_TESTING_EVENT * 1e3))
        event["end_ts"] = int(event["ts"] + (SECONDS_AFTER_SILENT_TESTING_EVENT * 1e3))
        return event
