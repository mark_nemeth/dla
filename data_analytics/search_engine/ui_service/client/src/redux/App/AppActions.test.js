import * as actions from './AppActions';
import configHandler from '../../config/configHandler';
import axios from 'axios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import MockAdapter from 'axios-mock-adapter';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const mock = new MockAdapter(axios);

describe('App Actions', () => {

  describe('error handling actions ', () => {

    it('show error', () => {
      const errorMessage = 'Test error message !';
      const expectedAction = {
        type: actions.showError.type,
        payload: errorMessage
      };

      expect(actions.showError(errorMessage)).toEqual(expectedAction);
    });

    it('hide error', () => {
      const expectedAction = {
        type: actions.hideError.type,
        payload: undefined
      };

      expect(actions.hideError()).toEqual(expectedAction);
    });
  });

  describe('load user data action ', () => {

    const apiPath = '/api/v1/user';
    configHandler.getUserDataAPI = jest.fn(() => {
      return {url: apiPath}
    });
    const mockBagRequest = mock.onGet(apiPath);

    it('creates fetchUserDataSuccess action when fetching user succeeded', () => {
      const userPayload = {name: 'danfuser@gmail.com', admin: false};
      mockBagRequest.reply(200, userPayload);

      const expectedActions = [{
        type: actions.fetchUserDataSuccess.type,
        payload: userPayload
      }];

      const store = mockStore({});
      return store.dispatch(actions.fetchUserData()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });

    it('creates fetchUserDataError action when fetching user failed', () => {
      mockBagRequest.reply(418, {});

      const expectedActions = [{
        type: actions.fetchUserDataError.type,
        payload: undefined
      }];

      const store = mockStore({});
      return store.dispatch(actions.fetchUserData()).then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
    });
  });
});