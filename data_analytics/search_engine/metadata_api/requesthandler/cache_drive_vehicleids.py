"""Search Engine REST API caching vehicleIds route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import logging

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing

from config.config_handler import config_dict
from handlers.auth import auth
from handlers.cache_distinct_values_handler import CacheDistinctValuesHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler

logger = logging.getLogger(__name__)


class CacheBagfileVehicleIDsRequestHandler(BaseRequestHandler):
    def initialize(self):
        """
        Hook for vehicleID Cache RequestHandler initialization

        :return: None
        """
        try:
            schema_guid = open(config_dict.get_value("schema_guid", "")).read()
            logger.info(
                f'type of file: {config_dict.get_value("schema_guid", "")}')

        except (IOError, OSError):
            error_message = 'Could not read schema'
            logger.exception(error_message)
            return

        try:
            self.schema_guid = json.loads(schema_guid)
        except ValueError:
            error_message = 'Bad schema: unable to parse schema'
            logger.exception(error_message)
            return

        self.caching_handler = CacheDistinctValuesHandler('drives', 'vehicle_id_num')

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        """
          Hook for cache vehicleIds POST method
          :return: None
          """
        #################################
        ##### Request Interpretation ####
        #################################
        tracing.start_task_in_context('cachevehicleid/post',
                                      context_headers=self.request.headers)

        #################################
        ####### Process Request #########
        #################################
        try:
            res = self.caching_handler.update_cache()
        except Exception as e:
            error_message = 'Server Error: ' + str(e)
            tracing.end_task(success=False)
            logger.exception(error_message)
            utils.build_response(self,
                                 utils.build_error_from_message(error_message),
                                 None,
                                 constants["REQ_STATUS_CODE_SERVER_ERROR"])

            return

        ################################
        ########## RESPONSE ############
        ################################

        logger.info("request served successfully")
        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_RESOURCE_CREATED"])

    def _handle_error(self, error_msg, status_code):
        logger.exception(error_msg)
        tracing.end_task(success=False)
        utils.build_response(self, utils.build_error_from_message(error_msg),
                             None, status_code)
