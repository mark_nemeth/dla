"""Utility scripts."""

__copyright__ = '''
COPYRIGHT: (c) 2017 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


def ts_to_float(timestamp):
    """Converts a timestamp with seconds and nanosecond part to float"""
    return timestamp.s + timestamp.ns / 1000000000.0


def message_time_delta(message_1, message_2, header=False):
    """Calculates the time difference between two ros messages based
    on the header timestamps (if provided) or from timestamp fields.
    return: message time delta as float
    """
    if header:
        timestamp_1 = message_1
        timestamp_2 = message_2
    else:
        timestamp_1 = message_1.header.timestamp
        timestamp_2 = message_2.header.timestamp

    return ts_to_float(timestamp_2) - ts_to_float(timestamp_1)


def charlist2str(chrlist):
    """Converts list of integers into a string assuming the integers represent
    chars"""
    list_of_char = [chr(x) for x in chrlist]
    string = ''.join(list_of_char).rstrip('\0')
    return string


def ts_to_hour(timestamp):
    """Converts a timestamp to hour"""
    return timestamp / 3600.0


def remove_invalid_scalars(scalars):
    """Remove scalars that have None, nan or +/-inf value,
    as these are invalid json characters."""
    remove_scalars_ind = []
    for ind in range(len(scalars)):
        val = str(scalars[ind]['value'])
        if any(x in val for x in ['nan', 'None', 'inf', 'null']):
            remove_scalars_ind.append(ind)
    for ind in sorted(remove_scalars_ind, reverse=True):
        del scalars[ind]
    return scalars
