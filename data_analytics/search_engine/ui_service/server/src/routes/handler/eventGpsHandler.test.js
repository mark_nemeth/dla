import eventGpsHandler from './eventGpsHandler';

let childBagfilesWithEvents;
let childBagfilesWithEventCoords;
let childBagfilesWithDifferentStructure;

beforeAll(() => {
  childBagfilesWithEvents = [
    {
      "version": "1.0",
      "guid": "00000000-0000-0000-0000-000000000001",
      "parent_guid": "00000000-0000-0000-0000-000000000002",
      "start": 1564600000000,
      "end": 1564600005000,
      "parent_index_start": 1,
      "parent_index_end": 2,
      "bva_max": 10,
      "metadata": {
        "ext1_1": {
          "disengagement": [
            {
              "ts": 1564600002200
            }
          ]
        },
        "ext1_3": {
          "gps": [
            {
              "latitude": 47.9100000000,
              "longitude": 8.7100000000,
              "ts": 1564600001000
            },
            {
              "latitude": 47.9200000000,
              "longitude": 8.7200000000,
              "ts": 1564600002000
            },
            {
              "latitude": 47.9300000000,
              "longitude": 8.7300000000,
              "ts": 1564600003000
            },
            {
              "latitude": 47.9400000000,
              "longitude": 8.7400000000,
              "ts": 1564600004000
            }
          ]
        }
      }
    },
    {
      "version": "1.0",
      "guid": "00000000-0000-0000-0000-000000000001",
      "parent_guid": "00000000-0000-0000-0000-000000000002",
      "start": 1564600000000,
      "end": 1564600005000,
      "parent_index_start": 1,
      "parent_index_end": 2,
      "bva_max": 10,
      "metadata": {
        "ext1_3": {
          "gps": [
            {
              "latitude": 47.9100000000,
              "longitude": 8.7100000000,
              "ts": 1564600001000
            },
            {
              "latitude": 47.9200000000,
              "longitude": 8.7200000000,
              "ts": 1564600002000
            },
            {
              "latitude": 47.9300000000,
              "longitude": 8.7300000000,
              "ts": 1564600003000
            },
            {
              "latitude": 47.9400000000,
              "longitude": 8.7400000000,
              "ts": 1564600004000
            }
          ]
        },
        "ext1_2": {
          "silent_testing": [
            {
              "ts": 1564600003200
            },
            {
              "ts": 1564600003900
            }
          ]
        }
      }
    }];
  childBagfilesWithEventCoords = [
    {
      "version": "1.0",
      "guid": "00000000-0000-0000-0000-000000000001",
      "parent_guid": "00000000-0000-0000-0000-000000000002",
      "start": 1564600000000,
      "end": 1564600005000,
      "parent_index_start": 1,
      "parent_index_end": 2,
      "bva_max": 10,
      "metadata": {
        "ext1_1": {
          "disengagement": [
            {
              "ts": 1564600002200,
              "latitude": 47.9200000000,
              "longitude": 8.7200000000
            }
          ]
        },
        "ext1_3": {
          "gps": [
            {
              "latitude": 47.9100000000,
              "longitude": 8.7100000000,
              "ts": 1564600001000
            },
            {
              "latitude": 47.9200000000,
              "longitude": 8.7200000000,
              "ts": 1564600002000
            },
            {
              "latitude": 47.9300000000,
              "longitude": 8.7300000000,
              "ts": 1564600003000
            },
            {
              "latitude": 47.9400000000,
              "longitude": 8.7400000000,
              "ts": 1564600004000
            }
          ]
        }
      }
    },
    {
      "version": "1.0",
      "guid": "00000000-0000-0000-0000-000000000001",
      "parent_guid": "00000000-0000-0000-0000-000000000002",
      "start": 1564600000000,
      "end": 1564600005000,
      "parent_index_start": 1,
      "parent_index_end": 2,
      "bva_max": 10,
      "metadata": {
        "ext1_3": {
          "gps": [
            {
              "latitude": 47.9100000000,
              "longitude": 8.7100000000,
              "ts": 1564600001000
            },
            {
              "latitude": 47.9200000000,
              "longitude": 8.7200000000,
              "ts": 1564600002000
            },
            {
              "latitude": 47.9300000000,
              "longitude": 8.7300000000,
              "ts": 1564600003000
            },
            {
              "latitude": 47.9400000000,
              "longitude": 8.7400000000,
              "ts": 1564600004000
            }
          ]
        },
        "ext1_2": {
          "silent_testing": [
            {
              "ts": 1564600003200,
              "latitude": 47.9300000000,
              "longitude": 8.7300000000
            },
            {
              "ts": 1564600003900,
              "latitude": 47.9400000000,
              "longitude": 8.7400000000
            }
          ]
        }
      }
    }];
  childBagfilesWithDifferentStructure = [
    {
      "version": "1.0",
      "guid": "00000000-0000-0000-0000-000000000001",
      "parent_guid": "00000000-0000-0000-0000-000000000002",
      "start": 1564600000000,
      "end": 1564600005000,
      "parent_index_start": 100,
      "parent_index_end": 101,
      "bva_max": 10,
      "metadata": {
        "disengagement": {
          "ts": 1564600003100,
          "type": "Manual",
          "error_msg": "[51]Distronic lever pushed; "
        }
      }
    }];
});

test('The most relevant GPS coords should be added to disengagement- and silent testing events.', () => {
  expect(eventGpsHandler.addGpsToEvents(childBagfilesWithEvents))
    .toStrictEqual(childBagfilesWithEventCoords);
});

test('ChildBagfiles with different structure should be unaffected.', () => {
  expect(eventGpsHandler.addGpsToEvents(childBagfilesWithDifferentStructure))
    .toStrictEqual(childBagfilesWithDifferentStructure);
});