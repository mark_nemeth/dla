"""Regressiontests for snippet bva route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from .testutils import get_testdb_connector

base_url = 'http://localhost:8889'
search_childbag_url = base_url + '/search/childbagfile'
bva_snippet_url = base_url + '/snippet/bva/'

bagfile_guid = '4bed6407-a01d-424c-b031-21a4b618b555'
child_guid = generate_deterministic_guid()

child_1 = {
    "version": "1.0",
    "guid": child_guid,
    "parent_guid": bagfile_guid,
    "link": "testdata",
    "file_name": "testdata_child1.bag",
    "start": 1558962320000,
    "end": 1558962372000,
    "parent_index_start": 169,
    "parent_index_end": 220,
    "size": 32768,
    "vehicle_id_num": "V-123-234",
    "processing_state": "NOT_PROCESSED",
    "metadata": {
        'ext1_4': {
            'stereo_cnn': [{
                'bounding_box_labels': {
                    '1': 2
                }
            }, {
                'bounding_box_labels': {
                    '9': 3
                }
            }, {
                'bounding_box_labels': {
                    '4': 1
                }
            }]
        }
    },
    "bva": 7,
    "is_test_data": 1
}

child_2 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "parent_guid": bagfile_guid,
    "link": "testdata",
    "file_name": "testdata_child2.bag",
    "start": 1558962402000,
    "end": 1558962458000,
    "parent_index_start": 251,
    "parent_index_end": 306,
    "size": 65536,
    "vehicle_id_num": "V-123-777",
    "extractor_id": "ext1",
    "bva": 8,
    "is_test_data": 1
}

child_3 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "size": 90000,
    "parent_guid": bagfile_guid,
    "link": "testdata",
    "file_name": "testdata_child3.bag",
    "start": 1558962597000,
    "end": 1558962628000,
    "parent_index_start": 446,
    "parent_index_end": 476,
    "vehicle_id_num": "V-123-777",
    "metadata": {
        'ext1_4': {
            'stereo_cnn': [{
                'bounding_box_labels': {
                    '8': 2
                }
            }, {
                'bounding_box_labels': {
                    '8': 3
                }
            }, {
                'bounding_box_labels': {
                    '4': 1
                }
            }]
        }
    },
    "bva": 9,
    "is_test_data": 1
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['child_bagfiles'].delete_many({'is_test_data': 1})
        con.add_child_bagfiles([child_1, child_2, child_3])


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['child_bagfiles'].delete_many({'is_test_data': 1})


class TestSearchChildBagfilesGET(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_guid(self):
        query_data = {'parent_guid': bagfile_guid}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search by parent guid failed')
        self.assertCountEqual([child_1, child_2, child_3], res_json,
                              'Child Bagfile search by parent guid failed')


    def test_search_by_child_guid(self):
        query_data = {'guid': child_guid}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code,
                         'Child Bagfile search by child guid failed')
        self.assertCountEqual([child_1], res_json,
                              'Child Bagfile search by child guid failed')

    def test_search_by_processing_state(self):
        query_data = {'processing_state': 'NOT_PROCESSED'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code,
                         'Childbagfile search by processing_state failed')
        self.assertCountEqual([child_1], res_json,
                              'Childbagfile search by processing_state failed')


    def test_search_by_file_path(self):
        query_data = {'file_path': 'testdata'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.assertCountEqual([child_1, child_2, child_3], res_json,
                              'Bagfile search by file path failed')

    def test_non_case_sensitive_search_by_file_path(self):
        query_data = {'file_path': 'TESTDATA'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file path failed')
        self.assertCountEqual([child_1, child_2, child_3], res_json,
                              'Bagfile search by file path failed')

    def test_search_by_file_name(self):
        query_data = {'file_name': 'testdata_child2'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file name failed')
        self.assertCountEqual([child_2], res_json,
                              'Bagfile search by file name failed')

    def test_non_case_sensitive_search_by_file_name(self):
        query_data = {'file_name': 'testdata_CHILD2'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search by file name failed')
        self.assertCountEqual([child_2], res_json,
                              'Bagfile search by file name failed')

    def test_search_by_vehicle_id_num(self):
        query_data = {'vehicle_id_num': 'V-123-777'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search by vehicle_id_num failed')
        self.assertCountEqual([child_2, child_3], res_json,
                              'Child bagfile search by vehicle_id_num failed')

    def test_search_by_multiple_vehicle_id_num(self):
        query_data = {"vehicle_id_num": ["V-123-234", "V-123-777"]}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(
            200, res.status_code,
            'Child bagfile search by multiple vehicle_id_num GET request failed'
        )
        self.assertCountEqual(
            [child_1, child_2, child_3], res_json,
            'Child bagfile search using multiple vehicle_id_num GET request failed'
        )

    def test_search_by_event(self):
        query_data = {"event": "metadata.ext1_4", "vehicle_id_num": ["V-123-234", "V-123-777"]}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search by event failed')
        self.assertCountEqual([child_1, child_3], res_json,
                              'Child bagfile search by event failed')

    def test_search_by_multiple_event(self):
        query_data = {"event": ["metadata.ext1_4.stereo_cnn", "metadata.ext1_4"], 'file_path': 'testdata'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search by event failed')
        self.assertCountEqual([child_1, child_3], res_json,
                              'Child bagfile search by event failed')

    def test_search_by_non_present_event(self):
        query_data = {"event": ["metadata.ext1_4.dummy"], 'file_path': 'testdata'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search by event failed')
        self.assertCountEqual([],  res_json,
                              'Child bagfile search by event failed')

    def test_search_by_any_event(self):
        query_data = {"event": ["metadata.ext1_4.stereo_cnn", "metadata.ext1_1.disengagement"], 'file_path': 'testdata'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search by event failed')
        self.assertCountEqual([child_1, child_3], res_json,
                              'Child bagfile search by event failed')


    def test_search_limit_sort(self):
        query_data = {
            'parent_guid': bagfile_guid,
            'limit': 2,
            'sortby': 'start'
        }
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']
        total = res.json()['total']

        self.assertEqual(3, total, 'Child Bagfile search limit sort result count failed.')

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search search limit sort failed')
        self.assertCountEqual([child_1, child_2], res_json,
                              'Child Bagfile search limit sort failed')

    def test_search_sort_by_size(self):
        query_data = {'parent_guid': bagfile_guid, 'sortby': 'size'}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search by file path failed')
        self.assertCountEqual([child_2], [res_json[1]],
                              'Child Bagfile search by file path failed')

    def test_search_invalid_limit(self):
        query_data = {
            'parent_guid': bagfile_guid,
            'limit': 'invalid',
            'sortby': 'start'
        }
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search by file path failed')
        self.assertCountEqual([child_1, child_2, child_3], res_json,
                              'Child Bagfile search by file path failed')

    def test_search_offset_descending_sort(self):
        query_data = {
            'parent_guid': bagfile_guid,
            'offset': 1,
            'sortby': '-start'
        }
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search by file path failed')
        self.assertCountEqual([child_1, child_2], res_json,
                              'Child Bagfile search by file path failed')

    def test_search_by_empty_data(self):
        query_data = {}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search by empty data failed')

    def test_search_by_cv1_classifier(self):
        prepare_db()
        query_data = {'cv1_classifier': 1}
        res = requests.get(url=search_childbag_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search for CV1 Classifier failed')
        self.assertCountEqual([child_1], res_json,
                              'Child Bagfile search for CV1 Classifier failed')
        cleanup_db()


class TestSearchChildBagfilesPOST(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_bva_and_guid(self):
        query_data = {'bva_min': 8, "parent_guid": bagfile_guid}
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search using bva and guid failed')
        self.assertCountEqual([child_2, child_3], res_json,
                              'Child Bagfile search using bva and guid failed')

    def test_search_by_guid(self):
        query_data = {"parent_guid": bagfile_guid}
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search using parent guid failed')
        self.assertCountEqual([child_1, child_2, child_3], res_json,
                              'Child Bagfile search using parent guid failed')

    def test_search_by_child_guid(self):
        query_data = {"guid": child_guid}
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child Bagfile search using child_guid failed')
        self.assertCountEqual([child_1], res_json,
                              'Child Bagfile search using child_guid failed')

    def test_case_insensitive_regex_search_by_file_path(self):
        query_data = {'file_path': 'TESTDATA'}
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Bagfile search using file path failed')
        self.assertCountEqual([child_1, child_2, child_3], res_json,
                              'Bagfile search using file path failed')

    def test_case_insensitive_regex_search_by_file_name(self):
        query_data = {'file_name': 'testdata_child3.bag'}
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']
        print(res_json)

        self.assertEqual(200, res.status_code,
                         'Bagfile search using file name failed')
        self.assertCountEqual([child_3], res_json,
                              'Bagfile search using file name failed')

    def test_search_by_invalid_bva(self):
        query_data = {'bva_min': 'a'}
        res = requests.post(url=search_childbag_url, json=query_data)

        self.assertEqual(
            400, res.status_code,
            'Child Bagfile search using invalid bva not detected')

    def test_search_by_invalid_guid(self):
        query_data = {"parent_guid": '4bed6407-a01d-424c-b031-21a4b618b55x'}
        res = requests.post(url=search_childbag_url, json=query_data)

        self.assertEqual(
            400, res.status_code,
            'Child Bagfile search using invalid parent guid not detected')

    def test_search_by_nested_structure(self):
        query_data = {'$and': [{'bva': {'$gt': 5}}, {'metadata': {}}]}
        res = requests.post(url=search_childbag_url, json=query_data)

        self.assertEqual(
            400, res.status_code,
            'Child Bagfile search using nested structure not detected')

    def test_search_by_multiple_vehicle_id_num(self):
        query_data = {
            '$or': [{
                'vehicle_id_num': 'V-123-234'
            }, {
                'vehicle_id_num': 'V-123-777'
            }]
        }
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search using vehicle_id_num failed')
        self.assertCountEqual(
            [child_1, child_2, child_3], res_json,
            'Child bagfile search using vehicle_id_num failed')

    def test_search_by_multiple_vehicle_id_num_in(self):
        query_data = {'vehicle_id_num': ['V-123-234', 'V-123-777']}
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code,
                         'Child bagfile search using vehicle_id_num failed')
        self.assertCountEqual(
            [child_1, child_2, child_3], res_json,
            'Child bagfile search using vehicle_id_num failed')

    def test_search_by_empty_data(self):
        query_data = {}
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code,
                         'Child Bagfile search using empty data failed')

        self.assertGreaterEqual(
            len(res_json), 3, 'Child Bagfile search using empty data failed')

    def test_search_by_cv1_classifier(self):
        prepare_db()
        query_data = {'cv1_classifier': 4}
        res = requests.post(url=search_childbag_url, json=query_data)
        res_json = res.json()['results']
        self.assertEqual(200, res.status_code,
                         'Child Bagfile search for CV1 Classifier failed')
        self.assertCountEqual([child_1, child_3], res_json,
                              'Child Bagfile search for CV1 Classifier failed')
        cleanup_db()


