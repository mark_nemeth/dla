__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''


#dla_converter text format
def dla_conv_format(output_txt: str):
    _last_key = ''
    topics = []
    fmt_dict = {}
    for entry in output_txt.splitlines():
        if _last_key == 'topics' and not entry.startswith('Finished in'):
            topics += [entry.strip()]

        elif entry.startswith('Topics'):
            _last_key = 'topics'
            entry_dict = {'topics_count': entry.split(':')[1].strip()}
            fmt_dict = {**fmt_dict, **entry_dict}

        elif entry.startswith('Finished in'):
            _last_key = 'finished_in'
            entry_dict = {'finished_in': entry.split(' in')[1].strip()}
            fmt_dict = {**fmt_dict, **entry_dict}

        else:
            _last_key = 'others'
            try:
                entry_dict = {entry.split(':')[0].strip().lower(): entry.split(':')[1].strip()}
                fmt_dict = {**fmt_dict, **entry_dict}

            except:
                print(entry)
    if topics:
        topics_dict = {'topics': topics}
        fmt_dict = {**fmt_dict, **topics_dict}
    return fmt_dict

def text2list_format(txt: str):
    return txt.split('\n') if txt is not None else ''
