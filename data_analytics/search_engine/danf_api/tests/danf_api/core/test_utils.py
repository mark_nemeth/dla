from fastapi import FastAPI
from fastapi.testclient import TestClient
from pydantic import BaseModel, Field

app = FastAPI()


class Model(BaseModel):
    name: str = Field(alias="alias")


class ModelNoAlias(BaseModel):
    name: str

    class Config:
        schema_extra = {
            "description": (
                "response_model_by_alias=False is basically a quick hack, to support "
                "proper OpenAPI use another model with the correct field names"
            )
        }


@app.get("/by-alias/model", response_model=Model)
def by_alias_model():
    return Model(alias="Foo")


openapi_schema = {
    "openapi": "3.0.2",
    "info": {"title": "FastAPI", "version": "0.1.0"},
    "paths": {
        "/dict": {
            "get": {
                "summary": "Read Dict",
                "operationId": "read_dict_dict_get",
                "responses": {
                    "200": {
                        "description": "Successful Response",
                        "content": {
                            "application/json": {
                                "schema": {"$ref": "#/components/schemas/Model"}
                            }
                        },
                    }
                },
            }
        },
        "/model": {
            "get": {
                "summary": "Read Model",
                "operationId": "read_model_model_get",
                "responses": {
                    "200": {
                        "description": "Successful Response",
                        "content": {
                            "application/json": {
                                "schema": {"$ref": "#/components/schemas/Model"}
                            }
                        },
                    }
                },
            }
        },
        "/by-alias/model": {
            "get": {
                "summary": "By Alias Model",
                "operationId": "by_alias_model_by_alias_model_get",
                "responses": {
                    "200": {
                        "description": "Successful Response",
                        "content": {
                            "application/json": {
                                "schema": {"$ref": "#/components/schemas/Model"}
                            }
                        },
                    }
                },
            }
        },
    },
    "components": {
        "schemas": {
            "Model": {
                "title": "Model",
                "required": ["alias"],
                "type": "object",
                "properties": {"alias": {"title": "Alias", "type": "string"}},
            },
        }
    },
}

client = TestClient(app)


def test_openapi_schema():
    response = client.get("/openapi.json")
    assert response.status_code == 200, response.text
    assert response.json() == openapi_schema


def test_read_model():
    response = client.get("/model")
    assert response.status_code == 200, response.text
    assert response.json() == {"name": "Foo"}


def test_read_model_by_alias():
    response = client.get("/by-alias/model")
    assert response.status_code == 200, response.text
    assert response.json() == {"alias": "Foo"}
