import {connect} from 'react-redux';
import {DriveInformationSection} from './DriveInformationSection';
import {showError} from '../../redux/App/AppActions';
import {highlightSelectedChildBagfile} from "../../redux/Overview/OverviewActions";

function mapStateToProps(state, ownProps) {
  const fileType = ownProps.fileType;
  const bagfile = state.overview.bagfile || null;
  const childbagfiles = state.overview.childbagfiles || [];
  const selectedChildbagfileGuid = state.overview.selectedChildbagfileGuid || null;
  const overviewFile = bagfile;
  const displayedLink = overviewFile.current_link || overviewFile.link;
  const flowRuns = state.overview.flowRuns || [];

  return {fileType, overviewFile, parent: bagfile, childbagfiles, displayedLink, selectedChildbagfileGuid, flowRuns};
}

export default connect(
    mapStateToProps,
    {highlightSelectedChildBagfile, showError}
)(DriveInformationSection);