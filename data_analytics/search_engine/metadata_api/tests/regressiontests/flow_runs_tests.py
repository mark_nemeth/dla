"""Regressiontests for flow run routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import random as rd
import unittest
from copy import deepcopy

import requests
from DANFUtils.utils import generate_deterministic_guid

from tests.regressiontests.testutils import get_testdb_connector

# urls
base_url = 'http://localhost:8887'
flow_runs_url = base_url + '/flow-runs'
flow_runs_step_runs_url_template = flow_runs_url + '/{}/steps/{}/runs'

rd.seed("test")

raw_data_flow = {
    "guid": generate_deterministic_guid(),
    "name": "danf-integration-test-flow",
    "flow_version": "1",
    "kubeflow_experiment_id": "11111111-1111-1111-1111-111111111111",
    "steps": [
        {"name": "danf-extractor"},
        {"name": "danf-splitter"}
    ]
}

raw_data_flow_run_create = {
    "reference": {
        "type": "bagfile_guid",
        "value": "11111111-1111-1111-1111-111111111111"
    },
    "flow_name": raw_data_flow["name"],
    "flow_version": raw_data_flow["flow_version"],
    "steps": [
        {
            "name": "danf-extractor",
            "image": "danf-extractorimg",
            "tag": "1.0",
            "is_essential": True,
            "max_retries": 3
        },
        {
            "name": "danf-splitter",
            "image": "danf-splitterimg",
            "tag": "5.1",
            "is_essential": True,
            "max_retries": 3
        }
    ]
}

raw_data_flow_run = {
    "version": "2.0",
    "guid": generate_deterministic_guid(),
    "started_at": 1577971500000,
    "status": "IN_PROGRESS",
    "reference": {
        "type": "bagfile_guid",
        "value": "11111111-1111-1111-1111-111111111111"
    },
    "flow_name": raw_data_flow["name"],
    "flow_version": raw_data_flow["flow_version"],
    "steps": [
        {
            "name": "danf-extractor",
            "image": "danf-extractorimg",
            "tag": "1.0",
            "max_retries": 3,
            "is_essential": True,
            "status": "PENDING",
            "runs": []
        },
        {
            "name": "danf-splitter",
            "image": "danf-splitterimg",
            "tag": "5.1",
            "max_retries": 3,
            "is_essential": True,
            "status": "PENDING",
            "runs": []
        }
    ]
}

raw_data_flow_run_step_run = {
    "started_at": 946684800010,
    "completed_at": 4133980798888,
    "status": "SUCCESSFUL",
    "report": {"message": "foo"}
}


def prepare_db():
    with get_testdb_connector() as con:
        con.db['flows'].delete_many({'name': raw_data_flow['name']})
        con.db['flow_runs'].delete_many({'reference.value': raw_data_flow_run['reference']['value']})
        con.add_flows([raw_data_flow])


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['flows'].delete_many({'name': raw_data_flow['name']})
        con.db['flow_runs'].delete_many({'reference.value': raw_data_flow_run['reference']['value']})


class TestFlowRunsPOST(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_flow_runs_POST_when_valid_flow_run_should_create_successful(self):
        data = deepcopy(raw_data_flow_run_create)

        res = requests.post(url=flow_runs_url, json=data)
        self.assertEqual(201, res.status_code, 'Flow run creation failed')
        location = res.headers['Location']
        self.assertRegex(
            location,
            '/flow-runs/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_flow_runs_POST_when_input_parameter_is_invalid_should_return_400(self):
        data = deepcopy(raw_data_flow_run_create)
        data['reference']['type'] = "xxx"

        res = requests.post(url=flow_runs_url, json=data)
        self.assertEqual(400, res.status_code, 'Invalid bagfile_guid not detected')

    def test_flow_runs_POST_when_required_parameter_is_missing_should_return_400(self):
        data = deepcopy(raw_data_flow_run_create)
        del data['reference']

        res = requests.post(url=flow_runs_url, json=data)
        self.assertEqual(400, res.status_code, 'Missing param "bagfile_guid" not detected')


class TestFlowRunGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_GET_flow_run_when_flow_run_exists_should_return_it(self):
        data = deepcopy(raw_data_flow_run)
        with get_testdb_connector() as con:
            con.add_flow_runs([data])

        res = requests.get(url=f"{flow_runs_url}/{raw_data_flow_run['guid']}")

        data['kubeflow_workflow_name'] = None
        data['kubeflow_workflow_id'] = None
        data['completed_at'] = None

        self.assertDictEqual(data,
                             res.json()['results'][0],
                             'Get by guid failed!')

    def test_GET_flow_run_when_guid_does_not_exist_should_return_404(self):
        res = requests.get(url=f"{flow_runs_url}/00000000-0000-0000-0000-000000000000")
        self.assertEqual(404, res.status_code, 'Unknown GUID not detected')


class TestFlowRunPATCH(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_when_all_step_runs_successful_should_determine_run_status_as_successful(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        self._add_step_run(flow_run, 1, 'SUCCESSFUL')
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        data = {"completed_at": 4133980798888}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(200, res.status_code, 'Completing flow failed')

        res_get = requests.get(url=f"{flow_runs_url}/{guid}")
        updated_run = res_get.json()['results'][0]
        self.assertEqual(data['completed_at'], updated_run['completed_at'])
        self.assertEqual('SUCCESSFUL', updated_run['status'])
        self.assertEqual('SUCCESSFUL', updated_run['steps'][0]['status'])
        self.assertEqual('SUCCESSFUL', updated_run['steps'][1]['status'])

    def test_when_some_step_run_failed_should_determine_run_status_as_failed(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        self._add_step_run(flow_run, 1, 'FAILED')
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        data = {"completed_at": 4133980798888}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(200, res.status_code, 'Completing flow failed')

        res_get = requests.get(url=f"{flow_runs_url}/{guid}")
        updated_run = res_get.json()['results'][0]
        self.assertEqual(data['completed_at'], updated_run['completed_at'])
        self.assertEqual('FAILED', updated_run['status'])
        self.assertEqual('SUCCESSFUL', updated_run['steps'][0]['status'])
        self.assertEqual('FAILED', updated_run['steps'][1]['status'])

    def test_when_non_essential_step_run_failed_should_determine_run_status_as_successful(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        self._add_step_run(flow_run, 1, 'FAILED')
        flow_run['steps'][1]['is_essential'] = False
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        data = {"completed_at": 4133980798888}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(200, res.status_code, 'Completing flow failed')

        res_get = requests.get(url=f"{flow_runs_url}/{guid}")
        updated_run = res_get.json()['results'][0]
        self.assertEqual(data['completed_at'], updated_run['completed_at'])
        self.assertEqual('SUCCESSFUL', updated_run['status'])
        self.assertEqual('SUCCESSFUL', updated_run['steps'][0]['status'])
        self.assertEqual('FAILED', updated_run['steps'][1]['status'])

    def test_when_step_runs_not_submitted_should_determine_run_status_as_not_executed(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        data = {"completed_at": 4133980798888}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(200, res.status_code, 'Completing flow failed')

        res_get = requests.get(url=f"{flow_runs_url}/{guid}")
        updated_run = res_get.json()['results'][0]
        self.assertEqual(data['completed_at'], updated_run['completed_at'])
        self.assertEqual('FAILED', updated_run['status'])
        self.assertEqual('SUCCESSFUL', updated_run['steps'][0]['status'])
        self.assertEqual('NOT_EXECUTED', updated_run['steps'][1]['status'])

    def test_when_some_step_run_failed_but_retried_successfully_should_determine_run_status_as_successful(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        self._add_step_run(flow_run, 1, 'FAILED')
        self._add_step_run(flow_run, 1, 'SUCCESSFUL')
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        data = {"completed_at": 4133980798888}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(200, res.status_code, 'Flow run update failed')

        res_get = requests.get(url=f"{flow_runs_url}/{guid}")
        updated_run = res_get.json()['results'][0]
        self.assertEqual(data['completed_at'], updated_run['completed_at'])
        self.assertEqual('SUCCESSFUL', updated_run['status'])
        self.assertEqual('SUCCESSFUL', updated_run['steps'][0]['status'])
        self.assertEqual('SUCCESSFUL', updated_run['steps'][1]['status'])

    def test_when_step_runs_successful_and_skipped_should_determine_run_status_as_successful(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        self._add_step_run(flow_run, 1, 'SKIPPED')
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        data = {"completed_at": 4133980798888}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(200, res.status_code, 'Flow run update failed')

        res_get = requests.get(url=f"{flow_runs_url}/{guid}")
        updated_run = res_get.json()['results'][0]
        self.assertEqual(data['completed_at'], updated_run['completed_at'])
        self.assertEqual('SUCCESSFUL', updated_run['status'])
        self.assertEqual('SUCCESSFUL', updated_run['steps'][0]['status'])
        self.assertEqual('SKIPPED', updated_run['steps'][1]['status'])

    def test_when_input_param_invalid_should_respond_with_400(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        self._add_step_run(flow_run, 1, 'SUCCESSFUL')
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        data = {"completed_at": 1}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(400, res.status_code, 'Invalid completed_at value not detected')

    def test_when_updating_other_params_than_completed_at_should_not_complete_flow(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        data = {"kubeflow_workflow_name": "danf-integration-test-flow-xxxxx",
                "kubeflow_workflow_id": "11111111-1111-1111-1111-111111111111"}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(200, res.status_code, 'Updating failed')

        res_get = requests.get(url=f"{flow_runs_url}/{guid}")
        updated_run = res_get.json()['results'][0]
        self.assertEqual('IN_PROGRESS', updated_run['status'])

    def test_when_completing_already_completed_run_should_return_conflict(self):
        flow_run = deepcopy(raw_data_flow_run)
        guid = raw_data_flow_run['guid']
        self._add_step_run(flow_run, 0, 'SUCCESSFUL')
        self._add_step_run(flow_run, 1, 'SUCCESSFUL')
        with get_testdb_connector() as con:
            con.add_flow_runs([flow_run])

        # complete flow
        data = {"completed_at": 4133980798888}
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(200, res.status_code, 'Completing flow failed')

        # update completed flow
        res = requests.patch(url=f"{flow_runs_url}/{guid}", json=data)
        self.assertEqual(409, res.status_code, 'Could complete completed flow')

    @staticmethod
    def _add_step_run(data, step_index, status):
        step_run = deepcopy(raw_data_flow_run_step_run)
        step_run['status'] = status
        data['steps'][step_index]['runs'].append(step_run)


class TestFlowRunsStepRunsPOST(unittest.TestCase):

    def setUp(self):
        prepare_db()
        data = deepcopy(raw_data_flow_run)
        with get_testdb_connector() as con:
            con.add_flow_runs([data])

    def tearDown(self):
        cleanup_db()

    def test_when_valid_flow_run_step_run_should_create_successful(self):
        data = deepcopy(raw_data_flow_run_step_run)
        guid = raw_data_flow_run['guid']
        step = raw_data_flow_run['steps'][0]['name']

        res = requests.post(url=flow_runs_step_runs_url_template.format(guid, step), json=data)
        self.assertEqual(201, res.status_code, 'Step run creation failed')

        location = res.headers['Location']
        self.assertRegex(
            location,
            '/flow-runs/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_when_input_parameter_is_invalid_should_return_400(self):
        data = deepcopy(raw_data_flow_run_step_run)
        data['status'] = 'UNKNOWN_STATUS'
        guid = raw_data_flow_run['guid']
        step = raw_data_flow_run['steps'][0]['name']

        res = requests.post(url=flow_runs_step_runs_url_template.format(guid, step), json=data)
        self.assertEqual(400, res.status_code, 'Invalid status not detected')

    def test_when_required_parameter_is_missing_should_return_400(self):
        data = deepcopy(raw_data_flow_run_step_run)
        del data['status']
        guid = raw_data_flow_run['guid']
        step = raw_data_flow_run['steps'][0]['name']

        res = requests.post(url=flow_runs_step_runs_url_template.format(guid, step), json=data)
        self.assertEqual(400, res.status_code, 'Missing param "status" not detected')

    def test_when_referenced_flow_run_does_not_exist_should_return_404(self):
        data = deepcopy(raw_data_flow_run_step_run)
        guid = "00000000-0000-0000-0000-000000000000"
        step = raw_data_flow_run['steps'][0]['name']

        res = requests.post(url=flow_runs_step_runs_url_template.format(guid, step), json=data)
        self.assertEqual(404, res.status_code, 'Missing flow run entity not detected')

    def test_when_referenced_flow_run_step_does_not_exist_should_return_404(self):
        data = deepcopy(raw_data_flow_run_step_run)
        guid = raw_data_flow_run['guid']
        step = 'unknown_step'

        res = requests.post(url=flow_runs_step_runs_url_template.format(guid, step), json=data)
        self.assertEqual(404, res.status_code, 'Missing step not detected')


if __name__ == '__main__':
    unittest.main()
