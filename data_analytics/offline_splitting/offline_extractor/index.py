"""Extractor"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import yaml
import sys
import rosbag
import numpy as np
from ContainerClient import ContainerClient
from DANFUtils.logging import logger

from interpreters.disengagement import DisengagementInterpreter
from config.constants import *
from utils import timestamp2ms


class Extractor:
    def __init__(self, file_path):
        self.file_path = file_path
        self.vin = "unknown"

        # open bag file for reading
        try:
            self.bag = self._get_bag_object()
        except Exception as exp:
            logger.exception(f"Error opening bag file! file: {file_path}")
            raise exp

        # get needed metadata (start time, end time, list of topics)
        try:
            self.start_ts, self.end_ts, self.messages, self.duration, self.size, self.topics_list = self._get_metadata(
            )
        except Exception as exp:
            logger.exception(f"Cannot get metadata of the bag file! file: {file_path}")
            raise exp

        # extract needed topics and run interpreters
        try:
            self.results = self._extract_bag_file_topics()
        except Exception as exp:
            logger.exception(f"Error extracting topics from file: {file_path}")
            raise exp

    def _get_bag_object(self):
        return rosbag.Bag(self.file_path, 'r')

    def _get_metadata(self):
        info_dict = yaml.safe_load(self.bag._get_yaml_info())

        topics = []
        for t in info_dict['topics']:
            topic = t['topic']
            topics.append(topic)
        return info_dict['start'], info_dict['end'], info_dict[
            'messages'], info_dict['duration'], info_dict['size'], topics

    def _extract_bag_file_topics(self):
        # initializing interpreters
        diseng_interpreter = DisengagementInterpreter()

        # check version based on start time of the bagfile and topics list
        try:
            evs_topic, log_topic = diseng_interpreter.set_config(
                self.start_ts, self.topics_list)
        except Exception as exp:
            logger.exception(
                "Version cannot be set! Cannot set topics and engagement handler! Check config file!"
            )
            raise exp

        # include all topics to be extracted
        bag_file_white_list = [
            evs_topic, log_topic
        ]

        # main loop over all filtered messages
        for topic, message, t in self.bag.read_messages(
                topics=bag_file_white_list):

            if (topic == log_topic):
                diseng_interpreter.collect_log_msg(message, t)

            if (topic == evs_topic):
                diseng_interpreter.feed_new_msg(message, t)

        # join two topics
        diseng_interpreter.join_disengagement_with_logrequest()

        # generate json objects
        diseng_interpreter.generate_report(self.start_ts, self.end_ts)

        return [diseng_interpreter.results]

    def close_extractor(self):
        # close bag file
        if self.bag:
            self.bag.close()

    def __del__(self):
        try:
            self.close_extractor()
        except Exception:
            logger.exception(f"can not close file! path: {self.file_path}")


def submit_metadata(extractor_results, client, path):

    filename = path.split("/")[-1].split(".")[0]

    for result in extractor_results:
        metadata = None

        if len(result.get("metadata_list")) > 0:
            metadata = result
            metadata["bagfile_guid"] = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"

        if metadata:
            length_metadata_list = len(metadata.get("metadata_list"))
            logger.info(
                "[{}] length of the metadata list: {}".format(result.get("extractor_id"), length_metadata_list))

            # split extraction request if the metadata list is longer than MAX_LENGTH_METADATA_LIST
            if length_metadata_list > MAX_LENGTH_METADATA_LIST:
                logger.info(
                    "[{}] Length of the metadata_list is higher than {}, extraction request will be splited into {} pieces"
                        .format(result.get("extractor_id"),
                                MAX_LENGTH_METADATA_LIST,
                                (length_metadata_list // MAX_LENGTH_METADATA_LIST) +
                                1))
                splited_metadata_list = np.array_split(
                    metadata.get("metadata_list"),
                    (length_metadata_list // MAX_LENGTH_METADATA_LIST) + 1)

                counter = 1
                for i in splited_metadata_list:
                    metadata["metadata_list"] = i.tolist()
                    logger.info("[{}] extraction_request = {}".format(result.get("extractor_id"),
                                                                      str(i)[:1000] +
                                                                      " ... ..." if len(str(i)) > 1000 else metadata))
                    logger.info("[{}] length of the metadata list: {}".format(result.get("extractor_id"),
                                                                              len(i)))

                    request_guid = client.submit_metadata(metadata, filename)
                    logger.info(
                        "[{}] [{}/{}] Extraction request is inserted into DB with guid = {}"
                            .format(result.get("extractor_id"), counter, len(splited_metadata_list), request_guid))

                    counter += 1

            else:
                request_guid = client.submit_metadata(metadata, filename)
                logger.info("[{}] extraction_request = {}".format(result.get("extractor_id"),
                                                                  str(metadata)[:1000] +
                                                                  " ... ..." if len(str(metadata)) > 1000 else metadata))
                logger.info(
                    "[{}] Extraction request is inserted into DB with guid = {}".
                        format(result.get("extractor_id"), request_guid))
        else:
            logger.warning(
                "[{}] The metadata list is empty, not saving extraction request!".format(result.get("extractor_id"))
            )

def main(path):
    logger.info("Extracting file {}".format(path))
    try:
        extractor = Extractor(path)
    except Exception:
        logger.exception(f"extraction error! Path: {path}")
        sys.exit(1)

    # send request to metadata api using metadata client
    try:
        client = ContainerClient()
        submit_metadata(extractor.results, client, path)


    except Exception:
        logger.exception("Error saving extraction request!")
        sys.exit(1)

    # end span
    sys.exit(0)


if __name__ == '__main__':
    logger.set_logging_folder(
        os.path.abspath(os.path.dirname(__file__)) + '/logs')
    logger.set_log_level('info')
    main(os.environ["BAGFILE_PATH"])
