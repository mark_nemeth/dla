import React from 'react';
import BagfileMap from './BagfileMap';
import {shallow} from 'enzyme/build';
import {getTestChildBagfileList} from '../../setupTests';

describe('BagfileMap component', () => {

  it('should renders without crashing', () => {
    const props = {
      lat: "48.73",
      lng: "9.1",
      route: getTestChildBagfileList(true).childBagfiles
    };
    const wrapper = shallow(<BagfileMap {...props}/>);
    expect(wrapper.exists()).toBe(true);
  });
});