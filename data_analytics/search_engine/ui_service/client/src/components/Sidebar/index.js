import {connect} from 'react-redux';
import {resetFilters, applyFilters} from '../../redux/Home/HomeActions';
import {FileTypes} from './FilterElements/ByFileFilter/ByFileFilter';
import {Sidebar} from './Sidebar';

function mapStateToProps(state, ownProps) {
  const fileType = state.fileType || FileTypes.ORIGINALFILE;
  const searchQuery = state.currentSearchQuery || '';
  return {searchQuery, fileType};
}

export default connect(
  mapStateToProps,
  {resetFilters, applyFilters}
)(Sidebar);