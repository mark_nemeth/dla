import React from 'react';
import {Sidebar} from './Sidebar';
import {shallow} from 'enzyme/build';

describe('Sidebar component', () => {

  it('should render the sidebar with link to bagfile page', () => {
    const wrapper = shallow(<Sidebar />);
    expect(wrapper.exists()).toBe(true);
  });

});