"""Regression tests for search flow route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

from .testutils import get_testdb_connector

search_url = 'http://localhost:8889/search/flow-run'

flow_run_1 = {
    "version": "1.3",
    "guid": generate_deterministic_guid(),
    "kubeflow_workflow_name": "danf-integration-test-flow-xxxxx",
    "kubeflow_workflow_id": "11111111-1111-1111-1111-111111111111",
    "started_at": 1577971401000,
    "completed_at": 1577971500000,
    "status": "SUCCESSFUL",
    "bagfile_guid": "11111111-1111-1111-1111-111111111111",
    "reference": {
        "type": "bagfile_guid",
        "value": "11111111-1111-1111-1111-111111111111"
    },
    "flow_guid": "11111111-1111-1111-1111-11111111111a",
    "steps": [
        {
            "name": "extractor",
            "version": "1.0",
            "status": "SUCCESSFUL",
            "runs": [
                {
                    "started_at": 946684800010,
                    "completed_at": 4133980798888,
                    "status": "SUCCESSFUL",
                    "report": {}
                }
            ]
        }
    ]
}

flow_run_2 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "kubeflow_workflow_name": "danf-integration-test-flow-xxxxx",
    "kubeflow_workflow_id": "11111111-1111-1111-1111-111111111111",
    "started_at": 1577971402000,
    "completed_at": 1577971500000,
    "status": "SUCCESSFUL",
    "bagfile_guid": "11111111-1111-1111-1111-111111111112",
    "reference": {
        "type": "bagfile_guid",
        "value": "11111111-1111-1111-1111-111111111112"
    },
    "flow_guid": "11111111-1111-1111-1111-11111111111a",
    "steps": [
        {
            "name": "extractor",
            "version": "1.0",
            "status": "SUCCESSFUL",
            "runs": [
                {
                    "started_at": 946684800010,
                    "completed_at": 4133980798888,
                    "status": "SUCCESSFUL",
                    "report": {}
                }
            ]
        }
    ]
}

flow_run_3 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "kubeflow_workflow_name": "danf-integration-test-flow-xxxxx",
    "kubeflow_workflow_id": "11111111-1111-1111-1111-111111111111",
    "started_at": 1577971403000,
    "completed_at": 1577971500000,
    "status": "SUCCESSFUL",
    "bagfile_guid": "11111111-1111-1111-1111-111111111113",
    "reference": {
        "type": "bagfile_guid",
        "value": "11111111-1111-1111-1111-111111111113"
    },
    "flow_guid": "11111111-1111-1111-1111-11111111111b",
    "steps": [
        {
            "name": "extractor",
            "version": "1.0",
            "status": "SUCCESSFUL",
            "runs": [
                {
                    "started_at": 946684800010,
                    "completed_at": 4133980798888,
                    "status": "SUCCESSFUL",
                    "report": {}
                }
            ]
        }
    ]
}

flow_run_4 = {
    "version": "1.0",
    "guid": generate_deterministic_guid(),
    "kubeflow_workflow_name": "danf-integration-test-flow-xxxxx",
    "kubeflow_workflow_id": "11111111-1111-1111-1111-000000000000",
    "started_at": 1577971404000,
    "completed_at": 1577971500000,
    "status": "FAILED",
    "bagfile_guid": "11111111-1111-1111-1111-111111111114",
    "reference": {
        "type": "bagfile_guid",
        "value": "11111111-1111-1111-1111-111111111114"
    },
    "flow_guid": "11111111-1111-1111-1111-11111111111b",
    "steps": [
        {
            "name": "extractor",
            "version": "1.0",
            "status": "FAILED",
            "runs": [
                {
                    "started_at": 946684800010,
                    "completed_at": 4133980798888,
                    "status": "FAILED",
                    "report": {}
                }
            ]
        }
    ]
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['flow_runs'].delete_many(
            {"kubeflow_workflow_name": "danf-integration-test-flow-xxxxx"}
        )
        con.add_flow_runs([flow_run_1, flow_run_2, flow_run_3, flow_run_4])


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['flow_runs'].delete_many(
            {"kubeflow_workflow_name": "danf-integration-test-flow-xxxxx"}
        )


class TestSearchFlowRunsGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_guid(self):
        query_data = {'guid': flow_run_1['guid']}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_1], res_json)

    def test_search_by_bagfile_guid(self):
        query_data = {'reference.value': flow_run_3['reference']['value']}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_3], res_json)

    def test_search_by_bagfile_guid_with_wrong_type(self):
        query_data = {
            'reference.type': 'childbagfile_guid',
            'reference.value': flow_run_3['reference']['value']}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertEqual(0, len(res_json))

    def test_search_by_flow_guid(self):
        query_data = {'flow_guid': flow_run_1['flow_guid']}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_1, flow_run_2], res_json)

    def test_search_by_status(self):
        query_data = {'status': flow_run_1['status']}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_1, flow_run_2, flow_run_3], res_json)

    def test_search_by_kubeflow_workflow_name(self):
        query_data = {'kubeflow_workflow_name': 'danf-integration-test-flow-xxxxx'}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_1, flow_run_2, flow_run_3, flow_run_4], res_json)

    def test_search_by_kubeflow_workflow_id(self):
        query_data = {'kubeflow_workflow_id': flow_run_4['kubeflow_workflow_id']}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_4], res_json)

    def test_search_limit_sort_offset_ascending(self):
        query_data = {
            'kubeflow_workflow_name': 'danf-integration-test-flow-xxxxx',
            'limit': 2,
            'offset': 1,
            'sortby': '+bagfile_guid'
        }
        
        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_2, flow_run_3], res_json)

    def test_search_limit_sort_offset_descending(self):
        query_data = {
            'kubeflow_workflow_name': 'danf-integration-test-flow-xxxxx',
            'limit': 2,
            'offset': 2,
            'sortby': '-bagfile_guid'
        }

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_2, flow_run_1], res_json)

    def test_search_by_started_at_lte(self):
        query_data = {'started_at[lte]': 1577971401005}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_1], res_json)

    def test_search_by_started_at_gte(self):
        query_data = {'started_at[gte]': 1577971401005}

        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_2, flow_run_3, flow_run_4], res_json)

    def test_search_by_empty_data(self):
        query_data = {}
        res = requests.get(url=search_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code)
        self.assertCountEqual([flow_run_1, flow_run_2, flow_run_3, flow_run_4], res_json)


if __name__ == '__main__':
    unittest.main()
