import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../Table';
import DateTimeEntry from './DateTimeEntry';

describe('Bagfile Table TopicEntry', () => {

  it('should format topic number correct ', () => {
    const formattedResult = 'Fri, 07 Jul 2017 00:16:47 GMT';
    const props = {timestamp: 1499386607672};
    const wrapper = shallow(<DateTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should create a disabled button when path not exists', () => {
    const props = {timestamp: null};
    const wrapper = shallow(<DateTimeEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});
