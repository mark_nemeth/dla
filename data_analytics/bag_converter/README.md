# Bag Converter

Bag Converter package contains 4 request types: info, bag2img, bag2vid, and bag2slice. 


## Build docker:
```console
 docker build -t bag-converter:latest . --network host --no-cache --build-arg http_proxy=http://localhost:3128/ --build-arg https_proxy=http://localhost:3128/.
```

## Run docker (locally):
```console
docker run -p 8891:8891  -v /mapr:/mapr -e APP_ENV="local"  -e APP_LANG="ENG" bag-converter:latest
```

to run it on Vaihingen integration cluster (already deployed)
```console
kubectl port-forward deployment/bag-converter  8891:8891 -n attdata-danf
```

Examples:
```console
http://127.0.0.1:8891/redoc
http://127.0.0.1:8891/docs
http://127.0.0.1:8891/info?bagfile=/mapr/059-va2.mbc.de/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A470239/2020/07/30/20200730_141149_CID_Master_1016_djostme_closed_loop_2_0_ORIGINAL.bag
http://127.0.0.1:8891/bag2img?relative_time=12&compression_quality=50&bagfile=/mapr/059-va2.mbc.de/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A470239/2020/07/30/20200730_141149_CID_Master_1016_djostme_closed_loop_2_0_ORIGINAL.bag&topic=/sensor/axis_camera/cam_front/compressed
http://127.0.0.1:8891/bag2vid?start_relative=0&end_relative=500&compression_quality=50&bagfile=/mapr/059-va2.mbc.de/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A470239/2020/07/30/20200730_141149_CID_Master_1016_djostme_closed_loop_2_0_ORIGINAL.bag&topic=/sensor/axis_camera/cam_front/compressed
http://127.0.0.1:8891/bag2img?relative_time=1000&compression_quality=50&bagfile=/mapr/059-va2.mbc.de/data/buffer/rd/athena/08_robotaxi/plog/WDD2221591A470791/2020/08/12/20200812_121942_CID_FBI-ATT-1281_M1015_AP_N02_after-system-reboot_0_ORIGINAL.bag&topic=/debug/StereoCameraFront/RectifiedColorImage/image/compressed

```
