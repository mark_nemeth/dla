import redisStoreSingleton from './redisStore';

/**
 * Class is used as an Interface to change between different
 * persistent store implementations for different methods.
 */
class StoreProvider {

  constructor() {
    this.redisStore = redisStoreSingleton;
  }

  //region session
  getSessionStore() {
    return this.redisStore.getSessionStore();
  }
  //endregion

  //region access token
  async getUserTokenDataAsync(userID) {
    const tokenData = await this.redisStore.getUserTokenDataAsync(userID);
    return tokenData;
  }

  saveUserTokenData(userID, tokenData) {
    return this.redisStore.saveUserTokenData(userID, tokenData);
  }

  deleteUserTokenData(userID) {
    return this.redisStore.deleteUserTokenData(userID);
  }
  //endregion

  //region user data
  getUserDataAsync(userID) {
    return this.redisStore.getUserDataAsync(userID);
  }

  saveUserData(userID, profile) {
    return this.redisStore.saveUserData(userID, profile);
  }

  deleteUserData(userID) {
    return this.redisStore.deleteUserData(userID);
  }
  //endregion

  _getSingleRandomTokenAsync() {
    return this.redisStore._getSingleRandomTokenAsync();
  }
}

export default new StoreProvider();