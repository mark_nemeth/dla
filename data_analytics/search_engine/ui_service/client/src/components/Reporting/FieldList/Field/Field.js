import React from 'react';
import Col from 'react-bootstrap/Col';
import LabelContainer from '../../../Utils/LabelContainer/LabelContainer';
import LabelEntry from '../../../AttributeFormatter/Label/LabelEntry';

export function Field(props) {
  return (
    <Col md={12} lg={12} xl={12} className="col-xxl-12 col-xxxl-12 col-xxxxl-12">
      <LabelContainer label={props.label} variant="card-style-filled">
        <LabelEntry value={props.displayValue} />
      </LabelContainer>
    </Col>
  );
}
