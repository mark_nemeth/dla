import React from 'react';
import { Table } from './Table/Table';


export function TableList(props) {

  const newTable = (item) => {
    return <Table key={item.caption} caption={item.caption} colLabels={item.colLabels} values={item.values} />;
  };

  return (
    <div>
      {props.tables.map(item => (newTable(item)))}
    </div>
  );
}