"""Tracing SDK"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import functools
import logging
import os
import traceback
from atexit import register, unregister
from enum import Enum

from opencensus.trace import execution_context
from opencensus.trace.propagation import text_format
from opencensus.trace.propagation.trace_context_http_header_format import \
    TraceContextPropagator
from opencensus.trace.samplers import AlwaysOnSampler
from opencensus.trace.span import SpanKind
from opencensus.trace.span_context import SpanContext
from opencensus.trace.status import Status
from opencensus.trace.trace_options import TraceOptions
from opencensus.trace.tracer import Tracer
from opencensus.trace.tracers.noop_tracer import NoopTracer

from DANFUtils.tracing.config.config_handler import config_dict
from DANFUtils.tracing.exporter.azure import AzureExporter


ENV_TRACE_ID = 'DANF_TRACING_TRACE_ID'
ENV_SPAN_ID = 'DANF_TRACING_SPAN_ID'
ENV_TRACE_OPTIONS = 'DANF_TRACING_TRACE_OPTIONS'

FATAL_ERROR_ATTRIBUTE = 'fatalError'
"""task attribute for storing information about unrecoverable errors"""

ERROR_ATTRIBUTE = 'error'
"""task attribute for storing information about recoverable errors"""

APPINSIGHTS_DISABLE_KEY = 'APPINSIGHTS_DISABLE_KEY'
"""can be used as APPINSIGHTS_INSTRUMENTATIONKEY in tests to disable sending 
traces to application insights"""

_SUCCESS_STATUS_CODE = 0
"""status code indicating success in open census"""

_UNKNOWN_ERROR_STATUS_CODE = 2
"""status code indicating an error with unknown reason in open census"""

_DANF_ENV = config_dict.get_value('DANF_ENV', None)

_global_exporter = None
"""lazy initialized tracing exporter"""

logger = logging.getLogger(__name__)


class Kind(Enum):
    REQUEST = 1
    DEPENDENCY = 2


def _get_span_context_from_env():
    return SpanContext(trace_id=os.environ.get(ENV_TRACE_ID, None),
                       span_id=os.environ.get(ENV_SPAN_ID, None),
                       trace_options=TraceOptions(
                           os.environ.get(ENV_TRACE_OPTIONS, None)))


def _create_tracer(span_context=None, headers=None):
    if span_context is None:
        if headers is None:
            span_context = _get_span_context_from_env()
        else:
            span_context = TraceContextPropagator().from_headers(headers)

    # tracer is automatically stored in execution context,
    # from where we will always retrieve it
    Tracer(sampler=AlwaysOnSampler(),
           span_context=span_context,
           propagator=text_format.TextFormatPropagator(),
           exporter=_get_exporter())

    # set exit handler
    register(_cleanup_at_shutdown)


def _get_exporter():
    global _global_exporter

    if _global_exporter:
        return _global_exporter

    # create new exporter
    appinsights_key = os.environ.get('APPINSIGHTS_INSTRUMENTATIONKEY')

    if not appinsights_key:
        logger.warning("env variable APPINSIGHTS_INSTRUMENTATIONKEY missing")
        appinsights_key = APPINSIGHTS_DISABLE_KEY

    if appinsights_key == APPINSIGHTS_DISABLE_KEY:
        # in test-mode no exporter
        _global_exporter = None
    else:
        # in execution mode
        _global_exporter = AzureExporter(
            connection_string=f'InstrumentationKey={appinsights_key}')
    return _global_exporter


def _get_tracer():
    tracer = execution_context.get_opencensus_tracer()
    if type(tracer) is NoopTracer:
        # if tracer is not initialized yet,
        # NoopTracer will be returned by execution context
        _create_tracer()
    return execution_context.get_opencensus_tracer()


def _check_task_whitelist(name, tasks_config):
    if name not in tasks_config:
        raise ValueError(f"task {name} not whitelisted, "
                         f"allowed tasks are: {tasks_config.keys()}")


def _get_default_kind(task_name, tasks_config):
    kind_as_str = tasks_config[task_name]["defaultKind"]
    return Kind[kind_as_str]


def _get_task_config():
    return config_dict.get_value("allowed_tasks", [])


def _cleanup_at_shutdown():
    end_all_tasks(success=False)

    # unregister exit handler to prevent loops
    unregister(_cleanup_at_shutdown)


def start_task_in_context(task_name,
                          attributes=None,
                          span_context=None,
                          context_headers=None):
    """Starts tracing the task with the given name in the specified
    context (= trace id). The context can be initialized from OpenCensus
    compliant http headers or by passing a explicit span_context object.
    If no context is specified, a new context (=trace id) will be created.
    The started task will become the currently active span,
    which is used by methods end_task and task_attribute.

    :param task_name: name of the task. Must be whitelisted
        via config.init#allowed_tasks
    :param attributes: optional dict of attributes
        that will be attached to the task
    :param span_context: optional context in which the task should be traced
    :param context_headers: optional context in which the task
        should be traced
    """
    _create_tracer(span_context=span_context, headers=context_headers)
    start_task(task_name, attributes=attributes)


def trace_task(name, kind=None):
    """By decorating a method with this decorator its start and end will be
    traced by calling start_task/end_task internally. Within the decorator
    task_attribute() can be used to attach attributes to the current span.
    The decorator will also catch exception raised by the decorated method and
    mark the span as a failure (success=False)."""

    def decorator_task(func):
        @functools.wraps(func)
        def wrapper_task(*args, **kwargs):
            start_task(name=name, kind=kind)
            try:
                value = func(*args, **kwargs)
            except Exception:
                stacktrace = traceback.format_exc()
                end_task(success=False,
                         attributes={FATAL_ERROR_ATTRIBUTE: stacktrace})
                raise
            end_task()
            return value

        return wrapper_task

    return decorator_task


def start_task(name, kind=None, attributes=None):
    """Starts tracing the task with the given name in the current context
    (=trace id). The started task will become the currently active span,
    which is used by methods end_task and task_attribute.

    :param name: name of the task. Must be whitelisted
        via config.init#allowed_tasks
    :param kind: REQUEST or DEPENDENCY
    :param attributes: optional dict of attributes
        that will be attached to the task
    """
    tasks_config = config_dict.get_value("allowed_tasks", [])
    _check_task_whitelist(name, tasks_config)
    if not kind:
        kind = _get_default_kind(name, tasks_config)

    try:
        span = _get_tracer().start_span(name=name)

        if attributes:
            for k, v in attributes.items():
                task_attribute(k, v)

        if _DANF_ENV:
            task_attribute('DANF_ENV', _DANF_ENV)

        # map the kind attribute as it is used by Application Insights
        # to their equivalent in OpenCensus
        if kind == Kind.DEPENDENCY:
            span.span_kind = SpanKind.CLIENT
        elif kind == Kind.REQUEST:
            span.span_kind = SpanKind.SERVER

    except Exception as e:
        logger.error(f"Unable to start tracing task {name}: {e}")


def task_attribute(name, value):
    """Attach an attribute key-value pair to the currently active span.

    :param name:
    :param value:
    """
    try:
        _get_tracer().add_attribute_to_current_span(name, value)
    except Exception as e:
        logger.error(f"Unable to set attribute {name}={value}: {e}")


def end_task(success=True, attributes=None):
    """End the currently active span and submit it to the tracing backend.
    The parameter success can be used to indicate a failure by setting it to
    False.

    :param success: mark the task as successful or failed
    :param attributes: optional dict of attributes
        that will be attached to the task
    """
    try:
        if attributes:
            for k, v in attributes.items():
                task_attribute(k, v)
        tracer = _get_tracer()
        span = _get_tracer().current_span()
        span.set_status(Status(
            _SUCCESS_STATUS_CODE if success else _UNKNOWN_ERROR_STATUS_CODE))
        tracer.end_span()
    except Exception as e:
        logger.error(f"Unable to end tracing task: {e}")


def end_all_tasks(success=False, attributes=None):
    """End all spans.

    :param success: mark all unfinished tasks as successful or failed
    :param attributes: optional dict of attributes that will be attached
        to all unfinished tasks
    """

    # set attribute
    for open_span in _get_tracer().tracer._spans_list:
        open_span.set_status(Status(
            _SUCCESS_STATUS_CODE if success else _UNKNOWN_ERROR_STATUS_CODE))
        if attributes:
            for k, v in attributes.items():
                open_span.add_attribute(k, v)

    # finish all spans
    _get_tracer().finish()


def get_propagation_vars():
    """Returns current trace context propagation variables as dict. This can
    be used to pass the trace information to subprocess/remote processes.

    :return: dict of trace context information for propagation
    """
    return {
        ENV_TRACE_ID: _get_tracer().span_context.trace_id,
        ENV_SPAN_ID: _get_tracer().span_context.span_id,
        ENV_TRACE_OPTIONS: _get_tracer().span_context.trace_options.trace_options_byte
    }


def get_propagation_headers():
    """Returns current trace context propagation variables as http headers.
    This can be used to pass the trace information to remote processes called
    via http.

    :return: trace context information for propagation in http header format
    """
    headers = TraceContextPropagator().to_headers(_get_tracer().span_context)
    return headers


def write_propagation_vars_to_dir(directory):
    """Persists the current trace context propagation variables to disk. Each
    variable is persisted as a file in the given directory.

    :param directory: directory to which files should be written
    """
    try:
        propagation_vars = get_propagation_vars()

        if not os.path.exists(directory):
            os.makedirs(directory)

        for var, value in propagation_vars.items():
            if not value:
                continue
            output_file = os.path.join(directory, var)
            with open(output_file, 'w') as f:
                f.write(value)

    except Exception as e:
        logger.error(f"Unable to persist propagation variables: {e}")
