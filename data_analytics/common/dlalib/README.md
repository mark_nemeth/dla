In its current state the package provides a collection of modules/functions to extract time windows of a given pyspark data frame.
Each functions operates on pyspark/python data structures and does not require the RDA. Each pyspark must have the columns
"ts" (timestamp) and "filename" (filename of e.g. the bag file).


## Example
```python
# create data frame (could also be done by RDA)
l = [(datetime.datetime(2018, 1, 1, 00, 00, 00), 'f1', 1), \
     (datetime.datetime(2018, 1, 1, 00, 00,  5), 'f1', 1), \
     (datetime.datetime(2018, 1, 1, 00, 00, 10), 'f1', 2), \
     (datetime.datetime(2018, 1, 1, 00, 00, 15), 'f1', 2), \
     (datetime.datetime(2018, 1, 1, 00, 00, 16), 'f1', 1), \
     (datetime.datetime(2018, 1, 1, 00, 00, 17), 'f1', 1), \
     (datetime.datetime(2018, 1, 1, 00, 00, 20), 'f2', 1), \
     (datetime.datetime(2018, 1, 1, 00, 00, 25), 'f2', 2), \
     (datetime.datetime(2018, 1, 1, 00, 00, 30), 'f2', 2), \
     (datetime.datetime(2018, 1, 1, 00, 00, 35), 'f2', 1)]
df = spark.createDataFrame(data = l, schema = ["ts", "filename", "ad_engagement_state"])

# extract intervals (beginning and end of each engaged time span)
intervals = extract_intervals(df, "ad_engagement_state", [ADEngagementState.initializing, ADEngagementState.ready, ADEngagementState.error], [ADEngagementState.engaged])
# extract windows of each interval
windows = extract_windows_from_intervals(intervals, window_length_ms=2000, overlap_ms=0)
# use udf to label windows
df_labeled = df.withColumn("labels", label_windows_udf(windows)(col("ts"), col("filename")))
#explode list of window labels into separate rows
df_exploded = explode_window_labels(df_labeled, 'labels')

```
