import React from 'react';
import DateTimeEntry from "../../../AttributeFormatter/DateTime/DateTimeEntry";
import LabelEntry from "../../../AttributeFormatter/Label/LabelEntry";
import LabelContainer from "../../../Utils/LabelContainer/LabelContainer";
import {Row} from "react-bootstrap";
import Card from "react-bootstrap/Card";
import './AlwaysOnRecordingMetadataView.scss';
import TopicListEntry from "../../../AttributeFormatter/Topic/TopicList/TopicListEntry";
import NameListEntry from "../../../AttributeFormatter/Name/NameList/NameListEntry";
import {FileTypes} from "../../../Sidebar/FilterElements/ByFileFilter/ByFileFilter";

export function AlwaysOnRecordingMetadataView(props) {

  function getFileObjects() {
    return props.recording.files.map((file, index) => {
      return {
        name: file,
        guid: props.recording.bagfile_guids[index]
      }
    })
  }

  return (
      <Card className="always-on-recording-information-section">
        <span className="always-on-recording-information-section-label">
          Always On Recording Information
        </span>
        <Row className='information-row'>
          <LabelContainer label="Recording Guid" variant="hundred-percent-card card-style-filled">
            <LabelEntry value={props.recording.guid}/>
          </LabelContainer>
        </Row>
        <Row className='information-row'>
          <LabelContainer label="Start" variant="fifty-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.recording.start}/>
          </LabelContainer>
          <LabelContainer label="End" variant="fifty-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.recording.end}/>
          </LabelContainer>
        </Row>
        <Row className='information-row'>
          <LabelContainer label="Recorder Version" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.recording.recorder_version}/>
          </LabelContainer>
          <LabelContainer label="Recorder Path" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.recording.recorder_path}/>
          </LabelContainer>
        </Row>
        <LabelContainer label="Files" variant="card-style-filled">
          <NameListEntry fileType={FileTypes.ALWAYSONFILE} files={getFileObjects()}/>
        </LabelContainer>
        <Row className='information-row'>
          <LabelContainer label="Topics" variant="hundred-percent-card card-style-filled">
            <TopicListEntry topics={props.recording.topics}/>
          </LabelContainer>
        </Row>
      </Card>
  )
}