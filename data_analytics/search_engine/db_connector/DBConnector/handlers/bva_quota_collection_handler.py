__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
from DANFUtils.logging import logger
from pymongo import ReturnDocument

class BvaQuotaCollectionHandler:
    # Workstream index
    COLLECTION_NAME = 'bva_quotas'
    INDEX_NAME = 'bva_quotas_guid_index'
    INDEX_FIELD_GUID = 'guid'

    def __init__(self, db):
        self.db = db
        self.col = self.db[self.COLLECTION_NAME]
        self.col.create_index(self.INDEX_FIELD_GUID,
                              unique=True,
                              name=self.INDEX_NAME)

    def get(self, active):
        logger.debug(f"Searching for quota documents in {self.COLLECTION_NAME} with active value: {active}")
        return self.col.find({"active": active}, projection={'_id': 0})

    def find_one_and_update(self, filter_query, update_request):
        logger.debug(f"Searching for quota document in {self.COLLECTION_NAME} with filter: {filter_query} and update values: {update_request}")
        return self.col.find_one_and_update(filter=filter_query,
                                            update=update_request,
                                            projection={'_id': 0},
                                            return_document=ReturnDocument.AFTER)

    def add(self, voting_quota):
        logger.debug(f"Adding new quota: {voting_quota}")
        self.col.insert_one(voting_quota)
