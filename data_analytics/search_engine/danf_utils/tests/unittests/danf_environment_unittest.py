__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

from DANFUtils.danf_environment import DanfEnv, Platform
from DANFUtils.danf_environment import Stage


class TestDanfEnv(unittest.TestCase):

    def test_stage_should_assign_correct_value_for_dev(self):
        env = DanfEnv('az_dev')
        self.assertEqual(env.stage, Stage.DEV)

    def test_stage_should_assign_correct_value_for_prod(self):
        env = DanfEnv('sv_prod')
        self.assertEqual(env.stage, Stage.PROD)

    def test_stage_should_always_assign_DEV_to_local(self):
        env = DanfEnv('local')
        self.assertEqual(env.stage, Stage.DEV)

    def test_platform_should_assign_correct_value_for_azure(self):
        env = DanfEnv('az_int')
        self.assertEqual(env.platform, Platform.AZURE)

    def test_platform_should_assign_correct_value_for_on_prem(self):
        env = DanfEnv('sv_prod')
        self.assertEqual(env.platform, Platform.ON_PREM)

    def test_platform_should_assign_correct_value_for_local(self):
        env = DanfEnv('local')
        self.assertEqual(env.platform, Platform.LOCAL)

    def test_when_env_string_is_unknown_should_raise_exception(self):
        self.assertRaises(ValueError, DanfEnv, 'unknown')


if __name__ == '__main__':
    unittest.main()
