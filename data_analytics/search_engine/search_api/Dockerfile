ARG DOWNLOADER_IMAGE=python:3.7
ARG IMAGE=python:3.7
FROM $DOWNLOADER_IMAGE as downloader
ARG INDEX_URL=https://pypi.org/simple
ARG EXTRA_INDEX_URL
ARG DANF_VERSION_TAG="DANF_VERSION_NEEDS_TO_BE_DEFINED"

#STAGE 1
COPY . /source
RUN sed -i 's/DANF_VERSION_TAG = "WILL_BE_TAKEN_FROM_GIT_TAG"/DANF_VERSION_TAG = "'"$DANF_VERSION_TAG"'"/g' source/config/constants.py

# Copy setup.py
COPY setup.py /downloads/setup.py
COPY config /downloads/config

# Permissions
RUN chmod a+r /downloads/setup.py

# Copy requirements to download folder
RUN python /downloads/setup.py egg_info \
&&  cp /search_api.egg-info/requires.txt /downloads

# Download the depenencys
RUN pip download -r /downloads/requires.txt \
    -d /downloads \
    --index-url $INDEX_URL \
    --extra-index-url $EXTRA_INDEX_URL 


# STAGE2
FROM $IMAGE
ARG workdir=/home/appuser

# switch to app user and working dir
WORKDIR $workdir

# install tools and add user
RUN pip install --upgrade \
    pip \
    setuptools \
    wheel \
&& groupadd --gid 1000 appuser \
&& useradd --uid 1000 --gid 1000 --home $workdir appuser \
&& chown -R appuser $workdir

USER appuser

# copy src
COPY --chown=appuser:appuser --from=downloader /source .

# copy dependencies
COPY --chown=appuser:appuser --from=downloader /downloads $workdir/downloads

# install
 RUN pip install . --no-cache-dir --no-index -f $workdir/downloads --user \
 && rm $workdir/downloads -r
# bind app to port
EXPOSE 8445

# start app
CMD [ "python", "-u", "index.py" ]