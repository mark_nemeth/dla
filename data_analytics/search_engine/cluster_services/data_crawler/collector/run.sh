#!/bin/sh
umask 002
time python3 -u main.py "$DIRECTORY" "$METADATA_API_CATALOG_URL" "$SEARCH_API_CATALOG_URL" --dry_run="$DRY_RUN" --concurrent_requests="$CONCURRENT_REQUESTS" --file_type="$FILE_TYPE"
