sudo -E apt-get install python3-pip -y
sudo -E pip3 install virtualenv
python3 -m virtualenv -p python3 jupyter
cd jupyter
source bin/activate
pip install matplotlib pillow pyspark==2.2.1 ipython
pip install /mapr/738.mbc.de/var/mdm/robodrive/libraries/rda1.2.0/python/*
pip install jupyter
pip install h5py
pip install pandas
pip install --upgrade tables
pip install findspark
pip install pyarrow
