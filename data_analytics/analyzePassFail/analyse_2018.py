import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import math
import csv
import os
import rosbag
import subprocess
import sys

do_plots = 1
do_calc = 0

# Load files ###############################
# bagfile = rosbag.Bag('20180214_105847_onlyimar.bag')
subprocess.call(["python", "extract/extract_GPS.py", sys.argv[1]])
subprocess.call(["python", "extract/extract_maps.py", sys.argv[1]])
subprocess.call(["python", "extract/extract_Ego.py", sys.argv[1]])
subprocess.call(["python", "extract/extract_imar.py", sys.argv[1]])

# Load files ###############################
file = 'csv/Out_CalService_instance_0_GPSInfoInterface_2018.csv'
# horizon_file = 'horizon_xy.csv'
delimiter_file = 'csv/Out_MapsService_instance_0_LocalizationHorizonInterface_delimiter.csv'
mapobject_file = 'csv/Out_MapsService_instance_0_LocalizationHorizonInterface_objects.csv'
refline_file = 'csv/Out_MapsService_instance_0_LocalizationHorizonInterface_reflines.csv'
virtualline_file = 'csv/Out_MapsService_instance_0_LocalizationHorizonInterface_delimiter_virtual.csv'
solidline_file = 'csv/Out_MapsService_instance_0_LocalizationHorizonInterface_delimiter_solid.csv'
dashedline_file = 'csv/Out_MapsService_instance_0_LocalizationHorizonInterface_delimiter_dashed.csv'
curb_file = 'csv/Out_MapsService_instance_0_LocalizationHorizonInterface_delimiter_curb.csv'
ego_file = 'csv/Out_CalService_instance_0_EgoVehStateInterface_2018.csv'
imar_inspva_file = 'csv/Out_CalService_instance_0_ImarInterface_inspva.csv'
imar_bestpos_file = 'csv/Out_CalService_instance_0_ImarInterface_bestpos.csv'

# Calculations ###############################
bag = rosbag.Bag(sys.argv[1])
topic = '/MapsService/instance_0/LocalizationHorizonInterface'
origin_lat = next(bag.read_messages(topics=[topic])).message.origin.latitude
origin_lon = next(bag.read_messages(topics=[topic])).message.origin.longitude
print(origin_lat)
print(origin_lon)


def calcX(lat, lon):
    R = 6371000
    X = R * math.cos(math.radians(lat)) * math.cos(math.radians(lon))
    return X
# z = R *sin(lat)
# print(calcX(48.678803, 8.976738))
# print(calcX(48.781623736, 8.919085746))


def calcY(lat, lon):
    R = 6371000
    Y = R * math.cos(math.radians(lat)) * math.sin(math.radians(lon))
    return Y
# print(calcY(48.678803, 8.976738))
# print(calcY(48.781623736, 8.919085746))


def calcTENCSY(lat, lon):
    R = 6371000
    dlat = math.radians(lat) - math.radians(origin_lat)
    Y = R * math.sin(dlat)
    return Y
# print(calcTENCSY(48.678803, 8.976738))
# print(calcTENCSY(48.703876275, 8.999824227))


def calcTENCSX(lat, lon):
    R = 6371000
    dlon = math.radians(lon) - math.radians(origin_lon)
    X = R * math.sin(dlon) * math.cos(math.radians(origin_lat))
    return X
# print(calcTENCSX(48.678803, 8.976738))
# print(calcTENCSX(48.703876275, 8.999824227))


def distance(origin_lat, origin_lon, destination_lat, destination_lon):
    radius = 6371  # km

    dlat = math.radians(destination_lat - origin_lat)
    dlon = math.radians(destination_lon - origin_lon)
    a = math.sin(dlat / 2) * math.sin(dlat / 2) + math.cos(math.radians(origin_lat)) * \
        math.cos(math.radians(destination_lat)) * \
        math.sin(dlon / 2) * math.sin(dlon / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c
    return d
# print(distance(48.781780686,8.920078173,48.781795115,8.92013367))


class ReadlineIterator(object):

    """An iterator that calls readline() to get its next value."""

    def __init__(self, f, commentstring="#"):
        self.f = f
        self.commentstring = commentstring

    def __iter__(self):
        for line in self.f:
            if line.strip() and not line.startswith(self.commentstring):
                yield line

latitudes = []
longitudes = []

with open(file, 'r') as f:
    it = ReadlineIterator(f)
    for row in csv.reader(it, delimiter=','):
        # print(row)
        # print(row[1:])
        latitudes.append(float(row[2]))
        longitudes.append(float(row[3]))
        # print(latitudes)

X_coordinates = []
Y_coordinates = []

for i in range(len(latitudes)):
    X_coordinates.append(calcTENCSX(latitudes[i], longitudes[i]))
    Y_coordinates.append(calcTENCSY(latitudes[i], longitudes[i]))
# print X_coordinates
# print Y_coordinates

distances = [0]
for i in range(len(latitudes) - 10):
    distances.append(distance(latitudes[i], longitudes[i], latitudes[
                     i + 10], longitudes[i + 10]))  # calculate for every 10 data points (1sec)
# print distances

velocities = []
for i in range(len(distances)):
    velocities.append(distances[i] * 3600)
# print velocities

acceleration = [0]
for i in range(len(velocities) - 1):
    acceleration.append(10 * (velocities[i + 1] - velocities[i]) / 3.6)
# print acceleration


def calcDistanceXY(data1, data2):
    dx = data1[0] - data2[0]
    dy = data1[1] - data2[1]
    d = math.sqrt(dx * dx + dy * dy)
    return d


def calcMinDistance(data, datalist, columnX, columnY, skip_dis):
    d_min = 100000
    for i in range(len(datalist)):
        if abs(data[0] - datalist[columnX][i]) < skip_dis and abs(data[1] - datalist[columnY][i]) < skip_dis:
            d = calcDistanceXY(
                data, [datalist[columnX][i], datalist[columnY][i]])
            if d < d_min:
                d_min = d
    return d_min

# DEFINE DATA ###############################

data = np.genfromtxt(file, delimiter=',', skip_header=0,
                     skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'])
# horizon_data = np.genfromtxt(horizon_file, delimiter=',', skip_header =
# 0, skip_footer=0, names=['x','y'])
delimiter_data = np.genfromtxt(delimiter_file, delimiter=',', skip_header=0,
                               skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'x', 'y', 'z'])
mapobject_data = np.genfromtxt(mapobject_file, delimiter=',', skip_header=0,
                               skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'x', 'y', 'z'])
refline_data = np.genfromtxt(refline_file, delimiter=',',
                             skip_header=0, skip_footer=0, names=['a', 'b', 'c', 'd', 'x', 'y', 'z'])

virtualline_data = np.genfromtxt(
    virtualline_file, delimiter=',', skip_header=0,
    skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'x', 'y', 'z'])
solidline_data = np.genfromtxt(solidline_file, delimiter=',', skip_header=0,
                               skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'x', 'y', 'z'])
dashedline_data = np.genfromtxt(
    dashedline_file, delimiter=',', skip_header=0,
    skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'x', 'y', 'z'])
curb_data = np.genfromtxt(curb_file, delimiter=',', skip_header=0,
                          skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'x', 'y', 'z'])

ego_data = np.genfromtxt(ego_file, delimiter=',', skip_header=0,
                         skip_footer=0, names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'])

imar_inspva_data = np.genfromtxt(
    imar_inspva_file, delimiter=',', skip_header=0,
    skip_footer=0, names=['time_s', 'time_ns', 'lon', 'lat', 'alt', 'roll', 'pitch'])
imar_bestpos_data = np.genfromtxt(
    imar_bestpos_file, delimiter=',', skip_header=0,
    skip_footer=0, names=['time_s', 'time_ns', 'lon', 'lat', 'height'])

# CALCULATE MIN DISTANCES ###############################
# print data['c'][0]
# print([calcTENCSX(data['c'][0],data['d'][0]), calcTENCSY(data['c'][0],data['d'][0])])
# print(calcMinDistance([calcTENCSX(data['c'][0],data['d'][0]),
# calcTENCSY(data['c'][0],data['d'][0])], curb_data, 'x', 'y'))
mindistances_solid = []
mindistances_dashed = []
mindistances_curb = []

if do_calc:

    for i in range(len(data)):
        mindis = calcMinDistance(
            [calcTENCSX(data['c'][i], data['d'][i]), calcTENCSY(data['c'][i], data['d'][i])], solidline_data, 'x', 'y', 100)
        print mindis, i
        mindistances_solid.append(mindis)

    for i in range(len(data)):
        mindis = calcMinDistance(
            [calcTENCSX(data['c'][i], data['d'][i]), calcTENCSY(data['c'][i], data['d'][i])], dashedline_data, 'x', 'y', 100)
        print mindis, i
        mindistances_dashed.append(mindis)

    for i in range(len(data)):
        mindis = calcMinDistance(
            [calcTENCSX(data['c'][i], data['d'][i]), calcTENCSY(data['c'][i], data['d'][i])], curb_data, 'x', 'y', 100)
        print mindis, i
        mindistances_curb.append(mindis)


# PLOT ###############################
if do_plots:

    directory = 'static/images/' + str(sys.argv[1])

    if not os.path.exists(directory):
        os.makedirs(directory)

    fig1 = plt.figure(num=None, figsize=(8, 6),
                      dpi=80, facecolor='None', edgecolor='k')
    ax1 = fig1.add_subplot(111)
    ax1.plot(data['c'], data['d'], 'o', ls='-', ms=4, markevery=2)
    ax1.set_title('Track latitutes vs. longitues')
    ax1.set_xlabel('latitutes')
    ax1.set_ylabel('longitues')
    fig1.savefig(directory + '/fig1.png', dpi=300)

    fig2 = plt.figure(num=None, figsize=(8, 6),
                      dpi=80, facecolor='None', edgecolor='k')
    ax2 = fig2.add_subplot(111)
    ax2.plot(data['g'], color='r', label='the data')
    ax2.set_title('Speed')
    ax2.set_xlabel('time [0.1s]')
    ax2.set_ylabel('velocity [m/s]')
    fig2.savefig(directory + '/fig2.png', dpi=300)

    '''
    #climb
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    ax3.plot(data['h'], color='r', label='the data')
    '''

    '''
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(111, projection = '3d')
    ax4.plot_wireframe(data['c'], data['d'], data['e'], rstride=5, cstride=5)
    '''

    fig5 = plt.figure(num=None, figsize=(8, 6),
                      dpi=80, facecolor="None", edgecolor='k')
    ax5 = fig5.add_subplot(111)
    ax5.plot(velocities, color='r', label='the data')
    ax5.set_title('Velocity calculated from GPS positions')
    ax5.set_xlabel('time [0.1s]')
    ax5.set_ylabel('velocity [km/h]')
    fig5.savefig(directory + '/fig5.png', dpi=300)

    fig6 = plt.figure(num=None, figsize=(8, 6),
                      dpi=80, facecolor="None", edgecolor='k')
    ax6 = fig6.add_subplot(111)
    ax6.plot(acceleration, color='r', label='the data')
    ax6.set_title('Acceleration calculated from GPS positions')
    ax6.set_xlabel('time [0.1s]')
    ax6.set_ylabel('acceleration [m/s^2]')
    fig6.savefig(directory + '/fig6.png', dpi=300)

    fig7 = plt.figure(num=None, figsize=(40, 30),
                      dpi=300, facecolor="None", edgecolor='k')
    ax7 = fig7.add_subplot(111)
    ax7.plot(X_coordinates, Y_coordinates, color='r', label='the data')
    ax7.set_title('Track XY plane')
    ax7.set_xlabel('X [m]')
    ax7.set_ylabel('Y [m]')
    fig7.savefig(directory + '/fig7.png', dpi=300)

    fig8 = plt.figure(num=None, figsize=(40, 30),
                      dpi=300, facecolor="None", edgecolor='k')
    ax8 = fig8.add_subplot(111, projection='3d')
    ax8.plot_wireframe(X_coordinates, Y_coordinates, range(
        0, len(X_coordinates)), linewidth=2.0, rstride=5, cstride=5)
    ax8.plot(X_coordinates, Y_coordinates, '-')
    ax8.set_title('Track vs time')
    ax8.set_xlabel('X [m]')
    ax8.set_ylabel('Y [m]')
    ax8.set_zlabel('time [0.1s]')
    fig8.savefig(directory + '/fig8.png', dpi=300)

    # fig9 = plt.figure()
    # ax9 = fig9.add_subplot(111)
    # ax9.scatter(horizon_data['y'], horizon_data['x'], s = 0.1, label='map')
    # ax9.scatter(X_coordinates, Y_coordinates, c='r', s=10, marker='o', lw = 0, label='route')
    # ax9.set_title('Track XY plane')
    # ax9.set_xlabel('X [m]')
    # ax9.set_ylabel('Y [m]')
    # fig9.savefig('plots/fig9.png', dpi = 300)
    fig10 = plt.figure(num=None, figsize=(40, 30),
                       dpi=300, facecolor="None", edgecolor='k')
    ax10 = fig10.add_subplot(111)
    # ax10.scatter(horizon_data['y'], horizon_data['x'], s = 0.1, label='map')
    ax10.scatter(delimiter_data['x'], delimiter_data[
                 'y'], s=10, c='black', lw=1, label='delimiter lines')
    ax10.scatter(mapobject_data['x'], mapobject_data[
                 'y'], s=0.5, c='lawngreen', lw=0, label='map objects')
    ax10.scatter(refline_data['x'], refline_data[
                 'y'], s=1, c='lightskyblue', marker='x', label='reference lines')
    ax10.scatter(X_coordinates, Y_coordinates,
                 c='r', s=10, marker='o', lw=0, label='route')
    ax10.set_title('Track XY plane')
    ax10.set_xlabel('X [m]')
    ax10.set_ylabel('Y [m]')
    lgnd = plt.legend(loc='upper left')
    lgnd.legendHandles[0]._sizes = [30]
    lgnd.legendHandles[1]._sizes = [30]
    lgnd.legendHandles[2]._sizes = [30]
    lgnd.legendHandles[3]._sizes = [30]
    fig10.savefig(directory + '/fig10.png', dpi=300)

    fig11 = plt.figure(num=None, figsize=(40, 30),
                       dpi=300, facecolor="None", edgecolor='k')
    ax11 = fig11.add_subplot(111)
    # cmap=plt.get_cmap('tab10')
    # colors=['red','tan','gold','palegreen','plum','g','c','darkviolet','aqua','tomato','lavender','pink']
    # ax11.scatter(delimiter_data['x'], delimiter_data['y'], s = 1, c='black',
    # lw = 0, label='delimiter lines')
    ax11.scatter(delimiter_data['x'], delimiter_data[
                 'y'], s=1, c=delimiter_data['f'] + 10, lw=0, label='delimiter lines')
    ax11.scatter(X_coordinates, Y_coordinates,
                 c='r', s=10, marker='o', lw=0, label='route')
    ax11.set_title('Track XY plane')
    ax11.set_xlabel('X [m]')
    ax11.set_ylabel('Y [m]')
    plt.gca().set_aspect('equal', adjustable='box')
    lgnd = plt.legend(loc='upper left')
    lgnd.legendHandles[0]._sizes = [30]
    lgnd.legendHandles[1]._sizes = [30]
    fig11.savefig(directory + '/fig11.png', dpi=300)

    fig12 = plt.figure(
        num=None, figsize=(40, 30), dpi=300, facecolor="None", edgecolor='k')
    ax12 = fig12.add_subplot(111)
    ax12.hist(delimiter_data['f'])
    # ax12.bar(range(12), delimiter_data['f'], color=range(12))
    fig12.savefig(directory + '/fig12.png', dpi=300)

    fig13 = plt.figure(
        num=None, figsize=(40, 30), dpi=300, facecolor="None", edgecolor='k')
    ax13 = fig13.add_subplot(111)
    ax13.scatter(virtualline_data['x'], virtualline_data[
                 'y'], s=1, c='lavender', lw=0, label='Virtual path')
    ax13.scatter(solidline_data['x'], solidline_data[
                 'y'], s=1, c='b', lw=0, label='Solid line')
    ax13.scatter(dashedline_data['x'], dashedline_data[
                 'y'], s=1, c='lightcoral', lw=0, label='Dashed line')
    ax13.scatter(curb_data['x'], curb_data[
                 'y'], s=1, c='slategrey', lw=0, label='Curb')
    ax13.scatter(X_coordinates, Y_coordinates, c='r',
                 s=10, marker='o', lw=0, label='GPS route')
    ax13.set_title('Track XY plane')
    ax13.set_xlabel('X [m]')
    ax13.set_ylabel('Y [m]')
    plt.gca().set_aspect('equal', adjustable='box')
    lgnd = plt.legend(loc='upper left')
    lgnd.legendHandles[0]._sizes = [30]
    lgnd.legendHandles[1]._sizes = [30]
    lgnd.legendHandles[2]._sizes = [30]
    lgnd.legendHandles[3]._sizes = [30]
    lgnd.legendHandles[4]._sizes = [30]
    fig13.savefig(directory + '/fig13.png', dpi=300)

    fig14 = plt.figure(
        num=None, figsize=(8, 6), dpi=80, facecolor="None", edgecolor='k')
    ax14 = fig14.add_subplot(111)
    ax14.plot(mindistances_solid, color='b',
              label='Solid line', marker='o', lw=0)
    ax14.plot(mindistances_dashed, color='lightcoral',
              label='Dashed line', marker='o', lw=0)
    # ax14.plot(mindistances_curb, color='slategrey', label='Curb',
    # marker='o', lw=0)
    ax14.set_title('Minimal distance to delimiter lines')
    ax14.set_xlabel('time [0.1s]')
    ax14.set_ylabel('distance [m]')
    lgnd = plt.legend(loc='upper left')
    fig14.savefig(directory + '/fig14.png', dpi=300)

    fig15 = plt.figure(
        num=None, figsize=(8, 6), dpi=80, facecolor="None", edgecolor='k')
    ax15 = fig15.add_subplot(111)
    ax15.plot(ego_data['h'])
    ax15.set_title('Engagement Status')
    engagenment_labels = ["", "Initializing", "Ready", "Engaged", "Error", ""]
    ax15.set_yticklabels(engagenment_labels)
    plt.yticks(np.arange(-1, 5, 1.0))
    fig15.savefig(directory + '/fig15.png', dpi=300)

    # plt.show()
    plt.close()
