import { connect } from 'react-redux';
import { MapDetail } from './MapDetail';
import { showError } from '../../redux/App/AppActions';
import { getGPSdata,cleanMapComponentData } from '../../redux/Overview/OverviewActions'
import './MapDetail.scss';

function mapStateToProps(state, ownProps) {
    return {
        points: state.points
    };
}

export default connect(
    mapStateToProps,
    { showError, getGPSdata, cleanMapComponentData }
)(MapDetail);