"""Job Record"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from config.config_handler import config_dict
from handlers.auth import auth
from handlers.utils import utils
from model.exceptions import DANFApiError
from proxies.storage_queue import EventListener
from requesthandler.base import BaseRequestHandler

logger = logging.getLogger(__name__)


class DeleteRecord(BaseRequestHandler):

    @auth.requires_scope(["openid"])
    def delete(self):
        queue_name = config_dict.get_value("queue_name")
        event_agent = EventListener(queue_name)
        event_id = self.get_argument('event_id')
        pop_receipt = self.get_argument("pop_receipt")

        res = event_agent.delete_event(event_id,
                                       pop_receipt)

        if not res:
            raise DANFApiError(500, f"deleting message: {event_id}, "
                                    f"with pop_receipt: {pop_receipt} failed")

        utils.build_response(response=self,
                             results=f"deleting message:{event_id} succeeded")
