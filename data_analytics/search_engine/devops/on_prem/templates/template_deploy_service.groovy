node('SMTCA01552') {
 stage('Get repo...') {
    checkout scm
 }

 stage('Launch kubeconfig cli...') {
 try{
    basename = sh (script: "basename ${params.path_to_yaml}", returnStdout: true).trim()
    withCredentials([usernamePassword(credentialsId: 'tech_user_dla', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
    withCredentials([usernamePassword(credentialsId: 'tech_user_dla_k8', usernameVariable: 'KUSERNAME', passwordVariable: 'KPASSWORD')]) {
        //set kubectl yaml to specific version
        if ("latest"=="${params.version}"){
            path = "${params.path_to_component}"
            image_version = sh (script: "python $path/setup.py --version", returnStdout: true).trim()
            git_tag = sh(returnStdout: true, script: "git describe --tag --abbrev=0").trim()
            image_version = "$image_version-$git_tag"
        }
        else {
            image_version = "${params.version}"
        }
        sh"""
        sed -i 's/${params.image_name}:latest/${params.image_name}:$image_version/g' ${params.path_to_yaml}
        """
        //START DOCKER AND LOGIN
        sh"""
        docker login athena.daimler.com --username="$USERNAME" --password="$PASSWORD"
        docker pull athena.daimler.com/dla_docker/kubectl:latest
        docker tag athena.daimler.com/dla_docker/kubectl:latest dla-danf-kubectl
        docker run --rm -itd --name dla-danf-kubectl dla-danf-kubectl
        docker cp ./${params.path_to_yaml} dla-danf-kubectl:/home/appuser/
        docker exec -t dla-danf-kubectl kubelogin --user oidc --username "$KUSERNAME" --password "$KPASSWORD" --insecure-skip-tls-verify
        docker exec -t dla-danf-kubectl kubectl config use-context oidc@${params.loc}-${params.stage}
        docker exec -t dla-danf-kubectl kubectl patch deployment ${params.deployment_label} -p '{"spec": {"template": {"metadata": {"labels": {"build_id": "$BUILD_NUMBER"}}}}}' -n dla-danf || true
        """
        //DEPLOY
        sh"docker exec -t dla-danf-kubectl kubectl apply -n dla-danf -f /home/appuser/$basename"
        sh'docker kill dla-danf-kubectl'
        sh'docker logout athena.daimler.com'
    }
    }
    }
    catch (e) {
    sh"""
    docker kill dla-danf-kubectl
    docker logout athena.daimler.com
    """
    deleteDir()
    throw e
    }
    deleteDir()
 }
}