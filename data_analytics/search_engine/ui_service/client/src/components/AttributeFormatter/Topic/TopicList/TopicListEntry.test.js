import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../../Table';
import TopicListEntry from './TopicListEntry';

describe('TopicListEntry', () => {

  it('should show list of topics', () => {
    const props = {topics: ['1', '2', '3', '4']};
    const wrapper = shallow(<TopicListEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('ScrollList').shallow().find('tr')).toHaveLength(4);
  });

  it('should filter by entering a topic', () => {
    const props = {topics: ['1', '2', '3', '4']};
    const wrapper = shallow(<TopicListEntry {...props} />);
    wrapper.find('FormControl').simulate('change', {target: {value: '1'}});
    expect(wrapper.find('ScrollList').shallow().find('tr')).toHaveLength(1);
  });

  it('should create a disabled button when path not exists', () => {
    const props = {topics: null};
    const wrapper = shallow(<TopicListEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});