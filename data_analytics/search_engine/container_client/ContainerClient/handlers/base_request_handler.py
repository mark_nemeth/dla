__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from enum import Enum

import requests
import time

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# disable detailed HTTP connection debug logs
from http.client import HTTPConnection
HTTPConnection.debuglevel = 0

from DANFUtils.constants import *
from DANFUtils.exceptions import RESTException
from DANFUtils.logging import logger


class BaseRequestHandler:

    COMPONENT = "ContainerClient"
    DEFAULT_RETRY_OPTIONS = Retry(total=3, backoff_factor=0.5, status_forcelist=[500, 502, 503, 504])

    def __init__(self, session_timeout=55, session_maxtime=1500):
        """
        :param session_timeout: refresh session if last request happened session_timeout seconds ago
        :param session_maxtime: maximum length of sessions in seconds
        """
        self.session_timeout = session_timeout
        self.session_maxtime = session_maxtime
        self._init_session()
        self._first_timestamp = time.time()
        self._last_timestamp = self._first_timestamp

    def _init_session(self):
        self.session = requests.Session()
        self.set_retries()

    def send_request(self, http_method, url, timeout=5*60, **kwargs):
        """
        Base method to send requests to the APIs
        :param http_method: The HTTP method to use for the request (Enum)
        :param url: The url to use for the request
        :param timeout: (optional) in seconds, default: 5 min. Details see Requests
        :param kwargs: Additional params for specific HTTP methods (use params for Requests)
        :return: Response of the request
        """
        if not isinstance(http_method, HttpMethod):
            raise TypeError('http_method has to be of type HttpMethod Enum')

        # periodically refresh sessions
        now = time.time()
        if now - self._last_timestamp > self.session_timeout or now - self._first_timestamp > self.session_maxtime:
            self._init_session()
            logger.debug(f"Request session refreshed.")
        self._last_timestamp = time.time()

        try:
            logger.debug(f'Sending {http_method.value} request to {url} with arguments: {kwargs.items()}')

            # TODO: Enable verify once signed certs are deployed
            res = self.session.request(http_method.value, url, timeout=timeout, verify=False, **kwargs)

            logger.debug(
                f'Successfully sent {http_method.value} request to url: {url}. Response: {res}')
        except requests.exceptions.ConnectionError as e:
            raise RESTException(self.COMPONENT, http_method.value, url, REST_CONNECTION_ERROR, e)

        except requests.exceptions.HTTPError as e:
            raise RESTException(self.COMPONENT, http_method.value, url, REST_HTTP_ERROR, e)

        except requests.exceptions.Timeout as e:
            raise RESTException(self.COMPONENT, http_method.value, url, REST_TIMEOUT_ERROR, e)

        except requests.exceptions.TooManyRedirects as e:
            raise RESTException(self.COMPONENT, http_method.value, url, REST_TOO_MANY_REDIRECTS_ERROR, e)

        except requests.exceptions.RequestException as e:
            raise RESTException(self.COMPONENT, http_method.value, url, None, e)

        return res

    def set_retries(self, retries=DEFAULT_RETRY_OPTIONS):
        """
        Set connection retry behavior
        :param retries: The maximum number of retries each connection
        should attempt. Set retries=0 to disable retries.
        If you need granular control over the conditions under
        which we retry a request, import urllib3's ``Retry`` class and pass
        that instead.
        :return:
        """
        assert isinstance(retries, (int, Retry))
        adapter = HTTPAdapter(max_retries=retries)
        self.session.mount('https://', adapter)
        self.session.mount('http://', adapter)


class HttpMethod(Enum):
    POST = 'POST'
    PATCH = 'PATCH'
    PUT = 'PUT'
    GET = 'GET'
    DELETE = 'DELETE'
    OPTIONS = 'OPTIONS'
    HEAD = 'HEAD'
