"""Regressiontests for bagfile routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import random as rd
import unittest
import os

import requests
from DANFUtils.utils import generate_deterministic_guid

os.environ["DANF_ENV"] = "local"
from tests.regressiontests.testutils import get_testdb_connector

# testdata
# can change the localhost into external ip:
base_url = 'http://localhost:8887'
file_url = base_url + '/file/'
guid_url = file_url + '{}/'
add_urls = guid_url + 'urls/'
update_url_status = add_urls + '{}' + '/status/'

https_url = 'https://localhost:8443/file/'

rd.seed("test")

raw_data_bagfile = {
    "file_guid": generate_deterministic_guid(),
    "parent_guid": generate_deterministic_guid(),
    "file_type": "ORIGINAL",
    "file_size": 21231,
    "tags": ["tag", "tag2"],
    "urls": []
}

add_url = {
    "url_id": 0,
    "url_status": "done",
    "entry_created_by": "me",
    "creation_date": 946684899000,
    "accessor_url": "ringdingboing.com"
}

put_url = {
    "file_guid": "efa459ea-ee8c-3ca4-894e-db77e160f32e",
    "url_id": 0,
    "url_status": "PROGRESS"
}

update_url = {
    "file_guid": "efa459ea-ee8c-3ca4-894e-db77e160f32e",
    "url_id": 0,
    "url_status": "DONE"
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['filecatalog'].delete_many(
            {'file_guid': raw_data_bagfile['file_guid']})


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['filecatalog'].delete_many(
            {'file_guid': raw_data_bagfile['file_guid']})


class TestFileCatalogPOST(unittest.TestCase):
    def test_filecatalog_POST(self):
        prepare_db()
        res = requests.put(url=guid_url.format(raw_data_bagfile['file_guid']),
                           json=raw_data_bagfile)
        self.assertEqual(201, res.status_code, 'file catalog create failed')
        data = add_url.copy()
        res = requests.post(url=add_urls.format(raw_data_bagfile['file_guid']),
                            json=data)
        self.assertEqual(200, res.status_code, 'file _post failed')
        cleanup_db()

    def test_filecatalog_invalid_POST(self):
        prepare_db()
        res = requests.put(url=guid_url.format(raw_data_bagfile['file_guid']),
                           json=raw_data_bagfile)
        self.assertEqual(201, res.status_code, 'file catalog create failed')
        data = add_url.copy()
        res = requests.post(url=add_urls.format(raw_data_bagfile['file_guid']),
                            json=data)
        self.assertEqual(200, res.status_code, 'file post failed')
        res = requests.post(url=add_urls.format(raw_data_bagfile['file_guid']),
                            json=data)
        self.assertEqual(409, res.status_code, 'duplicate url not detected')


class TestFileCatalogGET(unittest.TestCase):
    def test_filecatalog_GET(self):
        prepare_db()
        res = requests.put(url=guid_url.format(raw_data_bagfile['file_guid']),
                           json=raw_data_bagfile)
        self.assertEqual(201, res.status_code, 'file catalog create failed')
        res = requests.get(url=guid_url.format(raw_data_bagfile['file_guid']))
        self.assertEqual(200, res.status_code, 'file GET failed')
        cleanup_db()

    def test_bagfile_GET_invalid(self):
        prepare_db()
        res = requests.get(
            url=guid_url.format('12341234-1234-1234-1234-123412341235'))
        self.assertEqual(404, res.status_code, 'Invalid GUID not detected')
        cleanup_db()


class TestFileCatalogStatusPOST(unittest.TestCase):
    def test_filecatalogstatus_POST(self):
        prepare_db()
        res = requests.put(url=guid_url.format(raw_data_bagfile['file_guid']),
                           json=raw_data_bagfile)
        self.assertEqual(201, res.status_code, 'file catalog create failed')
        data = add_url.copy()
        res = requests.post(url=add_urls.format(raw_data_bagfile['file_guid']),
                            json=data)
        self.assertEqual(200, res.status_code, 'url post failed')
        data = update_url.copy()
        res = requests.put(url=update_url_status.format(
            raw_data_bagfile['file_guid'], update_url['url_id']),
                           json=data)
        self.assertEqual(200, res.status_code, 'url post failed')
        cleanup_db()


if __name__ == '__main__':
    unittest.main()
