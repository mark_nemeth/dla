import subprocess
import os
import sys

from termcolor import colored

from cars import *
from test_abstract import *

class LandingTest(AbstractTest):

    def run(self):
        landingProjectPath = self.path
        print(colored("**** TESTING CLUSTER {} ****".format(landingProjectPath), 'green'))
        if not os.path.exists(landingProjectPath):
            print(colored("ERROR: Path {} does not exist! Exit...".format(landingProjectPath), 'red'))
            sys.exit(1)

        result = subprocess.check_output(['du', '-d 2', '-h', '--exclude=run_info', landingProjectPath]).splitlines()
        result = [x.decode('UTF-8').split("\t") for x in result]

        warnings = {}
        errors = {}

        for entry in result:
            if entry[0] == "0":
                if "/" in os.path.relpath(entry[1], landingProjectPath):
                    vin = os.path.split(entry[1])[1]
                    if vin in warnings.keys():
                        warnings[vin].append(entry)
                    else:
                        warnings[vin] = [entry]
            else:
                if "/" in os.path.relpath(entry[1], landingProjectPath):
                    vin = os.path.split(entry[1])[1]

                    if vin in errors.keys():
                        errors[vin].append(entry)
                    else:
                        errors[vin] = [entry]

        for vehc in errors.keys():
            if vehc not in cars.carMap.keys():
                print("Vehicle {} undefined!".format(vehc))
                continue
            print("Errors for vehicle {} ({})".format(vehc, cars.carMap.get(vehc)))
            for entry in errors[vehc]:
                if len(cars.carMap.get(vehc)) > 5:
                    tabs = "\t"
                else:
                    tabs = "\t\t"
                print(colored(" * ERROR ({}): {}{} contains data of size {}".format(cars.carMap.get(vehc), tabs, entry[1], entry[0]), 'red'))

        for vehc in warnings.keys():
            if vehc not in cars.carMap.keys():
                print("Vehicle {} undefined!".format(vehc))
                continue
            print("Warnings for vehicle {} ({})".format(vehc, cars.carMap.get(vehc)))
            for entry in warnings[vehc]:
                if len(cars.carMap.get(vehc)) > 5:
                    tabs = "\t"
                else:
                    tabs = "\t\t"
                    print(colored(" * WARNING ({}): {}{} contains data of size {}".format(cars.carMap.get(vehc), tabs, entry[1], entry[0]), 'yellow'))

        if len(errors.keys()) > 0:
            return False
        else:
            return True
