#include <algorithm>
#include <vector>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <sys/errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/resource.h>
#include <signal.h>
#include "hps.h"

using namespace std;

#define MAX_RAW_PACKET_SIZE		(64 * 1024)

typedef struct
{
	uint32_t size;
	uint32_t sequence;
	uint64_t timestamp;
} msgHeaderT;

struct hpRawServerT
{
	hps_type type = HPS_RAW;
	const char *out = NULL;
	int sock = -1;
	thread handlerThread;
	bool shutdownRequest = false;
	hpStreamT *s = NULL;

	hpRawServerT(const char *dev, const char *out)
	{
		printf("RAW Server listening on device %s\n", dev);

		// Setup server socket
		sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	    if (sock == -1)
	    {
	    	perror("Failed to create RAW socket");
	    	throw exception();
	    }

	    // Set to promiscuous mode
	    struct ifreq ifr;
	    memset(&ifr, 0, sizeof(struct ifreq));
	    strcpy(ifr.ifr_name, dev);
	    if (ioctl(sock, SIOCGIFFLAGS, &ifr) == -1)
		{
			perror("Failed to get device flags");
			throw exception();
		}
	    ifr.ifr_flags |= IFF_PROMISC;
	    if (ioctl(sock, SIOCSIFFLAGS, &ifr) == -1)
		{
			perror("Failed to set promiscuous mode");
			throw exception();
		}

	    // Bind
	    struct sockaddr_ll addr;
	    memset(&addr, 0, sizeof(addr));
	    addr.sll_family = PF_PACKET;
	    addr.sll_ifindex = if_nametoindex(dev);
	    addr.sll_protocol = htons(ETH_P_ALL);
	    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)))
	    {
			perror("Failed to bind socket");
			throw exception();
	    }

		s = hps_open(out);
    	if (s == NULL)
    	{
    		printf("Failed to open otput stream %s\n", out);
    		throw exception();
    	}

	    handlerThread = thread(&hpRawServerT::run, this);
	}

	~hpRawServerT()
	{
	    shutdownRequest = true;
	    handlerThread.join();

	    // Close socket
		if (sock != -1)
		    close(sock);

	    // Close out stream
	    if (s != NULL)
	    	hps_close(s);
	}

	/*
	 * This handler receives and stores the data.
	 */
	void run()
	{
		uint32_t lastSequence = -1;

	    while (1)
	    {
			// Acquire buffer
			void *buffer;
			int n = hps_write_begin(s, &buffer, MAX_RAW_PACKET_SIZE);
			if (n < MAX_RAW_PACKET_SIZE)
			{
				if (0 <= n) {
					pthread_yield();
					continue;
				}
				else {
					perror("hps_write_begin() failed");
					break;
				}
			}

			// Receive data
			n = recv(sock, buffer, n, 0);
			if (0 < n)
			{
				// Ethernet
				struct ethhdr *eth = (struct ethhdr *)(buffer);
				if (ntohs(eth->h_proto) != ETH_P_IP) {
					continue;
				}
				printf("%.2X:%.2X:%.2X:%.2X:%.2X:%.2X -> %.2X:%.2X:%.2X:%.2X:%.2X:%.2X 0x%04X\n",
						eth->h_source[0], eth->h_source[1], eth->h_source[2],
						eth->h_source[3], eth->h_source[4], eth->h_source[5],
						eth->h_dest[0], eth->h_dest[1], eth->h_dest[2],
						eth->h_dest[3], eth->h_dest[4], eth->h_dest[5],
						ntohs(eth->h_proto));


				// IP
				struct iphdr *ip = (struct iphdr*)((uintptr_t)buffer + sizeof(struct ethhdr));
				if (ip->protocol != IPPROTO_UDP) {
					continue;
				}

				// UDP
				struct udphdr *udp = (struct udphdr*)((uintptr_t)ip + sizeof(struct iphdr));
				if (ntohs(udp->uh_dport) != 7000) {
					continue;
				}

				// Data
				void *data = (void*)((uintptr_t)udp + sizeof(struct udphdr));

				// Cast message header
				msgHeaderT *header = (msgHeaderT*)data;

				// Check sequence number
				uint32_t lost = header->sequence - lastSequence - 1;
				if (0 < lost)
				{
					__sync_add_and_fetch(&statistics.rxLostMessages, lost);
				}
				lastSequence = header->sequence;

				hps_write_end(s, n);

				size_t dataSize = n - sizeof(struct ethhdr) - sizeof(struct iphdr) - sizeof(struct udphdr);

				__sync_add_and_fetch(&statistics.rxMessages, 1);
				__sync_add_and_fetch(&statistics.rxBytes, dataSize);
			}
			else
			{
				perror("Failed to receive RAW packet");
				break;
			}
	    }
	}
};

hpRawServerT* hps_raw_start_server(const char *dev, const char *out)
{
	try
	{
		return new hpRawServerT(dev, out);
	}
	catch (exception&)
	{
		return NULL;
	}
}

int hps_raw_close_server(hpRawServerT *server)
{
    delete server;

    return 0;
}
