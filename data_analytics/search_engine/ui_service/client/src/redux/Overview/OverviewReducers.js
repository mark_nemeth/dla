import {createReducer} from '@reduxjs/toolkit';
import * as actions from './OverviewActions';

export const OverviewReducer = createReducer({}, {
  [actions.fetchFileByGuidSuccess]: (state, action) => {
    state.error = false;
    state.overview.bagfile = action.payload.bagfile;
    state.overview.childbagfiles = action.payload.childbagfiles;
  },
  [actions.loadMapError]: (state, action) => {
    state.points = action.payload;
  },
  [actions.loadMap]: (state, action) => {
    state.points = action.payload;
  },
  [actions.cleanMapComponent]: (state, action) => {
    state.points=[];
  },
  [actions.fetchFileByGuidError]: (state, action) => {
    state.error = true;
    state.errorMessageType = 'danger';
    state.errorMessage = action.payload;
    state.overview.bagfile = {};
    state.overview.childbagfiles = {};
  },
  [actions.fetchFlowRunsSuccess]: (state, action) => {
    state.error = false;
    state.overview.flowRuns = action.payload;
  },
  [actions.fetchFlowRunsError]: (state, action) => {
    state.error = true;
    state.errorMessageType = 'info';
    state.errorMessage = 'Problems with flow runs';
    state.overview.flowRuns = {};
  },
  [actions.highlightSelectedChildBagfile]: (state, action) => {
    state.overview.selectedChildbagfileGuid = action.payload;
  },
  [actions.highlightHoveringChildBagfile]: (state, action) => {
    state.overview.hoveringChildbagfile = action.payload;
  },
  [actions.highlightSelectedEvent]: (state, action) => {
    state.overview.selectedEvent = action.payload;
  },
  [actions.highlightHoveringEvent]: (state, action) => {
    state.overview.hoveringEvent = action.payload;
  },
  [actions.fetchFileByBagPathSuccess]: (state, action) => {
    state.error = false;
    let file = action.payload;

    for (let i = 0, len = file.locations.length; i < len; i++) {
      let location = file.locations[i];
      if (location.paths && location.paths.length > 0) {
        location.status = 'Available';
      } else if (!location.hasOwnProperty('transfer') || location.transfer === null) {
        location.status = 'Unavailable';
      } else if (location.transfer.status === "WAITING") {
        location.status = "Transfer Requested";
      } else if (location.transfer.status === "STARTED") {
        location.status = "Transferring";
      } else if (location.transfer.status === "ERROR") {
        location.status = "Error";
      } else if (location.transfer.status === "NEEDS_APPROVAL") {
        location.status = "Unavailable";
      } else if (location.transfer.status === "ALREADY_AVAILABLE") {
        location.status = "Unavailable"
      } else if (location.transfer.status === "COMPLETED") {
        location.status = "Unavailable"
      }
    }

    state.httpRequests.fetchFileByBagPathRequest.ongoing = false;
    state.overview.file = file;
  },
  [actions.fetchFileByBagPathError]: (state, action) => {
    state.overview.file = null;
    state.error = true;
    state.errorMessageType = 'danger';

    if (action.payload.response.status === 404) {
      state.error = false; // This is not an error case. Info is shown in LocationDetail as non-error notice
    } else if (action.payload.response.data.type === "apiHealthError") {
      state.errorMessage = action.payload.response.data.message;
    } else {
      state.errorMessage = "An unknown error occurred while loading the location information of this bagfile.";
    }
    state.httpRequests.fetchFileByBagPathRequest.ongoing = false;
  },
  [actions.fetchFileByBagPathBegin]: (state, action) => {
    state.httpRequests.fetchFileByBagPathRequest.ongoing = true;
  },
  [actions.clearOverviewBagfile]: (state, action) => {
    state.overview.bagfile = {};
    state.overview.file = {};
    state.overview.flowRuns = {};
    state.overview.childbagfiles = [];
  },
  [actions.showSelectedDriveItem]: (state, action) => {
    state.overview.selectedDriveItem = action.payload.item;
    state.overview.selectedDriveItemType = action.payload.itemType;
  },
  [actions.clearSelectedDriveItem]: (state, action) => {
    state.overview.selectedDriveItem = {};
    state.overview.selectedDriveItemType = {};
  },
});
