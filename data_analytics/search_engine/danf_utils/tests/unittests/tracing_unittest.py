__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
#os.environ['APPINSIGHTS_INSTRUMENTATIONKEY'] = '39c9fdb0-9fe0-45b2-bffb-d0c5b955dc63'

import subprocess
import unittest
from random import random
from time import sleep

from DANFUtils.tracing import tracing


class TestTracingStartAndEnd(unittest.TestCase):

    def test_single_trace(self):
        # start 'request' task for this subprocess
        tracing.start_task('danf-extractor')
        sleep(random()*5)
        tracing.end_task()

    def test_nested_trace(self):
        # start 'request' task for this subprocess
        tracing.start_task('danf-splitter')

        tracing.start_task('split-subprocess')
        sleep(random()*5)
        tracing.end_task()

        tracing.start_task('split-subprocess')
        sleep(random()*5)
        tracing.end_task()

        tracing.end_task()

    def test_nested_decorators_trace(self):
        self._do_split()

    def test_system_exit(self):
        tracing.start_task('danf-splitter')
        tracing.start_task('split-subprocess')
        exit()

    def test_propagation_via_env_vars_to_subprocesses(self):
        tracing.start_task('danf-splitter')

        env = dict(os.environ)
        env.update(tracing.get_propagation_vars())

        subprocess.run(['python', 'process_test_split_subprocess.py'], env=env)
        subprocess.run(['python', 'process_test_split_subprocess.py'], env=env)

        tracing.end_task()

    @tracing.trace_task('danf-splitter')
    def _do_split(self):
        self._do_split_subprocess()
        self._do_split_subprocess()

    @tracing.trace_task('split-subprocess')
    def _do_split_subprocess(self):
        sleep(random()*5)
