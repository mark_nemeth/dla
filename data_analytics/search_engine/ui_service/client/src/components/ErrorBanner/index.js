import {connect} from 'react-redux';
import {ErrorBanner} from './ErrorBanner';
import {hideError, showError} from '../../redux/App/AppActions';

function mapStateToProps(state, ownProps) {
  const error = state.error || false;
  const errorMessage = state.errorMessage || 'TEST ERROR MESSAGE';
  const variant = state.errorMessageType || 'primary';
  const disappearMilSeconds = state.errorDisappearTimer || 10000;
  return {error, errorMessage, variant, disappearMilSeconds};
}

export default connect(
  mapStateToProps,
  {showError, hideError}
)(ErrorBanner);