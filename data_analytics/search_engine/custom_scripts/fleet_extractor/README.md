### Version: see setup.py
Description: this version is suitable to run the the fleet_extractor locally.

# Description
The fleet extractor can take in raw data(Blue Pirate data, which need to be convertered), while the `dla_converter` should be `True`
and also can take in `Bagfile data`. After the extraction process, the output will be sent to the ContainerClient

# Prerequsities
The following ENV(environment variables) should be ready

## Mandatory ENV
1. Bagfile_GUID (if input is bagfile)
 e.g. 3bb266b2-b62d-49f8-b2ca-3c30287aa693
2. APPINSIGHTS_INSTRUMENTATIONKEY=APPINSIGHTS_DISABLE_KEY this is set to default (disable mode)
3. INPUT_PATH of the file
4. DLA_CONVERTER: True of False
5. DBC_FILE: 
   if want to use the dla_converter function, then this file is necessary 
   

## Optional ENV
1. OUTPUT_PATH of the file:  
the output path of the convertered bagfile, by default, the file will stored locally `./bagfile/tmp_convertered_file.bag`
We suggest that set OUTPUT_PATH,


# Run in Docker
1. build the docker image
```commandline
docker build -t fleet-extractor:test01 . --network host --no-cache --build-arg INDEX_URL_WITH_ACCESS=https://ATTDATA:rm3t6l2uav7wxwrvyoxxscc5wjpgrera32njstdbqiopng47oyaq@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/
```

2. run the docker locally
For example:
```commandline
docker run -v /lhome/yxiaoli/Downloads/:/data -e BAGFILE_GUID=2941229c-6d10-4226-8422-eb611055577d -e INPUT_PATH=/data/infra_can_example.bag -e DLA_CONVERTER=False -e OUTPUT_PATH=/data/output.bag -e DBC_FILE=/data/Infrastructure_CAN_222_190227.dbc --network host fleet-extractor:test01
```

**NOTE**
1. The file path should be mounted to docker container:
   
   When you run in docker, you cannot add your local file_path. Otherwise the "FileNotFound" error will occur.
   `-v your_local_path/:/path_in_docker` should be added. so when path environment have changed into, e.g.:
   `-e INPUT:PATH=/path_in_docker/XXX.zip`
    
2. Use the localhost network:
   
   add this `--network host`

3. OUPUT_PATH
   recommended to add.

# Reference
1. https://athena.daimler.com/confluence/display/ATROB/Data+Converter

