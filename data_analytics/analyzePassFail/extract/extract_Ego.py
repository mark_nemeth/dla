import rosbag
import sys

#bag = rosbag.Bag('20180214_105847_onlyimar.bag')

bag = rosbag.Bag(sys.argv[1])

topic = '/CalService/instance_0/EgoVehStateInterface'

filename = 'csv/Out'+topic.replace("/","_")+'_2018.csv'


with open(filename, 'w') as f:
  for msg in bag.read_messages(topics=[topic]):
    #print(msg.message)
    #print >> f, msg.message.longitude
    f.write(str(msg.message.header.timestamp.s)+","
           +str(msg.message.header.timestamp.ns)+","
           +str(msg.message.velocity_x)+","
           +str(msg.message.acceleration_x)+","
           +str(msg.message.acceleration_y)+","
           +str(msg.message.yaw_rate)+","
           +str(msg.message.vehicle_steer_angle)+","
           +str(msg.message.ad_engagement_state)+","
           +str(msg.message.current_drive_mode)
           +"\n")


f.close()
bag.close()    
