## Docker Container for running jupyter notebooks on the kubernetes cluster

to build docker image:
```console
docker build -t att-notebooks:latest . --network host --no-cache --build-arg EXTRA_INDEX_URL=https://njuwfxcpjy7wz7zynoxyns3djob5655googzdpb4aue5dxt4jgzq@daimler.pkgs.visualstudio.com/_packaging/ATTDATA/pypi/simple/ --build-arg http_proxy=http://localhost:3128/ --build-arg https_proxy=http://localhost:3128/
```
to run it locally
```console
docker run -p 8888:8888 att-notebooks:latest jupyter notebook --ip=0.0.0.0 --NotebookApp.token='c3e7519274f50cdfc22c82622da6cd2647675312e55ab56b'
```
to run it on Vaihingen integration cluster (already deployed)
```console
kubectl port-forward deployment/att-notebooks  8888:8888 -n attdata-danf
```
then, on your local browser, hit:
```console
http://localhost:8888/notebooks/
```
use this token to login:
```console
c3e7519274f50cdfc22c82622da6cd2647675312e55ab56b
```
Run the disengagement report Notebook or start your own notebook
The python environment has the container client set up in addition to all data science libraries

example of container client:
```console
from ContainerClient import ContainerClient
cont = ContainerClient()
# meta-api call
cb = cont.get_bagfile(bagfile_guid='0405224f-8f2a-4b79-9fd6-6a81c8905486') 
# search-api call
search_result = cont.search_bagfiles({}, limit=100, offset=10)
```
(for advance users)
you can use curl to call meta-api and search-api, example:
```console
#!curl -k --header "Content-Type: application/json" --request GET https://search-api-service.attdata-danf.svc.cluster.local/search/bagfile?limit=100&offset=200 > json100_2.json
```
In addition to direct access to MapR, example:
```console
! ls /mapr/059-va2.mbc.de/data/testzone/buffer_test/rd/athena/
! cat /mapr/059-va2.mbc.de/data/testzone/buffer_test/rd/athena/08_robotaxi/plog/WDD2221591A470239/2020/03/11/20200311_113605_alwayson.bag	
```

### Applications included in the dokcer
* [Jupyter Notebooks](https://jupyter-notebook.readthedocs.io/en/stable/)
* [JupyterLab](https://jupyterlab.readthedocs.io/en/latest/)
* [Hail 0.2](https://hail.is/docs/0.2/index.html)
* [ROS docker](https://github.com/shikishima-TasakiLab/ros1-docker)

## Kernels
* Python 2.7
* Python 3.6

## Libraries
   * google-cloud-sdk=0.36.*
   * numpy=1.18.*
   * pandas=1.*
   * seaborn=0.10.*
   * firecloud-api=0.26.*
   * scikit-learn=0.23.*
   * statsmodels=0.11.*
   * ggplot=0.11.*
   * bokeh=2.*
   * pyfasta=0.5.*
   * pdoc=0.3.*
   * biopython=1.*
   * bx-python=0.8.*
   * fastinterval=0.1.*
   * matplotlib-venn=0.11.*
   * python-datauri=0.2*
   * jupyter_contrib_nbextensions=0.5.*
   * jupyter_nbextensions_configurator=0.4.*
   * cookiecutter=1.7.*
   * fa2=0.3.* 
   * mnnpy=0.1.* 
   * MulticoreTSNE=0.1.* 
   * plotly=4.8.*
   * beautifulsoup4=4.9.* 
   * pythran-openblas=0.3.* 
   * bokeh=2.1.* 
   * Bottleneck=1.3.* 
   * cloudpickle=1.4.* 
   * Cython=0.29.* 
   * dask=2.15.* 
   * dill=0.3.* 
   * h5py=2.10.* 
   * ipywidgets=7.5.* 
   * ipympl=0.5.* 
   *  numba=0.48.* 
   *  numexpr=2.7.* 
   *  patsy=0.5.* 
   *  protobuf=3.11.* 
   *  tables=3.6.* 
   *  scikit-image=0.16.* 
   *  sqlalchemy=1.3.* 
   *  statsmodels=0.11.* 
   *  sympy=1.5.* 
   *  vincent=0.4.* 
   *  widgetsnbextension=3.5.*
   *  xlrd=1.2.* 
   *  pandas-profiling=2.8.*  
   *  pyrosbag=0.1.*  
   * chart-studio=1.1.*  
   * opencv-python=4.3.*

## Extensions
* jupyter_localize_extension.py
   * Server-side extension to provide the `/localize` endpoint to localize/delocalize files between
     GCS and the Leo cluster.
* jupyter_delocalize.py
   * Provides a custom Jupyter [ContentsManager](https://jupyter-notebook.readthedocs.io/en/stable/extending/contents.html)
     which persists notebooks to a configurable GCS location upon save.
* google_sign_in.js
   * Front-end extension which keeps the user signed into Google while using a notebook.