# Periodic tasks
### Version: see setup.py

This component can executes various optional tasks. The cronjob folder contains a number of
kubernetes cron job definitions to periodically execute those tasks.

## First steps
1. Log into athena artifactory with your credentials.
2. Search for "ATTDATADANF_pypi" and generate a password.
3. Create your EXTRA_INDEX_URL with your generated password. (it looks like: https://<YOUR_USERNAME>:<YOUR_KEY_FROM_ARTIFACTORY>@athena.daimler.com/artifactory/api/pypi/ATTDATADANF_pypi/simple/)


```console
export index_url_with_access=https://<YOUR_USERNAME>:<YOUR_KEY_FROM_ARTIFACTORY>@athena.daimler.com/artifactory/api/pypi/ATTDATADANF_pypi/simple/
```

## Build and push docker image:

```console
registry=dlaregistrydev.azurecr.io
docker build -t ${registry}/periodic-tasks:${version} . --network=host --no-cache --build-arg INDEX_URL_WITH_ACCESS=$index_url_with_access_from_azure_devops
docker push ${registry}/periodic-tasks:${version}
```


## Usage

The script supports execution of multiple optional tasks. Tasks are activated via command line arguments.
The base scripts as well as the individual tasks depend on environment variables for input.
Those are listed in the Environment variables section.


### Run script locally:
```console
# export required env variables
export ...

# display help
python -m periodictasks.index --help

# execute refresh-caches and refill-quota tasks
python -m periodictasks.index --tasks refresh-caches refill-quota
```


### Run docker image

```console
# display help
docker run --network=host -it [-e <env variables>] ${image}

# execute delete-queue-message and set-sync-flag tasks
docker run --network=host -it [-e <env variables>] ${image} --tasks refresh-caches refill-quota
```


### Tasks

- refresh-caches: Trigger endpoints for refreshing caches
- refill-quota: Trigger endpoint for refilling voting quota


### Environment variables

Describes the required and optional env variables for the base script and individual tasks.

|Task|Parameter                  |Required   |Description|
|---|---|---|---|
|base script            |APPINSIGHTS_INSTRUMENTATIONKEY |yes    |see DANFTracingClient documentation|
|base script            |DANF_TRACING_TRACE_ID          |no     |see DANFTracingClient documentation|
|base script            |DANF_TRACING_SPAN_ID           |no     |see DANFTracingClient documentation|
|base script            |DANF_TRACING_TRACE_OPTIONS     |no     |see DANFTracingClient documentation|
|refresh-caches         |metadata client related envs   |yes    |Env variables required to make ContainerClient calls to metadata API|
|refill-quota           |metadata client related envs   |yes    |Env variables required to make ContainerClient calls to metadata API|
