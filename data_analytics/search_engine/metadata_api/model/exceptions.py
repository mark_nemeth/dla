"""Exceptions"""
from typing import List, Union

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.exceptions import DANFBaseException
from tornado.web import HTTPError


class DANFApiError(HTTPError, DANFBaseException):

    @staticmethod
    def raise_from(exp, status_code):
        raise DANFApiError(status_code, str(exp)) from exp

    def __init__(self, status_code: int, message: str):
        HTTPError.__init__(self, status_code)
        DANFBaseException.__init__(self, "MetadataAPI", message)
        self.error_message = message

    def __str__(self):
        return self.error_message


class RequestNotValidError(DANFApiError):

    def __init__(self, message):
        super(RequestNotValidError, self).__init__(400, message)


class InvariantViolationError(DANFApiError):

    def __init__(self, message):
        super(InvariantViolationError, self).__init__(
            409, f"Invariant violation: {message}")


class GuidNotFoundError(DANFApiError):

    def __init__(self, guids: Union[str, List[str]], entity: str = 'entity'):
        msg = (f"GUID(s) {guids} could not be found. "
               f"{entity}(s) could not be retrieved")
        super(GuidNotFoundError, self).__init__(404, msg)


class TimestampOutOfRangeException(DANFBaseException):
    """Exception class for handling the violation of valid time stamp ranges
    """

    def __init__(self, msg, timestamp, min, max):
        msg = f"Timestamp {timestamp} not in range ({min}, {max}). ({msg})"
        super(TimestampOutOfRangeException, self).__init__('MetadataAPI', msg)
