To install notebook run:
sudo ./install.sh

Make sure /mapr/738.mbc.de is mapped on your Linux machine
sudo it4ad-posix-client stop
sudo it4ad-posix-client start

To start PySpark console session run:
./session.sh

To start Jupyter browser session run:
./notebook.sh

To shutdown Jupyter session press CTRL-C and confirm with 'y'

