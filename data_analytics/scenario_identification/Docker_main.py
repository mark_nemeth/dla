import numpy as np
import math
import rosbag
import rospy as rspy
import keras
import os
import json
import codecs


def main():
    try:
        os.environ["BAG_PATH"]
    except OSError:
        print ("BAG_PATH variable not set")
    else:
        print ("Successfully set BAG_PATH variable")
    try:
        os.environ["NN_MODEL"]
    except OSError:
        print ("NN_MODEL variable not set")
    else:
        print ("Successfully set NN_MODEL variable")
    try:
        int(os.environ["TAKEOUT"])
    except OSError:
        print ("TAKEOUT variable not set")
    else:
        print ("Successfully set TAKEOUT variable")
    bag_path = os.environ["BAG_PATH"]
    model_name = os.environ["NN_MODEL"]
    takeout = int(os.environ["TAKEOUT"])
    bag = rosbag.Bag(bag_path)
    # variables that have to match with the net
    most_objects = 256
    nr_of_extra_info = 2
    vars_in_obj = 7
    output_csv = np.empty([most_objects * vars_in_obj + nr_of_extra_info, 0])
    current_row = 0
    # going through the .bag file and restructure it for the net
    for topic, msg, t in bag.read_messages(topics=["/fusion/FusionDynamicWorldService/0/DynamicWorld"]):
        current_array = np.zeros(most_objects * vars_in_obj + nr_of_extra_info)
        current_matrix = []
        a_timestamp = rspy.Time(msg.header.timestamp.s, msg.header.timestamp.ns)
        # read out the variables of the objects
        for i in msg.object_list:
            current_matrix.append(np.zeros(vars_in_obj))
            current_matrix[-1][0:2] = i.state_estimate.obb[6:8]
            current_matrix[-1][2:4] = i.state_estimate.vel[0:2]
            current_matrix[-1][4] = math.atan2(i.state_estimate.obb[1], i.state_estimate.obb[0])
            current_matrix[-1][5:7] = i.state_estimate.obb[9:11]
        # read the time
        current_array[most_objects * vars_in_obj + 0] = a_timestamp.secs
        current_array[most_objects * vars_in_obj + 1] = a_timestamp.nsecs
        # sorting of the objects for the angle
        sorting_array = np.empty((len(current_matrix)))
        finder_list = []
        for i in range(len(current_matrix)):
            sorting_array[i] = math.atan2(current_matrix[i][1], current_matrix[i][0])
            finder_list.append(math.atan2(current_matrix[i][1], current_matrix[i][0]))
        sorted_angles = sort_for_angle_new(sorting_array, most_objects)
        for i in range(most_objects):
            if len(sorted_angles[i]) > 0:
                current_array[i * vars_in_obj:(i + 1) * vars_in_obj] = current_matrix[finder_list.index(sorted_angles[i][0])]
        # saving it in the big net
        output_csv = np.column_stack((output_csv, current_array))
        current_row += 1
        if current_row % 100 == 0:
            print "Datasets processed: " + str(current_row)

    bag.close()
    output_csv = np.transpose(output_csv)
    # loading model and predict labels
    trained_model = keras.models.load_model(model_name)
    validation_generator = DataGenerator(dataset=output_csv[::takeout, :most_objects * vars_in_obj], dim=(41, most_objects, vars_in_obj))
    timestamps = output_csv[::takeout, most_objects * vars_in_obj:most_objects * vars_in_obj+2]
    del output_csv
    predictions = trained_model.predict_generator(validation_generator)
    predictions = np.column_stack((predictions, timestamps))

    try:
        os.mkdir("output")
    except OSError:
        print ("")
    else:
        print ("Successfully created the directory prediction_results ")
    json_dumper = {}
    json_dumper['moment'] = []
    for i in range(len(predictions)):
        json_dumper['moment'].append({"prediction": predictions[i, 0], "second": predictions[i, 1], "nanosecond": predictions[i, 2]})
    json.dump(json_dumper, codecs.open("output/predicted_net.json", 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)


class DataGenerator(keras.utils.Sequence):
    # Generates data for the Net
    def __init__(self, dataset, dim=(32, 32, 32)):                 # dim=(time_based_steps, max_number_of_objects, params_per_obj) so wird es beim aktuellen Netz uebergeben
        # Initialization
        self.dim = dim
        self.batch_size = 1
        self.dataset = dataset
        self.on_epoch_end()
        self.list_IDs = range(len(self.dataset))
        self.indexes = np.arange(len(self.dataset))

    def __len__(self):
        # Denotes the number of batches per epoch
        return int(np.floor(len(self.dataset) / self.batch_size))

    def __getitem__(self, index):
        # Generate one batch of data
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Find list of IDs
        list_id_temp = [self.list_IDs[k] for k in indexes]
        # Generate data
        x = self.__data_generation(list_id_temp)
        return x, 1

    def on_epoch_end(self):
        # Updates indexes after each epoch
        self.indexes = np.arange(len(self.dataset))

    def __data_generation(self, list_id_temp):
        # Generates data containing batch_size samples
        # Initialization

        x = np.empty((self.batch_size, self.dim[0], self.dim[1], self.dim[2]))
        helpvar = np.empty([self.dim[0], self.dim[1], self.dim[2]])
        # Generate data
        for i, ID in enumerate(list_id_temp):
            # Store sample
            for j in range(self.dim[0]):
                helpvar[j, :, :] = np.reshape(self.dataset[int(ID)], (self.dim[1], self.dim[2]))
            x[i] = helpvar[:, :, :]
        return x


def sort_for_angle_new(array_angles, size):
    # function for sorting
    if len(array_angles) > size:
        print "ERROR"
    fit_into_array = []
    for i in range(size):
        fit_into_array.append([])
    array_angles = np.sort(array_angles)
    if len(array_angles) > 0 and (array_angles[0] < 2*math.pi/size*0-math.pi or array_angles[-1] > 2*math.pi/size*size-math.pi):
        print "ERROR"
    array_angles_test = np.copy(array_angles)
    for i in range(size):
        while len(array_angles_test) > 0 and 2*math.pi/size*i-math.pi <= array_angles_test[0] <= 2*math.pi/size*(i+1)-math.pi:
            fit_into_array[i].append(array_angles_test[0])
            array_angles_test = np.delete(array_angles_test, 0)

    while len(max(fit_into_array, key=len)) > 1:
        longest_list = max(fit_into_array, key = len)
        [angles, start_index, end_index] = get_positions(fit_into_array, longest_list, size, fit_into_array.index(longest_list), fit_into_array.index(longest_list))
        if start_index < end_index:
            for i in range(len(angles)):
                fit_into_array[start_index+i] = [angles[i]]
        else:
            for i in range(size-start_index):
                fit_into_array[start_index+i] = [angles[i]]
            for j in range(end_index+1):
                fit_into_array[j] = [angles[size-start_index+j]]

    return np.asarray(fit_into_array)


def get_positions(list_of_objects, angles, size, start_i, end_i):
    x_center = []
    y_center = []
    start_index = 0
    end_index = 0
    for i in range(len(angles)):
        x_center.append(math.cos(angles[i]))
        y_center.append(math.sin(angles[i]))
    x_center = sum(x_center)/len(x_center)
    y_center = sum(y_center)/len(y_center)
    if x_center == 0 and y_center == 0:
        print "center look out!"
    center = math.atan2(y_center, x_center)
    start = center - (len(angles)-1.)/2*2*math.pi/size
    if start < -math.pi:
        start += 2*math.pi
    end = center + (len(angles)-1.)/2*2*math.pi/size
    if end > math.pi:
        end -= 2*math.pi
    for i in range(size):
        if 2*math.pi/size*i-math.pi <= start < 2*math.pi/size*(i+1)-math.pi:
            start_index = i
            break
    for i in range(size):
        if 2*math.pi/size*i-math.pi <= end < 2*math.pi/size*(i+1)-math.pi:
            end_index = i
            break

    if start_i < start_index and not start_i <= end_i < start_index:
        while start_i < start_index:
            start_index -= 1
            end_index -= 1
    elif start_index < end_i < start_i:
        end_index -= start_index+1
        start_index = size-1
        while start_i < start_index:
            start_index -= 1
            end_index -= 1

    if end_i > end_index and not end_i >= start_i > end_index:
        while end_i > end_index:
            end_index += 1
            start_index += 1
    elif end_index > start_i > end_i:
        start_index += size - end_index
        end_index = 0
        while end_i > end_index:
            end_index += 1
            start_index += 1

    new_angles, start_index, end_index = get_close_angles(list_of_objects, start_index, end_index, size)
    if len(new_angles) > len(angles):
        [new_angles, start_index, end_index] = get_positions(list_of_objects, new_angles, size, start_index, end_index)
    return [new_angles, start_index, end_index]


def get_close_angles(list_of_objects, start_index, end_index, size):
    while 1:
        if start_index:
            if not len(list_of_objects[start_index - 1]) == 0:
                start_index -= 1
            else:
                break
        else:
            if not len(list_of_objects[start_index - 1]) == 0:
                start_index = len(list_of_objects) - 1
            else:
                break

    while 1:
        if not end_index == len(list_of_objects)-1:
            if not len(list_of_objects[end_index + 1]) == 0:
                end_index += 1
            else:
                break
        else:
            if not len(list_of_objects[0]) == 0:
                end_index = 0
            else:
                break
    new_angles = get_angles_in_range(list_of_objects, start_index, end_index, size)
    return new_angles, start_index, end_index


def get_angles_in_range(list_of_objects, start_index, end_index, size):
    new_angles = np.empty((1, 0))
    if start_index < end_index:
        for i in range(end_index - start_index + 1):
            new_angles = np.append(new_angles, np.array(list_of_objects[start_index + i])).flatten()
    else:
        for i in range(size - start_index):
            new_angles = np.append(new_angles, np.array(list_of_objects[start_index + i])).flatten()

        new_angles_help = np.empty((1, 0))
        for i in range(end_index - 0 + 1):
            new_angles_help = np.append(new_angles_help, np.array(list_of_objects[i])).flatten()
        new_angles = np.append(new_angles, new_angles_help)
    return new_angles


if __name__ == "__main__":
    main()
