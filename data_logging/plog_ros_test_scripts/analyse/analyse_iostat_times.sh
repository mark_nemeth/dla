#!/bin/sh

inf=$1

sed '1,$s/,/\./g' | awk 'BEGIN {    tot = totlow = tothi = klow = khi = 0; dev0 = dev1 = 0;
                prevfl = flag = 0; tt = 0.0; ave = avelow = avehi = 0.0; TF=1.0; }
    /nvme0n1/{ wawait = $12; if( wawait > 0.15 ) dev0++; }
    /nvme1n1/{ wawait = $12; if( wawait > 0.15 ) dev1++; }
    /md127/{wmb = $7; ave += wmb; tot++;
                if( wmb < 3300.0) { flag = 0; klow++; totlow++; avelow += wmb; }
                else           { flag = 1; khi++; tothi++; avehi += wmb; }
                if( prevfl != flag ) {
                        if ( flag == 1 ) {
                                if( klow > 0 ) avelow /= klow;
                                print "low: ",klow," ave ",avelow," time ",klow * TF " sec"," w_await nvme0 e1 ",dev0,dev1;
                                tt += (klow * TF);
                                klow = 0; avelow = 0.0;
                        }
                        if ( flag == 0 ) {
                                if( khi > 0 ) avehi /= khi;
                                print " hi: ",khi," ave ",avehi," time ",khi * TF " sec"," w_await nvme0 e1 ",dev0,dev1;
                                tt += (khi * TF);
                                khi = 0; avehi = 0.0;
                        }
                        prevfl = flag;
                        dev0 = dev1 = 0;
                }
        }
    END { print "tot: ",tot," low ",totlow," hi ",tothi," ave ",ave/tot," tot time ", tt; }'

