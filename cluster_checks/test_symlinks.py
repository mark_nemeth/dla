import os

from test_abstract import *

class FindSymlinks(AbstractTest):

    def run(self):
        for component in [d for d in os.listdir(self.path) if not os.path.isfile(os.path.join(self.path, d))]:
            for vin in [d for d in os.listdir(os.path.join(self.path, component)) if not os.path.isfile(os.path.join(os.path.join(self.path, component), d))]:
                if vin != "byName" and vin != "byVehicleID":
                    current_path = os.path.join(self.path, component, vin)
                    for i in os.listdir(current_path):
                        if os.path.islink(i):
                            print(i)
