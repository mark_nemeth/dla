__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from .handlers.deletion_handler import DeletionHandler
from .handlers.update_handler import UpdateHandler
from DANFUtils.logging import logger
from config.constants import DANF_VERSION_TAG

def main():
    deletion_handler = DeletionHandler()
    update_handler = UpdateHandler()
    failed = False

    try:
        deletion_handler.sync_deleted_bagfiles_to_azure()
    except Exception as e:
        logger.warning(f"Deleting metadata failed with exception {e}")
        failed = True

    try:
        update_handler.upsert_bagfiles_to_azure()
    except Exception as e:
        logger.warning(f"Syncing metadata failed with exception {e}")
        failed = True

    if failed:
        raise RuntimeError("Could not fully sync metadata to Azure")


if __name__ == '__main__':
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    main()
