import React, { useState } from 'react';
import ScrollList from '../Utils/ScrollList/ScrollList';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import { timestampConverter } from '../../utils/valueConverter';
import { isObjectValid, isStringValid } from '../../utils/helperFunctions';
import ModalBox from '../Utils/ModalBox/ModalBox';
import './ProcessStatus.scss'

export const StatusTypes = Object.freeze({
  SUCCESS: 'SUCCESS',
  FAILED: 'FAILED',
  RUNNING: 'RUNNING',
  SKIPPED: 'SKIPPED',
  PENDING: 'PENDING',
  FLOWRUN_SUCCESSFUL: 'SUCCESSFUL'
});

export default function ProcessStatus(props) {

  const [modalShow, setModalShow] = useState(false);
  const [modalInfo, setModalInfo] = useState({ header: '', body: '' });

  if (!isObjectValid(props.state))
    return <div className="process-state-empty-container">Currently no process status available</div>;

  const state = hasOldStateModel(props.state) ? props.state : transformState(props.state);

  function hasOldStateModel(state) {
    // Old state is an array and has no guid which represent a flow run
    return Array.isArray(state) && !state[0].guid;
  }

  function transformState(state) {
    if (!Array.isArray(state) || !state.length) return [];

    // new flowRun data model
    let transformedState = [];
    let status = state[0].status === StatusTypes.FLOWRUN_SUCCESSFUL ? StatusTypes.SUCCESS : StatusTypes.FAILED;
    let steps = state[0].steps;
    for (let step of steps) {
      let runs = step.runs.slice();
      runs.sort((a, b) => a.completed_at - b.completed_at);
      if (runs[runs.length - 1].status !== StatusTypes.FLOWRUN_SUCCESSFUL) {
        // overwrite status, if most current run failed
        status = StatusTypes.FAILED;
      }
      const time = runs[runs.length - 1].completed_at;
      let report = {};
      for (let run of runs) {
        if (run.status !== StatusTypes.FLOWRUN_SUCCESSFUL) {
          report = run.report;
        }
      }

      // put everything into new data model
      const transformedStateEl = {
        name: step.name,
        status: status,
        version: step.tag,
        time: time,
        report: report
      };
      transformedState.push(transformedStateEl);
    }
    return transformedState;
  }

  function getIcon(status) {
    switch (status) {
      case StatusTypes.SUCCESS:
        return 'icon-check-1 success';
      case StatusTypes.FAILED:
        return 'icon-close failure';
      case StatusTypes.SKIPPED:
        return 'icon-alert-diamond skip';
      case StatusTypes.PENDING:
        return 'icon-navigation-menu-horizontal pending';
      case StatusTypes.RUNNING:
        return 'icon-hourglass';
      default:
        let error = { message: "Can not find item for status " + status }
        throw error
    }
  }

  function runInfoFormatter(run) {
    return `${run.name} v.${run.version} ${timestampConverter(run.time)}`;
  }

  const runListComponent = (runItem) => (
    <div className="process-state-body-item">
      <div className={"process-state-icon " + getIcon(runItem.status)} />
      <OverlayTrigger placement="right" delay={{ show: 400, hide: 0 }} outOfBoundaries overlay={
        <Tooltip className="process-state-info-tooltip">{runInfoFormatter(runItem)}</Tooltip>
      }>
        <div className="process-state-info">{runInfoFormatter(runItem)}</div>
      </OverlayTrigger>
      {isStringValid(runItem.report.message) === true &&
        <div className="process-state-detail" onClick={() => {
          setModalInfo({ header: runInfoFormatter(runItem), body: runItem.report.message });
          setModalShow(true);
        }}> Details </div> 
        }
    </div>
  );

  return (
    <>
    <ScrollList {...props}
      listHeight={state.length === 0 ? '28px' : (state.length < 3 ? '150px' : '250px')}
      showLoading={false}
      itemList={state}
      itemVariant="process-state-body-item-style"
      listItemComponent={runListComponent}
    />
    <ModalBox
        header={modalInfo.header} body={modalInfo.body}
        show={modalShow} onHide={() => setModalShow(false)}
      />
      </>
  );
}