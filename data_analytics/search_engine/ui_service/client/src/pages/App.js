import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import {store} from '../redux/Store';
import Header from '../components/Header';
import HomePage from './HomePage';
import OverviewPage from './OverviewPage';
import Announcements from './Announcements/Announcements';
import SupportPage from './Support';
import ErrorPage from './ErrorPage/ErrorPage';
import ReportingPage from './ReportingPage/';
import IngestPage from "./IngestPage";
import './App.scss';


function App() {
  return (
    <Provider store={store}>
      <Router>
        <Header />
        <div className="app-content">
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/ingest" component={IngestPage} />
            <Route path="/reporting" component={ReportingPage} />
            <Route path="/announcements" component={Announcements} />
            <Route path="/support" component={SupportPage} />
            <Route path="/originalfile/:guid" component={OverviewPage} />
            <Route path="/alwaysonfile/:guid" component={OverviewPage} />
            <Route path="/childbagfile/:guid" component={OverviewPage} />
            <Route path="/drive/:guid" component={OverviewPage} />
            <Route component={ErrorPage} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
