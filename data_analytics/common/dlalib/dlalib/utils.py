# -*- coding: utf-8 -*-
__copyright__ = '''
COPYRIGHT: (c) 2017-2018 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

"""Module provides a set of general purpose helper functions."""


from pyspark.sql.functions import udf
from pyspark.sql.types import TimestampType


def timestamp_from_s_and_ns_udf(epoch=datetime.datetime(1970, 1, 1)):
    """Return function object for converting timestamp based on seconds since reference date and ns since 
    previous second into spark timestamp of type TimestampType. 

    Returned function object f(s, ns, epoch) expects s, ns and epoch (default: 01.01.1970) as parameters.
    """
    def _timestamp_from_s_and_ns(s, ns, epoch):
        micro = ns // 1000
        # Spark TimestampType has microsecond precision (nothing more precise available)
        time = epoch + datetime.timedelta(seconds=int(s)) + datetime.timedelta(microseconds=micro)
        return time
    return udf(lambda s, ns: _timestamp_from_s_and_ns(s, ns, epoch), TimestampType())
