

export function getTransferRequestId(fileGuid, targetCluster) {
    return targetCluster + "_" + fileGuid;
}

export function transferRequestIsOngoing(state, fileGuid, targetCluster) {
    const requestId = getTransferRequestId(fileGuid, targetCluster);
    let requestIsOngoing = false;

    let foundRequest = getTransferRequestById(state, requestId);
    if (foundRequest) {
        requestIsOngoing = foundRequest.ongoing;
    }

    return requestIsOngoing;
}

export function getCurrentTransferRequestStatus(state, fileGuid, targetCluster) {
    const requestId = getTransferRequestId(fileGuid, targetCluster);

    let foundRequest = getTransferRequestById(state, requestId);

    if (foundRequest) return foundRequest;
    return { requestId: requestId, ongoing: false };
}

export function getTransferRequestById(state, requestId) {

    let foundRequest = null;
    state.httpRequests.requestFileTransferRequests.ongoing.forEach(request => {
        if (request.requestId === requestId) {
            foundRequest = request;
        }
    });
    return foundRequest;
}
