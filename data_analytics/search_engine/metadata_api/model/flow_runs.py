__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List, Optional, Dict, Any

from pydantic import BaseModel, constr

from model.types import UnixTimestampMilliseconds, Guid


class FlowRunStepRun(BaseModel):
    started_at: UnixTimestampMilliseconds
    completed_at: UnixTimestampMilliseconds
    status: constr(regex='SKIPPED|SUCCESSFUL|FAILED')
    report: Dict[str, Any]


class FlowRunStep(BaseModel):
    name: str
    image: str
    tag: str
    max_retries: int
    is_essential: Optional[bool]
    status: constr(regex='PENDING|SKIPPED|NOT_EXECUTED|SUCCESSFUL|FAILED')
    runs: List[FlowRunStepRun] = []


class Reference(BaseModel):
    type: constr(regex='bagfile_guid|childbagfile_guid')
    value: str


class FlowRun(BaseModel):
    version: str = "2.0"
    guid: Guid
    kubeflow_workflow_name: Optional[str]
    kubeflow_workflow_id: Optional[Guid]
    started_at: UnixTimestampMilliseconds
    completed_at: Optional[UnixTimestampMilliseconds]
    status: constr(regex='PENDING|IN_PROGRESS|SUCCESSFUL|FAILED')
    reference: Reference
    flow_name: str
    flow_version: str
    steps: List[FlowRunStep]


class CreateFlowRunRequest(BaseModel):

    class Step(BaseModel):
        name: str
        image: str
        tag: str
        max_retries: int
        is_essential: bool = True

    reference: Reference
    flow_name: str
    flow_version: str
    steps: List[Step]


class UpdateFlowRunRequest(BaseModel):
    status: Optional[constr(regex='IN_PROGRESS')]
    kubeflow_workflow_name: Optional[str]
    kubeflow_workflow_id: Optional[str]
    completed_at: Optional[UnixTimestampMilliseconds]
