import config from './config'

class ConfigHandler {

  constructor() {
    this.config = config[process.env.NODE_ENV || 'production']
  }

  getBagfileAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_BAGFILE_API || this.config.bagfileApi,
      params: query
    };
  }

  getDriveAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_DRIVE_FILE_API || this.config.driveApi,
      params: query
    };
  }

  getChildBagfileAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_CHILD_BAGFILE_API || this.config.childBagfileApi,
      params: query
    };
  }

  getBagfileFiltersAPI() {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_BAGFILE_FILTERS_API || this.config.bagfileFiltersApi
    };
  }

  getDriveFiltersAPI() {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_DRIVE_FILE_FILTERS_API || this.config.driveFiltersApi
    };
  }

  getOriginalFileFiltersAPI() {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_ORIGINAL_FILE_FILTERS_API || this.config.originalFileFiltersApi
    };
  }

  getAlwaysonFileFiltersAPI() {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_ALWAYSON_FILE_FILTERS_API || this.config.alwaysonFileFiltersApi
    };
  }

  getChildBagfileFiltersAPI() {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_CHILD_BAGFILE_FILTERS_API || this.config.childBagfileFiltersApi
    };
  }

  getReportingKpiAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_REPORTING_KPI_API || this.config.reportingKpiAPI,
      params: query
    };
  }

  getFilterFeaturesAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_FILTER_FEATURES_API || this.config.filterFeaturesApi,
      params: query
    };
  }

  getReportingTablesSimpleAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_REPORTING_TABLES_SIMPLE_API || this.config.reportingTablesSimpleAPI,
      params: query
    };
  }

  getReportingTablesAggregationAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_REPORTING_TABLES_AGG_API || this.config.reportingTablesAggregationAPI,
      params: query
    };
  }

  getFlowRunsAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_FLOW_RUNS_API || this.config.flowRunsApi,
      params: query
    };
  }

  getLocationAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_LOCATIONS_API || this.config.locationsApi,
      params: query
    }
  }

  getLeafLetMapApiKey() {
    return {
      ApiKey: process.env.REACT_APP_LEAFLETMAP_API_KEY || this.config.leafLetMapApiKey
    }
  }

  getUserDataAPI() {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_USER_DATA_API || this.config.userDataApi
    };
  }

  getReprocessAPI(bodyData) {
    return {
      method: 'post',
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_REPROCESS_API || this.config.reprocessApi,
      data: {'file_paths': [bodyData]},
    };
  }

  getDataTransferAPI(query) {
    return {
      method: 'post',
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_DATA_TRANSFER_API || this.config.dataTransferApi,
      params: query
    };
  }

  getLatestIngestsAPI() {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_LATEST_INGEST_PER_CAR_API || this.config.latestIngestsApi
    };
  }

  getIngestAPI(query) {
    return {
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_INGEST_API || this.config.ingestApi,
      params: query
    };
  }

  getVersionAPI() {
    return {
      method: 'get',
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_VERSION_API || this.config.versionApi,
    }
  }

  getFeatureToggleAPI() {
    return {
      method: 'get',
      baseURL: process.env.REACT_APP_NODE_HOST || this.config.host,
      url: process.env.REACT_APP_NODE_FEATURE_TOGGLE_API || this.config.featureToggleApi,
    }
  }

  getJiraIssueSearchUrl(jqlQuery) {
    return (process.env.REACT_APP_NODE_JIRA_ISSUE_SEARCH_BASE_URL || this.config.jiraIssueSearchBaseUrl) + encodeURI(jqlQuery);
  }
}

const configHandler = new ConfigHandler();
export default configHandler;