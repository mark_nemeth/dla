__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.logging import logger
from DBConnector.exceptions import BulkOperationException, \
    IndexOverwriteException
from DBConnector.utils import calculate_request_size
from pymongo import UpdateOne
from pymongo.errors import BulkWriteError


class AccountCollectionHandler:

    # Account index
    COLLECTION_NAME = 'accounts'
    INDEX_NAME = 'accounts_guid_index'
    INDEX_FIELD_GUID = 'guid'

    def __init__(self, db):
        self.db = db
        self.col = self.db[self.COLLECTION_NAME]
        self.col.create_index(self.INDEX_FIELD_GUID,
                              unique=True,
                              name=self.INDEX_NAME)
        self.col.create_index('key_hash',
                              unique=True,
                              name='key_hash_index')

    def add(self, accounts):
        assert isinstance(accounts, list), \
            'Data inserted into db has to be a list of dicts'
        assert all(isinstance(x, dict) for x in accounts), \
            'Data inserted into db has to be a list of dicts'

        num_docs = len(accounts)
        request_size = calculate_request_size(accounts)
        logger.debug(f"Adding {num_docs} documents to collection "
                     f"'{self.COLLECTION_NAME}' "
                     f"(Request size: {request_size:.2f} kb)")

        try:
            self.col.insert_many(accounts)
        except BulkWriteError as e:
            errors = '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            msg = (f"An error occurred while performing "
                   f"a bulk operation (bulk_write). Details:\n{errors}")
            logger.exception(msg)
            raise BulkOperationException(msg)

        for x in accounts:
            del x['_id']

    def update(self, guids, field_updates, create_if_missing):
        for update in field_updates:
            if self.INDEX_FIELD_GUID in update.keys():
                msg = 'Overwriting index fields is not permitted!'
                logger.error(msg)
                raise IndexOverwriteException(msg)

        update_operations = [
            {"$set": {**update}} for update in field_updates
        ]
        self._batch_update_by_guid(guids, update_operations, create_if_missing)

    def search(self, query, limit, offset, sortby):
        assert isinstance(query, dict), 'Expected dict query'
        assert isinstance(limit, int), 'Expected int limit'
        assert isinstance(offset, int), 'Expected int offset'
        assert isinstance(sortby, (str, list)), 'Expected str or list sortby'

        request = query
        request_size = calculate_request_size(request)
        logger.debug(f"Searching documents in collection "
                     f"'{self.COLLECTION_NAME}' "
                     f"(Request size: {request_size:.2f} kb)")

        cursor = self.col.find(
            request,
            {'_id': 0}
        ).sort(sortby)
        if offset > 0:
            cursor = cursor.skip(offset)
        if limit > 0:
            cursor = cursor.limit(limit)
        return cursor

    def _batch_update_by_guid(self,
                              guids,
                              update_operations,
                              create_if_missing=False):
        """Performs batch updates on multiple documents. update_operations[i]
        is performed on the document identified by guids[i]. Therefore,
        update_operations and guids should be lists of the same size.

        :param guids: List of guids identifying the documents to update
        :param update_operations: List of update operations dicts. Each element
            must be a valid mongo db update operator
            (see https://docs.mongodb.com/manual/reference/operator/update/)
        :param create_if_missing: bool that maps to mongoDB upsert param
        """
        assert isinstance(guids, list), \
            'Expected a list of guid strings.'
        assert all(isinstance(x, str) for x in guids), \
            'Expected a list of update guid strings.'
        assert isinstance(update_operations, list), \
            'Expected a list of update_operations dicts.'
        assert all(isinstance(x, dict) for x in update_operations), \
            'Expected a list of update_operations dicts.'
        assert isinstance(create_if_missing, bool), \
            'create_if_missing has to be of type bool'
        assert len(guids) == len(update_operations), \
            'List guids and list update_operations should be of same length'

        requests = []
        for guid, op in zip(guids, update_operations):
            # Add GUID to dict and create update request
            requests.append(
                UpdateOne(filter={self.INDEX_FIELD_GUID: guid},
                          update=op,
                          upsert=create_if_missing))

        logger.debug(f"Updating {len(guids)} documents in "
                     f"collection '{self.COLLECTION_NAME}'")
        try:
            self.col.bulk_write(requests)
        except BulkWriteError as e:
            errors = '\n'.join(
                str(msg['errmsg']) for msg in e.details['writeErrors'])
            msg = (f"An error occurred while performing "
                   f"a bulk operation (bulk_write). Details:\n{errors}")
            logger.exception(msg)
            raise BulkOperationException(msg)
