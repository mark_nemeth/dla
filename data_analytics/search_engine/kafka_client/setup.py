"""Setup file"""
from setuptools import setup, find_packages

setup(name='KafkaClient',
      version='0.0.2',
      packages=find_packages(exclude=["tests"]),
      long_description=open('README.md', 'r').read(),
      long_description_content_type="text/markdown",
      test_suite="tests",
      install_requires=[
          'confluent-kafka==1.1.0', 'DANFUtils>=0.1.1'
      ])
