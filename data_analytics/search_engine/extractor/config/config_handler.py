"""Config Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import logging
import os
from datetime import datetime

logger = logging.getLogger(__name__)


class ConfigHandler:
    def __init__(self):
        """Read json config file for topics versioning
        """
        self.configs_obj = {}
        try:
            path = os.path.abspath(os.path.dirname(__file__))
            with open(f"{path}/version.json", "r") as f:
                self.configs_obj = json.load(f)
        except Exception:
            raise ValueError(
                "Config file for topics versioning not available, please provide one!"
            )

    @staticmethod
    def get_version_based_on_ts(ts, conf) -> str:
        """Get topics version based on the recording start time of the bagfile

        :param ts: recording start time of the bagfile
        :param conf: config object
        :return: topics version, e.g. v1
        """
        bagfile_datetime = datetime.fromtimestamp(ts/1000.)
        version = None
        for key, value in conf.items():
            if version is None:
                time_range_start = datetime.strptime(value['date_start'],
                                                     '%d-%m-%Y')
                time_range_end = datetime.strptime(value['date_end'],
                                                   '%d-%m-%Y')
                if (time_range_start <= bagfile_datetime <= time_range_end):
                    version = key
        return version

    @staticmethod
    def get_topics_for_version(conf, version) -> list:
        """Get topics based on version
        """
        return list(conf.get(version)['topics'].values())

    @staticmethod
    def get_topic_for_version(conf, topic, version) -> str:
        """Get a specific topic based on version
        """
        return conf.get(version)['topics'].get(topic)

    def get_relevant_topic_for_interpreter(self, interpreter_name, start_ts, topics_list) -> list:
        """Get the relevant topic for a given interpreter and checks if the required topic is available in the bagfile

        :param interpreter_name
        :param start_ts: recording start time of the bagfile
        :param topics_list: list of available topics in the bagfile
        :return: Relevant topic for the given interpreter
        """
        conf = self.configs_obj[interpreter_name]
        version = self.get_version_based_on_ts(start_ts, conf)
        logger.info(f"Set {interpreter_name} version to: {version}")

        topics = self.get_topics_for_version(conf, version)
        logger.info(f"Set {interpreter_name} topics to: {topics}")

        for topic in topics:
            if topic not in topics_list:
                logger.info(
                    f"{interpreter_name} topic {topic} is not available in the bagfile.")
                topics.remove(topic)
        return topics

    def get_config_for_disengagement(self, start_ts, topics_list) -> list:
        """Special config handler for disengagement interpreter,
        since it returns multiple topics and the engagement handler

        :param start_ts: recording start time of the bagfile
        :param topics_list: list of available topics in the bagfile
        :return: List of topics and engagement handler
        """
        disengagement_conf = self.configs_obj['disengagement']
        version = self.get_version_based_on_ts(start_ts, disengagement_conf)
        logger.info(f"Set disengagement version to: {version}")

        try:
            logger.info(f"Set disengagement topics to: {disengagement_conf.get(version)['topics']}")
            logger.info(f"Set engagement handler to: {disengagement_conf.get(version)['engagement_handler']}")

            engagement_state_topic = self.get_topic_for_version(
                disengagement_conf, "engagement_state_topic", version)
            log_request_topic = self.get_topic_for_version(
                disengagement_conf, "log_request_topic", version)

            # addtional check for v2 and v3:
            # check if the renamed topic is really in the topic list, if not, switch from v2 to v1 / from v3 to v2
            # since new sw release could be deployed in different vehicles
            if version in ["v2", "v3"]:
                if engagement_state_topic not in topics_list:
                    if version == "v2":
                        version = "v1"
                    else:
                        version = "v2"
                    logger.warning(
                        "topics not available in the bagfile, set version to previous version instead."
                    )

                    engagement_state_topic = disengagement_conf.get(
                        version)['topics'].get("engagement_state_topic")
                    log_request_topic = disengagement_conf.get(
                        version)['topics'].get("log_request_topic")

                    logger.info(f"Set disengagement topics to: {disengagement_conf.get(version)['topics']}")

            engagement_handler = disengagement_conf.get(version)['engagement_handler']
        except Exception as exp:
            raise ValueError("Topics and handler cannot be set!", exp)

        return engagement_state_topic, log_request_topic, engagement_handler
