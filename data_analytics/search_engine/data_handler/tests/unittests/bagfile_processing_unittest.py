import os
import unittest

from ContainerClient.container_client import ContainerClient
from DANFUtils.utils import generate_bag_guid
from handlers.file_reprocessing import FileReprocessingHandler
from proxies.metadata_api import MetadataApiProxy
from proxies.search_api import SearchApiProxy

# TODO: Update paths pointing to existing files before running tests
RAW_BAG_PATH = '<PATH_TO_EXISTING_FILE>'
UPDATED_BAG_PATH = '<PATH_TO_EXISTING_FILE>'
TEST_GUID = 'c52adb2f-bcbe-4554-815b-d3573fd77b14'


class BagfileProcessingUnittest(unittest.TestCase):
    def setUp(self):
        # Requirement - we should run metadata and search API locally
        os.environ['METADATA_API_BASE_URL'] = 'http://localhost:8887/'
        os.environ['SEARCH_API_BASE_URL'] = 'http://localhost:8889/'
        os.environ['DANF_ENV'] = 'local'
        self.clear_db()

    def tearDown(self):
        self.clear_db()

    @staticmethod
    def clear_db():
        search_proxy = SearchApiProxy()
        metadata_proxy = MetadataApiProxy()
        for path in [RAW_BAG_PATH]:
            guid = generate_bag_guid(path)
            bag = search_proxy.get_bagfile(guid)
            soft_deleted_bag = search_proxy.get_bagfile(guid, is_deleted=True)
            if not bag and not soft_deleted_bag:
                continue
            metadata_proxy.delete_bagfile_and_orphans(guid, hard_delete=True)

    def test_bagfile_soft_deletion(self):
        metadata_proxy = MetadataApiProxy()
        search_proxy = SearchApiProxy()

        bag_guid = self.insert_test_bagfile_to_db(metadata_proxy)
        bag = search_proxy.get_bagfile(bag_guid)
        deleted_bag = dict(bag)
        deleted_bag['is_deleted'] = True

        # Soft delete
        metadata_proxy.delete_bagfile_and_orphans(guid=bag_guid)
        actual_deleted_bag = search_proxy.get_bagfile(bag_guid, is_deleted=True)

        self.assertDictEqual(actual_deleted_bag, deleted_bag)

    def test_bagfile_flagging_for_reprocessing(self):
        metadata_proxy = MetadataApiProxy()
        search_proxy = SearchApiProxy()

        bag_guid = self.insert_test_bagfile_to_db(metadata_proxy)
        bag = search_proxy.get_bagfile(bag_guid)

        expected_result = [{'file_path': RAW_BAG_PATH,
                            'bagfile_guid': bag_guid,
                            'result': 'SUCCESSFUL'}]

        reprocessing_handler = FileReprocessingHandler()
        result = reprocessing_handler.reprocess_all([RAW_BAG_PATH])

        self.assertEqual(result, expected_result)

        expected_flagged_bag = dict(bag)
        expected_flagged_bag['is_deleted'] = True
        flagged_bag = search_proxy.get_bagfile(bag_guid, is_deleted=True)
        self.assertDictEqual(flagged_bag, expected_flagged_bag)

    def test_hide_retriggered_bagfiles(self):
        metadata_proxy = MetadataApiProxy()
        cc = ContainerClient()

        self.insert_test_bagfile_to_db(metadata_proxy, RAW_BAG_PATH)
        assert RAW_BAG_PATH in cc.get_all_bagfile_links()
        reprocessing_handler = FileReprocessingHandler()
        reprocessing_handler.reprocess_all([RAW_BAG_PATH])

        all_links = cc.get_all_bagfile_links()
        self.assertNotIn(RAW_BAG_PATH, all_links)

    def test_update_retriggered_bagfile(self):
        metadata_proxy = MetadataApiProxy()
        search_proxy = SearchApiProxy()

        # Insert
        guid = self.insert_test_bagfile_to_db(metadata_proxy, RAW_BAG_PATH)

        # Move bag - update current link
        cc = ContainerClient()
        cc.update_bagfile(guid, {'current_link': UPDATED_BAG_PATH})

        # Soft delete
        reprocessing_handler = FileReprocessingHandler()
        reprocessing_handler.reprocess_all([UPDATED_BAG_PATH])
        assert search_proxy.get_bagfile(guid, is_deleted=True) is not None

        # Create / Update
        metadata_proxy = MetadataApiProxy()
        metadata_proxy.upsert_bagfile(
            file_guid=TEST_GUID,
            file_path=UPDATED_BAG_PATH,
            file_name='updated.bag',
            file_type='ALWAYSON',
            drive_types=['updated_types'],
            processing_state='BEING_PROCESSED',
            processing_priority=1
        )
        expected_updated_bag = {
            'file_guid': TEST_GUID,
            'guid': guid,
            'version': metadata_proxy.BAGFILE_DATA_VERSION,
            'link': RAW_BAG_PATH,
            'current_link': UPDATED_BAG_PATH,
            'file_name': 'updated.bag',
            'file_type': 'ALWAYSON',
            'drive_types': ['updated_types'],
            'processing_state': 'BEING_PROCESSED',
            'processing_priority': 1,
            'state': [],
            'is_deleted': False
        }
        updated_bag = search_proxy.get_bagfile(guid)
        self.assertDictEqual(updated_bag, expected_updated_bag)

    @staticmethod
    def insert_test_bagfile_to_db(metadata_proxy, path=RAW_BAG_PATH):
        bag_guid = metadata_proxy.upsert_bagfile(
            file_path=path,
            file_name='20200311_213450_qg3_smoke_test_one_button_start_0_ORIGINAL.bag',
            file_type='ALWAYSON',
            file_tag=['test'],
            drive_types=[],
            processing_state='SUCCESSFUL',
            processing_priority=0
        )
        return bag_guid
