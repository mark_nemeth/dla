"""Basic crud operations."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import List, Callable, Optional, Tuple, Dict, Any, Union

from motor.motor_asyncio import AsyncIOMotorCollection, AsyncIOMotorCursor
from pymongo import ASCENDING, DESCENDING

from ..core.errors import RHSColonOperatorError
from ..models.base import OpValue, QueryParams, FilterParams

EQ_OP_MAP = {"": "$eq", "eq": "$eq"}
INTERVAL_OP_MAP = {"gt": "$gt", "gte": "$gte", "lt": "$lt", "lte": "$lte"}
REGEX_OP_MAP = {"re": "$regex"}


async def get_record(
    collection: AsyncIOMotorCollection,
    key: str,
    value: Any,
    default_projection: Optional[List[str]],
    projection_modifiers: Optional[List[OpValue]],
) -> dict:
    """Get a document from the given collection by a unique key value pair.

    Args:
        collection: db collection to query
        key: name of the field used as a key
        value: value of the key for getting the record
        default_projection: list of fields to be returned by default
        projection_modifiers: setting to modify the default fields

    Returns:
        the matched record

    Raises:
        KeyError: if no record is found with the specified key value pair
    """
    record = await collection.find_one(
        filter={key: value},
        projection=construct_projection(defaults=default_projection, modifiers=projection_modifiers),
    )
    if record:
        return record
    raise KeyError(f"No record found with {key}: {value}")


async def count_records(
    collection: AsyncIOMotorCollection,
    filter_constructor: Callable[[FilterParams], dict],
    filter_params: FilterParams,
) -> int:
    """Count documents in a given collection matching the criteria.

    Args:
        collection: db collection to query
        filter_constructor: callable that will create the filter object from the filter params
        filter_params: object holding filter settings

    Returns:
        the number of matching documents
    """
    return int(await collection.count_documents(filter_constructor(filter_params)))


def find_records(
    collection: AsyncIOMotorCollection,
    filter_constructor: Callable[[FilterParams], dict],
    default_projection: Optional[List[str]],
    query_params: QueryParams,
) -> AsyncIOMotorCursor:
    """Find documents in a given collection matching the criteria.

    Args:
        collection: db collection to query
        filter_constructor: callable that will create the filter object from the filter params
        default_projection: list of fields to be returned by default
        query_params: object holding query settings

    Returns:
        an async iterable producing matching documents
    """
    return collection.find(
        filter=filter_constructor(query_params.filter_params),
        projection=construct_projection(defaults=default_projection, modifiers=query_params.fields_),
        sort=construct_sorting(query_params.order_by, list_format=True),
        limit=query_params.limit,
        skip=query_params.offset,
    )


def construct_projection(defaults: Optional[List[str]], modifiers: Optional[List[OpValue]]) -> Optional[dict]:
    """Construct the projection object for the db query.

    If any of the modifiers is specified without an operator, then the defaults are ignored
    and modifiers are considered on their own as the absolute specification of the fields.
    Otherwise the modifiers are considered relative to the defaults.

    Args:
        defaults: list of fields to be returned by default
        modifiers: list of field inclusion and exclusion settings or explicit selection of fields

    Returns:
        dict to be applied for projecting a db query or None if the dict would be empty

    Raises:
        RHSColonOperatorError: if a projection operator is invalid
    """
    defaults = defaults or []
    modifiers = modifiers or []

    absolute_modifiers = any(m.op == "" for m in modifiers)
    projection = {} if absolute_modifiers else {field: True for field in defaults}

    operator_mapping = {"inc": True, "exc": False, "": True}
    try:
        projection.update({m.value: operator_mapping[m.op.lower()] for m in modifiers})
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="projection", valid_op=list(operator_mapping))

    # Make sure that the projection is a valid mongo projection
    # With the exception of the _id field, inclusion and exclusion statements cannot be combined in a projection
    if absolute_modifiers or defaults:
        # Drop the False fields / keep the True fields only
        # Results a projection that explicitly defines what fields should be returned
        projection = {field: include for field, include in projection.items() if include}
    else:  # no defaults is specified (default is everything) and the modifier is relative
        # Drop the True fields / keep the False fields only
        # Results a projection that defines what fields should be returned relative to the whole document
        # by excluding certain fields from it
        projection = {field: include for field, include in projection.items() if not include}

    return projection or None


def construct_sorting(
    order_by: List[OpValue], list_format: bool = False
) -> Union[Dict[str, int], List[Tuple[str, int]]]:
    """Construct the sorting object for the db query.

    Args:
        order_by: ordering info
        list_format: construct the filter in list of tuples format

    Returns:
        dict or list to be applied for sorting a db query

    Raises:
        RHSColonOperatorError: if an ordering operator is invalid
    """
    operator_mapping = {"asc": ASCENDING, "desc": DESCENDING, "": DESCENDING}
    try:
        if list_format:
            return [(by.value, operator_mapping[by.op.lower()]) for by in order_by]
        return {by.value: operator_mapping[by.op.lower()] for by in order_by}
    except KeyError as e:
        raise RHSColonOperatorError(op=e.args[0], context="sorting", valid_op=list(operator_mapping))
