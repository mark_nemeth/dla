#!/bin/sh
#This script writes the bandwith measured with the bandwidth monitor to a file
#csv output format:
#Type rate:
#unix timestamp;iface_name;bytes_out/s;bytes_in/s;bytes_total/s;bytes_in;bytes_out;packets_out/s;packets_in/s;packets_total/s;packets_in;packets_out;errors_out/s;errors_in/s;errors_in;errors_out\n
#Type svg, sum, max:
#unix timestamp;iface_name;bytes_out;bytes_in;bytes_total;packets_out;packets_in;packets_total;errors_out;errors_in\n
#Use --count 0 to skip the all zero output after start.
T=$1
INTERFACE=$2
FILENAME="bwm_${INTERFACE}_$T.log"
RATE=1000


bwm-ng -I "$INTERFACE" -t "$RATE" -o csv -F "$FILENAME"
