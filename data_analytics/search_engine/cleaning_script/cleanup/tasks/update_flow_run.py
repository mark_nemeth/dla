__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os

from ContainerClient.container_client import ContainerClient
from DANFUtils.exceptions import RESTException
from DANFUtils.utils import utils

logger = logging.getLogger(__name__)


class UpdateFlowRunTask:

    def __init__(self):
        self.client = ContainerClient()

    def execute(self):
        """Updates the flow run document associated with this step to
        indicate that the run is terminated.
        """
        logger.info("Executing update flow run")

        # required env variables
        run_guid = os.getenv('FLOW_RUN_GUID')
        bagfile_guid = os.getenv('BAGFILE_GUID')
        if not run_guid:
            raise ValueError("Update flow run failed. "
                             "Required env var FLOW_RUN_GUID is missing")
        if not bagfile_guid:
            raise ValueError("Update flow run failed. "
                             "Required env var BAGFILE_GUID is missing")

        # optional env variables
        workflow_name = self._get_wf_name_from_pod_name(os.getenv("POD_NAME"))

        try:
            self.client.update_flow_run(
                run_guid,
                {
                    'completed_at': utils.get_timestamp(),
                    'kubeflow_workflow_name': workflow_name or 'unknown'
                }
            )
        except (RESTException, AssertionError) as e:
            # fail task in order to fail step
            raise RuntimeError(f"Update flow run failed. "
                               f"Flow run guid: {run_guid}") from e

        try:
            run = self.client.get_flow_run(run_guid)
        except (RESTException, AssertionError) as e:
            # fail task in order to fail step
            raise RuntimeError(f"Retrieving flow run failed. "
                               f"Flow run guid: {run_guid}") from e

        # update processing state of processed file
        try:
            ref = run['reference']
            if ref['type'] == 'bagfile_guid':
                self.client.update_bagfile(
                    ref['value'], {'processing_state': run['status']})
            elif ref['type'] == 'childbagfile_guid':
                self.client.update_childbagfile(
                    ref['value'], {'processing_state': run['status']})
            else:
                logger.warning(f"Unexpected reference in flow run: {run}")
        except (RESTException, AssertionError) as e:
            # fail task in order to fail step
            raise RuntimeError(f"Update processing state failed. "
                               f"Bagfile guid: {bagfile_guid}") from e

        logger.info(f"Update flow run successful state successful. "
                    f"Flow run guid: {run_guid}. Final flow state: {run}")

    @staticmethod
    def _get_wf_name_from_pod_name(pod_name):
        return pod_name.rsplit('-', 1)[0] if pod_name else None

