#!/bin/bash
set -e
########GLOBALS##################
current_dir=$(cd "$(dirname "$0")" && pwd)
dir_one_up=${current_dir%/*} #dla_repo

display_help() {
    echo "Usage: $0 [option...]" >&2
    echo
    echo "   -d                 Create diffs on all *.py files related to the code check guidelines defined in the .style.yapf file"
    echo "   -r                 Replace all *.py files with formatted ones related to the code check guidelines defined in the .style.yapf file"
    echo "   -s [file_path]     Create a single diff on a single file [path] related to the code check guidelines defined in the .style.yapf file"
    echo "   -c                 Cleanup all folders....  delete .diff files and zero byte files"
    echo "   -a [file_path]     Replace a single file with formatted one related to the code check guidelines defined in the .style.yapf file"
    echo
    exit 1
}

create_log() {
find . -name "codecheck.log" -delete
bash -c 'echo "#################This files should be adapted to the code style guidelines###################">>code_check/codecheck.log'
bash -c 'echo "#############################################################################################">>code_check/codecheck.log'

while IFS= read -r line
do
  if [ -s $line ]
    then
        echo "$line">>code_check/codecheck.log

  else
	    :
  fi
done < "$1"

find . -name "codecheck_tmp.log" -delete
#find . -type f -empty -delete
find . -name "*.diff" -empty -delete
}


while getopts drs:ca:h option
do
case "${option}"
in
d)
   cd $dir_one_up
   find . -iname '*.py' -exec bash -c 'yapf -d --style code_check/.style.yapf {} > {}.diff |       #create diffs to the formatted style guidelines
   echo "{}.diff" >> code_check/codecheck_tmp.log'  \;
   create_log code_check/codecheck_tmp.log
   echo "Creating diff based on style guideline.... "                   ;;

r) cd $dir_one_up
   find . -iname '*.py' -exec bash -c 'yapf -i --style code_check/.style.yapf {}' \;
   echo "Replacing all *.py files with formatted version....";;                                  #replace files with formatted versions

s) yapf -d --style .style.yapf ${OPTARG} > ${OPTARG}.diff                            #create single diff on file to the formatted style guidelines
   echo "Create single diff...."                                    ;;
c) cd $dir_one_up
   find . -name "*.diff" -delete                                                     #cleanup.... deletes the *.diff files and empty files
   find . -name "codecheck.log" -delete
   echo "Cleanup... delete logfiles and diff files..."              ;;
a) yapf -i --style .style.yapf ${OPTARG}                                            #replace existing py file with formatted one.... require path of file
   echo "Replacing file with formatted version....."                    ;;
h) display_help ;;
esac
done