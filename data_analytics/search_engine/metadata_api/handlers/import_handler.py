__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.exceptions import GUIDNotFoundException

from handlers.db_data_handler import DB_Data_Handler


logger = logging.getLogger(__name__)


class ImportHandler:

    def __init__(self):
        self.db_data_handler = DB_Data_Handler()

    def upsert_metadata(self, metadata):
        self._upsert_bagfiles(metadata)
        # Sequences are deleted by the upsert_bagfiles function since update bagfiles will
        # delete the bagfile first which automatically cascades the deletion to all related sequences
        self._add_sequences(metadata)
        self._upsert_childbagfiles(metadata)
        self._upsert_extractor_requests(metadata)

    def _upsert_bagfiles(self, metadata):
        bagfile_list = metadata.get("bagfiles", None)
        if bagfile_list:
            bagfile_guid_list = [bagfile['guid'] for bagfile in bagfile_list]
            try:
                self.db_data_handler.delete_bagfiles(bagfile_guid_list)
            except GUIDNotFoundException as e:
                logger.info(f"No bagfiles found to update, will create bagfiles. {e}")
            self.db_data_handler.add_bagfiles(bagfile_list)

    def _upsert_childbagfiles(self, metadata):
        childbagfile_list = metadata.get("childbagfiles", None)
        if childbagfile_list:
            childbagfile_guid_list = [childbagfile['guid'] for childbagfile in childbagfile_list]
            try:
                self.db_data_handler.delete_child_bagfiles(childbagfile_guid_list)
            except GUIDNotFoundException as e:
                logger.info(f"No childbagfiles found to update, will create childbagfiles. {e}")
            self.db_data_handler.add_child_bagfiles(childbagfile_list)

    def _add_sequences(self, metadata):
        sequence_list = metadata.get("sequences", None)
        if sequence_list:
            self.db_data_handler.add_sequences(sequence_list)

    def _upsert_extractor_requests(self, metadata):
        extractor_request_list = metadata.get("extractor_requests", None)
        if extractor_request_list:
            extractor_request_guid_list = [extractor_request['guid'] for extractor_request in extractor_request_list]
            try:
                self.db_data_handler.delete_extractor_requests(extractor_request_guid_list)
            except GUIDNotFoundException as e:
                logger.info(f"No extractor_requests found to update, will create extractor_requests. {e}")
            self.db_data_handler.add_extractor_requests(extractor_request_list)
