import {connect} from 'react-redux';
import {filterByDuration, setCurrentDurationRange} from '../../../../redux/Home/HomeActions';
import {ByDurationFilter, STATIC_LIMITS} from './ByDurationFilter';
import {FileTypes} from '../ByFileFilter/ByFileFilter';
import {toMinutesConverter} from '../../../../utils/valueConverter';

function mapStateToProps(state, ownProps) {
  let limits;
  switch (state.fileType) {
    case FileTypes.ORIGINALFILE:
      limits = toMinutesConverter(state.filterOptions.originalFileDurationLimits ?
        {
          min: state.filterOptions.originalFileDurationLimits[0],
          max: state.filterOptions.originalFileDurationLimits[1]
        }
        : STATIC_LIMITS.ORIGINALFILE);
      break;
    case FileTypes.ALWAYSONFILE:
      limits = toMinutesConverter(state.filterOptions.alwaysonFileDurationLimits ?
          {
            min: state.filterOptions.alwaysonFileDurationLimits[0],
            max: state.filterOptions.alwaysonFileDurationLimits[1]
          }
          : STATIC_LIMITS.ALWAYSONFILE);
      break;
    case FileTypes.CHILDBAGFILE:
      limits = toMinutesConverter(state.filterOptions.childBagfileDurationLimits ?
          {
            min: state.filterOptions.childBagfileDurationLimits[0],
            max: state.filterOptions.childBagfileDurationLimits[1]
          }
          : STATIC_LIMITS.CHILD_BAGFILE);
      break;
    case FileTypes.DRIVE:
      limits = toMinutesConverter(state.filterOptions.driveDurationLimits ?
          {
            min: state.filterOptions.driveDurationLimits[0],
            max: state.filterOptions.driveDurationLimits[1]
          }
          : STATIC_LIMITS.DRIVE);
      break;
  }

  const duration = state.currentDurationRange || {
    'min': limits.min,
    'max': limits.max
  };
  const steps = state.fileType === FileTypes.CHILDBAGFILE ? 0.05 : 1.0;
  return {duration, limits, steps};
}

export default connect(
  mapStateToProps,
  {setCurrentDurationRange, filterByDuration}
)(ByDurationFilter);