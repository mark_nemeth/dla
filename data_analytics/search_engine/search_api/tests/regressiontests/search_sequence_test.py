__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import random as rd
import unittest

import numpy as np
import requests
from DANFUtils.utils import generate_deterministic_guid

from .testutils import get_testdb_connector

base_url = 'http://localhost:8889'
search_seq_url = base_url + '/search/sequence'

seq_1 = {
    "bagfile_guid": generate_deterministic_guid(),
    "index": 0,
    "version": "0.0.1",
    "extractors": {
        "ext1_3": {
            "bva": 0,
            "metadata": {
                "gps": [
                    {
                        "latitude": 0,
                        "longitude": 0,
                        "ts": 1565799669
                    }
                ]
            }
        },
    },
    "start": 1570786402000,
    "end": 1570786403000,
    "link": "testdata",
    "guid": generate_deterministic_guid()
}


def prepare_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['sequences'].delete_many({'link': 'testdata'})
        con.add_sequences([seq_1])


def cleanup_db():
    connector = get_testdb_connector()
    with connector as con:
        con.db['sequences'].delete_many({'link': 'testdata'})


class TestSearchSequencesGET(unittest.TestCase):

    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_search_by_bagfile_guid(self):
        query_data = {
            'bagfile_guid': seq_1['bagfile_guid']
        }
        res = requests.get(url=search_seq_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'Bagfile search by guid failed')
        self.assertCountEqual([seq_1], res_json,
                              'Bagfile search by guid failed')
