#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <libaio.h>
#include "ros/console.h"
#include "aiostream.h"

#define KB						(1024)
#define MB						(1024 * 1024)
#define GB						(1024llu * 1024llu * 1024llu)
#define CACHE_LINE_SIZE			(64)

// Configuration
#define THREAD_SAFE_API			(0)
#define BLOCK_SIZE				(1 * MB)
#define BUFFER_SIZE				(1 * GB)
#define MAX_QUEUE_DEPTH			(128)
#define FILE_TRUNCATE_TRESHOLD	(BUFFER_SIZE)
#define FILE_TRUNCATE_SIZE		(BUFFER_SIZE)
#define AIO_MAX_EVENTS			(MAX_QUEUE_DEPTH)

#define CTX_COUNT				(BUFFER_SIZE / BLOCK_SIZE)
// Maximum buffer space that can be requested via aios_write_begin() call [bytes].
#define MAX_MESSAGE_SIZE		((BUFFER_SIZE) / 2)

#define MIN(a, b)				((a) < (b) ? (a) : (b))

#define INFO(fmt, ...) ROS_INFO(fmt, ##__VA_ARGS__)
#define WARNING(fmt, ...) ROS_WARN(fmt, ##__VA_ARGS__)
#define ERROR(fmt, ...) ROS_ERROR(fmt, ##__VA_ARGS__)

typedef struct aioStreamS
{
	// File
	int fd;
	int fdOwner;
	int sh;
	long long fileSize;
	int err;
	// Buffer
	void *buffer;
	void *mirror;
	// Internal
	int threadStarted;
	pthread_t thread;
	// AIO contexts
	int aioCtxInitialized;
	io_context_t ctx;
	struct iocb iocb[CTX_COUNT];
	// Inter-thread variables
	volatile int shutdown;
	pthread_mutex_t apiMutex;
	pthread_mutex_t syncMutex;
	pthread_cond_t condvar;
	volatile int queueDepth;
	// Read offset (incremented in BLOCK_SIZE granularity)
	long long r;
	// Complete offset (incremented in BLOCK_SIZE granularity)
	volatile long long __attribute__((aligned(CACHE_LINE_SIZE))) c;
	// Write offset
	volatile long long __attribute__((aligned(CACHE_LINE_SIZE))) w;
} aioStreamT;

static void *streamHandler(void *arg);

aioStreamT *aios_open(const char *fileName, int fd)
{
	int e;

	aioStreamT *s = (aioStreamT*)malloc(sizeof(aioStreamT));
	if (s == NULL)
		return NULL;
	memset(s, 0, sizeof(aioStreamT));
	s->fd = -1;
	s->sh = -1;

	if (fd == -1)
	{
		// Open file
		INFO("aiostream open file %s", fileName);
		s->fd = open(fileName, O_CREAT | O_RDWR | O_NOATIME | O_EXCL | O_TRUNC | O_DIRECT, S_IRUSR | S_IWUSR);
		if (s->fd < 0)
		{
			ERROR("open() failed (%d)", errno);
			aios_close(s);
			return NULL;
		}
		s->fdOwner = 1;
	}
	else
	{
		INFO("aiostream open file descriptor %d", fd);
		s->fd = fd;
	}

    posix_fadvise(s->fd, 0, 0, POSIX_FADV_DONTNEED);
    posix_fadvise(s->fd, 0, 0, POSIX_FADV_SEQUENTIAL);

	// Map buffer
	{
		s->sh = shm_open("/aio_internal_shm", O_CREAT | O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
		if (s->sh == -1)
		{
			ERROR("shm_open() failed (%d)", errno);
			aios_close(s);
			return NULL;
		}

		if (shm_unlink("/aio_internal_shm") != 0)
		{
			ERROR("shm_unlink() failed (%d)", errno);
			aios_close(s);
			return NULL;
		}

		if (ftruncate(s->sh, 2 * BUFFER_SIZE) == -1)
		{
			ERROR("Failed to truncate shared memory segment (%d)", errno);
			aios_close(s);
			return NULL;
		}

        // Map buffer memory
        s->buffer = (uint8_t*)mmap64(NULL, 2 * BUFFER_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, s->sh, 0);
        if (s->buffer == MAP_FAILED)
        {
			ERROR("Failed to map internal memory (%d)", errno);
            aios_close(s);
            return NULL;
        }
		// Lock buffer memory
        e = mlock(s->buffer, BUFFER_SIZE);
		if (e != 0)
		{
			WARNING("Failed to lock internal memory (%d)", errno);
		}
		// Map mirror
		s->mirror = mmap64((void*)((uintptr_t)s->buffer + BUFFER_SIZE),
				BUFFER_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, s->sh, 0);
		if ((uintptr_t)s->mirror != (uintptr_t)s->buffer + BUFFER_SIZE)
		{
			ERROR("Failed to map internal memory mirror (%d)", errno);
			aios_close(s);
			return NULL;
		}
	}

	// Create AIO context
	e = io_setup(AIO_MAX_EVENTS, &s->ctx);
	if (e != 0)
	{
		ERROR("io_setup() failed (%d)", e);
		aios_close(s);
		return NULL;
	}
	s->aioCtxInitialized = 1;

	// Prepare IOCBs
	for (long i = 0; i < CTX_COUNT; i++)
	{
		void *block = (void*)((uintptr_t)s->buffer + i * BLOCK_SIZE);
		io_prep_pwrite(&s->iocb[i], s->fd, block, BLOCK_SIZE, 0);
		s->iocb[i].data = &s->iocb[i];
		s->iocb[i].u.c.offset = 0xffffffffffffffff;
	}

	pthread_cond_init(&s->condvar, NULL);
	pthread_mutex_init(&s->syncMutex, NULL);
	pthread_mutex_init(&s->apiMutex, NULL);

	// Start I/O handler thread
	e = pthread_create(&s->thread, NULL, streamHandler, s);
	if (e != 0)
	{
		ERROR("pthread_create() failed (%d)", e);
		aios_close(s);
		return NULL;
	}
	s->threadStarted = 1;

	return s;
}

static void *streamHandler(void *arg)
{
	aioStreamT *s = (aioStreamT*)arg;
	struct timespec timeout = { .tv_sec = 0, .tv_nsec = 0 * 1000 * 1000 };
	struct io_event events[AIO_MAX_EVENTS];
	struct iocb *iocbs[MAX_QUEUE_DEPTH];

	INFO("aiostream handler start");

	// I/O handler shall not process SIGTERM signals.
	sigset_t mask;
	sigemptyset(&mask);
	sigaddset(&mask, SIGTERM);
	sigaddset(&mask, SIGINT);
	sigprocmask(SIG_BLOCK, &mask, NULL);

	while (1)
	{
		// Fetch write offset
		long long w = s->w;

		// Advance file size
		if (!s->shutdown && s->fileSize <= w + FILE_TRUNCATE_TRESHOLD)
		{
			int e = fallocate64(s->fd, 0, s->fileSize, FILE_TRUNCATE_SIZE);
			if (e == 0)
			{
				s->fileSize += FILE_TRUNCATE_SIZE;
			}
			else
			{
				ERROR("Failed to extend file size (%d)", errno);
				s->err = errno;
				s->shutdown = 1;
			}
		}

		// Process completion events
		if (0 < s->queueDepth)
		{
			int n = io_getevents(s->ctx, 1, AIO_MAX_EVENTS, events, &timeout);

			if (0 < n)
			{
				for (int i = 0; i < n; i++)
				{
					if ((events[i].res != BLOCK_SIZE) || (events[i].res2 != 0))
					{
						ERROR("AIO event error %lu", events[i].res2);
					}
					// Clear offset to indicate that this block is available again
					((struct iocb*)events[i].data)->u.c.offset = 0xffffffffffffffff;
				}
				s->queueDepth -= n;

				// Iterate completed blocks and advance completed offset
				long long c = s->c;
				while (c < s->r)
				{
					int idx = (c % BUFFER_SIZE) / BLOCK_SIZE;
					if (s->iocb[idx].u.c.offset != 0xffffffffffffffff)
						break;
					c += BLOCK_SIZE;
				}
				if (s->c < c)
				{
					s->c = c;

					// Signal that new space is available
					pthread_mutex_lock(&s->syncMutex);
					pthread_cond_signal(&s->condvar);
					pthread_mutex_unlock(&s->syncMutex);
				}
			}
			else if (n < 0)
			{
				ERROR("io_getevents() failed (%d)", n);
				break;
			}
		}

		// Enqueue new blocks writes
		int writeBlockCount = 0;
		long long r = s->r;
		long long rMax = w & ~(BLOCK_SIZE - 1);
		if (s->shutdown)
			rMax = w;
		while (r < rMax && s->queueDepth + writeBlockCount < MAX_QUEUE_DEPTH)
		{
			// Prepare write
			int idx = (r % BUFFER_SIZE) / BLOCK_SIZE;
			s->iocb[idx].u.c.offset = r;
			iocbs[writeBlockCount] = &s->iocb[idx];
			r += BLOCK_SIZE;
			writeBlockCount++;
		}
		// Submit block writes
		if (0 < writeBlockCount)
		{
			int n = io_submit(s->ctx, writeBlockCount, iocbs);
			if (0 < n)
			{
				s->r += n * BLOCK_SIZE;
				s->queueDepth += n;
			}
			else if (n < 0)
			{
				if (n == -(EAGAIN))
				{
					// Insufficient resources are available to queue any iocbs
				}
				else
				{
					ERROR("io_submit() failed (%d)", n);
				}
			}
		}

		// Check if shutdown is requested and all data is written
		if (s->shutdown && w <= s->c)
		{
			break;
		}

		// Be nice
		usleep(5000);
	}

	INFO("aiostream handler shutdown");

	return NULL;
}

static void acquireFreeSpace(aioStreamT *s, unsigned size)
{
	// Wait for free space
	pthread_mutex_lock(&s->syncMutex);
	while (1)
	{
		// Check free space
		long long space = s->c + BUFFER_SIZE - s->w;
		if (size <= space)
			break;

		pthread_cond_wait(&s->condvar, &s->syncMutex);
	}
	pthread_mutex_unlock(&s->syncMutex);
}

int aios_write_begin(aioStreamT *s, void **buffer, unsigned size)
{
	if (s == NULL)
	{
		ERROR("Invalid stream handle");
		errno = EINVAL;
		return EINVAL;
	}

	if (buffer == NULL)
	{
		ERROR("Invalid buffer pointer");
		errno = EINVAL;
		return EINVAL;
	}

	if (MAX_MESSAGE_SIZE < size)
	{
		ERROR("Requested buffer size %u exceeded maximum size of %llu bytes", size, MAX_MESSAGE_SIZE);
		errno = EOVERFLOW;
		return EOVERFLOW;
	}

	if (s->shutdown)
	{
		ERROR("Failed to begin write. Stream terminated (%d)", s->err);
		errno = s->err;
		return -1;
	}

#if THREAD_SAFE_API
	pthread_mutex_lock(&s->apiMutex);
#endif

	*buffer = (void*)((uintptr_t)s->buffer + (s->w % BUFFER_SIZE));

	acquireFreeSpace(s, size);
	return 0;
}

int aios_write_end(aioStreamT *s, unsigned size)
{
	if (s == NULL)
	{
		ERROR("Invalid stream handle");
		errno = EINVAL;
		return EINVAL;
	}

	if (s->shutdown)
	{
		ERROR("Failed to end write. Stream terminated (%d)", s->err);
		errno = s->err;
		return -1;
	}

	s->w += size;

#if THREAD_SAFE_API
	pthread_mutex_unlock(&s->apiMutex);
#endif

	return 0;
}

int aios_write(aioStreamT *s, void *data, unsigned size)
{
	if (s == NULL)
	{
		ERROR("Invalid stream handle");
		errno = EINVAL;
		return -1;
	}

	if (data == NULL)
	{
		ERROR("Invalid data pointer");
		errno = EINVAL;
		return -1;
	}

#if THREAD_SAFE_API
	pthread_mutex_lock(&s->apiMutex);
#endif

	if (s->shutdown)
	{
		ERROR("Failed to end write. Stream terminated (%d)", s->err);
		errno = s->err;
		return -1;
	}

	while (0 < size)
	{
		int writeSize = MIN(size, MAX_MESSAGE_SIZE);

		acquireFreeSpace(s, writeSize);

		memcpy((void*)((uintptr_t)s->buffer + (s->w % BUFFER_SIZE)), data, writeSize);

		s->w += writeSize;
		size -= writeSize;
		data = (void*)((uintptr_t)data + writeSize);
	}

#if THREAD_SAFE_API
	pthread_mutex_unlock(&s->apiMutex);
#endif

	return 0;
}

int aios_close(aioStreamT *s)
{
	int e;
	void *rc;

	INFO("aiostream closing file");

	if (s == NULL)
	{
		ERROR("Invalid stream handle");
		return -1;
	}

	if (s->threadStarted)
	{
		// Shutdown handler thread
		s->shutdown = 1;
		pthread_join(s->thread, &rc);
	}

	if (s->aioCtxInitialized)
		io_destroy(s->ctx);

	if (s->fd != -1)
	{
		// Truncate file
		e = ftruncate64(s->fd, s->w);
		if (e != 0)
		{
			ERROR("Failed to truncate file (%d)", errno);
		}
		// Close file if we are the owner
		if (s->fdOwner == 1)
			close(s->fd);
	}

	if (s->mirror != NULL)
		munmap(s->mirror, BUFFER_SIZE);

	if (s->buffer != NULL)
		munmap(s->buffer, BUFFER_SIZE);

	if (s->sh != -1)
		close(s->sh);

	pthread_cond_destroy(&s->condvar);
	pthread_mutex_destroy(&s->syncMutex);
	pthread_mutex_destroy(&s->apiMutex);

	memset(s, 0, sizeof(aioStreamT));
	free(s);

	INFO("aiostream file closed");

	return 0;
}
