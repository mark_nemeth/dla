node("${params.node}") {
 stage('Get repo...') {
    checkout scm
 }
 stage('Building docker...') {
  try{
    withCredentials([file(credentialsId: 'tech_user_cert', variable: 'cert')]) {
        withCredentials([usernamePassword(credentialsId: 'tech_user_dla', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
            sh """
            if [ -d "docker_build_temp" ]; then rm -Rf docker_build_temp; fi
            cp -R ${params.path_to_component} docker_build_temp
            chmod -R 777 docker_build_temp
            cp $cert docker_build_temp/cert.pem
            """
            git_tag = sh(returnStdout: true, script: "git describe --tag --abbrev=0").trim()
            docker_build_args = "--no-cache --network host --build-arg https_proxy=https://pr-ext-sif.rd.corpintra.net:3128 \
            --build-arg IMAGE=${params.image} \
            --build-arg DAP_BUILD=true \
            --build-arg DANF_VERSION_TAG=$git_tag"
            path = "docker_build_temp"
            image_name = "${params.image_name}"
            regex = $/ "\"version\"\s*:\s*\"\K.*?(?=\")" /$
            image_version = sh (script: "grep -Po $regex $path/server/package.json", returnStdout: true).trim()
            //Build docker and push
            sh """
            cd $path
            docker rmi ${params.image} --force
            docker login athena.daimler.com --username="$USERNAME" --password="$PASSWORD"
            docker pull athena.daimler.com/dla_docker/$image_name:$image_version-$git_tag > /dev/null && exit 0 || echo "Build and push"
            docker build . -t athena.daimler.com/dla_docker/$image_name:latest $docker_build_args
            docker push athena.daimler.com/dla_docker/$image_name:latest
            docker tag athena.daimler.com/dla_docker/$image_name:latest athena.daimler.com/dla_docker/$image_name:$image_version-$git_tag
            docker push athena.daimler.com/dla_docker/$image_name:$image_version-$git_tag
            docker rmi ${params.image} --force
            docker rmi athena.daimler.com/dla_docker/$image_name:$image_version-$git_tag --force
            docker rmi athena.daimler.com/dla_docker/$image_name:latest --force
            docker logout athena.daimler.com
            """
        }
    }
  }
  catch (e) {
    deleteDir()
    throw e
  }
   deleteDir()
 }
}
