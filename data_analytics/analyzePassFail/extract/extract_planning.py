import rosbag
import sys

#bag = rosbag.Bag('20180214_105847_onlyimar.bag')

bag = rosbag.Bag(sys.argv[1])

topic = '/MapsService/instance_0/LocalizationHorizonInterface'

filename_delimiter = 'csv/Out'+topic.replace("/","_")+'_delimiter.csv'
filename_objects = 'csv/Out'+topic.replace("/","_")+'_objects.csv'
filename_reflines = 'csv/Out'+topic.replace("/","_")+'_reflines.csv'
txtfilename = 'csv/Out'+topic.replace("/","_")+'.txt'

filename_delimiter_virtual = 'csv/Out'+topic.replace("/","_")+'_delimiter_virtual.csv'
filename_delimiter_solid = 'csv/Out'+topic.replace("/","_")+'_delimiter_solid.csv'
filename_delimiter_dashed = 'csv/Out'+topic.replace("/","_")+'_delimiter_dashed.csv'
filename_delimiter_curb = 'csv/Out'+topic.replace("/","_")+'_delimiter_curb.csv'

with open(filename_delimiter, 'w') as f, open(filename_delimiter_virtual, 'w') as fv, open(filename_delimiter_solid, 'w') as fs, open(filename_delimiter_dashed, 'w') as fd, open(filename_delimiter_curb, 'w') as fc, open(filename_objects, 'w') as f1, open(filename_reflines, 'w') as f2, open(txtfilename, 'w') as txtfile:
  for msg in bag.read_messages(topics=[topic]):
    print >> txtfile, msg.message
    #print >> f, msg.message.delimiter_lines[0].points[0].x_
    for i in range(0, len(msg.message.delimiter_lines)):
      for j in range(0, len(msg.message.delimiter_lines[i].points)):
        f.write(str(msg.message.header.timestamp.s)+","
               +str(msg.message.header.timestamp.ns)+","
               +str(msg.message.delimiter_lines[i].bndid)+","
               +str(msg.message.delimiter_lines[i].attrid)+","
               +str(msg.message.delimiter_lines[i].reflineid)+","
               +str(msg.message.delimiter_lines[i].type)+","
               +str(msg.message.delimiter_lines[i].color)+","
               +str(msg.message.delimiter_lines[i].points[j].x_)+","
               +str(msg.message.delimiter_lines[i].points[j].y_)+","
               +str(msg.message.delimiter_lines[i].points[j].z_)
               +"\n")
        if msg.message.delimiter_lines[i].type==1:
          fv.write(str(msg.message.header.timestamp.s)+","
               +str(msg.message.header.timestamp.ns)+","
               +str(msg.message.delimiter_lines[i].bndid)+","
               +str(msg.message.delimiter_lines[i].attrid)+","
               +str(msg.message.delimiter_lines[i].reflineid)+","
               +str(msg.message.delimiter_lines[i].type)+","
               +str(msg.message.delimiter_lines[i].color)+","
               +str(msg.message.delimiter_lines[i].points[j].x_)+","
               +str(msg.message.delimiter_lines[i].points[j].y_)+","
               +str(msg.message.delimiter_lines[i].points[j].z_)
               +"\n")
        elif msg.message.delimiter_lines[i].type==2:
           fs.write(str(msg.message.header.timestamp.s)+","
               +str(msg.message.header.timestamp.ns)+","
               +str(msg.message.delimiter_lines[i].bndid)+","
               +str(msg.message.delimiter_lines[i].attrid)+","
               +str(msg.message.delimiter_lines[i].reflineid)+","
               +str(msg.message.delimiter_lines[i].type)+","
               +str(msg.message.delimiter_lines[i].color)+","
               +str(msg.message.delimiter_lines[i].points[j].x_)+","
               +str(msg.message.delimiter_lines[i].points[j].y_)+","
               +str(msg.message.delimiter_lines[i].points[j].z_)
               +"\n")
        elif msg.message.delimiter_lines[i].type==3:
           fd.write(str(msg.message.header.timestamp.s)+","
               +str(msg.message.header.timestamp.ns)+","
               +str(msg.message.delimiter_lines[i].bndid)+","
               +str(msg.message.delimiter_lines[i].attrid)+","
               +str(msg.message.delimiter_lines[i].reflineid)+","
               +str(msg.message.delimiter_lines[i].type)+","
               +str(msg.message.delimiter_lines[i].color)+","
               +str(msg.message.delimiter_lines[i].points[j].x_)+","
               +str(msg.message.delimiter_lines[i].points[j].y_)+","
               +str(msg.message.delimiter_lines[i].points[j].z_)
               +"\n")
        elif msg.message.delimiter_lines[i].type==10:
           fc.write(str(msg.message.header.timestamp.s)+","
               +str(msg.message.header.timestamp.ns)+","
               +str(msg.message.delimiter_lines[i].bndid)+","
               +str(msg.message.delimiter_lines[i].attrid)+","
               +str(msg.message.delimiter_lines[i].reflineid)+","
               +str(msg.message.delimiter_lines[i].type)+","
               +str(msg.message.delimiter_lines[i].color)+","
               +str(msg.message.delimiter_lines[i].points[j].x_)+","
               +str(msg.message.delimiter_lines[i].points[j].y_)+","
               +str(msg.message.delimiter_lines[i].points[j].z_)
               +"\n")


    for i in range(0, len(msg.message.map_objects)):
      #print(msg.message.map_objects[i].outline[0].x_)
      for j in range(0, len(msg.message.map_objects[i].outline)):
        f1.write(str(msg.message.header.timestamp.s)+","
               +str(msg.message.header.timestamp.ns)+","
               +str(msg.message.map_objects[i].id)+","
               +str(msg.message.map_objects[i].type)+","
               +str(msg.message.map_objects[i].shape_type)+","
               +str(msg.message.map_objects[i].outline[j].x_)+","
               +str(msg.message.map_objects[i].outline[j].y_)+","
               +str(msg.message.map_objects[i].outline[j].z_)
               +"\n")  

    for i in range(0, len(msg.message.reference_lines)):
      #print(msg.message.map_objects[i].outline[0].x_)
      for j in range(0, len(msg.message.reference_lines[i].points)):
        f2.write(str(msg.message.header.timestamp.s)+","
               +str(msg.message.header.timestamp.ns)+","
               +str(msg.message.reference_lines[i].id)+","
               +str(msg.message.reference_lines[i].index)+","
               +str(msg.message.reference_lines[i].points[j].x_)+","
               +str(msg.message.reference_lines[i].points[j].y_)+","
               +str(msg.message.reference_lines[i].points[j].z_)
               +"\n")  


f.close()
bag.close()    
