""""Constants"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import uuid

STATUS_OK = "ok"
STATUS_ERR = "error"

REQ_STATUS_CODE_OK = 200
REQ_STATUS_CODE_RESOURCE_CREATED = 201
REQ_STATUS_CODE_BAD_REQ = 400
REQ_STATUS_CODE_NOT_FOUND = 404
REQ_STATUS_CODE_SERVER_ERROR = 500
REST_CONNECTION_ERROR = "rest_connection_error"
REST_HTTP_ERROR = "rest_http_error"
REST_TIMEOUT_ERROR = "rest_timeout_error"
REST_TOO_MANY_REDIRECTS_ERROR = "rest_too_many_redirects_error"


constants = {
    "STATUS_OK": STATUS_OK,
    "STATUS_ERR": STATUS_ERR,

    "REQ_STATUS_CODE_OK": REQ_STATUS_CODE_OK,
    "REQ_STATUS_CODE_RESOURCE_CREATED": REQ_STATUS_CODE_RESOURCE_CREATED,
    "REQ_STATUS_CODE_BAD_REQ": REQ_STATUS_CODE_BAD_REQ,
    "REQ_STATUS_CODE_NOT_FOUND": REQ_STATUS_CODE_NOT_FOUND,
    "REQ_STATUS_CODE_SERVER_ERROR": REQ_STATUS_CODE_SERVER_ERROR,
}

ROOT_FILE_GUID = uuid.uuid3(uuid.NAMESPACE_URL, "https://athena.daimler.com/")