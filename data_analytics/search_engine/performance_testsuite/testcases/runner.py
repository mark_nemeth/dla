__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse

import invokust
from DANFUtils.logging import logger
from config.constants import DANF_VERSION_TAG


def _get_commandline_args():
    parser = argparse.ArgumentParser(description='Execute a locust file')
    parser.add_argument('--file',
                        action='store',
                        required=True,
                        help='path to locust file')
    parser.add_argument('--host',
                        action='store',
                        required=True,
                        help='host to contact in test')
    parser.add_argument('--parallelism',
                        type=int,
                        action='store',
                        required=False,
                        default=1,
                        help='number of concurrent users')
    parser.add_argument('--duration',
                        type=int,
                        action='store',
                        required=False,
                        default=5,
                        help='duration of test in minutes')
    return parser.parse_args()


def _run_test(settings):
    logger.info(f"Starting performance test with settings: {settings}")
    loadtest = invokust.LocustLoadTest(settings)
    loadtest.run()
    return loadtest.stats()


def main(args):
    logger.info(f"Runner started with args: {args}")

    settings = invokust.create_settings(
        locustfile=args.file,
        host=args.host,
        num_clients=args.parallelism,
        hatch_rate=1,
        run_time=f"{args.duration}m"
    )
    stats = _run_test(settings)
    logger.info(f"Completed performance test. Stats: {stats}")


if __name__ == '__main__':
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    args = _get_commandline_args()
    main(args)
