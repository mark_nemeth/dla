import {createReducer} from '@reduxjs/toolkit';
import * as actions from './HomeActions';
import {FileTypes} from "../../components/Sidebar/FilterElements/ByFileFilter/ByFileFilter";

export const HomeReducer = createReducer({}, {
  [actions.fetchBagfilesSuccess]: (state, action) => {
    state.error = false;
    state.showTableMessage = false;
    state.bagfiles = action.payload.bagfiles;
    state.total = action.payload.total;
    state.currentFiles = state.bagfiles;
    state.showLoading = false;
  },
  [actions.fetchBagfilesError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.bagfiles = [];
    state.total = 0;
    state.currentFiles = state.bagfiles;
    state.showLoading = false;
  },
  [actions.fetchBagfilesEmpty]: (state, action) => {
    state.showTableMessage = true;
    state.tableMessage = action.payload;
    state.bagfiles = [];
    state.total = 0;
    state.currentFiles = state.bagfiles;
    state.showLoading = false;
  },
  [actions.fetchOriginalFilesSuccess]: (state, action) => {
    state.error = false;
    state.showTableMessage = false;
    state.originalFiles = action.payload.bagfiles;
    state.total = action.payload.total;
    state.currentFiles = state.originalFiles;
    state.showLoading = false;
  },
  [actions.fetchOriginalFilesError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.originalFiles = [];
    state.total = 0;
    state.currentFiles = state.originalFiles;
    state.showLoading = false;
  },
  [actions.fetchOriginalFilesEmpty]: (state, action) => {
    state.showTableMessage = true;
    state.tableMessage = action.payload;
    state.originalFiles = [];
    state.total = 0;
    state.currentFiles = state.originalFiles;
    state.showLoading = false;
  },
  [actions.fetchAlwaysonFilesSuccess]: (state, action) => {
    state.error = false;
    state.showTableMessage = false;
    state.alwaysonFiles = action.payload.bagfiles;
    state.total = action.payload.total;
    state.currentFiles = state.alwaysonFiles;
    state.showLoading = false;
  },
  [actions.fetchAlwaysonFilesError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.alwaysonFiles = [];
    state.total = 0;
    state.currentFiles = state.alwaysonFiles;
    state.showLoading = false;
  },
  [actions.fetchAlwaysonFilesEmpty]: (state, action) => {
    state.showTableMessage = true;
    state.tableMessage = action.payload;
    state.alwaysonFiles = [];
    state.total = 0;
    state.currentFiles = state.alwaysonFiles;
    state.showLoading = false;
  },
  [actions.fetchChildBagfilesSuccess]: (state, action) => {
    state.error = false;
    state.showTableMessage = false;
    state.childBagfiles = action.payload.childBagfiles;
    state.total = action.payload.total;
    state.currentFiles = state.childBagfiles;
    state.showLoading = false;
  },
  [actions.fetchChildBagfilesError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.childBagfiles = [];
    state.total = 0;
    state.currentFiles = state.childBagfiles;
    state.showLoading = false;
  },
  [actions.fetchChildBagfilesEmpty]: (state, action) => {
    state.showTableMessage = true;
    state.tableMessage = action.payload;
    state.childBagfiles = [];
    state.total = 0;
    state.currentFiles = state.childBagfiles;
    state.showLoading = false;
  },
  [actions.fetchDrivesSuccess]: (state, action) => {
    state.error = false;
    state.showTableMessage = false;
    state.drives = action.payload.drives;
    state.total = action.payload.total;
    state.currentFiles = state.drives;
    state.showLoading = false;
  },
  [actions.fetchDrivesError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
    state.drives = [];
    state.total = 0;
    state.currentFiles = state.drives;
    state.showLoading = false;
  },
  [actions.fetchDrivesEmpty]: (state, action) => {
    state.showTableMessage = true;
    state.tableMessage = action.payload;
    state.drives = [];
    state.total = 0;
    state.currentFiles = state.drives;
    state.showLoading = false;
  },
  [actions.fetchBagfileFiltersSuccess]: (state, action) => {
    state.error = false;
    state.filterOptions.bagfileVehicleIdList = action.payload.vehicle_ids;
    state.filterOptions.bagfileSizeLimits = action.payload.size_min_max;
    state.filterOptions.bagfileDurationLimits = action.payload.duration_min_max;
    state.filterOptions.bagfileTopicList = action.payload.topics;
  },
  [actions.fetchBagfileFiltersError]: (state, action) => {
    state.error = true;
    state.filterOptions.bagfileVehicleIdList = [];
    state.filterOptions.bagfileSizeLimits = [];
    state.filterOptions.bagfileDurationLimits = [];
    state.filterOptions.bagfileTopicList = [];
  },
  [actions.fetchOriginalFileFiltersSuccess]: (state, action) => {
    state.error = false;
    state.filterOptions.originalFileVehicleIdList = action.payload.vehicle_ids;
    state.filterOptions.originalFileSizeLimits = action.payload.size_min_max;
    state.filterOptions.originalFileDurationLimits = action.payload.duration_min_max;
    state.filterOptions.originalFileTopicList = action.payload.topics;
  },
  [actions.fetchOriginalFileFiltersError]: (state, action) => {
    state.error = true;
    state.filterOptions.originalFileVehicleIdList = [];
    state.filterOptions.originalFileSizeLimits = [];
    state.filterOptions.originalFileDurationLimits = [];
    state.filterOptions.originalFileTopicList = [];
  },
  [actions.fetchDriveFiltersSuccess]: (state, action) => {
    state.error = false;
    state.filterOptions.driveVehicleIdList = action.payload.vehicle_ids;
    state.filterOptions.driveDurationLimits = action.payload.duration_min_max;
    state.filterOptions.drivenByValueList = action.payload.driven_by_values;
    state.filterOptions.driverList = action.payload.drivers;
    state.filterOptions.operatorList = action.payload.operators;
    state.filterOptions.hardwareReleaseList = action.payload.hardware_releases;
  },
  [actions.fetchDriveFiltersError]: (state, action) => {
    state.error = true;
    state.filterOptions.driveVehicleIdList = [];
    state.filterOptions.driveDurationLimits = [];
    state.filterOptions.drivenByValueList = [];
    state.filterOptions.driverList = [];
    state.filterOptions.operatorList = [];
    state.filterOptions.hardwareReleaseList = [];
  },
  [actions.fetchAlwaysonFileFiltersSuccess]: (state, action) => {
    state.error = false;
    state.filterOptions.alwaysonFileVehicleIdList = action.payload.vehicle_ids;
    state.filterOptions.alwaysonFileSizeLimits = action.payload.size_min_max;
    state.filterOptions.alwaysonFileDurationLimits = action.payload.duration_min_max;
    state.filterOptions.alwaysonFileTopicList = action.payload.topics;
  },
  [actions.fetchAlwaysonFileFiltersError]: (state, action) => {
    state.error = true;
    state.filterOptions.alwaysonFileVehicleIdList = [];
    state.filterOptions.alwaysonFileSizeLimits = [];
    state.filterOptions.alwaysonFileDurationLimits = [];
    state.filterOptions.alwaysonFileTopicList = [];
  },
  [actions.fetchChildBagfileFiltersSuccess]: (state, action) => {
    state.error = false;
    state.filterOptions.childBagfileVehicleIdList = action.payload.vehicle_ids;
    state.filterOptions.childBagfileSizeLimits = action.payload.size_min_max;
    state.filterOptions.childBagfileDurationLimits = action.payload.duration_min_max;
    state.filterOptions.childBagfileTopicList = action.payload.topics;
  },
  [actions.fetchChildBagfileFiltersError]: (state, action) => {
    state.error = true;
    state.filterOptions.childBagfileVehicleIdList = [];
    state.filterOptions.childBagfileSizeLimits = [];
    state.filterOptions.childBagfileDurationLimits = [];
    state.filterOptions.childBagfileTopicList = [];
  },
  [actions.showLoading]: (state, action) => {
    state.showLoading = true;
  },
  [actions.setAppliedFilters]: (state, action) => {
    state.filters.offset = 0;
    state.appliedFilters = state.filters;
  },
  [actions.resetFilters]: (state, action) => {
    const tmpSearchQuery = state.filters.file_name; // keep search input when reset is clicked
    const tmpFileTypeQuery = state.filters.file_type; // keep file type when reset is clicked
    state.filters = {};
    state.appliedFilters = {};

    state.filters.file_name = tmpSearchQuery;
    state.filters.file_type = tmpFileTypeQuery;

    state.appliedFilters.file_name = tmpSearchQuery;
    state.appliedFilters.file_type = tmpFileTypeQuery;

    state.currentDurationRange = undefined;
    state.currentSizeRange = undefined;
    state.currentTopicSearchQuery = undefined;
  },
  [actions.setFilterByFileType]: (state, action) => {
    const tmpSearchQuery = state.filters.file_name; // keep search input when reset is clicked
    state.fileType = action.payload;
    state.currentFiles = [];
    state.filters = {};
    state.appliedFilters = {};
    switch (state.fileType) {
      case 'originalfile':
        state.appliedFilters.file_type = 'ORIGINAL';
        state.filters.file_type = 'ORIGINAL';
        break;
      case 'alwaysonfile':
        state.appliedFilters.file_type = 'ALWAYSON';
        state.filters.file_type = 'ALWAYSON';
        break;
      default:
    }
    state.filters.file_name = tmpSearchQuery;
    state.appliedFilters.file_name = tmpSearchQuery;
    state.currentDurationRange = undefined;
    state.currentSizeRange = undefined;
  },
  [actions.setSortBy]: (state, action) => {
    state.filters.sortby = action.payload;
    state.appliedFilters.sortby = action.payload;
  },
  [actions.setSortBy]: (state, action) => {
    state.filters.sortby = action.payload;
    state.appliedFilters.sortby = action.payload;
  },
  [actions.setPaginationLimit]: (state, action) => {
    state.filters.limit = action.payload;
    state.appliedFilters.limit = action.payload;
  },
  [actions.setPaginationOffset]: (state, action) => {
    state.filters.offset = action.payload;
    state.appliedFilters.offset = action.payload;
  },
  [actions.setCurrentSearchQuery]: (state, action) => {
    if (state.fileType === FileTypes.DRIVE) {
      state.filters.description = action.payload;
    } else {
      state.filters.file_name = action.payload;
    }
  },
  [actions.setFilterBySearchQuery]: (state, action) => {
    if (state.fileType === FileTypes.DRIVE) {
      state.filters.offset = 0;
      state.appliedFilters.offset = 0;
      state.filters.description = action.payload;
      state.appliedFilters.description = action.payload;
      state.filters.file_name = undefined;
      state.appliedFilters.file_name = undefined;
    } else {
      state.filters.offset = 0;
      state.appliedFilters.offset = 0;
      state.filters.file_name = action.payload;
      state.appliedFilters.file_name = action.payload;
      state.filters.description = undefined;
      state.appliedFilters.description = undefined;
    }
  },
  [actions.filterByVIDs]: (state, action) => {
    state.filters.vehicle_id_num = action.payload
  },
  [actions.filterByDrivers]: (state, action) => {
    state.filters.driver = action.payload;
  },
  [actions.filterByOperators]: (state, action) => {
    state.filters.operator = action.payload;
  },
  [actions.filterByDrivenByValues]: (state, action) => {
    state.filters.driven_by = action.payload;
  },
  [actions.filterByHardwareReleases]: (state, action) => {
    state.filters.hardware_release = action.payload;
  },
  [actions.filterByDate]: (state, action) => {
    state.filters['start[gte]'] = action.payload.startdate;
    state.filters['start[lte]'] = action.payload.enddate;
  },
  [actions.setCurrentDurationRange]: (state, action) => {
    state.currentDurationRange = action.payload;
  },
  [actions.filterByDuration]: (state, action) => {
    state.filters['duration[gte]'] = action.payload.min;
    state.filters['duration[lte]'] = action.payload.max;
  },
  [actions.setCurrentSizeRange]: (state, action) => {
    state.currentSizeRange = action.payload;
  },
  [actions.filterBySize]: (state, action) => {
    state.filters['size[gte]'] = action.payload.min;
    state.filters['size[lte]'] = action.payload.max;
  },
  [actions.filterByEvents]: (state, action) => {
    state.filters.event = action.payload;
  },
  [actions.setCurrentTopicSearchQuery]: (state, action) => {
    state.currentTopicSearchQuery = action.payload;
  },
  [actions.filterByTopics]: (state, action) => {
    state.filters.topics = action.payload
  },
  [actions.filterByStatus]: (state, action) => {
    state.filters.processing_state = action.payload
  },
  [actions.filterByBagfileType]: (state, action) => {
    state.filters.file_type = action.payload
  },
  [actions.filterByDriveType]: (state, action) => {
    state.filters.drive_types = action.payload
  },
  [actions.setCurrentVelocityRange]: (state, action) => {
    state.currentVelocityRange = action.payload;
  },
  [actions.filterByVelocity]: (state, action) => {
    state.filters['feat.vel[gte]'] = action.payload.min.toFixed(1);
    state.filters['feat.vel[lte]'] = action.payload.max.toFixed(1);
  },
  [actions.setCurrentAccelerationRange]: (state, action) => {
    state.currentAccelerationRange = action.payload;
  },
  [actions.filterByAcceleration]: (state, action) => {
    state.filters['feat.acc[gte]'] = action.payload.min.toFixed(1);
    state.filters['feat.acc[lte]'] = action.payload.max.toFixed(1);
  },
  [actions.setCurrentJerkRange]: (state, action) => {
    state.currentJerkRange = action.payload;
  },
  [actions.filterByJerk]: (state, action) => {
    state.filters['feat.jerk[gte]'] = action.payload.min.toFixed(2);
    state.filters['feat.jerk[lte]'] = action.payload.max.toFixed(2);
  },
  [actions.filterByLaneChange]: (state, action) => {
    state.filters['feat.number_of_lanechanges[gte]'] = action.payload;
    if (action.payload === 0) delete state.filters['feat.number_of_lanechanges[gte]']
  },
  [actions.filterByEngagementState]: (state, action) => {
    state.filters['feat.eng_state'] = action.payload;
    if (action.payload === 0) delete state.filters['feat.eng_state']
  },
  [actions.reprocessingBagfileSuccess]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'success';
  },
  [actions.reprocessingBagfileError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
  },
  [actions.selectTableEntry]: (state, action) => {
    state.selectedTablelRows = action.payload
  },
  [actions.deleteSelectedBagfiles]: (state, action) => {
    // TODO delete files from server
  },
  [actions.downloadSelectedBagfiles]: (state, action) => {
    // TODO download for all selectedBagfiles
  },
  [actions.exportResultsWarning]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'info';
  },
  [actions.exportResultsError]: (state, action) => {
    state.error = true;
    state.errorMessage = action.payload;
    state.errorMessageType = 'danger';
  }
});