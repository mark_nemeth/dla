"""Metadata REST API account route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants

from handlers.auth import auth
from handlers.utils import utils
from handlers.accounts import AccountHandler
from model.accounts import CreateAccountRequest, UpdateAccountRequest
from requesthandler.base_request_handler import BaseRequestHandler


_RESP_EXCLUDES = {'key_hash'}
"""Parameters of accounts that should not be included in responses"""


class AccountsRequestHandler(BaseRequestHandler):

    _handler = AccountHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET, POST')

    @auth.requires_entitlements(["openid"])
    def get(self):
        tracing.start_task_in_context('accounts/get',
                                      context_headers=self.request.headers)

        accounts = self._handler.get_all()

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=[account.dict(exclude=_RESP_EXCLUDES) for account in accounts],
            status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('accounts/post',
                                      context_headers=self.request.headers)

        create_account_req = self.deserialize(self.request.body,
                                         CreateAccountRequest)

        new_account = self._handler.create(create_account_req)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=[new_account.dict(exclude=_RESP_EXCLUDES)],
            status_code=constants['REQ_STATUS_CODE_RESOURCE_CREATED'],
            location=f"/accounts/{new_account.guid}")


class AccountRequestHandler(BaseRequestHandler):

    _schema_guid = BaseRequestHandler.load_json_schema("guid.schema")
    _handler = AccountHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET, PATCH')

    @auth.requires_entitlements(["openid"])
    def get(self, guid):
        tracing.start_task_in_context('accounts/{guid}/get',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        account = self._handler.get(guid)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=[account.dict(exclude=_RESP_EXCLUDES)],
            status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def patch(self, guid):
        tracing.start_task_in_context('accounts/{guid}/patch',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        update_req = self.deserialize(self.request.body,
                                      UpdateAccountRequest)

        new_account = self._handler.update(guid, update_req)

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=[new_account.dict(exclude=_RESP_EXCLUDES)],
            status_code=constants["REQ_STATUS_CODE_OK"])
