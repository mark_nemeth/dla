__copyright__ = '''
COPYRIGHT: (c) 2020 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import subprocess
import tempfile
from typing import List

from config import config_handler as configurator
from config import logging_handler as logger
from config import ExceptionHandler
from utils.text_format import dla_conv_format


def _call(commands: List[str]):
    path = os.path.abspath(os.path.dirname(__file__))
    dla_converter = f"{path}/dla_converter"
    if not os.path.isfile(dla_converter):
        raise ExceptionHandler(code='dla_converter_not_found_err')

    _converter = [
        "timeout", configurator.get_value('dla_conv_timeout_app'),
        dla_converter,
    ]
    commands = _converter + commands

    if configurator.get_value('log_level_env') == 'debug':
        logger.debug(f'send command to dla_converter: {commands}')

    proc = subprocess.Popen(commands, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = proc.communicate()
    output = output.decode("utf-8")
    returncode = proc.returncode

    if configurator.get_value('log_level_env') == 'debug':
        logger.debug(f'results of dla_converter: {output}')

    if returncode == 0:
        return output
    else:
        if returncode == 124:
            raise ExceptionHandler(code='dla_converter_timeout_err', debug_cmd=err, debug_code=returncode,
                                   debug_msg=output)
        else:
            raise ExceptionHandler(code='dla_converter_err', debug_cmd=err, debug_code=returncode,
                                   debug_msg=output)


def _check_file_exists(filename: str):
    if not os.path.isfile(filename):
        raise ExceptionHandler(code='bagfile_not_found_err', debug_msg=f'No such file: {filename}')


def get_help():
    logger.info(f'show help')
    commands = ['--help']
    return _call(commands)


def get_info(bagfile: str):
    _check_file_exists(bagfile)
    logger.info(f'get info for: {bagfile}')
    commands = ['--info', bagfile]
    return dla_conv_format(_call(commands))


# by topic
def sliceby_topic(
        bagfile: str,
        topic: str
):
    _check_file_exists(bagfile)
    logger.info(f'get bagfile slice for: {bagfile} by topic {topic}')
    bagfile_slice = tempfile.NamedTemporaryFile(suffix='.bag')

    commands = [
        '--in', bagfile,
        '--topic', topic,
        '--out', bagfile_slice.name
    ]

    return _call(commands), bagfile_slice


def sliceby_topic_time(
        bagfile: str,
        topic: str,
        start_time: int,
        end_time: int
):
    _check_file_exists(bagfile)
    logger.info(f'get bagfile slice for: {bagfile} by topic {topic} and nanosecond time [{start_time}, '
                f'{end_time}]')

    bagfile_slice = tempfile.NamedTemporaryFile(suffix='.bag')

    commands = [
        '--in', bagfile,
        '--topic', topic,
        '--time', str(start_time), str(end_time),
        '--out', bagfile_slice.name
    ]

    return _call(commands), bagfile_slice


def sliceby_topic_relative_time(
        bagfile: str,
        topic: str,
        start_relative: int,
        end_relative: int
):
    _check_file_exists(bagfile)
    logger.info(f'get bagfile slice for: {bagfile} by topic {topic} and relative time [{start_relative}, '
                f'{end_relative}]')
    bagfile_slice = tempfile.NamedTemporaryFile(suffix='.bag')

    commands = [
        '--in', bagfile,
        '--topic', topic,
        '--relative-time', str(start_relative), str(end_relative),
        '--out', bagfile_slice.name
    ]

    return _call(commands), bagfile_slice


# profile
def sliceby_profile(
        bagfile: str,
        profile: str,
        apply_time_correct: bool = False
):
    _check_file_exists(bagfile)
    logger.info(f'get bagfile slice for: {bagfile} by profile {profile}')
    bagfile_slice = tempfile.NamedTemporaryFile(suffix='.bag')

    commands = [
        '--in', bagfile,
        '--profile', configurator.get_value('topics_profiles_app'), profile,
        '--topics', '/vehicle_id', '/vehicle_vin'
    ]

    if apply_time_correct:
        commands += [
            '--fix-time'
        ]

    commands += [
        '--out', bagfile_slice.name
    ]

    return _call(commands), bagfile_slice


def sliceby_profile_time(
        bagfile: str,
        profile: str,
        start_time: int,
        end_time: int,
        apply_time_correct: bool = False
):
    _check_file_exists(bagfile)
    logger.info(f'get bagfile slice for: {bagfile} by profile {profile} and nanosecond time [{start_time}, '
                f'{end_time}]')
    bagfile_slice = tempfile.NamedTemporaryFile(suffix='.bag')

    commands = [
        '--in', bagfile,
        '--profile', configurator.get_value('topics_profiles_app'), profile,
        '--topics', '/vehicle_id', '/vehicle_vin',
        '--time', str(start_time), str(end_time),
    ]

    if apply_time_correct:
        commands += [
            '--fix-time'
        ]

    commands += [
        '--out', bagfile_slice.name
    ]

    return _call(commands), bagfile_slice


def sliceby_profile_relative_time(
        bagfile: str,
        profile: str,
        start_relative: int,
        end_relative: int,
        apply_time_correct: bool = False
):
    _check_file_exists(bagfile)
    logger.info(f'get bagfile slice for: {bagfile} by profile {profile} and relative time [{start_relative}, '
                f'{end_relative}]')
    bagfile_slice = tempfile.NamedTemporaryFile(suffix='.bag')

    commands = [
        '--in', bagfile,
        '--profile', configurator.get_value('topics_profiles_app'), profile,
        '--topics', '/vehicle_id', '/vehicle_vin',
        '--relative-time', str(start_relative), str(end_relative),
    ]

    if apply_time_correct:
        commands += [
            '--fix-time'
        ]

    commands += [
        '--out', bagfile_slice.name
    ]

    return _call(commands), bagfile_slice


if __name__ == "__main__":
    output1 = get_help()
    bagfile = "/mapr/059-va2.mbc.de/data/buffer/rd/athena/08_robotaxi/plog/" \
              "WDD2221591A470239/2020/07/30/" \
              "20200730_141149_CID_Master_1016_djostme_closed_loop_2_0_ORIGINAL.bag"
    topic = "/sensor/axis_camera/cam_front/compressed"
    _check_file_exists(bagfile)
    print(f'\nbagfile size: {os.stat(bagfile).st_size}')

    output2 = get_info(bagfile=bagfile)

    output3, bagfile_slice1 = sliceby_topic(
        bagfile=bagfile,
        topic=topic
    )
    print(f'\nslice size: {os.stat(bagfile_slice1.name).st_size}')

    output4, bagfile_slice2 = sliceby_topic_time(
        bagfile=bagfile,
        topic=topic,
        start_time=1596111854607958517,
        end_time=1596111854607958517 + 60e9  # 1 second
    )
    print(f'\nslice size: {os.stat(bagfile_slice2.name).st_size}')

    output5, bagfile_slice3 = sliceby_topic_relative_time(
        bagfile=bagfile,
        topic=topic,
        start_relative=0,
        end_relative=20  # 20%
    )
    print(f'\nslice size: {os.stat(bagfile_slice3.name).st_size}')

    # bu profile
    profile = 'localization_and_map_creation_io'
    output6, bagfile_slice4 = sliceby_profile(
        bagfile=bagfile,
        profile=profile,
        apply_time_correct=False
    )
    print(f'\nslice size: {os.stat(bagfile_slice4.name).st_size}')

    output7, bagfile_slice5 = sliceby_profile_time(
        bagfile=bagfile,
        profile=profile,
        start_time=1596111854607958517,
        end_time=1596111854607958517 + 30e9,  # 0.5 second
        apply_time_correct=False
    )
    print(f'\nslice size: {os.stat(bagfile_slice5.name).st_size}')

    output8, bagfile_slice6 = sliceby_profile_relative_time(
        bagfile=bagfile,
        profile=profile,
        start_relative=0,
        end_relative=20,  # 20%
        apply_time_correct=False
    )
    print(f'\nslice size: {os.stat(bagfile_slice6.name).st_size}')

    # bu profile with apply_time_correct
    profile = 'localization_and_map_creation_io'
    output9, bagfile_slice5 = sliceby_profile(
        bagfile=bagfile,
        profile=profile,
        apply_time_correct=True
    )
    print()
    print(f'slice size: {os.stat(bagfile_slice5.name).st_size}')

    output10, bagfile_slice6 = sliceby_profile_time(
        bagfile=bagfile,
        profile=profile,
        start_time=1596111854607958517,
        end_time=1596111854607958517 + 30e9,  # 0.5 second
        apply_time_correct=True
    )
    print(f'\nslice size: {os.stat(bagfile_slice6.name).st_size}')

    output11, bagfile_slice7 = sliceby_profile_relative_time(
        bagfile=bagfile,
        profile=profile,
        start_relative=0,
        end_relative=20,  # 20%
        apply_time_correct=True
    )
    print(f'\nslice size: {os.stat(bagfile_slice7.name).st_size}')
