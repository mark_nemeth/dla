import React from 'react';
import DateTimeEntry from "../../../AttributeFormatter/DateTime/DateTimeEntry";
import LabelEntry from "../../../AttributeFormatter/Label/LabelEntry";
import LabelContainer from "../../../Utils/LabelContainer/LabelContainer";
import Card from "react-bootstrap/Card";
import {Row} from "react-bootstrap";
import './MissionMetadataView.scss';

export function MissionMetadataView(props) {

  return (
      <Card className="mission-information-section">
        <span className="mission-information-section-label">
          Mission Information
        </span>
        <Row className='information-row'>
          <LabelContainer label="Mission Guid" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.mission.guid}/>
          </LabelContainer>
          <LabelContainer label="Mission Id" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.mission.mission_id}/>
          </LabelContainer>
        </Row>
        <Row className='information-row'>
          <LabelContainer label="Mission Name" variant="fifty-percent-card card-style-filled">
            <LabelEntry value={props.mission.mission_name}/>
          </LabelContainer>
          <LabelContainer label="Mission Start" variant="fifty-percent-card card-style-filled">
            <DateTimeEntry timestamp={props.mission.start}/>
          </LabelContainer>
        </Row>
      </Card>
  )
}