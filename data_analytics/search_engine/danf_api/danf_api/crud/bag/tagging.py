"""Operations related to bag tags."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from uuid import UUID

from ..tagging import tag_resource, find_tags
from ...db.mongodb import get_bagfile_collection, get_childbagfile_collection
from ...models.bagtag import ManyBagTags
from ...models.tag import TagQueryParams, Tag


async def tag_bag(guid: UUID, tag: Tag, delete: bool = False) -> None:
    """Add or delete a tag for / from a given bag.

    Args:
        guid: id of the bag
        tag: tag to be added or removed
        delete: delete the tag instead of adding it

    Raises:
        KeyError if the given bag is not found or the given tag is not found when trying to delete it
    """
    for get_collection in [get_childbagfile_collection, get_bagfile_collection]:
        try:
            await tag_resource(collection=get_collection(), guid=guid, tag=tag, delete=delete)
        except KeyError:
            continue
        else:
            return

    raise KeyError(f"No bag was found with guid: {guid}")


async def find_bag_tags(query_params: TagQueryParams) -> ManyBagTags:
    """Get bag tags matching the criteria.

    Args:
        query_params: object holding the query settings

    Returns:
        object holding the matching tags and some extra information about the query as a whole
    """
    offset = query_params.offset
    query_params.offset = 0
    limit = query_params.limit
    query_params.limit = 0

    matching_bagfile_tags = await find_tags(collection=get_bagfile_collection(), query_params=query_params)
    matching_childbagfile_tags = await find_tags(collection=get_childbagfile_collection(), query_params=query_params)
    matching_tags = matching_bagfile_tags.tags + matching_childbagfile_tags.tags

    # drop duplicates (present both in the bagfiles and the child_bagfiles collection)
    matching_tags = list(set(matching_tags))

    # sort
    matching_tags.sort(key=lambda tag: (tag.namespace, tag.value))

    # paginate
    end = None if limit == 0 else offset + limit
    queried_tags = matching_tags[offset:end]

    return ManyBagTags(bagtags=queried_tags, total_count=len(matching_tags))
