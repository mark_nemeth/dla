"""DBConnector"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from functools import wraps
from typing import List

import pymongo
from DANFUtils.logging import logger
from DBConnector.exceptions import InvalidConfigurationException
from DBConnector.handlers.account_collection_handler import \
    AccountCollectionHandler
from DBConnector.handlers.bagfile_collection_handler import \
    BagfileCollectionHandler
from DBConnector.handlers.bva_quota_collection_handler import \
    BvaQuotaCollectionHandler
from DBConnector.handlers.bva_vote_collection_handler import \
    BvaVoteCollectionHandler
from DBConnector.handlers.child_bagfile_collection_handler import \
    ChildBagfileCollectionHandler
from DBConnector.handlers.document_collection_handler import \
    DocumentCollectionHandler
from DBConnector.handlers.drive_collection_handler import \
    DriveCollectionHandler
from DBConnector.handlers.extractor_request_collection_handler import \
    ExtractorRequestCollectionHandler
from DBConnector.handlers.feature_bucket_collection_handler import FeatureBucketCollectionHandler
from DBConnector.handlers.file_catalog_handler import \
    FileCatalogHandler
from DBConnector.handlers.flow_collection_handler import FlowCollectionHandler
from DBConnector.handlers.flow_run_collection_handler import \
    FlowRunCollectionHandler
from DBConnector.handlers.sequence_collection_handler import \
    SequenceCollectionHandler


def ensure_client(func):
    @wraps(func)
    def wrapper_decorator(*args, **kwargs):
        if DBConnector._client is None:
            DBConnector._initialize_client()
        value = func(*args, **kwargs)
        return value

    return wrapper_decorator


class DBConnector:
    """
    DBConnector class for executing simple queries on NoSQL Databases
    """
    _uri = None
    _db_name = None
    _client = None
    _db = None

    @classmethod
    def _initialize_client(cls):
        if cls._client is None:
            logger.debug(
                f"Establishing MongoDB connection to database: {DBConnector._db_name}"
            )
            cls._client = pymongo.MongoClient(cls._uri)
            cls._db = cls._client.get_database(name=cls._db_name)

    def __init__(self,
                 hostname="localhost",
                 port=27017,
                 db_name="",
                 user="",
                 key="",
                 protocol="mongodb",
                 ssl=True,
                 uri=None):
        self.db = None

        self.document_handler = None
        self.bagfile_handler = None
        self.sequence_handler = None
        self.ext_request_handler = None
        self.child_bagfile_handler = None
        self.feature_bucket_handler = None
        self.flow_handler = None
        self.flow_run_handler = None
        self.bva_workstream_handler = None
        self.bva_quota_handler = None
        self.bva_vote_handler = None
        self.account_handler = None
        self.drive_handler = None
        self.filecatalog_handler = None

        # Initialize uri if not provided as an argument
        if uri is None:
            self.hostname = hostname
            self.port = port
            self.db_name = db_name
            self.user = user
            self.key = key
            self.protocol = protocol
            self.ssl = str(ssl).lower()

            assert None not in (self.protocol, self.hostname, self.port,
                                self.db_name, self.user,
                                self.key), "DB connection config invalid!"

            uri = f"{self.protocol}://{self.user}:{self.key}@{self.hostname}:{self.port}/?ssl={self.ssl}"

        # Initialize class variables or check if they match arguments
        if DBConnector._uri is None:
            DBConnector._uri = uri
        elif DBConnector._uri != uri:
            error_message = f"DB Connector URI mismatch error for database {db_name}"
            logger.error(error_message)
            raise InvalidConfigurationException(error_message)
        if DBConnector._db_name is None:
            DBConnector._db_name = db_name
        elif DBConnector._db_name != db_name:
            error_message = f"DB Connector DB name mismatch error for database {db_name}"
            logger.error(error_message)
            raise InvalidConfigurationException(error_message)

    def __enter__(self):
        # Context manager is kept for compatibility but its use is optional
        if DBConnector._client is None:
            DBConnector._initialize_client()
        self.db = DBConnector._client[DBConnector._db_name]
        return self

    def __exit__(self, exc_type, exc_value, tb):
        # Connections are not closed until the service is running
        pass

    @ensure_client
    def add_documents(self, documents):
        """
        Add multiple documents to the db
        :param documents: A list of dicts holding document data
        """
        if self.document_handler is None:
            self.document_handler = DocumentCollectionHandler(DBConnector._db)
        self.document_handler.add(documents)

    @ensure_client
    def get_documents(self, guids):
        """
        Get a list of documents that match the provided criteria
        :param guids: A list of document guids
        :return: A list of dicts holding document data
        """
        if self.document_handler is None:
            self.document_handler = DocumentCollectionHandler(DBConnector._db)
        return self.document_handler.get(guids)

    @ensure_client
    def search_documents(self, query=None, limit=100, offset=0, sortby="_id"):
        """
        Search for documents that match the provided query
        :param query: A query object
        :param limit: Maximum number of documents to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding document data
        """
        if self.document_handler is None:
            self.document_handler = DocumentCollectionHandler(DBConnector._db)
        return self.document_handler.search(query=query,
                                            limit=limit,
                                            offset=offset,
                                            sortby=sortby)

    @ensure_client
    def update_documents(self, guids, document_data, create_if_missing=False):
        """
        Update a list of documents that match the provided criteria
        :param guids: A list of document guids
        :param document_data: A list of dictionaries containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on documents for which the GUID could not be found
        """
        if self.document_handler is None:
            self.document_handler = DocumentCollectionHandler(DBConnector._db)
        self.document_handler.update(guids, document_data, create_if_missing)

    @ensure_client
    def push_to_arrays_in_documents(self,
                                    guids,
                                    array_dicts,
                                    create_if_missing=False):
        """
        Inserts new values into an array in a list of documents. Insert happens at the end of the array
        :param guids: A list of document guids
        :param array_dicts: A list of dictionaries containing the field/values that will be updated. Please consider the
         MongoDB documentation for legal input values https://docs.mongodb.com/manual/reference/operator/update/push/
        :param create_if_missing: If True, perform insert operation on documents for which the GUID could not be found
        :return:
        """
        if self.document_handler is None:
            self.document_handler = DocumentCollectionHandler(DBConnector._db)
        self.document_handler.push_to_arrays_in_documents(
            guids, array_dicts, create_if_missing)

    @ensure_client
    def pop_from_arrays_in_documents(self, guids, array_dicts):
        """
        Deletes the first element in an array in a list of documents.
        :param guids: A list of document guids
        :param array_dicts: A list of dictionaries containing the arrays from which the first element will be dropped
        :return:
        """
        if self.document_handler is None:
            self.document_handler = DocumentCollectionHandler(DBConnector._db)
        self.document_handler.pop_from_array_in_documents(guids, array_dicts)

    @ensure_client
    def delete_documents(self, guids):
        """
        Delete multiple documents that match the provided criteria
        :param guids: A list of document guids
        """
        if self.document_handler is None:
            self.document_handler = DocumentCollectionHandler(DBConnector._db)
        self.document_handler.delete(guids)

    @ensure_client
    def add_bagfiles(self, bagfiles):
        """
        Add multiple BagFile documents to the db
        :param bagfiles: A list of dicts holding BagFile data
        """
        if self.bagfile_handler is None:
            self.bagfile_handler = BagfileCollectionHandler(DBConnector._db)
        self.bagfile_handler.add(bagfiles)

    @ensure_client
    def get_bagfiles(self, guids):
        """
        Get a list of documents containing BagFile data  that match the provided criteria
        :param guids: A list of BagFile guids
        :return: A list of dicts holding BagFile data
        """
        if self.bagfile_handler is None:
            self.bagfile_handler = BagfileCollectionHandler(DBConnector._db)
        return self.bagfile_handler.get(guids)

    @ensure_client
    def search_bagfiles(self,
                        query=None,
                        limit=100,
                        offset=0,
                        sortby="_id",
                        projection=None):
        """
        Search for documents that match the provided query
        :param query: A query object
        :param limit: Maximum number of documents to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :param projection: Projection for result properties
        :return: A list of dicts holding BagFile data
        :raises DBConnectorBaseException: when search error occurs
        """
        if self.bagfile_handler is None:
            self.bagfile_handler = BagfileCollectionHandler(DBConnector._db)
        return self.bagfile_handler.search(query=query,
                                           limit=limit,
                                           offset=offset,
                                           sortby=sortby,
                                           projection=projection)

    @ensure_client
    def update_bagfiles(self, guids, bagfile_data, create_if_missing=False):
        """
        Update a list of documents containing BagFile data that match the provided criteria
        :param guids: A list of BagFile guids
        :param bagfile_data: A list of dictionaries containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on bagfiles for which the GUID could not be found
        """
        if self.bagfile_handler is None:
            self.bagfile_handler = BagfileCollectionHandler(DBConnector._db)
        self.bagfile_handler.update(guids, bagfile_data, create_if_missing)

    @ensure_client
    def delete_bagfiles(self, guids):
        """
        Delete multiple documents containing BagFile data that match the provided criteria
        :param guids: A list of BagFile guids
        """
        if self.bagfile_handler is None:
            self.bagfile_handler = BagfileCollectionHandler(DBConnector._db)
        self.bagfile_handler.delete(guids)

    # Attention: Method naming is misleading but is not (yet)changed for
    # for backward comparability. The purpose of this method is to update
    # a generic field/property of a bagfile document. There is no relationship
    # to the tags-feature. tag is just used as an alias for field/property
    # here
    @ensure_client
    def update_tag(self, bag_guids, values, field, append=False):
        """Generic batch update mechanism that updates a single field on
        multiple bagfile documents.

        :param bag_guids: List of guids identifying the bagfiles to update
        :param values: List of values. values[i] will be set or pushed to the
            specified field of the bagfile with bag_guids[]
        :param field: The field to update
        :param append: if True, the values will be pushed to the field ($push),
            if False, values will override existing values ($set)
        """
        if self.bagfile_handler is None:
            self.bagfile_handler = BagfileCollectionHandler(DBConnector._db)
        self.bagfile_handler.update_field(bag_guids, values, field, append)

    @ensure_client
    def add_sequences(self, sequences):
        """
        Add multiple Sequence documents to the db
        :param sequences: A list of dicts holding Sequence data
        """
        if self.sequence_handler is None:
            self.sequence_handler = SequenceCollectionHandler(DBConnector._db)
        self.sequence_handler.add(sequences)

    @ensure_client
    def get_sequences(self, guids):
        """
        Get a list of documents containing Sequence data that match the provided criteria
        :param guids: A list of sequence guids
        :return: A list of dicts holding Sequence data
        """
        if self.sequence_handler is None:
            self.sequence_handler = SequenceCollectionHandler(DBConnector._db)
        return self.sequence_handler.get(guids)

    @ensure_client
    def get_sequences_by_index(self, bagfile_guid_index_tuples):
        """
        Get a list of documents containing Sequence data that match the provided criteria
        :param bagfile_guid_index_tuples: A list of (bagfile_guid, index) tuples
        :return: A list of dicts holding Sequence data
        """
        if self.sequence_handler is None:
            self.sequence_handler = SequenceCollectionHandler(DBConnector._db)
        return self.sequence_handler.get_by_index(bagfile_guid_index_tuples)

    @ensure_client
    def get_sequences_by_bagfile_guid(self, bagfile_guids):
        """
        Get a list of documents containing Sequence data that match the provided criteria
        :param bagfile_guids: A list of bagfile_guids
        :return: A list of dicts holding Sequence data
        """
        if self.sequence_handler is None:
            self.sequence_handler = SequenceCollectionHandler(DBConnector._db)
        return self.sequence_handler.get_by_bagfile_guid(bagfile_guids)

    @ensure_client
    def update_sequences(self, guids, sequence_data, create_if_missing=False):
        """
        Update a list of documents containing Sequence data that match the provided criteria
        :param guids: A list of sequence guids
        :param sequence_data: A dictionary containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on sequences for which the GUID could not be found
        """
        if self.sequence_handler is None:
            self.sequence_handler = SequenceCollectionHandler(DBConnector._db)
        self.sequence_handler.update(guids, sequence_data, create_if_missing)

    @ensure_client
    def update_sequences_by_index(self,
                                  bagfile_guid_index_tuples,
                                  sequence_data,
                                  upsert=False):
        """
        Update a list of documents containing Sequence data that match the provided criteria
        :param bagfile_guid_index_tuples: A list of (bagfile_guid, index) tuples
        :param sequence_data: A dictionary containing the fields/values that will be updated
        :param upsert: If True, perform update operation on sequences for which the GUID is present
        """
        if self.sequence_handler is None:
            self.sequence_handler = SequenceCollectionHandler(DBConnector._db)
        self.sequence_handler.update_by_index(bagfile_guid_index_tuples,
                                              sequence_data, upsert)

    @ensure_client
    def delete_sequences(self, guids):
        """
        Delete multiple documents containing Sequences data that match the provided criteria
        :param guids: A list of Sequences guids
        """
        if self.sequence_handler is None:
            self.sequence_handler = SequenceCollectionHandler(DBConnector._db)
        self.sequence_handler.delete(guids)

    @ensure_client
    def add_extractor_requests(self, requests):
        """
        Add multiple ExtractorRequest documents to the db
        :param requests: A list of dicts holding ExtractorRequest data
        """
        if self.ext_request_handler is None:
            self.ext_request_handler = ExtractorRequestCollectionHandler(
                DBConnector._db)
        self.ext_request_handler.add(requests)

    @ensure_client
    def get_extractor_requests(self, guids):
        """
        Get a list of documents containing Extractor Request data that match the provided criteria
        :param guids: A list of extractor request guids
        :return: A list of dicts holding Extractor Request data
        """
        if self.ext_request_handler is None:
            self.ext_request_handler = ExtractorRequestCollectionHandler(
                DBConnector._db)
        return self.ext_request_handler.get(guids)

    @ensure_client
    def get_extractor_requests_by_bagfile_guid(self, bagfile_guids):
        """
        Get a list of documents containing Extractor Request data that match the provided criteria
        :param guids: A list of bagfile guids
        :return: A list of dicts holding Extractor Request data
        """
        if self.ext_request_handler is None:
            self.ext_request_handler = ExtractorRequestCollectionHandler(
                DBConnector._db)
        return self.ext_request_handler.get_by_bagfile_guids(bagfile_guids)

    @ensure_client
    def search_extractor_requests(self,
                                  query=None,
                                  limit=100,
                                  offset=0,
                                  sortby="_id"):
        """
        Search for documents that match the provided query
        :param query: A query object
        :param limit: Maximum number of documents to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding document data
        :raises DBConnectorBaseException: when search error occurs
        """
        if self.ext_request_handler is None:
            self.ext_request_handler = ExtractorRequestCollectionHandler(
                DBConnector._db)
        return self.ext_request_handler.search(query=query,
                                               limit=limit,
                                               offset=offset,
                                               sortby=sortby)

    @ensure_client
    def update_extractor_requests(self,
                                  guids,
                                  request_data,
                                  create_if_missing=False):
        """
        Update a list of documents containing ExtractorRequest data that match the provided criteria
        :param guids: A list of extractor request guids
        :param request_data: A dictionary containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on extrequests for which the GUID could not be found
        """
        if self.ext_request_handler is None:
            self.ext_request_handler = ExtractorRequestCollectionHandler(
                DBConnector._db)
        self.ext_request_handler.update(guids, request_data, create_if_missing)

    @ensure_client
    def delete_extractor_requests(self, guids):
        """
        Delete multiple documents containing Extractor request data that match the provided criteria
        :param guids: A list of Extractor Request guids
        """
        if self.ext_request_handler is None:
            self.ext_request_handler = ExtractorRequestCollectionHandler(
                DBConnector._db)
        self.ext_request_handler.delete(guids)

    @ensure_client
    def aggregate_extractor_requests(self, pipeline: List[dict]):
        """
        Apply the provided aggregation pipeline on the extractor_requests collection 
        :param pipeline: A list of aggregation steps
        :return: A list of dicts holding aggregation results
        :raises DBConnectorBaseException: when aggregation error occurs
        """
        if self.ext_request_handler is None:
            self.ext_request_handler = ExtractorRequestCollectionHandler(
                DBConnector._db)
        return self.ext_request_handler.aggregate(pipeline)

    @ensure_client
    def add_child_bagfiles(self, child_bagfiles):
        """
        Add multiple Child BagFile documents to the db
        :param child_bagfiles: A list of dicts holding Child BagFile data
        """
        if self.child_bagfile_handler is None:
            self.child_bagfile_handler = ChildBagfileCollectionHandler(
                DBConnector._db)
        self.child_bagfile_handler.add(child_bagfiles)

    @ensure_client
    def get_child_bagfiles(self, guids):
        """
        Get a list of documents containing Child BagFile data that match the provided criteria
        :param guids: A list of Child BagFile guids
        :return: A list of dicts holding Child BagFile data
        """
        if self.child_bagfile_handler is None:
            self.child_bagfile_handler = ChildBagfileCollectionHandler(
                DBConnector._db)
        return self.child_bagfile_handler.get(guids)

    @ensure_client
    def get_child_bagfiles_by_bagfile_guid(self, bagfile_guids):
        """
        Get a list of documents containing Child BagFile data that match the provided criteria
        :param guids: A list of BagFile guids
        :return: A list of dicts holding Child BagFile data for the Bagfiles
        """
        if self.child_bagfile_handler is None:
            self.child_bagfile_handler = ChildBagfileCollectionHandler(
                DBConnector._db)
        return self.child_bagfile_handler.get_by_bagfile_guids(bagfile_guids)

    @ensure_client
    def search_child_bagfiles(self,
                              query=None,
                              limit=100,
                              offset=0,
                              sortby="_id"):
        """
        Search for documents that match the provided query
        :param query: A query object
        :param limit: Maximum number of documents to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding Child BagFile data
        """
        if self.child_bagfile_handler is None:
            self.child_bagfile_handler = ChildBagfileCollectionHandler(
                DBConnector._db)
        return self.child_bagfile_handler.search(query=query,
                                                 limit=limit,
                                                 offset=offset,
                                                 sortby=sortby)

    @ensure_client
    def update_child_bagfiles(self,
                              guids,
                              child_bagfile_data,
                              create_if_missing=False):
        """
        Update a list of documents containing Child BagFile data that match the provided criteria
        :param guids: A list of Child BagFile guids
        :param child_bagfile_data: A dictionary containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on ChildBag for which the GUID could not be found
        """
        if self.child_bagfile_handler is None:
            self.child_bagfile_handler = ChildBagfileCollectionHandler(
                DBConnector._db)
        self.child_bagfile_handler.update(guids, child_bagfile_data,
                                          create_if_missing)

    @ensure_client
    def delete_child_bagfiles(self, guids):
        """
        Delete multiple documents containing Child BagFile data that match the provided criteria
        :param guids: A list of Child BagFile guids
        """
        if self.child_bagfile_handler is None:
            self.child_bagfile_handler = ChildBagfileCollectionHandler(
                DBConnector._db)
        self.child_bagfile_handler.delete(guids)

    @ensure_client
    def add_feature_buckets(self, feature_buckets):
        """
        Add multiple FeatureBucket documents to the db
        :param feature_buckets: A list of dicts holding FeatureBucket data
        """
        if self.feature_bucket_handler is None:
            self.feature_bucket_handler = FeatureBucketCollectionHandler(
                DBConnector._db)
        self.feature_bucket_handler.add(feature_buckets)

    @ensure_client
    def get_feature_buckets(self, guids):
        """
        Get a list of documents containing FeatureBucket data that match the provided criteria
        :param guids: A list of FeatureBucket guids
        :return: A list of dicts holding FeatureBucket data
        """
        if self.feature_bucket_handler is None:
            self.feature_bucket_handler = FeatureBucketCollectionHandler(
                DBConnector._db)
        return self.feature_bucket_handler.get(guids)

    @ensure_client
    def get_feature_buckets_by_bagfile_guid(self, bagfile_guids):
        """
        Get a list of documents containing FeatureBucket data that match the provided criteria
        :param bagfile_guids: A list of BagFile guids
        :return: A list of dicts holding FeatureBucket data for the Bagfiles
        """
        if self.feature_bucket_handler is None:
            self.feature_bucket_handler = FeatureBucketCollectionHandler(
                DBConnector._db)
        return self.feature_bucket_handler.get_by_bagfile_guids(bagfile_guids)

    @ensure_client
    def update_feature_buckets(self,
                               guids,
                               feature_bucket_data,
                               create_if_missing=False):
        """
        Update a list of documents containing FeatureBucket data that match the provided criteria
        :param guids: A list of FeatureBucket guids
        :param feature_bucket_data: A dictionary containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on FeatureBucket for which the GUID could not be found
        """
        if self.feature_bucket_handler is None:
            self.feature_bucket_handler = FeatureBucketCollectionHandler(
                DBConnector._db)
        self.feature_bucket_handler.update(guids, feature_bucket_data,
                                           create_if_missing)

    @ensure_client
    def delete_feature_buckets(self, guids):
        """
        Delete multiple documents containing FeatureBucket data that match the provided criteria
        :param guids: A list of FeatureBucket guids
        """
        if self.feature_bucket_handler is None:
            self.feature_bucket_handler = FeatureBucketCollectionHandler(
                DBConnector._db)
        self.feature_bucket_handler.delete(guids)

    @ensure_client
    def search_feature_buckets(self,
                               query=None,
                               limit=100,
                               offset=0,
                               sortby="_id",
                               projection=None):
        """
        Search for documents that match the provided query
        :param query: A query object
        :param limit: Maximum number of documents to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :param projection: Projection for result properties
        :return: A list of dicts holding FeatureBucketdata
        :raises DBConnectorBaseException: when search error occurs
        """
        if self.feature_bucket_handler is None:
            self.feature_bucket_handler = FeatureBucketCollectionHandler(
                DBConnector._db)
        return self.feature_bucket_handler.search(query=query,
                                                  limit=limit,
                                                  offset=offset,
                                                  sortby=sortby,
                                                  projection=projection)

    @ensure_client
    def aggregate_feature_buckets(self, pipeline: List[dict]):
        """
        Apply the provided aggregation pipeline on the feature_bucket collection
        :param pipeline: A list of aggregation steps
        :return: A list of dicts holding aggregation results
        :raises DBConnectorBaseException: when aggregation error occurs
        """
        if self.feature_bucket_handler is None:
            self.feature_bucket_handler = FeatureBucketCollectionHandler(
                DBConnector._db)
        return self.feature_bucket_handler.aggregate(pipeline)

    @ensure_client
    def add_drives(self, drives):
        """
        Add multiple Drive documents to the db
        :param drives: A list of dicts holding Drive data
        """
        if self.drive_handler is None:
            self.drive_handler = DriveCollectionHandler(DBConnector._db)
        self.drive_handler.add(drives)

    @ensure_client
    def get_distinct_field_values(self, collection: str, field: str):
        handler = self.get_collection_handler(collection)
        return handler.get_distinct_field_values(field)

    @ensure_client
    def get_max_field_value(self, collection: str, field: str):
        handler = self.get_collection_handler(collection)
        return handler.get_max_field_value(field)

    @ensure_client
    def get_min_field_value(self, collection: str, field: str):
        handler = self.get_collection_handler(collection)
        return handler.get_min_field_value(field)

    def get_collection_handler(self, collection):
        if collection == 'drives':
            if self.drive_handler is None:
                self.drive_handler = DriveCollectionHandler(DBConnector._db)
            handler = self.drive_handler
        elif collection == 'bagfiles':
            if self.bagfile_handler is None:
                self.bagfile_handler = BagfileCollectionHandler(DBConnector._db)
            handler = self.bagfile_handler
        elif collection == 'childbagfiles':
            if self.child_bagfile_handler is None:
                self.child_bagfile_handler = ChildBagfileCollectionHandler(DBConnector._db)
            handler = self.child_bagfile_handler
        return handler

    @ensure_client
    def get_drives(self, guids):
        """
        Get a list of documents containing Drive data that match the provided criteria
        :param guids: A list of Drive guids
        :return: A list of dicts holding Drive data
        """
        if self.drive_handler is None:
            self.drive_handler = DriveCollectionHandler(DBConnector._db)
        return self.drive_handler.get(guids)

    @ensure_client
    def search_drives(self,
                      query=None,
                      limit=100,
                      offset=0,
                      sortby="_id",
                      projection=None):
        """
        Search for documents that match the provided query
        :param query: A query object
        :param limit: Maximum number of documents to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding Drive data
        """
        if self.drive_handler is None:
            self.drive_handler = DriveCollectionHandler(DBConnector._db)
        return self.drive_handler.search(query=query,
                                         limit=limit,
                                         offset=offset,
                                         sortby=sortby,
                                         projection=projection)

    @ensure_client
    def update_drives(self, guids, drive_data, create_if_missing=False):
        """
        Update a list of documents containing Drive data that match the provided criteria
        :param guids: A list of Drive guids
        :param drive_data: A list of dictionaries containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on drives for which the GUID could not be found
        """
        if self.drive_handler is None:
            self.drive_handler = DriveCollectionHandler(DBConnector._db)
        self.drive_handler.update(guids, drive_data, create_if_missing)

    @ensure_client
    def delete_drives(self, guids):
        """
        Delete multiple documents containing Drive data that match the provided criteria
        :param guids: A list of BagFile guids
        """
        if self.drive_handler is None:
            self.drive_handler = DriveCollectionHandler(DBConnector._db)
        self.drive_handler.delete(guids)

    @ensure_client
    def get_flows(self, guids):
        """Retrieve all flow documents identified by any of the given guids.

        :param guids: List of guids identifying flows
        :return: list of dicts representing flow documents
        """
        if self.flow_handler is None:
            self.flow_handler = FlowCollectionHandler(DBConnector._db)
        return self.flow_handler.get(guids)

    @ensure_client
    def search_flows(self, query=None, limit=100, offset=0, sortby="_id"):
        """Search for flows that match the provided query.

        :param query: A query object
        :param limit: Maximum number of flows to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding flow data
        """
        if self.flow_handler is None:
            self.flow_handler = FlowCollectionHandler(DBConnector._db)
        return self.flow_handler.search(query=query,
                                        limit=limit,
                                        offset=offset,
                                        sortby=sortby)

    @ensure_client
    def add_catalog(self, catalog):
        """
        Add catalogs for file transfer
        :param catalog: A list of File catalogs
        """
        if self.filecatalog_handler is None:
            self.filecatalog_handler = FileCatalogHandler(DBConnector._db)
        self.filecatalog_handler.add(catalog)

    @ensure_client
    def get_catalog_all(self, guid):
        """
        Fetch file catalogs
        :param guids: A list of File guids
        """
        if self.filecatalog_handler is None:
            self.filecatalog_handler = FileCatalogHandler(DBConnector._db)
        return self.filecatalog_handler.get_all(guid)

    @ensure_client
    def get_catalog(self, file_guid, url_status):
        """
        Fetch file catalogs based on guid and status
        :param guids: Object containing file guid and status
        """
        if self.filecatalog_handler is None:
            self.filecatalog_handler = FileCatalogHandler(DBConnector._db)
        return self.filecatalog_handler.get(file_guid, url_status)

    @ensure_client
    def update_catalog(self, guids, catalog_data, create_if_missing=False):
        """
        Update a list of catalogs containing catalog data that match the update'ble fields

        :param guids: A list of File guids
        :param catalog_data: A dictionary containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on ChildBag for which the GUID could not be found
        """
        if self.filecatalog_handler is None:
            self.filecatalog_handler = FileCatalogHandler(DBConnector._db)
        self.filecatalog_handler.update(guids, catalog_data, create_if_missing)

    @ensure_client
    def delete_catalog(self, guid: str) -> int:
        if self.filecatalog_handler is None:
            self.filecatalog_handler = FileCatalogHandler(DBConnector._db)
        return self.filecatalog_handler.delete(guid)

    @ensure_client
    def update_catalog_url_status(self,
                                  guid,
                                  url_id,
                                  status,
                                  last_changed,
                                  create_if_missing=False):
        """
        Update a list of catalogs containing catalog data that match the update'ble fields

        :param guids: A list of File guids
        :param catalog_data: A dictionary containing the fields/values that will be updated
        :param create_if_missing: If True, perform insert operation on ChildBag for which the GUID could not be found
        """
        if self.filecatalog_handler is None:
            self.filecatalog_handler = FileCatalogHandler(DBConnector._db)
        self.filecatalog_handler.update_url_status(guid,
                                                   url_id,
                                                   status,
                                                   last_changed,
                                                   create_if_missing)

    def search_catalogs(self,
                        query=None,
                        limit=100,
                        offset=0,
                        sortby="file_guid"):
        """Search for catalogs that match the provided query.

        :param query: A query object
        :param limit: Maximum number of catalogs to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding accounts data
        """
        if self.filecatalog_handler is None:
            self.filecatalog_handler = FileCatalogHandler(DBConnector._db)
        return self.filecatalog_handler.search(query, limit, offset, sortby)

    @ensure_client
    def add_flows(self, flows):
        """Add multiple flow documents to the db.

        :param flows: List of dicts representing the flow documents
        """
        if self.flow_handler is None:
            self.flow_handler = FlowCollectionHandler(DBConnector._db)
        self.flow_handler.add(flows)

    @ensure_client
    def delete_flows_by_guids(self, guids):
        """Delete all flow documents identified by any of the given guids.

        :param guids: List of guids identifying flows
        """
        if self.flow_handler is None:
            self.flow_handler = FlowCollectionHandler(DBConnector._db)
        self.flow_handler.delete_by_guids(guids)

    @ensure_client
    def get_flow_runs(self, guids):
        """Retrieve all flow run documents identified by any of the given
        guids.

        :param guids: List of guids identifying flow runs
        :return: list of dicts representing flow run documents
        """
        if self.flow_handler is None:
            self.flow_handler = FlowRunCollectionHandler(DBConnector._db)
        return self.flow_handler.get(guids)

    @ensure_client
    def search_flow_runs(self, query=None, limit=100, offset=0, sortby="_id"):
        """Search for flow runs that match the provided query.

        :param query: A query object
        :param limit: Maximum number of flow runs to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding flow data
        """
        if self.flow_run_handler is None:
            self.flow_run_handler = FlowRunCollectionHandler(DBConnector._db)
        return self.flow_run_handler.search(query=query,
                                            limit=limit,
                                            offset=offset,
                                            sortby=sortby)

    @ensure_client
    def add_flow_runs(self, flows):
        """Add multiple flow run documents to the db.

        :param flows: List of dicts representing the flow run documents
        """
        if self.flow_run_handler is None:
            self.flow_run_handler = FlowRunCollectionHandler(DBConnector._db)
        self.flow_run_handler.add(flows)

    @ensure_client
    def update_flow_runs(self, guids, field_updates, create_if_missing=False):
        """Performs multiple update operations on multiple flow run documents.

        :param guids: List of guids identifying flows
        :param field_updates: List of dicts representing the desired updates.
            updated field_updates[i] will be performed on the flow run
            identified by guids[i].
            field_updates[i] is a dict where keys identify fields of the
            document and values the values that values that those fields should
            be set to. Each field_updates[i] should be a valid input for the
            mongo DB $set update operator
        :param create_if_missing: set to true for upsert
        """
        if self.flow_run_handler is None:
            self.flow_run_handler = FlowRunCollectionHandler(DBConnector._db)
        self.flow_run_handler.update(guids, field_updates, create_if_missing)

    @ensure_client
    def delete_flow_runs_by_guids(self, guids):
        """Delete all flow run documents identified by any of the given guids.

        :param guids: List of guids identifying flow runs
        """
        if self.flow_run_handler is None:
            self.flow_run_handler = FlowRunCollectionHandler(DBConnector._db)
        self.flow_run_handler.delete_by_guids(guids)

    @ensure_client
    def add_flow_run_step_run(self, guid, step_index, run):
        """Adds a step run sub-document to a step of a flow run document.

        :param guid: identifies the flow run to update
        :param step_index: identifies the step within the flow run to update
            by its index in the step array
        :param run: dict representing the step run sub-document to add
        """
        if self.flow_run_handler is None:
            self.flow_run_handler = FlowRunCollectionHandler(DBConnector._db)
        self.flow_run_handler.add_step_run(guid, step_index, run)

    @ensure_client
    def add_quota(self, voting_quota):
        """
        Adds a new quota document for the BVA rating
        :param voting_quota:
        :return:
        """
        if self.bva_quota_handler is None:
            self.bva_quota_handler = BvaQuotaCollectionHandler(DBConnector._db)
        return self.bva_quota_handler.add(voting_quota)

    @ensure_client
    def get_quotas(self, active):
        """
        Retrieve all quotas depending if you want the active or inactive quotas
        :param active: identifies if you want all active or inactive quotas
        :return: List of quotas
        """
        if self.bva_quota_handler is None:
            self.bva_quota_handler = BvaQuotaCollectionHandler(DBConnector._db)
        return self.bva_quota_handler.get(active)

    @ensure_client
    def get_votes(self, active_voting_id):
        """
        Retrieve all votes for a voting round
        :param active_voting_id: the guid of the quota document for the voting round
        :return: List of votes
        """
        if self.bva_vote_handler is None:
            self.bva_vote_handler = BvaVoteCollectionHandler(DBConnector._db)
        return self.bva_vote_handler.get(active_voting_id)

    @ensure_client
    def add_vote(self, vote):
        """
         Adds a new vote document for the BVA rating
        :param vote: Vote document to be added to the database
        :return: updated quota document
        """
        if self.bva_vote_handler is None:
            self.bva_vote_handler = BvaVoteCollectionHandler(DBConnector._db)
        return self.bva_vote_handler.add(vote)

    @ensure_client
    def delete_vote_by_guid(self, guid):
        """
        Deletes a vote by guid
        :param guid: guid of the vote to delete
        :return: updated quota document
        """
        if self.bva_vote_handler is None:
            self.bva_vote_handler = BvaVoteCollectionHandler(DBConnector._db)
        return self.bva_vote_handler.delete_by_guid(guid)

    @ensure_client
    def get_vote_by_guid(self, guid):
        """
        Retrieves a specific vote by its guid
        :param guid: guid of the vote
        :return: vote document
        """
        if self.bva_vote_handler is None:
            self.bva_vote_handler = BvaVoteCollectionHandler(DBConnector._db)
        return self.bva_vote_handler.get_by_guid(guid)

    @ensure_client
    def find_and_update_vote(self, filter_query, update_request):
        """
        Finds and updated a given vote
        :param filter_query: query to find the specific vote
        :param update_request: update request to update vote
        :return: returns updated vote
        """
        if self.bva_vote_handler is None:
            self.bva_vote_handler = BvaVoteCollectionHandler(DBConnector._db)
        return self.bva_vote_handler.find_and_update_vote(
            filter_query, update_request)

    @ensure_client
    def search_votes(self, query=None, limit=100, offset=0, sortby="guid"):
        """Search for votes that match the provided query.

        :param query: A query object
        :param limit: Maximum number of accounts to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding vote data
        """
        if self.bva_vote_handler is None:
            self.bva_vote_handler = BvaVoteCollectionHandler(DBConnector._db)
        return self.bva_vote_handler.search(query, limit, offset, sortby)

    @ensure_client
    def find_and_update_quota(self, filter_query, update_request):
        """
        Finds and updated a given quota
        :param filter_query: query to find the specific quota
        :param update_request: update request to update quota
        :return: returns updated quota
        """
        if self.bva_quota_handler is None:
            self.bva_quota_handler = BvaQuotaCollectionHandler(DBConnector._db)
        return self.bva_quota_handler.find_one_and_update(
            filter_query, update_request)

    @ensure_client
    def add_accounts(self, accounts):
        """Add multiple account documents to the db.

        :param accounts: List of dicts representing the account documents
        """
        if self.account_handler is None:
            self.account_handler = AccountCollectionHandler(DBConnector._db)
        return self.account_handler.add(accounts)

    @ensure_client
    def search_accounts(self, query=None, limit=100, offset=0, sortby="guid"):
        """Search for accounts that match the provided query.

        :param query: A query object
        :param limit: Maximum number of accounts to return
        :param offset: Offset of query results for batching/paging
        :param sortby: Specifies the ordering of the results
        :return: A list of dicts holding accounts data
        """
        if self.account_handler is None:
            self.account_handler = AccountCollectionHandler(DBConnector._db)
        return self.account_handler.search(query, limit, offset, sortby)

    @ensure_client
    def update_accounts(self, guids, field_updates, create_if_missing=False):
        """Performs multiple update operations on multiple account
        documents.

        :param guids: List of guids identifying accounts
        :param field_updates: List of dicts representing the desired updates.
            updated field_updates[i] will be performed on the account
            identified by guids[i].
            field_updates[i] is a dict where keys identify fields of the
            document and values the values that values that those fields should
            be set to. Each field_updates[i] should be a valid input for the
            mongo DB $set update operator
        :param create_if_missing: set to true for upsert
        """
        if self.account_handler is None:
            self.account_handler = AccountCollectionHandler(DBConnector._db)
        return self.account_handler.update(guids, field_updates,
                                           create_if_missing)
