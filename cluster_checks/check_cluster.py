#!/usr/bin/python

from test_landing import *
from test_symlinks import *

import argparse


paths = { "hks": "/mapr/738.mbc.de/data/landing/rd/athena/08_robotaxi",
          "sv" : "/media/cluster_sv_rw/data/landing/rd/athena/08_robotaxi",
          "ulm": "/mapr/059-ul0.mbc.de/data/landing/rd/athena/08_robotaxi"
        }

parser = argparse.ArgumentParser()
group = parser.add_argument_group('tests')
group.add_argument('-l', '--landing', action='store_true', help="Check for data in Landing zone")
group.add_argument('-s', '--symlink', action='store_true', help="Check for unwanted symlinks")
parser.add_argument('-c', '--cluster', nargs='+', choices=paths.keys(), help="specify a subset of clusters to apply tests on")
args = parser.parse_args()

if args.cluster:
    cluster_paths = dict((key, paths[key]) for key in args.cluster)
else:
    cluster_paths = paths

if args.landing:
    for i in cluster_paths.keys():
        LandingTest(cluster_paths[i]).run()

if args.symlink:
    for i in cluster_paths.keys():
        FindSymlinks(cluster_paths[i]).run()
