"""Search Engine REST API bagfile search route (including feature queries)"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from typing import List, Tuple

from DANFUtils.constants import constants
from DANFUtils.tracing import tracing
from tornado.escape import recursive_unicode

from handlers.auth import auth
from handlers.bagfile_search_handler import BagfileSearchHandler
from handlers.feature_search_handler import FeatureSearchHandler
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler

logger = logging.getLogger(__name__)

DEFAULT_RES_LIMIT = 50


class BagfileFeatureRequestHandler(BaseRequestHandler):
    def initialize(self):
        self.bag_search_handler = BagfileSearchHandler()
        self.feature_search_handler = FeatureSearchHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, GET')

    @auth.requires_entitlements(["openid"])
    def get(self):
        tracing.start_task_in_context('bagfilefeaturesearch/get',
                                      context_headers=self.request.headers)

        # Prepare kwargs
        bag_search_kwargs, feat_search_kwargs = self.prepare_query_args(
            self.request.query_arguments)

        # check if feature search is necessary
        if len(feat_search_kwargs["query"]) > 0:
            res, total = self.bag_search_with_features(bag_search_kwargs,
                                                       feat_search_kwargs)
        else:
            res, total = self.bag_search(bag_search_kwargs)

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"],
                             total=total)

    def prepare_query_args(self, query_arguments) -> Tuple[dict, dict]:
        u_args = recursive_unicode(query_arguments)
        bag_search_kwargs = self.bag_search_handler.create_search_kwargs(
            u_args)

        # remove start/end/guid from feature query, these keys are already part of the bag query and can cause errors here
        key_blacklist = [
            "start[lte]", "start[gte]", "end[lte]", "end[gte]", "guid",
            "bagfile_guid"
        ]
        for k in key_blacklist:
            if k in u_args:
                del u_args[k]
        feat_search_kwargs = self.feature_search_handler.create_search_kwargs(
            u_args)
        return bag_search_kwargs, feat_search_kwargs

    def bag_search(self, bag_search_kwargs: dict):
        logger.info(f"Executing bagfile search query: {bag_search_kwargs}")
        return self.bag_search_handler.perform_search(**bag_search_kwargs)

    def bag_search_with_features(self, bag_search_kwargs: dict,
                                 feat_search_kwargs: dict):
        res = []
        total = 0
        # save limits/offset
        if "limit" in self.request.query_arguments:
            res_limit = abs(int(self.request.query_arguments["limit"][0]))
        else:
            res_limit = DEFAULT_RES_LIMIT
        if "offset" in self.request.query_arguments:
            res_offset = abs(int(self.request.query_arguments["offset"][0]))
        else:
            res_offset = 0

        # replace limits/offset to get all bagfiles
        bag_search_kwargs["limit"] = 0
        bag_search_kwargs["offset"] = 0
        bag_search_kwargs["projection"] = {"guid": 1}

        # get all matching bagfiles from bagfiles collection (guids only)
        logger.info(
            f"Executing bagfile search query (get matching guids from bagfiles collection): {bag_search_kwargs}"
        )
        bag_res, bag_total = self.bag_search_handler.perform_search(
            **bag_search_kwargs)

        if len(bag_res) > 0:
            # get all matching bagfiles from feature bucket collection (guids only)
            guids_matching_bag_params = list(set([f["guid"] for f in bag_res]))
            guids_res, total = self.feature_search_handler.perform_search_with_aggregation(
                feat_search_kwargs['query']['features'],
                guids_matching_bag_params, res_offset, res_limit)

            if len(guids_res) > 0:
                bag_search_kwargs["projection"] = {"_id": 0}
                bag_search_kwargs["query"]["guid"] = {"$in": guids_res}
                # get all matching bagfiles from feature bucket collection (all data)
                res, t = self.bag_search_handler.perform_search(
                    **bag_search_kwargs)
        return res, total
