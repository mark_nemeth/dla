import storeProvider from '../../models/storeProvider';
import {isUserPermitted} from './permissonHelper';

describe('permissionHelper', () => {

  it('should return true if user has permission', () => {
    storeProvider.getUserDataAsync = jest.fn(() => {
      return Promise.resolve({displayName: 'danfuser0@gmail.com'})
    });

    return isUserPermitted('123').then(permitted => {
      return expect(permitted).toBeTruthy();
    });
  });

  it('should return false if user has no permission', () => {
    storeProvider.getUserDataAsync = jest.fn(() => {
      return Promise.resolve({displayName: 'danfuser3@gmail.com'})
    });

    return isUserPermitted('123').then(permitted => {
      return expect(permitted).toBeFalsy();
    });
  });
});