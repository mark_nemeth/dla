import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import 'react-input-range/lib/css/index.css';
import './ByLaneChangeFilter.scss';


export function ByLaneChangeFilter(props) {

  function onLaneChangeSelect() {
    props.filterByLaneChange(props.selected ? 0 : 1);
  }

  function resetRangeSelection() {
    props.filterByLaneChange(0);
  }

  const active = props.selected;

  return (
    <CollapseContainer label="Lane changes"
                       active={active}
                       resetCallback={resetRangeSelection}
    >
      <div className="lane-change-container">
        <label>
            <input type='checkbox' value={active}
             onChange={event => onLaneChangeSelect()}
             checked={active}/>
                &nbsp; contains lane change(s)
        </label>
      </div>
    </CollapseContainer>
  );
}