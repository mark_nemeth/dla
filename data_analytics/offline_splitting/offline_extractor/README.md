# Offline Extractor
# Current version : 0.0.2

## Usage
### Run in Docker:
Build docker image:
```console
docker build --network host -t offline_extractor:0.0.2 . 
```

Execution:
```console
docker run -it -v /mapr/738.mbc.de/data/input/rd/athena/08_robotaxi:/input -v $PWD:/output -e BAGFILE_PATH=/input/plog/WDD2221591A411952/2019/05/27/20190527_150227_5214_104-12_CTD_Mission-3_3_ORIGINAL_STRIPPED_cid_bageval.bag -e OFFLINE_OUTPUT_PATH=/output/ --network host offline_extractor:0.0.2
```
* Change $PWD to the path of the folder where you want to save the json file!