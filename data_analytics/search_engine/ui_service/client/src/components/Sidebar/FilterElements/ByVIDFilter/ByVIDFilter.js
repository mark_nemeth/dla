import React from 'react';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import ScrollList from '../../../Utils/ScrollList/ScrollList';
import './ByVIDFilter.scss'

export function ByVIDFilter(props) {

  function onVidSelect(vid) {
    if (props.selectedVidList.includes(vid)) props.filterByVIDs(props.selectedVidList.filter(t => t !== vid));
    else props.filterByVIDs(props.selectedVidList.concat(vid));
  }

  function resetVehicleList() {
    props.filterByVIDs([]);
  }

  const showLoading = props.vehicleIDList === undefined || props.vehicleIDList.length <= 0;
  const subLabel = props.selectedVidList.length + ' selected';
  const active = props.selectedVidList.length > 0;

  const listItemComponent = (item) => (
    <label className="vid-list-item">
      <input type="checkbox" value={item}
             onChange={event => onVidSelect(event.target.value)}
             checked={props.selectedVidList.includes(item)}/>
      &nbsp; {item}
    </label>
  );

  return (
    <CollapseContainer label={active > 0 ? "Vehicle: " + subLabel : "Vehicle"}
                       active={active}
                       variant="list"
                       resetCallback={resetVehicleList}
    >
      <ScrollList
        showLoading={showLoading}
        itemList={props.vehicleIDList}
        listItemComponent={listItemComponent}
      />
    </CollapseContainer>
  );
}