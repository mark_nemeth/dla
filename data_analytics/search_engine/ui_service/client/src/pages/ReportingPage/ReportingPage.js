import React, { useEffect } from 'react';

import ErrorBanner from '../../components/ErrorBanner';
import './ReportingPage.scss'
import { Container, Row, Col } from 'react-bootstrap';
import { DateSelector } from '../../components/Reporting/DateSelector/DateSelector';
import Button from 'react-bootstrap/Button';
import { dateToTimestampConverter } from '../../utils/valueConverter';
import { DropDownSelector } from '../../components/Reporting/DropDownSelector/DropDownSelector';
import { FieldList } from '../../components/Reporting/FieldList/FieldList';
import { TableList } from '../../components/Reporting/TableList/TableList';
import Spinner from 'react-bootstrap/Spinner';
import { DropDownMultiSelector } from '../../components/Reporting/DropDownMultiSelector/DropDownMultiSelector';

export function ReportingPage(props) {

  useEffect(() => {
    document.title = 'DANF - Reporting';
  }, []);

  function onLocationSelect(selectedLocation) {
    props.selectReportingLocation({
      location: selectedLocation
    });
  }

  function onReportTypeSelect(selectedReportType) {
    props.selectReportType({
      reportType: selectedReportType
    })
  }

  function onStartDateSelection(date) {
    props.selectReportingDate({
      startDate: dateToTimestampConverter(date),
      endDate: props.selectedEndDate
    })
  }

  function onEndDateSelection(date) {
    props.selectReportingDate({
      startDate: props.selectedStartDate,
      endDate: dateToTimestampConverter(date, true)
    })
  }

  function onTripFiltersSelect(filterIdx) {
    props.setTripFilterSelection({
      idx: filterIdx,
      value: !props.tripFilters[filterIdx].selected
    })
  }
  const showLoading = props.error === false && props.showLoading;

  return (
    <div className="reporting-page">
      <div className="reporting-page-action-bar">
        <DropDownSelector
          items={props.reportTypes}
          selectedItem={props.selectedReportType}
          prefix="Format: "
          onSelect={onReportTypeSelect}
        />
        <DropDownSelector
          items={props.locations}
          selectedItem={props.selectedLocation}
          prefix="Location: "
          onSelect={onLocationSelect}
        />
        <DropDownMultiSelector
          label="Filters"
          items={props.tripFilters}
          onSelect={onTripFiltersSelect}
        />
        <DateSelector
          start={props.selectedStartDate}
          end={props.selectedEndDate}
          onStartDateSelection={onStartDateSelection}
          onEndDateSelection={onEndDateSelection}
        />
        <Button className="create-report-btn btn-info" onClick={props.createReport} disabled={props.showLoading}>Create report</Button>
        <Button className="export-excel-btn btn-info" onClick={props.exportReport} disabled={!props.enableExport || !props.showReport}>Export as .xlsx</Button>
      </div>
      <Container fluid className="reporting-page-content">
        {showLoading && (
          <div className="loading-spinner-container">
            <Spinner animation="border" variant="info" />
          </div>
        )}
        {props.showReport && (
          <Row className='mb-12'>
            <Col md={9} lg={9} xl={9} className="col-xxl-9 col-xxxl-9 col-xxxxl-9">
              <div className="reporting-page-tables-section">
                {props.aggregationTables.length > 0 ? <TableList tables={props.aggregationTables} /> : null}
                {props.simpleTables.length > 0 ? <TableList tables={props.simpleTables} /> : null}
              </div>
            </Col>
            <Col md={3} lg={3} xl={3} className="col-xxl-3 col-xxxl-3 col-xxxxl-3">
              <div className="reporting-page-kpi-section">
                {props.kpis.length > 0 ? <FieldList fields={props.kpis} /> : null}
              </div>
            </Col>
          </Row>
        )}
      </Container>
      <ErrorBanner />
    </div>
  );
}
