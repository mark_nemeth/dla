#!/bin/sh

if [ -z "$BAGFILE_PATH" ]; then
    echo "Need to set BAGFILE_PATH !"
    exit 1
fi

echo "BAGFILE_PATH is set to: $BAGFILE_PATH"

python3 -u index.py