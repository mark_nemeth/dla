"""Handler class for db"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
from typing import List

from DANFUtils.exceptions import GUIDNotFoundException
from DBConnector.db_connector import DBConnector
from config.config_handler import config_dict
from model.accounts import Account
from model.exceptions import GuidNotFoundError

logger = logging.getLogger(__name__)


class DBDataHandler:
    def __init__(self):
        ssl_enabled = config_dict.get_value("db_ssl", "True") == "True"

        self.connector = DBConnector(
            hostname=config_dict.get_value("db_hostname"),
            port=config_dict.get_value("db_port"),
            db_name=config_dict.get_value("db_name"),
            user=config_dict.get_value("db_user"),
            key=config_dict.get_value("db_key"),
            protocol=config_dict.get_value("db_protocol"),
            ssl=ssl_enabled)

    def add_child_bagfiles(self, child_bagfile_data):
        with self.connector as db:
            db.add_child_bagfiles(child_bagfile_data)

    def get_sequences_by_bagfile_guid(self, bagfile_guid):
        """
        Get all sequences from the DB that belong to the specified bagfile.

        :param bagfile_guid:
        :return: list of sequences
        """
        sequences = []
        with self.connector as db:
            sequences = [*db.get_sequences_by_bagfile_guid([bagfile_guid])]
        return sequences

    def get_bagfiles(self, guids):
        bagfiles = []
        with self.connector as db:
            bagfiles = [*db.get_bagfiles(guids)]

        if len(bagfiles) == len(guids) and all(
            [x['guid'] in guids for x in bagfiles]):
            return bagfiles
        else:
            raise GUIDNotFoundException(
                'SearchAPI', guids,
                'Bagfile(s) could not be retrieved: bagfile guid(s) not found in db'
            )

    def search_bagfiles(self,
                        query=None,
                        limit=100,
                        offset=0,
                        sortby='guid',
                        projection=None):
        results, total = [], 0
        with self.connector as db:
            cursor = db.search_bagfiles(query=query,
                                        limit=0,
                                        offset=offset,
                                        sortby=sortby,
                                        projection=projection)
            total = cursor.count()
            results = [*cursor.limit(limit)]
        return results, total

    def search_child_bagfiles(self,
                              query=None,
                              limit=100,
                              offset=0,
                              sortby='guid'):
        results, total = [], 0
        with self.connector as db:
            cursor = db.search_child_bagfiles(query=query,
                                              limit=0,
                                              offset=offset,
                                              sortby=sortby)
            total = cursor.count()
            results = [*cursor.limit(limit)]
        return results, total

    def search_feature_buckets(self,
                               query: dict = None,
                               limit: int = 0,
                               offset: int = 0,
                               sortby: str = 'guid',
                               projection: dict = None) -> (list, int):
        results, total = [], 0
        with self.connector as db:
            cursor = db.search_feature_buckets(query=query,
                                               limit=limit,
                                               offset=offset,
                                               sortby=sortby,
                                               projection=projection)
            total = cursor.count()
            if limit == 0:
                results = [*cursor]
            else:
                results = [*cursor.limit(limit)]
        return results, total

    def search_features(self, query: dict, projection: dict) -> list:
        results = []
        with self.connector as db:
            cursor = db.search_feature_buckets(query=query,
                                               projection=projection)

            results = [*cursor]
        return results

    def search_drives(self,
                      query=None,
                      limit=100,
                      offset=0,
                      sortby='start',
                      projection=None):
        results, total = [], 0
        with self.connector as db:
            cursor = db.search_drives(query=query,
                                      limit=0,
                                      offset=offset,
                                      sortby=sortby,
                                      projection=projection)

            total = cursor.count()
            results = [*cursor.limit(limit)]
        return results, total

    def search_extractor_requests(self,
                                  query=None,
                                  limit=100,
                                  offset=0,
                                  sortby='guid'):
        results = []
        with self.connector as db:
            cursor = db.search_extractor_requests(query=query,
                                                  limit=limit,
                                                  offset=offset,
                                                  sortby=sortby)
            results = [*cursor]
        return results

    def search_flows(self, query=None, limit=100, offset=0, sortby='guid'):
        results = []
        with self.connector as db:
            cursor = db.search_flows(query, limit, offset, sortby)
            results = [*cursor]
        return results

    def search_flow_runs(self, query=None, limit=100, offset=0, sortby='guid'):
        results = []
        with self.connector as db:
            cursor = db.search_flow_runs(query, limit, offset, sortby)
            results = [*cursor]
        return results

    def search_catalogs(self, query=None, limit=100, offset=0, sortby='guid'):
        results, total = [], 0
        with self.connector as db:
            cursor = db.search_catalogs(query=query,
                                        limit=0,
                                        offset=offset,
                                        sortby=sortby)
            total = cursor.count()
            results = [*cursor.limit(limit)]
        return results, total

    def search_votes(self, query=None, limit=100, offset=0, sortby='guid'):
        results, total = [], 0
        with self.connector as db:
            cursor = db.search_votes(query=query,
                                     limit=0,
                                     offset=offset,
                                     sortby=sortby)
            total = cursor.count()
            results = list(cursor.limit(limit))
        return results, total

    def get_all_topics(self):
        return self.get_cached_values('topics_cache', 'topics')

    def get_all_vins(self):
        return self.get_cached_values('bagfiles_vehicle_id_num_cache')

    def get_all_location(self):
        return self.get_cached_values('bagfiles_origin_cache')

    def get_bagfile_duration_min_max(self):
        with self.connector as db:
            duration_document_list = list(
                db.search_documents(query={'type': 'bagfiles_duration_cache'}))

        if not duration_document_list:
            logger.info(
                "No duration cache found for the bagfile, returning default values"
            )
            return [0, 28800000]
        else:
            results = [
                duration_document_list[0].get("min", 0),
                duration_document_list[0].get("max", 28800000)
            ]
        return results

    def get_childbagfile_duration_min_max(self):
        with self.connector as db:
            duration_document_list = list(
                db.search_documents(
                    query={'type': 'childbagfiles_duration_cache'}))

        if not duration_document_list:
            logger.info(
                "No duration cache found for the childbagfile, returning default values"
            )
            return [0, 28800000]
        else:
            return [
                duration_document_list[0].get("min", 0),
                duration_document_list[0].get("max", 28800000)
            ]

    def get_drive_duration_min_max(self):
        with self.connector as db:
            duration_document_list = list(
                db.search_documents(query={'type': 'drives_duration_cache'}))

        if not duration_document_list:
            logger.info(
                "No duration cache found for the drive, returning default values"
            )
            return [0, 28800000]
        else:
            results = [
                duration_document_list[0].get("min", 0),
                duration_document_list[0].get("max", 28800000)
            ]
        return results

    def get_bagfile_size_min_max(self):
        with self.connector as db:
            size_document_list = list(
                db.search_documents(query={'type': 'bagfiles_size_cache'}))

        if not size_document_list:
            logger.info(
                "No size cache found for the bagfile, returning default values"
            )
            return [1000, 10000000000000]
        else:
            return [
                size_document_list[0].get("min", 1000),
                size_document_list[0].get("max", 10000000000000)
            ]

    def get_childbagfile_size_min_max(self):
        with self.connector as db:
            size_document_list = list(
                db.search_documents(query={'type': 'childbagfiles_size_cache'}))

        if not size_document_list:
            logger.info(
                "No size cache found for the childbagfile, returning default values"
            )
            return [1000, 10000000000000]
        else:
            results = [
                size_document_list[0].get("min", 1000),
                size_document_list[0].get("max", 10000000000000)
            ]
        return results

    def get_all_bagfileguids_to_delete(self):
        results = []
        with self.connector as db:
            bagfile_guid_delete_document_list = [
                *db.search_documents(query={'type': 'delete_flag'})
            ]

        if not bagfile_guid_delete_document_list:
            logger.info("No delete flags found, returning empty list")
            results = []
        elif len(bagfile_guid_delete_document_list) > 1:
            logger.error(
                f"More then one document exists for bagfile guids to delete, "
                f"aborting. {bagfile_guid_delete_document_list}")
            raise ValueError
        else:
            results = bagfile_guid_delete_document_list[0].get("bagfile_guids")
        return results

    def get_all_bagfileguids_to_sync(self):
        results = []
        with self.connector as db:
            bagfile_guid_sync_document_list = [
                *db.search_documents(query={'type': 'sync_flag'})
            ]

        if not bagfile_guid_sync_document_list:
            logger.info("No sync flags found, returning empty list")
            results = []
        elif len(bagfile_guid_sync_document_list) > 1:
            logger.error(f"More then one document exists for bagfile guids to "
                         f"sync, aborting. {bagfile_guid_sync_document_list}")
            raise ValueError
        else:
            results = bagfile_guid_sync_document_list[0].get("bagfile_guids")
        return results

    def get_all_driven_by_values(self):
        return self.get_cached_values('drives_driven_by_cache')

    def get_all_drivers(self):
        return self.get_cached_values('drives_driver_cache')

    def get_all_operators(self):
        return self.get_cached_values('drives_operator_cache')

    def get_all_hardware_releases(self):
        return self.get_cached_values('drives_hardware_release_cache')

    def get_cached_values(self, cache_type: str, values_field: str = 'values'):
        results = []
        with self.connector as db:
            document_list = [
                *db.search_documents(query={'type': cache_type})
            ]
        if not document_list:
            logger.info(f"No cache found for {cache_type}, returning empty list")
            results = []
        else:
            results = document_list[0].get(values_field)
        return results

    def get_extractor_requests_by_bagfile_guid(self, bagfile_guid):
        """
        Get extractor requests from the DB that belong to the specified bagfile.

        :param bagfile_guid:
        :return: list of extractor requests
        """
        with self.connector as db:
            results = [
                *db.get_extractor_requests_by_bagfile_guid([bagfile_guid])
            ]

        return results

    def aggregate_extractor_requests(self, pipeline: List[dict]):
        with self.connector as db:
            results = [*db.aggregate_extractor_requests(pipeline=pipeline)]
        return results

    def aggregate_feature_buckets(self, pipeline: List[dict]):
        with self.connector as db:
            results = [*db.aggregate_feature_buckets(pipeline=pipeline)]
        return results

    def get_account_by_key_hash(self, key_hash) -> Account:
        with self.connector as db:
            as_dicts = db.search_accounts(query={'account_key_hash': key_hash},
                                          limit=1)
        accounts = [Account.parse_obj(account) for account in as_dicts]
        if not accounts:
            raise GuidNotFoundError(key_hash, 'account')
        return accounts[0]
