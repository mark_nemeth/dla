"""Metadata Handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os
from typing import List

import requests
from ContainerClient.config.config_handler import config_dict
from ContainerClient.handlers.auth_handler import create_session
from ContainerClient.handlers.base_request_handler import BaseRequestHandler, HttpMethod
from DANFUtils.constants import *
from DANFUtils.exceptions import MissingEnvironmentVariableException, RESTException
from DANFUtils.logging import logger
from DANFUtils.tracing import tracing


class MetadataHandler(BaseRequestHandler):
    """
    Metadata Handler class
    """

    def __init__(self):
        super().__init__()

        self.BASE_URL = config_dict.get_value('METADATA_API_BASE_URL')
        if not self.BASE_URL:
            raise MissingEnvironmentVariableException('ContainerClient',
                                                      'METADATA_API_BASE_URL')

        self.EXTRACTOR_REQUEST_URL = self.BASE_URL + "extractorrequest/"
        self.BAGFILE_URL = self.BASE_URL + "bagfile/"
        self.DRIVEFILE_URL = self.BASE_URL + "drive/"
        self.BAGFILE_STATE_URL_TEMPLATE = self.BASE_URL + "bagfile/{}/state"
        self.BAGFILE_CUSTOM_ATTRIBUTES_URL_TEMPLATE = self.BASE_URL + "bagfile/{}/custom_attributes"
        self.CHILDBAGFILE_CUSTOM_ATTRIBUTES_URL_TEMPLATE = self.BASE_URL + "childbagfile/{}/custom_attributes"
        self.CHILDBAGFILE_URL = self.BASE_URL + "childbagfile/"
        self.FEATURE_BUCKET_URL = self.BASE_URL + "featurebucket/"
        self.ADD_FLAG_URL_TEMPLATE = self.BASE_URL + "bagfile/{}/flag"
        self.DELETE_FLAG_URL = self.BASE_URL + "bagfile/flag"
        self.IMPORT_URL = self.BASE_URL + "import"
        self.FLOW_URL = self.BASE_URL + "flows"
        self.FLOW_RUN_URL = self.BASE_URL + "flow-runs"
        self.CATALOG_URL = self.BASE_URL + "file"
        self.CACHE_TOPICS_URL = self.BASE_URL + "cache/topics"
        self.CACHE_VEHICLE_IDS_URL = self.BASE_URL + "cache/vehicleids"
        self.CACHE_ORIGINS_URL = self.BASE_URL + "cache/origins"
        self.CACHE_DRIVERS_URL = self.BASE_URL + "cache/drivers"
        self.CACHE_OPERATORS_URL = self.BASE_URL + "cache/operators"
        self.CACHE_HARDWARE_RELEASES_URL = self.BASE_URL + "cache/hardware-releases"
        self.CACHE_DRIVEN_BY_VALUES_URL = self.BASE_URL + "cache/driven-by-values"
        self.CACHE_BAGFILE_SIZE_DURATION_TIME_URL = self.BASE_URL + "cache/size-duration-time"
        self.CACHE_CHILDBAGFILE_SIZE_DURATION_TIME_URL = self.BASE_URL + "cache/childbagfile-size-duration-time"
        self.CACHE_DRIVE_DURATION_TIME_URL = self.BASE_URL + "cache/drive-duration-time"
        self.BVA_QUOTAS_URL = self.BASE_URL + "bva/quotas"

        self.OFFLINE_MODE = config_dict.get_value("OFFLINE_MODE")
        if self.OFFLINE_MODE in ["True", "TRUE", "true", "T", "t", True]:
            self.OFFLINE_MODE = True
        elif self.OFFLINE_MODE in ["False", "FALSE", "false", "F", "f", False]:
            self.OFFLINE_MODE = False
        else:
            logger.warning("Invalid OFFLINE_MODE configuration value. "
                           "OFFLINE mode will be turned off by default.")
            self.OFFLINE_MODE = False
        self.OFFLINE_OUTPUT_PATH = config_dict.get_value("OFFLINE_OUTPUT_PATH")

    def _init_session(self):
        self.AUTH = config_dict.get_value('AUTH')
        if self.AUTH == "auth0":
            token_url = config_dict.get_value('AUTH_TOKEN_URL')
            client_credentials_id = config_dict.get_value(
                'AUTH_METADATA_CLIENT_CRED_ID')
            client_credentials_secret = config_dict.get_value(
                'AUTH_METADATA_CLIENT_CRED_SECRET')
            audience = config_dict.get_value('AUTH_METADATA_AUDIENCE')
            if token_url and client_credentials_id and client_credentials_secret and audience:
                self.session = create_session(
                    token_url=token_url,
                    client_credentials_id=client_credentials_id,
                    client_credentials_secret=client_credentials_secret,
                    audience=audience)
        if 'session' not in self.__dict__:
            self.session = requests.Session()
        self.set_retries()

    @tracing.trace_task('bagfiles/get', tracing.Kind.DEPENDENCY)
    def get_bagfile(self, guid, whitelist):
        assert isinstance(guid, str), 'guid has to be of type str.'

        tracing.task_attribute("bagfile_guid", guid)

        url = self.BAGFILE_URL + guid
        params = {}
        if whitelist:
            params['whitelist'] = ','.join(whitelist)
        res = self.send_request(HttpMethod.GET,
                                url=url,
                                params=params,
                                headers=tracing.get_propagation_headers())

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.GET.value, url,
                                res.status_code, res.text)
        bagfile = json.loads(res.text)['results'][0]
        return bagfile, res

    @tracing.trace_task('bagfiles/post', tracing.Kind.DEPENDENCY)
    def create_bagfile(self, bagfile_data_dict):
        assert isinstance(bagfile_data_dict,
                          dict), 'bagfile_data_dict has to be of type dict.'

        res = self.send_request(HttpMethod.POST,
                                url=self.BAGFILE_URL,
                                json=bagfile_data_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_RESOURCE_CREATED:
            guid = res.headers['Location'].split('/')[2]
        else:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                self.BAGFILE_URL, res.status_code, res.text)

        tracing.task_attribute("bagfile_guid", guid)

        return guid, res

    @tracing.trace_task('drivefiles/post', tracing.Kind.DEPENDENCY)
    def create_drivefile(self, drive_file_dict):
        assert isinstance(drive_file_dict,
                          dict), 'drive_file_dict has to be of type dict.'
        logger.info(drive_file_dict)
        res = self.send_request(HttpMethod.POST,
                                url=self.DRIVEFILE_URL,
                                json=drive_file_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_RESOURCE_CREATED:
            guid = res.headers['Location'].split('/')[2]
        else:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                self.DRIVEFILE_URL, res.status_code, res.text)
        return guid, res

    @tracing.trace_task('bagfiles/patch', tracing.Kind.DEPENDENCY)
    def update_bagfile(self, guid, bagfile_data_dict):
        # validate input data
        assert isinstance(guid, str), 'guid has to be of type string.'
        assert isinstance(bagfile_data_dict,
                          dict), 'bagfile_data_dict has to be of type dict.'

        tracing.task_attribute("bagfile_guid", guid)

        # send PATCH request
        url = self.BAGFILE_URL + guid
        res = self.send_request(HttpMethod.PATCH,
                                url=url,
                                json=bagfile_data_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.PATCH.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('bagfiles/delete', tracing.Kind.DEPENDENCY)
    def delete_bagfile(self, guid, cascade_to_list, hard_delete):
        # validate input data
        assert isinstance(guid, str), 'guid has to be of type string.'

        tracing.task_attribute("bagfile_guid", guid)

        # send DELETE request
        url = self.BAGFILE_URL + guid
        res = self.send_request(HttpMethod.DELETE,
                                url=url,
                                headers=tracing.get_propagation_headers(),
                                params={"cascade_to": cascade_to_list,
                                        "hard_delete": hard_delete})

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.DELETE.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('drives/patch', tracing.Kind.DEPENDENCY)
    def update_drive(self, guid, drive_data_dict):
        # validate input data
        assert isinstance(guid, str), 'guid has to be of type string.'
        assert isinstance(drive_data_dict,
                          dict), 'drive_data_dict has to be of type dict.'

        tracing.task_attribute("drive_guid", guid)
        if 'guid' in drive_data_dict:
            drive_data_dict.pop('guid')
        # send PATCH request
        url = self.DRIVEFILE_URL + guid
        res = self.send_request(HttpMethod.PATCH,
                                url=url,
                                json=drive_data_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.PATCH.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('drivefiles/delete', tracing.Kind.DEPENDENCY)
    def delete_drivefile(self, guid, cascade_to_list, hard_delete):
        # validate input data
        assert isinstance(guid, str), 'guid has to be of type string.'

        tracing.task_attribute("drive_guid", guid)

        # send DELETE request
        url = self.DRIVEFILE_URL + guid
        res = self.send_request(HttpMethod.DELETE,
                                url=url,
                                headers=tracing.get_propagation_headers(),
                                params={"cascade_to": cascade_to_list,
                                        "hard_delete": hard_delete})

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.DELETE.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('bagfiles/custom_attributes/patch',
                        tracing.Kind.DEPENDENCY)
    def add_bagfile_custom_attributes(self, bagfile_guid, extractor_id,
                                      custom_attributes_dict):
        assert isinstance(bagfile_guid, str), 'guid has to be of type string.'
        assert isinstance(extractor_id,
                          str), 'extractor_id has to be of type string.'
        assert isinstance(custom_attributes_dict,
                          dict), 'custom_attributes has to be of type dict.'

        tracing.task_attribute("bagfile_guid", bagfile_guid)
        custom_attributes_dict_with_extractor_id = dict
        custom_attributes_dict_with_extractor_id = {
            extractor_id: custom_attributes_dict
        }

        url = self.BAGFILE_CUSTOM_ATTRIBUTES_URL_TEMPLATE.format(bagfile_guid)
        res = self.send_request(HttpMethod.PATCH,
                                url=url,
                                json=custom_attributes_dict_with_extractor_id,
                                headers=tracing.get_propagation_headers())

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('bagfiles/state/post', tracing.Kind.DEPENDENCY)
    def add_bagfile_state_event(self, bagfile_guid, state_event):
        assert isinstance(bagfile_guid, str), 'guid has to be of type string.'
        assert isinstance(state_event,
                          dict), 'state_event has to be of type dict.'

        tracing.task_attribute("bagfile_guid", bagfile_guid)

        url = self.BAGFILE_STATE_URL_TEMPLATE.format(bagfile_guid)
        res = self.send_request(HttpMethod.POST,
                                url=url,
                                json=state_event,
                                headers=tracing.get_propagation_headers())

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('childbagfiles/post', tracing.Kind.DEPENDENCY)
    def create_childbagfile(self, child_bagfile_data_dict):
        assert isinstance(
            child_bagfile_data_dict,
            dict), 'child_bagfile_data_dict has to be of type dict.'

        res = self.send_request(HttpMethod.POST,
                                url=self.CHILDBAGFILE_URL,
                                json=child_bagfile_data_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_RESOURCE_CREATED:
            guid = res.headers['Location'].split('/')[2]
        else:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                self.CHILDBAGFILE_URL, res.status_code,
                                res.text)

        tracing.task_attribute("childbagfile_guid", guid)

        return guid, res

    @tracing.trace_task('childbagfiles/patch', kind=tracing.Kind.DEPENDENCY)
    def update_childbagfile(self, childbagfile_guid, child_bagfile_data_dict):
        assert isinstance(childbagfile_guid,
                          str), 'guid has to be of type string.'
        assert isinstance(
            child_bagfile_data_dict,
            dict), 'child_bagfile_data_dict has to be of type dict.'

        tracing.task_attribute("childbagfile_guid", childbagfile_guid)

        url = self.CHILDBAGFILE_URL + childbagfile_guid
        res = self.send_request(HttpMethod.PATCH,
                                url=url,
                                json=child_bagfile_data_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.PATCH.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('childbagfiles/{guid}/custom_attributes/patch',
                        tracing.Kind.DEPENDENCY)
    def add_childbagfile_custom_attributes(self, childbagfile_guid,
                                           extractor_id,
                                           custom_attributes_dict):
        assert isinstance(childbagfile_guid,
                          str), 'guid has to be of type string.'
        assert isinstance(extractor_id,
                          str), 'extractor_id has to be of type string.'
        assert isinstance(custom_attributes_dict,
                          dict), 'custom_attributes has to be of type dict.'

        tracing.task_attribute("childbagfile_guid", childbagfile_guid)
        custom_attributes_dict_with_extractor_id = dict
        custom_attributes_dict_with_extractor_id = {
            extractor_id: custom_attributes_dict
        }

        url = self.CHILDBAGFILE_CUSTOM_ATTRIBUTES_URL_TEMPLATE.format(
            childbagfile_guid)
        res = self.send_request(HttpMethod.PATCH,
                                url=url,
                                json=custom_attributes_dict_with_extractor_id,
                                headers=tracing.get_propagation_headers())

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('childbagfiles/delete', kind=tracing.Kind.DEPENDENCY)
    def delete_childbagfile(self, guid, hard_delete):
        assert isinstance(guid, str), 'guid has to be of type string.'

        tracing.task_attribute("childbagfile_guid", guid)

        url = self.CHILDBAGFILE_URL + guid
        res = self.send_request(HttpMethod.DELETE,
                                url=url,
                                headers=tracing.get_propagation_headers(),
                                params={"hard_delete": hard_delete})

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.DELETE.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('extractorrequest/post', kind=tracing.Kind.DEPENDENCY)
    def submit_metadata(self, extracted_events, filename):
        guids = None
        res = None

        if filename is None:
            filename = config_dict.get_value("FILE_NAME")

        # validate input data
        assert isinstance(extracted_events, list) or isinstance(extracted_events, dict), \
            'extracted_events has to be of type list or dict (legacy format)'

        # send POST request or save to the file
        if self.OFFLINE_MODE is True:
            logger.debug(
                "Entering the OFFLINE mode, going to save ExtractorRequest to file"
            )
            script_dir = os.getcwd()
            path = os.path.join(script_dir, self.OFFLINE_OUTPUT_PATH)
            final_path = f'{path}{filename}.json'

            # save as an appending array
            if os.path.isfile(final_path):
                with open(final_path, 'r') as outfile:
                    pre_data = json.load(outfile)

            elif not os.path.exists(path):
                pre_data = []
                os.makedirs(path)
                logger.info(
                    "the extractor_request file is empty, we create a new  one"
                )
            else:
                pre_data = []

            with open(final_path, 'w+') as outfile:
                pre_data.append(extracted_events)
                json.dump(pre_data, outfile)
        else:
            res = self.send_request(HttpMethod.POST,
                                    url=self.EXTRACTOR_REQUEST_URL,
                                    json=extracted_events,
                                    headers=tracing.get_propagation_headers())

            if res.status_code == REQ_STATUS_CODE_RESOURCE_CREATED:
                data = json.loads(res.content.decode())['results']
                if 'guids' in data:
                    guids = data['guids']
                else:
                    # it can be removed later, it is for two-way compatibility between container client and metadata api
                    guids = res.headers['Location'].split('/')[2]
            else:
                raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                    self.EXTRACTOR_REQUEST_URL,
                                    res.status_code, res.text)

        return guids, res

    @tracing.trace_task('extractorrequest/delete',
                        kind=tracing.Kind.DEPENDENCY)
    def delete_extractorrequest(self, guid):
        assert isinstance(guid, str), 'guid has to be of type string.'

        tracing.task_attribute("extractorrequest_guid", guid)

        # send DELETE request
        url = self.EXTRACTOR_REQUEST_URL + guid
        res = self.send_request(HttpMethod.DELETE,
                                url=url,
                                headers=tracing.get_propagation_headers())
        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.DELETE.value, url,
                                res.status_code, res.text)

        return res

    @tracing.trace_task('featurebuckets/post', tracing.Kind.DEPENDENCY)
    def create_feature_bucket(self, feature_bucket: dict
                              ) -> (str, requests.Response):
        assert isinstance(feature_bucket,
                          dict), 'feature_bucket has to be of type dict.'

        res = self.send_request(HttpMethod.POST,
                                url=self.FEATURE_BUCKET_URL,
                                json=feature_bucket,
                                headers=tracing.get_propagation_headers())

        if res.status_code == REQ_STATUS_CODE_RESOURCE_CREATED:
            guid = res.headers['Location'].split('/')[2]
        else:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                self.FEATURE_BUCKET_URL, res.status_code,
                                res.text)

        tracing.task_attribute("feature_bucket_guid", guid)

        return guid, res

    @tracing.trace_task('featurebuckets/patch', kind=tracing.Kind.DEPENDENCY)
    def update_feature_bucket(self, feature_bucket_guid: str,
                              feature_bucket_dict: dict
                              ) -> (str, requests.Response):
        assert isinstance(feature_bucket_guid,
                          str), 'guid has to be of type string.'
        assert isinstance(feature_bucket_dict,
                          dict), 'feature_bucket has to be of type dict.'

        tracing.task_attribute("feature_bucket_guid", feature_bucket_guid)

        url = self.FEATURE_BUCKET_URL + feature_bucket_guid
        res = self.send_request(HttpMethod.PATCH,
                                url=url,
                                json=feature_bucket_dict,
                                headers=tracing.get_propagation_headers())

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.PATCH.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('bagfiles/flag/post', kind=tracing.Kind.DEPENDENCY)
    def add_flag(self, flag_type, bagfile_guid):
        data_dict = {"flag_type": flag_type}

        url = self.ADD_FLAG_URL_TEMPLATE.format(bagfile_guid)
        res = self.send_request(HttpMethod.POST, url=url, json=data_dict)

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                self.ADD_FLAG_URL_TEMPLATE, res.status_code,
                                res.text)

        return res

    @tracing.trace_task('bagfiles/flag/delete', kind=tracing.Kind.DEPENDENCY)
    def delete_flag(self, flag_type):
        data_dict = {"flag_type": flag_type}

        url = self.DELETE_FLAG_URL
        res = self.send_request(HttpMethod.DELETE, url=url, json=data_dict)

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.DELETE.value,
                                self.DELETE_FLAG_URL, res.status_code,
                                res.text)
        return res

    @tracing.trace_task('import/post', kind=tracing.Kind.DEPENDENCY)
    def import_metadata(self, sync_metadata):

        url = self.IMPORT_URL
        res = self.send_request(HttpMethod.POST, url=url, json=sync_metadata)

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                self.IMPORT_URL, res.status_code, res.text)
        return res

    @tracing.trace_task('flows/post', kind=tracing.Kind.DEPENDENCY)
    def create_flow(self, flow_data):
        assert isinstance(flow_data, dict), \
            'flow_data has to be of type dict.'

        url = self.FLOW_URL
        res = self.send_request(HttpMethod.POST, url=url, json=flow_data)

        if res.status_code != REQ_STATUS_CODE_RESOURCE_CREATED:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value, url,
                                res.status_code, res.text)
        flow = json.loads(res.text)['results'][0]
        return flow, res

    @tracing.trace_task('flows/{guid}/get', kind=tracing.Kind.DEPENDENCY)
    def get_flow(self, guid):
        assert isinstance(guid, str), \
            'guid has to be of type string.'

        url = f"{self.FLOW_URL}/{guid}"
        res = self.send_request(HttpMethod.GET, url=url)

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.GET.value, url,
                                res.status_code, res.text)
        flow_run = json.loads(res.text)['results'][0]
        return flow_run, res

    @tracing.trace_task('flow-runs/post', kind=tracing.Kind.DEPENDENCY)
    def create_flow_run(self, flow_run_data):
        assert isinstance(flow_run_data, dict), \
            'flow_run_data has to be of type dict.'

        url = self.FLOW_RUN_URL
        res = self.send_request(HttpMethod.POST, url=url, json=flow_run_data)

        if res.status_code != REQ_STATUS_CODE_RESOURCE_CREATED:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value, url,
                                res.status_code, res.text)
        flow_run = json.loads(res.text)['results'][0]
        return flow_run, res

    @tracing.trace_task('flow-runs/{guid}/get', kind=tracing.Kind.DEPENDENCY)
    def get_flow_run(self, guid):
        assert isinstance(guid, str), \
            'guid has to be of type string.'

        url = f"{self.FLOW_RUN_URL}/{guid}"
        res = self.send_request(HttpMethod.GET, url=url)

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.GET.value, url,
                                res.status_code, res.text)
        flow_run = json.loads(res.text)['results'][0]
        return flow_run, res

    @tracing.trace_task('filecatalog/{guid}/get', kind=tracing.Kind.DEPENDENCY)
    def get_catalog(self, guid):
        assert isinstance(guid, str), \
            'guid has to be of type string.'

        url = f"{self.CATALOG_URL}/{guid}"
        res = self.send_request(HttpMethod.GET, url=url)

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.GET.value, url,
                                res.status_code, res.text)
        catalog = json.loads(res.text)['results'][0]
        return catalog, res

    @tracing.trace_task('file/{file_guid}/put', kind=tracing.Kind.DEPENDENCY)
    def add_file_to_catalog(self, file_guid, file_size, file_type, parent_guid, tags=None):
        if tags is None:
            tags = []
        url = f'{self.CATALOG_URL}/{file_guid}'
        payload = {
            "parent_guid": parent_guid,
            "file_type": file_type,
            "file_size": file_size,
            "tags": tags
        }

        res = self.get_file_from_file_catalog(file_guid)
        if res.status_code == REQ_STATUS_CODE_NOT_FOUND:
            res = self.send_request(HttpMethod.PUT, url=url, json=payload)

            if res.status_code != REQ_STATUS_CODE_RESOURCE_CREATED:
                raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                    self.CATALOG_URL, res.status_code, res.text)
            return res
        elif res.status_code == REQ_STATUS_CODE_OK:
            response_dict = res.json()["results"]
            if response_dict["parent_guid"] == parent_guid and response_dict["file_type"] == file_type and \
                    response_dict["file_size"] == file_size:
                return res
            else:
                raise RESTException(self.COMPONENT, HttpMethod.POST.value,
                                    self.CATALOG_URL, res.status_code, res.text)

    @tracing.trace_task('file/{file_guid}/urls/post', kind=tracing.Kind.DEPENDENCY)
    def add_accessor_url_to_catalog(self, file_guid, accessor_url, creation_date, entry_created_by,
                                    expiration_date=None, url_status="Available"):
        res = self.get_file_from_file_catalog(file_guid)

        existing_accessor_urls = res.json()["results"]["urls"]
        for url_id, existing_accessor_url in enumerate(existing_accessor_urls):
            if existing_accessor_url["accessor_url"] == accessor_url:
                if existing_accessor_url["url_status"] != url_status:
                    return self.update_url_status_in_file_catalog(file_guid, url_id, url_status)
                else:
                    return

        url = f'{self.CATALOG_URL}/{file_guid}/urls'
        payload = {
            "accessor_url": accessor_url,
            "creation_date": creation_date,
            "entry_created_by": entry_created_by,
            "expiration_date": expiration_date,
            "url_status": url_status
        }

        res = self.send_request(HttpMethod.POST, url=url, json=payload)

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.PATCH.value,
                                self.CATALOG_URL, res.status_code, res.text)
        return res

    @tracing.trace_task('file/{file_guid}/get', kind=tracing.Kind.DEPENDENCY)
    def get_file_from_file_catalog(self, file_guid):
        url = f'{self.CATALOG_URL}/{file_guid}'
        res = self.send_request(HttpMethod.GET, url=url)
        return res

    @tracing.trace_task('file/{file_guid}/urls/{url_id}/put', kind=tracing.Kind.DEPENDENCY)
    def update_url_status_in_file_catalog(self, file_guid, url_id, url_status):
        url = f'{self.CATALOG_URL}/{file_guid}/urls/{url_id}/status'
        payload = {
            "url_status": url_status
        }
        res = self.send_request(HttpMethod.PUT, url=url, json=payload)
        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.PATCH.value,
                                self.CATALOG_URL, res.status_code, res.text)
        return res

    @tracing.trace_task('flow-runs/{guid}/patch', kind=tracing.Kind.DEPENDENCY)
    def update_flow_run(self, guid, flow_run_update):
        assert isinstance(guid, str), \
            'guid has to be of type string.'

        url = f"{self.FLOW_RUN_URL}/{guid}"
        res = self.send_request(HttpMethod.PATCH,
                                url=url,
                                json=flow_run_update)

        if res.status_code != REQ_STATUS_CODE_OK:
            raise RESTException(self.COMPONENT, HttpMethod.GET.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('flow-runs/{guid}/steps/{name}/runs/post',
                        kind=tracing.Kind.DEPENDENCY)
    def add_flow_run_step_run(self, guid, step_name, flow_run_step_run_data):
        assert isinstance(guid, str), \
            'guid has to be of type string.'
        assert isinstance(step_name, str), \
            'step_name has to be of type string.'
        assert isinstance(flow_run_step_run_data, dict), \
            'flow_run_step_run_data has to be of type dict.'

        url = f"{self.FLOW_RUN_URL}/{guid}/steps/{step_name}/runs"
        res = self.send_request(HttpMethod.POST,
                                url=url,
                                json=flow_run_step_run_data)

        if res.status_code != REQ_STATUS_CODE_RESOURCE_CREATED:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value, url,
                                res.status_code, res.text)
        return res

    @tracing.trace_task('cachetopic/post', kind=tracing.Kind.DEPENDENCY)
    def cache_topics(self):
        url = self.CACHE_TOPICS_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cachevehicleid/post', kind=tracing.Kind.DEPENDENCY)
    def cache_vehicleids(self):
        url = self.CACHE_VEHICLE_IDS_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cacheorigin/post', kind=tracing.Kind.DEPENDENCY)
    def cache_origins(self):
        url = self.CACHE_ORIGINS_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cachedriver/post', kind=tracing.Kind.DEPENDENCY)
    def cache_drivers(self):
        url = self.CACHE_DRIVERS_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cacheoperator/post', kind=tracing.Kind.DEPENDENCY)
    def cache_operators(self):
        url = self.CACHE_OPERATORS_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cachehardwarerelease/post', kind=tracing.Kind.DEPENDENCY)
    def cache_hardware_releases(self):
        url = self.CACHE_HARDWARE_RELEASES_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cachedrivenbyvalue/post', kind=tracing.Kind.DEPENDENCY)
    def cache_driven_by_values(self):
        url = self.CACHE_DRIVEN_BY_VALUES_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cachebagfilesizeduration/post',
                        kind=tracing.Kind.DEPENDENCY)
    def cache_bagfile_size_duration_time(self):
        url = self.CACHE_BAGFILE_SIZE_DURATION_TIME_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cachedriveduration/post',
                        kind=tracing.Kind.DEPENDENCY)
    def cache_drive_duration_time(self):
        url = self.CACHE_DRIVE_DURATION_TIME_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('cachechildbagfilesizeduration/post',
                        kind=tracing.Kind.DEPENDENCY)
    def cache_childbagfile_size_duration_time(self):
        url = self.CACHE_CHILDBAGFILE_SIZE_DURATION_TIME_URL

        return self.send_cache_refresh_request(url)

    @tracing.trace_task('quota/post', kind=tracing.Kind.DEPENDENCY)
    def refill_quota(self):
        url = self.BVA_QUOTAS_URL

        res = self.send_request(HttpMethod.POST, url=url)

        if res.status_code != REQ_STATUS_CODE_RESOURCE_CREATED:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value, url,
                                res.status_code, res.text)
        return res

    def send_cache_refresh_request(self, url: str):
        res = self.send_request(HttpMethod.POST, url=url)

        if res.status_code != REQ_STATUS_CODE_RESOURCE_CREATED:
            raise RESTException(self.COMPONENT, HttpMethod.POST.value, url,
                                res.status_code, res.text)
        return res
