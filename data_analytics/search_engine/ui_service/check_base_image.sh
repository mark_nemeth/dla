#!/bin/sh
printf "Check if ui base image needs to be updated...\n"
docker pull athena.daimler.com/dla_docker/ui-base-image:latest
docker run -dit athena.daimler.com/dla_docker/ui-base-image:latest

# Copy package.jsons of client and server to from BaseImage to local disk
docker cp $(docker ps -alq):/usr/src/app/server/package.json ./server/config/package.json
docker cp $(docker ps -alq):/usr/src/app/client/package.json ./client/docs/package.json
docker kill $(docker ps -alq)

server_dependencies=$(jq '.dependencies' ./server/package.json)
server_dev_dependencies=$(jq '.devDependencies' ./server/package.json)

base_server_dependencies=$(jq '.dependencies' ./server/config/package.json)
base_server_dev_dependencies=$(jq '.devDependencies' ./server/config/package.json)

# Check server dependencies
if [ "$server_dependencies" == "$base_server_dependencies" ] 
then
  server_dependencies_changed=false
  echo -e "Server dependencies \e[32munchanged\e[0m."
else
  server_dependencies_changed=true
  echo -e "Server dependencies \e[31mdiffer\e[0m."
fi

if [ "$server_dev_dependencies" == "$base_server_dev_dependencies" ] 
then
  server_dev_dependencies_changed=false
  echo -e "Server dev dependencies \e[32munchanged\e[0m."
else
  server_dev_dependencies_changed=true
  echo -e "Server dev dependencies \e[31mdiffer\e[0m."
fi

# Check client dependencies
client_dependencies=$(jq '.dependencies' ./client/package.json)
client_dev_dependencies=$(jq '.devDependencies' ./client/package.json)

base_client_dependencies=$(jq '.dependencies' ./client/docs/package.json)
base_client_dev_dependencies=$(jq '.devDependencies' ./client/docs/package.json)

if [ "$client_dependencies" == "$base_client_dependencies" ] 
then
  client_dependencies_changed=false
  echo -e "Client dependencies \e[32munchanged\e[0m."
else
  client_dependencies_changed=true
  echo -e "Client dependencies \e[31mdiffer\e[0m."
fi

if [ "$client_dev_dependencies" == "$base_client_dev_dependencies" ] 
then
  client_dev_dependencies_changed=false
  echo -e "Client dev dependencies \e[32munchanged\e[0m."
else
  client_dev_dependencies_changed=true
  echo -e "Client dev dependencies \e[31mdiffer\e[0m."
fi

# Check if package.jsons of BaseImage differ from local ones. If yes build new BaseImage.
if [ $server_dependencies_changed == true ] || [ $server_dev_dependencies_changed == true ] || [ $client_dependencies_changed == true ] || [ $client_dev_dependencies_changed == true ]
then
  printf "Base image needs to be updated...\n"
  printf "Starting base image build...\n"
  cd ../devops/on_prem/ui_base_image/ && ./build_base_image.sh
else
  printf "Base image needs NOT to be updated...\n"
fi

# cleanup package.jsons of BaseImage from local disk
rm ./client/docs/package.json && rm ./server/config/package.json
