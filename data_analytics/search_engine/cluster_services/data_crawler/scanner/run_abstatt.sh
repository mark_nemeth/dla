#!/bin/sh
export http_proxy=""
export https_proxy=""
bash /mnt/hdfs/kerberos/container-setup.sh
sleep 60
echo "Deleting old crawler output..."
rm -vf "${OUTPUT_PATH}"/*
time python3 -u scanner/scanner.py "$OUTPUT_PATH" "$DIRECTORIES" "$NUM_PROCESSES" "$NUM_WORKERS" "$CHUNK_SIZE" --whitelist "$PATTERN"
SRC=$(echo "${OUTPUT_PATH}" | sed -e "s@/mnt/hdfs/hdfs@@")
kinit emtc_its_s_dtransfer@ATHENA.CORPINTRA.NET -k -t /mnt/keytab/emtc_its_s_dtransfer.keytab
data=$(sed -e "s@\$origin_var@${SRC}/\*@; s@\$destination_var@${VAI_CRAWLER_INDEX}@" < job.xml)
curl -kX POST --negotiate -u: https://smtcae000094.athena.corpintra.net:11443/oozie/v2/jobs?action=start -d "$data" -H "Content-Type:application/xml" -H "Accept:application/json"
