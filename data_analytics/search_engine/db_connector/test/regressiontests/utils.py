"""Test utils"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import math
import numpy as np
from datetime import datetime, timedelta
import random as rd
from DANFUtils.utils import generate_deterministic_guid
rd.seed("test")


# testdata
def generate_testdata():

    bag_1 = {
        "guid": generate_deterministic_guid(),
        "link": "/test/test/test/test/test/test/test",
        "size": 21231,
        "num_messages": 4444,
        "start": 1475272800000,
        "end": 1475272800001,
        "duration": 752728,
        "vehicle_id_num": "V-123-234",
        "extractor": "ext1",
        "version": "1.0",
        "is_test_data": 1
    }

    bag_2 = {
        "guid": generate_deterministic_guid(),
        "link": "/test/test/test/test/test/test/test",
        "size": 21231,
        "num_messages": 4444,
        "start": 1475272800000,
        "end": 1475272800001,
        "duration": 752728,
        "vehicle_id_num": "V-123-234",
        "extractor": "ext1",
        "version": "1.0",
        "is_test_data": 1
    }

    bag_3 = {
        "guid": generate_deterministic_guid(),
        "link": "/test/test/test/test/test/test/test",
        "size": 21231,
        "num_messages": 4444,
        "start": 1475272800000,
        "end": 1475272800001,
        "duration": 752728,
        "vehicle_id_num": "V-123-234",
        "extractor": "ext1",
        "version": "1.0",
        "is_test_data": 1
    }

    seq_1 = {
        "guid": generate_deterministic_guid(),
        "bagfile_guid": bag_1['guid'],
        "index": 8,
        "start": 1475272800000,
        "end": 1475272800000,
        "type": "basic",
        "extractor": "ext1",
        "version": "1.0",
        "is_test_data": 1,
        "extractors": {
            "ext1": {
                "metadata": {
                    "disengagement": [{
                        "ts":
                        1558962227475,
                        "type":
                        "Unknown",
                        "error msg":
                        "LogRequest topic unavailable"
                    }, {
                        "ts":
                        1558962229515,
                        "type":
                        "Unknown",
                        "error msg":
                        "LogRequest topic unavailable"
                    }]
                },
                "bva": 10
            }
        }
    }

    seq_2 = {
        "guid": generate_deterministic_guid(),
        "bagfile_guid": bag_2['guid'],
        "index": 3,
        "start": 1475272800000,
        "end": 1475272800000,
        "type": "basic",
        "extractor": "ext1",
        "version": "1.0",
        "is_test_data": 1
    }

    seq_3 = {
        "guid": generate_deterministic_guid(),
        "bagfile_guid": bag_2['guid'],
        "index": 4,
        "start": 1475272800000,
        "end": 1475272800000,
        "type": "basic",
        "extractor": "ext1",
        "version": "1.0",
        "is_test_data": 1
    }

    ext_1 = {
        "version": "1.0",
        "guid": generate_deterministic_guid(),
        "bagfile_guid": bag_1['guid'],
        "start": 1475272800000,
        "end": 1475272800000,
        "extractor_id": "ext1",
        "metadata": {
            "disengagement": {},
            "silent_testing": {}
        },
        "bva": 8,
        "is_test_data": 1
    }

    ext_2 = {
        "version": "1.0",
        "guid": generate_deterministic_guid(),
        "bagfile_guid": bag_2['guid'],
        "start": 1475272800000,
        "end": 1475272800000,
        "extractor_id": "ext1",
        "metadata": {
            "disengagement": {},
            "silent_testing": {}
        },
        "bva": 8,
        "is_test_data": 1
    }

    ext_3 = {
        "version": "1.0",
        "guid": generate_deterministic_guid(),
        "bagfile_guid": bag_2['guid'],
        "start": 1475272800000,
        "end": 1475272800000,
        "extractor_id": "ext1",
        "metadata": {
            "disengagement": {},
            "silent_testing": {}
        },
        "bva": 8,
        "is_test_data": 1
    }

    ext_large = {
        "version":
        "1.0",
        "guid":
        generate_deterministic_guid(),
        "bagfile_guid":
        bag_2['guid'],
        "extractor_id":
        "ext1",
        'metadata_list': [{
            'end': 1554728092197,
            'bva': 5,
            'type': 'basic',
            'metadata': {
                'disengagement': {
                    'type':
                    'err',
                    'ts':
                    1554728032197,
                    'objects': [{
                        'testcase':
                        2,
                        'label':
                        20,
                        'position':
                        [123.12121, 123.42342342, 5454.423432, 4242.423423]
                    }],
                    'errmsg':
                    'test'
                }
            },
            'start': 1554727972197
        } for i in range(1, 10000)],
        "bva":
        8,
        "is_test_data":
        1
    }

    child_1 = {
        "version": "1.0",
        "guid": generate_deterministic_guid(),
        "parent_guid": bag_1['guid'],
        "start": 1475272800000,
        "end": 1475272800000,
        "extractor_id": "ext1",
        "metadata": {
            "ext1_4": {
                "stereo_cnn": [{
                    "bounding_box_labels": {
                        "1": 2
                    }
                }, {
                    "bounding_box_labels": {
                        "3": 3
                    }
                }, {
                    "bounding_box_labels": {
                        "4": 1
                    }
                }]
            }
        },
        "bva": 7,
        "is_test_data": 1
    }

    child_2 = {
        "version": "1.0",
        "guid": generate_deterministic_guid(),
        "parent_guid": bag_2['guid'],
        "start": 1475272800000,
        "end": 1475272800000,
        "extractor_id": "ext1",
        "metadata": {
            "ext1_4": {
                "stereo_cnn": [{
                    "bounding_box_labels": {
                        "2": 1
                    }
                }, {
                    "bounding_box_labels": {
                        "3": 4
                    }
                }, {
                    "bounding_box_labels": {
                        "7": 3
                    }
                }]
            }
        },
        "bva": 8,
        "is_test_data": 1
    }
    child_3 = {
        "version": "1.0",
        "guid": generate_deterministic_guid(),
        "parent_guid": bag_2['guid'],
        "start": 1475272800000,
        "end": 1475272800000,
        "extractor_id": "ext1",
        "metadata": {
            "ext1_4": {
                "stereo_cnn": [{
                    "bounding_box_labels": {
                        "2": 2
                    }
                }, {
                    "bounding_box_labels": {
                        "5": 1
                    }
                }, {
                    "bounding_box_labels": {
                        "6": 1
                    }
                }]
            }
        },
        "bva": 9,
        "is_test_data": 1
    }

    return bag_1, bag_2, bag_3, seq_1, seq_2, seq_3, ext_1, ext_2, ext_3, ext_large, child_1, child_2, child_3


def generate_seq(index):
    return {
        "guid": generate_deterministic_guid(),
        "bagfile_guid": "233985c0-5f9d-4e46-9c7c-56fe58bce5ed",
        "index": index,
        "start": 1475272800000 + index * 1000,
        "end": 1475272800000 + index * 1000 + 999,
        "type": "basic",
        "extractors": {
            "ext1": {
                "metadata": ["dummydata" * 100]
            }
        },
        "is_test_data": 1
    }


def generate_feature_buckets(bag_file_guid: str,
                             duration: timedelta,
                             bag_file_start: datetime = datetime.strptime(
                                 '01.01.2020 09:30:00,00',
                                 '%d.%m.%Y %H:%M:%S,%f'),
                             version: str = "1.0") -> list:
    feat_bucket_docs = []
    duration_in_s = math.ceil(duration.total_seconds())
    ts = bag_file_start
    features = []
    for m in range(duration_in_s):
        feat = {
            "ts": ts.timestamp() * 1000,
            "vel": float(np.random.normal(30, 10, 1)),
            "acc": float(np.random.normal(3, 2, 1)),
            "eng_state": int(np.random.choice(4, 1)),
            "loc": {
                "type":
                "Point",
                "coordinates": [
                    float(np.random.normal(49, 1, 1)),
                    float(np.random.normal(9, 1, 1))
                ]
            },
            "altitude": float(np.random.normal(50, 1, 1))
        }
        features.append(feat)

        if len(features) == 60 or m == duration_in_s - 1:
            doc = {
                "version": version,
                "guid": generate_deterministic_guid(),
                "bagfile_guid": bag_file_guid,
                "start": features[0]["ts"],
                "end": features[-1]["ts"],
                "start_index": m - len(features),
                "end_index": m,
                "tags": ["", ""],
                "features": features,
                "is_test_data": 1
            }

            feat_bucket_docs.append(doc)
            features = []
        ts += timedelta(0, 1)

    return feat_bucket_docs
