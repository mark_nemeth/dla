"""Setup file"""
from setuptools import setup, find_packages
from config.constants import VERSION


setup(
    name='data_handler',
    version=VERSION,
    packages=find_packages(),
    install_requires=[
        'tornado>=6.0.4',
        'PyYAML>=5.1.1',
        'requests>=2.22.0',
        'Authlib>=0.11',
        'gnupg>=2.3.1',
        'urllib3>=1.24.3',
        'azure-storage-queue==2.1.0',  # later versions has introduced breaking changes
        'kfp>=1.0.0',
        'azure-storage-blob==2.1.0',
        'DANFUtils>=0.2.29',
        'ContainerClient>=0.6.102',
        'pymongo==3.12.2',
        'coverage>=5.2.1',
        'pydantic>=1.2,<2.0',
        'dataclasses_json~=0.5.2',
    ]
)
