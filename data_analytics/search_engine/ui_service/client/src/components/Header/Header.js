import React, {useEffect} from 'react';
import {NavLink} from 'react-router-dom';
import {OverlayTrigger} from "react-bootstrap";
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import featureToggle from '../../config/featureToggleHelper';
import logo from '../../assets/images/logo.png';
import Badge from 'react-bootstrap/Badge'
import Tooltip from "react-bootstrap/Tooltip";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
  faBullhorn,
  faCar,
  faFileContract,
  faHome,
  faQuestionCircle,
  faSignOutAlt
} from "@fortawesome/free-solid-svg-icons";
import announcements from '../../pages/Announcements/Announcements.json';
import './Header.scss';

export function Header(props) {

  useEffect(() => {
    props.fetchFeatureToggles();
  }, []);

  const daysToShow = 3;
  let displayBadge = false;

  // 3 days in milliseconds
  const daysInMilliseconds = daysToShow * 86400000;

  const currentDate = new Date();
  const checkDate = currentDate - daysInMilliseconds;
  for (const announcement of announcements) {
    let announcementDate = new Date(announcement.date);
    if (announcementDate > checkDate) {
      displayBadge = true;
      break;
    }
  }

  function renderTooltip(text) {
    return (props) => (
        <Tooltip id="button-tooltip" {...props}>
          {text}
        </Tooltip>
    );
  }

  function showContentWithTooltip(htmlContent, tooltipText) {
    return (
        <OverlayTrigger placement="bottom"
                        delay={{show: 250, hide: 400}}
                        overlay={renderTooltip(tooltipText)}>
          {htmlContent}
        </OverlayTrigger>
    );
  }

  return (
      <header>
        <Navbar>
          <Navbar.Brand as={NavLink} exact to="/" id="navbar-brand-box">
            <img src={logo} alt="" width="72" height="72"/>
            <div>
              <div className="navbar-brand-title">DANF Search Engine</div>
              {
                featureToggle.showVersion &&
                <div className="navbar-brand-version">Version: 1.0.0</div>
              }
            </div>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={NavLink} exact to="/">
                <FontAwesomeIcon icon={faHome} className="navbar-main-item-icon"/>Home
              </Nav.Link>
              {
                props.isIngestDashboardActive &&
                <Nav.Link as={NavLink} exact to="/ingest">
                  <FontAwesomeIcon icon={faCar} className="navbar-main-item-icon"/>Ingest
                </Nav.Link>
              }
              <Nav.Link as={NavLink} exact to="/reporting">
                <FontAwesomeIcon icon={faFileContract} className="navbar-main-item-icon"/>Reporting
              </Nav.Link>
            </Nav>
            <Nav>
              {
                showContentWithTooltip(
                    <Nav.Link as={NavLink} to="/announcements">
                      <FontAwesomeIcon icon={faBullhorn}/>
                      {
                        displayBadge &&
                        <Badge className="navbar-badge">NEW</Badge>
                      }
                    </Nav.Link>, "Announcements"
                )
              }
              {
                showContentWithTooltip(
                    <Nav.Link as={NavLink} to="/support">
                      <FontAwesomeIcon icon={faQuestionCircle}/>
                    </Nav.Link>, "Support"
                )
              }
              {
                showContentWithTooltip(
                    <Nav.Link href="/logout">
                      <FontAwesomeIcon icon={faSignOutAlt}/>
                    </Nav.Link>, "Logout"
                )
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
  );
}
