#!/bin/sh

log="/mapr/738.mbc.de/tmp/test_splitter/log"

FILES="
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2018/07/06/20180706_101546_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/01/15/20190115_104516_vehicle_startup07_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/01/15/20190115_141451_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/01/17/20190117_142739_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/02/26/20190226_153144_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/03/15/20190315_094319_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313024308/2019/04/01/20190401_152933_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121789/2019/04/08/20190408_145104_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A411952/2019/06/18/20190618_150557_scene_3_tv_STRIPPED_cid_bageval.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121789/2019/03/18/20190318_111656_big_bounding_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A411952/2019/06/13/20190613_140017_TC_3-01-02-01_01_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A396347/2019/06/03/20190603_160512_demo_bosch_board2_STRIPPED_cid_bageval.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A396347/2019/04/24/20190424_144801_traffic_llight_testride2_ORIGINAL_STRIPPED_cid_bageval.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A411952/2019/03/22/20190322_143720_test_planner_fix_ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A396347/2019/05/29/20190529_153030_RC10413_STRIPPED_cid_bageval.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A411952/2019/03/29/20190329_090716_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A417333/2019/01/22/20190122_131938_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/davos/WDF44781313121754/2018/12/11/20181211_091123__ORIGINAL.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A411952/2019/06/19/20190619_084146_alwayson.bag
/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi/plog/WDD2221591A411952/2019/06/18/20190618_133431_cv1_calib_bauhaus_wall_STRIPPED_cid_bageval.bag
"

for file in $FILES
do
    echo "----------------------------------------------------------------------------------------------------------------" >>$log
    echo "Processing $file" >>$log

    filesize=$(stat -c%s "$file")
    echo "size of the bagfile: $filesize" >>$log

    # set env variables
    export OUTPUT_FOLDER=/output

    # run extractor
    echo "****** begin of extractor ********" >>$log

    start=`date +%s`

    filename=$(sed 's|/mapr/738.mbc.de/data/input/rd/athena/08_robotaxi|/input|g' <<< $file)

    bash -c "docker run -it -v /mapr/738.mbc.de/data/input/rd/athena/08_robotaxi:/input -e BAGFILE_PATH="$filename" -e METADATA_API_BASE_URL=http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/  -e SEARCH_API_BASE_URL=http://danf-search-api-dev.westeurope.cloudapp.azure.com:8889/ --network host dlaregistry.azurecr.io/extractor:0.2.1 &>> $log"

    end=`date +%s`
    runtime=$((end-start))

    echo "****** end of extractor ********" >>$log
    echo "execution time for extractor: $runtime s" >>$log

    # get guid from log
    cat $log|grep 'with bagfile_guid' > guid_list
    guid=$(ps -ef | grep "port 10 -" | grep -v "grep port 10 -"| awk '{$1=$1};1'| awk '/./{line=$0} END{print $NF}' guid_list)
    guid_1=${guid::-1}
    rm guid_list

    # run splitter
    echo "****** begin of splitter ********" >>$log
    start=`date +%s`

    bash -c "docker run -v /mapr/738.mbc.de/data/input/rd/athena/08_robotaxi:/input -v /mapr/738.mbc.de/tmp/test_splitter:/output -e BAGFILE_GUID=$guid_1 -e BAGFILE_PATH="$filename" -e OUTPUT_FOLDER=/output -e METADATA_API_BASE_URL=http://danf-metadata-api-dev.westeurope.cloudapp.azure.com:8887/ -e SEARCH_API_BASE_URL=http://danf-search-api-dev.westeurope.cloudapp.azure.com:8889/ -e BVA_THRESHOLD=10 --network host dlaregistry.azurecr.io/splitter:0.1.8 &>> $log"

    end=`date +%s`
    runtime=$((end-start))
    echo "****** end of splitter ********" >>$log
    echo "execution time for splitter: $runtime s" >>$log

done