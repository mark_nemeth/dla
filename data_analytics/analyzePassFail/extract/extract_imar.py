import rosbag
import sys

#bag = rosbag.Bag('../bagfiles/20180214_105847_onlyimar.bag')

bag = rosbag.Bag(sys.argv[1])

topic = '/CalService/instance_0/ImarInterface'

filename = 'csv/Out'+topic.replace("/","_")+'_inspva.csv'
filename2 = 'csv/Out'+topic.replace("/","_")+'_bestpos.csv'


with open(filename, 'w') as f:
  for msg in bag.read_messages(topics=[topic]):
    #print(msg.message.imar_inspva)
    #print >> f, msg.message.longitude
    f.write(str(msg.message.header.timestamp.s)+","
           +str(msg.message.header.timestamp.ns)+","
           +str(msg.message.imar_inspva.latitude)+","
           +str(msg.message.imar_inspva.longitude)+","
           +str(msg.message.imar_inspva.altitude)+","
           +str(msg.message.imar_inspva.roll)+","
           +str(msg.message.imar_inspva.pitch)
           +"\n")


with open(filename2, 'w') as f2:
  for msg in bag.read_messages(topics=[topic]):
    #print(msg.message.imar_inspva)
    #print >> f, msg.message.longitude
    f2.write(str(msg.message.header.timestamp.s)+","
           +str(msg.message.header.timestamp.ns)+","
           +str(msg.message.imar_best_pos.latitude)+","
           +str(msg.message.imar_best_pos.longitude)+","
           +str(msg.message.imar_best_pos.height)
           +"\n")

f.close()
bag.close()    
