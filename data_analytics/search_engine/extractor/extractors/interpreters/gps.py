"""GPS Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os
import numpy as np
from config.constants import *
from extractors.utils import timestamp2s, timestamp2ms
from extractors.base_interpreter import Interpreter

BVA_GPS = 0


class GpsInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.last_ts = None
        self.current_ts = None
        self.latitudes_for_current_ts = []
        self.longitudes_for_current_ts = []
        self.bva = BVA_GPS
        self.event_type = "metadata_enrichment"
        self.downsample_factor = os.environ.get('GPS_DOWNSAMPLE_FACTOR', 5)

    def feed_new_msg(self, topic, msg, t):
        self.last_ts = self.current_ts
        self.current_ts = timestamp2s(t)

        # First gps message in the bagfile: append to list
        if self.last_ts is None:
            self.latitudes_for_current_ts.append(msg.latitude)
            self.longitudes_for_current_ts.append(msg.longitude)

        else:
            # Still in the same second: append gps coordinates to the same list
            if self.last_ts == self.current_ts:
                self.latitudes_for_current_ts.append(msg.latitude)
                self.longitudes_for_current_ts.append(msg.longitude)

            # Entering the next second: calculate the average gps for the last second and clean up the lists
            else:
                average_latitude = np.array(
                    self.latitudes_for_current_ts).mean()
                average_longtitude = np.array(
                    self.longitudes_for_current_ts).mean()

                gps_obj = {}
                gps_obj["ts"] = self.last_ts
                gps_obj["latitude"] = average_latitude
                gps_obj["longitude"] = average_longtitude

                gps_obj = self.calc_start_end_ts_snippet(gps_obj)

                self.event_list += [gps_obj]

                self.latitudes_for_current_ts = []
                self.longitudes_for_current_ts = []

                self.latitudes_for_current_ts.append(msg.latitude)
                self.longitudes_for_current_ts.append(msg.longitude)

        return self.event_list

    @staticmethod
    def calc_start_end_ts_snippet(event) -> dict:
        event["start_ts"] = int(event["ts"] * 1000)
        event["end_ts"] = int(((event["ts"] + 1) * 1000) - 1)
        event["ts"] = int(event["ts"] * 1000)
        return event