__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os

from DANFUtils.exceptions import DANFBaseException
from DANFUtils.logging import logger
from DANFUtils.utils import validate_json
from DBConnector.exceptions import InvalidQueryException, \
    InvalidConfigurationException
from DBConnector.utils import calculate_request_size
from pymongo import ReturnDocument


class BvaVoteCollectionHandler:
    # Workstream index
    COLLECTION_NAME = 'bva_votes'
    INDEX_NAME = 'bva_votes_guid_index'
    INDEX_FIELD_GUID = 'guid'

    def __init__(self, db):
        self.db = db
        self.col = self.db[self.COLLECTION_NAME]
        self.col.create_index(self.INDEX_FIELD_GUID,
                              unique=True,
                              name=self.INDEX_NAME)

    def get(self, quota_document_guid):
        logger.debug(
            f"Searching for vote documents in {self.COLLECTION_NAME} with quota guid: {quota_document_guid}")
        return self.col.find({"quota_document_guid": quota_document_guid},
                             projection={'_id': 0})

    def get_by_guid(self, guid):
        logger.debug(
            f"Searching for vote document in {self.COLLECTION_NAME} with guid: {guid}")
        return self.col.find_one({'guid': guid}, projection={'_id': 0})

    def add(self, vote):
        logger.debug(
            f"Add vote document in {self.COLLECTION_NAME} with values: {vote}")
        self.col.insert_one(vote)

    def delete_by_guid(self, guid):
        logger.debug(
            f"Delete vote document in {self.COLLECTION_NAME} with guid: {guid}")
        return self.col.delete_one({'guid': guid})

    def find_and_update_vote(self, filter_query, update_request):
        logger.debug(
            f"Searching for vote document in {self.COLLECTION_NAME} with filter_query: {filter_query} and update values: {update_request}")
        return self.col.find_one_and_update(filter=filter_query,
                                            update=update_request,
                                            projection={'_id': 0},
                                            return_document=ReturnDocument.AFTER)

    def search(self, query, limit, offset, sortby):
        assert isinstance(query, dict), 'Expected dict query'
        assert isinstance(limit, int), 'Expected int limit'
        assert isinstance(offset, int), 'Expected int offset'
        assert isinstance(sortby, (str, list)), 'Expected str or list sortby'

        request = query
        request_size = calculate_request_size(request)
        logger.debug(f"Searching documents in collection "
                     f"'{self.COLLECTION_NAME}' "
                     f"(Request size: {request_size:.2f} kb)")

        cursor = self.col.find(request, {'_id': 0}).sort(sortby)
        if offset > 0:
            cursor = cursor.skip(offset)
        if limit > 0:
            cursor = cursor.limit(limit)
        return cursor
