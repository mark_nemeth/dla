import React, {useState} from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import {Link} from 'react-router-dom';
import ModalBox from '../../Utils/ModalBox/ModalBox';
import {FileTypes} from '../../Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import {copyToClipboard} from '../../../utils/copyToClipboard';
import featureToggle from '../../../config/featureToggleHelper';

export default function ActionEntry(props) {

  const [modalShow, setModalShow] = useState(false);
  const [modalMultiplexShow, setModalMultiplexShow] = useState(false);

  const showAdminItem = props.adminUser;
  const showPathActions = featureToggle.showPathActions && props.fileType !== FileTypes.DRIVE;
  const showReprocessAction = showAdminItem && props.fileType !== FileTypes.DRIVE
  let path = '';
  switch (props.fileType) {
    case FileTypes.ORIGINALFILE:
      path = `/originalfile/${props.guid}`;
      break;
    case FileTypes.ALWAYSONFILE:
      path = `/alwaysonfile/${props.guid}`;
      break;
    case FileTypes.CHILDBAGFILE:
      path = `/childbagfile/${props.guid}`;
      break;
    case FileTypes.DRIVE:
      path = `/drive/${props.guid}`;
      break;
    default:
      path = `/originalfile/${props.guid}`;
      break;
  }

  const CopyPathItem = () => {
    if (!showPathActions) return null;
    return (
      <Dropdown.Item onClick={() => copyToClipboard(props.link,
        () => props.showBanner({message: 'Path was copied to clipboard', type: 'primary'}),
        () => props.showBanner({message: 'Can not copy path to clipboard', type: 'error'}))}>
        Copy Path
      </Dropdown.Item>
    )
  };

  const ShowPathItem = () => {
    if (!showPathActions) return null;
    return (
      <Dropdown.Item onClick={() => setModalShow(true)}>
        Show Path
      </Dropdown.Item>
    )
  };

  const ViewOverviewPageItem = () => {
    return (
      <Dropdown.Item as={Link} to={path}>
        View
      </Dropdown.Item>
    )
  };

  const ReprocessFileItem = () => {
    if (!showReprocessAction) return null;
    return (
      <Dropdown.Item onClick={() => props.reprocessCallback(props.link)}>Reprocess</Dropdown.Item>
    )
  };

  const isFusionIoSnippet = props.fileName && props.fileName.includes('fusion_io') === true
  let fusionMultiplexCommand = null;
  if (isFusionIoSnippet) {
    const debugFileName = props.fileName.replace('fusion_io', 'debug_io');
    const outputFileName = '/cache/' + props.fileName.replace('fusion_io', 'fusion_io_debug_io');
    fusionMultiplexCommand = 'dla_converter --in ' + props.fileName + ' ' + debugFileName + ' --out ' + outputFileName
  }

  const ShowMultiplexCommandItem = () => {
    if (!isFusionIoSnippet) {
      return null;
    }

    return (
      <Dropdown.Item onClick={() => setModalMultiplexShow(true)}>Show Fusion IO Multiplex Command</Dropdown.Item>
    )
  }

  return (
    <>
      <DropdownButton title="">
        <CopyPathItem/>
        <ShowPathItem/>
        <ViewOverviewPageItem/>
        <ReprocessFileItem/>
        {props.fileType === FileTypes.CHILDBAGFILE && <ShowMultiplexCommandItem/>}
      </DropdownButton>
      {showPathActions && <ModalBox
        header="File Path" body={props.link}
        show={modalShow} onHide={() => setModalShow(false)}
      />}
      <ModalBox
        header="File Path" body={fusionMultiplexCommand}
        show={modalMultiplexShow} onHide={() => setModalMultiplexShow(false)}
      />
    </>
  )
}