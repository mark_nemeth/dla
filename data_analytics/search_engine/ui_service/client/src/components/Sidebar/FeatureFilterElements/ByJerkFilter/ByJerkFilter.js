import React from 'react';
import Form from 'react-bootstrap/Form';
import CollapseContainer from '../../../Utils/CollapsContainer/CollapseContainer';
import {isObjectValid} from '../../../../utils/helperFunctions';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import './ByJerkFilter.scss';

export const STATIC_LIMITS = {
  min: -1,
  max: 1
};

export function ByJerkFilter(props) {

  function onRangeSelection(value) {
    if (isObjectValid(value)) props.filterByJerk(value);
    else props.filterByJerk({min: null, max: null})
  }

  function resetRangeSelection() {
    props.setCurrentJerkRange(props.limits);
    props.filterByJerk({min: null, max: null})
  }

  const subLabel = props.jerk.min.toFixed(2) + ' - ' + props.jerk.max.toFixed(2) + ' m/s3';
  const active = props.jerk.min !== props.limits.min || props.jerk.max !== props.limits.max;

  return (
    <CollapseContainer label={active ? "Jerk: " + subLabel : "Jerk"}
                       active={active}
                       resetCallback={resetRangeSelection}
    >
      <div className="track-container">
        <Form>
          <InputRange
            className="input-range"
            draggableTrack
            step={props.steps}
            maxValue={props.limits.max}
            minValue={props.limits.min}
            onChange={value => props.setCurrentJerkRange(value)}
            onChangeComplete={onRangeSelection}
            value={props.jerk}
          />
        </Form>
      </div>
    </CollapseContainer>
  );
}