#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Revision $Id$

# Simple talker demo that published std_msgs/Strings messages
# to the 'chatter' topic

import rospy
import sys
import getopt
from std_msgs.msg import String


def talker(argv):
    packet_size = 64 * 1000  # kByte
    bandwidth = 1e9  # bit/s
    topic_suffix = ''

    try:
        opts, args = getopt.getopt(
            argv, "hb:p:t:", ["bw=", "packet-size=", "topic-suffix="])
    except getopt.GetoptError:
        print 'talker.py --bw <bandwidth> --packet-size <size> --topic-suffix <suffix>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'talker.py --bw <bandwidth> --packet-size <size> --topic-suffix <suffix>'
            sys.exit()
        elif opt in ("b", "--bw"):
            bandwidth = float(arg)
        elif opt in ("p", "--packet-size"):
            packet_size = int(arg)
        elif opt in ("t", "--topic-suffix"):
            topic_suffix = arg

    synth_data_pack = '0' * packet_size
    pub = rospy.Publisher('SyntheticData%s' %
                          topic_suffix, String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(bandwidth / (packet_size * 8))  # Hz
    while not rospy.is_shutdown():
        # rospy.loginfo(synth_data_pack)
        pub.publish(synth_data_pack)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker(sys.argv[1:])
    except rospy.ROSInterruptException:
        pass
