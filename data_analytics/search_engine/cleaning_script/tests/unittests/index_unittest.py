"""Unittest for bagfiles handler"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest
from collections import namedtuple
from test.support import EnvironmentVarGuard
from unittest.mock import MagicMock

from cleanup import index

Args = namedtuple(
    'Args',
    'delete_queue_message, set_sync_flag, update_processing_state, update_flow_run'
)


class TestIndex(unittest.TestCase):

    def setUp(self):
        self.env = EnvironmentVarGuard()
        self.env.set('APPINSIGHTS_INSTRUMENTATIONKEY', 'APPINSIGHTS_DISABLE_KEY')
        index.update_processing_state = MagicMock(return_value=None)
        index.set_sync_flag = MagicMock(return_value=None)
        index.delete_queue_message = MagicMock(return_value=None)
        index.update_flow_run = MagicMock(return_value=None)

    def test_should_only_execute_activated_tasks(self):
        args = Args(delete_queue_message=True,
                    set_sync_flag=False,
                    update_processing_state=True,
                    update_flow_run=False)
        index.main(args)
        index.update_processing_state.assert_called_once()
        index.set_sync_flag.assert_not_called()
        index.delete_queue_message.assert_called_once()
        index.set_sync_flag.assert_not_called()


if __name__ == '__main__':
    unittest.main()
