"""Unittests for Metadata Client"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import unittest

from DANFUtils.logging import logger
from DBConnector import DBConnector
from ContainerClient.container_client import ContainerClient
from utils import generate_testdata, generate_test_sequence, generate_state_testdata, generate_childbagfile_testdata, generate_custom_attributes
import testconfig

logger.set_log_level('debug')


def cleanup_db():
    connector = DBConnector(
        testconfig.db_hostname, testconfig.db_port, testconfig.db_name,
        testconfig.db_user, testconfig.db_key, testconfig.db_protocol)
    with connector as con:
        res = con.db['bagfiles'].find({'link': "/path/to/bagfile.bag"})
        for x in res:
            con.db['bagfiles'].delete_many({'guid': x['guid']})
            con.db['extractor_requests'].delete_many({'bagfile_guid': x['guid']})
        con.db['sequences'].delete_many({'bagfile_guid': '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'})
        con.db['child_bagfiles'].delete_many({'parent_guid': '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'})


class TestMetaDataSubmit(unittest.TestCase):

    def test_create_bagfile(self):
        cleanup_db()
        client = ContainerClient()
        bagfile_data, bagfile_data_post, bagfile_data_patch, metadata = generate_testdata()
        guid = client.create_bagfile(bagfile_data)
        self.assertEqual(201, client._last_response.status_code, 'Bagfile creation failed')
        self.assertRegex(guid,
                         '([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
                         'Invalid GUID has been returned')
        cleanup_db()

    def test_update_bagfile(self):
        cleanup_db()
        client = ContainerClient()
        bagfile_data, bagfile_data_post, bagfile_data_patch, metadata = generate_testdata()
        guid = client.create_bagfile(bagfile_data_post)
        client.update_bagfile(guid, bagfile_data_patch)
        self.assertEqual(200, client._last_response.status_code, 'Bagfile update failed')
        cleanup_db()

    def test_delete_bagfile(self):
        cleanup_db()
        client = ContainerClient()
        bagfile_data, bagfile_data_post, bagfile_data_patch, metadata = generate_testdata()
        guid = client.create_bagfile(bagfile_data)
        client.delete_bagfile(guid)
        self.assertEqual(200, client._last_response.status_code, 'Bagfile delete failed')
        cleanup_db()

    def test_add_bagfile_custom_attributes(self):
        cleanup_db()
        client = ContainerClient()
        bagfile_data, bagfile_data_post, bagfile_data_patch, metadata = generate_testdata()
        custom_attributes_dict = generate_custom_attributes()
        guid = client.create_bagfile(bagfile_data)
        client.add_bagfile_custom_attributes(guid, custom_attributes_dict)
        self.assertEqual(200, client._last_response.status_code, 'Bagfile custom attributes added')
        cleanup_db()

    def test_add_bagfile_state_event(self):
        cleanup_db()
        client = ContainerClient()
        bagfile_data, bagfile_data_post, bagfile_data_patch, metadata = generate_testdata()
        state_data = generate_state_testdata()
        guid = client.create_bagfile(bagfile_data)
        client.add_bagfile_state_event(guid, state_data)
        self.assertEqual(200, client._last_response.status_code, 'Bagfile state added')
        cleanup_db()

    def test_create_childbagfile(self):
        cleanup_db()
        client = ContainerClient()
        childbagfile_data = generate_childbagfile_testdata()
        guid = client.create_childbagfile(childbagfile_data)
        self.assertEqual(201, client._last_response.status_code, 'Child bagfile creation failed')
        self.assertRegex(guid,
                         '([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
                         'Invalid GUID has been returned')
        cleanup_db()

    def test_add_childbagfile_custom_attributes(self):
        cleanup_db()
        client = ContainerClient()
        childbagfile_data, childbagfile_data_post, childbagfile_data_patch, metadata = generate_testdata()
        custom_attributes_dict = generate_custom_attributes()
        guid = client.create_childbagfile(childbagfile_data)
        client.add_childbagfile_custom_attributes(guid, custom_attributes_dict)
        self.assertEqual(200, client._last_response.status_code, 'ChildBagfile custom attributes added')
        cleanup_db()

    def test_add_metadata(self):
        cleanup_db()
        client = ContainerClient()
        bagfile_data, bagfile_data_post, bagfile_data_patch, metadata = generate_testdata()
        guid = client.create_bagfile(bagfile_data)
        metadata['bagfile_guid'] = guid
        extractor_guids = client.submit_metadata(metadata)
        res = client._last_response
        self.assertEqual(201, res.status_code, 'Adding Metadata failed')
        if isinstance(extractor_guids, list):
            for guid in extractor_guids:
                self.assertRegex(guid,
                                 '([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
                                 'Invalid GUID has been returned')
        else:
            self.assertRegex(extractor_guids,
                             '([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
                             'Invalid GUID has been returned')

        cleanup_db()

    def test_add_delete_metadata(self):
        cleanup_db()
        client = ContainerClient()
        bagfile_data, bagfile_data_post, bagfile_data_patch, metadata = generate_testdata()
        guid = client.create_bagfile(bagfile_data)
        metadata['bagfile_guid'] = guid
        guid = client.submit_metadata(metadata)
        self.assertEqual(201, client._last_response.status_code, 'Adding Metadata failed')
        self.assertRegex(guid,
                         '([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
                         'Invalid GUID has been returned')
        client.delete_extractor(guid)
        self.assertEqual(200, client._last_response.status_code, 'Deletion of Metadata is failed')

        cleanup_db()

    def test_get_snippet_bva(self):
        cleanup_db()

        sequences = [generate_test_sequence(x) for x in range(0, 10)]
        connector = DBConnector(
            testconfig.db_hostname, testconfig.db_port, testconfig.db_name,
            testconfig.db_user, testconfig.db_key, testconfig.db_protocol)
        with connector as con:
            con.add_sequences(sequences)
        client = ContainerClient()
        bagfile_guid = '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'
        bva_limit = 6
        snippets = client.get_snippets_bva(bagfile_guid, bva_limit, extractors=None)

        expected_values = [(1475272800000, 1475272807999, 0, 7),
                           (1475272809000, 1475272809999, 9, 9)]
        actual_values = [(x['start'], x['end'], x['parent_index_start'], x['parent_index_end']) for x in snippets]

        self.assertCountEqual(expected_values, actual_values, 'Snippet BVA post failed')

        self.assertEqual(bagfile_guid, snippets[1]['parent_guid'], 'Snippet BVA post failed')

        cleanup_db()

    def test_get_snippet_bva_extractors(self):
        cleanup_db()

        sequences = [generate_test_sequence(x) for x in range(0, 10)]
        connector = DBConnector(
            testconfig.db_hostname, testconfig.db_port, testconfig.db_name,
            testconfig.db_user, testconfig.db_key, testconfig.db_protocol)
        with connector as con:
            con.add_sequences(sequences)
        client = ContainerClient()
        bagfile_guid = '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'
        bva_limit = 6
        snippets = client.get_snippets_bva(bagfile_guid, bva_limit, extractors=['ext1'])

        expected_values = [(1475272800000, 1475272800999, 0, 0),
                           (1475272804000, 1475272807999, 4, 7)]
        actual_values = [(x['start'], x['end'], x['parent_index_start'], x['parent_index_end']) for x in snippets]

        self.assertCountEqual(expected_values, actual_values, 'Snippet BVA post failed')

        self.assertEqual(bagfile_guid, snippets[1]['parent_guid'], 'Snippet BVA post failed')

        cleanup_db()

    def test_get_filters(self):

        client = ContainerClient()
        res_json = client.get_filters()

        self.assertIsInstance(res_json, dict)
        self.assertIsInstance(res_json.get('duration_min_max', None), list)
        self.assertIsInstance(res_json.get('locations', None), list)
        self.assertIsInstance(res_json.get('size_min_max', None), list)
        self.assertIsInstance(res_json.get('topics', None), list)
        self.assertIsInstance(res_json.get('vehicle_ids', None), list)


if __name__ == '__main__':
    unittest.main()
