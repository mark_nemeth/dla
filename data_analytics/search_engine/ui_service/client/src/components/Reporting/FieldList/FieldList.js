import React from 'react';
import { Field } from './Field/Field';
import './FieldList.scss';
import { Card, Row, Col } from 'react-bootstrap';

export function FieldList(props) {
  return (
    <Row className='mb-4'>
      <Col>
        <Card className='card-width-auto reporting-field-card'>
          <Card.Body>
            <Row>
              {props.fields.map(item => (
                <Field key={item.id} label={item.label} displayValue={item.value} />
              ))}
            </Row>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
