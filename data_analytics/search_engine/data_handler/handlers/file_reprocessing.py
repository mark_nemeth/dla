__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from handlers.file_check_handler import FileCheckHandler
from handlers.utils import utils
from proxies.metadata_api import MetadataApiProxy
from proxies.search_api import SearchApiProxy
from proxies.blob_storage import BlobStorageHandler

logger = logging.getLogger(__name__)


class FileReprocessingHandler:

    def __init__(self):
        self.metadata_api = MetadataApiProxy()
        self.search_api = SearchApiProxy()
        self.file_check_handler = FileCheckHandler()
        self.blob_storage_handler = BlobStorageHandler()

    def reprocess_all(self, paths):
        results = list()
        paths = list(set(paths))
        for path in paths:
            try:
                results.extend(
                    self._reprocess_file(path)
                )
            except Exception:
                logger.exception(f"failed to trigger reprocessing of {path}")
                results.append({'file_path': path,
                                'result': 'FAILED'})
        return results

    def _retrieve_files(self, path: str):
        '''
        retrieve files according to diff path/file type
        :param path:
        :return: bagfiles
        '''
        bagfiles = []
        if path.endswith("bag"):
            bagfiles = self.search_api.get_bagfiles_by_path(path)
        return bagfiles

    def _reprocess_file(self, path):
        bagfiles = self._retrieve_files(path)
        results = list()
        for bagfile in bagfiles:
            results.append(self._reprocess_bagfile(bagfile))
        return results

    def _reprocess_bagfile(self, bagfile):
        guid = bagfile['guid']
        if "current_link" in bagfile:
            path = bagfile['current_link']
        else:
            path = bagfile['link']
        try:
            if not self.blob_storage_handler.is_blob_available(path):
                raise FileNotFoundError(f'File cannot be found on path - {path}.')

            self.metadata_api.delete_bagfile_and_orphans(guid)
            bagfile_guid = self.metadata_api.upsert_bagfile(path,
                                                            bagfile['file_name'],
                                                            bagfile['file_type'],
                                                            bagfile['drive_types'],
                                                            "NOT_PROCESSED",
                                                            bagfile['processing_priority'])
        except Exception as e:
            logger.exception(f"failed to delete (for reprocessing) bagfile - {guid}, {path}, {e}")
            return {'file_path': path,
                    'bagfile_guid': guid,
                    'result': 'FAILED'}
        else:
            logger.info(f"successfully triggered reprocessing of {bagfile_guid}, {path}")
            return {'file_path': path,
                    'bagfile_guid': guid,
                    'result': 'SUCCESSFUL'}
