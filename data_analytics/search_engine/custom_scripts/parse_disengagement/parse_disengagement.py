#!/usr/bin/env python

"""Module implementing the ParseDisengagement class, used to
parse disengagement information from bagfiles.

The LogRequest topic (source: SEM) is used to identify AD
state changes from engaged to non-engaged.
Based on SEM definition:
* A disengagement event is defined if:
    -any change occur in eDegradation level, or
    -any change in eOperationMode
* The car is in the autonomous mode when:
    1) eSystemState = 2 (RUN)
    2) eOperationMode = 4 (Autonomous Driving)
    3) eDegradationMode is not changing and !=4

Because the eSystemStatus topic doesn't contain any velocity
information (compared to the old ADSTATE topic from CAL),

- Dsm topic data is used to compute car velocity (Vx)
- GPS topic data is used to estimate the lat/lon of the disengagement
(eg, useful to distinguish between private/public roads).

==========================
Fallback trajectory logic:
- Currently looking into /planning/TrajectoryBundleStats topic for
message.trajectory_bundle_statistics.chosen_task_name == 'FALLBACK'

"""

__copyright__ = '''
COPYRIGHT: (c) 2017-2018 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import subprocess
from collections import namedtuple
from datetime import datetime
import numpy as np
import sys
import os
import pandas as pd
from pyproj import Proj, transform
import rosbag
from shapely.geometry import shape, Point
import yaml
from scipy.stats import norm, gaussian_kde, entropy
import traceback

from utility import (ts_to_float, message_time_delta, charlist2str)

from ContainerClient import ContainerClient
from DANFUtils.tracing import tracing
from DANFUtils.logging import logger
from metadata_manager import MetadataManager
from DANFUtils.utils import utils as danfutils
from config.constants import EXTRACTOR_NAME, VERSION, SCHEMA_VERSION

LastValidGpsTimestamp = namedtuple('LastValidGpsTimestamp', 's ns')
LastValidGpsHeader = namedtuple('LastValidGpsHeader', 'timestamp')
LastValidGpsMessage = namedtuple('LastValidGpsMessage', 'header latitude longitude altitude')

# Define ecef and lat/lon/alt models.
ecef_model = Proj(proj='geocent', ellps='WGS84', datum='WGS84')
lla_model = Proj(proj='latlong', ellps='WGS84', datum='WGS84', radians=False)


class LocMessage(object):
    """Wrapper class around Localization messages from topic:
    /localization/LocalizationService/0/LocDataInterface"""

    def __init__(self, message=None):
        self.s = None
        self.ns = None
        self.timestamp = None
        self.latitude = None
        self.longitude = None
        self.altitude = None
        self.valid = True
        if message:
            self.validate_input(message)

    def lla_from_ecef(self, ecef):
        """Converts earth-centric coordinates (ecef) to lon, lat, alt.
        This was checked with LOC team. Then ecef at index 3, 7, and
        11 corresponds to ecef elements x, y, and z."""
        self.longitude, self.latitude, self.altitude = \
            transform(ecef_model, lla_model, *ecef[3:12:4])

    def validate_input(self, msg):
        try:
            self.s = msg.loc_state[0].timestamp.s
            self.ns = msg.loc_state[0].timestamp.ns
            self.lla_from_ecef(msg.loc_state[0].ecef_T_front_axle)
            self.valid = True
        except (AttributeError, KeyError) as err:
            logger.error('Localization message does not contain valid information'
                         ': {}'.format(err))
            self.valid = False
        except (IndexError, ValueError) as err:
            logger.error('Conversion from ecef to lat/lon/alt failed,'
                         'possibly due to malformed Localization message'
                         'at "loc_state[0].ecef_T_front_axle": {}'.format(err))
            self.valid = False

        if self.s and self.ns:
            self.ts_to_date()

    def ts_to_date(self):
        """Convert timestamp (posix) to date (datetime)"""
        self.timestamp = datetime.fromtimestamp(ts_to_float(self)).strftime("%Y-%m-%d %H:%M:%S")


class TimeStamp(object):
    """Wrapper class around Timestamp message from topic:
    /planning/TrajectoryBundleStats"""

    def __init__(self, message):
        self.msg = message
        self.s = None
        self.ns = None
        self.ts = None
        self.ts_d = None
        self.validate_input()

    def validate_input(self):
        """Get timestamp information in a robust way - sometimes these
        fields are empty or badly defined."""
        try:
            self.s = self.msg.header.stamp.secs
            self.ns = self.msg.header.stamp.nsecs
        except AttributeError as err:
            logger.error('Fallback event does not have valid timestamp: {}'
                         ''.format(err))
            pass
        if self.s and self.ns:
            self.ts = self.ts_to_date()

    def ts_to_date(self):
        """Convert timestamp (posix) to date (datetime)"""
        return datetime.fromtimestamp(ts_to_float(self)).strftime("%Y-%m-%d %H:%M:%S")


class ParseDisengagement(object):
    """
    Parse disengagement information using messages published on the Loc,
    LogRequest and EgoVehicleState topics (defined in config/config.yaml).

    The script generates [N] csv files, 10 scalars, and 2 summaries.

    Files:
    - {}_waypoints.csv: All fields associated with autonomous trips.

    Scalars:
    - number_of_disengagement: Total number of disengagements.
    - number_of_disengagement_types: Number of different disengagements.
    - distance_engaged: Total distance driving in AD mode.
    - duration_engaged: Total duration driving in AD mode.
    - disengagement_per_km_rate: Disengagement rate per driven km.
    - disengagement_per_mile_rate: Disengagement rate per driven mile.
    - disengagement_per_hour_rate: Disengagements rate per driven hour.
    - minutes_per_disengagement_rate: Disengagement rate in minutes.
    - number_of_fallbacks: Total number of fallback behavior events.
    - max_fallback_dur: Maximum duration for fallback behavior event.

    Summaries:
    - disengagement_distance_summary: Distance for AD trips.
    - disengagement_duration_summary: Duration for AD trips.
    """

    def __init__(self, bagfile_path='input/input_old.bag', debug=False):

        self.bagfile = None
        self.bagfile_path = bagfile_path

        self.invalid_gps_records = []
        self.prev_valid_loc_msg = None
        self.last_location_message = None
        self.last_valid_loc_message = None
        self.nr_disengagements = 0
        self.distance_tot_dict = {'velocity_sum': 0,
                                  'velocity_msg_no': 0}
        self.distance_tot = None
        self.duration_tot = None
        self.engagement_state = 0
        self.last_engagement_state = 0
        self.last_degradation_level = 0
        self.current_velocity = 0
        self.regions = []
        self.private_regions = None
        self.current_trip = []
        self.relevant_topics = {}
        self.log_requests = []
        self.trips = []
        self.waypoints = []
        self.fallbacks_ts = []
        self.params = {}
        self.summary = pd.DataFrame()
        self.scalars = []
        self.summaries = []
        self.result_code = 0
        self.result_code_str = 'Using latest parser. Last run on: {} UTC.' \
                               ''.format(datetime.utcnow())

        self.initialize_location_message()

        if not debug:
            logger.set_log_level("info")

    def run(self, guid, client):

        logger.info("checking if bagfile exists (1/11)")
        if not self.bagfile_path:
            logger.error('Skipping processing: missing bagfile argument.')
            return

        if not os.path.exists(self.bagfile_path):
            logger.error('Skipping processing: bagfile is not where bagfile argument is.')
            return

        logger.info("loading regions (2/11)")
        self.load_regions()

        logger.info("loading parser parameters (3/11)")
        self.load_parser_parameters()

        logger.info("stripping bagfile (4/11)")
        self.strip_bagfile()

        logger.info("checking if stripped bagfile exists (5/11)")
        if not os.path.exists(self.bagfile_path):
            logger.info("no stripped bagfile exists -> no relevant topics were found, no disengagement, no autonomous miles")
            self.result_code = 700
            self.result_code_str += '\n[700]Missing topics: {}' \
                .format(', '.join(set(self.relevant_topics.values())))
            logger.info('Skipping Disengagement. {}'.format(self.result_code_str.strip()))
            self.send_not_engaged(client)
            return

        logger.info("loading bagfile (6/11)")
        self.load_bagfile()

        logger.info("check if bagfile has all topics (7/11)")
        if not self.check_bagfile_has_necessary_topics(client):
            return

        logger.info("iterating through bagfile (8/11)")
        self.parse_disengagements()

        logger.info("merging trips and logrequests (9/11)")
        self.merge_trips_and_logrequests()

        logger.info("making summary and sending (10/11)")
        self.compute_scalars(guid, client)
        logger.info("finished, tracing is next and last step (11/11)")

    def strip_bagfile(self):
        if not os.path.exists("stripping_dir/"):
            os.mkdir("stripping_dir/")
        subprocess.check_call([
            "dla_converter/dla_converter",
            "--in", self.bagfile_path,
            "--whitelist", "dla_converter/whitelist.txt",
            "--out", "stripping_dir/" + os.path.basename(self.bagfile_path)
        ])
        self.bagfile_path = "stripping_dir/" + os.path.basename(self.bagfile_path)

    def load_parser_parameters(self):
        """Read relevant topics and parameters from the yaml config."""
        try:
            with open('config/config.yaml', "rb") as file:
                stream = file.read()
            self.params = yaml.safe_load(stream)['parse_disengagement']
            self.relevant_topics = self.params['TOPICS']
        except (KeyError, AttributeError, yaml.YAMLError) as exception:
            msg = '\nError while reading from disengagement config file:' \
                  '{}'.format(exception)
            logger.error(msg.rstrip())
            self.result_code = 1
            self.result_code_str += msg

    def check_bagfile_has_necessary_topics(self, client) -> bool:
        """Verify that the required topics are present. Planning topic
        is used only for fallback estimation and is not required."""
        self.duration_tot = self.bagfile.get_end_time() - self.bagfile.get_start_time()
        type_topic_info = self.bagfile.get_type_and_topic_info()
        bag_topics = set(type_topic_info.topics.keys())
        required_topics = set(self.relevant_topics.values())
        missing_topics = required_topics - bag_topics
        if len(missing_topics):
            self.result_code = 700
            self.result_code_str += '\n[700]Missing topics: {}' \
                .format(', '.join(missing_topics))
            logger.info('Skipping Disengagement. {}'.format(self.result_code_str.strip()))
            self.send_not_engaged(client)
            return False
        else:
            return True

    def load_bagfile(self):
        """load the bagfile in bagfile_path and store it in bagfile"""
        self.bagfile = rosbag.Bag(self.bagfile_path)

    def load_regions(self):
        """Load GeoJSON from regions and private_regions. The file contains the
        regions that are considered private areas"""
        private_areas_file = os.path.join('config',
                                          'private_testing_grounds',
                                          'geofences.geojson')
        try:
            with open(private_areas_file, "rb") as file:
                stream = file.read()
            self.private_regions = yaml.safe_load(stream)
        except (IOError, AttributeError, yaml.YAMLError) as exception:
            msg = "Error reading {} file.\n{}.".format(private_areas_file, exception)
            self.result_code = 1
            self.result_code_str += msg
            logger.error(msg)

        # Get all geojson files in the regions folder
        region_de = os.path.join('config', 'regions', 'germany.geojson')
        region_us = os.path.join('config', 'regions', 'california.geojson')
        for region_file in [region_de, region_us]:
            with open(region_file, "rb") as file:
                stream = file.read()
            region = yaml.safe_load(stream)
            if 'features' in region and len(region['features']) == 1:
                self.regions.append(region['features'][0])
            else:
                logger.error("Unable to extract region features from {}."
                             .format(region_file))

    def get_region(self, longitude, latitude):
        """Returns a dictionary with name, country and state if
        applicable. Otherwise returns None, None"""
        country, state = None, None
        for region in self.regions:
            polygon = shape(region['geometry'])

            # check if the coordinates fall into the region
            if polygon.contains(Point(longitude, latitude)):
                properties = region['properties']

                # extract country and state from the regions properties
                country = properties['country']
                state = None
                if 'state' in properties and not properties['state'] == '':
                    state = properties['state']
                    return country, state
                break
        return country, state

    def tag_geolocation(self, row):
        """Checks if the mean latitude and longitude of a given row falls
        into a private/public region, and assign a geolocation based on
        geoJSON file. This is also used to tag each second in the trip as
        counting towards public or private road driving."""

        if self.private_regions is None:
            logger.error('No private testing grounds loaded')

        # construct point based on lat/long returned by geocoder
        point = Point(row['longitude']['mean'], row['latitude']['mean'])

        # Use the first-defined geolocation to associate the trip to private/public
        # areas, but iterate through all geolocations to append multiple geotags.

        private, geofence = None, []
        # check each polygon to see if it contains the point
        for feature in self.private_regions['features']:
            polygon = shape(feature['geometry'])
            if polygon.contains(point):
                if private is None:
                    private = feature['properties']['private']
                geofence.append(feature['properties']['name'])

        row['in_private'] = private or False
        row['geofence'] = geofence
        return row

    def handle_logrequest(self, lr):
        """Add a LogRequest to the global list of LogRequests."""
        # Only kDisengagementManual (0) and kDisengagementError (1) are
        # relevant for the scope of reporting autonomous trips to the DMV.
        # Also: Filter only messages from SEM (with source == 11).
        if (lr.cause == 0 or lr.cause == 1) and lr.source == 11:
            log_request_line = dict(ts=ts_to_float(lr.header.timestamp),
                                    ts_s=lr.header.timestamp.s,
                                    ts_ns=lr.header.timestamp.ns,
                                    cause=lr.cause,
                                    source=lr.source,
                                    details=charlist2str(lr.details))

            self.log_requests.append(log_request_line)

    def handle_ego_vehicle_state(self, evs):
        """Handle ego vehicle state. Based on engagement_state transitions
        we either start a trip, append to a trip or end a trip."""
        header_ts = evs.header.timestamp

        degradation_level = evs.eSystemStatus.eDegradationLevel
        operation_mode = evs.eSystemStatus.eOperationMode
        system_state = evs.eSystemStatus.eSystemState
        self.engagement_state = \
            (system_state == self.params['STATES']['SEM_SYSTEM_STATE_RUN']) and \
            (operation_mode == self.params['STATES']['SEM_OP_MODE_AD']) and \
            (degradation_level != self.params['STATES']['SEM_DEGRAD_MODE_DISENG'])

        if self.last_engagement_state:
            self.engagement_state = self.engagement_state and \
                                    (degradation_level == self.last_degradation_level)

        if self.engagement_state and (not self.last_engagement_state):
            # Transition into engaged, create a new empty current_trip list
            logger.debug("{}.{} Engaged".format(header_ts.s, header_ts.ns))
            # reset current trip
            self.current_trip = []
            self.create_vehicle_aggr_state(evs)
        elif self.engagement_state and self.last_engagement_state:
            # In state 'engaged', add messages to current trip
            self.create_vehicle_aggr_state(evs)
        elif (not self.engagement_state) and self.last_engagement_state:
            # Transition from 'engaged' to 'disengaged' aka. 'disengagement'
            logger.debug("{}.{} Disengaged".format(header_ts.s, header_ts.ns))
            self.create_vehicle_aggr_state(evs)
            if self.result_code == 0:
                self.create_trip()

        # store current engagement state and degradation level for tracking
        self.last_engagement_state = self.engagement_state
        self.last_degradation_level = degradation_level

    def handle_localization(self, msg):
        """Create lat/lon message from localization topic that also stores
        previous VALID message. When timestamps (0, 0) are encountered, the
        previous location message is used."""
        # Save last message from localization
        loc_msg = LocMessage(msg)
        self.last_location_message = loc_msg
        if not loc_msg.valid:
            logger.debug("Invalid message from localization.")
            return
        self.last_valid_loc_message = loc_msg

    def handle_velocity(self, msg):
        """Read and store longitudinal velocity for later computation of distance
        travelled, as well as the end velocity of AD trips"""
        self.current_velocity = msg.output_data.vmo.fvx
        self.distance_tot_dict['velocity_sum'] += self.current_velocity
        self.distance_tot_dict['velocity_msg_no'] += 1

    def initialize_location_message(self):
        """Initialize location message. This becomes necessary when multiple initial
        gps messages are None (invalid)."""
        self.last_valid_loc_message = LocMessage()

    def parse_disengagements(self):
        """Parse the disengagement and vehicle state information needed
        to find all autonomous trips recorded in the bagfile."""
        if self.result_code:
            # Do not execute if upstream errors.
            return

        try:
            for topic, message, timestamp in self.bagfile.read_messages(
                    topics=self.params['TOPICS'].values()):
                if topic == self.params['TOPICS']['localization']:
                    self.handle_localization(message)
                elif topic == self.params['TOPICS']['dsm_odometry']:
                    self.handle_velocity(message)
                elif topic == self.params['TOPICS']['log_request']:
                    self.handle_logrequest(message)
                elif topic == self.params['TOPICS']['ego_state']:
                    self.handle_ego_vehicle_state(message)
                    if self.result_code != 0:
                        return
            self.check_missing_disengagement()
        except Exception as exception:
            msg = "Failing exception: {}".format(exception)
            self.result_code = 1
            self.result_code_str += msg
            logger.error(msg)
            raise exception

    def check_missing_disengagement(self):
        """Catch if ego engaged at end of recording. If it did, create
        the trip. Note, this will not count as disengagement, but the
        trip will be stored in the database."""
        self.nr_disengagements = len(self.trips)
        if self.last_engagement_state:
            error_msg = "\n[703]Ego engaged and did not disengage during last" \
                        " trip of recording. Disengagement not counted."
            logger.debug(error_msg)
            self.result_code_str += error_msg
            self.create_trip()

    def create_vehicle_aggr_state(self, evs):
        """Taking a egovehicle state and location message as input this
        method creates a new vehicle state aggregated by second by merging
        the evs and location_message into a dictionary and appending it
        to the current_trip."""
        header_ts = evs.header.timestamp
        location_message = self.last_location_message
        vehicle_aggr_state = dict(ts=ts_to_float(header_ts),
                                  ts_s=header_ts.s,
                                  ts_ns=header_ts.ns,
                                  velocity=self.current_velocity,
                                  engagement_state=self.engagement_state,
                                  loc_ts=None,
                                  latitude=0,
                                  longitude=0,
                                  altitude=0,
                                  loc_ts_diff=0.0,
                                  loc_error=0
                                  )

        # Check if a location message exists and is valid (delta < 0.5s)
        ts_lgr = "{}.{}".format(evs.header.timestamp.s, evs.header.timestamp.ns)
        if (location_message is not None) and location_message.valid:
            delta = message_time_delta(location_message, header_ts, header=True)
            ts_gps = "{}.{}".format(location_message.s, location_message.ns)
            if ts_gps == '0.0':
                # gps has invalid header.timestamp. Previous valid location message
                # is used and the evaluation continues.
                vehicle_aggr_state['loc_error'] = 705
                self.invalid_gps_records.append(ts_lgr)

                if not self.last_valid_loc_message:
                    delta = 0
                    error_msg = "\n[705]Invalid loc message (ts=0.0) at " \
                                "LogRequest timestamp: {}. No older " \
                                "valid LOC message was found.".format(ts_lgr)
                else:
                    delta = message_time_delta(self.last_valid_loc_message, header_ts, header=True)
                    location_message = self.last_valid_loc_message
                    error_msg = "\n[705]Invalid LOC message (ts=0.0) at " \
                                "LogRequest timestamp: {}. Using last " \
                                "valid loc message at timestamp: {}.{} " \
                                "(delta with LogRequest ts: {}). " \
                        .format(ts_lgr, location_message.s, location_message.ns, delta)
                logger.debug(error_msg.rstrip())
                self.result_code_str += error_msg
        else:
            vehicle_aggr_state['loc_error'] = 705
            delta = 0
            error_msg = "\n[705]No LOC message found at " \
                        "LogRequest timestamp: {}.".format(ts_lgr)
            logger.debug(error_msg.rstrip())
            self.result_code_str += error_msg

        if delta >= self.params['MAX_EGO_STATE_LOC_MESSAGE_DELTA']:
            # gps message is not null, but the delta is too large and would
            # produce inaccurate results.
            vehicle_aggr_state['loc_error'] = 701
            error_msg = "\n[701]Delta between timestamp from LOC message and " \
                        "timestamp of disengagement from LogRequest is greater" \
                        " than tolerance ({}s): {}s." \
                .format(self.params['MAX_EGO_STATE_LOC_MESSAGE_DELTA'], delta)
            logger.debug(error_msg.rstrip())
            self.result_code_str += error_msg

        vehicle_aggr_state['loc_ts'] = ts_to_float(location_message)
        vehicle_aggr_state['latitude'] = location_message.latitude
        vehicle_aggr_state['longitude'] = location_message.longitude
        vehicle_aggr_state['altitude'] = location_message.altitude
        vehicle_aggr_state['loc_ts_diff'] = delta

        self.current_trip.append(vehicle_aggr_state)

    def create_trip(self):
        """Summarizes the current trip into a trip entry
        and adds it to the global trips"""
        trip = pd.DataFrame(self.current_trip)

        # Group by the seconds field and aggregate means and group sizes
        groups = trip.groupby(trip['ts_s']).agg(['mean', 'size'])

        # We expect to receive EGO_STATE_MESSAGE_FREQUENCY samples per second
        groups['ratio'] = groups.velocity['size'] / self.params['EGO_STATE_MESSAGE_FREQUENCY']
        # Velocity is in m/s. Average velocity per second is the distance
        # travelled in that second. Note: this is sensitive to message drop
        groups['distance'] = groups.velocity['mean'] * groups['ratio']
        trip_distance = groups.distance.sum()
        groups = groups.apply((lambda x: self.tag_geolocation(x)), 1)
        trip_distance_public = \
            groups[groups.in_private == False].distance.sum()
        trip_end = trip.tail(1)

        self.trips.append(
            dict(distance=trip_distance,
                 end_ts_f=trip_end.ts.item(),
                 end_ts_s=trip_end.ts_s.item(),
                 end_ts_ns=trip_end.ts_ns.item(),
                 end_planned=0,
                 manual_unplanned=0,
                 distance_public=trip_distance_public,
                 end_public_road=(groups.tail(1).in_private.item() == 0),
                 end_details=None,
                 geofence=groups.tail(1).geofence.values[0]
                 ))

    def merge_trips_and_logrequests(self):
        """Merges data from logrequests with trips based on the
        logrequest header and trips end_timestamp."""
        if self.result_code:
            # Do not execute if upstream errors.
            return

        if len(self.log_requests) == 0 and len(self.trips) != 0:
            # Should this string say "AD" instead of "AV"?
            error_msg = ("\n[704]Ego engaged and disengaged from AV mode "
                         "but no log request messages were recorded in bagfile.")
            logger.error(error_msg.rstrip())
            self.result_code_str += error_msg
            self.result_code = 704
            return

        log_requests = pd.DataFrame(self.log_requests)
        trips = pd.DataFrame(self.trips)

        for idx, row in trips.iterrows():
            # get index of closest log_request
            log_request_index = (log_requests['ts'] - row.end_ts_f).abs().argsort()[:1]
            closest_lr = log_requests.loc[log_request_index]
            diff = (row.end_ts_f - closest_lr.ts).values[0]

            # if the lever was pushed, the end was planned
            details = closest_lr.details.item()
            end_planned = "[51]Distronic lever pushed" in details
            possible_unplanned_details = ["[52]Accelerator pedal pressed", "[53]Brake pedal pressed",
                                          "[54]Steering takeover", "[55]Acceleration or brake pedal pressed"]


            # we allow for some tolerance, in case a message was missed,
            # ideally the delta is supposed to be 0 at all times
            if abs(diff) < self.params['MAX_TRIP_END_LOG_REQUEST_DELTA']:
                if any(item in details for item in possible_unplanned_details):
                    trips.loc[idx, 'manual_unplanned'] = True
                trips.loc[idx, 'end_planned'] = end_planned
                trips.loc[idx, 'end_details'] = details
            else:
                error_msg = ('\n[702]No matching logrequest found at {}.{}.'
                             .format(trips.loc[idx, 'end_ts_s'],
                                     trips.loc[idx, 'end_ts_ns']))
                logger.error(error_msg.rstrip())
                self.result_code = 702
                self.result_code_str += error_msg

        self.summary = trips

    def compute_scalars(self, guid: str, client):
        """Compute scalars from disengagement entries."""

        if self.summary.empty:
            self.send_not_engaged(client)
            return

        trips = self.summary
        diseng_tot = self.nr_disengagements
        planned_diseng = trips.end_planned.sum()
        manual_unplanned_diseng = len(trips[(trips.end_planned == False) & (trips.manual_unplanned == True)])
        error_diseng = diseng_tot - planned_diseng - manual_unplanned_diseng
        distance_ad = trips.distance.sum()
        if isinstance(error_diseng, np.generic):
            error_diseng = error_diseng.item()
        if isinstance(manual_unplanned_diseng, np.generic):
            manual_unplanned_diseng = manual_unplanned_diseng.item()
        distance_ad_public = trips.distance_public.sum()
        total_public_diseng = len(trips[(trips.end_public_road == True)])
        diseng_public = len(trips[(trips.end_planned == False) & (trips.end_public_road == True) & (trips.manual_unplanned == False)])
        manual_unplanned_diseng_public = len(trips[(trips.end_planned == False) & (trips.manual_unplanned == True) & (trips.end_public_road == True)])
        end_details = trips.end_details.to_list()
        geofence_details = trips.geofence.to_list()

        logger.info("Trying to send a bagfile custom attributes update request")
        client.add_bagfile_custom_attributes(guid, EXTRACTOR_NAME, {"schema_version": SCHEMA_VERSION,
                                                                    "docker_image_name": EXTRACTOR_NAME,
                                                                    "docker_image_version": VERSION,
                                                                    "number_of_total_disengagements": diseng_tot,
                                                                    "number_of_error_disengagements": error_diseng,
                                                                    "number_of_manual_unplanned_disengagements": manual_unplanned_diseng,
                                                                    "total_engaged_distance": distance_ad,
                                                                    "engaged_distance_in_public": distance_ad_public,
                                                                    "number_of_public_total_disengagements": total_public_diseng,
                                                                    "number_of_public_error_disengagements": diseng_public,
                                                                    "number_of_public_manual_unplanned_disengagements": manual_unplanned_diseng_public,
                                                                    "end_details": end_details,
                                                                    "geofence_details": geofence_details,
                                                                    "average_mileage": distance_ad / max(error_diseng,
                                                                                                         1)})
        logger.info("Send request succeeded")

    @staticmethod
    def send_not_engaged(client):
        logger.info("Trying to send a bagfile custom attributes update request")
        client.add_bagfile_custom_attributes(guid, EXTRACTOR_NAME, {"schema_version": SCHEMA_VERSION,
                                                                    "docker_image_name": EXTRACTOR_NAME,
                                                                    "docker_image_version": VERSION,
                                                                    "number_of_total_disengagements": 0,
                                                                    "number_of_error_disengagements": 0,
                                                                    "number_of_manual_unplanned_disengagements": 0,
                                                                    "total_engaged_distance": 0,
                                                                    "engaged_distance_in_public": 0,
                                                                    "number_of_public_total_disengagements": 0,
                                                                    "number_of_public_error_disengagements": 0,
                                                                    "number_of_public_manual_unplanned_disengagements": 0,
                                                                    "end_details": [],
                                                                    "geofence_details": [],
                                                                    "average_mileage": 0})
        logger.info("Send request succeeded")


def sample_negentropy(sequence):
    """Calculate an approximate negentropy for the kde of samples.

    This negentropy is calculated for a Gaussian Kernel Density Estimation
    modelling the samples.  Negentropy of a distribution d is defined as
     S(g) - S(d) >= 0
    where g is the normal distribution with the same mean and std as d.
    Note that this approximation fails to be non-negative.
    """
    mean = np.mean(sequence)
    std = np.std(sequence)
    if std > 0:
        gaussian = norm(mean, std)
        kde = gaussian_kde(sequence)
        lo = mean - 10 * std
        hi = mean + 10 * std
        n = 1001
        xs = np.linspace(lo, hi, n)
        return entropy(gaussian.pdf(xs)) - entropy(kde.evaluate(xs))
    else:
        return 0.0


def sample_statistics(sequence, percentiles=None, negentropy=True):
    """Provide summary statistics for samples of Euclidean data.
    Sequence is a numpy array and percentiles is a list of quantiles
    Percentiles can be specified, for example by
    quantiles=[100, 99, 95, 66, 50, 33, 5, 1, 0].
    This would include max (for 100th percentile), the median
    (for the 50th percentile), and the minimum (for the 0th percentile).
    Default percentiles correspond to sigma, 2 sigma, and 3 sigma
    of a Gaussian distribution, and to min, max, and median.

    Non-gaussianity is measured as an approximate negentropy.
    """
    if not sequence.size:
        return {}
    if percentiles is None:
        percentiles = sorted([100, 50, 0] + list(map(lambda x: 100 * norm.cdf(x), [-3, -2, -1, 1, 2, 3])), reverse=True)

    if type(sequence) is np.ndarray:
        sequence_length = sequence.size
    else:
        sequence_length = len(sequence)
    return {
        'n_samples': sequence_length,
        'mu': np.mean(sequence),
        'sigma': np.std(sequence),
        'min': np.min(sequence),
        'max': np.max(sequence),
        'quartiles': list(np.percentile(sequence, [25, 50, 75])),
        'percentiles': percentiles,
        # np.percentile performs linear interpolation between samples.
        # This can be changed in version 1.9.0 or later.
        'percentile_values': list(np.percentile(sequence, percentiles)),
        # sample_negentropy can be computationally too expensive for long sequences
        'non_gaussianity': sample_negentropy(sequence) if negentropy else None
    }


if __name__ == "__main__":
    logger.info("starting disengegamgent report script")
    step = "disengagement-report"
    tracing.start_task(step)
    metadata_manager = MetadataManager()
    if not os.path.exists(os.path.abspath(os.path.dirname(__file__)) + '/logs'):
        os.makedirs(os.path.abspath(os.path.dirname(__file__)) + '/logs')
    logger.set_logging_folder(
        os.path.abspath(os.path.dirname(__file__)) + '/logs')
    logger.set_log_level('info')
    started_at = danfutils.get_timestamp()
    guid = os.environ["BAGFILE_GUID"]
    bag_path = os.environ["BAGFILE_PATH"]
    try:
        client = ContainerClient()

        parser = ParseDisengagement
        parser(bagfile_path=bag_path).run(guid, client)
    except:
        tracing.task_attribute("fatalError", traceback.format_exc())
        tracing.end_task(success=False)
        metadata_manager.submit_state_updates(guid, os.environ["FLOW_RUN_GUID"], "FAILED", started_at)
        sys.exit(1)
    else:
        tracing.end_task()
        metadata_manager.submit_state_updates(guid, os.environ["FLOW_RUN_GUID"], "SUCCESSFUL", started_at)
