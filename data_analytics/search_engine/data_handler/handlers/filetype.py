__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import re
from enum import Enum

from config.config_handler import config_dict


class BagfileType(Enum):
    UNKNOWN = 0
    ALWAYSON = 1
    ORIGINAL = 2
    DML_CV1 = 3
    SNIPPET = 4


_FILE_TYPE_WHITELIST = {BagfileType[x] for x in config_dict.get_value('file_type_whitelist')}
_FILE_NAME_PATTERNS = config_dict.get_value('file_name_to_type')


def is_file_type_whitelisted(file_type: BagfileType) -> bool:
    """
    Decides whether bagfiles of the specified BagfileType should be processed
    by the data pipeline.

    :param file_type: BagfileType to check
    :return: True iff types of the given BagfileType should be processed
    """
    assert isinstance(file_type, BagfileType), 'file_type has to be of type BagfileType'
    return file_type in _FILE_TYPE_WHITELIST


def determine_bagfile_type(file_name: str) -> BagfileType:
    """
    Determines the BagfileType of a bagfile based on the
    file name.

    :param file_name: Of the bagfile to classify
    :return: BagfileType
    """
    for pattern in _FILE_NAME_PATTERNS:
        if re.search(pattern, file_name):
            file_type = _FILE_NAME_PATTERNS[pattern]
            return BagfileType[file_type]

    # if no file name pattern matches, return UNKNOWN as default
    return BagfileType.UNKNOWN