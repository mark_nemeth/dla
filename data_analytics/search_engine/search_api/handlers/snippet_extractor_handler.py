"""Handler class for snippet extractor"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

from DANFUtils.utils import generate_guid
from packaging.version import parse, Version

from config.config_handler import config_dict
from handlers.db_data_handler import DBDataHandler
from handlers.utils import utils

logger = logging.getLogger(__name__)

EXTRACTOR_EVENT_WHITELIST = config_dict.get_value('extractor_event_whitelist')


class SnippetExtractorHandler:
    def __init__(self):
        self.db_data_handler = DBDataHandler()

    def get_snippets_extractor(self,
                               bagfile_guid,
                               bva_limit=0,
                               extractors=None,
                               generate_child_bagfiles=True):
        """
        Generates snippets for a bagfile based on extractor metadata

        :param bagfile_guid: guid of bagfile to be processed
        :param bva_limit: int minimum BVA value to filter sequences
        :param gps_subsampling_ratio: int subsampling ratio for gps_track aggregation
        :param extractors: if a list of extractors is provided then process only the metadata of these
        :param generate_child_bagfiles: bool to generate child bagfiles or not to generate it
        :return: list of child bagfile JSON metadata
        """

        # retrieve bagfile and extractor requests for bagfile from db
        bagfile = self.db_data_handler.get_bagfiles([bagfile_guid])[0]
        extracted_events = self.db_data_handler.search_extractor_requests(
            query={"bagfile_guid": bagfile_guid}, limit=1000, sortby="start")

        # create child bagfile dicts
        child_bagfiles = []
        for event in extracted_events:
            # filter by extractor_id if extractors param is not None
            if extractors is not None and event.get(
                    'extractor_id', '') not in extractors:
                continue
            # filter by bva_limit
            if event.get('bva', 0) < bva_limit:
                continue

            # filter by extractor event type
            extractor_event_type = event.get('event_type', None)
            if extractor_event_type is not None and extractor_event_type not in EXTRACTOR_EVENT_WHITELIST:
                continue

            # Has to be removed as soon as old extractor events (< 1.0) are removed
            # Ensures that legacy and new extracted event generate the same bagfile structure
            event_version = parse(event.get('version', "0.0"))
            if event_version < Version("1.0"):
                child_bag = self._parse_extractor_event_legacy(event,
                                                               bagfile_guid,
                                                               bagfile)
            else:
                child_bag = self._parse_extractor_event(event, bagfile_guid,
                                                        bagfile)

            child_bagfiles.append(child_bag)

        if generate_child_bagfiles:
            # add child bagfiles to db if non-empty
            if child_bagfiles:
                self.db_data_handler.add_child_bagfiles(child_bagfiles)
            else:
                logger.info(
                    f"No child bagfiles found for bagfile {bagfile_guid} with bva limit {bva_limit}."
                )

        return child_bagfiles

    def _parse_extractor_event_legacy(self, event, bagfile_guid, bagfile):
        child_bag = {
            'version': '1.0',
            'guid': generate_guid(),
            'parent_guid': bagfile_guid,
            'start': event.get('start', 0),
            'end': event.get('end', 1),
            'parent_index_start': utils.timestamp_to_sequence_index(
                event.get('start', 0), bagfile.get('start', 0)),
            'parent_index_end': utils.timestamp_to_sequence_index(
                event.get('end', 0), bagfile.get('start', 0)),
            'bva_max': event.get('bva', 0),
            'metadata': event.get('metadata', {})
        }
        return child_bag

    def _parse_extractor_event(self, event, bagfile_guid, bagfile):
        metadata = event.get('metadata', {})
        metadata.update({'ts': event.get('ts', 0)})
        child_bag = {
            'version': '1.0',
            'guid': generate_guid(),
            'parent_guid': bagfile_guid,
            'start': event.get('start', 0),
            'end': event.get('end', 1),
            'parent_index_start': utils.timestamp_to_sequence_index(
                event.get('start', 0), bagfile.get('start', 0)),
            'parent_index_end': utils.timestamp_to_sequence_index(
                event.get('end', 0), bagfile.get('start', 0)),
            'bva_max': event.get('bva', 0),
            'metadata': {event.get('extractor_id', ''): metadata}
        }
        return child_bag
