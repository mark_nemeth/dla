import {OverviewReducer} from './OverviewReducers';
import {fetchFileByGuidError, fetchFileByGuidSuccess} from './OverviewActions'
import {getTestOriginalFileList, getTestChildBagfileList} from '../../setupTests';
import {insertString} from '../../assets/string/stringHelper';
import strings from '../../assets/string/default';


describe('Overview reducers', () => {

  it('should return the initial state', () => {
    expect(OverviewReducer({overview: {}}, {}))
      .toEqual({overview: {}});
  });

  describe('fetch file by guid', () => {

    it('should handle fetchFileByGuidError', () => {
      const errorMsg = insertString(strings.no_bagfile_error);
      expect(OverviewReducer({overview: {}}, {
        type: fetchFileByGuidError.type,
        payload: errorMsg
      })).toEqual({
        error: true,
        errorMessage: errorMsg,
        errorMessageType: 'danger',
        overview: {
          bagfile: {},
          childbagfiles: {}
        }
      });
    });

    it('should handle fetchFileByGuidSuccess', () => {
      const bagfilePayload = getTestOriginalFileList(true);
      const childbagfilePayload = getTestChildBagfileList();
      expect(OverviewReducer({overview: {}}, {
        type: fetchFileByGuidSuccess.type,
        payload: {bagfile: bagfilePayload, childbagfiles: childbagfilePayload}
      })).toEqual({
        error: false,
        overview: {
          bagfile: bagfilePayload,
          childbagfiles: childbagfilePayload
        }
      });
    });
  });
});