import {connect} from 'react-redux';
import {Table} from './Table';
import {
  sortBy,
  selectTableEntry,
  setPaginationLimit,
  setPaginationOffset,
  filterWithPagination,
  reprocessBagfile
} from '../../redux/Home/HomeActions';
import {showError} from '../../redux/App/AppActions';
import {FileTypes} from '../Sidebar/FilterElements/ByFileFilter/ByFileFilter';
import {isObjectValid} from '../../utils/helperFunctions';

export const defaultEmptyValue = '-';

function mapStateToProps(state, ownProps) {
  const fileType = state.fileType || FileTypes.ORIGINALFILE;
  const selectedRows = state.selectedTablelRows || [];
  const total = state.total || 50;
  const limit = state.appliedFilters.limit || 50;
  const offset = state.appliedFilters.offset || 0;
  const loadingState = state.showLoading === undefined ? true : state.showLoading;
  const tableMessage = state.tableMessage || '';
  const showTableMessage = state.showTableMessage || false;
  const displayedLink = (fileType === FileTypes.ORIGINALFILE || fileType === FileTypes.ALWAYSONFILE) ? 'current_link' : 'link';
  const adminUser = !isObjectValid(state.user) ? false : state.user.admin || false;
  return {fileType, selectedRows, total, limit, offset, loadingState, tableMessage, showTableMessage, displayedLink, adminUser};
}

export default connect(
  mapStateToProps,
  {sortBy, selectTableEntry, setPaginationLimit, setPaginationOffset, filterWithPagination, reprocessBagfile, showError}
)(Table);