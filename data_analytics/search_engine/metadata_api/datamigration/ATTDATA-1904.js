/*
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
*/

// https://athena.daimler.com/jira/browse/ATTDATA-1904
// set newly introduced property processing_priority to 0 for all bagfiles
db.bagfiles.update(
    { processing_priority: { $exists: false } },
    {
        $set: {
            processing_priority: NumberInt(0)
        }
    },
    {
        multi: true
    }
);