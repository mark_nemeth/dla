"""Search Engine and Metadata REST API feature buckets route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from typing import List

from DANFUtils.tracing import tracing
from DANFUtils.constants import constants
from DANFUtils.exceptions import GUIDNotFoundException

from handlers.auth import auth
from handlers.feature_buckets import FeatureBucketsHandler
from model.exceptions import GuidNotFoundError
from handlers.utils import utils
from requesthandler.base_request_handler import BaseRequestHandler


class FeatureBucketsRequestHandler(BaseRequestHandler):
    """Request handler for feature buckets route: /featurebucket"""

    _schema_post = BaseRequestHandler.load_json_schema(
        "feature_bucket_metadata_post.schema")

    feature_bucket_handler = FeatureBucketsHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, POST')

    @auth.requires_entitlements(["openid"])
    def post(self):
        tracing.start_task_in_context('featurebuckets/post',
                                      context_headers=self.request.headers)
        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_post)

        guid = self.feature_bucket_handler.add_feature_bucket_to_db(data)
        res = 'Feature Bucket(s) successfully added to db.'

        tracing.end_task()
        utils.build_response(
            response=self,
            err=None,
            results=res,
            status_code=constants["REQ_STATUS_CODE_RESOURCE_CREATED"],
            location="/featurebucket/" + guid)


class FeatureBucketRequestHandler(BaseRequestHandler):
    """Request handler for feature bucket route: /featurebucket/{guid}"""

    _schema_guid = BaseRequestHandler.load_json_schema(
        "guid.schema")
    _schema_patch = BaseRequestHandler.load_json_schema(
        "feature_bucket_metadata_patch.schema")

    feature_bucket_handler = FeatureBucketsHandler()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Access-Control-Allow-Methods',
                        'OPTIONS, GET, PATCH, DELETE')

    @auth.requires_entitlements(["openid"])
    def get(self, guid):
        tracing.start_task_in_context('featurebuckets/get',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        try:
            res = self.feature_bucket_handler.get_feature_bucket_from_db(guid)
        except GUIDNotFoundException as e:
            raise GuidNotFoundError(guid, 'featurebucket') from e

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def patch(self, guid):
        tracing.start_task_in_context('featurebuckets/patch',
                                      context_headers=self.request.headers)

        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)
        data = self.validate_json(self.request.body.decode('utf-8'),
                                  self._schema_patch)

        try:
            self.feature_bucket_handler.update_feature_bucket_in_db(guid, data)
        except GUIDNotFoundException as e:
            raise GuidNotFoundError(guid, 'featurebucket') from e

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results="Feature Bucket updated",
                             status_code=constants["REQ_STATUS_CODE_OK"])

    @auth.requires_entitlements(["openid"])
    def delete(self, guid):
        tracing.start_task_in_context('featurebuckets/delete',
                                      context_headers=self.request.headers)
        self.validate_json('{"guid":"' + str(guid) + '"}', self._schema_guid)

        try:
            res = self.feature_bucket_handler.delete_feature_bucket_from_db(guid)
        except GUIDNotFoundException as e:
            raise GuidNotFoundError(guid, 'featurebucket') from e

        tracing.end_task()
        utils.build_response(response=self,
                             err=None,
                             results=res,
                             status_code=constants["REQ_STATUS_CODE_OK"])


