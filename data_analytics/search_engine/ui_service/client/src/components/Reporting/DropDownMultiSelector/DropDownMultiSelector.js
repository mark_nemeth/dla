import React from 'react';
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import Form from 'react-bootstrap/Form';
import './DropDownMultiSelector.scss';
import { Icon } from 'react-icons-kit'
import { checkmark } from 'react-icons-kit/icomoon/checkmark'

export function DropDownMultiSelector(props) {

  return (
    <Form className="report-dropdown-multi-wrapper">
      <DropdownButton
        onSelect={props.onSelect}
        title={props.label}>
        {props.items.map((item, idx) => (
          <Dropdown.Item eventKey={idx} key={item.id} disabled={!item.enabled}>
            <span>{item.name}</span>
            <span className="ml-auto">
              {item.selected ? <Icon icon={checkmark} className="report-dropdown-multi-icon" /> : null}
            </span>
          </Dropdown.Item>
        ))}
      </DropdownButton>
    </Form>
  );
}
