__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os

from ContainerClient.container_client import ContainerClient
from DANFUtils.exceptions import RESTException

logger = logging.getLogger(__name__)


class DeleteQueueMessageTask:

    def __init__(self):
        self.client = ContainerClient()

    def execute(self):
        """Triggers deletion of a message from the azure storage queue via
        data handler.
        """
        logger.info("Executing delete queue message")
        event_id = os.getenv('EVENT_ID', None)
        pop_receipt = os.getenv('POP_RECEIPT', None)

        if not event_id or not pop_receipt:
            logger.error("Delete queue message failed. Required env vars "
                         "EVENT_ID and/or POP_RECEIPT are missing")
            return

        try:
            self.client.delete_event(event_id, pop_receipt)
        except (RESTException, AssertionError):
            logger.exception(f"Delete queue message failed. "
                             f"Event ID: {event_id}")
        else:
            logger.info(f"Delete queue message successful. "
                        f"Event ID: {event_id}")