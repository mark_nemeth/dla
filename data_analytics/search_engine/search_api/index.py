"""Search Engine REST API"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import ssl

import tornado.httpserver
import tornado.ioloop
import tornado.web
from DANFUtils.danf_environment import danf_env, Platform
from DANFUtils.logging import configure_root_logger

from config.config_handler import config_dict
from config.constants import DANF_VERSION_TAG
from requesthandler.bagfile_feature_request_handler import BagfileFeatureRequestHandler
from requesthandler.bagfile_filters_request_handler import \
    BagFileFiltersRequestHandler
from requesthandler.bagfile_request_handler import BagfileRequestHandler
from requesthandler.child_bagfile_request_handler import \
    ChildBagfileRequestHandler
from requesthandler.drive_filters_request_handler import \
    DriveFiltersRequestHandler
from requesthandler.childbagfile_filters_request_handler import \
    ChildBagFileFiltersRequestHandler
from requesthandler.drive_request_handler import \
    DriveRequestHandler
from requesthandler.crawler_request_handler import CrawlerRequestHandler
from requesthandler.delete_bagfile_request_handler import \
    DeleteBagfileRequestHandler
from requesthandler.extractor_request_request_handler import \
    ExtractorRequestsRequestHandler
from requesthandler.feature_filter_request_handler import FeatureFilterRequestHandler
from requesthandler.feature_search_request_handler import FeatureSearchRequestHandler
from requesthandler.flow_request_handler import FlowRequestHandler
from requesthandler.flow_run_request_handler import FlowRunRequestHandler
from requesthandler.reporting_trips_kpi_request_handler import ReportingTripsKPIRequestHandler
from requesthandler.reporting_trips_simple_table_request_handler import ReportingTripsSimpleTableRequestHandler
from requesthandler.reporting_trips_table_agg_request_handler import ReportingTripsTableAggRequestHandler
from requesthandler.sequence_request_handler import SequenceRequestHandler
from requesthandler.snippet_bva_request_handler import SnippetBVARequestHandler
from requesthandler.snippet_extractor_request_handler import \
    SnippetExtractorRequestHandler
from requesthandler.sync_bagfile_request_handler import \
    SyncBagfileRequestHandler
from requesthandler.vote_request_handler import VoteRequestHandler
from requesthandler.catalog_request_handler import CatalogRequestHandler

logger = logging.getLogger(__name__)


# Handlers
def _make_app():
    return tornado.web.Application(
        [(r"/snippet/bva/?", SnippetBVARequestHandler),
         (r"/snippet/extractor/?", SnippetExtractorRequestHandler),
         (r"/search/bagfile/?", BagfileRequestHandler),
         (r"/search/bagfile/features?", BagfileFeatureRequestHandler),
         (r"/search/childbagfile/?", ChildBagfileRequestHandler),
         (r"/search/drive/?", DriveRequestHandler),
         (r"/search/sequence/?", SequenceRequestHandler),
         (r"/search/features/?", FeatureSearchRequestHandler),
         (r"/filter/features/?", FeatureFilterRequestHandler),
         (r"/search/flow/?", FlowRequestHandler),
         (r"/search/flow-run/?", FlowRunRequestHandler),
         (r"/search/extractorrequest/?", ExtractorRequestsRequestHandler),
         (r"/search/vote/?", VoteRequestHandler),
         (r"/reporting/trips/kpi/?", ReportingTripsKPIRequestHandler),
         (r"/reporting/trips/tables/agg/?", ReportingTripsTableAggRequestHandler),
         (r"/reporting/trips/tables/simple?", ReportingTripsSimpleTableRequestHandler),
         (r"/file/", CatalogRequestHandler),
         (r"/bagfiles/guids/synchronizable/?", SyncBagfileRequestHandler),
         (r"/bagfiles/guids/deletable/?", DeleteBagfileRequestHandler),
         (r"/crawler/bagfiles/?", CrawlerRequestHandler),
         (r"/filters/?", BagFileFiltersRequestHandler),
         (r"/childbagfile_filters/?", ChildBagFileFiltersRequestHandler),
         (r"/drive_filters/?", DriveFiltersRequestHandler)],
        debug=config_dict.get_value('log_level').upper() == 'DEBUG')


def _main():
    log_level = config_dict.get_value('log_level', 'INFO').upper()
    configure_root_logger(
        log_level=log_level,
        enable_azure_log_aggregation=danf_env().platform != Platform.LOCAL,
    )
    logger.setLevel(log_level)

    logger.info("#############################")
    logger.info("######## Search API #########")
    logger.info("#############################")
    logger.info(f"DANF_VERSION_TAG: {DANF_VERSION_TAG}")
    logger.info(f"DANF ENV: {config_dict.get_value('DANF_ENV')}")
    logger.info("starting server...")

    app_https_port = config_dict.get_value('app_https_port', 8445)
    app_http_port = config_dict.get_value('app_http_port', 0)
    ssl_cert = config_dict.get_value('ssl_cert', None)
    ssl_key = config_dict.get_value('ssl_key', None)
    app = _make_app()

    if app_http_port:
        app.listen(app_http_port)
        logger.info(f"app is listening on HTTP port: {app_http_port} ...")
    if app_https_port:
        https_server = tornado.httpserver.HTTPServer(app,
                                                     ssl_options={
                                                         'certfile':
                                                         ssl_cert,
                                                         'keyfile':
                                                         ssl_key,
                                                         'ssl_version':
                                                         ssl.PROTOCOL_TLSv1_2
                                                     })
        https_server.listen(app_https_port)
        logger.info(f"app is listening on HTTPS port: {app_https_port} ...")

    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    _main()
