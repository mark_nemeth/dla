__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os

from config.config_handler import config_dict
from proxies.kubernetes import KubernetesApiProxy

logger = logging.getLogger(__name__)


class FlowCapacityService:
    """FlowCapacityService instances interface with the kubernetes API in
    order to estimate free capacities for launching flows in the cluster.
    """

    def __init__(self, resource_usage_ratio: float = 0.9):
        """Creates a new FlowCapacityService instance.

        :param resource_usage_ratio: Fraction of the total cluster resources
            that should be utilized. Must be between 0 and 1.
            Keep below 1 in order to have some buffer.
        """
        if not 0.0 <= resource_usage_ratio <= 1.0:
            raise ValueError("resource_usage_ratio must be in rang [0.0, 1.0]")
        self._resource_usage_ratio = resource_usage_ratio

        self._kube_api = KubernetesApiProxy()

        self._required_cpu_request_per_flow = self._load_from_config(
            'required_cpu_request_per_flow')
        """Max. required cpu request per flow in millicpu"""

        self._required_cpu_limit_per_flow = self._load_from_config(
            'required_cpu_limit_per_flow')
        """Max. required cpu limit per flow in millicpu"""

        self._required_memory_request_per_flow = self._load_from_config(
            'required_memory_request_per_flow')
        """Max. required memory request per flow in byte"""

        self._required_memory_limit_per_flow = self._load_from_config(
            'required_memory_limit_per_flow')
        """Max. required memory limit per flow in millicpu"""

    def get_available_flow_capacity(self) -> int:
        """Computes the number of flows that could be additionally executed
        at the current time within the cluster resource limitations.
        """
        if os.environ['DANF_ENV'] not in ['sv_prod', 'sv_int', 'vai_int', 'vai_prod']:
            # resource quota not used in azure. fall back to 100
            return 100

        quota = self._kube_api.get_resource_quota('default', 'kubeflow')
        if not quota:
            logger.warning("resource quota not present. "
                           "Estimate free capacity of 100")
            # 100 is the amount used before introducing load depending sched.
            return 100

        # compute available resources per resource type
        available_cpu_limit = self._compute_available_resources(
            quota.cpu_limit, quota.cpu_limit_used)
        available_cpu_request = self._compute_available_resources(
            quota.cpu_request, quota.cpu_request_used)
        available_memory_limit = self._compute_available_resources(
            quota.memory_limit, quota.memory_limit_used)
        available_memory_request = self._compute_available_resources(
            quota.memory_request, quota.memory_request_used)

        # compute capacity for each resource type and set the minimum as
        # overall capacity
        capacity = min(
            available_cpu_request / self._required_cpu_request_per_flow,
            available_cpu_limit / self._required_cpu_limit_per_flow,
            available_memory_request / self._required_memory_request_per_flow,
            available_memory_limit / self._required_memory_limit_per_flow
        )

        logger.info(f"Computed capacity of {capacity} based on available "
                    f"resources - cpu limit: {available_cpu_limit}, "
                    f"cpu req.: {available_cpu_request}, "
                    f"memory limit: {available_memory_limit}, "
                    f"memory req.: {available_memory_request}. Quota: {quota}")
        return int(capacity)

    def _compute_available_resources(self, hard_limit: int, used: int) -> float:
        allowed = self._resource_usage_ratio * hard_limit
        available = allowed - used
        return max(available, 0)

    @staticmethod
    def _load_from_config(key: str) -> int:
        val = config_dict.get_value(key)
        if val is None:
            logger.warning(f"config param {key} is undefined, "
                           f"setting value to 0")
            val = 0
        return int(val)
