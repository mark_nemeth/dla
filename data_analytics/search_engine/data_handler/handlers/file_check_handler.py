"""File Check"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os
import re

from ContainerClient.container_client import ContainerClient
from azure.common import AzureHttpError
from azure.storage.blob import BlockBlobService

from config.config_handler import config_dict

logger = logging.getLogger(__name__)


class FileCheckHandler:

    def __init__(self):
        self.container_client = ContainerClient()
        self.danf_env = config_dict.get_value("DANF_ENV")

    def check_if_file_exist(self, paths: [], delete_files: bool = False):
        self._validate_params(paths, delete_files)

        # perform initial check for file existence
        file_statuses = []
        if self.danf_env in ["az_dev", "az_int", "az_prod"]:
            file_statuses = self._check_if_files_are_in_azure(paths, delete_files)

        return file_statuses

    def _validate_params(self, paths, delete_files):
        if not paths:
            raise ValueError("Path cannot be none, empty or whitespace.")

        assert isinstance(paths, list)
        assert isinstance(delete_files, bool)

        for path in paths:
            assert isinstance(path, str)
            if not path.endswith(".bag"):
                raise ValueError("Wrong file extension! Only Bagfiles allowed! (*.bag)")

    def _check_if_files_are_in_azure(self, paths, delete_files):
        blob_names = []
        container_names = []
        result = []

        for path in paths:
            blob_name = os.path.split(path)[1]
            blob_names.append(blob_name)
            container_names.append(
                self._get_blob_container_name_from_path(path))

        for blob_name, container_name, path in zip(blob_names,
                                                   container_names,
                                                   paths):
            block_blob_service = self._build_blob_service(path)

            if block_blob_service is None:
                continue

            status = {'path': path,
                      'location': 'cloud'}

            try:
                if block_blob_service.exists(container_name, blob_name):
                    timestamp = BlockBlobService.get_blob_properties(
                        block_blob_service, container_name,
                        blob_name).properties.last_modified
                    size = BlockBlobService.get_blob_properties(
                        block_blob_service, container_name,
                        blob_name).properties.content_length
                    type = BlockBlobService.get_blob_properties(
                        block_blob_service, container_name,
                        blob_name).properties.blob_type

                    status['exists'] = True
                    status['size'] = size
                    status['timestamp'] = timestamp
                    status['blob_type'] = type

                    if delete_files:
                        block_blob_service.delete_blob(container_name,
                                                       blob_name)
                        status['deleted'] = True
                    else:
                        status['deleted'] = False

                else:
                    status['exists'] = False
                    status['size'] = None
                    status['timestamp'] = None
                    status['blob_type'] = None
                    status['deleted'] = False

            except (ValueError, TypeError, AzureHttpError) as error:
                status['exists'] = False
                status['size'] = None
                status['timestamp'] = None
                status['blob_type'] = None
                status['deleted'] = False
                status['error'] = str(error)
                logger.error(f"Invalid path! Blob container name in path "
                             f"is invalid! path: {path}")
            result.append(status)
        return result

    def _get_blob_container_name_from_path(self, path: str):
        if not path or path.isspace():
            raise ValueError("Path cannot be none, empty or whitespace.")
        pattern = re.compile("\/([0-9]{4}\-[0-9]{2}\-[0-9]{2}){1}\/")
        match = pattern.findall(path)
        if not match:
            return []
        else:
            return match[0]

    def _build_blob_service(self, path):
        pattern = re.compile("\/dla[a-z]*[0-9]+[a-z]+\/")
        match = pattern.findall(path)
        if not match:
            return None
        else:
            match = match[0].replace("/", "")
            storage_account_key = config_dict.get_value(
                f"access_key_{match}")
            return BlockBlobService(
                account_name=match,
                account_key=storage_account_key)

    def _create_on_prem_path(self, path):
        pattern = re.compile("\/mapr\/[0-9]+\.[a-z]+\.[a-z]{2}")
        match = pattern.findall(path)
        if not match:
            return path
        else:
            match = match[0].replace("/mapr", "")
            path = path.replace(match, "")
            return path
