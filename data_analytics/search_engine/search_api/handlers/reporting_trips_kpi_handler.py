"""Handler class for feature search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

from handlers.db_data_handler import DBDataHandler
import re

from handlers.reporting_utils import *

logger = logging.getLogger(__name__)


class KPI:
    def __init__(self, kpi_id: str, kpi_label: str):
        self.id = kpi_id
        self.label = kpi_label

    def fill(self, value):
        return {"id": self.id, "label": self.label, "value": value}


DAYS_ENG = KPI('days_eng', 'Days of engaged testing')
UNPLANNED_DISENG = KPI('unplanned_diseng', 'Unplanned disengagements')
UNPLANNED_MAN_DISENG = KPI('unplanned_man_diseng',
                           'Unplanned manual disengagements')
UNPLANNED_ERR_DISENG = KPI('unplanned_err_diseng',
                           'Unplanned error disengagements')
PLANNED_MAN_DISENG = KPI('planned_diseng', 'Planned disengagements')
MILES_DRIVEN = KPI('miles_driven', 'Autonomous miles driven')
KM_DRIVEN = KPI('km_driven', 'Autonomous km driven')
MILES_PER_UNPLANNED_DISENG = KPI('miles_per_unplanned_diseng',
                                 'Miles per unplanned disengagement')
KM_PER_UNPLANNED_DISENG = KPI('km_per_unplanned_diseng',
                              'km per unplanned disengagement')

TRIPS_TOTAL = KPI('total_trips', 'Total trips')
IGN_TRIPS_TOTAL = KPI('ign_trips', 'Ignored trips')
IGN_TRIPS_MISS_DISENG_INFO = KPI('ign_trips_miss_diseng_info',
                                 'Ignored trips with no diseng. info')
IGN_TRIPS_NEGL_DISTANCE = KPI('ign_trips_negl_distance',
                              'Ignored trips with negligible distance')
IGN_TRIPS_NEGL_DURATION = KPI('ign_trips_negl_duration',
                              'Ignored trips with negligible duration')
IGN_TRIPS_PRIV = KPI('ign_trips_priv', 'Ignored trips on private roads')


class ReportingTripsKPIHandler:
    """
    Class for fetching report data (kpis/tables) 
    """

    def __init__(self):
        self.db_handler = DBDataHandler()

    def get_kpis(self, start: int, end: int, location: str, report_type: str,
                 ign_dur: bool, ign_dist: bool, ign_no_info: bool,
                 ign_priv: bool) -> list:
        """
           Get a dict of kpi values
            :param start: start timestamp in ns
            :param end: end timestamp in ns
            :param location: country string ["DE", "US"]
            :param report_type: Type of the report ["DMV"]
            :param ign_dur: Ignore trips with short duration
            :param ign_dist: Ignore trips with short distance
            :param ign_no_info:  Ignore trips with no diseng info
            :param ign_priv: Ignore trips on private roads
            :return: dict of kpis 
        """
        # TODO: Create aggregation pipelines in a flexible, reusable way instead of hard-wiring everything

        if location not in REPORTING_LOCATIONS:
            raise ValueError(
                f"'location' has to be one of {REPORTING_LOCATIONS.keys()}")
        else:
            loc = REPORTING_LOCATIONS[location]
            kpis = []
            if report_type in [REPORT_TYPE_DMV, REPORT_TYPE_RP]:
                public_only = True
                if report_type == REPORT_TYPE_DMV:
                    public_only = ign_priv

                # get diseng values and days of testing from db
                result_kpi_query = self.db_handler.aggregate_extractor_requests(
                    self._kpi_pipeline(start, end, loc, ign_dur, ign_dist,
                                       ign_no_info, public_only))

                if len(result_kpi_query
                       ) > 0:  # check if any results were returned
                    # add disengagement & distance kpis
                    k = result_kpi_query[0]
                    kpis.append(DAYS_ENG.fill(k[DAYS_ENG.id]))
                    unplanned_diseng = k[UNPLANNED_MAN_DISENG.id] + k[
                        UNPLANNED_ERR_DISENG.id]
                    kpis.append(UNPLANNED_DISENG.fill(unplanned_diseng))
                    kpis.append(
                        UNPLANNED_MAN_DISENG.fill(k[UNPLANNED_MAN_DISENG.id]))
                    kpis.append(
                        UNPLANNED_ERR_DISENG.fill(k[UNPLANNED_ERR_DISENG.id]))
                    kpis.append(
                        PLANNED_MAN_DISENG.fill(k[PLANNED_MAN_DISENG.id]))

                    if unplanned_diseng > 0:
                        km_per_diseng = k[KM_DRIVEN.id] / unplanned_diseng
                    else:
                        km_per_diseng = 0

                    if report_type == REPORT_TYPE_DMV:
                        kpis.append(
                            MILES_DRIVEN.fill(
                                round(k[KM_DRIVEN.id] * MILES_IN_KM, 4)))
                        kpis.append(
                            MILES_PER_UNPLANNED_DISENG.fill(
                                round(km_per_diseng * MILES_IN_KM, 4)))
                    else:
                        kpis.append(KM_DRIVEN.fill(round(k[KM_DRIVEN.id], 4)))
                        kpis.append(
                            KM_PER_UNPLANNED_DISENG.fill(
                                round(km_per_diseng, 4)))

                    # get trips that where ignored
                    result_ign_query = self.db_handler.aggregate_extractor_requests(
                        self._ignored_files_pipeline(start, end, loc))
                    if len(result_ign_query
                           ) > 0:  # check if any results were returned
                        # add filtered trip kpis
                        i = result_ign_query[0]
                        kpis.append(TRIPS_TOTAL.fill(i[TRIPS_TOTAL.id]))
                        kpis.append(
                            IGN_TRIPS_TOTAL.fill(i[TRIPS_TOTAL.id] -
                                                 k[IGN_TRIPS_TOTAL.id]))
                        if report_type == REPORT_TYPE_DMV and ign_priv:
                            kpis.append(
                                IGN_TRIPS_PRIV.fill(i[IGN_TRIPS_PRIV.id]))
                        if ign_no_info:
                            kpis.append(
                                IGN_TRIPS_MISS_DISENG_INFO.fill(
                                    i[IGN_TRIPS_MISS_DISENG_INFO.id]))
                        if ign_dist:
                            kpis.append(
                                IGN_TRIPS_NEGL_DISTANCE.fill(
                                    i[IGN_TRIPS_NEGL_DISTANCE.id]))
                        if ign_dur:
                            kpis.append(
                                IGN_TRIPS_NEGL_DURATION.fill(
                                    i[IGN_TRIPS_NEGL_DURATION.id]))

            return kpis

    def _kpi_pipeline(self, start: int, end: int, location: str, ign_dur: bool,
                      ign_dist: bool, ign_no_info: bool,
                      public_only: bool) -> list:
        return [
            self._kpi_match_stage(start, end, location, ign_dur, ign_dist,
                                  ign_no_info, public_only),
            self._kpi_add_fields_stage_1(),
            self._kpi_group_stage(public_only),
            self._kpi_add_fields_stage_2(),
        ]

    @staticmethod
    def _kpi_match_stage(start: int, end: int, location: str, ign_dur: bool,
                         ign_dist: bool, ign_no_info: bool,
                         public_only: bool) -> dict:
        stage = {
            "start": {
                "$gte": start
            },
            "end": {
                "$lte": end
            },
            "extractor_id": "feature_extraction_diseng_details"
        }
        if location in ["DE", "US"]:
            stage["metadata.country"] = location
        elif location != "Any":
            stage["metadata.geofence"] = location

        if ign_no_info:
            stage["metadata.end_details"] = {"$ne": None}
        if ign_dist:
            stage["metadata.distance"] = {"$gte": MIN_DRIVE_DISTANCE}
        if ign_dur:
            stage["metadata.duration"] = {"$gte": MIN_DRIVE_DURATION}
        if public_only:
            stage["$or"] = [{
                "metadata.end_public_road": {
                    "$eq": True
                }
            }, {
                "metadata.distance_public": {
                    "$ne": 0
                }
            }]
        return {"$match": stage}

    @staticmethod
    def _kpi_add_fields_stage_1() -> dict:
        return {
            "$addFields": {
                "manual_unplanned_diseng": {
                    "$cond": [{
                        "$and": [{
                            "$eq": ["$metadata.manual_disengagement", True]
                        }, {
                            "$ne": ["$metadata.end_planned", True]
                        }]
                    }, 1, 0]
                },
                "manual_planned_diseng": {
                    "$cond": [{
                        "$and": [{
                            "$eq": ["$metadata.manual_disengagement", True]
                        }, {
                            "$eq": ["$metadata.end_planned", True]
                        }]
                    }, 1, 0]
                },
                "error_diseng": {
                    "$cond": [{
                        "$and": [{
                            "$ne": ["$metadata.manual_disengagement", True]
                        }, {
                            "$ne": ["$metadata.end_planned", True]
                        }]
                    }, 1, 0]
                },
                "date": {
                    "$substr": ["$metadata.end_ts", 0, 10]
                }
            }
        }

    @staticmethod
    def _kpi_group_stage(public_only: bool) -> dict:
        if public_only:
            dist = "$metadata.distance_public"
        else:
            dist = "$metadata.distance"
        return {
            "$group": {
                "_id": None,
                'days_eng': {
                    "$addToSet": "$date"
                },
                UNPLANNED_MAN_DISENG.id: {
                    "$sum": "$manual_unplanned_diseng"
                },
                PLANNED_MAN_DISENG.id: {
                    "$sum": "$manual_planned_diseng"
                },
                UNPLANNED_ERR_DISENG.id: {
                    "$sum": "$error_diseng"
                },
                'meters': {
                    "$sum": dist
                },
                IGN_TRIPS_TOTAL.id: {
                    "$sum": 1
                }
            }
        }

    @staticmethod
    def _kpi_add_fields_stage_2() -> dict:
        return {
            "$addFields": {
                DAYS_ENG.id: {
                    "$size": "$days_eng"
                },
                KM_DRIVEN.id: {
                    "$divide": ["$meters", 1000]
                }
            }
        }

    def _ignored_files_pipeline(self, start: int, end: int, location: str):
        return [
            self._ignored_files_match_stage(start, end, location),
            self._ignored_files_add_fields_stage(),
            self._ignored_files_group_stage()
        ]

    @staticmethod
    def _ignored_files_match_stage(start: int, end: int,
                                   location: str) -> dict:
        stage = {
            "$match": {
                "start": {
                    "$gte": start
                },
                "end": {
                    "$lte": end
                },
                "extractor_id": "feature_extraction_diseng_details"
            }
        }

        if location in ["DE", "US"]:
            stage["$match"]["metadata.country"] = location
        elif location != "Any":
            stage["$match"]["metadata.geofence"] = location

        return stage

    @staticmethod
    def _ignored_files_add_fields_stage() -> dict:
        return {
            "$addFields": {
                "missing_diseng_info": {
                    "$cond": [{
                        "$eq": ["$metadata.end_details", None]
                    }, 1, 0]
                },
                "negligible_distance": {
                    "$cond": [{
                        "$lt": ["$metadata.distance", MIN_DRIVE_DISTANCE]
                    }, 1, 0]
                },
                "negligible_duration": {
                    "$cond": [{
                        "$lt": ["$metadata.duration", MIN_DRIVE_DURATION]
                    }, 1, 0]
                },
                "private_road_only": {
                    "$cond": [{
                        "$and": [{
                            "$eq": ["$metadata.end_public_road", False]
                        }, {
                            "$eq": ["$metadata.distance_public", 0]
                        }]
                    }, 1, 0]
                }
            }
        }

    @staticmethod
    def _ignored_files_group_stage() -> dict:
        return {
            "$group": {
                "_id": None,
                IGN_TRIPS_MISS_DISENG_INFO.id: {
                    "$sum": "$missing_diseng_info"
                },
                IGN_TRIPS_NEGL_DISTANCE.id: {
                    "$sum": "$negligible_distance"
                },
                IGN_TRIPS_NEGL_DURATION.id: {
                    "$sum": "$negligible_duration"
                },
                IGN_TRIPS_PRIV.id: {
                    "$sum": "$private_road_only"
                },
                TRIPS_TOTAL.id: {
                    "$sum": 1
                }
            }
        }
