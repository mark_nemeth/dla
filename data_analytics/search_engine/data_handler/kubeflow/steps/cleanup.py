__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.tracing import tracing
from DANFUtils.tracing.tracing import (ENV_SPAN_ID, ENV_TRACE_ID,
                                       ENV_TRACE_OPTIONS)
from kfp import dsl
from kubernetes.client import V1EnvVarSource, V1ObjectFieldSelector
from kubernetes.client.models import V1EnvVar

from config.config_handler import config_dict
from kubeflow.steps.common import (VM_LABEL_ATTRIBUTE, SERVICE_VM_TITLE,
                                   RETRY_NUM, PIPELINE_POD_LABEL,
                                   BAGFILE_POD_LABEL, FILE_NAME_POD_LABEL,
                                   sanitize_label, METADATA_API_BASE_URL,
                                   SEARCH_API_BASE_URL,
                                   DATA_HANDLER_BASE_URL, APPINSIGHTS_KEY,
                                   DANF_ENV, AUTH, AUTH_TOKEN_URL,
                                   AUTH_DATAHANDLER_AUDIENCE,
                                   AUTH_DATAHANDLER_CLIENT_CRED_ID,
                                   AUTH_DATAHANDLER_CLIENT_CRED_SECRET,
                                   CPU_RESOURCE_TITLE,
                                   MEMORY_RESOURCE_TITLE, TESTING_POD_LABEL,
                                   HTTP_PROXY, HTTPS_PROXY, NO_PROXY,
                                   AUTH_SEARCH_CLIENT_CRED_SECRET,
                                   AUTH_SEARCH_CLIENT_CRED_ID,
                                   AUTH_SEARCH_AUDIENCE,
                                   AUTH_METADATA_CLIENT_CRED_SECRET,
                                   AUTH_METADATA_CLIENT_CRED_ID,
                                   AUTH_METADATA_AUDIENCE)


class CleanupOp(dsl.ContainerOp):

    NAME = "danf-cleanup"
    DOCKER_IMG = config_dict.get_value(NAME)
    CPU_LIMIT = "100m"
    CPU_REQUEST = "100m"
    MEMORY_LIMIT = "200Mi"
    MEMORY_REQUEST = "100Mi"
    RUN_AS_EXIT_HANDLER = True

    @classmethod
    def create_for_azure(cls,
                         tasks,
                         flow_run_guid,
                         bagfile_guid,
                         bagfile_name,
                         test_mode,
                         message_id=None,
                         message_pop_receipt=None):
        """
        :param tasks: list of tasks that should be executed,
            e.g. [set-sync-flag]
        :param flow_run_guid: id of the flow run in which this step is executed
        :param bagfile_guid: of the bagfile being processed
        :param bagfile_name: of the bagfile to process
        :param test_mode: bool to indicate if step should be run in test mode
        :param message_id: of the message that triggered the flow
        :param message_pop_receipt: of the message that triggered the flow
        :return: new instance of the cleanup operation
        """
        step = cls._create_base_instance(tasks,
                                         flow_run_guid,
                                         bagfile_guid,
                                         bagfile_name,
                                         test_mode)
        step.add_node_selector_constraint(VM_LABEL_ATTRIBUTE, SERVICE_VM_TITLE)

        # add optional env variables
        if message_id:
            step.add_env_variable(V1EnvVar(name="EVENT_ID", value=message_id))
        if message_pop_receipt:
            step.add_env_variable(V1EnvVar(name="POP_RECEIPT", value=message_pop_receipt))

        return step

    @classmethod
    def create_for_on_prem(cls,
                           tasks,
                           flow_run_guid,
                           bagfile_guid,
                           bagfile_name,
                           test_mode):
        step = cls._create_base_instance(tasks,
                                         flow_run_guid,
                                         bagfile_guid,
                                         bagfile_name,
                                         test_mode)

        step.add_env_variable(V1EnvVar(name="HTTP_PROXY", value=HTTP_PROXY)) \
            .add_env_variable(V1EnvVar(name="HTTPS_PROXY", value=HTTPS_PROXY)) \
            .add_env_variable(V1EnvVar(name="no_proxy", value=NO_PROXY))
        return step

    @classmethod
    def _create_base_instance(cls,
                              tasks,
                              flow_run_guid,
                              bagfile_guid,
                              bagfile_name,
                              test_mode):
        args = ['--' + task for task in tasks]

        step = CleanupOp(args) \
            .set_retry(RETRY_NUM) \
            .add_pod_label(name=PIPELINE_POD_LABEL, value=sanitize_label(cls.NAME)) \
            .add_pod_label(name=BAGFILE_POD_LABEL, value=sanitize_label(bagfile_guid)) \
            .add_pod_label(name=FILE_NAME_POD_LABEL, value=sanitize_label(bagfile_name))

        if test_mode:
            step.add_pod_label(name=TESTING_POD_LABEL, value='true')

        step.add_env_variable(V1EnvVar(name="METADATA_API_BASE_URL", value=METADATA_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="SEARCH_API_BASE_URL", value=SEARCH_API_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="DATA_HANDLER_BASE_URL", value=DATA_HANDLER_BASE_URL)) \
            .add_env_variable(V1EnvVar(name="APPINSIGHTS_INSTRUMENTATIONKEY", value=APPINSIGHTS_KEY)) \
            .add_env_variable(V1EnvVar(name="DANF_ENV", value=DANF_ENV)) \
            .add_env_variable(V1EnvVar(name="BAGFILE_GUID", value=bagfile_guid)) \
            .add_env_variable(V1EnvVar(name="FLOW_RUN_GUID", value=flow_run_guid)) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_ID, value=tracing.get_propagation_vars().get(ENV_TRACE_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_SPAN_ID, value=tracing.get_propagation_vars().get(ENV_SPAN_ID))) \
            .add_env_variable(V1EnvVar(name=ENV_TRACE_OPTIONS, value=tracing.get_propagation_vars().get(ENV_TRACE_OPTIONS))) \
            .add_env_variable(V1EnvVar(name="AUTH", value=AUTH)) \
            .add_env_variable(V1EnvVar(name="AUTH_TOKEN_URL", value=AUTH_TOKEN_URL)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_AUDIENCE", value=AUTH_DATAHANDLER_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_ID", value=AUTH_DATAHANDLER_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_DATAHANDLER_CLIENT_CRED_SECRET", value=AUTH_DATAHANDLER_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_AUDIENCE", value=AUTH_METADATA_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_ID", value=AUTH_METADATA_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_METADATA_CLIENT_CRED_SECRET", value=AUTH_METADATA_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_AUDIENCE", value=AUTH_SEARCH_AUDIENCE)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_ID", value=AUTH_SEARCH_CLIENT_CRED_ID)) \
            .add_env_variable(V1EnvVar(name="AUTH_SEARCH_CLIENT_CRED_SECRET", value=AUTH_SEARCH_CLIENT_CRED_SECRET)) \
            .add_env_variable(V1EnvVar(
                name="POD_NAME",
                value_from=V1EnvVarSource(field_ref=V1ObjectFieldSelector(field_path='metadata.name'))))

        step.add_resource_request(CPU_RESOURCE_TITLE, cls.CPU_REQUEST) \
            .add_resource_request(MEMORY_RESOURCE_TITLE, cls.MEMORY_REQUEST) \
            .add_resource_limit(CPU_RESOURCE_TITLE, cls.CPU_LIMIT) \
            .add_resource_limit(MEMORY_RESOURCE_TITLE, cls.MEMORY_LIMIT)

        return step

    def __init__(self, args):
        super().__init__(
            name=self.NAME,
            image=self.DOCKER_IMG,
            arguments=args,
            is_exit_handler=self.RUN_AS_EXIT_HANDLER
        )
