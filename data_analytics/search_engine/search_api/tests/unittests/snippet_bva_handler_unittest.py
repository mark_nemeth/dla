"""Unittest for snippet bva route"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''
import unittest
from unittest.mock import patch, MagicMock

from DANFUtils.utils import generate_deterministic_guid

from handlers.snippet_bva_handler import SnippetBVAHandler

bagfile_guid = '233985c0-5f9d-4e46-9c7c-56fe58bce5ed'
guid = generate_deterministic_guid()


def generate_test_sequence(index):
    return {
        "guid": guid,
        "bagfile_guid": bagfile_guid,
        "index": index,
        "start": 1475272800000 + index * 1000,
        "end": 1475272800000 + index * 1000 + 999,
        "type": "basic",
        "extractors": {
            "ext1": {
                "bva": 5,
            },
            "ext2": {
                "bva": 5,
            },
            "ext3": {
                "bva": 5
            }
        }
    }


guid = "test"


class TestSnippetBVAHandler(unittest.TestCase):
    @patch('handlers.snippet_bva_handler.DBDataHandler')
    def test_childbag_seq_bva_limit_equal(self, mocked_DBDataHandler):
        ######Mock DBDataHandler ##############################
        mm = MagicMock()
        mm.get_bagfiles.return_value = "TEST"
        mm.get_sequences_by_bagfile_guid.return_value = [
            generate_test_sequence(2),
            generate_test_sequence(5)
        ]
        mm.add_child_bagfiles.return_value = "TEST"
        mocked_DBDataHandler.return_value = mm

        ########################################################
        #######Test#############################################
        sBva_handler = SnippetBVAHandler()
        res = sBva_handler.get_snippets_bva(guid, bva_limit=5)
        self.assertEqual(res[0]["version"], '1.0')
        self.assertEqual(res[0]["parent_guid"], guid)
        self.assertEqual(res[0]["start"], 1475272802000)
        self.assertEqual(res[0]["end"], 1475272802999)
        self.assertEqual(res[0]["parent_index_start"], 2)
        self.assertEqual(res[0]["parent_index_end"], 2)
        self.assertEqual(res[0]["bva_max"], 5)
        self.assertEqual(res[0]["metadata"], {
            'ext1': {},
            'ext2': {},
            'ext3': {}
        })

        self.assertEqual(res[1]["version"], '1.0')
        self.assertEqual(res[1]["parent_guid"], guid)
        self.assertEqual(res[1]["start"], 1475272805000)
        self.assertEqual(res[1]["end"], 1475272805999)
        self.assertEqual(res[1]["parent_index_start"], 5)
        self.assertEqual(res[1]["parent_index_end"], 5)
        self.assertEqual(res[1]["bva_max"], 5)
        self.assertEqual(res[1]["metadata"], {
            'ext1': {},
            'ext2': {},
            'ext3': {}
        })

        with self.assertRaises(IndexError):
            res[2]

    @patch('handlers.snippet_bva_handler.DBDataHandler')
    def test_childbag_seq_one_seq_bva_limit_not_exceeded(
            self, mocked_DBDataHandler):
        seq1 = {
            "guid": guid,
            "bagfile_guid": bagfile_guid,
            "index": 1,
            "start": 1475272800000 + 2 * 1000,
            "end": 1475272800000 + 5 * 1000 + 999,
            "type": "basic",
            "extractors": {
                "ext1": {
                    "bva": 3,
                },
                "ext2": {
                    "bva": 3,
                },
                "ext3": {
                    "bva": 3
                }
            }
        }

        seq2 = {
            "guid": guid,
            "bagfile_guid": bagfile_guid,
            "index": 5,
            "start": 1475272800000 + 4 * 1000,
            "end": 1475272800000 + 10 * 1000 + 999,
            "type": "basic",
            "extractors": {
                "ext1": {
                    "bva": 8,
                },
                "ext2": {
                    "bva": 8,
                },
                "ext3": {
                    "bva": 8
                }
            }
        }
        ######Mock DBDataHandler ##############################
        mm = MagicMock()
        mm.get_bagfiles.return_value = "TEST"
        mm.get_sequences_by_bagfile_guid.return_value = [seq1, seq2]
        mm.add_child_bagfiles.return_value = "TEST"
        mocked_DBDataHandler.return_value = mm

        ########################################################
        #######Test#############################################
        sBva_handler = SnippetBVAHandler()
        res = sBva_handler.get_snippets_bva(guid, bva_limit=7)
        self.assertEqual(res[0]["version"], '1.0')
        self.assertEqual(res[0]["parent_guid"], guid)
        self.assertEqual(res[0]["start"], 1475272804000)
        self.assertEqual(res[0]["end"], 1475272810999)
        self.assertEqual(res[0]["parent_index_start"], 5)
        self.assertEqual(res[0]["parent_index_end"], 5)
        self.assertEqual(res[0]["bva_max"], 8)
        self.assertEqual(res[0]["metadata"], {
            'ext1': {},
            'ext2': {},
            'ext3': {}
        })

        with self.assertRaises(IndexError):
            res[1]

    @patch('handlers.snippet_bva_handler.DBDataHandler')
    def test_childbag_seq_both_seq_bva_limit_not_exceeded(
            self, mocked_DBDataHandler):
        seq1 = {
            "guid": guid,
            "bagfile_guid": bagfile_guid,
            "index": 1,
            "start": 1475272800000 + 2 * 1000,
            "end": 1475272800000 + 5 * 1000 + 999,
            "type": "basic",
            "extractors": {
                "ext1": {
                    "bva": 3,
                },
                "ext2": {
                    "bva": 3,
                },
                "ext3": {
                    "bva": 3
                }
            }
        }

        seq2 = {
            "guid": guid,
            "bagfile_guid": bagfile_guid,
            "index": 5,
            "start": 1475272800000 + 4 * 1000,
            "end": 1475272800000 + 10 * 1000 + 999,
            "type": "basic",
            "extractors": {
                "ext1": {
                    "bva": 8,
                },
                "ext2": {
                    "bva": 8,
                },
                "ext3": {
                    "bva": 8
                }
            }
        }
        #################Mock DBDataHandler ####################
        mm = MagicMock()
        mm.get_bagfiles.return_value = "TEST"
        mm.get_sequences_by_bagfile_guid.return_value = [seq1, seq2]
        mm.add_child_bagfiles.return_value = "TEST"
        mocked_DBDataHandler.return_value = mm

        ########################################################
        #####################Test###############################
        sBva_handler = SnippetBVAHandler()
        res = sBva_handler.get_snippets_bva(guid, bva_limit=9)
        with self.assertRaises(IndexError):
            res[0]


if __name__ == '__main__':

    unittest.main()
