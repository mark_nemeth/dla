"""Unittests for DBConnector"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import os
import unittest

import requests
from DANFUtils.logging import logger
from datetime import timedelta, datetime
# Set env before importing db connector
from .utils import generate_bagfile, generate_feature_buckets

os.environ["DANF_ENV"] = "local"
from .testutils import get_testdb_connector

# increase log level for tests
logger.set_log_level('debug')

base_url = 'http://localhost:8889'
report_kpi_url = base_url + '/reporting/trips/kpi'

bag_1 = {
    "version": "1.0",
    "guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "link": "testdata",
    "file_name": "testdata1.bag",
    "size": 21231,
    "num_messages": 4444,
    "start": 1597838471160,
    "end": 1597840223670,
    "duration": 3001,
    "vehicle_id_num": "V-123-234",
    "topics": ["topic2"],
    "drive_types": ["CID"],
    "is_test_data": 1,
    "custom_attributes": {
        "feature_extraction": {
            "version": "0.1.22",
            "number_of_planned_disengagements_total": 2,
            "number_of_manual_unplanned_disengagements_total": 3,
            "number_of_error_disengagements_total": 4,
            "number_of_public_planned_disengagements_total": 2,
            "number_of_public_manual_unplanned_disengagements_total": 3,
            "number_of_public_error_disengagements_total": 4,
            "total_engaged_distance": 12345.9203894549,
            "engaged_distance_in_public": 12345.9203894549,
            "duration_driven_in_engaged_mode": 706.360106,
            "country": "DE",
            "total_distance": 1636367.63637027
        }
    }
}

ext_1 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999999",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597838758163,
    "start": 1597838656213,
    "end": 1597838758163,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:04:16.213098+00:00",
        "end_ts": "2020-08-19 12:05:58.163086+00:00",
        "duration": 101.949988,
        "distance_public": 828.144621129744,
        "distance": 928.144621129744,
        "end_details": "not_filtered anymore",
        "end_planned": 0,
        "end_velocity": -4.96524948357546e-05,
        "country": "DE"
    },
    "is_test_data": 1
}

ext_2 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999998",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597838797863,
    "start": 1597838797853,
    "end": 1597838797863,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:06:37.853083+00:00",
        "end_ts": "2020-08-19 12:06:37.863079+00:00",
        "duration": 0.009996,
        "distance_public": 0.975415342082024,
        "distance": 0.975415342082024,
        "end_details": None,
        "end_planned": 0,
        "end_velocity": 10.3755190467834,
        "country": "DE"
    },
    "is_test_data": 1
}

ext_3 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999997",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597839172173,
    "start": 1597838956283,
    "end": 1597839172173,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:09:16.283082+00:00",
        "end_ts": "2020-08-19 12:12:52.173077+00:00",
        "duration": 215.889995,
        "distance_public": 2384.73693096242,
        "distance": 2384.73693096242,
        "end_details": "[51]Distronic lever pushed; ",
        "end_planned": True,
        "end_velocity": 23.8649177837372,
        "country": "DE",
        "manual_disengagement": True
    },
    "is_test_data": 1
}

ext_4 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999996",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597839182043,
    "start": 1597839180383,
    "end": 1597839182043,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:13:00.383084+00:00",
        "end_ts": "2020-08-19 12:13:02.043078+00:00",
        "duration": 1.659994,
        "distance_public": 36.3831270868416,
        "distance": 36.3831270868416,
        "end_details":
        "[52]Accelerator pedal pressed; [55]Acceleration or brake pedal pressed; ",
        "end_planned": False,
        "end_velocity": 22.9763528823853,
        "country": "DE",
        "manual_disengagement": True
    },
    "is_test_data": 1
}

ext_5 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999995",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597839519453,
    "start": 1597839197663,
    "end": 1597839519453,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:13:17.663087+00:00",
        "end_ts": "2020-08-19 12:18:39.453095+00:00",
        "duration": 321.790008,
        "distance_public": 7675.85577658969,
        "distance": 7675.85577658969,
        "end_details": "Error ",
        "end_planned": False,
        "end_velocity": 21.2098003482819,
        "country": "DE",
        "manual_disengagement": False
    },
    "is_test_data": 1
}

ext_6 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999994",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1497839538483,
    "start": 1497839527223,
    "end": 1497839538483,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:18:47.223082+00:00",
        "end_ts": "2020-08-19 12:18:58.483078+00:00",
        "duration": 11.259996,
        "distance_public": 283.271966150785,
        "distance": 283.271966150785,
        "end_details": "[54]Steering takeover; ",
        "end_planned": False,
        "end_velocity": 26.6848262023926,
        "country": "DE",
        "manual_disengagement": True
    },
    "is_test_data": 1
}

ext_7 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999993",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1497839538484,
    "start": 1497839527224,
    "end": 1497839538484,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:18:47.223082+00:00",
        "end_ts": "2020-08-19 12:18:58.483078+00:00",
        "duration": 11.259996,
        "distance_public": 283.271966150785,
        "distance": 283.271966150785,
        "end_details": "[54]Steering takeover; ",
        "end_planned": False,
        "end_velocity": 26.6848262023926,
        "country": "US",
        "manual_disengagement": True
    },
    "is_test_data": 1
}

ext_8 = {
    "guid": "22222222-aaaa-bbbb-cccc-999999999992",
    "bagfile_guid": "11111111-aaaa-bbbb-cccc-999999999999",
    "ts": 1597839182043,
    "start": 1597839180383,
    "end": 1597839182043,
    "extractor_id": "feature_extraction_diseng_details",
    "metadata": {
        "start_ts": "2020-08-19 12:13:00.383084+00:00",
        "end_ts": "2020-08-19 12:13:02.043078+00:00",
        "duration": 1.659994,
        "distance_public": 36.3831270868416,
        "distance": 36.3831270868416,
        "end_details":
        "[52]Accelerator pedal pressed; [55]Acceleration or brake pedal pressed; ",
        "end_planned": False,
        "geofence": "SJ pilot",
        "end_velocity": 22.9763528823853,
        "country": "US",
        "manual_disengagement": True
    },
    "is_test_data": 1
}


class TestReportingTripsKPIsGET(unittest.TestCase):
    def setUp(self):
        self.connector = get_testdb_connector()
        self.cleanup_db()

        self.connector.add_bagfiles([bag_1])
        self.connector.add_extractor_requests(
            [ext_1, ext_2, ext_3, ext_4, ext_5, ext_6, ext_7, ext_8])

    def tearDown(self):
        self.cleanup_db()
        pass

    def cleanup_db(self):
        with self.connector as con:
            con.db['bagfiles'].delete_many({"is_test_data": 1})
            con.db['extractor_requests'].delete_many({"is_test_data": 1})

    def test_kpi_retrieval_invalid_param_name(self):
        query_data = {
            "start": "test",
            "end": 1603196749000,
            "country": "ger",
            "report_type": "dmv",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(400, res.status_code, 'KPI retrieval failed')

    def test_kpi_retrieval_invalid_location(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "test",
            "report_type": "dmv",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(500, res.status_code, 'KPI retrieval failed')

    def test_kpi_retrieval_missing_param(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "ger",
            "report_type": "dmv",
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(400, res.status_code, 'KPI retrieval failed')

    def test_kpi_retrieval_no_return(self):
        query_data = {
            "start": 2475158400000,
            "end": 2575158400001,
            "location": "ger",
            "report_type": "dmv",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')
        res_json = res.json()['results']
        self.assertDictEqual({'kpis': []}, res_json, 'KPI retrieval failed')

    def test_kpi_retrieval_all_filters(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "ger",
            "report_type": "dmv",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')

        expected_res = [{
            'id': 'days_eng',
            'label': 'Days of engaged testing',
            'value': 1
        }, {
            'id': 'unplanned_diseng',
            'label': 'Unplanned disengagements',
            'value': 3
        }, {
            'id': 'unplanned_man_diseng',
            'label': 'Unplanned manual disengagements',
            'value': 1
        }, {
            'id': 'unplanned_err_diseng',
            'label': 'Unplanned error disengagements',
            'value': 2
        }, {
            'id': 'planned_diseng',
            'label': 'Planned disengagements',
            'value': 1
        }, {
            'id': 'miles_driven',
            'label': 'Autonomous miles driven',
            'value': 6.7886
        }, {
            'id': 'miles_per_unplanned_diseng',
            'label': 'Miles per unplanned disengagement',
            'value': 2.2629
        }, {
            'id': 'total_trips',
            'label': 'Total trips',
            'value': 5
        }, {
            'id': 'ign_trips',
            'label': 'Ignored trips',
            'value': 1
        }, {
            'id': 'ign_trips_miss_diseng_info',
            'label': 'Ignored trips with no diseng. info',
            'value': 1
        }, {
            'id': 'ign_trips_negl_distance',
            'label': 'Ignored trips with negligible distance',
            'value': 1
        }, {
            'id': 'ign_trips_negl_duration',
            'label': 'Ignored trips with negligible duration',
            'value': 1
        }, {
            'id': 'ign_trips_priv',
            'label': 'Ignored trips on private roads',
            'value': 0
        }]

        self.assertCountEqual(expected_res, res_json["kpis"],
                              'KPI retrieval failed')

    def test_kpi_retrieval_less_filters(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "ger",
            "report_type": "dmv",
            "ign_dur": False,
            "ign_dist": False,
            "ign_no_info": False,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')

        expected_res = [{
            'id': 'days_eng',
            'label': 'Days of engaged testing',
            'value': 1
        }, {
            'id': 'unplanned_diseng',
            'label': 'Unplanned disengagements',
            'value': 4
        }, {
            'id': 'unplanned_man_diseng',
            'label': 'Unplanned manual disengagements',
            'value': 1
        }, {
            'id': 'unplanned_err_diseng',
            'label': 'Unplanned error disengagements',
            'value': 3
        }, {
            'id': 'planned_diseng',
            'label': 'Planned disengagements',
            'value': 1
        }, {
            'id': 'miles_driven',
            'label': 'Autonomous miles driven',
            'value': 6.7892
        }, {
            'id': 'miles_per_unplanned_diseng',
            'label': 'Miles per unplanned disengagement',
            'value': 1.6973
        }, {
            'id': 'total_trips',
            'label': 'Total trips',
            'value': 5
        }, {
            'id': 'ign_trips_priv',
            'label': 'Ignored trips on private roads',
            'value': 0
        }, {
            'id': 'ign_trips',
            'label': 'Ignored trips',
            'value': 0
        }]

        self.assertCountEqual(expected_res, res_json["kpis"],
                              'KPI retrieval failed')

    def test_kpi_retrieval_no_filters(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "ger",
            "report_type": "dmv",
            "ign_dur": False,
            "ign_dist": False,
            "ign_no_info": False,
            "ign_priv": False
        }
        res = requests.get(url=report_kpi_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')

        expected_res = [{
            'id': 'days_eng',
            'label': 'Days of engaged testing',
            'value': 1
        }, {
            'id': 'unplanned_diseng',
            'label': 'Unplanned disengagements',
            'value': 4
        }, {
            'id': 'unplanned_man_diseng',
            'label': 'Unplanned manual disengagements',
            'value': 1
        }, {
            'id': 'unplanned_err_diseng',
            'label': 'Unplanned error disengagements',
            'value': 3
        }, {
            'id': 'planned_diseng',
            'label': 'Planned disengagements',
            'value': 1
        }, {
            'id': 'miles_driven',
            'label': 'Autonomous miles driven',
            'value': 6.8513
        }, {
            'id': 'miles_per_unplanned_diseng',
            'label': 'Miles per unplanned disengagement',
            'value': 1.7128
        }, {
            'id': 'total_trips',
            'label': 'Total trips',
            'value': 5
        }, {
            'id': 'ign_trips',
            'label': 'Ignored trips',
            'value': 0
        }]

        self.assertCountEqual(expected_res, res_json["kpis"],
                              'KPI retrieval failed')

    def test_geofence_retrieval(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "sjpilot",
            "report_type": "dmv",
            "ign_dur": False,
            "ign_dist": False,
            "ign_no_info": False,
            "ign_priv": False
        }
        res = requests.get(url=report_kpi_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')

        expected_res = [{
            'id': 'days_eng',
            'label': 'Days of engaged testing',
            'value': 1
        }, {
            'id': 'unplanned_diseng',
            'label': 'Unplanned disengagements',
            'value': 1
        }, {
            'id': 'unplanned_man_diseng',
            'label': 'Unplanned manual disengagements',
            'value': 1
        }, {
            'id': 'unplanned_err_diseng',
            'label': 'Unplanned error disengagements',
            'value': 0
        }, {
            'id': 'planned_diseng',
            'label': 'Planned disengagements',
            'value': 0
        }, {
            'id': 'miles_driven',
            'label': 'Autonomous miles driven',
            'value': 0.0226
        }, {
            'id': 'miles_per_unplanned_diseng',
            'label': 'Miles per unplanned disengagement',
            'value': 0.0226
        }, {
            'id': 'total_trips',
            'label': 'Total trips',
            'value': 1
        }, {
            'id': 'ign_trips',
            'label': 'Ignored trips',
            'value': 0
        }]
        self.maxDiff = None

        self.assertCountEqual(expected_res, res_json["kpis"],
                              'KPI retrieval failed')

    def test_kpi_retrieval_all_filters_rp_report(self):
        query_data = {
            "start": 1575158400000,
            "end": 1603196749000,
            "location": "ger",
            "report_type": "rp",
            "ign_dur": True,
            "ign_dist": True,
            "ign_no_info": True,
            "ign_priv": True
        }
        res = requests.get(url=report_kpi_url, params=query_data)
        res_json = res.json()['results']

        self.assertEqual(200, res.status_code, 'KPI retrieval failed')

        expected_res = [{
            'id': 'days_eng',
            'label': 'Days of engaged testing',
            'value': 1
        }, {
            'id': 'unplanned_diseng',
            'label': 'Unplanned disengagements',
            'value': 3
        }, {
            'id': 'unplanned_man_diseng',
            'label': 'Unplanned manual disengagements',
            'value': 1
        }, {
            'id': 'unplanned_err_diseng',
            'label': 'Unplanned error disengagements',
            'value': 2
        }, {
            'id': 'planned_diseng',
            'label': 'Planned disengagements',
            'value': 1
        }, {
            'id': 'km_driven',
            'label': 'Autonomous km driven',
            'value': 10.9251
        }, {
            'id': 'km_per_unplanned_diseng',
            'label': 'km per unplanned disengagement',
            'value': 3.6417
        }, {
            'id': 'total_trips',
            'label': 'Total trips',
            'value': 5
        }, {
            'id': 'ign_trips',
            'label': 'Ignored trips',
            'value': 1
        }, {
            'id': 'ign_trips_miss_diseng_info',
            'label': 'Ignored trips with no diseng. info',
            'value': 1
        }, {
            'id': 'ign_trips_negl_distance',
            'label': 'Ignored trips with negligible distance',
            'value': 1
        }, {
            'id': 'ign_trips_negl_duration',
            'label': 'Ignored trips with negligible duration',
            'value': 1
        }]

        self.assertCountEqual(expected_res, res_json["kpis"],
                              'KPI retrieval failed')
