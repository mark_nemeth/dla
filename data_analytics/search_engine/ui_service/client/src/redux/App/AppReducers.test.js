import {AppReducer} from './AppReducers';
import {hideError, showError, fetchUserDataError, fetchUserDataSuccess} from './AppActions';

describe('App reducers', () => {

  describe('error handling', () => {
    it('show error', () => {
      const errorMessage = 'Test error message !';
      expect(AppReducer({}, {
        type: showError.type,
        payload: {
          message: errorMessage,
          type: 'warning'
        }
      })).toEqual({
        error: true,
        errorMessage: errorMessage,
        errorMessageType: 'warning'
      });
    });
    it('hide error', () => {
      expect(AppReducer({}, {
        type: hideError.type
      })).toEqual({
        error: false,
        errorMessageType: null
      });
    });
  });

  describe('fetch user', () => {
    it('should handle fetchUserDataError', () => {
      expect(AppReducer({}, {
        type: fetchUserDataError.type,
      })).toEqual({
        user: null
      });
    });
    it('should handle fetchUserDataSuccess', () => {
      const userPayload = {name: 'danfuser@gmail.com', admin: false};
      expect(AppReducer({}, {
        type: fetchUserDataSuccess.type,
        payload: userPayload
      })).toEqual({
        user: userPayload
      });
    });
  });

});