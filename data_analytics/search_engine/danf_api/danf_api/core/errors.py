"""Error handling."""

__copyright__ = """
COPYRIGHT: (c) 2021 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

from typing import List

from fastapi import status
from fastapi.requests import Request
from fastapi.responses import JSONResponse


class RHSColonOperatorError(Exception):
    """Custom exception to raise if an invalid RHS Colon Operator is encountered."""

    def __init__(self, op: str, context: str, valid_op: List[str]):
        """Construct custom error message.

        Args:
            op: the invalid operator
            context: the context in which the operator is not valid
            valid_op: valid operators for the given context
        """
        super().__init__(f"'{op}' is an invalid operator for {context}. Valid operators are {valid_op}.")


async def rhs_colon_operator_error_handler(request: Request, exc: RHSColonOperatorError) -> JSONResponse:
    return JSONResponse(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, content={"detail": exc.args[0]})


class RHSColonValueError(Exception):
    """Custom exception to raise if an invalid RHS Colon Value is encountered."""

    def __init__(self, value: str, context: str, valid_value: str):
        """Construct custom error message.

        Args:
            value: the invalid value
            context: the context in which the value is not valid
            valid_value: valid value for the given context
        """
        super().__init__(f"'{value}' is an invalid value for {context}. A valid value must be {valid_value}.")


async def rhs_colon_value_error_handler(request: Request, exc: RHSColonValueError) -> JSONResponse:
    return JSONResponse(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, content={"detail": exc.args[0]})
