__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import rospy
from extractors.utils import rospytime2int, timestamp2ms
from extractors.base_interpreter import Interpreter

# Currently the list is empty, so no events will befiltered. It will be updated later
INTERESTING_CV3TL_SILENT_TESTING_TESTCASES = []
SECONDS_BEFORE_SILENT_TESTING_EVENT = 3
SECONDS_AFTER_SILENT_TESTING_EVENT = 3
BVA_CV3TL_SILENT_TESTING = 10
# Time between two consecutive extractions are triggered
MILLISECONDS_BETWEEN_EXTRACTED_EVENTS = 1000 * SECONDS_AFTER_SILENT_TESTING_EVENT


class TrafficLightsSilentTestingInterpreter(Interpreter):
    """
    Interpreter for traffic light silent testing events that takes care of event extraction from
    traffic light silent testing messages and creating metadata for extraction requests.
    """

    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.bva = BVA_CV3TL_SILENT_TESTING
        self.trigger_events = []

    def feed_new_msg(self, msg, t):
        """
        Extract silent testing events from the message and add them to the internal storage.

        Args:
            msg(obj): Message object as read from a bagfile
            t(rospy.Time): Timestamp of the message as read from a bagfile
        """
        objects = []

        for obj in msg.detected_objects_of_interest:
            if obj.test_case in INTERESTING_CV3TL_SILENT_TESTING_TESTCASES:
                objects.append({
                    "testcase": obj.test_case,
                    "label": obj.label.id,
                    "position": [
                        obj.position_image.top_left[0],
                        obj.position_image.top_left[1],
                        obj.position_image.bottom_right[0],
                        obj.position_image.bottom_right[1]
                    ],
                })

        if objects:
            # Only add a new event outside of the snippet of the previous event
            if not self.trigger_events or self._is_new_event(t):
                self.trigger_events.append({
                    "ts": rospytime2int(t),
                    "start_ts": rospytime2int(t) - (SECONDS_BEFORE_SILENT_TESTING_EVENT * 1e3),
                    "end_ts": rospytime2int(t) + (SECONDS_AFTER_SILENT_TESTING_EVENT * 1e3),
                    "objects": objects,
                })

    def _is_new_event(self, t):
        """
        Checks if the given timestamp is not in the range of the last extracted event.

        Args:
            t(rospy.Time): Timestamp of the message as read from a bagfile

        Returns:
            :obj:`bool` True if the timestamp corresponds to an event that should be extracted
        """
        last_event_timestamp_ms = rospytime2int(self.trigger_events[-1]["rosts"])
        event_timestamp_ms = rospytime2int(t)

        return last_event_timestamp_ms + MILLISECONDS_BETWEEN_EXTRACTED_EVENTS < event_timestamp_ms
