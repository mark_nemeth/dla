import {connect} from 'react-redux';
import {filterByDrivenByValues} from '../../../../redux/Home/HomeActions';
import {ByDrivenByValueFilter} from './ByDrivenByValueFilter';

function mapStateToProps(state, ownProps) {
  let drivenByValueList = state.filterOptions.drivenByValueList || [];
  const selectedDrivenByValueList = state.filters.driven_by || [];

  return {selectedDrivenByValueList, drivenByValueList};
}

export default connect(
  mapStateToProps,
  {filterByDrivenByValues}
)(ByDrivenByValueFilter);