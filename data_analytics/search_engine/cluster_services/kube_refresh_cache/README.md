# DANF - Kubernetes cache refresher
### Version: see setup.py

The DANF - kubernetes cache refresher is use to run the cron job once in two week to send the request to caching endpoint to calculate the options of different filters

## How to deploy the Kubernetes cache refresher in Kubernetes

### Manually

1. build docker image:

    ```console
    docker build -t dlaregistrydev.azurecr.io/cache-refresher:${version} . --network=host --no-cache
    ```

2. push to the Azure Registry

    ```console
    az acr login --name dlaregistrydev docker push dlaregistrydev.azurecr.io/cache-refresher:${version}
    ```

3. connecting to aks and applying to Kubernetes

    ```console
    az aks get-credentials --resource-group ATTDATA --name ${dla-kubecluster-aks-dev dla-kubecluster-aks-prod}
    kubectl apply -f kubernetes/
    ```

4. start the dashboard to monitor

    ```console
    az aks browse --resource-group ATTDATA --name ${dla-kubecluster-aks-dev, dla-kubecluster-aks-prod}
    ```