"""Handler class for utility functions"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import random
import string
import time
import os
import json

from bson.json_util import dumps

from config.config_handler import config_dict

logger = logging.getLogger(__name__)

DEFAULT_ERROR = "Server Error!"
ERROR_KEY_WORD = "error"
RESULTS_KEY_WORD = "results"
ERROR_MESSAGE_KEY_WORD = "error_message"

STORAGE_ACCOUNTS_ID_REF = "storage_account_ids"


class Utils:

    def filter_invalid_chars_for_blobs(self, name):
        def is_valid(str):
            return str.isalpha() or str.isnumeric()

        filtered = ''.join(filter(is_valid, name))
        return filtered.lower()

    def validate_sa(self, storage_account):
        sa_whitelist = config_dict.get_value("input_storage_whitelist")
        if storage_account in sa_whitelist:
            return True
        return False

    def generate_random_id(self, length):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(length))

    def get_storage_id(self, storage_account_name):
        ids = config_dict.get_value(STORAGE_ACCOUNTS_ID_REF)
        return ids.get(storage_account_name, "none")

    def build_target_container_name(self, storage_account, source_container, file_name):
        stor_id = self.get_storage_id(storage_account)
        random_id = self.generate_random_id(5)
        return "{}-{}-{}".format(stor_id, source_container, random_id)

    def measure_time(self, func):
        def wrapper(*args, **kwargs):
            start = time.time()
            print("start [{}]".format(func.__name__))
            func(*args, **kwargs)
            end = time.time()
            print("{}, time passed: {}".format(func.__name__, end - start))

        return wrapper

    def _get_vin_map(self):
        """
        load the default vin-id mapping
        :return:
        """
        path = os.path.abspath(os.path.dirname(__file__))
        try:
            with open(f"{path}/../config/vin_map.init", "r") as f:
                vin_dict = json.load(f)
        except Exception:
            raise ValueError('vin_map is missing! Crash app!')
        return vin_dict

    def map_id_to_vin(self, paths):
        """
        use to mapping the legacy paths into the authentic, which will accelerate the speed to compare
        :param the legacy paths:
        :return: the authentic paths
        """
        vin_map = self._get_vin_map()
        new_paths = []
        for path in paths:
            if "byName" in path:
                target = "byName"
            elif "byVehicleID" in path:
                target = "byVehicleID"
            else:
                logger.debug(f"path: {path} cannot be converted, return the original")
                new_paths.append(path)
                continue
            items = path.split("/")
            index = items.index(target)
            id = items[index + 1]
            vin = vin_map[id]
            path = path.replace(f"/{target}/{id}", f"/{vin}")
            new_paths.append(path)
        return new_paths

    ###################################
    ######### ROUTES UTILS ############
    ###################################
    def build_response(self,
                       response,
                       err=None,
                       results=None,
                       status_code=200,
                       location=None):

        # bind status code
        response.set_header("Content-Type", "application/json")
        response.set_status(status_code)
        if location is not None:
            response.set_header("Location", location)

        if err is not None:
            res = {ERROR_KEY_WORD: err}
        elif results is not None:
            res = {RESULTS_KEY_WORD: results}
        else:
            res = {}

        logger.debug(f"Response {res}")
        return response.finish(dumps(res))

    def build_error_from_message(self, message):
        return {
            ERROR_MESSAGE_KEY_WORD: message
        }

    ###################################
    ######### Config UTILS ############
    ###################################
    def get_storage_account_access_key(self, account_name):
        return config_dict.get_value(f"access_key_{account_name}")


utils = Utils()
