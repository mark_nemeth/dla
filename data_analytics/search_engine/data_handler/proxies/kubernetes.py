"""Interface to the kubernetes API server"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os
from dataclasses import dataclass
from decimal import Decimal, ROUND_HALF_UP

from kubernetes import client, config
from kubernetes.client.rest import ApiException

from config.config_handler import config_dict

logger = logging.getLogger(__name__)

path = os.path.abspath(os.path.dirname(__file__))
CUSTOM_NODE_RESOURCE_TITLE = "daimler.com~1athena-custom-storage"
CUSTOM_NODE_RESOURCE_VALUE = 6
VM_LABEL_ATTRIBUTE = "beta.kubernetes.io/instance-type"
CUSTOM_STORAGE_VM_TITLE = "Standard_L8s"


class KubernetesApiProxy:
    """Kubernetes API client that can be run locally or in-cluster"""

    def __init__(self):
        proxy = config_dict.get_value("proxy")
        kube_host = config_dict.get_value("kube_host", None)

        if kube_host:
            # if kube host variable is set, assume locally running DH instance
            # and configure accordingly
            kube_config_obj = client.Configuration()
            kube_config_obj.host = kube_host
            kube_config_obj.verify_ssl = False
            kube_config_obj.debug = True
            kube_config_obj.api_key = {
                "authorization": "Bearer " + config_dict.get_value("kube_access_token")
            }

            client.Configuration.set_default(kube_config_obj)

            # add proxy settings if needed
            proxy_set = os.environ.get("HTTP_PROXY")
            if proxy_set:
                logger.info("Setting proxy: {}".format(proxy))
                client.Configuration._default.proxy = proxy
        else:
            # kube host variable not set, assume DH runs in cluster
            # and configure accordingly
            config.load_incluster_config()

        # prepare API
        self.batch_api = client.BatchV1Api()
        self.core_api = client.CoreV1Api()
        self.delete_options = client.V1DeleteOptions()

    def get_resource_quota(self, name: str, namespace: str):
        """Returns a ResourceQuota instance representing the specified
        kubernetes object.

        :param name: of the kubernetes ResourceQuota object
        :param namespace: of the ResourceQuota object
        :return: ResourceQuota or None if it does not exist
        """
        try:
            kube_quota = self.core_api.read_namespaced_resource_quota(
                name, namespace)
        except ApiException as e:
            if e.status == 404:
                return None
            raise e

        status = kube_quota.status
        hard = status.hard
        used = status.used

        # convert V1ResourceQuota object to internal representation
        quota = ResourceQuota(
            cpu_limit=KubeUtils.convert_to_millicpus(hard['limits.cpu']),
            cpu_request=KubeUtils.convert_to_millicpus(hard['requests.cpu']),
            cpu_limit_used=KubeUtils.convert_to_millicpus(used['limits.cpu']),
            cpu_request_used=KubeUtils.convert_to_millicpus(used['requests.cpu']),
            memory_limit=KubeUtils.convert_to_bytes(hard['limits.memory']),
            memory_request=KubeUtils.convert_to_bytes(hard['requests.memory']),
            memory_limit_used=KubeUtils.convert_to_bytes(used['limits.memory']),
            memory_request_used=KubeUtils.convert_to_bytes(used['requests.memory'])
        )

        return quota

    def update_cluster_with_custom_storage_definition(self):
        node_list = self._list_nodes()
        if node_list is None:
            logger.error("error updating cluster for custom storage...")
            return

        for node in node_list:
            name = node.get("name")
            vm_type = node.get("vm_type")
            if vm_type == CUSTOM_STORAGE_VM_TITLE:
                logger.info("updating node: {} with custom storage definition".format(name))
                if not self._submit_custom_storage_definition(name):
                    logger.error("error adding custom storage definition to node: {}".format(name))
                else:
                    logger.info("node {} updated".format(name))

    def _list_nodes(self):
        try:
            node_list = self.core_api.list_node(pretty=True)
        except Exception as e:
            logger.exception(f"Error listing nodes: {e}")
            return None

        results = []
        node_items = node_list.items
        for node in node_items:
            try:
                results.append({
                    "name": node.metadata.name,
                    "vm_type": node.metadata.labels.get("beta.kubernetes.io/instance-type")
                })
            except Exception as e:
                logger.exception(f"Error parsing node attributes: {e}")

        return results

    def _submit_custom_storage_definition(self, node_name):
        body = [{
            "op": "add",
            "path": "/status/capacity/{}".format(CUSTOM_NODE_RESOURCE_TITLE),
            "value": CUSTOM_NODE_RESOURCE_VALUE
        }]

        try:
            self.core_api.patch_node_status(node_name, body)
        except Exception as e:
            logger.exception(f"Error updating custom storage definitions: {e}")
            return False
        return True


@dataclass
class ResourceQuota:
    """Internal representation of a kubernetes ResourceQuota object.

    All CPU resources are stored in millicpu, all memory resources in byte.
    """
    cpu_limit: int
    cpu_request: int
    cpu_limit_used: int
    cpu_request_used: int
    memory_limit: int
    memory_request: int
    memory_limit_used: int
    memory_request_used: int


class KubeUtils(object):
    """Utility methods for dealing with kubernetes objects"""

    @classmethod
    def convert_to_millicpus(cls, value: str) -> int:
        # case 1: Value is given in millicpu format, e.g. 750m. Only stripping
        # of 'm' and casting to int required
        if value.endswith('m'):
            return int(value[:-1])

        # case 2: Value is given in cpu format, e.g. 1.5. Convert to millicpu
        return int(1000 * Decimal(value).quantize(Decimal('1.'),
                                                  rounding=ROUND_HALF_UP))

    @classmethod
    def convert_to_bytes(cls, value: str) -> int:
        # case 1: Value given in byte format, e.g. 52346
        if value.endswith(('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')):
            return int(value)
    
        # case 2-7: Value given in power of two notation, e.g. Kibibyte (KiB)
        # abbreviated in kubernetes as Ki
        if value.endswith('Ki'):
            return int(value[:-2]) * 1024
    
        if value.endswith('Mi'):
            return int(value[:-2]) * 1024 * 1024
    
        if value.endswith('Gi'):
            return int(value[:-2]) * 1024 * 1024 * 1024
    
        if value.endswith('Ti'):
            return int(value[:-2]) * 1024 * 1024 * 1024 * 1024
    
        if value.endswith('Pi'):
            return int(value[:-2]) * 1024 * 1024 * 1024 * 1024 * 1024
    
        if value.endswith('Ei'):
            return int(value[:-2]) * 1024 * 1024 * 1024 * 1024 * 1024 * 1024
    
        # case 8-13: Value given in power of 10 notation, e.g. Kilobyte (Kb)
        # abbreviated in kubernetes as K
        if value.endswith('K'):
            return int(value[:-1]) * 1000
    
        if value.endswith('M'):
            return int(value[:-1]) * 1000 * 1000
    
        if value.endswith('G'):
            return int(value[:-1]) * 1000 * 1000 * 1000
    
        if value.endswith('T'):
            return int(value[:-1]) * 1000 * 1000 * 1000 * 1000
    
        if value.endswith('P'):
            return int(value[:-1]) * 1000 * 1000 * 1000 * 1000 * 1000
    
        if value.endswith('E'):
            return int(value[:-1]) * 1000 * 1000 * 1000 * 1000 * 1000 * 1000
    
        raise ValueError(f"Cannot convert '{value}' to bytes. Unknown unit")

