__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from pydantic import BaseModel
from model.types import Guid, UnixTimestampMilliseconds
from typing import Optional


class VoteDocument(BaseModel):
    guid: Guid
    version: str = "1.0"
    bagfile_guid: Guid
    extracted_event_guid: Optional[Guid]
    account_name: str
    quota_document_guid: str
    start_ts: UnixTimestampMilliseconds
    end_ts: UnixTimestampMilliseconds
    quota: int = 0


class VoteRequest(BaseModel):
    bagfile_guid: Guid
    extracted_event_guid: Optional[Guid]
    start_ts: UnixTimestampMilliseconds
    end_ts: UnixTimestampMilliseconds


class VotePatchRequest(BaseModel):
    start_ts: Optional[UnixTimestampMilliseconds]
    end_ts: Optional[UnixTimestampMilliseconds]


class VoteResponse(BaseModel):
    guid: Guid
    bagfile_guid: Guid
    extracted_event_guid: Optional[Guid]
    account_name: str
    start_ts: UnixTimestampMilliseconds
    end_ts: UnixTimestampMilliseconds
    quota: int