import React from 'react';
import {ErrorBanner} from './ErrorBanner';
import {shallow} from 'enzyme/build';



describe('ErrorBanner component', () => {

  it('should hide automatically after 10 sec', () => {
    jest.useFakeTimers();
    const reduxAction = jest.fn();
    const props = {
      error: true,
      errorMessage: 'Test error message 2 !',
      variant: 'info',
      hideError: reduxAction
    };
    const wrapper = shallow(<ErrorBanner {...props} />);
    jest.runAllTimers();
    expect(reduxAction).toHaveBeenCalled();
  });

  it('should renders when error happen', () => {
    const props = {
      error: true,
      errorMessage: 'Test error message !',
      variant: 'info'
    };
    const wrapper = shallow(<ErrorBanner {...props} />);
    const banner = wrapper.find('div');
    expect(banner.exists()).toBe(true);
  });

  it('should not renders when no error happen', () => {
    const props = {
      error: false
    };
    const wrapper = shallow(<ErrorBanner {...props} />);
    const banner = wrapper.find('div');
    expect(banner.exists()).toBe(false);
  });

  it('should hide when click on close', () => {
    const reduxAction = jest.fn();
    const props = {
      error: true,
      errorMessage: 'Test error message !',
      variant: 'info',
      hideError: reduxAction
    };
    const wrapper = shallow(<ErrorBanner {...props} />);
    const closeIcon = wrapper.find('.error-banner-close-icon');
    closeIcon.simulate('click');
    expect(reduxAction).toHaveBeenCalled();
  });
});