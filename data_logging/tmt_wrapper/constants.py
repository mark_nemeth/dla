# Environment variables
ENV_MODE = "TMT_MODE"
ENV_CONFIG = "TMT_CONFIG"
ENV_TMT_PATH = "TMT_PATH"
ENV_DANF_INPUT_PATH = "INPUT_PATH"
ENV_DANF_OUTPUT_PATH = "OUTPUT_PATH"

# Environment variable values
MODE_SPLITTING = "splitting"
MODE_CONVERSION = "conversion"
MODE_DANF_SPLITTING = "danf-splitting"


# json schema file paths
SCHEMA_FILE_CONVERSION = "tmt_wrapper/schema/conversion.schema"
SCHEMA_FILE_SPLITTING = "tmt_wrapper/schema/splitting.schema"
SCHEMA_FILE_DANF_SPLITTING = "tmt_wrapper/schema/danf_splitting.schema"