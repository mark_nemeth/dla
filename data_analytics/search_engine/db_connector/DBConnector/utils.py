"""Utils"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import json
import sys

import bson
from DANFUtils.logging import logger

MAXIMUM_REQUEST_BATCH_SIZE = 1000


def calculate_request_size(data: [dict, list]) -> float:
    """
    Calculate the BSON encoded size of data
    :param data: JSON data
    :return: Size in KB
    """
    return len(bson.BSON.encode({"": data})) / 1024


def delete_all_documents_from_collection(collection_name, db_handle):

    col = db_handle[collection_name]
    # uses internal ids instead of guids to avoid problems with empty docs
    internal_ids = [x['_id'] for x in col.find({}, {'_id': 1})]

    #col.delete_many({}) doesn't work due to "Request rate is large error"
    num_docs = len(internal_ids)
    if num_docs > 0:
        request = {'_id': {'$in': internal_ids}}
        logger.debug("Info: Deleting  " + str(num_docs) +
                     " documents from collection \"" + collection_name + "\"")
        if num_docs <= MAXIMUM_REQUEST_BATCH_SIZE:
            col.delete_many(request)
        else:
            # batch delete
            logger.debug(
                "Info: Number of delete operations exceeds maximum for single data."
                + "Performing document deletion in batches of size " +
                str(MAXIMUM_REQUEST_BATCH_SIZE))
            i = 0
            while i < num_docs:
                start_id = i
                i += MAXIMUM_REQUEST_BATCH_SIZE
                end_id = i
                if end_id >= num_docs:
                    end_id = num_docs
                logger.debug("Info: Deleting batch objects " + str(end_id) +
                             " of " + str(num_docs))
                request = {'_id': {'$in': internal_ids[start_id:end_id]}}
                col.delete_many(request)
