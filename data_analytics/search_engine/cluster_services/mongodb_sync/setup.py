"""Setup file"""
from setuptools import setup
from config.constants import VERSION

setup(
    name='mongodb_sync',
    version=VERSION,
    install_requires=[
        'requests>=2.22.0',
        'DANFUtils>=0.2.29',
        'ContainerClient>=0.6.102',
    ]
)
