__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import argparse
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from azure.storage.blob import BlockBlobService


class SearchBlobs:

    def __init__(self, storage_account, search_list, key):
        self.search_list = search_list
        self.storage_account = storage_account
        self.storage_service = BlockBlobService(
            account_name=self.storage_account,
            account_key=key,
            protocol='https')

    def get_bagfile_names(self):
        dml_list = []
        for container in self.storage_service.list_containers():
            for blob in self.storage_service.list_blob_names(container.name):
                if all(x in blob for x in self.search_list):
                    length = self.storage_service.get_blob_properties(container.name,
                                                                      blob).properties.content_length
                    dml_list.append({'container_name': container.name, 'blob_name': blob, 'length': length})

        print(dml_list)


def main(storage_account, search_list, key):
    cd = SearchBlobs(storage_account, search_list, key)
    cd.get_bagfile_names()


def _get_commandline_args():
    parser = argparse.ArgumentParser(
        description='Search for bagfiles in all containers in a storage account. Returns a list of tuples '
                    '{container_name, bagfile_name}')
    parser.add_argument('--storage_account',
                        action='store',
                        required=True,
                        help='storage_account name is required')

    parser.add_argument('-s', '--search',
                        action='append',
                        required=True,
                        help='search args are required')

    parser.add_argument('--key',
                        action='store',
                        required=False,
                        help='key is "not" required')

    return parser.parse_args()


if __name__ == '__main__':
    args = _get_commandline_args()
    main(args.storage_account, args.search, args.key)
