"""Kubeflow"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging
import os
from typing import Optional, Type

import kfp
from DANFUtils.exceptions import RESTException

from config.config_handler import config_dict
from kubeflow.flow_metadata import build_flow_run_metadata
from kubeflow.flows.common import BaseFlow
from kubeflow.flows.danf_az_snippet_flow import DanfAzSnippetFlow
from kubeflow.flows.danf_azure_processing_flow import DanfAzureProcessingFlow
from kubeflow.flows.danf_original_processing_flow import DanfOriginalProcessingFlow
from kubeflow.flows.danf_alwayson_processing_flow import DanfAlwaysonFlow
from kubeflow.flows.danf_az_original_processing_flow import DanfAzOriginalProcessingFlow
from kubeflow.flows.danf_az_alwayson_processing_flow import DanfAzAlwaysonFlow
from model.exceptions import EventProcessingError
from model.paths import BlobStoragePath
from proxies.metadata_api import MetadataApiProxy

logger = logging.getLogger(__name__)


class FlowManager:

    GENERATED_FLOW = "processing_pipeline.yaml"

    def __init__(self):
        # If you want to run the data handler locally, you need to set
        # the value according to the README
        pipeline_host = config_dict.get_value("pipeline_host_endpoint", None)
        self.client = kfp.Client(host=pipeline_host)
        self.metadata_api = MetadataApiProxy()
        self.experiment_ids = dict()

    def launch_danf_azure_processing_flow(self,
                                          bagfile: dict,
                                          test_mode: bool):
        return self._launch_flow(DanfAzureProcessingFlow,
                                 bagfile,
                                 test_mode=test_mode)

    def launch_danf_az_snippet_flow(self,
                                    bagfile: dict,
                                    input_location: BlobStoragePath,
                                    test_mode: bool):
        return self._launch_flow(DanfAzSnippetFlow,
                                 bagfile,
                                 input_location=input_location,
                                 test_mode=test_mode)

    def launch_danf_original_processing_flow(self,
                                             bagfile: dict,
                                             output_path_override: Optional[str],
                                             test_mode: bool):
        return self._launch_flow(DanfOriginalProcessingFlow,
                                 bagfile,
                                 output_path_override=output_path_override,
                                 test_mode=test_mode)

    def launch_alwayson_processing_flow(self,
                                        bagfile: dict,
                                        output_path_override: Optional[str],
                                        test_mode: bool):
        return self._launch_flow(DanfAlwaysonFlow,
                                 bagfile,
                                 output_path_override=output_path_override,
                                 test_mode=test_mode)

    def launch_danf_az_original_processing_flow(self,
                                                bagfile: dict,
                                                input_location: BlobStoragePath,
                                                test_mode: bool):
        return self._launch_flow(DanfAzOriginalProcessingFlow,
                                 bagfile,
                                 input_location=input_location,
                                 test_mode=test_mode)

    def launch_az_alwayson_processing_flow(self,
                                           bagfile: dict,
                                           input_location: BlobStoragePath,
                                           test_mode: bool):
        return self._launch_flow(DanfAzAlwaysonFlow,
                                 bagfile,
                                 input_location=input_location,
                                 test_mode=test_mode)

    def _launch_flow(self, flow_cls: Type[BaseFlow], bagfile: dict, **kwargs):
        """Launches an instance of the given flow_cls.

        To achieve that:
        1. Flow run metadata is created
        2. flow_cls is initiated by calling flow_cls.create(bagfile, **kwargs)
        3. the flow is compiled
        4. the complied flow is submitted to the kubeflow job API

        :param flow_cls: Flow to launch (Class extending BaseFlow)
        :param bagfile: bagfile for which flow should be launched
        :param kwargs: additional parameters used to initialize flow
        :return: kubeflow job
        """
        flow_run_guid = self._create_flow_run_metadata(flow_cls, bagfile)
        logger.info("flow run metadataa created")
        flow = self._build_flow(flow_cls, bagfile, flow_run_guid, **kwargs)
        logger.info(f"flow is built: {flow}")
        self._compile_flow(bagfile, flow)
        logger.info("flow is compiled")
        res = self._submit_compiled_flow(bagfile, flow)
        logger.info("flow will be submitted")
        self.metadata_api.set_flow_run_wf_id(flow_run_guid, res.id)

        logger.info(f"job submitted successfully for bagfile "
                    f"guid: {bagfile['guid']} location: {bagfile['link']}. "
                    f"job_id: {res.id}")
        return res

    def _create_flow_run_metadata(
            self, flow: Type[BaseFlow], bagfile: dict) -> str:
        run_req = build_flow_run_metadata(bagfile, flow)
        try:
            run = self.metadata_api.create_flow_run(run_req.dict())
            return run['guid']
        except RESTException as e:
            raise EventProcessingError(f"error creating flow run metadata "
                                       f"for bagfile {bagfile}") from e

    def _build_flow(self, flow_cls, bagfile, flow_run_guid, **kwargs):
        try:
            flow = flow_cls.create(bagfile, flow_run_guid, **kwargs)
        except Exception as e:
            raise EventProcessingError(f"error building {flow_cls.__name__} "
                                       f"for bagfile {bagfile}") from e
        return flow

    def _compile_flow(self, bagfile, flow):
        try:
            logger.info(f"Starting compiler: {flow.pipeline} / {self.GENERATED_FLOW}")
            kfp.compiler.Compiler().compile(flow.pipeline, self.GENERATED_FLOW)
            logger.info(f"Compiler finished")
        except Exception as e:
            raise EventProcessingError(f"error compiling kubeflow pipeline "
                                       f"for bagfile: {bagfile}") from e

    def _submit_compiled_flow(self, bagfile: dict, flow: BaseFlow):
        try:
            res = self.client.run_pipeline(
                experiment_id=self._get_experiment_id(flow),
                job_name="processing-flow-job",
                pipeline_package_path=self.GENERATED_FLOW)
        except Exception as e:
            raise EventProcessingError(f"error submitting job "
                                       f"for bagfile: {bagfile}") from e
        finally:
            self._clean_up()
        return res

    def _clean_up(self):
        try:
            os.remove(self.GENERATED_FLOW)
        except:
            pass

    def _get_experiment_id(self, flow: BaseFlow) -> str:
        """Returns the experiment id to use when launching the given flow.
        If no experiment exists, a new one will be created. Experiment IDs
        are locally cached.
        """
        experiment_name = flow.NAME

        # try to retrieve experiment id from local cache
        if experiment_name in self.experiment_ids:
            return self.experiment_ids[experiment_name]

        try:
            # experiment id not in local cache. try retrieving from api
            experiment = self.client.get_experiment(
                experiment_name=experiment_name)
        except (ValueError, TypeError):
            logger.info(f"Experiment {experiment_name} does not exist. "
                        f"Creating new experiment")
            experiment = self.client.create_experiment(experiment_name)

        self.experiment_ids[experiment_name] = experiment.id
        return experiment.id
