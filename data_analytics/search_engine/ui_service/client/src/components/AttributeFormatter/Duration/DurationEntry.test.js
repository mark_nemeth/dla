import React from 'react';
import {shallow} from 'enzyme';
import {defaultEmptyValue} from '../../Table';
import DurationEntry from './DurationEntry';

describe('Bagfile Table DurationEntry', () => {

  it('should format duration timestamp to readable string', () => {
    const formattedResult = '01h 57m 58s';
    const props = {duration: 7078464};
    const wrapper = shallow(<DurationEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(formattedResult);
  });

  it('should create a disabled button when path not exists', () => {
    const props = {duration: null};
    const wrapper = shallow(<DurationEntry {...props} />);
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('span').props().children).toBe(defaultEmptyValue);
  });
});
