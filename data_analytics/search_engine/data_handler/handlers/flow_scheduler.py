__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import logging

from DANFUtils.danf_environment import danf_env, Platform

from handlers.bagfile_event_processor import get_cloud_event_processor
from handlers.flow_capacity_service import FlowCapacityService
from proxies.search_api import SearchApiProxy

logger = logging.getLogger(__name__)


class FlowScheduler:

    def __init__(self):
        self.search_api = SearchApiProxy()
        self.bagfile_event_processor = get_cloud_event_processor()
        self.flow_capacity_service = FlowCapacityService()
        self.platform = danf_env().platform

    def schedule_next_flows(self):
        capacity = self.flow_capacity_service.get_available_flow_capacity()
        # get unprocessed bagfiles
        bagfiles = self.search_api.get_unprocessed_bagfiles(capacity)

        logger.info(f"Scheduling {len(bagfiles)} unprocessed bagfiles. "
                    f"Capacity: {capacity}")
        for bagfile in bagfiles:
            self.bagfile_event_processor.process_new_bagfile_metadata_event(bagfile, False)

        # get unprocessed childbagfiles
        childbagfiles = self.search_api.get_unprocessed_childbagfiles(capacity)

        logger.info(f"Scheduling {len(childbagfiles)} unprocessed childbagfiles. "
                    f"Capachildbagfilescity: {capacity}")
        for childbagfile in childbagfiles:
            self.bagfile_event_processor.process_new_bagfile_metadata_event(childbagfile, True)
