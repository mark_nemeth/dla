__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DBConnector import DBConnector
from locust import Locust, events

from utils.locust_tools import wrap_with_time_tracking

class DbConnectorLocust(Locust):
    """Custom locust base user that can be used for performance
    testing of DBClient. The attribute self.client refers to a
    a DBConnected enabled for performance measurements.
    """

    port = None
    user = None
    key = None
    db_name = 'metadata'
    protocol = 'mongodb'

    def __init__(self, *args, **kwargs):
        super(DbConnectorLocust, self).__init__(*args, **kwargs)

        # create a standard DBConnector instance
        self.client = DBConnector(hostname=self.host,
                                  port=self.port,
                                  db_name=self.db_name,
                                  user=self.user,
                                  key=self.key,
                                  protocol=self.protocol)

        # add a decorator to all methods of the DBConnector except for
        # 'magic methods' like __str__()
        # the decorator will take care of performing the time measurements
        for attr_name in DBConnector.__dict__:
            attr = getattr(self.client, attr_name)
            if callable(attr) and not attr_name.startswith('__'):
                decorated_method = wrap_with_time_tracking(attr, 'dbconnector')
                setattr(self.client, attr_name, decorated_method)