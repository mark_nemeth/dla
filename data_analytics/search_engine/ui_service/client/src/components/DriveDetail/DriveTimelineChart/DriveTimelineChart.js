import React from 'react';
import Chart from "react-apexcharts";
import {DriveItemTypes} from "../DriveDetail";
import {timestampToTimeStringConverter} from "../../../utils/valueConverter";

class DataWrapper {
  name;
  item;
  dataPoint;
  itemType;

  constructor(item, start_timestamp, end_timestamp, itemType, fillColor, rowLabel) {
    this.item = item;
    this.dataPoint = {
      x: rowLabel,
      y: [start_timestamp, end_timestamp],
      fillColor: fillColor
    };
    this.itemType = itemType;
  }

  static forSystemRun(system_run) {
    return new DataWrapper(system_run, system_run.start, system_run.end, DriveItemTypes.SYSTEM_RUN,
        '#007A93', DriveItemTypes.SYSTEM_RUN)
  }

  static forRecording(recording, type) {
    return new DataWrapper(recording, recording.start, recording.end, type, '#007A93', type)
  }

  static forEngagement(engagement) {
    return new DataWrapper(engagement, engagement.engagement, engagement.disengagement, DriveItemTypes.ENGAGEMENT,
        '#546E7A', DriveItemTypes.ENGAGEMENT)
  }

  static forMission(mission, possible_mission_ends) {
    let mission_end = Math.min(...possible_mission_ends.filter(date => date > mission.start));
    return new DataWrapper(mission, mission.start, mission_end, DriveItemTypes.MISSION,
        '#546E7A', DriveItemTypes.MISSION)
  }

  static forDrive(drive) {
    return new DataWrapper(drive, drive.start, drive.end, DriveItemTypes.DRIVE, '#0D47A1', DriveItemTypes.DRIVE)
  }

  static forAnnotation(annotation, possible_end_markers) {
    let startDate = new Date(annotation.timestamp);
    let endDate = new Date(Math.min(...possible_end_markers.filter(t => t > annotation.timestamp)))
    return new DataWrapper(annotation, startDate.getTime(), endDate.getTime(), DriveItemTypes.ANNOTATION,
        '#FF9800', DriveItemTypes.DRIVE)
  }
}

export function DriveTimelineChart(props) {
  let data_wrappers;

  function setSelectedDriveItem(selectedDriveItem) {
    const selectedItem = {
      item: selectedDriveItem.item,
      itemType: selectedDriveItem.itemType
    }
    props.showSelectedDriveItem(selectedItem);
  }

  function getTimelineSeriesFromDrive(drive) {
    let system_runs_data = drive.system_runs.map(DataWrapper.forSystemRun);
    let always_on_recordings_data = drive.system_runs
        .map(run => run.always_on_recording)
        .map(rec => DataWrapper.forRecording(rec, DriveItemTypes.ALWAYS_ON_RECORDING));
    let on_demand_recordings_data = drive.system_runs
        .flatMap(run => run.on_demand_recordings)
        .map(rec => DataWrapper.forRecording(rec, DriveItemTypes.ON_DEMAND_RECORDING));
    let engagements_data = drive.system_runs.flatMap(run => run.autonomous_drives).map(DataWrapper.forEngagement);
    let annotations_data = drive.annotations.map(annotation => {
      const interval = (drive.end - drive.start) * 0.01
      const possible_ends = [drive.end, annotation.timestamp + interval]
          .concat(drive.annotations.map(a => a.timestamp))
      return DataWrapper.forAnnotation(annotation, possible_ends)
    })

    let always_on_ends = drive.system_runs
        .map(run => run.always_on_recording)
        .map(recording => recording.recording_end);
    let on_demand_ends = drive.system_runs
        .flatMap(run => run.on_demand_recordings)
        .map(recording => recording.recording_end);
    let possible_mission_ends = [drive.end].concat(always_on_ends, on_demand_ends);

    let missions_data = drive.system_runs
        .flatMap(run => run.missions)
        .map(mission => DataWrapper.forMission(mission, possible_mission_ends));

    let drive_data = DataWrapper.forDrive(drive);

    data_wrappers = engagements_data.concat(missions_data, on_demand_recordings_data, always_on_recordings_data,
        system_runs_data, drive_data, annotations_data);

    for (const wrapper of data_wrappers) {
      if (wrapper.dataPoint.y[0] < drive.start) {
        console.warn("Item start is before drive start for item:", wrapper)
      }
      if (wrapper.dataPoint.y[1] > drive.end) {
        console.warn("Item end is after drive end for item:", wrapper)
      }
    }

    return [
      {
        name: 'Timestamps',
        data: data_wrappers.map(wrapper => wrapper.dataPoint)
      }
    ]
  }

  function getChartOptions(setSelectedDriveItem) {
    return {
      chart: {
        type: 'rangeBar',
        events: {
          dataPointSelection: function (event, chartContext, config) {
            setSelectedDriveItem(data_wrappers[config.dataPointIndex]);
          }
        },
        animations: {
          enabled: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: true,
          barHeight: '70%'
        }
      },
      xaxis: {
        type: 'datetime',
        labels: {
          formatter: function (value) {
            return timestampToTimeStringConverter(value)
          }
        },
        tickAmount: 5 //required to position the labels at the tick positions
      },
      stroke: {
        width: 1
      },
      fill: {
        type: 'solid',
        opacity: 0.7
      },
      tooltip: {
        enabled: false,
        x: {
          format: 'hh:mm:ss'
        }
      },
      colors: ['#FFFFFF'],
      dataLabels: {
        enabled: true,
        textAnchor: 'middle',
        formatter: function (val, opt) {
          let wrapper = data_wrappers[opt.dataPointIndex];
          let item = wrapper.item;
          switch (wrapper.itemType) {
            case DriveItemTypes.MISSION:
              return item.mission_name || "";
            case DriveItemTypes.ON_DEMAND_RECORDING:
              return item.name || "";
            case DriveItemTypes.SYSTEM_RUN:
              return item.test_drive_type || "";
            case DriveItemTypes.ENGAGEMENT:
              return item.disengagement_type || "";
            default:
              return "";
          }
        },
      },
    }
  }

  return (
      <Chart series={getTimelineSeriesFromDrive(props.drive)}
             options={getChartOptions(setSelectedDriveItem)}
             type="rangeBar"
             height="450"/>
  )
}