"""Silent Testing Interpreter"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import yaml
from config.constants import *
from extractors.utils import rospytime2int, timestamp2ms, aracom_timestamp2ms
from extractors.base_interpreter import Interpreter

# consider only one silent testing event in every X sec in order not to produce to many snippets
BVA_LIDAR_SILENT_TESTING = 10
BREAK_AFTER_SILENT_TESTING_EVENT_LIDAR = 120
SECONDS_BEFORE_SILENT_TESTING_EVENT = 3
SECONDS_AFTER_SILENT_TESTING_EVENT = 3


class LidarSilentTestingInterpreter(Interpreter):
    def __init__(self, name):
        Interpreter.__init__(self, name)
        self.results = {}
        self.lidar_silent_testing = []
        self.last_silent_testing_event_ts = None
        self.bva = BVA_LIDAR_SILENT_TESTING

    def feed_new_msg(self, topic, msg, t,):
        if msg.silent_testing.is_valid_trigger:
            lidar_silent_testing = {}
            lidar_silent_testing["ts"] = aracom_timestamp2ms(msg.header.timestamp)
            msg = yaml.load(str(msg.silent_testing))
            lidar_silent_testing["msg"] = msg
            lidar_silent_testing["topic"] = topic

            if self.last_silent_testing_event_ts is None \
                    or lidar_silent_testing["ts"] - self.last_silent_testing_event_ts > BREAK_AFTER_SILENT_TESTING_EVENT_LIDAR * 1e3:
                lidar_silent_testing = self.calc_start_end_ts_snippet(lidar_silent_testing)
                self.event_list += [lidar_silent_testing]
                self.last_silent_testing_event_ts = lidar_silent_testing["ts"]

            return self.lidar_silent_testing
        return None

    @staticmethod
    def calc_start_end_ts_snippet(event):
        event["start_ts"] = int(event["ts"] - SECONDS_BEFORE_SILENT_TESTING_EVENT * 1e3)
        event["end_ts"] = int(event["ts"] + SECONDS_AFTER_SILENT_TESTING_EVENT * 1e3)
        return event
