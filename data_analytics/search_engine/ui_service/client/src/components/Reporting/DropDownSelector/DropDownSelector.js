import React from 'react';
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import Form from 'react-bootstrap/Form';
import './DropDownSelector.scss';


export function DropDownSelector(props) {

  return (
    <Form className="dropdown-wrapper">
      <DropdownButton
        onSelect={props.onSelect}
        title={props.prefix + props.items[props.selectedItem].name}>
        {Object.keys(props.items).map(key => (
          <Dropdown.Item
            key={key}
            eventKey={key}
            disabled={!props.items[key].enabled}>
            {props.items[key].name}
          </Dropdown.Item>
        ))}
      </DropdownButton>
    </Form>
  );
}
