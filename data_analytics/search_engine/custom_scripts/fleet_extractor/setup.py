"""Setup file"""
from setuptools import setup, find_packages

setup(
    name='fleet_extractor',
    version='0.0.7',
    packages=find_packages(),
    install_requires=[
        'gnupg==2.3.1',
        'DANFUtils==0.2.29',
        'DANFTracingClient==0.1.11',
        'ContainerClient==0.6.102',
        'pycrypto'
    ]
)
