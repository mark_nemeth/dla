# ATPDML Co-Driver Event Extractor 

This DANF Extractor filters bagfiles for events that have been 
tagged by a Co-Driver manually during ATPDML Special Acquisition 
Drives. During such drives the Co-Driver can access a touch panel 
in the car and annotate various interesting events like parking, 
special signs, vulnerable road users etc.

# Interface 

During drives, events are published to the `/events` topic. The
respective ROS-message contains the type of event in the field 
`frame-id`:

```
stamp: 
  secs: 1565122015
  nsecs: 262175921
frame_id: "Parking"
``` 

This is translated to a DANF extraction request containing the 
respective rosbag-snippet with the event in the middle of a 6 
seconds long snippet (currently hard-coded within the extractor).

The extraction request saved to the DANF metadata database looks 
like follows:

```
{
  'version': '1.0',
  'bagfile_guid': 'fb59ecdb-72bb-4388-81b8-9b64e721678a', 
  'extractor_id': 'codrive:1.0',
  'metadata_list': [
    {
        'start': 1565121978,
        'end': 1565122069,
        'type': 'codrive',
        'metadata': {
            'ts': 1565121994.8745327,
            'frame_id': 'Parking',
            'namespace': 'codrive',
            'version': '1.0'
        },
        'bva': 10
    },
    ... 
  ]
}
```

The `extractor_id`, the `metadata_list[...].metadata.version`,
and the -`.namespace` fields are used to identify the version of 
the extractor running and serve to manage any future metadata 
format changes.

# Building

The `codrive` container needs a base container called `altros(:latest)`
which needs to be built separately with connection to the Dedicated
Athena Platform. Therefore, run `altros/build.sh` first, then
`codrive/build.sh` - or export the altros Docker image to the build
environment and run the Docker build process there.

# Test it Yourself

You can get a sample bag file which has three events annotated 
from the following sources:

On MapR-exUlm@Vaih: `.../input/rd/athena/08_robotaxi/vis/WDD2221591A417319/2019/08/06/20190806_194026_DML_PER_Route_4_BW_cv1_2.bag` (carful, it's quite big)

You can also use the files in `codrive_test`, e.g. `./build-and-test.sh`
on local machine in order to test locally.

```
./build-and-test.sh <path-to-bag> <bag-guid>
```

The test script writes output of the container to a 
separate log file `docker-*.log`. If you see an exception that
DANF API is not reachable, this is perfectly normal and indicates
that a request would be made to DANF (which usually is not reachable
on your localhost). 

# Learn More

* [About the recent DML Special Recording](https://athena.daimler.com/confluence/pages/viewpage.action?pageId=154319776)
* [About DANF Extractors](https://athena.daimler.com/confluence/display/ATROB/How+to+add+your+own+Extractor+to+DANF)
