"""Setup file"""
from setuptools import setup, find_packages
from config.constants import VERSION

setup(name='test_framework',
      version=VERSION,
      packages=find_packages(),
      test_suite="tests",
      install_requires=[
          'pyassert>=0.4.2', 'requests>=2.22.0', 'configparser>=3.8.1',
          'apscheduler>=3.6.0', 'ContainerClient==0.6.102'
      ])
