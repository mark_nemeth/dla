#include <getopt.h>
#include <vector>
#include <chrono>
#include "ros/ros.h"
#include "dla/TestMsg.h"

using namespace std;
using namespace std::chrono;

// Settings
long long runTimeMs = 30 * 1000;
int topicStartIdx = 0;
int topicCount = 50;
unsigned msgSize = 128 * 1024;
double hz = 85;
const char *topicNamePrefix = "/test_";

const int headerSize = (4 + 8 + 4) + sizeof(dla::TestMsg::_id_type) + 4;

struct TxContext
{
	ros::Publisher pub;
	dla::TestMsg msg;
};

void help()
{
	printf("tx [options]\n");
	printf("Options:\n");
	printf(" -t,--time <runtime ms>\n");
	printf(" -r,--rate <Hz>\n");
	printf(" -s,--start <start index>\n");
	printf(" -c,--count <count>\n");
	printf(" --size <message size>\n");
	printf(" --prefix <topic_name_prefix>\n");
}

int main(int argc, char **argv)
{
	// Parse options
    int idx = 1;
    while (idx < argc)
    {
        if (strcmp(argv[idx], "-t") == 0 || strcmp(argv[idx], "--time") == 0)
        {
        	runTimeMs = atoll(argv[++idx]);
        }
        else if (strcmp(argv[idx], "-r") == 0 || strcmp(argv[idx], "--rate") == 0)
        {
        	hz = atof(argv[++idx]);
        }
        else if (strcmp(argv[idx], "-s") == 0 || strcmp(argv[idx], "--start") == 0)
        {
        	topicStartIdx = atoi(argv[++idx]);
        }
        else if (strcmp(argv[idx], "-c") == 0 || strcmp(argv[idx], "--count") == 0)
        {
        	topicCount = atoi(argv[++idx]);
        }
        else if (strcmp(argv[idx], "--size") == 0)
        {
        	msgSize = max(headerSize, atoi(argv[++idx]));
        }
        else if (strcmp(argv[idx], "--prefix") == 0)
        {
        	topicNamePrefix = argv[++idx];
        }
        else
        {
        	help();
        	return 0;
        }

        idx++;
    }

	// Initialize node
	ros::init(argc, argv, "tx", ros::init_options::AnonymousName);
	ros::NodeHandle n;

	// Publish publishers
	vector<TxContext> senders;
	for (unsigned i = topicStartIdx; i < topicStartIdx + topicCount; i++)
	{
		char topicName[256];
		sprintf(topicName, "%s%03u", topicNamePrefix, i);

		TxContext ctx;
		// Generate message
		ctx.msg.id = i;
		ctx.msg.header.seq = 0;
		unsigned dataSize = msgSize - headerSize;
		ctx.msg.data.resize(dataSize);
		for (size_t j = 0; j < dataSize; j++)
			ctx.msg.data[j] = (uint8_t)j;

		printf("TX topic: %s message size: %d, rate: %.2f Hz\n", topicName, msgSize, hz);
		ctx.pub = n.advertise<dla::TestMsg>(string(topicName), 1000);
		senders.push_back(ctx);
	}

	static high_resolution_clock::time_point start = high_resolution_clock::now();

	// Run
	ros::Rate loop_rate(hz);
	while (ros::ok())
	{
		// Check run time timeout
		high_resolution_clock::time_point t = high_resolution_clock::now();
		long long dt = duration_cast<milliseconds>(t - start).count();
		if (runTimeMs < dt)
			break;

		// Publish all topic messages
		for (TxContext &ctx : senders)
		{
			// Set transmission timestamp
			ctx.msg.header.stamp = ros::Time::now();
			// Publish message
			ctx.pub.publish(ctx.msg);
			// Increment sequence counter
			ctx.msg.header.seq++;
		}

		ros::spinOnce();

		loop_rate.sleep();
	}

	if (ros::isStarted())
	{
		printf("Shutdown\n");
		ros::shutdown();
		ros::waitForShutdown();
	}

	return 0;
}
