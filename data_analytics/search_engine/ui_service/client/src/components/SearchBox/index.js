import {connect} from 'react-redux';
import {filterBySearchQuery, setCurrentSearchQuery} from '../../redux/Home/HomeActions';
import {SearchBox} from './SearchBox';
import {FileTypes} from "../Sidebar/FilterElements/ByFileFilter/ByFileFilter";

function mapStateToProps(state, ownProps) {
  const fileType = state.fileType
  let searchQuery;
  let hint;
  switch (fileType) {
    case FileTypes.DRIVE:
      searchQuery = state.filters.description || "";
      hint = "Search by description ...";
      break;
    default:
      searchQuery = state.filters.file_name || "";
      hint = "Search by name ...";
      break;
  }
  return {hint, fileType, searchQuery};
}

export default connect(
    mapStateToProps,
    {setCurrentSearchQuery, filterBySearchQuery}
)(SearchBox);