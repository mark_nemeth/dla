"""Setup file"""
from setuptools import setup, find_packages
from config.constants import VERSION

setup(name='performance-testsuite',
      version=VERSION,
      packages=find_packages(),
      test_suite="tests",
      install_requires=[
          'locustio==0.9.0',
          'invokust>=0.5.2',
          'pymongo==3.12.2',
          'DANFUtils>=0.2.29',
          'DBConnector>=0.4.70'
      ])
