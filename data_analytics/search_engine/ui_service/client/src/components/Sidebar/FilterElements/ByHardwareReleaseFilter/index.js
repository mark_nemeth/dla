import {connect} from 'react-redux';
import {filterByHardwareReleases} from '../../../../redux/Home/HomeActions';
import {ByHardwareReleaseFilter} from './ByHardwareReleaseFilter';

function mapStateToProps(state, ownProps) {
  let hardwareReleaseList = state.filterOptions.hardwareReleaseList || [];
  const selectedHardwareReleaseList = state.filters.hardware_release || [];

  return {selectedHardwareReleaseList, hardwareReleaseList};
}

export default connect(
  mapStateToProps,
  {filterByHardwareReleases}
)(ByHardwareReleaseFilter);