#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/app/altros/devel/.private/rosbag:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/app/altros/devel/.private/rosbag/lib:$LD_LIBRARY_PATH"
export PWD="/app/altros/build/rosbag"
export PYTHONPATH="/app/altros/devel/.private/rosbag/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/app/altros/devel/.private/rosbag/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/app/altros/src/rosbag:$ROS_PACKAGE_PATH"