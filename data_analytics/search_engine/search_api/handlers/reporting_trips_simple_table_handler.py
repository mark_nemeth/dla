"""Handler class for feature search"""

__copyright__ = """
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
"""

import logging

from handlers.db_data_handler import DBDataHandler
from handlers.reporting_utils import *

logger = logging.getLogger(__name__)

DISENGAGEMENT_DETAILS_TEXTS = {
    "m1": "The planner was not able to generate a valid trajectory.",
    "m1b": "A valid trajectory could not be generated.",
    "m1c":
    "The planner was not able to generate a valid trajectory within a time cycle.",
    "m2": "The system asked the operator to take control of the vehicle.",
    "m2b": "The operator took manual control of the vehicle.",
    "m3": "Unknown error that caused the system to disengage.",
    "m4":
    "The vehicle operator disabled the autonomous system by pressing the appropriate button.",
    "m5":
    "The system disengaged because the driver seat belt not being properly fastened.",
    "m6":
    "The system disengaged because the driver had reached the maximum driving time in autonomous mode.",
    "m7": "The system disengaged because of an error with the maps component.",
    "m8":
    "The system disengaged because of an error with the fusion component.",
    "m9": "A general error caused the system to stop the engaged status.",
    "m10":
    "The operator disengaged the system manually to remain in the operational design domain.",
    "m10a":
    "This was achieved by pressing the accelerator pedal to increase velocity of the vehicle.",
    "m10b":
    "This was accomplished by pressing the brake pedal to reduce the velocity of the vehicle.",
    "m10c":
    "The driver performed a steering manouver to correct the trajectory of the vehicle.",
    "m11":
    "An issue with motion control system caused the system to disengage.",
    "m11b":
    "The component motion control could not initialize correctly, causing the system to disengage.",
    "m12":
    "An error with imar initialization resulted in the disengagement of the system.",
    "m13":
    "A middlware component was offline, leading to the system disengagement.",
    "m14":
    "An issue with localization component caused the system to disengage.",
    "m15":
    "An issue with pedestrian detection caused the system to disengage.",
    "m16": "An issue with environment model caused the system to disengage."
}

col_labels = ["VIN", "Date", "Disengagement type", "Disengagement details"]
caption = "Unplanned Disengagement Details"


class ReportingTripsSimpleTableHandler:
    """
    Class for fetching report data (kpis/tables) 
    """

    def __init__(self):
        self.db_handler = DBDataHandler()

    def get_table(self, start: int, end: int, location: str, report_type: str,
                  ign_dur: bool, ign_dist: bool, ign_no_info: bool,
                  ign_priv: bool) -> list:
        """
           Get a simple table
            :param start: start timestamp in ns
            :param end: end timestamp in ns
            :param location: country string ["DE", "US"]
            :param report_type: Type of the report ["DMV"]
            :param ign_dur: Ignore trips with short duration
            :param ign_dist: Ignore trips with short distance
            :param ign_no_info:  Ignore trips with no diseng info
            :param ign_priv: Ignore trips on private roads
            :return: dict
        """
        if location not in REPORTING_LOCATIONS:
            raise ValueError(
                f"'location' has to be one of {REPORTING_LOCATIONS.keys()}")
        else:
            loc = REPORTING_LOCATIONS[location]
            if report_type == REPORT_TYPE_DMV:
                # get diseng values and days of testing from db
                result_dict = self.db_handler.aggregate_extractor_requests(
                    self._pipeline(start, end, loc, ign_dur, ign_dist,
                                   ign_no_info, ign_priv))
                if len(result_dict) > 0:
                    result_table = self._transform_table(result_dict)
                    return [{
                        "caption": caption,
                        "col_labels": col_labels,
                        "values": result_table[0],
                        "key_value_map": result_table[1]
                    }]
            return []

    def _pipeline(self, start: int, end: int, location: str, ign_dur: bool,
                  ign_dist: bool, ign_no_info: bool,
                  public_only: bool) -> list:
        return [
            self._match_stage(start, end, location, ign_dur, ign_dist,
                              ign_no_info, public_only),
            self._project_stage()
        ]

    @staticmethod
    def _match_stage(start: int, end: int, location: str, ign_dur: bool,
                     ign_dist: bool, ign_no_info: bool,
                     public_only: bool) -> dict:
        stage = {
            "start": {
                "$gte": start
            },
            "end": {
                "$lte": end
            },
            "extractor_id": "feature_extraction_diseng_details",
            "metadata.end_planned": {
                "$ne": True
            }
        }
        if location in ["DE", "US"]:
            stage["metadata.country"] = location
        elif location != "Any":
            stage["metadata.geofence"] = location

        if ign_no_info:
            stage["metadata.end_details"] = {"$ne": None}
        if ign_dist:
            stage["metadata.distance"] = {"$gte": MIN_DRIVE_DISTANCE}
        if ign_dur:
            stage["metadata.duration"] = {"$gte": MIN_DRIVE_DURATION}
        if public_only:
            stage["$or"] = [{
                "metadata.end_public_road": {
                    "$eq": True
                }
            }, {
                "metadata.distance_public": {
                    "$ne": 0
                }
            }]
        return {"$match": stage}

    @staticmethod
    def _project_stage() -> dict:
        return {
            "$project": {
                "vin": "$metadata.vin",
                "date": {
                    "$substr": ["$metadata.end_ts", 0, 10]
                },
                "disengagement_type": {
                    "$cond": [{
                        "$ne": ["$metadata.manual_disengagement", True]
                    }, "error", "manual"]
                },
                "end_details": "$metadata.end_details",
            }
        }

    def _transform_table(self, result_dict):
        vin_value_list = list(
            set([
                d["vin"] if d["vin"] is not None else "Unknown"
                for d in result_dict if "vin" in d
            ] + ["Unknown"]))
        vin_value_list.sort()
        details_value_list = list(
            set([d["end_details"]
                 for d in result_dict if "end_details" in d] + [""]))
        details_value_list = [
            "" if v is None else v for v in details_value_list
        ]
        details_value_list.sort()
        vin_key_value_map = {
            i: vin_value_list[i]
            for i in range(0, len(vin_value_list))
        }
        details_key_value_map = {
            f"d{i}": details_value_list[i]
            for i in range(0, len(details_value_list))
        }

        result_table = []
        for item in result_dict:
            result_entry = [
                self._replace_value_by_key(item.get("vin", "Unknown"),
                                           vin_key_value_map),
                item.get("date"),
                item.get("disengagement_type"),
                self._replace_value_by_key(item.get("end_details", ""),
                                           details_key_value_map)
            ]
            result_table.append(result_entry)

        # Translate details messages into texts
        details_key_value_map_translated = dict(
            (k, self._translate_into_details_text(v))
            for k, v in details_key_value_map.items())

        key_value_map = {
            **vin_key_value_map,
            **details_key_value_map_translated
        }

        return result_table, key_value_map

    @staticmethod
    def _replace_value_by_key(value, key_value_map):
        if value is None:
            value = ""
        key = None
        for k, v in key_value_map.items():
            if v == value:
                key = k
                break
        return key

    @staticmethod
    def _translate_into_details_text(details_str) -> str:
        detail_text = []
        if details_str:
            if any(d in details_str for d in [
                    "[10.1]AV system handoff", "[18]Error in trajectory check",
                    "[10]PLN no valid trajectory available"
            ]):
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m1"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if "[19]Error in trajectory check - NaN/SNA/MIN/MAX detected" in details_str:
                detail_text.append(DISENGAGEMENT_DETAILS_TEXTS.get("m1b"))
            if any(d in details_str for d in [
                    "[51]Distronic lever pushed",
                    "[52]Accelerator pedal pressed", "[53]Brake pedal pressed",
                    "[54]Steering takeover",
                    "[55]Acceleration or brake pedal pressed"
            ]):
                detail_text.append(DISENGAGEMENT_DETAILS_TEXTS.get("m10"))
            if "[51]Distronic lever pushed" in details_str:
                detail_text.append(DISENGAGEMENT_DETAILS_TEXTS.get("m2b"))
            if "[52]Accelerator pedal pressed" in details_str:
                detail_text.append(DISENGAGEMENT_DETAILS_TEXTS.get("m10a"))
            if "[53]Brake pedal pressed" in details_str or "[55]Acceleration or brake pedal pressed" in details_str:
                detail_text.append(DISENGAGEMENT_DETAILS_TEXTS.get("m10b"))
            if "[54]Steering takeover" in details_str:
                detail_text.append(DISENGAGEMENT_DETAILS_TEXTS.get("m10c"))
            if any(d in details_str for d in [
                    "[7.6]MC_Lon passive dis-engagement", "[7.2]mc init",
                    "[7.3]MC_Lat error", "[7.7]MC_Lat init"
            ]):
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m11"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if "[8]Imar error" in details_str:
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m12"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if "[15.2]Driver seat belt not fastened" in details_str:
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m5"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2b")
                ])
            if "[16]Driver time limit exceeded" in details_str:
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m6"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2b")
                ])
            if any(d in details_str for d in [
                    "[17]Environment model error",
                    "[20]Environment fixed vector size exceeded"
            ]):
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m16"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if "[21]Maps incident detected" in details_str:
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m7"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if any(d in details_str for d in [
                    "[25.2]Fusion Grid World state timeout",
                    "[26.2]Fusion Dynamic World state timeout",
                    "[27.2]Fusion Traffic Regulation state timeout"
            ]):
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m8"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if any(d in details_str for d in [
                    "[5]TI TxTime error - CAL MC Planing issue",
                    "[5]Cmdtraj. timeout"
            ]):
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m1c"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if any(d in details_str for d in
                   ["[13]COM state error", "[9]CAL COM CAN not active"]):
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m13"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if any(d in details_str for d in [
                    "[28.1]Localization state error",
                    "[28.3]Localization incident detected"
            ]):
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m14"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
            if any(d in details_str for d in [
                    "[29.2]Pedestrian detection MRCFL1 state timeout",
                    "[29.5]Pedestrian detection MRCFL2 state timeout"
            ]):
                detail_text.extend([
                    DISENGAGEMENT_DETAILS_TEXTS.get("m15"),
                    DISENGAGEMENT_DETAILS_TEXTS.get("m2")
                ])
        if len(detail_text) == 0:
            if details_str:
                detail_text.append(details_str)
            else:
                detail_text.append(DISENGAGEMENT_DETAILS_TEXTS.get("m3"))
        detail_text_remove_duplicates = []
        [
            detail_text_remove_duplicates.append(x) for x in detail_text
            if x not in detail_text_remove_duplicates
        ]

        return " ".join(detail_text_remove_duplicates)
