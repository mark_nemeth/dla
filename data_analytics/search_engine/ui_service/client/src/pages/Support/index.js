import {connect} from 'react-redux';
import {Support} from './Support';
import {fetchDanfVersionInformation} from '../../redux/App/AppActions';

function mapStateToProps(state, ownProps) {
    const uiVersion = state.versions.uiVersion;
    const danfVersion = state.versions.danfVersion;
    
  
    return {uiVersion, danfVersion};
  }
  
  export default connect(
    mapStateToProps,
    {fetchDanfVersionInformation}
  )(Support);