import React from 'react';
import {shallow} from "enzyme/build";
import {ByAccelerationFilter} from "./ByAccelerationFilter";
import InputRange from 'react-input-range';

describe('ByAccelerationFilter component', () => {

  it('should renders without crashing', () => {
    const props = {
      limits: {
        min: 0,
        max: 30
      },
      duration: {
        min: 1000,
        max: 2000
      },
    };
    const wrapper = shallow(<ByAccelerationFilter {...props} />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should call action when range is selected', () => {
    const reduxAction = jest.fn();
    const props = {
      limits: {
        min: 0,
        max: 30
      },
      duration: {
        min: 1000,
        max: 2000
      },
      filterByDuration: reduxAction
    };
    const wrapper = shallow(<ByAccelerationFilter {...props} />);
    wrapper.find(InputRange).simulate('onChange',{ target: {value: { min: 1000, max:2000 } }})
    wrapper.find(InputRange).simulate('onChangeComplete')
    // expect(reduxAction).toHaveBeenCalled();
  })

});
