"""Regressiontests for sequence routes"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

import os

os.environ["DANF_ENV"] = "local"
import random as rd
import unittest

import requests
from DANFUtils.utils import generate_deterministic_guid

#from handlers.extractor_requests import ExtractorRequestsHandler
from tests.regressiontests.testutils import get_testdb_connector

bagfile_url = 'http://localhost:8887/bagfile/'
sequence_url = 'http://localhost:8887/sequence/'
extractor_request_url = 'http://localhost:8887/extractorrequest/'

rd.seed("test")

bagfile_guid = generate_deterministic_guid()

raw_data_bagfile = {
    "version": "1.0",
    "guid": bagfile_guid,
    "link": "testdata",
    "size": 21231,
    "num_messages": 4444,
    "start": 1557847000000,
    "end": 1557847250000,
    "duration": 752728000,
    "vehicle_id_num": "V-123-234",
    "extractor": "ext1"
}

raw_data_extractor_request = {
    "version":
    "1.0",
    "guid":
    generate_deterministic_guid(),
    "bagfile_guid":
    raw_data_bagfile['guid'],
    "extractor_id":
    "ext1",
    "metadata_list": [{
        "ts": 1557847255000,
        "start": 1557847224000,
        "end": 1557847263000,
        "bva": 5,
        "is_test_data": 1
    }]
}


def prepare_db():
    with get_testdb_connector() as con:
        con.db['bagfiles'].delete_many({'guid': bagfile_guid})
        con.db['extractor_requests'].delete_many(
            {'bagfile_guid': bagfile_guid})
        con.db['sequences'].delete_many({'bagfile_guid': bagfile_guid})


def cleanup_db():
    with get_testdb_connector() as con:
        con.db['bagfiles'].delete_many({'guid': bagfile_guid})
        con.db['extractor_requests'].delete_many(
            {'bagfile_guid': bagfile_guid})
        con.db['sequences'].delete_many({'bagfile_guid': bagfile_guid})


class TestExtractorRequestPOST(unittest.TestCase):
    def setUp(self):
        prepare_db()

    def tearDown(self):
        cleanup_db()

    def test_extractor_request_POST_insert(self):
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        data = {
            "version":
            "0.1",
            "bagfile_guid":
            bagfile_guid,
            "extractor_id":
            "ext1",
            "metadata_list": [{
                "start": 1557847248000,
                "end": 1557847250000,
                "metadata": {
                    "key1": "value1"
                },
                "is_test_data": 1
            }, {
                "start": 1557847247000,
                "end": 1557847249000,
                "bva": 7,
                "is_test_data": 1
            }]
        }

        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(201, res.status_code, 'Sequence post failed')
        self.assertRegex(
            res.headers['Location'],
            '/extractorrequest/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_extractor_request_POST_invalid_bagfile_guid(self):
        data = {
            "version":
            "0.1",
            "bagfile_guid":
            "12345678-abcd-1234-abcd-0123456789ab",
            "extractor_id":
            "ext1",
            "metadata_list": [{
                "start": 1475273900000,
                "end": 1475274900000,
                "bva": 3,
                "is_test_data": 1
            }]
        }

        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(500, res.status_code,
                         'Fake bagfile_guid not detected')

    def test_extractor_request_POST_missing_attributes(self):
        data = {
            "bagfile_guid": "12345678-abcd-1234-abcd-0123456789ab",
            "extractor_id": "ext1"
        }

        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(400, res.status_code,
                         'Missing attributes not detected')

    def test_extractor_request_POST_empty_metadata(self):
        data = {
            "bagfile_guid": "12345678-abcd-1234-abcd-0123456789ab",
            "extractor_id": "ext1",
            "metadata_list": []
        }

        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(400, res.status_code, 'Missing metadata not detected')

    def test_extractor_request_POST_timestamp_out_of_range(self):
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        data = [{
            "version": "1.0",
            "extractor_version": "1.0",
            "bagfile_guid": bagfile_guid,
            "extractor_id": "logbook",
            "ts": 1557847111111,
            "start": 1555555500000,
            "end": 1557847119999,
            "bva": 1,
            "metadata": {
                "ts": 1557847125000,
                "summary": "logbook summary message"
            },
            "tags": ["logbook"],
            "event_type": "filter"
        }]
        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(400, res.status_code,
                         'Invalid timestamps not detected')

    def test_extractor_request_POST_legacy_subsecond_interval(self):
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        data = {
            "version":
            "0.1",
            "bagfile_guid":
            bagfile_guid,
            "extractor_id":
            "ext1",
            "metadata_list": [{
                "start": 1557847248100,
                "end": 1557847248600,
                "metadata": {
                    "key1": "value1"
                },
                "is_test_data": 1
            }]
        }

        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(201, res.status_code, 'Sequence post failed')
        self.assertRegex(
            res.headers['Location'],
            '/extractorrequest/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    # def test_extractor_DELETE_delete(self):
    #     connector = get_testdb_connector()
    #     with connector as con:
    #         con.add_bagfiles([raw_data_bagfile])
    #
    #     rqh = ExtractorRequestsHandler()
    #     extr_req = {
    #         'version':
    #         '0.1',
    #         'bagfile_guid':
    #         bagfile_guid,
    #         'extractor_id':
    #         'ext1',
    #         'metadata_list': [{
    #             'start': 1557847248000,
    #             'end': 1557847250000,
    #             'metadata': {
    #                 'key1': 'value1'
    #             },
    #             'is_test_data': 1
    #         }, {
    #             'start': 1557847247000,
    #             'end': 1557847249000,
    #             'bva': 7,
    #             'is_test_data': 1
    #         }],
    #         'guid':
    #         ''
    #     }
    #     guids = rqh.add_extractor_requests_to_db_legacy(extr_req)
    #     res = requests.delete(url=extractor_request_url + guids[0])
    #
    #     self.assertEqual(res.status_code,
    #                      200,
    #                      msg="Deleting metadata request failed!")
    #     res = requests.delete(url=extractor_request_url + guids[0])
    #     self.assertEqual(res.status_code,
    #                      500,
    #                      msg="Metadata request was not deleted!"
    #                      )  # check if it was really deleted

    def test_extractor_request_OPTIONS(self):
        res = requests.options(url=extractor_request_url)
        self.assertEqual(200, res.status_code,
                         'Fetching Extractor Request route options failed')
        expected_result = 'OPTIONS, POST, DELETE'
        self.assertEqual(expected_result,
                         res.headers['Access-Control-Allow-Methods'],
                         'Fetching Extractor Request  route options failed')

    def test_extractor_request_v1_POST(self):
        connector = get_testdb_connector()

        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        data = [{
            "version": "1.0",
            "extractor_version": "1.0",
            "bagfile_guid": bagfile_guid,
            "extractor_id": "gps",
            "ts": 1557847111222,
            "start": 1557847110000,
            "end": 1557847111999,
            "bva": 0,
            "metadata": {
                "latitude": 50.000000000000000,
                "longitude": 9.0000000000
            },
            "tags": ["tag"],
            "event_type": "metadata_enrichment"
        }, {
            "version": "1.0",
            "extractor_version": "1.0",
            "bagfile_guid": bagfile_guid,
            "extractor_id": "logbook",
            "ts": 1557847111111,
            "start": 1557847100000,
            "end": 1557847119999,
            "bva": 1,
            "metadata": {
                "ts": 1557847125000,
                "summary": "logbook summary message"
            },
            "tags": ["logbook"],
            "event_type": "filter"
        }]

        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(201, res.status_code, 'extractor post failed')
        self.assertRegex(
            res.headers['Location'],
            '/extractorrequest/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

        # test upsert
        data[0]["bva"] = 5
        data[0]["metadata"]["test"] = "test"
        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(201, res.status_code, 'extractor post failed')
        self.assertRegex(
            res.headers['Location'],
            '/extractorrequest/([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})$',
            'Invalid Location header')

    def test_extractor_request_v1_POST_should_only_accept_version_1_0(self):
        connector = get_testdb_connector()
        with connector as con:
            con.add_bagfiles([raw_data_bagfile])
        data = [{
            "version": "2.0",
            "extractor_version": "1.0",
            "bagfile_guid": bagfile_guid,
            "extractor_id": "gps",
            "ts": 1541684694000,
            "start": 1541684694000,
            "end": 1541684694999,
            "bva": 10,
            "metadata": {
                "latitude": 50.000000000000000,
                "longitude": 9.0000000000
            },
            "tags": ["tag"],
            "event_type": "metadata_enrichment",
            "is_test_data": 1
        }]
        res = requests.post(url=extractor_request_url, json=data)
        self.assertEqual(400, res.status_code, 'Wrong version not detected')


if __name__ == '__main__':
    unittest.main()
