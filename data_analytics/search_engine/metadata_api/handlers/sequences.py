"""Handler class for sequences"""

__copyright__ = '''
COPYRIGHT: (c) 2019 Daimler AG and Robert Bosch GmbH
The reproduction, distribution and utilization of this file as
well as the communication of its contents to others without express
authorization is prohibited. Offenders will be held liable for the
payment of damages and can be prosecuted. All rights reserved
particularly in the event of the grant of a patent, utility model
or design.
'''

from DANFUtils.utils import generate_guid
from handlers.db_data_handler import DB_Data_Handler
from handlers.bagfiles import BagFilesHandler

COUNT_KEY = "count"
RESULTS_KEY = "results"

GPS_TAG = "gps"
GPS_VERIFICATION_LIST = {
    "maxLatitude": (int, float, complex),
    "minLatitude": (int, float, complex),
    "maxLongitude": (int, float, complex),
    "minLongitude": (int, float, complex)
}


class SequenceHandler():
    def __init__(self):
        self.db_data_handler = DB_Data_Handler()
        self.bagfile_handler = BagFilesHandler()

    def add_sequence_to_db(self, sequence):
        bagfile_guid = sequence["bagfile_guid"]
        sequence['guid'] = generate_guid()
        self.db_data_handler.add_sequences([sequence])
        self.bagfile_handler.update_field([bagfile_guid], [sequence['guid']], "sequences", append=True)
        return sequence['guid']

    def get_sequence_from_db(self, guid):
        return self.db_data_handler.get_sequences([guid])

    def update_sequence_in_db(self, guid, data):
        self.db_data_handler.update_sequences([guid], [data])

    def delete_sequence_from_db(self, guid):
        self.db_data_handler.delete_sequences([guid])
