"""Setup file"""
from setuptools import setup, find_packages
from distutils.cmd import Command
from distutils.log import INFO
import subprocess
import os


class RunPylint(Command):
    """Run pyling on all python source files"""

    description = 'run Pylint on Python source files'
    user_options = [
        # (long option, short option, description).
        ('pylint-rcfile=', None, 'path to Pylint config file'),
    ]

    def initialize_options(self):
        self.pylint_rcfile = '.pylintrc'

    def finalize_options(self):
        if self.pylint_rcfile:
            assert os.path.exists(self.pylint_rcfile), (
                    'Pylint config file %s does not exist.' % self.pylint_rcfile)

    def run(self):
        command = ['pylint']
        if self.pylint_rcfile:
            command.append('--rcfile=%s' % self.pylint_rcfile)
        command.append(os.getcwd() + "/")
        self.announce(
            'Running command: %s' % str(command),
            level=INFO)
        subprocess.check_call(command)


setup(
    name='code_check',
    version='0.27.0',
    packages=find_packages(),
    long_description=open('README.md', 'r').read(),
    long_description_content_type="text/markdown",
    test_suite="tests",
    install_requires=[
        'yapf>=0.27.0'
    ],
    cmdclass={
        'pylint': RunPylint,
    }
)
